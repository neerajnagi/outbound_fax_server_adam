require 'sidekiq'
require_relative './global.rb'
require_relative './send_fax_unit.rb'
class SendFax
	include Sidekiq::Worker
	def perform(uuid , destinations,attachment)
		puts "#{uuid} new fax job #{attachment}  #{destinations}"
                destinations.split(",").map{|x| x.strip}.uniq.each do |dest|
				SendFaxUnit.perform_async(select_node, uuid, attachment, dest)
		end
	end
	def select_node
		ALL_FS.sample
	end	

end
=begin

	def reset_counter
		@counter = 0 
	end
	def incr_counter
		@counter = @counter + 1
	end
	def counter_overflow
		if @counter > BATCH_SIZE
			reset_counter
			return true
		else
			incr_counter
			return false
		end
	end
	def calls_count(host)
		return `#{FS_CLI} -p #{FS_CLI_PASSWORD} -H #{host} -x "show calls count"`.strip.gsub(/\s+.*$/,'').to_i
	end
	def	select_node 
		while(true)
			ALL_FS.shuffle.each do |node|
				node_count = calls_count(node)
				puts ">>>>>>>>>>>>>>>>  #{node} - #{node_count}    <<<<<<<<<<<<<<<<<<<<"
				if node_count < (NODE_CAPACITY - BATCH_SIZE)
					return node
				end
			end
		puts "goodnight !!!!!!!!!!!!!!!!!!!!!!!!"
		sleep(SLEEP_INTERVAL)
		puts "!!!!!!!!!!!!!!!!!!!!! goodmorning"
		end
	end
end
=end
