require 'sidekiq'
require_relative './global.rb'
class SendFaxUnit
	include Sidekiq::Worker

        def perform(host, uuid, attachment, dest)
                puts "FAX JOB Addition -> [#{uuid}, #{host}, #{dest}"
		sleep(INTERSESSION_DELAY)

		while(true)
		
			if calls_count(host) < NODE_CAPACITY
        		        Process.spawn("#{FS_CLI} -p #{FS_CLI_PASSWORD} -H #{host} -x \"originate {job_id=#{uuid},provider_prefix=#{PROVIDER_PREFIX},ignore_early_media=true,absolute_codec_string='PCMU,PCMA',fax_enable_t38=true,fax_verbose=true,fax_use_ecm=true,fax_enable_t38_request=true}sofia/external/#{PROVIDER_PREFIX}#{dest}@#{PROVIDER}    &txfax('#{REPO}/#{attachment}')\" ")
		                puts "----------------------------------------"
				break
			else
				sleep(SLEEP_INTERVAL)
			end
		end
        end
        def calls_count(host)
                return `#{FS_CLI} -p #{FS_CLI_PASSWORD} -H #{host} -x "show calls count"`.strip.gsub(/\s+.*$/,'').to_i
        end


end

