(function () {

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// packages/underscore-tests/each_test.js                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Tinytest.add("underscore - each", function (test) {                    // 1
  // arrays                                                            // 2
  _.each([42], function (val, index) {                                 // 3
    test.equal(index, 0);                                              // 4
    test.equal(val, 42);                                               // 5
  });                                                                  // 6
                                                                       // 7
  // objects with 'length' field aren't treated as arrays              // 8
  _.each({length: 42}, function (val, key) {                           // 9
    test.equal(key, 'length');                                         // 10
    test.equal(val, 42);                                               // 11
  });                                                                  // 12
                                                                       // 13
  // The special 'arguments' variable is treated as an                 // 14
  // array                                                             // 15
  (function () {                                                       // 16
    _.each(arguments, function (val, index) {                          // 17
      test.equal(index, 0);                                            // 18
      test.equal(val, 42);                                             // 19
    });                                                                // 20
  })(42);                                                              // 21
                                                                       // 22
  // An object with a 'callee' field isn't treated as arguments        // 23
  _.each({callee: 42}, function (val, key) {                           // 24
    test.equal(key, 'callee');                                         // 25
    test.equal(val, 42);                                               // 26
  });                                                                  // 27
                                                                       // 28
  // An object with a 'callee' field isn't treated as arguments        // 29
  _.each({length: 4, callee: 42}, function (val, key) {                // 30
    if (key === 'callee')                                              // 31
      test.equal(val, 42);                                             // 32
    else if (key === 'length')                                         // 33
      test.equal(val, 4);                                              // 34
    else                                                               // 35
      test.fail({message: 'unexpected key: ' + key});                  // 36
  });                                                                  // 37
                                                                       // 38
                                                                       // 39
  // NOTE: An object with a numberic 'length' field *and* a function   // 40
  // 'callee' field will be treated as an array in IE. This may or may // 41
  // not be fixable, but isn't a big deal since: (1) 'callee' is a     // 42
  // pretty rare key, and (2) JSON objects can't have functions        // 43
  // anyways, which is the main use-case for _.each.                   // 44
});                                                                    // 45
                                                                       // 46
/////////////////////////////////////////////////////////////////////////

}).call(this);
