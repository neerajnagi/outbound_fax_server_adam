(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/mongo_livedata_tests.js                                                                     //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
// This is a magic collection that fails its writes on the server when                                                 // 1
// the selector (or inserted document) contains fail: true.                                                            // 2
                                                                                                                       // 3
var TRANSFORMS = {};                                                                                                   // 4
if (Meteor.isServer) {                                                                                                 // 5
  Meteor.methods({                                                                                                     // 6
    createInsecureCollection: function (name, options) {                                                               // 7
      check(name, String);                                                                                             // 8
      check(options, Match.Optional({                                                                                  // 9
        transformName: Match.Optional(String),                                                                         // 10
        idGeneration: Match.Optional(String)                                                                           // 11
      }));                                                                                                             // 12
                                                                                                                       // 13
      if (options && options.transformName) {                                                                          // 14
        options.transform = TRANSFORMS[options.transformName];                                                         // 15
      }                                                                                                                // 16
      var c = new Meteor.Collection(name, options);                                                                    // 17
      c._insecure = true;                                                                                              // 18
      Meteor.publish('c-' + name, function () {                                                                        // 19
        return c.find();                                                                                               // 20
      });                                                                                                              // 21
    }                                                                                                                  // 22
  });                                                                                                                  // 23
}                                                                                                                      // 24
                                                                                                                       // 25
var runInFence = function (f) {                                                                                        // 26
  if (Meteor.isClient) {                                                                                               // 27
    f();                                                                                                               // 28
  } else {                                                                                                             // 29
    var fence = new DDPServer._WriteFence;                                                                             // 30
    DDPServer._CurrentWriteFence.withValue(fence, f);                                                                  // 31
    fence.armAndWait();                                                                                                // 32
  }                                                                                                                    // 33
};                                                                                                                     // 34
                                                                                                                       // 35
// Helpers for upsert tests                                                                                            // 36
                                                                                                                       // 37
var stripId = function (obj) {                                                                                         // 38
  delete obj._id;                                                                                                      // 39
};                                                                                                                     // 40
                                                                                                                       // 41
var compareResults = function (test, skipIds, actual, expected) {                                                      // 42
  if (skipIds) {                                                                                                       // 43
    _.map(actual, stripId);                                                                                            // 44
    _.map(expected, stripId);                                                                                          // 45
  }                                                                                                                    // 46
  // (technically should ignore order in comparison)                                                                   // 47
  test.equal(actual, expected);                                                                                        // 48
};                                                                                                                     // 49
                                                                                                                       // 50
var upsert = function (coll, useUpdate, query, mod, options, callback) {                                               // 51
  if (! callback && typeof options === "function") {                                                                   // 52
    callback = options;                                                                                                // 53
    options = {};                                                                                                      // 54
  }                                                                                                                    // 55
                                                                                                                       // 56
  if (useUpdate) {                                                                                                     // 57
    if (callback)                                                                                                      // 58
      return coll.update(query, mod,                                                                                   // 59
                         _.extend({ upsert: true }, options),                                                          // 60
                         function (err, result) {                                                                      // 61
                           callback(err, ! err && {                                                                    // 62
                             numberAffected: result                                                                    // 63
                           });                                                                                         // 64
                         });                                                                                           // 65
    return {                                                                                                           // 66
      numberAffected: coll.update(query, mod,                                                                          // 67
                                  _.extend({ upsert: true }, options))                                                 // 68
    };                                                                                                                 // 69
  } else {                                                                                                             // 70
    return coll.upsert(query, mod, options, callback);                                                                 // 71
  }                                                                                                                    // 72
};                                                                                                                     // 73
                                                                                                                       // 74
var upsertTestMethod = "livedata_upsert_test_method";                                                                  // 75
var upsertTestMethodColl;                                                                                              // 76
                                                                                                                       // 77
// This is the implementation of the upsert test method on both the client and                                         // 78
// the server. On the client, we get a test object. On the server, we just throw                                       // 79
// errors if something doesn't go according to plan, and when the client                                               // 80
// receives those errors it will cause the test to fail.                                                               // 81
//                                                                                                                     // 82
// Client-side exceptions in here will NOT cause the test to fail! Because it's                                        // 83
// a stub, those exceptions will get caught and logged.                                                                // 84
var upsertTestMethodImpl = function (coll, useUpdate, test) {                                                          // 85
  coll.remove({});                                                                                                     // 86
  var result1 = upsert(coll, useUpdate, { foo: "bar" }, { foo: "bar" });                                               // 87
                                                                                                                       // 88
  if (! test) {                                                                                                        // 89
    test = {                                                                                                           // 90
      equal: function (a, b) {                                                                                         // 91
        if (! EJSON.equals(a, b))                                                                                      // 92
          throw new Error("Not equal: " +                                                                              // 93
                          JSON.stringify(a) + ", " + JSON.stringify(b));                                               // 94
      },                                                                                                               // 95
      isTrue: function (a) {                                                                                           // 96
        if (! a)                                                                                                       // 97
          throw new Error("Not truthy: " + JSON.stringify(a));                                                         // 98
      },                                                                                                               // 99
      isFalse: function (a) {                                                                                          // 100
        if (a)                                                                                                         // 101
          throw new Error("Not falsey: " + JSON.stringify(a));                                                         // 102
      }                                                                                                                // 103
    };                                                                                                                 // 104
  }                                                                                                                    // 105
                                                                                                                       // 106
  // if we don't test this, then testing result1.numberAffected will throw,                                            // 107
  // which will get caught and logged and the whole test will pass!                                                    // 108
  test.isTrue(result1);                                                                                                // 109
                                                                                                                       // 110
  test.equal(result1.numberAffected, 1);                                                                               // 111
  if (! useUpdate)                                                                                                     // 112
    test.isTrue(result1.insertedId);                                                                                   // 113
  var fooId = result1.insertedId;                                                                                      // 114
  var obj = coll.findOne({ foo: "bar" });                                                                              // 115
  test.isTrue(obj);                                                                                                    // 116
  if (! useUpdate)                                                                                                     // 117
    test.equal(obj._id, result1.insertedId);                                                                           // 118
  var result2 = upsert(coll, useUpdate, { _id: fooId },                                                                // 119
                       { $set: { foo: "baz " } });                                                                     // 120
  test.isTrue(result2);                                                                                                // 121
  test.equal(result2.numberAffected, 1);                                                                               // 122
  test.isFalse(result2.insertedId);                                                                                    // 123
};                                                                                                                     // 124
                                                                                                                       // 125
if (Meteor.isServer) {                                                                                                 // 126
  var m = {};                                                                                                          // 127
  m[upsertTestMethod] = function (run, useUpdate, options) {                                                           // 128
    check(run, String);                                                                                                // 129
    check(useUpdate, Boolean);                                                                                         // 130
    upsertTestMethodColl = new Meteor.Collection(upsertTestMethod + "_collection_" + run, options);                    // 131
    upsertTestMethodImpl(upsertTestMethodColl, useUpdate);                                                             // 132
  };                                                                                                                   // 133
  Meteor.methods(m);                                                                                                   // 134
}                                                                                                                      // 135
                                                                                                                       // 136
Meteor._FailureTestCollection =                                                                                        // 137
  new Meteor.Collection("___meteor_failure_test_collection");                                                          // 138
                                                                                                                       // 139
// For test "document with a custom type"                                                                              // 140
var Dog = function (name, color, actions) {                                                                            // 141
  var self = this;                                                                                                     // 142
  self.color = color;                                                                                                  // 143
  self.name = name;                                                                                                    // 144
  self.actions = actions || [{name: "wag"}, {name: "swim"}];                                                           // 145
};                                                                                                                     // 146
_.extend(Dog.prototype, {                                                                                              // 147
  getName: function () { return this.name;},                                                                           // 148
  getColor: function () { return this.name;},                                                                          // 149
  equals: function (other) { return other.name === this.name &&                                                        // 150
                             other.color === this.color &&                                                             // 151
                             EJSON.equals(other.actions, this.actions);},                                              // 152
  toJSONValue: function () { return {color: this.color, name: this.name, actions: this.actions};},                     // 153
  typeName: function () { return "dog"; },                                                                             // 154
  clone: function () { return new Dog(this.name, this.color); },                                                       // 155
  speak: function () { return "woof"; }                                                                                // 156
});                                                                                                                    // 157
EJSON.addType("dog", function (o) { return new Dog(o.name, o.color, o.actions);});                                     // 158
                                                                                                                       // 159
                                                                                                                       // 160
// Parameterize tests.                                                                                                 // 161
_.each( ['STRING', 'MONGO'], function(idGeneration) {                                                                  // 162
                                                                                                                       // 163
var collectionOptions = { idGeneration: idGeneration};                                                                 // 164
                                                                                                                       // 165
testAsyncMulti("mongo-livedata - database error reporting. " + idGeneration, [                                         // 166
  function (test, expect) {                                                                                            // 167
    var ftc = Meteor._FailureTestCollection;                                                                           // 168
                                                                                                                       // 169
    var exception = function (err, res) {                                                                              // 170
      test.instanceOf(err, Error);                                                                                     // 171
    };                                                                                                                 // 172
                                                                                                                       // 173
    _.each(["insert", "remove", "update"], function (op) {                                                             // 174
      var arg = (op === "insert" ? {} : 'bla');                                                                        // 175
      var arg2 = {};                                                                                                   // 176
                                                                                                                       // 177
      var callOp = function (callback) {                                                                               // 178
        if (op === "update") {                                                                                         // 179
          ftc[op](arg, arg2, callback);                                                                                // 180
        } else {                                                                                                       // 181
          ftc[op](arg, callback);                                                                                      // 182
        }                                                                                                              // 183
      };                                                                                                               // 184
                                                                                                                       // 185
      if (Meteor.isServer) {                                                                                           // 186
        test.throws(function () {                                                                                      // 187
          callOp();                                                                                                    // 188
        });                                                                                                            // 189
                                                                                                                       // 190
        callOp(expect(exception));                                                                                     // 191
      }                                                                                                                // 192
                                                                                                                       // 193
      if (Meteor.isClient) {                                                                                           // 194
        callOp(expect(exception));                                                                                     // 195
                                                                                                                       // 196
        // This would log to console in normal operation.                                                              // 197
        Meteor._suppress_log(1);                                                                                       // 198
        callOp();                                                                                                      // 199
      }                                                                                                                // 200
    });                                                                                                                // 201
  }                                                                                                                    // 202
]);                                                                                                                    // 203
                                                                                                                       // 204
                                                                                                                       // 205
Tinytest.addAsync("mongo-livedata - basics, " + idGeneration, function (test, onComplete) {                            // 206
  var run = test.runId();                                                                                              // 207
  var coll, coll2;                                                                                                     // 208
  if (Meteor.isClient) {                                                                                               // 209
    coll = new Meteor.Collection(null, collectionOptions) ; // local, unmanaged                                        // 210
    coll2 = new Meteor.Collection(null, collectionOptions); // local, unmanaged                                        // 211
  } else {                                                                                                             // 212
    coll = new Meteor.Collection("livedata_test_collection_"+run, collectionOptions);                                  // 213
    coll2 = new Meteor.Collection("livedata_test_collection_2_"+run, collectionOptions);                               // 214
  }                                                                                                                    // 215
                                                                                                                       // 216
  var log = '';                                                                                                        // 217
  var obs = coll.find({run: run}, {sort: ["x"]}).observe({                                                             // 218
    addedAt: function (doc, before_index, before) {                                                                    // 219
      log += 'a(' + doc.x + ',' + before_index + ',' + before + ')';                                                   // 220
    },                                                                                                                 // 221
    changedAt: function (new_doc, old_doc, at_index) {                                                                 // 222
      log += 'c(' + new_doc.x + ',' + at_index + ',' + old_doc.x + ')';                                                // 223
    },                                                                                                                 // 224
    movedTo: function (doc, old_index, new_index) {                                                                    // 225
      log += 'm(' + doc.x + ',' + old_index + ',' + new_index + ')';                                                   // 226
    },                                                                                                                 // 227
    removedAt: function (doc, at_index) {                                                                              // 228
      log += 'r(' + doc.x + ',' + at_index + ')';                                                                      // 229
    }                                                                                                                  // 230
  });                                                                                                                  // 231
                                                                                                                       // 232
  var captureObserve = function (f) {                                                                                  // 233
    if (Meteor.isClient) {                                                                                             // 234
      f();                                                                                                             // 235
    } else {                                                                                                           // 236
      var fence = new DDPServer._WriteFence;                                                                           // 237
      DDPServer._CurrentWriteFence.withValue(fence, f);                                                                // 238
      fence.armAndWait();                                                                                              // 239
    }                                                                                                                  // 240
                                                                                                                       // 241
    var ret = log;                                                                                                     // 242
    log = '';                                                                                                          // 243
    return ret;                                                                                                        // 244
  };                                                                                                                   // 245
                                                                                                                       // 246
  var expectObserve = function (expected, f) {                                                                         // 247
    if (!(expected instanceof Array))                                                                                  // 248
      expected = [expected];                                                                                           // 249
                                                                                                                       // 250
    test.include(expected, captureObserve(f));                                                                         // 251
  };                                                                                                                   // 252
                                                                                                                       // 253
  test.equal(coll.find({run: run}).count(), 0);                                                                        // 254
  test.equal(coll.findOne("abc"), undefined);                                                                          // 255
  test.equal(coll.findOne({run: run}), undefined);                                                                     // 256
                                                                                                                       // 257
  expectObserve('a(1,0,null)', function () {                                                                           // 258
    var id = coll.insert({run: run, x: 1});                                                                            // 259
    test.equal(coll.find({run: run}).count(), 1);                                                                      // 260
    test.equal(coll.findOne(id).x, 1);                                                                                 // 261
    test.equal(coll.findOne({run: run}).x, 1);                                                                         // 262
  });                                                                                                                  // 263
                                                                                                                       // 264
  expectObserve('a(4,1,null)', function () {                                                                           // 265
    var id2 = coll.insert({run: run, x: 4});                                                                           // 266
    test.equal(coll.find({run: run}).count(), 2);                                                                      // 267
    test.equal(coll.find({_id: id2}).count(), 1);                                                                      // 268
    test.equal(coll.findOne(id2).x, 4);                                                                                // 269
  });                                                                                                                  // 270
                                                                                                                       // 271
  test.equal(coll.findOne({run: run}, {sort: ["x"], skip: 0}).x, 1);                                                   // 272
  test.equal(coll.findOne({run: run}, {sort: ["x"], skip: 1}).x, 4);                                                   // 273
  test.equal(coll.findOne({run: run}, {sort: {x: -1}, skip: 0}).x, 4);                                                 // 274
  test.equal(coll.findOne({run: run}, {sort: {x: -1}, skip: 1}).x, 1);                                                 // 275
                                                                                                                       // 276
                                                                                                                       // 277
  var cur = coll.find({run: run}, {sort: ["x"]});                                                                      // 278
  var total = 0;                                                                                                       // 279
  var index = 0;                                                                                                       // 280
  var context = {};                                                                                                    // 281
  cur.forEach(function (doc, i, cursor) {                                                                              // 282
    test.equal(i, index++);                                                                                            // 283
    test.isTrue(cursor === cur);                                                                                       // 284
    test.isTrue(context === this);                                                                                     // 285
    total *= 10;                                                                                                       // 286
    if (Meteor.isServer) {                                                                                             // 287
      // Verify that the callbacks from forEach run sequentially and that                                              // 288
      // forEach waits for them to complete (issue# 321). If they do not run                                           // 289
      // sequentially, then the second callback could execute during the first                                         // 290
      // callback's sleep sleep and the *= 10 will occur before the += 1, then                                         // 291
      // total (at test.equal time) will be 5. If forEach does not wait for the                                        // 292
      // callbacks to complete, then total (at test.equal time) will be 0.                                             // 293
      Meteor._sleepForMs(5);                                                                                           // 294
    }                                                                                                                  // 295
    total += doc.x;                                                                                                    // 296
    // verify the meteor environment is set up here                                                                    // 297
    coll2.insert({total:total});                                                                                       // 298
  }, context);                                                                                                         // 299
  test.equal(total, 14);                                                                                               // 300
                                                                                                                       // 301
  cur.rewind();                                                                                                        // 302
  index = 0;                                                                                                           // 303
  test.equal(cur.map(function (doc, i, cursor) {                                                                       // 304
    // XXX we could theoretically make map run its iterations in parallel or                                           // 305
    // something which would make this fail                                                                            // 306
    test.equal(i, index++);                                                                                            // 307
    test.isTrue(cursor === cur);                                                                                       // 308
    test.isTrue(context === this);                                                                                     // 309
    return doc.x * 2;                                                                                                  // 310
  }, context), [2, 8]);                                                                                                // 311
                                                                                                                       // 312
  test.equal(_.pluck(coll.find({run: run}, {sort: {x: -1}}).fetch(), "x"),                                             // 313
             [4, 1]);                                                                                                  // 314
                                                                                                                       // 315
  expectObserve('', function () {                                                                                      // 316
    var count = coll.update({run: run, x: -1}, {$inc: {x: 2}}, {multi: true});                                         // 317
    test.equal(count, 0);                                                                                              // 318
  });                                                                                                                  // 319
                                                                                                                       // 320
  expectObserve('c(3,0,1)c(6,1,4)', function () {                                                                      // 321
    var count = coll.update({run: run}, {$inc: {x: 2}}, {multi: true});                                                // 322
    test.equal(count, 2);                                                                                              // 323
    test.equal(_.pluck(coll.find({run: run}, {sort: {x: -1}}).fetch(), "x"),                                           // 324
               [6, 3]);                                                                                                // 325
  });                                                                                                                  // 326
                                                                                                                       // 327
  expectObserve(['c(13,0,3)m(13,0,1)', 'm(6,1,0)c(13,1,3)',                                                            // 328
                 'c(13,0,3)m(6,1,0)', 'm(3,0,1)c(13,1,3)'], function () {                                              // 329
    coll.update({run: run, x: 3}, {$inc: {x: 10}}, {multi: true});                                                     // 330
    test.equal(_.pluck(coll.find({run: run}, {sort: {x: -1}}).fetch(), "x"),                                           // 331
               [13, 6]);                                                                                               // 332
  });                                                                                                                  // 333
                                                                                                                       // 334
  expectObserve('r(13,1)', function () {                                                                               // 335
    var count = coll.remove({run: run, x: {$gt: 10}});                                                                 // 336
    test.equal(count, 1);                                                                                              // 337
    test.equal(coll.find({run: run}).count(), 1);                                                                      // 338
  });                                                                                                                  // 339
                                                                                                                       // 340
  expectObserve('r(6,0)', function () {                                                                                // 341
    coll.remove({run: run});                                                                                           // 342
    test.equal(coll.find({run: run}).count(), 0);                                                                      // 343
  });                                                                                                                  // 344
                                                                                                                       // 345
  expectObserve('', function () {                                                                                      // 346
    var count = coll.remove({run: run});                                                                               // 347
    test.equal(count, 0);                                                                                              // 348
    test.equal(coll.find({run: run}).count(), 0);                                                                      // 349
  });                                                                                                                  // 350
                                                                                                                       // 351
  obs.stop();                                                                                                          // 352
  onComplete();                                                                                                        // 353
});                                                                                                                    // 354
                                                                                                                       // 355
Tinytest.addAsync("mongo-livedata - fuzz test, " + idGeneration, function(test, onComplete) {                          // 356
                                                                                                                       // 357
  var run = Random.id();                                                                                               // 358
  var coll;                                                                                                            // 359
  if (Meteor.isClient) {                                                                                               // 360
    coll = new Meteor.Collection(null, collectionOptions); // local, unmanaged                                         // 361
  } else {                                                                                                             // 362
    coll = new Meteor.Collection("livedata_test_collection_"+run, collectionOptions);                                  // 363
  }                                                                                                                    // 364
                                                                                                                       // 365
  // fuzz test of observe(), especially the server-side diffing                                                        // 366
  var actual = [];                                                                                                     // 367
  var correct = [];                                                                                                    // 368
  var counters = {add: 0, change: 0, move: 0, remove: 0};                                                              // 369
                                                                                                                       // 370
  var obs = coll.find({run: run}, {sort: ["x"]}).observe({                                                             // 371
    addedAt: function (doc, before_index) {                                                                            // 372
      counters.add++;                                                                                                  // 373
      actual.splice(before_index, 0, doc.x);                                                                           // 374
    },                                                                                                                 // 375
    changedAt: function (new_doc, old_doc, at_index) {                                                                 // 376
      counters.change++;                                                                                               // 377
      test.equal(actual[at_index], old_doc.x);                                                                         // 378
      actual[at_index] = new_doc.x;                                                                                    // 379
    },                                                                                                                 // 380
    movedTo: function (doc, old_index, new_index) {                                                                    // 381
      counters.move++;                                                                                                 // 382
      test.equal(actual[old_index], doc.x);                                                                            // 383
      actual.splice(old_index, 1);                                                                                     // 384
      actual.splice(new_index, 0, doc.x);                                                                              // 385
    },                                                                                                                 // 386
    removedAt: function (doc, at_index) {                                                                              // 387
      counters.remove++;                                                                                               // 388
      test.equal(actual[at_index], doc.x);                                                                             // 389
      actual.splice(at_index, 1);                                                                                      // 390
    }                                                                                                                  // 391
  });                                                                                                                  // 392
                                                                                                                       // 393
  if (Meteor.isServer) {                                                                                               // 394
    // For now, has to be polling (not oplog) because it is ordered observe.                                           // 395
    test.isTrue(obs._multiplexer._observeDriver._suspendPolling);                                                      // 396
  }                                                                                                                    // 397
                                                                                                                       // 398
  var step = 0;                                                                                                        // 399
                                                                                                                       // 400
  // Use non-deterministic randomness so we can have a shorter fuzz                                                    // 401
  // test (fewer iterations).  For deterministic (fully seeded)                                                        // 402
  // randomness, remove the call to Random.fraction().                                                                 // 403
  var seededRandom = new SeededRandom("foobard" + Random.fraction());                                                  // 404
  // Random integer in [0,n)                                                                                           // 405
  var rnd = function (n) {                                                                                             // 406
    return seededRandom.nextIntBetween(0, n-1);                                                                        // 407
  };                                                                                                                   // 408
                                                                                                                       // 409
  var finishObserve = function (f) {                                                                                   // 410
    if (Meteor.isClient) {                                                                                             // 411
      f();                                                                                                             // 412
    } else {                                                                                                           // 413
      var fence = new DDPServer._WriteFence;                                                                           // 414
      DDPServer._CurrentWriteFence.withValue(fence, f);                                                                // 415
      fence.armAndWait();                                                                                              // 416
    }                                                                                                                  // 417
  };                                                                                                                   // 418
                                                                                                                       // 419
  var doStep = function () {                                                                                           // 420
    if (step++ === 5) { // run N random tests                                                                          // 421
      obs.stop();                                                                                                      // 422
      onComplete();                                                                                                    // 423
      return;                                                                                                          // 424
    }                                                                                                                  // 425
                                                                                                                       // 426
    var max_counters = _.clone(counters);                                                                              // 427
                                                                                                                       // 428
    finishObserve(function () {                                                                                        // 429
      if (Meteor.isServer)                                                                                             // 430
        obs._multiplexer._observeDriver._suspendPolling();                                                             // 431
                                                                                                                       // 432
      // Do a batch of 1-10 operations                                                                                 // 433
      var batch_count = rnd(10) + 1;                                                                                   // 434
      for (var i = 0; i < batch_count; i++) {                                                                          // 435
        // 25% add, 25% remove, 25% change in place, 25% change and move                                               // 436
        var op = rnd(4);                                                                                               // 437
        var which = rnd(correct.length);                                                                               // 438
        if (op === 0 || step < 2 || !correct.length) {                                                                 // 439
          // Add                                                                                                       // 440
          var x = rnd(1000000);                                                                                        // 441
          coll.insert({run: run, x: x});                                                                               // 442
          correct.push(x);                                                                                             // 443
          max_counters.add++;                                                                                          // 444
        } else if (op === 1 || op === 2) {                                                                             // 445
          var x = correct[which];                                                                                      // 446
          if (op === 1)                                                                                                // 447
            // Small change, not likely to cause a move                                                                // 448
            var val = x + (rnd(2) ? -1 : 1);                                                                           // 449
          else                                                                                                         // 450
            // Large change, likely to cause a move                                                                    // 451
            var val = rnd(1000000);                                                                                    // 452
          coll.update({run: run, x: x}, {$set: {x: val}});                                                             // 453
          correct[which] = val;                                                                                        // 454
          max_counters.change++;                                                                                       // 455
          max_counters.move++;                                                                                         // 456
        } else {                                                                                                       // 457
          coll.remove({run: run, x: correct[which]});                                                                  // 458
          correct.splice(which, 1);                                                                                    // 459
          max_counters.remove++;                                                                                       // 460
        }                                                                                                              // 461
      }                                                                                                                // 462
      if (Meteor.isServer)                                                                                             // 463
        obs._multiplexer._observeDriver._resumePolling();                                                              // 464
                                                                                                                       // 465
    });                                                                                                                // 466
                                                                                                                       // 467
    // Did we actually deliver messages that mutated the array in the                                                  // 468
    // right way?                                                                                                      // 469
    correct.sort(function (a,b) {return a-b;});                                                                        // 470
    test.equal(actual, correct);                                                                                       // 471
                                                                                                                       // 472
    // Did we limit ourselves to one 'moved' message per change,                                                       // 473
    // rather than O(results) moved messages?                                                                          // 474
    _.each(max_counters, function (v, k) {                                                                             // 475
      test.isTrue(max_counters[k] >= counters[k], k);                                                                  // 476
    });                                                                                                                // 477
                                                                                                                       // 478
    Meteor.defer(doStep);                                                                                              // 479
  };                                                                                                                   // 480
                                                                                                                       // 481
  doStep();                                                                                                            // 482
                                                                                                                       // 483
});                                                                                                                    // 484
                                                                                                                       // 485
Tinytest.addAsync("mongo-livedata - scribbling, " + idGeneration, function (test, onComplete) {                        // 486
  var run = test.runId();                                                                                              // 487
  var coll;                                                                                                            // 488
  if (Meteor.isClient) {                                                                                               // 489
    coll = new Meteor.Collection(null, collectionOptions); // local, unmanaged                                         // 490
  } else {                                                                                                             // 491
    coll = new Meteor.Collection("livedata_test_collection_"+run, collectionOptions);                                  // 492
  }                                                                                                                    // 493
                                                                                                                       // 494
  var numAddeds = 0;                                                                                                   // 495
  var handle = coll.find({run: run}).observe({                                                                         // 496
    addedAt: function (o) {                                                                                            // 497
      // test that we can scribble on the object we get back from Mongo without                                        // 498
      // breaking anything.  The worst possible scribble is messing with _id.                                          // 499
      delete o._id;                                                                                                    // 500
      numAddeds++;                                                                                                     // 501
    }                                                                                                                  // 502
  });                                                                                                                  // 503
  _.each([123, 456, 789], function (abc) {                                                                             // 504
    runInFence(function () {                                                                                           // 505
      coll.insert({run: run, abc: abc});                                                                               // 506
    });                                                                                                                // 507
  });                                                                                                                  // 508
  handle.stop();                                                                                                       // 509
  // will be 6 (1+2+3) if we broke diffing!                                                                            // 510
  test.equal(numAddeds, 3);                                                                                            // 511
                                                                                                                       // 512
  onComplete();                                                                                                        // 513
});                                                                                                                    // 514
                                                                                                                       // 515
Tinytest.addAsync("mongo-livedata - stop handle in callback, " + idGeneration, function (test, onComplete) {           // 516
  var run = Random.id();                                                                                               // 517
  var coll;                                                                                                            // 518
  if (Meteor.isClient) {                                                                                               // 519
    coll = new Meteor.Collection(null, collectionOptions); // local, unmanaged                                         // 520
  } else {                                                                                                             // 521
    coll = new Meteor.Collection("stopHandleInCallback-"+run, collectionOptions);                                      // 522
  }                                                                                                                    // 523
                                                                                                                       // 524
  var output = [];                                                                                                     // 525
                                                                                                                       // 526
  var handle = coll.find().observe({                                                                                   // 527
    added: function (doc) {                                                                                            // 528
      output.push({added: doc._id});                                                                                   // 529
    },                                                                                                                 // 530
    changed: function (newDoc) {                                                                                       // 531
      output.push('changed');                                                                                          // 532
      handle.stop();                                                                                                   // 533
    }                                                                                                                  // 534
  });                                                                                                                  // 535
                                                                                                                       // 536
  test.equal(output, []);                                                                                              // 537
                                                                                                                       // 538
  // Insert a document. Observe that the added callback is called.                                                     // 539
  var docId;                                                                                                           // 540
  runInFence(function () {                                                                                             // 541
    docId = coll.insert({foo: 42});                                                                                    // 542
  });                                                                                                                  // 543
  test.length(output, 1);                                                                                              // 544
  test.equal(output.shift(), {added: docId});                                                                          // 545
                                                                                                                       // 546
  // Update it. Observe that the changed callback is called. This should also                                          // 547
  // stop the observation.                                                                                             // 548
  runInFence(function() {                                                                                              // 549
    coll.update(docId, {$set: {bar: 10}});                                                                             // 550
  });                                                                                                                  // 551
  test.length(output, 1);                                                                                              // 552
  test.equal(output.shift(), 'changed');                                                                               // 553
                                                                                                                       // 554
  // Update again. This shouldn't call the callback because we stopped the                                             // 555
  // observation.                                                                                                      // 556
  runInFence(function() {                                                                                              // 557
    coll.update(docId, {$set: {baz: 40}});                                                                             // 558
  });                                                                                                                  // 559
  test.length(output, 0);                                                                                              // 560
                                                                                                                       // 561
  test.equal(coll.find().count(), 1);                                                                                  // 562
  test.equal(coll.findOne(docId),                                                                                      // 563
             {_id: docId, foo: 42, bar: 10, baz: 40});                                                                 // 564
                                                                                                                       // 565
  onComplete();                                                                                                        // 566
});                                                                                                                    // 567
                                                                                                                       // 568
// This behavior isn't great, but it beats deadlock.                                                                   // 569
if (Meteor.isServer) {                                                                                                 // 570
  Tinytest.addAsync("mongo-livedata - recursive observe throws, " + idGeneration, function (test, onComplete) {        // 571
    var run = test.runId();                                                                                            // 572
    var coll = new Meteor.Collection("observeInCallback-"+run, collectionOptions);                                     // 573
                                                                                                                       // 574
    var callbackCalled = false;                                                                                        // 575
    var handle = coll.find({}).observe({                                                                               // 576
      added: function (newDoc) {                                                                                       // 577
        callbackCalled = true;                                                                                         // 578
        test.throws(function () {                                                                                      // 579
          coll.find({}).observe();                                                                                     // 580
        });                                                                                                            // 581
      }                                                                                                                // 582
    });                                                                                                                // 583
    test.isFalse(callbackCalled);                                                                                      // 584
    // Insert a document. Observe that the added callback is called.                                                   // 585
    runInFence(function () {                                                                                           // 586
      coll.insert({foo: 42});                                                                                          // 587
    });                                                                                                                // 588
    test.isTrue(callbackCalled);                                                                                       // 589
                                                                                                                       // 590
    handle.stop();                                                                                                     // 591
                                                                                                                       // 592
    onComplete();                                                                                                      // 593
  });                                                                                                                  // 594
                                                                                                                       // 595
  Tinytest.addAsync("mongo-livedata - cursor dedup, " + idGeneration, function (test, onComplete) {                    // 596
    var run = test.runId();                                                                                            // 597
    var coll = new Meteor.Collection("cursorDedup-"+run, collectionOptions);                                           // 598
                                                                                                                       // 599
    var observer = function (noAdded) {                                                                                // 600
      var output = [];                                                                                                 // 601
      var callbacks = {                                                                                                // 602
        changed: function (newDoc) {                                                                                   // 603
          output.push({changed: newDoc._id});                                                                          // 604
        }                                                                                                              // 605
      };                                                                                                               // 606
      if (!noAdded) {                                                                                                  // 607
        callbacks.added = function (doc) {                                                                             // 608
          output.push({added: doc._id});                                                                               // 609
        };                                                                                                             // 610
      }                                                                                                                // 611
      var handle = coll.find({foo: 22}).observe(callbacks);                                                            // 612
      return {output: output, handle: handle};                                                                         // 613
    };                                                                                                                 // 614
                                                                                                                       // 615
    // Insert a doc and start observing.                                                                               // 616
    var docId1 = coll.insert({foo: 22});                                                                               // 617
    var o1 = observer();                                                                                               // 618
    // Initial add.                                                                                                    // 619
    test.length(o1.output, 1);                                                                                         // 620
    test.equal(o1.output.shift(), {added: docId1});                                                                    // 621
                                                                                                                       // 622
    // Insert another doc (blocking until observes have fired).                                                        // 623
    var docId2;                                                                                                        // 624
    runInFence(function () {                                                                                           // 625
      docId2 = coll.insert({foo: 22, bar: 5});                                                                         // 626
    });                                                                                                                // 627
    // Observed add.                                                                                                   // 628
    test.length(o1.output, 1);                                                                                         // 629
    test.equal(o1.output.shift(), {added: docId2});                                                                    // 630
                                                                                                                       // 631
    // Second identical observe.                                                                                       // 632
    var o2 = observer();                                                                                               // 633
    // Initial adds.                                                                                                   // 634
    test.length(o2.output, 2);                                                                                         // 635
    test.include([docId1, docId2], o2.output[0].added);                                                                // 636
    test.include([docId1, docId2], o2.output[1].added);                                                                // 637
    test.notEqual(o2.output[0].added, o2.output[1].added);                                                             // 638
    o2.output.length = 0;                                                                                              // 639
    // Original observe not affected.                                                                                  // 640
    test.length(o1.output, 0);                                                                                         // 641
                                                                                                                       // 642
    // White-box test: both observes should share an ObserveMultiplexer.                                               // 643
    var observeMultiplexer = o1.handle._multiplexer;                                                                   // 644
    test.isTrue(observeMultiplexer);                                                                                   // 645
    test.isTrue(observeMultiplexer === o2.handle._multiplexer);                                                        // 646
                                                                                                                       // 647
    // Update. Both observes fire.                                                                                     // 648
    runInFence(function () {                                                                                           // 649
      coll.update(docId1, {$set: {x: 'y'}});                                                                           // 650
    });                                                                                                                // 651
    test.length(o1.output, 1);                                                                                         // 652
    test.length(o2.output, 1);                                                                                         // 653
    test.equal(o1.output.shift(), {changed: docId1});                                                                  // 654
    test.equal(o2.output.shift(), {changed: docId1});                                                                  // 655
                                                                                                                       // 656
    // Stop first handle. Second handle still around.                                                                  // 657
    o1.handle.stop();                                                                                                  // 658
    test.length(o1.output, 0);                                                                                         // 659
    test.length(o2.output, 0);                                                                                         // 660
                                                                                                                       // 661
    // Another update. Just the second handle should fire.                                                             // 662
    runInFence(function () {                                                                                           // 663
      coll.update(docId2, {$set: {z: 'y'}});                                                                           // 664
    });                                                                                                                // 665
    test.length(o1.output, 0);                                                                                         // 666
    test.length(o2.output, 1);                                                                                         // 667
    test.equal(o2.output.shift(), {changed: docId2});                                                                  // 668
                                                                                                                       // 669
    // Stop second handle. Nothing should happen, but the multiplexer should                                           // 670
    // be stopped.                                                                                                     // 671
    test.isTrue(observeMultiplexer._handles);  // This will change.                                                    // 672
    o2.handle.stop();                                                                                                  // 673
    test.length(o1.output, 0);                                                                                         // 674
    test.length(o2.output, 0);                                                                                         // 675
    // White-box: ObserveMultiplexer has nulled its _handles so you can't                                              // 676
    // accidentally join to it.                                                                                        // 677
    test.isNull(observeMultiplexer._handles);                                                                          // 678
                                                                                                                       // 679
    // Start yet another handle on the same query.                                                                     // 680
    var o3 = observer();                                                                                               // 681
    // Initial adds.                                                                                                   // 682
    test.length(o3.output, 2);                                                                                         // 683
    test.include([docId1, docId2], o3.output[0].added);                                                                // 684
    test.include([docId1, docId2], o3.output[1].added);                                                                // 685
    test.notEqual(o3.output[0].added, o3.output[1].added);                                                             // 686
    // Old observers not called.                                                                                       // 687
    test.length(o1.output, 0);                                                                                         // 688
    test.length(o2.output, 0);                                                                                         // 689
    // White-box: Different ObserveMultiplexer.                                                                        // 690
    test.isTrue(observeMultiplexer !== o3.handle._multiplexer);                                                        // 691
                                                                                                                       // 692
    // Start another handle with no added callback. Regression test for #589.                                          // 693
    var o4 = observer(true);                                                                                           // 694
                                                                                                                       // 695
    o3.handle.stop();                                                                                                  // 696
    o4.handle.stop();                                                                                                  // 697
                                                                                                                       // 698
    onComplete();                                                                                                      // 699
  });                                                                                                                  // 700
                                                                                                                       // 701
  Tinytest.addAsync("mongo-livedata - async server-side insert, " + idGeneration, function (test, onComplete) {        // 702
    // Tests that insert returns before the callback runs. Relies on the fact                                          // 703
    // that mongo does not run the callback before spinning off the event loop.                                        // 704
    var cname = Random.id();                                                                                           // 705
    var coll = new Meteor.Collection(cname);                                                                           // 706
    var doc = { foo: "bar" };                                                                                          // 707
    var x = 0;                                                                                                         // 708
    coll.insert(doc, function (err, result) {                                                                          // 709
      test.equal(err, null);                                                                                           // 710
      test.equal(x, 1);                                                                                                // 711
      onComplete();                                                                                                    // 712
    });                                                                                                                // 713
    x++;                                                                                                               // 714
  });                                                                                                                  // 715
                                                                                                                       // 716
  Tinytest.addAsync("mongo-livedata - async server-side update, " + idGeneration, function (test, onComplete) {        // 717
    // Tests that update returns before the callback runs.                                                             // 718
    var cname = Random.id();                                                                                           // 719
    var coll = new Meteor.Collection(cname);                                                                           // 720
    var doc = { foo: "bar" };                                                                                          // 721
    var x = 0;                                                                                                         // 722
    var id = coll.insert(doc);                                                                                         // 723
    coll.update(id, { $set: { foo: "baz" } }, function (err, result) {                                                 // 724
      test.equal(err, null);                                                                                           // 725
      test.equal(result, 1);                                                                                           // 726
      test.equal(x, 1);                                                                                                // 727
      onComplete();                                                                                                    // 728
    });                                                                                                                // 729
    x++;                                                                                                               // 730
  });                                                                                                                  // 731
                                                                                                                       // 732
  Tinytest.addAsync("mongo-livedata - async server-side remove, " + idGeneration, function (test, onComplete) {        // 733
    // Tests that remove returns before the callback runs.                                                             // 734
    var cname = Random.id();                                                                                           // 735
    var coll = new Meteor.Collection(cname);                                                                           // 736
    var doc = { foo: "bar" };                                                                                          // 737
    var x = 0;                                                                                                         // 738
    var id = coll.insert(doc);                                                                                         // 739
    coll.remove(id, function (err, result) {                                                                           // 740
      test.equal(err, null);                                                                                           // 741
      test.isFalse(coll.findOne(id));                                                                                  // 742
      test.equal(x, 1);                                                                                                // 743
      onComplete();                                                                                                    // 744
    });                                                                                                                // 745
    x++;                                                                                                               // 746
  });                                                                                                                  // 747
                                                                                                                       // 748
  // compares arrays a and b w/o looking at order                                                                      // 749
  var setsEqual = function (a, b) {                                                                                    // 750
    a = _.map(a, EJSON.stringify);                                                                                     // 751
    b = _.map(b, EJSON.stringify);                                                                                     // 752
    return _.isEmpty(_.difference(a, b)) && _.isEmpty(_.difference(b, a));                                             // 753
  };                                                                                                                   // 754
                                                                                                                       // 755
  // This test mainly checks the correctness of oplog code dealing with limited                                        // 756
  // queries. Compitablity with poll-diff is added as well.                                                            // 757
  Tinytest.addAsync("mongo-livedata - observe sorted, limited " + idGeneration, function (test, onComplete) {          // 758
    var run = test.runId();                                                                                            // 759
    var coll = new Meteor.Collection("observeLimit-"+run, collectionOptions);                                          // 760
                                                                                                                       // 761
    var observer = function () {                                                                                       // 762
      var state = {};                                                                                                  // 763
      var output = [];                                                                                                 // 764
      var callbacks = {                                                                                                // 765
        changed: function (newDoc) {                                                                                   // 766
          output.push({changed: newDoc._id});                                                                          // 767
          state[newDoc._id] = newDoc;                                                                                  // 768
        },                                                                                                             // 769
        added: function (newDoc) {                                                                                     // 770
          output.push({added: newDoc._id});                                                                            // 771
          state[newDoc._id] = newDoc;                                                                                  // 772
        },                                                                                                             // 773
        removed: function (oldDoc) {                                                                                   // 774
          output.push({removed: oldDoc._id});                                                                          // 775
          delete state[oldDoc._id];                                                                                    // 776
        }                                                                                                              // 777
      };                                                                                                               // 778
      var handle = coll.find({foo: 22},                                                                                // 779
                             {sort: {bar: 1}, limit: 3}).observe(callbacks);                                           // 780
                                                                                                                       // 781
      return {output: output, handle: handle, state: state};                                                           // 782
    };                                                                                                                 // 783
    var clearOutput = function (o) { o.output.splice(0, o.output.length); };                                           // 784
                                                                                                                       // 785
    var ins = function (doc) {                                                                                         // 786
      var id; runInFence(function () { id = coll.insert(doc); });                                                      // 787
      return id;                                                                                                       // 788
    };                                                                                                                 // 789
    var rem = function (sel) { runInFence(function () { coll.remove(sel); }); };                                       // 790
    var upd = function (sel, mod, opt) {                                                                               // 791
      runInFence(function () {                                                                                         // 792
        coll.update(sel, mod, opt);                                                                                    // 793
      });                                                                                                              // 794
    };                                                                                                                 // 795
    // tests '_id' subfields for all documents in oplog buffer                                                         // 796
    var testOplogBufferIds = function (ids) {                                                                          // 797
      if (!usesOplog)                                                                                                  // 798
        return;                                                                                                        // 799
      var bufferIds = [];                                                                                              // 800
      o.handle._multiplexer._observeDriver._unpublishedBuffer.forEach(function (x, id) {                               // 801
        bufferIds.push(id);                                                                                            // 802
      });                                                                                                              // 803
                                                                                                                       // 804
      test.isTrue(setsEqual(ids, bufferIds), "expected: " + ids + "; got: " + bufferIds);                              // 805
    };                                                                                                                 // 806
    var testSafeAppendToBufferFlag = function (expected) {                                                             // 807
      if (!usesOplog)                                                                                                  // 808
        return;                                                                                                        // 809
      test.equal(o.handle._multiplexer._observeDriver._safeAppendToBuffer,                                             // 810
                 expected);                                                                                            // 811
    };                                                                                                                 // 812
                                                                                                                       // 813
    // We'll describe our state as follows.  5:1 means "the document with                                              // 814
    // _id=docId1 and bar=5".  We list documents as                                                                    // 815
    //   [ currently published | in the buffer ] outside the buffer                                                    // 816
    // If safeToAppendToBuffer is true, we'll say ]! instead.                                                          // 817
                                                                                                                       // 818
    // Insert a doc and start observing.                                                                               // 819
    var docId1 = ins({foo: 22, bar: 5});                                                                               // 820
    waitUntilOplogCaughtUp();                                                                                          // 821
                                                                                                                       // 822
    // State: [ 5:1 | ]!                                                                                               // 823
    var o = observer();                                                                                                // 824
    var usesOplog = o.handle._multiplexer._observeDriver._usesOplog;                                                   // 825
    // Initial add.                                                                                                    // 826
    test.length(o.output, 1);                                                                                          // 827
    test.equal(o.output.shift(), {added: docId1});                                                                     // 828
    testSafeAppendToBufferFlag(true);                                                                                  // 829
                                                                                                                       // 830
    // Insert another doc (blocking until observes have fired).                                                        // 831
    // State: [ 5:1 6:2 | ]!                                                                                           // 832
    var docId2 = ins({foo: 22, bar: 6});                                                                               // 833
    // Observed add.                                                                                                   // 834
    test.length(o.output, 1);                                                                                          // 835
    test.equal(o.output.shift(), {added: docId2});                                                                     // 836
    testSafeAppendToBufferFlag(true);                                                                                  // 837
                                                                                                                       // 838
    var docId3 = ins({ foo: 22, bar: 3 });                                                                             // 839
    // State: [ 3:3 5:1 6:2 | ]!                                                                                       // 840
    test.length(o.output, 1);                                                                                          // 841
    test.equal(o.output.shift(), {added: docId3});                                                                     // 842
    testSafeAppendToBufferFlag(true);                                                                                  // 843
                                                                                                                       // 844
    // Add a non-matching document                                                                                     // 845
    ins({ foo: 13 });                                                                                                  // 846
    // It shouldn't be added                                                                                           // 847
    test.length(o.output, 0);                                                                                          // 848
                                                                                                                       // 849
    // Add something that matches but is too big to fit in                                                             // 850
    var docId4 = ins({ foo: 22, bar: 7 });                                                                             // 851
    // State: [ 3:3 5:1 6:2 | 7:4 ]!                                                                                   // 852
    // It shouldn't be added but should end up in the buffer.                                                          // 853
    test.length(o.output, 0);                                                                                          // 854
    testOplogBufferIds([docId4]);                                                                                      // 855
    testSafeAppendToBufferFlag(true);                                                                                  // 856
                                                                                                                       // 857
    // Let's add something small enough to fit in                                                                      // 858
    var docId5 = ins({ foo: 22, bar: -1 });                                                                            // 859
    // State: [ -1:5 3:3 5:1 | 6:2 7:4 ]!                                                                              // 860
    // We should get an added and a removed events                                                                     // 861
    test.length(o.output, 2);                                                                                          // 862
    // doc 2 was removed from the published set as it is too big to be in                                              // 863
    test.isTrue(setsEqual(o.output, [{added: docId5}, {removed: docId2}]));                                            // 864
    clearOutput(o);                                                                                                    // 865
    testOplogBufferIds([docId2, docId4]);                                                                              // 866
    testSafeAppendToBufferFlag(true);                                                                                  // 867
                                                                                                                       // 868
    // Now remove something and that doc 2 should be right back                                                        // 869
    rem(docId5);                                                                                                       // 870
    // State: [ 3:3 5:1 6:2 | 7:4 ]!                                                                                   // 871
    test.length(o.output, 2);                                                                                          // 872
    test.isTrue(setsEqual(o.output, [{removed: docId5}, {added: docId2}]));                                            // 873
    clearOutput(o);                                                                                                    // 874
    testOplogBufferIds([docId4]);                                                                                      // 875
    testSafeAppendToBufferFlag(true);                                                                                  // 876
                                                                                                                       // 877
    // Add some negative numbers overflowing the buffer.                                                               // 878
    // New documents will take the published place, [3 5 6] will take the buffer                                       // 879
    // and 7 will be outside of the buffer in MongoDB.                                                                 // 880
    var docId6 = ins({ foo: 22, bar: -1 });                                                                            // 881
    var docId7 = ins({ foo: 22, bar: -2 });                                                                            // 882
    var docId8 = ins({ foo: 22, bar: -3 });                                                                            // 883
    // State: [ -3:8 -2:7 -1:6 | 3:3 5:1 6:2 ] 7:4                                                                     // 884
    test.length(o.output, 6);                                                                                          // 885
    var expected = [{added: docId6}, {removed: docId2},                                                                // 886
                    {added: docId7}, {removed: docId1},                                                                // 887
                    {added: docId8}, {removed: docId3}];                                                               // 888
    test.isTrue(setsEqual(o.output, expected));                                                                        // 889
    clearOutput(o);                                                                                                    // 890
    testOplogBufferIds([docId1, docId2, docId3]);                                                                      // 891
    testSafeAppendToBufferFlag(false);                                                                                 // 892
                                                                                                                       // 893
    // If we update first 3 docs (increment them by 20), it would be                                                   // 894
    // interesting.                                                                                                    // 895
    upd({ bar: { $lt: 0 }}, { $inc: { bar: 20 } }, { multi: true });                                                   // 896
    // State: [ 3:3 5:1 6:2 | ] 7:4 17:8 18:7 19:6                                                                     // 897
    //   which triggers re-poll leaving us at                                                                          // 898
    // State: [ 3:3 5:1 6:2 | 7:4 17:8 18:7 ] 19:6                                                                     // 899
                                                                                                                       // 900
    // The updated documents can't find their place in published and they can't                                        // 901
    // be buffered as we are not aware of the situation outside of the buffer.                                         // 902
    // But since our buffer becomes empty, it will be refilled partially with                                          // 903
    // updated documents.                                                                                              // 904
    test.length(o.output, 6);                                                                                          // 905
    var expectedRemoves = [{removed: docId6},                                                                          // 906
                           {removed: docId7},                                                                          // 907
                           {removed: docId8}];                                                                         // 908
    var expectedAdds = [{added: docId3},                                                                               // 909
                        {added: docId1},                                                                               // 910
                        {added: docId2}];                                                                              // 911
                                                                                                                       // 912
    test.isTrue(setsEqual(o.output, expectedAdds.concat(expectedRemoves)));                                            // 913
    clearOutput(o);                                                                                                    // 914
    testOplogBufferIds([docId4, docId7, docId8]);                                                                      // 915
    testSafeAppendToBufferFlag(false);                                                                                 // 916
                                                                                                                       // 917
    // Remove first 4 docs (3, 1, 2, 4) forcing buffer to become empty and                                             // 918
    // schedule a repoll.                                                                                              // 919
    rem({ bar: { $lt: 10 } });                                                                                         // 920
    // State: [ 17:8 18:7 19:6 | ]!                                                                                    // 921
                                                                                                                       // 922
    // XXX the oplog code analyzes the events one by one: one remove after                                             // 923
    // another. Poll-n-diff code, on the other side, analyzes the batch action                                         // 924
    // of multiple remove. Because of that difference, expected outputs differ.                                        // 925
    if (usesOplog) {                                                                                                   // 926
      var expectedRemoves = [{removed: docId3}, {removed: docId1},                                                     // 927
                             {removed: docId2}, {removed: docId4}];                                                    // 928
      var expectedAdds = [{added: docId4}, {added: docId8},                                                            // 929
                          {added: docId7}, {added: docId6}];                                                           // 930
                                                                                                                       // 931
      test.length(o.output, 8);                                                                                        // 932
    } else {                                                                                                           // 933
      var expectedRemoves = [{removed: docId3}, {removed: docId1},                                                     // 934
                             {removed: docId2}];                                                                       // 935
      var expectedAdds = [{added: docId8}, {added: docId7}, {added: docId6}];                                          // 936
                                                                                                                       // 937
      test.length(o.output, 6);                                                                                        // 938
    }                                                                                                                  // 939
                                                                                                                       // 940
    test.isTrue(setsEqual(o.output, expectedAdds.concat(expectedRemoves)));                                            // 941
    clearOutput(o);                                                                                                    // 942
    testOplogBufferIds([]);                                                                                            // 943
    testSafeAppendToBufferFlag(true);                                                                                  // 944
                                                                                                                       // 945
    var docId9 = ins({ foo: 22, bar: 21 });                                                                            // 946
    var docId10 = ins({ foo: 22, bar: 31 });                                                                           // 947
    var docId11 = ins({ foo: 22, bar: 41 });                                                                           // 948
    var docId12 = ins({ foo: 22, bar: 51 });                                                                           // 949
    // State: [ 17:8 18:7 19:6 | 21:9 31:10 41:11 ] 51:12                                                              // 950
                                                                                                                       // 951
    testOplogBufferIds([docId9, docId10, docId11]);                                                                    // 952
    testSafeAppendToBufferFlag(false);                                                                                 // 953
    test.length(o.output, 0);                                                                                          // 954
    upd({ bar: { $lt: 20 } }, { $inc: { bar: 5 } }, { multi: true });                                                  // 955
    // State: [ 21:9 22:8 23:7 | 24:6 31:10 41:11 ] 51:12                                                              // 956
    test.length(o.output, 4);                                                                                          // 957
    test.isTrue(setsEqual(o.output, [{removed: docId6},                                                                // 958
                                     {added: docId9},                                                                  // 959
                                     {changed: docId7},                                                                // 960
                                     {changed: docId8}]));                                                             // 961
    clearOutput(o);                                                                                                    // 962
    testOplogBufferIds([docId6, docId10, docId11]);                                                                    // 963
    testSafeAppendToBufferFlag(false);                                                                                 // 964
                                                                                                                       // 965
    rem(docId9);                                                                                                       // 966
    // State: [ 22:8 23:7 24:6 | 31:10 41:11 ] 51:12                                                                   // 967
    test.length(o.output, 2);                                                                                          // 968
    test.isTrue(setsEqual(o.output, [{removed: docId9}, {added: docId6}]));                                            // 969
    clearOutput(o);                                                                                                    // 970
    testOplogBufferIds([docId10, docId11]);                                                                            // 971
    testSafeAppendToBufferFlag(false);                                                                                 // 972
                                                                                                                       // 973
    upd({ bar: { $gt: 25 } }, { $inc: { bar: -7.5 } }, { multi: true });                                               // 974
    // State: [ 22:8 23:7 23.5:10 | 24:6 ] 33.5:11 43.5:12                                                             // 975
    // 33.5 doesn't update in-place in buffer, because it the driver is not sure                                       // 976
    // it can do it: because the buffer does not have the safe append flag set,                                        // 977
    // for all it knows there is a different doc which is less than 33.5.                                              // 978
    test.length(o.output, 2);                                                                                          // 979
    test.isTrue(setsEqual(o.output, [{removed: docId6}, {added: docId10}]));                                           // 980
    clearOutput(o);                                                                                                    // 981
    testOplogBufferIds([docId6]);                                                                                      // 982
    testSafeAppendToBufferFlag(false);                                                                                 // 983
                                                                                                                       // 984
    // Force buffer objects to be moved into published set so we can check them                                        // 985
    rem(docId7);                                                                                                       // 986
    rem(docId8);                                                                                                       // 987
    rem(docId10);                                                                                                      // 988
    // State: [ 24:6 | ] 33.5:11 43.5:12                                                                               // 989
    //    triggers repoll                                                                                              // 990
    // State: [ 24:6 33.5:11 43.5:12 | ]!                                                                              // 991
    test.length(o.output, 6);                                                                                          // 992
    test.isTrue(setsEqual(o.output, [{removed: docId7}, {removed: docId8},                                             // 993
                                     {removed: docId10}, {added: docId6},                                              // 994
                                     {added: docId11}, {added: docId12}]));                                            // 995
                                                                                                                       // 996
    test.length(_.keys(o.state), 3);                                                                                   // 997
    test.equal(o.state[docId6], { _id: docId6, foo: 22, bar: 24 });                                                    // 998
    test.equal(o.state[docId11], { _id: docId11, foo: 22, bar: 33.5 });                                                // 999
    test.equal(o.state[docId12], { _id: docId12, foo: 22, bar: 43.5 });                                                // 1000
    clearOutput(o);                                                                                                    // 1001
    testOplogBufferIds([]);                                                                                            // 1002
    testSafeAppendToBufferFlag(true);                                                                                  // 1003
                                                                                                                       // 1004
    var docId13 = ins({ foo: 22, bar: 50 });                                                                           // 1005
    var docId14 = ins({ foo: 22, bar: 51 });                                                                           // 1006
    var docId15 = ins({ foo: 22, bar: 52 });                                                                           // 1007
    var docId16 = ins({ foo: 22, bar: 53 });                                                                           // 1008
    // State: [ 24:6 33.5:11 43.5:12 | 50:13 51:14 52:15 ] 53:16                                                       // 1009
    test.length(o.output, 0);                                                                                          // 1010
    testOplogBufferIds([docId13, docId14, docId15]);                                                                   // 1011
    testSafeAppendToBufferFlag(false);                                                                                 // 1012
                                                                                                                       // 1013
    // Update something that's outside the buffer to be in the buffer, writing                                         // 1014
    // only to the sort key.                                                                                           // 1015
    upd(docId16, {$set: {bar: 10}});                                                                                   // 1016
    // State: [ 10:16 24:6 33.5:11 | 43.5:12 50:13 51:14 ] 52:15                                                       // 1017
    test.length(o.output, 2);                                                                                          // 1018
    test.isTrue(setsEqual(o.output, [{removed: docId12}, {added: docId16}]));                                          // 1019
    clearOutput(o);                                                                                                    // 1020
    testOplogBufferIds([docId12, docId13, docId14]);                                                                   // 1021
    testSafeAppendToBufferFlag(false);                                                                                 // 1022
                                                                                                                       // 1023
    o.handle.stop();                                                                                                   // 1024
    onComplete();                                                                                                      // 1025
  });                                                                                                                  // 1026
                                                                                                                       // 1027
  Tinytest.addAsync("mongo-livedata - observe sorted, limited, sort fields " + idGeneration, function (test, onComplete) {
    var run = test.runId();                                                                                            // 1029
    var coll = new Meteor.Collection("observeLimit-"+run, collectionOptions);                                          // 1030
                                                                                                                       // 1031
    var observer = function () {                                                                                       // 1032
      var state = {};                                                                                                  // 1033
      var output = [];                                                                                                 // 1034
      var callbacks = {                                                                                                // 1035
        changed: function (newDoc) {                                                                                   // 1036
          output.push({changed: newDoc._id});                                                                          // 1037
          state[newDoc._id] = newDoc;                                                                                  // 1038
        },                                                                                                             // 1039
        added: function (newDoc) {                                                                                     // 1040
          output.push({added: newDoc._id});                                                                            // 1041
          state[newDoc._id] = newDoc;                                                                                  // 1042
        },                                                                                                             // 1043
        removed: function (oldDoc) {                                                                                   // 1044
          output.push({removed: oldDoc._id});                                                                          // 1045
          delete state[oldDoc._id];                                                                                    // 1046
        }                                                                                                              // 1047
      };                                                                                                               // 1048
      var handle = coll.find({}, {sort: {x: 1},                                                                        // 1049
                                  limit: 2,                                                                            // 1050
                                  fields: {y: 1}}).observe(callbacks);                                                 // 1051
                                                                                                                       // 1052
      return {output: output, handle: handle, state: state};                                                           // 1053
    };                                                                                                                 // 1054
    var clearOutput = function (o) { o.output.splice(0, o.output.length); };                                           // 1055
    var ins = function (doc) {                                                                                         // 1056
      var id; runInFence(function () { id = coll.insert(doc); });                                                      // 1057
      return id;                                                                                                       // 1058
    };                                                                                                                 // 1059
    var rem = function (id) {                                                                                          // 1060
      runInFence(function () { coll.remove(id); });                                                                    // 1061
    };                                                                                                                 // 1062
                                                                                                                       // 1063
    var o = observer();                                                                                                // 1064
                                                                                                                       // 1065
    var docId1 = ins({ x: 1, y: 1222 });                                                                               // 1066
    var docId2 = ins({ x: 5, y: 5222 });                                                                               // 1067
                                                                                                                       // 1068
    test.length(o.output, 2);                                                                                          // 1069
    test.equal(o.output, [{added: docId1}, {added: docId2}]);                                                          // 1070
    clearOutput(o);                                                                                                    // 1071
                                                                                                                       // 1072
    var docId3 = ins({ x: 7, y: 7222 });                                                                               // 1073
    test.length(o.output, 0);                                                                                          // 1074
                                                                                                                       // 1075
    var docId4 = ins({ x: -1, y: -1222 });                                                                             // 1076
                                                                                                                       // 1077
    // Becomes [docId4 docId1 | docId2 docId3]                                                                         // 1078
    test.length(o.output, 2);                                                                                          // 1079
    test.isTrue(setsEqual(o.output, [{added: docId4}, {removed: docId2}]));                                            // 1080
                                                                                                                       // 1081
    test.equal(_.size(o.state), 2);                                                                                    // 1082
    test.equal(o.state[docId4], {_id: docId4, y: -1222});                                                              // 1083
    test.equal(o.state[docId1], {_id: docId1, y: 1222});                                                               // 1084
    clearOutput(o);                                                                                                    // 1085
                                                                                                                       // 1086
    rem(docId2);                                                                                                       // 1087
    // Becomes [docId4 docId1 | docId3]                                                                                // 1088
    test.length(o.output, 0);                                                                                          // 1089
                                                                                                                       // 1090
    rem(docId4);                                                                                                       // 1091
    // Becomes [docId1 docId3]                                                                                         // 1092
    test.length(o.output, 2);                                                                                          // 1093
    test.isTrue(setsEqual(o.output, [{added: docId3}, {removed: docId4}]));                                            // 1094
                                                                                                                       // 1095
    test.equal(_.size(o.state), 2);                                                                                    // 1096
    test.equal(o.state[docId3], {_id: docId3, y: 7222});                                                               // 1097
    test.equal(o.state[docId1], {_id: docId1, y: 1222});                                                               // 1098
    clearOutput(o);                                                                                                    // 1099
                                                                                                                       // 1100
    onComplete();                                                                                                      // 1101
  });                                                                                                                  // 1102
                                                                                                                       // 1103
  Tinytest.addAsync("mongo-livedata - observe sorted, limited, big initial set" + idGeneration, function (test, onComplete) {
    var run = test.runId();                                                                                            // 1105
    var coll = new Meteor.Collection("observeLimit-"+run, collectionOptions);                                          // 1106
                                                                                                                       // 1107
    var observer = function () {                                                                                       // 1108
      var state = {};                                                                                                  // 1109
      var output = [];                                                                                                 // 1110
      var callbacks = {                                                                                                // 1111
        changed: function (newDoc) {                                                                                   // 1112
          output.push({changed: newDoc._id});                                                                          // 1113
          state[newDoc._id] = newDoc;                                                                                  // 1114
        },                                                                                                             // 1115
        added: function (newDoc) {                                                                                     // 1116
          output.push({added: newDoc._id});                                                                            // 1117
          state[newDoc._id] = newDoc;                                                                                  // 1118
        },                                                                                                             // 1119
        removed: function (oldDoc) {                                                                                   // 1120
          output.push({removed: oldDoc._id});                                                                          // 1121
          delete state[oldDoc._id];                                                                                    // 1122
        }                                                                                                              // 1123
      };                                                                                                               // 1124
      var handle = coll.find({}, {sort: {x: 1, y: 1}, limit: 3})                                                       // 1125
                    .observe(callbacks);                                                                               // 1126
                                                                                                                       // 1127
      return {output: output, handle: handle, state: state};                                                           // 1128
    };                                                                                                                 // 1129
    var clearOutput = function (o) { o.output.splice(0, o.output.length); };                                           // 1130
    var ins = function (doc) {                                                                                         // 1131
      var id; runInFence(function () { id = coll.insert(doc); });                                                      // 1132
      return id;                                                                                                       // 1133
    };                                                                                                                 // 1134
    var rem = function (id) {                                                                                          // 1135
      runInFence(function () { coll.remove(id); });                                                                    // 1136
    };                                                                                                                 // 1137
    // tests '_id' subfields for all documents in oplog buffer                                                         // 1138
    var testOplogBufferIds = function (ids) {                                                                          // 1139
      var bufferIds = [];                                                                                              // 1140
      o.handle._multiplexer._observeDriver._unpublishedBuffer.forEach(function (x, id) {                               // 1141
        bufferIds.push(id);                                                                                            // 1142
      });                                                                                                              // 1143
                                                                                                                       // 1144
      test.isTrue(setsEqual(ids, bufferIds), "expected: " + ids + "; got: " + bufferIds);                              // 1145
    };                                                                                                                 // 1146
    var testSafeAppendToBufferFlag = function (expected) {                                                             // 1147
      if (expected)                                                                                                    // 1148
        test.isTrue(o.handle._multiplexer._observeDriver._safeAppendToBuffer);                                         // 1149
      else                                                                                                             // 1150
        test.isFalse(o.handle._multiplexer._observeDriver._safeAppendToBuffer);                                        // 1151
    };                                                                                                                 // 1152
                                                                                                                       // 1153
    var ids = {};                                                                                                      // 1154
    _.each([2, 4, 1, 3, 5, 5, 9, 1, 3, 2, 5], function (x, i) {                                                        // 1155
      ids[i] = ins({ x: x, y: i });                                                                                    // 1156
    });                                                                                                                // 1157
                                                                                                                       // 1158
    // Ensure that we are past all the 'i' entries before we run the query, so                                         // 1159
    // that we get the expected phase transitions.                                                                     // 1160
    waitUntilOplogCaughtUp();                                                                                          // 1161
                                                                                                                       // 1162
    var o = observer();                                                                                                // 1163
    var usesOplog = o.handle._multiplexer._observeDriver._usesOplog;                                                   // 1164
    //  x: [1 1 2 | 2 3 3] 4 5 5 5  9                                                                                  // 1165
    // id: [2 7 0 | 9 3 8] 1 4 5 10 6                                                                                  // 1166
                                                                                                                       // 1167
    test.length(o.output, 3);                                                                                          // 1168
    test.isTrue(setsEqual([{added: ids[2]}, {added: ids[7]}, {added: ids[0]}], o.output));                             // 1169
    usesOplog && testOplogBufferIds([ids[9], ids[3], ids[8]]);                                                         // 1170
    usesOplog && testSafeAppendToBufferFlag(false);                                                                    // 1171
    clearOutput(o);                                                                                                    // 1172
                                                                                                                       // 1173
    rem(ids[0]);                                                                                                       // 1174
    //  x: [1 1 2 | 3 3] 4 5 5 5  9                                                                                    // 1175
    // id: [2 7 9 | 3 8] 1 4 5 10 6                                                                                    // 1176
    test.length(o.output, 2);                                                                                          // 1177
    test.isTrue(setsEqual([{removed: ids[0]}, {added: ids[9]}], o.output));                                            // 1178
    usesOplog && testOplogBufferIds([ids[3], ids[8]]);                                                                 // 1179
    usesOplog && testSafeAppendToBufferFlag(false);                                                                    // 1180
    clearOutput(o);                                                                                                    // 1181
                                                                                                                       // 1182
    rem(ids[7]);                                                                                                       // 1183
    //  x: [1 2 3 | 3] 4 5 5 5  9                                                                                      // 1184
    // id: [2 9 3 | 8] 1 4 5 10 6                                                                                      // 1185
    test.length(o.output, 2);                                                                                          // 1186
    test.isTrue(setsEqual([{removed: ids[7]}, {added: ids[3]}], o.output));                                            // 1187
    usesOplog && testOplogBufferIds([ids[8]]);                                                                         // 1188
    usesOplog && testSafeAppendToBufferFlag(false);                                                                    // 1189
    clearOutput(o);                                                                                                    // 1190
                                                                                                                       // 1191
    rem(ids[3]);                                                                                                       // 1192
    //  x: [1 2 3 | 4 5 5] 5  9                                                                                        // 1193
    // id: [2 9 8 | 1 4 5] 10 6                                                                                        // 1194
    test.length(o.output, 2);                                                                                          // 1195
    test.isTrue(setsEqual([{removed: ids[3]}, {added: ids[8]}], o.output));                                            // 1196
    usesOplog && testOplogBufferIds([ids[1], ids[4], ids[5]]);                                                         // 1197
    usesOplog && testSafeAppendToBufferFlag(false);                                                                    // 1198
    clearOutput(o);                                                                                                    // 1199
                                                                                                                       // 1200
    rem({ x: {$lt: 4} });                                                                                              // 1201
    //  x: [4 5 5 | 5  9]                                                                                              // 1202
    // id: [1 4 5 | 10 6]                                                                                              // 1203
    test.length(o.output, 6);                                                                                          // 1204
    test.isTrue(setsEqual([{removed: ids[2]}, {removed: ids[9]}, {removed: ids[8]},                                    // 1205
                           {added: ids[5]}, {added: ids[4]}, {added: ids[1]}], o.output));                             // 1206
    usesOplog && testOplogBufferIds([ids[10], ids[6]]);                                                                // 1207
    usesOplog && testSafeAppendToBufferFlag(true);                                                                     // 1208
    clearOutput(o);                                                                                                    // 1209
                                                                                                                       // 1210
                                                                                                                       // 1211
    onComplete();                                                                                                      // 1212
  });                                                                                                                  // 1213
}                                                                                                                      // 1214
                                                                                                                       // 1215
                                                                                                                       // 1216
testAsyncMulti('mongo-livedata - empty documents, ' + idGeneration, [                                                  // 1217
  function (test, expect) {                                                                                            // 1218
    var collectionName = Random.id();                                                                                  // 1219
    if (Meteor.isClient) {                                                                                             // 1220
      Meteor.call('createInsecureCollection', collectionName);                                                         // 1221
      Meteor.subscribe('c-' + collectionName);                                                                         // 1222
    }                                                                                                                  // 1223
                                                                                                                       // 1224
    var coll = new Meteor.Collection(collectionName, collectionOptions);                                               // 1225
                                                                                                                       // 1226
    coll.insert({}, expect(function (err, id) {                                                                        // 1227
      test.isFalse(err);                                                                                               // 1228
      test.isTrue(id);                                                                                                 // 1229
      var cursor = coll.find();                                                                                        // 1230
      test.equal(cursor.count(), 1);                                                                                   // 1231
    }));                                                                                                               // 1232
  }                                                                                                                    // 1233
]);                                                                                                                    // 1234
                                                                                                                       // 1235
// See https://github.com/meteor/meteor/issues/594.                                                                    // 1236
testAsyncMulti('mongo-livedata - document with length, ' + idGeneration, [                                             // 1237
  function (test, expect) {                                                                                            // 1238
    var self = this;                                                                                                   // 1239
    var collectionName = Random.id();                                                                                  // 1240
    if (Meteor.isClient) {                                                                                             // 1241
      Meteor.call('createInsecureCollection', collectionName);                                                         // 1242
      Meteor.subscribe('c-' + collectionName);                                                                         // 1243
    }                                                                                                                  // 1244
                                                                                                                       // 1245
    self.coll = new Meteor.Collection(collectionName, collectionOptions);                                              // 1246
                                                                                                                       // 1247
    self.coll.insert({foo: 'x', length: 0}, expect(function (err, id) {                                                // 1248
      test.isFalse(err);                                                                                               // 1249
      test.isTrue(id);                                                                                                 // 1250
      self.docId = id;                                                                                                 // 1251
      test.equal(self.coll.findOne(self.docId),                                                                        // 1252
                 {_id: self.docId, foo: 'x', length: 0});                                                              // 1253
    }));                                                                                                               // 1254
  },                                                                                                                   // 1255
  function (test, expect) {                                                                                            // 1256
    var self = this;                                                                                                   // 1257
    self.coll.update(self.docId, {$set: {length: 5}}, expect(function (err) {                                          // 1258
      test.isFalse(err);                                                                                               // 1259
      test.equal(self.coll.findOne(self.docId),                                                                        // 1260
                 {_id: self.docId, foo: 'x', length: 5});                                                              // 1261
    }));                                                                                                               // 1262
  }                                                                                                                    // 1263
]);                                                                                                                    // 1264
                                                                                                                       // 1265
testAsyncMulti('mongo-livedata - document with a date, ' + idGeneration, [                                             // 1266
  function (test, expect) {                                                                                            // 1267
    var collectionName = Random.id();                                                                                  // 1268
    if (Meteor.isClient) {                                                                                             // 1269
      Meteor.call('createInsecureCollection', collectionName, collectionOptions);                                      // 1270
      Meteor.subscribe('c-' + collectionName);                                                                         // 1271
    }                                                                                                                  // 1272
                                                                                                                       // 1273
    var coll = new Meteor.Collection(collectionName, collectionOptions);                                               // 1274
    var docId;                                                                                                         // 1275
    coll.insert({d: new Date(1356152390004)}, expect(function (err, id) {                                              // 1276
      test.isFalse(err);                                                                                               // 1277
      test.isTrue(id);                                                                                                 // 1278
      docId = id;                                                                                                      // 1279
      var cursor = coll.find();                                                                                        // 1280
      test.equal(cursor.count(), 1);                                                                                   // 1281
      test.equal(coll.findOne().d.getFullYear(), 2012);                                                                // 1282
    }));                                                                                                               // 1283
  }                                                                                                                    // 1284
]);                                                                                                                    // 1285
                                                                                                                       // 1286
testAsyncMulti('mongo-livedata - document goes through a transform, ' + idGeneration, [                                // 1287
  function (test, expect) {                                                                                            // 1288
    var self = this;                                                                                                   // 1289
    var seconds = function (doc) {                                                                                     // 1290
      doc.seconds = function () {return doc.d.getSeconds();};                                                          // 1291
      return doc;                                                                                                      // 1292
    };                                                                                                                 // 1293
    TRANSFORMS["seconds"] = seconds;                                                                                   // 1294
    var collectionOptions = {                                                                                          // 1295
      idGeneration: idGeneration,                                                                                      // 1296
      transform: seconds,                                                                                              // 1297
      transformName: "seconds"                                                                                         // 1298
    };                                                                                                                 // 1299
    var collectionName = Random.id();                                                                                  // 1300
    if (Meteor.isClient) {                                                                                             // 1301
      Meteor.call('createInsecureCollection', collectionName, collectionOptions);                                      // 1302
      Meteor.subscribe('c-' + collectionName);                                                                         // 1303
    }                                                                                                                  // 1304
                                                                                                                       // 1305
    self.coll = new Meteor.Collection(collectionName, collectionOptions);                                              // 1306
    var obs;                                                                                                           // 1307
    var expectAdd = expect(function (doc) {                                                                            // 1308
      test.equal(doc.seconds(), 50);                                                                                   // 1309
    });                                                                                                                // 1310
    var expectRemove = expect (function (doc) {                                                                        // 1311
      test.equal(doc.seconds(), 50);                                                                                   // 1312
      obs.stop();                                                                                                      // 1313
    });                                                                                                                // 1314
    self.coll.insert({d: new Date(1356152390004)}, expect(function (err, id) {                                         // 1315
      test.isFalse(err);                                                                                               // 1316
      test.isTrue(id);                                                                                                 // 1317
      var cursor = self.coll.find();                                                                                   // 1318
      obs = cursor.observe({                                                                                           // 1319
        added: expectAdd,                                                                                              // 1320
        removed: expectRemove                                                                                          // 1321
      });                                                                                                              // 1322
      test.equal(cursor.count(), 1);                                                                                   // 1323
      test.equal(cursor.fetch()[0].seconds(), 50);                                                                     // 1324
      test.equal(self.coll.findOne().seconds(), 50);                                                                   // 1325
      test.equal(self.coll.findOne({}, {transform: null}).seconds, undefined);                                         // 1326
      test.equal(self.coll.findOne({}, {                                                                               // 1327
        transform: function (doc) {return {seconds: doc.d.getSeconds()};}                                              // 1328
      }).seconds, 50);                                                                                                 // 1329
      self.coll.remove(id);                                                                                            // 1330
    }));                                                                                                               // 1331
  },                                                                                                                   // 1332
  function (test, expect) {                                                                                            // 1333
    var self = this;                                                                                                   // 1334
    self.coll.insert({d: new Date(1356152390004)}, expect(function (err, id) {                                         // 1335
      test.isFalse(err);                                                                                               // 1336
      test.isTrue(id);                                                                                                 // 1337
      self.id1 = id;                                                                                                   // 1338
    }));                                                                                                               // 1339
    self.coll.insert({d: new Date(1356152391004)}, expect(function (err, id) {                                         // 1340
      test.isFalse(err);                                                                                               // 1341
      test.isTrue(id);                                                                                                 // 1342
      self.id2 = id;                                                                                                   // 1343
    }));                                                                                                               // 1344
  }                                                                                                                    // 1345
]);                                                                                                                    // 1346
                                                                                                                       // 1347
testAsyncMulti('mongo-livedata - transform sets _id if not present, ' + idGeneration, [                                // 1348
  function (test, expect) {                                                                                            // 1349
    var self = this;                                                                                                   // 1350
    var justId = function (doc) {                                                                                      // 1351
      return _.omit(doc, '_id');                                                                                       // 1352
    };                                                                                                                 // 1353
    TRANSFORMS["justId"] = justId;                                                                                     // 1354
    var collectionOptions = {                                                                                          // 1355
      idGeneration: idGeneration,                                                                                      // 1356
      transform: justId,                                                                                               // 1357
      transformName: "justId"                                                                                          // 1358
    };                                                                                                                 // 1359
    var collectionName = Random.id();                                                                                  // 1360
    if (Meteor.isClient) {                                                                                             // 1361
      Meteor.call('createInsecureCollection', collectionName, collectionOptions);                                      // 1362
      Meteor.subscribe('c-' + collectionName);                                                                         // 1363
    }                                                                                                                  // 1364
    self.coll = new Meteor.Collection(collectionName, collectionOptions);                                              // 1365
    self.coll.insert({}, expect(function (err, id) {                                                                   // 1366
      test.isFalse(err);                                                                                               // 1367
      test.isTrue(id);                                                                                                 // 1368
      test.equal(self.coll.findOne()._id, id);                                                                         // 1369
    }));                                                                                                               // 1370
  }                                                                                                                    // 1371
]);                                                                                                                    // 1372
                                                                                                                       // 1373
testAsyncMulti('mongo-livedata - document with binary data, ' + idGeneration, [                                        // 1374
  function (test, expect) {                                                                                            // 1375
    // XXX probably shouldn't use EJSON's private test symbols                                                         // 1376
    var bin = EJSONTest.base64Decode(                                                                                  // 1377
      "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyBy" +                                                         // 1378
        "ZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJv" +                                                       // 1379
        "bSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhl" +                                                       // 1380
        "IG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdo" +                                                       // 1381
        "dCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdl" +                                                       // 1382
        "bmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9y" +                                                       // 1383
        "dCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=");                                                           // 1384
    var collectionName = Random.id();                                                                                  // 1385
    if (Meteor.isClient) {                                                                                             // 1386
      Meteor.call('createInsecureCollection', collectionName, collectionOptions);                                      // 1387
      Meteor.subscribe('c-' + collectionName);                                                                         // 1388
    }                                                                                                                  // 1389
                                                                                                                       // 1390
    var coll = new Meteor.Collection(collectionName, collectionOptions);                                               // 1391
    var docId;                                                                                                         // 1392
    coll.insert({b: bin}, expect(function (err, id) {                                                                  // 1393
      test.isFalse(err);                                                                                               // 1394
      test.isTrue(id);                                                                                                 // 1395
      docId = id;                                                                                                      // 1396
      var cursor = coll.find();                                                                                        // 1397
      test.equal(cursor.count(), 1);                                                                                   // 1398
      var inColl = coll.findOne();                                                                                     // 1399
      test.isTrue(EJSON.isBinary(inColl.b));                                                                           // 1400
      test.equal(inColl.b, bin);                                                                                       // 1401
    }));                                                                                                               // 1402
  }                                                                                                                    // 1403
]);                                                                                                                    // 1404
                                                                                                                       // 1405
testAsyncMulti('mongo-livedata - document with a custom type, ' + idGeneration, [                                      // 1406
  function (test, expect) {                                                                                            // 1407
    var collectionName = Random.id();                                                                                  // 1408
    if (Meteor.isClient) {                                                                                             // 1409
      Meteor.call('createInsecureCollection', collectionName, collectionOptions);                                      // 1410
      Meteor.subscribe('c-' + collectionName);                                                                         // 1411
    }                                                                                                                  // 1412
                                                                                                                       // 1413
    var coll = new Meteor.Collection(collectionName, collectionOptions);                                               // 1414
    var docId;                                                                                                         // 1415
    // Dog is implemented at the top of the file, outside of the idGeneration                                          // 1416
    // loop (so that we only call EJSON.addType once).                                                                 // 1417
    var d = new Dog("reginald", "purple");                                                                             // 1418
    coll.insert({d: d}, expect(function (err, id) {                                                                    // 1419
      test.isFalse(err);                                                                                               // 1420
      test.isTrue(id);                                                                                                 // 1421
      docId = id;                                                                                                      // 1422
      var cursor = coll.find();                                                                                        // 1423
      test.equal(cursor.count(), 1);                                                                                   // 1424
      var inColl = coll.findOne();                                                                                     // 1425
      test.isTrue(inColl);                                                                                             // 1426
      inColl && test.equal(inColl.d.speak(), "woof");                                                                  // 1427
    }));                                                                                                               // 1428
  }                                                                                                                    // 1429
]);                                                                                                                    // 1430
                                                                                                                       // 1431
if (Meteor.isServer) {                                                                                                 // 1432
  Tinytest.addAsync("mongo-livedata - update return values, " + idGeneration, function (test, onComplete) {            // 1433
    var run = test.runId();                                                                                            // 1434
    var coll = new Meteor.Collection("livedata_update_result_"+run, collectionOptions);                                // 1435
                                                                                                                       // 1436
    coll.insert({ foo: "bar" });                                                                                       // 1437
    coll.insert({ foo: "baz" });                                                                                       // 1438
    test.equal(coll.update({}, { $set: { foo: "qux" } }, { multi: true }),                                             // 1439
               2);                                                                                                     // 1440
    coll.update({}, { $set: { foo: "quux" } }, { multi: true }, function (err, result) {                               // 1441
      test.isFalse(err);                                                                                               // 1442
      test.equal(result, 2);                                                                                           // 1443
      onComplete();                                                                                                    // 1444
    });                                                                                                                // 1445
  });                                                                                                                  // 1446
                                                                                                                       // 1447
  Tinytest.addAsync("mongo-livedata - remove return values, " + idGeneration, function (test, onComplete) {            // 1448
    var run = test.runId();                                                                                            // 1449
    var coll = new Meteor.Collection("livedata_update_result_"+run, collectionOptions);                                // 1450
                                                                                                                       // 1451
    coll.insert({ foo: "bar" });                                                                                       // 1452
    coll.insert({ foo: "baz" });                                                                                       // 1453
    test.equal(coll.remove({}), 2);                                                                                    // 1454
    coll.insert({ foo: "bar" });                                                                                       // 1455
    coll.insert({ foo: "baz" });                                                                                       // 1456
    coll.remove({}, function (err, result) {                                                                           // 1457
      test.isFalse(err);                                                                                               // 1458
      test.equal(result, 2);                                                                                           // 1459
      onComplete();                                                                                                    // 1460
    });                                                                                                                // 1461
  });                                                                                                                  // 1462
                                                                                                                       // 1463
                                                                                                                       // 1464
  Tinytest.addAsync("mongo-livedata - id-based invalidation, " + idGeneration, function (test, onComplete) {           // 1465
    var run = test.runId();                                                                                            // 1466
    var coll = new Meteor.Collection("livedata_invalidation_collection_"+run, collectionOptions);                      // 1467
                                                                                                                       // 1468
    coll.allow({                                                                                                       // 1469
      update: function () {return true;},                                                                              // 1470
      remove: function () {return true;}                                                                               // 1471
    });                                                                                                                // 1472
                                                                                                                       // 1473
    var id1 = coll.insert({x: 42, is1: true});                                                                         // 1474
    var id2 = coll.insert({x: 50, is2: true});                                                                         // 1475
                                                                                                                       // 1476
    var polls = {};                                                                                                    // 1477
    var handlesToStop = [];                                                                                            // 1478
    var observe = function (name, query) {                                                                             // 1479
      var handle = coll.find(query).observeChanges({                                                                   // 1480
        // Make sure that we only poll on invalidation, not due to time, and                                           // 1481
        // keep track of when we do. Note: this option disables the use of                                             // 1482
        // oplogs (which admittedly is somewhat irrelevant to this feature).                                           // 1483
        _testOnlyPollCallback: function () {                                                                           // 1484
          polls[name] = (name in polls ? polls[name] + 1 : 1);                                                         // 1485
        }                                                                                                              // 1486
      });                                                                                                              // 1487
      handlesToStop.push(handle);                                                                                      // 1488
    };                                                                                                                 // 1489
                                                                                                                       // 1490
    observe("all", {});                                                                                                // 1491
    observe("id1Direct", id1);                                                                                         // 1492
    observe("id1InQuery", {_id: id1, z: null});                                                                        // 1493
    observe("id2Direct", id2);                                                                                         // 1494
    observe("id2InQuery", {_id: id2, z: null});                                                                        // 1495
    observe("bothIds", {_id: {$in: [id1, id2]}});                                                                      // 1496
                                                                                                                       // 1497
    var resetPollsAndRunInFence = function (f) {                                                                       // 1498
      polls = {};                                                                                                      // 1499
      runInFence(f);                                                                                                   // 1500
    };                                                                                                                 // 1501
                                                                                                                       // 1502
    // Update id1 directly. This should poll all but the "id2" queries. "all"                                          // 1503
    // and "bothIds" increment by 2 because they are looking at both.                                                  // 1504
    resetPollsAndRunInFence(function () {                                                                              // 1505
      coll.update(id1, {$inc: {x: 1}});                                                                                // 1506
    });                                                                                                                // 1507
    test.equal(                                                                                                        // 1508
      polls,                                                                                                           // 1509
      {all: 1, id1Direct: 1, id1InQuery: 1, bothIds: 1});                                                              // 1510
                                                                                                                       // 1511
    // Update id2 using a funny query. This should poll all but the "id1"                                              // 1512
    // queries.                                                                                                        // 1513
    resetPollsAndRunInFence(function () {                                                                              // 1514
      coll.update({_id: id2, q: null}, {$inc: {x: 1}});                                                                // 1515
    });                                                                                                                // 1516
    test.equal(                                                                                                        // 1517
      polls,                                                                                                           // 1518
      {all: 1, id2Direct: 1, id2InQuery: 1, bothIds: 1});                                                              // 1519
                                                                                                                       // 1520
    // Update both using a $in query. Should poll each of them exactly once.                                           // 1521
    resetPollsAndRunInFence(function () {                                                                              // 1522
      coll.update({_id: {$in: [id1, id2]}, q: null}, {$inc: {x: 1}});                                                  // 1523
    });                                                                                                                // 1524
    test.equal(                                                                                                        // 1525
      polls,                                                                                                           // 1526
      {all: 1, id1Direct: 1, id1InQuery: 1, id2Direct: 1, id2InQuery: 1,                                               // 1527
       bothIds: 1});                                                                                                   // 1528
                                                                                                                       // 1529
    _.each(handlesToStop, function (h) {h.stop();});                                                                   // 1530
    onComplete();                                                                                                      // 1531
  });                                                                                                                  // 1532
                                                                                                                       // 1533
  Tinytest.add("mongo-livedata - upsert error parse, " + idGeneration, function (test) {                               // 1534
    var run = test.runId();                                                                                            // 1535
    var coll = new Meteor.Collection("livedata_upsert_errorparse_collection_"+run, collectionOptions);                 // 1536
                                                                                                                       // 1537
    coll.insert({_id: 'foobar'});                                                                                      // 1538
    var err;                                                                                                           // 1539
    try {                                                                                                              // 1540
      coll.update({_id: 'foobar'}, {_id: 'cowbar'});                                                                   // 1541
    } catch (e) {                                                                                                      // 1542
      err = e;                                                                                                         // 1543
    }                                                                                                                  // 1544
    test.isTrue(err);                                                                                                  // 1545
    test.isTrue(MongoInternals.Connection._isCannotChangeIdError(err));                                                // 1546
                                                                                                                       // 1547
    try {                                                                                                              // 1548
      coll.insert({_id: 'foobar'});                                                                                    // 1549
    } catch (e) {                                                                                                      // 1550
      err = e;                                                                                                         // 1551
    }                                                                                                                  // 1552
    test.isTrue(err);                                                                                                  // 1553
    // duplicate id error is not same as change id error                                                               // 1554
    test.isFalse(MongoInternals.Connection._isCannotChangeIdError(err));                                               // 1555
  });                                                                                                                  // 1556
                                                                                                                       // 1557
} // end Meteor.isServer                                                                                               // 1558
                                                                                                                       // 1559
// This test is duplicated below (with some changes) for async upserts that go                                         // 1560
// over the network.                                                                                                   // 1561
_.each(Meteor.isServer ? [true, false] : [true], function (minimongo) {                                                // 1562
  _.each([true, false], function (useUpdate) {                                                                         // 1563
    _.each([true, false], function (useDirectCollection) {                                                             // 1564
      Tinytest.add("mongo-livedata - " + (useUpdate ? "update " : "") + "upsert" + (minimongo ? " minimongo" : "") + (useDirectCollection ? " direct collection " : "") + ", " + idGeneration, function (test) {
        var run = test.runId();                                                                                        // 1566
        var options = collectionOptions;                                                                               // 1567
        // We don't get ids back when we use update() to upsert, or when we are                                        // 1568
        // directly calling MongoConnection.upsert().                                                                  // 1569
        var skipIds = useUpdate || (! minimongo && useDirectCollection);                                               // 1570
        if (minimongo)                                                                                                 // 1571
          options = _.extend({}, collectionOptions, { connection: null });                                             // 1572
        var coll = new Meteor.Collection(                                                                              // 1573
          "livedata_upsert_collection_"+run+                                                                           // 1574
            (useUpdate ? "_update_" : "") +                                                                            // 1575
            (minimongo ? "_minimongo_" : "") +                                                                         // 1576
            (useDirectCollection ? "_direct_" : "") + "",                                                              // 1577
          options                                                                                                      // 1578
        );                                                                                                             // 1579
        if (useDirectCollection)                                                                                       // 1580
          coll = coll._collection;                                                                                     // 1581
                                                                                                                       // 1582
        var result1 = upsert(coll, useUpdate, {foo: 'bar'}, {foo: 'bar'});                                             // 1583
        test.equal(result1.numberAffected, 1);                                                                         // 1584
        if (! skipIds)                                                                                                 // 1585
          test.isTrue(result1.insertedId);                                                                             // 1586
        compareResults(test, skipIds, coll.find().fetch(), [{foo: 'bar', _id: result1.insertedId}]);                   // 1587
                                                                                                                       // 1588
        var result2 = upsert(coll, useUpdate, {foo: 'bar'}, {foo: 'baz'});                                             // 1589
        test.equal(result2.numberAffected, 1);                                                                         // 1590
        if (! skipIds)                                                                                                 // 1591
          test.isFalse(result2.insertedId);                                                                            // 1592
        compareResults(test, skipIds, coll.find().fetch(), [{foo: 'baz', _id: result1.insertedId}]);                   // 1593
                                                                                                                       // 1594
        coll.remove({});                                                                                               // 1595
                                                                                                                       // 1596
        // Test values that require transformation to go into Mongo:                                                   // 1597
                                                                                                                       // 1598
        var t1 = new Meteor.Collection.ObjectID();                                                                     // 1599
        var t2 = new Meteor.Collection.ObjectID();                                                                     // 1600
        var result3 = upsert(coll, useUpdate, {foo: t1}, {foo: t1});                                                   // 1601
        test.equal(result3.numberAffected, 1);                                                                         // 1602
        if (! skipIds)                                                                                                 // 1603
          test.isTrue(result3.insertedId);                                                                             // 1604
        compareResults(test, skipIds, coll.find().fetch(), [{foo: t1, _id: result3.insertedId}]);                      // 1605
                                                                                                                       // 1606
        var result4 = upsert(coll, useUpdate, {foo: t1}, {foo: t2});                                                   // 1607
        test.equal(result2.numberAffected, 1);                                                                         // 1608
        if (! skipIds)                                                                                                 // 1609
          test.isFalse(result2.insertedId);                                                                            // 1610
        compareResults(test, skipIds, coll.find().fetch(), [{foo: t2, _id: result3.insertedId}]);                      // 1611
                                                                                                                       // 1612
        coll.remove({});                                                                                               // 1613
                                                                                                                       // 1614
        // Test modification by upsert                                                                                 // 1615
                                                                                                                       // 1616
        var result5 = upsert(coll, useUpdate, {name: 'David'}, {$set: {foo: 1}});                                      // 1617
        test.equal(result5.numberAffected, 1);                                                                         // 1618
        if (! skipIds)                                                                                                 // 1619
          test.isTrue(result5.insertedId);                                                                             // 1620
        var davidId = result5.insertedId;                                                                              // 1621
        compareResults(test, skipIds, coll.find().fetch(), [{name: 'David', foo: 1, _id: davidId}]);                   // 1622
                                                                                                                       // 1623
        test.throws(function () {                                                                                      // 1624
          // test that bad modifier fails fast                                                                         // 1625
          upsert(coll, useUpdate, {name: 'David'}, {$blah: {foo: 2}});                                                 // 1626
        });                                                                                                            // 1627
                                                                                                                       // 1628
                                                                                                                       // 1629
        var result6 = upsert(coll, useUpdate, {name: 'David'}, {$set: {foo: 2}});                                      // 1630
        test.equal(result6.numberAffected, 1);                                                                         // 1631
        if (! skipIds)                                                                                                 // 1632
          test.isFalse(result6.insertedId);                                                                            // 1633
        compareResults(test, skipIds, coll.find().fetch(), [{name: 'David', foo: 2,                                    // 1634
                                                               _id: result5.insertedId}]);                             // 1635
                                                                                                                       // 1636
        var emilyId = coll.insert({name: 'Emily', foo: 2});                                                            // 1637
        compareResults(test, skipIds, coll.find().fetch(), [{name: 'David', foo: 2, _id: davidId},                     // 1638
                                                              {name: 'Emily', foo: 2, _id: emilyId}]);                 // 1639
                                                                                                                       // 1640
        // multi update by upsert                                                                                      // 1641
        var result7 = upsert(coll, useUpdate, {foo: 2},                                                                // 1642
                             {$set: {bar: 7},                                                                          // 1643
                              $setOnInsert: {name: 'Fred', foo: 2}},                                                   // 1644
                             {multi: true});                                                                           // 1645
        test.equal(result7.numberAffected, 2);                                                                         // 1646
        if (! skipIds)                                                                                                 // 1647
          test.isFalse(result7.insertedId);                                                                            // 1648
        compareResults(test, skipIds, coll.find().fetch(), [{name: 'David', foo: 2, bar: 7, _id: davidId},             // 1649
                                                              {name: 'Emily', foo: 2, bar: 7, _id: emilyId}]);         // 1650
                                                                                                                       // 1651
        // insert by multi upsert                                                                                      // 1652
        var result8 = upsert(coll, useUpdate, {foo: 3},                                                                // 1653
                             {$set: {bar: 7},                                                                          // 1654
                              $setOnInsert: {name: 'Fred', foo: 2}},                                                   // 1655
                             {multi: true});                                                                           // 1656
        test.equal(result8.numberAffected, 1);                                                                         // 1657
        if (! skipIds)                                                                                                 // 1658
          test.isTrue(result8.insertedId);                                                                             // 1659
        var fredId = result8.insertedId;                                                                               // 1660
        compareResults(test, skipIds, coll.find().fetch(),                                                             // 1661
                       [{name: 'David', foo: 2, bar: 7, _id: davidId},                                                 // 1662
                        {name: 'Emily', foo: 2, bar: 7, _id: emilyId},                                                 // 1663
                        {name: 'Fred', foo: 2, bar: 7, _id: fredId}]);                                                 // 1664
                                                                                                                       // 1665
        // test `insertedId` option                                                                                    // 1666
        var result9 = upsert(coll, useUpdate, {name: 'Steve'},                                                         // 1667
                             {name: 'Steve'},                                                                          // 1668
                             {insertedId: 'steve'});                                                                   // 1669
        test.equal(result9.numberAffected, 1);                                                                         // 1670
        if (! skipIds)                                                                                                 // 1671
          test.equal(result9.insertedId, 'steve');                                                                     // 1672
        compareResults(test, skipIds, coll.find().fetch(),                                                             // 1673
                       [{name: 'David', foo: 2, bar: 7, _id: davidId},                                                 // 1674
                        {name: 'Emily', foo: 2, bar: 7, _id: emilyId},                                                 // 1675
                        {name: 'Fred', foo: 2, bar: 7, _id: fredId},                                                   // 1676
                        {name: 'Steve', _id: 'steve'}]);                                                               // 1677
        test.isTrue(coll.findOne('steve'));                                                                            // 1678
        test.isFalse(coll.findOne('fred'));                                                                            // 1679
                                                                                                                       // 1680
        // Test $ operator in selectors.                                                                               // 1681
                                                                                                                       // 1682
        var result10 = upsert(coll, useUpdate,                                                                         // 1683
                              {$or: [{name: 'David'}, {name: 'Emily'}]},                                               // 1684
                              {$set: {foo: 3}}, {multi: true});                                                        // 1685
        test.equal(result10.numberAffected, 2);                                                                        // 1686
        if (! skipIds)                                                                                                 // 1687
          test.isFalse(result10.insertedId);                                                                           // 1688
        compareResults(test, skipIds,                                                                                  // 1689
                       [coll.findOne({name: 'David'}), coll.findOne({name: 'Emily'})],                                 // 1690
                       [{name: 'David', foo: 3, bar: 7, _id: davidId},                                                 // 1691
                        {name: 'Emily', foo: 3, bar: 7, _id: emilyId}]                                                 // 1692
                      );                                                                                               // 1693
                                                                                                                       // 1694
        var result11 = upsert(                                                                                         // 1695
          coll, useUpdate,                                                                                             // 1696
          {                                                                                                            // 1697
            name: 'Charlie',                                                                                           // 1698
            $or: [{ foo: 2}, { bar: 7 }]                                                                               // 1699
          },                                                                                                           // 1700
          { $set: { foo: 3 } }                                                                                         // 1701
        );                                                                                                             // 1702
        test.equal(result11.numberAffected, 1);                                                                        // 1703
        if (! skipIds)                                                                                                 // 1704
          test.isTrue(result11.insertedId);                                                                            // 1705
        var charlieId = result11.insertedId;                                                                           // 1706
        compareResults(test, skipIds,                                                                                  // 1707
                       coll.find({ name: 'Charlie' }).fetch(),                                                         // 1708
                       [{name: 'Charlie', foo: 3, _id: charlieId}]);                                                   // 1709
      });                                                                                                              // 1710
    });                                                                                                                // 1711
  });                                                                                                                  // 1712
});                                                                                                                    // 1713
                                                                                                                       // 1714
var asyncUpsertTestName = function (useNetwork, useDirectCollection,                                                   // 1715
                                    useUpdate, idGeneration) {                                                         // 1716
  return "mongo-livedata - async " +                                                                                   // 1717
    (useUpdate ? "update " : "") +                                                                                     // 1718
    "upsert " +                                                                                                        // 1719
    (useNetwork ? "over network " : "") +                                                                              // 1720
    (useDirectCollection ? ", direct collection " : "") +                                                              // 1721
    idGeneration;                                                                                                      // 1722
};                                                                                                                     // 1723
                                                                                                                       // 1724
// This is a duplicate of the test above, with some changes to make it work for                                        // 1725
// callback style. On the client, we test server-backed and in-memory                                                  // 1726
// collections, and run the tests for both the Meteor.Collection and the                                               // 1727
// LocalCollection. On the server, we test mongo-backed collections, for both                                          // 1728
// the Meteor.Collection and the MongoConnection.                                                                      // 1729
_.each(Meteor.isServer ? [false] : [true, false], function (useNetwork) {                                              // 1730
  _.each(useNetwork ? [false] : [true, false], function (useDirectCollection) {                                        // 1731
    _.each([true, false], function (useUpdate) {                                                                       // 1732
      Tinytest.addAsync(asyncUpsertTestName(useNetwork, useDirectCollection, useUpdate, idGeneration), function (test, onComplete) {
        var coll;                                                                                                      // 1734
        var run = test.runId();                                                                                        // 1735
        var collName = "livedata_upsert_collection_"+run+                                                              // 1736
              (useUpdate ? "_update_" : "") +                                                                          // 1737
              (useNetwork ? "_network_" : "") +                                                                        // 1738
              (useDirectCollection ? "_direct_" : "");                                                                 // 1739
        if (useNetwork) {                                                                                              // 1740
          Meteor.call("createInsecureCollection", collName, collectionOptions);                                        // 1741
          coll = new Meteor.Collection(collName, collectionOptions);                                                   // 1742
          Meteor.subscribe("c-" + collName);                                                                           // 1743
        } else {                                                                                                       // 1744
          var opts = _.clone(collectionOptions);                                                                       // 1745
          if (Meteor.isClient)                                                                                         // 1746
            opts.connection = null;                                                                                    // 1747
          coll = new Meteor.Collection(collName, opts);                                                                // 1748
          if (useDirectCollection)                                                                                     // 1749
            coll = coll._collection;                                                                                   // 1750
        }                                                                                                              // 1751
                                                                                                                       // 1752
        var result1;                                                                                                   // 1753
        var next1 = function (err, result) {                                                                           // 1754
          result1 = result;                                                                                            // 1755
          test.equal(result1.numberAffected, 1);                                                                       // 1756
          if (! useUpdate) {                                                                                           // 1757
            test.isTrue(result1.insertedId);                                                                           // 1758
            test.equal(result1.insertedId, 'foo');                                                                     // 1759
          }                                                                                                            // 1760
          compareResults(test, useUpdate, coll.find().fetch(), [{foo: 'bar', _id: 'foo'}]);                            // 1761
          upsert(coll, useUpdate, {_id: 'foo'}, {foo: 'baz'}, next2);                                                  // 1762
        };                                                                                                             // 1763
                                                                                                                       // 1764
        // Test starts here.                                                                                           // 1765
        upsert(coll, useUpdate, {_id: 'foo'}, {_id: 'foo', foo: 'bar'}, next1);                                        // 1766
                                                                                                                       // 1767
        var t1, t2, result2;                                                                                           // 1768
        var next2 = function (err, result) {                                                                           // 1769
          result2 = result;                                                                                            // 1770
          test.equal(result2.numberAffected, 1);                                                                       // 1771
          if (! useUpdate)                                                                                             // 1772
            test.isFalse(result2.insertedId);                                                                          // 1773
          compareResults(test, useUpdate, coll.find().fetch(), [{foo: 'baz', _id: result1.insertedId}]);               // 1774
          coll.remove({_id: 'foo'});                                                                                   // 1775
          compareResults(test, useUpdate, coll.find().fetch(), []);                                                    // 1776
                                                                                                                       // 1777
          // Test values that require transformation to go into Mongo:                                                 // 1778
                                                                                                                       // 1779
          t1 = new Meteor.Collection.ObjectID();                                                                       // 1780
          t2 = new Meteor.Collection.ObjectID();                                                                       // 1781
          upsert(coll, useUpdate, {_id: t1}, {_id: t1, foo: 'bar'}, next3);                                            // 1782
        };                                                                                                             // 1783
                                                                                                                       // 1784
        var result3;                                                                                                   // 1785
        var next3 = function (err, result) {                                                                           // 1786
          result3 = result;                                                                                            // 1787
          test.equal(result3.numberAffected, 1);                                                                       // 1788
          if (! useUpdate) {                                                                                           // 1789
            test.isTrue(result3.insertedId);                                                                           // 1790
            test.equal(t1, result3.insertedId);                                                                        // 1791
          }                                                                                                            // 1792
          compareResults(test, useUpdate, coll.find().fetch(), [{_id: t1, foo: 'bar'}]);                               // 1793
                                                                                                                       // 1794
          upsert(coll, useUpdate, {_id: t1}, {foo: t2}, next4);                                                        // 1795
        };                                                                                                             // 1796
                                                                                                                       // 1797
        var next4 = function (err, result4) {                                                                          // 1798
          test.equal(result2.numberAffected, 1);                                                                       // 1799
          if (! useUpdate)                                                                                             // 1800
            test.isFalse(result2.insertedId);                                                                          // 1801
          compareResults(test, useUpdate, coll.find().fetch(), [{foo: t2, _id: result3.insertedId}]);                  // 1802
                                                                                                                       // 1803
          coll.remove({_id: t1});                                                                                      // 1804
                                                                                                                       // 1805
          // Test modification by upsert                                                                               // 1806
          upsert(coll, useUpdate, {_id: 'David'}, {$set: {foo: 1}}, next5);                                            // 1807
        };                                                                                                             // 1808
                                                                                                                       // 1809
        var result5;                                                                                                   // 1810
        var next5 = function (err, result) {                                                                           // 1811
          result5 = result;                                                                                            // 1812
          test.equal(result5.numberAffected, 1);                                                                       // 1813
          if (! useUpdate) {                                                                                           // 1814
            test.isTrue(result5.insertedId);                                                                           // 1815
            test.equal(result5.insertedId, 'David');                                                                   // 1816
          }                                                                                                            // 1817
          var davidId = result5.insertedId;                                                                            // 1818
          compareResults(test, useUpdate, coll.find().fetch(), [{foo: 1, _id: davidId}]);                              // 1819
                                                                                                                       // 1820
          if (! Meteor.isClient && useDirectCollection) {                                                              // 1821
            // test that bad modifier fails                                                                            // 1822
            // The stub throws an exception about the invalid modifier, which                                          // 1823
            // livedata logs (so we suppress it).                                                                      // 1824
            Meteor._suppress_log(1);                                                                                   // 1825
            upsert(coll, useUpdate, {_id: 'David'}, {$blah: {foo: 2}}, function (err) {                                // 1826
              if (! (Meteor.isClient && useDirectCollection))                                                          // 1827
                test.isTrue(err);                                                                                      // 1828
              upsert(coll, useUpdate, {_id: 'David'}, {$set: {foo: 2}}, next6);                                        // 1829
            });                                                                                                        // 1830
          } else {                                                                                                     // 1831
            // XXX skip this test for now for LocalCollection; the fact that                                           // 1832
            // we're in a nested sequence of callbacks means we're inside a                                            // 1833
            // Meteor.defer, which means the exception just gets                                                       // 1834
            // logged. Something should be done about this at some point?  Maybe                                       // 1835
            // LocalCollection callbacks don't really have to be deferred.                                             // 1836
            upsert(coll, useUpdate, {_id: 'David'}, {$set: {foo: 2}}, next6);                                          // 1837
          }                                                                                                            // 1838
        };                                                                                                             // 1839
                                                                                                                       // 1840
        var result6;                                                                                                   // 1841
        var next6 = function (err, result) {                                                                           // 1842
          result6 = result;                                                                                            // 1843
          test.equal(result6.numberAffected, 1);                                                                       // 1844
          if (! useUpdate)                                                                                             // 1845
            test.isFalse(result6.insertedId);                                                                          // 1846
          compareResults(test, useUpdate, coll.find().fetch(), [{_id: 'David', foo: 2}]);                              // 1847
                                                                                                                       // 1848
          var emilyId = coll.insert({_id: 'Emily', foo: 2});                                                           // 1849
          compareResults(test, useUpdate, coll.find().fetch(), [{_id: 'David', foo: 2},                                // 1850
                                                                {_id: 'Emily', foo: 2}]);                              // 1851
                                                                                                                       // 1852
          // multi update by upsert.                                                                                   // 1853
          // We can't actually update multiple documents since we have to do it by                                     // 1854
          // id, but at least make sure the multi flag doesn't mess anything up.                                       // 1855
          upsert(coll, useUpdate, {_id: 'Emily'},                                                                      // 1856
                 {$set: {bar: 7},                                                                                      // 1857
                  $setOnInsert: {name: 'Fred', foo: 2}},                                                               // 1858
                 {multi: true}, next7);                                                                                // 1859
        };                                                                                                             // 1860
                                                                                                                       // 1861
        var result7;                                                                                                   // 1862
        var next7 = function (err, result) {                                                                           // 1863
          result7 = result;                                                                                            // 1864
          test.equal(result7.numberAffected, 1);                                                                       // 1865
          if (! useUpdate)                                                                                             // 1866
            test.isFalse(result7.insertedId);                                                                          // 1867
          compareResults(test, useUpdate, coll.find().fetch(), [{_id: 'David', foo: 2},                                // 1868
                                                                {_id: 'Emily', foo: 2, bar: 7}]);                      // 1869
                                                                                                                       // 1870
          // insert by multi upsert                                                                                    // 1871
          upsert(coll, useUpdate, {_id: 'Fred'},                                                                       // 1872
                 {$set: {bar: 7},                                                                                      // 1873
                  $setOnInsert: {name: 'Fred', foo: 2}},                                                               // 1874
                 {multi: true}, next8);                                                                                // 1875
                                                                                                                       // 1876
        };                                                                                                             // 1877
                                                                                                                       // 1878
        var result8;                                                                                                   // 1879
        var next8 = function (err, result) {                                                                           // 1880
          result8 = result;                                                                                            // 1881
                                                                                                                       // 1882
          test.equal(result8.numberAffected, 1);                                                                       // 1883
          if (! useUpdate) {                                                                                           // 1884
            test.isTrue(result8.insertedId);                                                                           // 1885
            test.equal(result8.insertedId, 'Fred');                                                                    // 1886
          }                                                                                                            // 1887
          var fredId = result8.insertedId;                                                                             // 1888
          compareResults(test, useUpdate,  coll.find().fetch(),                                                        // 1889
                         [{_id: 'David', foo: 2},                                                                      // 1890
                          {_id: 'Emily', foo: 2, bar: 7},                                                              // 1891
                          {name: 'Fred', foo: 2, bar: 7, _id: fredId}]);                                               // 1892
          onComplete();                                                                                                // 1893
        };                                                                                                             // 1894
      });                                                                                                              // 1895
    });                                                                                                                // 1896
  });                                                                                                                  // 1897
});                                                                                                                    // 1898
                                                                                                                       // 1899
if (Meteor.isClient) {                                                                                                 // 1900
  Tinytest.addAsync("mongo-livedata - async update/remove return values over network " + idGeneration, function (test, onComplete) {
    var coll;                                                                                                          // 1902
    var run = test.runId();                                                                                            // 1903
    var collName = "livedata_upsert_collection_"+run;                                                                  // 1904
    Meteor.call("createInsecureCollection", collName, collectionOptions);                                              // 1905
    coll = new Meteor.Collection(collName, collectionOptions);                                                         // 1906
    Meteor.subscribe("c-" + collName);                                                                                 // 1907
                                                                                                                       // 1908
    coll.insert({ _id: "foo" });                                                                                       // 1909
    coll.insert({ _id: "bar" });                                                                                       // 1910
    coll.update({ _id: "foo" }, { $set: { foo: 1 } }, { multi: true }, function (err, result) {                        // 1911
      test.isFalse(err);                                                                                               // 1912
      test.equal(result, 1);                                                                                           // 1913
      coll.update({ _id: "foo" }, { _id: "foo", foo: 2 }, function (err, result) {                                     // 1914
        test.isFalse(err);                                                                                             // 1915
        test.equal(result, 1);                                                                                         // 1916
        coll.update({ _id: "baz" }, { $set: { foo: 1 } }, function (err, result) {                                     // 1917
          test.isFalse(err);                                                                                           // 1918
          test.equal(result, 0);                                                                                       // 1919
          coll.remove({ _id: "foo" }, function (err, result) {                                                         // 1920
            test.equal(result, 1);                                                                                     // 1921
            coll.remove({ _id: "baz" }, function (err, result) {                                                       // 1922
              test.equal(result, 0);                                                                                   // 1923
              onComplete();                                                                                            // 1924
            });                                                                                                        // 1925
          });                                                                                                          // 1926
        });                                                                                                            // 1927
      });                                                                                                              // 1928
    });                                                                                                                // 1929
  });                                                                                                                  // 1930
}                                                                                                                      // 1931
                                                                                                                       // 1932
// Runs a method and its stub which do some upserts. The method throws an error                                        // 1933
// if we don't get the right return values.                                                                            // 1934
if (Meteor.isClient) {                                                                                                 // 1935
  _.each([true, false], function (useUpdate) {                                                                         // 1936
    Tinytest.addAsync("mongo-livedata - " + (useUpdate ? "update " : "") + "upsert in method, " + idGeneration, function (test, onComplete) {
      var run = test.runId();                                                                                          // 1938
      upsertTestMethodColl = new Meteor.Collection(upsertTestMethod + "_collection_" + run, collectionOptions);        // 1939
      var m = {};                                                                                                      // 1940
      delete Meteor.connection._methodHandlers[upsertTestMethod];                                                      // 1941
      m[upsertTestMethod] = function (run, useUpdate, options) {                                                       // 1942
        upsertTestMethodImpl(upsertTestMethodColl, useUpdate, test);                                                   // 1943
      };                                                                                                               // 1944
      Meteor.methods(m);                                                                                               // 1945
      Meteor.call(upsertTestMethod, run, useUpdate, collectionOptions, function (err, result) {                        // 1946
        test.isFalse(err);                                                                                             // 1947
        onComplete();                                                                                                  // 1948
      });                                                                                                              // 1949
    });                                                                                                                // 1950
  });                                                                                                                  // 1951
}                                                                                                                      // 1952
                                                                                                                       // 1953
_.each(Meteor.isServer ? [true, false] : [true], function (minimongo) {                                                // 1954
  _.each([true, false], function (useUpdate) {                                                                         // 1955
    Tinytest.add("mongo-livedata - " + (useUpdate ? "update " : "") + "upsert by id" + (minimongo ? " minimongo" : "") + ", " + idGeneration, function (test) {
      var run = test.runId();                                                                                          // 1957
      var options = collectionOptions;                                                                                 // 1958
      if (minimongo)                                                                                                   // 1959
        options = _.extend({}, collectionOptions, { connection: null });                                               // 1960
      var coll = new Meteor.Collection("livedata_upsert_by_id_collection_"+run, options);                              // 1961
                                                                                                                       // 1962
      var ret;                                                                                                         // 1963
      ret = upsert(coll, useUpdate, {_id: 'foo'}, {$set: {x: 1}});                                                     // 1964
      test.equal(ret.numberAffected, 1);                                                                               // 1965
      if (! useUpdate)                                                                                                 // 1966
        test.equal(ret.insertedId, 'foo');                                                                             // 1967
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 1968
                     [{_id: 'foo', x: 1}]);                                                                            // 1969
                                                                                                                       // 1970
      ret = upsert(coll, useUpdate, {_id: 'foo'}, {$set: {x: 2}});                                                     // 1971
      test.equal(ret.numberAffected, 1);                                                                               // 1972
      if (! useUpdate)                                                                                                 // 1973
        test.isFalse(ret.insertedId);                                                                                  // 1974
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 1975
                     [{_id: 'foo', x: 2}]);                                                                            // 1976
                                                                                                                       // 1977
      ret = upsert(coll, useUpdate, {_id: 'bar'}, {$set: {x: 1}});                                                     // 1978
      test.equal(ret.numberAffected, 1);                                                                               // 1979
      if (! useUpdate)                                                                                                 // 1980
        test.equal(ret.insertedId, 'bar');                                                                             // 1981
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 1982
                     [{_id: 'foo', x: 2},                                                                              // 1983
                      {_id: 'bar', x: 1}]);                                                                            // 1984
                                                                                                                       // 1985
      coll.remove({});                                                                                                 // 1986
                                                                                                                       // 1987
      ret = upsert(coll, useUpdate, {_id: 'traz'}, {x: 1});                                                            // 1988
      test.equal(ret.numberAffected, 1);                                                                               // 1989
      var myId = ret.insertedId;                                                                                       // 1990
      if (! useUpdate) {                                                                                               // 1991
        test.isTrue(myId);                                                                                             // 1992
        // upsert with entire document does NOT take _id from                                                          // 1993
        // the query.                                                                                                  // 1994
        test.notEqual(myId, 'traz');                                                                                   // 1995
      } else {                                                                                                         // 1996
        myId = coll.findOne()._id;                                                                                     // 1997
      }                                                                                                                // 1998
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 1999
                     [{x: 1, _id: myId}]);                                                                             // 2000
                                                                                                                       // 2001
      // this time, insert as _id 'traz'                                                                               // 2002
      ret = upsert(coll, useUpdate, {_id: 'traz'}, {_id: 'traz', x: 2});                                               // 2003
      test.equal(ret.numberAffected, 1);                                                                               // 2004
      if (! useUpdate)                                                                                                 // 2005
        test.equal(ret.insertedId, 'traz');                                                                            // 2006
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 2007
                     [{x: 1, _id: myId},                                                                               // 2008
                      {x: 2, _id: 'traz'}]);                                                                           // 2009
                                                                                                                       // 2010
      // now update _id 'traz'                                                                                         // 2011
      ret = upsert(coll, useUpdate, {_id: 'traz'}, {x: 3});                                                            // 2012
      test.equal(ret.numberAffected, 1);                                                                               // 2013
      test.isFalse(ret.insertedId);                                                                                    // 2014
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 2015
                     [{x: 1, _id: myId},                                                                               // 2016
                      {x: 3, _id: 'traz'}]);                                                                           // 2017
                                                                                                                       // 2018
      // now update, passing _id (which is ok as long as it's the same)                                                // 2019
      ret = upsert(coll, useUpdate, {_id: 'traz'}, {_id: 'traz', x: 4});                                               // 2020
      test.equal(ret.numberAffected, 1);                                                                               // 2021
      test.isFalse(ret.insertedId);                                                                                    // 2022
      compareResults(test, useUpdate, coll.find().fetch(),                                                             // 2023
                     [{x: 1, _id: myId},                                                                               // 2024
                      {x: 4, _id: 'traz'}]);                                                                           // 2025
                                                                                                                       // 2026
    });                                                                                                                // 2027
  });                                                                                                                  // 2028
});                                                                                                                    // 2029
                                                                                                                       // 2030
});  // end idGeneration parametrization                                                                               // 2031
                                                                                                                       // 2032
Tinytest.add('mongo-livedata - rewrite selector', function (test) {                                                    // 2033
  test.equal(Meteor.Collection._rewriteSelector({x: /^o+B/im}),                                                        // 2034
             {x: {$regex: '^o+B', $options: 'im'}});                                                                   // 2035
  test.equal(Meteor.Collection._rewriteSelector({x: {$regex: /^o+B/im}}),                                              // 2036
             {x: {$regex: '^o+B', $options: 'im'}});                                                                   // 2037
  test.equal(Meteor.Collection._rewriteSelector({x: /^o+B/}),                                                          // 2038
             {x: {$regex: '^o+B'}});                                                                                   // 2039
  test.equal(Meteor.Collection._rewriteSelector({x: {$regex: /^o+B/}}),                                                // 2040
             {x: {$regex: '^o+B'}});                                                                                   // 2041
  test.equal(Meteor.Collection._rewriteSelector('foo'),                                                                // 2042
             {_id: 'foo'});                                                                                            // 2043
                                                                                                                       // 2044
  test.equal(                                                                                                          // 2045
    Meteor.Collection._rewriteSelector(                                                                                // 2046
      {'$or': [                                                                                                        // 2047
        {x: /^o/},                                                                                                     // 2048
        {y: /^p/},                                                                                                     // 2049
        {z: 'q'},                                                                                                      // 2050
        {w: {$regex: /^r/}}                                                                                            // 2051
      ]}                                                                                                               // 2052
    ),                                                                                                                 // 2053
    {'$or': [                                                                                                          // 2054
      {x: {$regex: '^o'}},                                                                                             // 2055
      {y: {$regex: '^p'}},                                                                                             // 2056
      {z: 'q'},                                                                                                        // 2057
      {w: {$regex: '^r'}}                                                                                              // 2058
    ]}                                                                                                                 // 2059
  );                                                                                                                   // 2060
                                                                                                                       // 2061
  test.equal(                                                                                                          // 2062
    Meteor.Collection._rewriteSelector(                                                                                // 2063
      {'$or': [                                                                                                        // 2064
        {'$and': [                                                                                                     // 2065
          {x: /^a/i},                                                                                                  // 2066
          {y: /^b/},                                                                                                   // 2067
          {z: {$regex: /^c/i}},                                                                                        // 2068
          {w: {$regex: '^[abc]', $options: 'i'}}, // make sure we don't break vanilla selectors                        // 2069
          {v: {$regex: /O/, $options: 'i'}}, // $options should override the ones on the RegExp object                 // 2070
          {u: {$regex: /O/m, $options: 'i'}} // $options should override the ones on the RegExp object                 // 2071
        ]},                                                                                                            // 2072
        {'$nor': [                                                                                                     // 2073
          {s: /^d/},                                                                                                   // 2074
          {t: /^e/i},                                                                                                  // 2075
          {u: {$regex: /^f/i}},                                                                                        // 2076
          // even empty string overrides built-in flags                                                                // 2077
          {v: {$regex: /^g/i, $options: ''}}                                                                           // 2078
        ]}                                                                                                             // 2079
      ]}                                                                                                               // 2080
    ),                                                                                                                 // 2081
    {'$or': [                                                                                                          // 2082
      {'$and': [                                                                                                       // 2083
        {x: {$regex: '^a', $options: 'i'}},                                                                            // 2084
        {y: {$regex: '^b'}},                                                                                           // 2085
        {z: {$regex: '^c', $options: 'i'}},                                                                            // 2086
        {w: {$regex: '^[abc]', $options: 'i'}},                                                                        // 2087
        {v: {$regex: 'O', $options: 'i'}},                                                                             // 2088
        {u: {$regex: 'O', $options: 'i'}}                                                                              // 2089
      ]},                                                                                                              // 2090
      {'$nor': [                                                                                                       // 2091
        {s: {$regex: '^d'}},                                                                                           // 2092
        {t: {$regex: '^e', $options: 'i'}},                                                                            // 2093
        {u: {$regex: '^f', $options: 'i'}},                                                                            // 2094
        {v: {$regex: '^g', $options: ''}}                                                                              // 2095
      ]}                                                                                                               // 2096
    ]}                                                                                                                 // 2097
  );                                                                                                                   // 2098
                                                                                                                       // 2099
  var oid = new Meteor.Collection.ObjectID();                                                                          // 2100
  test.equal(Meteor.Collection._rewriteSelector(oid),                                                                  // 2101
             {_id: oid});                                                                                              // 2102
});                                                                                                                    // 2103
                                                                                                                       // 2104
testAsyncMulti('mongo-livedata - specified _id', [                                                                     // 2105
  function (test, expect) {                                                                                            // 2106
    var collectionName = Random.id();                                                                                  // 2107
    if (Meteor.isClient) {                                                                                             // 2108
      Meteor.call('createInsecureCollection', collectionName);                                                         // 2109
      Meteor.subscribe('c-' + collectionName);                                                                         // 2110
    }                                                                                                                  // 2111
    var expectError = expect(function (err, result) {                                                                  // 2112
      test.isTrue(err);                                                                                                // 2113
      var doc = coll.findOne();                                                                                        // 2114
      test.equal(doc.name, "foo");                                                                                     // 2115
    });                                                                                                                // 2116
    var coll = new Meteor.Collection(collectionName);                                                                  // 2117
    coll.insert({_id: "foo", name: "foo"}, expect(function (err1, id) {                                                // 2118
      test.equal(id, "foo");                                                                                           // 2119
      var doc = coll.findOne();                                                                                        // 2120
      test.equal(doc._id, "foo");                                                                                      // 2121
      Meteor._suppress_log(1);                                                                                         // 2122
      coll.insert({_id: "foo", name: "bar"}, expectError);                                                             // 2123
    }));                                                                                                               // 2124
  }                                                                                                                    // 2125
]);                                                                                                                    // 2126
                                                                                                                       // 2127
testAsyncMulti('mongo-livedata - empty string _id', [                                                                  // 2128
  function (test, expect) {                                                                                            // 2129
    var self = this;                                                                                                   // 2130
    self.collectionName = Random.id();                                                                                 // 2131
    if (Meteor.isClient) {                                                                                             // 2132
      Meteor.call('createInsecureCollection', self.collectionName);                                                    // 2133
      Meteor.subscribe('c-' + self.collectionName);                                                                    // 2134
    }                                                                                                                  // 2135
    self.coll = new Meteor.Collection(self.collectionName);                                                            // 2136
    try {                                                                                                              // 2137
      self.coll.insert({_id: "", f: "foo"});                                                                           // 2138
      test.fail("Insert with an empty _id should fail");                                                               // 2139
    } catch (e) {                                                                                                      // 2140
      // ok                                                                                                            // 2141
    }                                                                                                                  // 2142
    self.coll.insert({_id: "realid", f: "bar"}, expect(function (err, res) {                                           // 2143
      test.equal(res, "realid");                                                                                       // 2144
    }));                                                                                                               // 2145
  },                                                                                                                   // 2146
  function (test, expect) {                                                                                            // 2147
    var self = this;                                                                                                   // 2148
    var docs = self.coll.find().fetch();                                                                               // 2149
    test.equal(docs, [{_id: "realid", f: "bar"}]);                                                                     // 2150
  },                                                                                                                   // 2151
  function (test, expect) {                                                                                            // 2152
    var self = this;                                                                                                   // 2153
    if (Meteor.isServer) {                                                                                             // 2154
      self.coll._collection.insert({_id: "", f: "baz"});                                                               // 2155
      test.equal(self.coll.find().fetch().length, 2);                                                                  // 2156
    }                                                                                                                  // 2157
  }                                                                                                                    // 2158
]);                                                                                                                    // 2159
                                                                                                                       // 2160
                                                                                                                       // 2161
if (Meteor.isServer) {                                                                                                 // 2162
                                                                                                                       // 2163
  testAsyncMulti("mongo-livedata - minimongo on server to server connection", [                                        // 2164
    function (test, expect) {                                                                                          // 2165
      var self = this;                                                                                                 // 2166
      Meteor._debug("connection setup");                                                                               // 2167
      self.id = Random.id();                                                                                           // 2168
      var C = self.C = new Meteor.Collection("ServerMinimongo_" + self.id);                                            // 2169
      C.allow({                                                                                                        // 2170
        insert: function () {return true;},                                                                            // 2171
        update: function () {return true;},                                                                            // 2172
        remove: function () {return true;}                                                                             // 2173
      });                                                                                                              // 2174
      C.insert({a: 0, b: 1});                                                                                          // 2175
      C.insert({a: 0, b: 2});                                                                                          // 2176
      C.insert({a: 1, b: 3});                                                                                          // 2177
      Meteor.publish(self.id, function () {                                                                            // 2178
        return C.find({a: 0});                                                                                         // 2179
      });                                                                                                              // 2180
                                                                                                                       // 2181
      self.conn = DDP.connect(Meteor.absoluteUrl());                                                                   // 2182
      pollUntil(expect, function () {                                                                                  // 2183
        return self.conn.status().connected;                                                                           // 2184
      }, 10000);                                                                                                       // 2185
    },                                                                                                                 // 2186
                                                                                                                       // 2187
    function (test, expect) {                                                                                          // 2188
      var self = this;                                                                                                 // 2189
      if (self.conn.status().connected) {                                                                              // 2190
        self.miniC = new Meteor.Collection("ServerMinimongo_" + self.id, {                                             // 2191
          connection: self.conn                                                                                        // 2192
        });                                                                                                            // 2193
        var exp = expect(function (err) {                                                                              // 2194
          test.isFalse(err);                                                                                           // 2195
        });                                                                                                            // 2196
        self.conn.subscribe(self.id, {                                                                                 // 2197
          onError: exp,                                                                                                // 2198
          onReady: exp                                                                                                 // 2199
        });                                                                                                            // 2200
      }                                                                                                                // 2201
    },                                                                                                                 // 2202
                                                                                                                       // 2203
    function (test, expect) {                                                                                          // 2204
      var self = this;                                                                                                 // 2205
      if (self.miniC) {                                                                                                // 2206
        var contents = self.miniC.find().fetch();                                                                      // 2207
        test.equal(contents.length, 2);                                                                                // 2208
        test.equal(contents[0].a, 0);                                                                                  // 2209
      }                                                                                                                // 2210
    },                                                                                                                 // 2211
                                                                                                                       // 2212
    function (test, expect) {                                                                                          // 2213
      var self = this;                                                                                                 // 2214
      if (!self.miniC)                                                                                                 // 2215
        return;                                                                                                        // 2216
      self.miniC.insert({a:0, b:3});                                                                                   // 2217
      var contents = self.miniC.find({b:3}).fetch();                                                                   // 2218
      test.equal(contents.length, 1);                                                                                  // 2219
      test.equal(contents[0].a, 0);                                                                                    // 2220
    }                                                                                                                  // 2221
  ]);                                                                                                                  // 2222
                                                                                                                       // 2223
  testAsyncMulti("mongo-livedata - minimongo observe on server", [                                                     // 2224
    function (test, expect) {                                                                                          // 2225
      var self = this;                                                                                                 // 2226
      self.id = Random.id();                                                                                           // 2227
      self.C = new Meteor.Collection("ServerMinimongoObserve_" + self.id);                                             // 2228
      self.events = [];                                                                                                // 2229
                                                                                                                       // 2230
      Meteor.publish(self.id, function () {                                                                            // 2231
        return self.C.find();                                                                                          // 2232
      });                                                                                                              // 2233
                                                                                                                       // 2234
      self.conn = DDP.connect(Meteor.absoluteUrl());                                                                   // 2235
      pollUntil(expect, function () {                                                                                  // 2236
        return self.conn.status().connected;                                                                           // 2237
      }, 10000);                                                                                                       // 2238
    },                                                                                                                 // 2239
                                                                                                                       // 2240
    function (test, expect) {                                                                                          // 2241
      var self = this;                                                                                                 // 2242
      if (self.conn.status().connected) {                                                                              // 2243
        self.miniC = new Meteor.Collection("ServerMinimongoObserve_" + self.id, {                                      // 2244
          connection: self.conn                                                                                        // 2245
        });                                                                                                            // 2246
        var exp = expect(function (err) {                                                                              // 2247
          test.isFalse(err);                                                                                           // 2248
        });                                                                                                            // 2249
        self.conn.subscribe(self.id, {                                                                                 // 2250
          onError: exp,                                                                                                // 2251
          onReady: exp                                                                                                 // 2252
        });                                                                                                            // 2253
      }                                                                                                                // 2254
    },                                                                                                                 // 2255
                                                                                                                       // 2256
    function (test, expect) {                                                                                          // 2257
      var self = this;                                                                                                 // 2258
      if (self.miniC) {                                                                                                // 2259
        self.obs = self.miniC.find().observeChanges({                                                                  // 2260
          added: function (id, fields) {                                                                               // 2261
            self.events.push({evt: "a", id: id});                                                                      // 2262
            Meteor._sleepForMs(200);                                                                                   // 2263
            self.events.push({evt: "b", id: id});                                                                      // 2264
          }                                                                                                            // 2265
        });                                                                                                            // 2266
        self.one = self.C.insert({});                                                                                  // 2267
        self.two = self.C.insert({});                                                                                  // 2268
        pollUntil(expect, function () {                                                                                // 2269
          return self.events.length === 4;                                                                             // 2270
        }, 10000);                                                                                                     // 2271
      }                                                                                                                // 2272
    },                                                                                                                 // 2273
                                                                                                                       // 2274
    function (test, expect) {                                                                                          // 2275
      var self = this;                                                                                                 // 2276
      if (self.miniC) {                                                                                                // 2277
        test.equal(self.events, [                                                                                      // 2278
          {evt: "a", id: self.one},                                                                                    // 2279
          {evt: "b", id: self.one},                                                                                    // 2280
          {evt: "a", id: self.two},                                                                                    // 2281
          {evt: "b", id: self.two}                                                                                     // 2282
        ]);                                                                                                            // 2283
      }                                                                                                                // 2284
      self.obs && self.obs.stop();                                                                                     // 2285
    }                                                                                                                  // 2286
  ]);                                                                                                                  // 2287
}                                                                                                                      // 2288
                                                                                                                       // 2289
Tinytest.addAsync("mongo-livedata - local collections with different connections", function (test, onComplete) {       // 2290
  var cname = Random.id();                                                                                             // 2291
  var cname2 = Random.id();                                                                                            // 2292
  var coll1 = new Meteor.Collection(cname);                                                                            // 2293
  var doc = { foo: "bar" };                                                                                            // 2294
  var coll2 = new Meteor.Collection(cname2, { connection: null });                                                     // 2295
  coll2.insert(doc, function (err, id) {                                                                               // 2296
    test.equal(coll1.find(doc).count(), 0);                                                                            // 2297
    test.equal(coll2.find(doc).count(), 1);                                                                            // 2298
    onComplete();                                                                                                      // 2299
  });                                                                                                                  // 2300
});                                                                                                                    // 2301
                                                                                                                       // 2302
Tinytest.addAsync("mongo-livedata - local collection with null connection, w/ callback", function (test, onComplete) { // 2303
  var cname = Random.id();                                                                                             // 2304
  var coll1 = new Meteor.Collection(cname, { connection: null });                                                      // 2305
  var doc = { foo: "bar" };                                                                                            // 2306
  var docId = coll1.insert(doc, function (err, id) {                                                                   // 2307
    test.equal(docId, id);                                                                                             // 2308
    test.equal(coll1.findOne(doc)._id, id);                                                                            // 2309
    onComplete();                                                                                                      // 2310
  });                                                                                                                  // 2311
});                                                                                                                    // 2312
                                                                                                                       // 2313
Tinytest.addAsync("mongo-livedata - local collection with null connection, w/o callback", function (test, onComplete) {
  var cname = Random.id();                                                                                             // 2315
  var coll1 = new Meteor.Collection(cname, { connection: null });                                                      // 2316
  var doc = { foo: "bar" };                                                                                            // 2317
  var docId = coll1.insert(doc);                                                                                       // 2318
  test.equal(coll1.findOne(doc)._id, docId);                                                                           // 2319
  onComplete();                                                                                                        // 2320
});                                                                                                                    // 2321
                                                                                                                       // 2322
testAsyncMulti("mongo-livedata - update handles $push with $each correctly", [                                         // 2323
  function (test, expect) {                                                                                            // 2324
    var self = this;                                                                                                   // 2325
    var collectionName = Random.id();                                                                                  // 2326
    if (Meteor.isClient) {                                                                                             // 2327
      Meteor.call('createInsecureCollection', collectionName);                                                         // 2328
      Meteor.subscribe('c-' + collectionName);                                                                         // 2329
    }                                                                                                                  // 2330
                                                                                                                       // 2331
    self.collection = new Meteor.Collection(collectionName);                                                           // 2332
                                                                                                                       // 2333
    self.id = self.collection.insert(                                                                                  // 2334
      {name: 'jens', elements: ['X', 'Y']}, expect(function (err, res) {                                               // 2335
        test.isFalse(err);                                                                                             // 2336
        test.equal(self.id, res);                                                                                      // 2337
        }));                                                                                                           // 2338
  },                                                                                                                   // 2339
  function (test, expect) {                                                                                            // 2340
    var self = this;                                                                                                   // 2341
    self.collection.update(self.id, {                                                                                  // 2342
      $push: {                                                                                                         // 2343
        elements: {                                                                                                    // 2344
          $each: ['A', 'B', 'C'],                                                                                      // 2345
          $slice: -4                                                                                                   // 2346
        }}}, expect(function (err, res) {                                                                              // 2347
          test.isFalse(err);                                                                                           // 2348
          test.equal(                                                                                                  // 2349
            self.collection.findOne(self.id),                                                                          // 2350
            {_id: self.id, name: 'jens', elements: ['Y', 'A', 'B', 'C']});                                             // 2351
        }));                                                                                                           // 2352
  }                                                                                                                    // 2353
]);                                                                                                                    // 2354
                                                                                                                       // 2355
if (Meteor.isServer) {                                                                                                 // 2356
  Tinytest.add("mongo-livedata - upsert handles $push with $each correctly", function (test) {                         // 2357
    var collection = new Meteor.Collection(Random.id());                                                               // 2358
                                                                                                                       // 2359
    var result = collection.upsert(                                                                                    // 2360
      {name: 'jens'},                                                                                                  // 2361
      {$push: {                                                                                                        // 2362
        elements: {                                                                                                    // 2363
          $each: ['A', 'B', 'C'],                                                                                      // 2364
          $slice: -4                                                                                                   // 2365
        }}});                                                                                                          // 2366
                                                                                                                       // 2367
    test.equal(collection.findOne(result.insertedId),                                                                  // 2368
               {_id: result.insertedId,                                                                                // 2369
                name: 'jens',                                                                                          // 2370
                elements: ['A', 'B', 'C']});                                                                           // 2371
                                                                                                                       // 2372
    var id = collection.insert({name: "david", elements: ['X', 'Y']});                                                 // 2373
    result = collection.upsert(                                                                                        // 2374
      {name: 'david'},                                                                                                 // 2375
      {$push: {                                                                                                        // 2376
        elements: {                                                                                                    // 2377
          $each: ['A', 'B', 'C'],                                                                                      // 2378
          $slice: -4                                                                                                   // 2379
        }}});                                                                                                          // 2380
                                                                                                                       // 2381
    test.equal(collection.findOne(id),                                                                                 // 2382
               {_id: id,                                                                                               // 2383
                name: 'david',                                                                                         // 2384
                elements: ['Y', 'A', 'B', 'C']});                                                                      // 2385
  });                                                                                                                  // 2386
}                                                                                                                      // 2387
                                                                                                                       // 2388
// This is a VERY white-box test.                                                                                      // 2389
Meteor.isServer && Tinytest.add("mongo-livedata - oplog - _disableOplog", function (test) {                            // 2390
  var collName = Random.id();                                                                                          // 2391
  var coll = new Meteor.Collection(collName);                                                                          // 2392
  if (MongoInternals.defaultRemoteCollectionDriver().mongo._oplogHandle) {                                             // 2393
    var observeWithOplog = coll.find({x: 5})                                                                           // 2394
          .observeChanges({added: function () {}});                                                                    // 2395
    test.isTrue(observeWithOplog._multiplexer._observeDriver._usesOplog);                                              // 2396
    observeWithOplog.stop();                                                                                           // 2397
  }                                                                                                                    // 2398
  var observeWithoutOplog = coll.find({x: 6}, {_disableOplog: true})                                                   // 2399
        .observeChanges({added: function () {}});                                                                      // 2400
  test.isFalse(observeWithoutOplog._multiplexer._observeDriver._usesOplog);                                            // 2401
  observeWithoutOplog.stop();                                                                                          // 2402
});                                                                                                                    // 2403
                                                                                                                       // 2404
Meteor.isServer && Tinytest.add("mongo-livedata - oplog - include selector fields", function (test) {                  // 2405
  var collName = "includeSelector" + Random.id();                                                                      // 2406
  var coll = new Meteor.Collection(collName);                                                                          // 2407
                                                                                                                       // 2408
  var docId = coll.insert({a: 1, b: [3, 2], c: 'foo'});                                                                // 2409
  test.isTrue(docId);                                                                                                  // 2410
                                                                                                                       // 2411
  // Wait until we've processed the insert oplog entry. (If the insert shows up                                        // 2412
  // during the observeChanges, the bug in question is not consistently                                                // 2413
  // reproduced.) We don't have to do this for polling observe (eg                                                     // 2414
  // --disable-oplog).                                                                                                 // 2415
  waitUntilOplogCaughtUp();                                                                                            // 2416
                                                                                                                       // 2417
  var output = [];                                                                                                     // 2418
  var handle = coll.find({a: 1, b: 2}, {fields: {c: 1}}).observeChanges({                                              // 2419
    added: function (id, fields) {                                                                                     // 2420
      output.push(['added', id, fields]);                                                                              // 2421
    },                                                                                                                 // 2422
    changed: function (id, fields) {                                                                                   // 2423
      output.push(['changed', id, fields]);                                                                            // 2424
    },                                                                                                                 // 2425
    removed: function (id) {                                                                                           // 2426
      output.push(['removed', id]);                                                                                    // 2427
    }                                                                                                                  // 2428
  });                                                                                                                  // 2429
  // Initially should match the document.                                                                              // 2430
  test.length(output, 1);                                                                                              // 2431
  test.equal(output.shift(), ['added', docId, {c: 'foo'}]);                                                            // 2432
                                                                                                                       // 2433
  // Update in such a way that, if we only knew about the published field 'c'                                          // 2434
  // and the changed field 'b' (but not the field 'a'), we would think it didn't                                       // 2435
  // match any more.  (This is a regression test for a bug that existed because                                        // 2436
  // we used to not use the shared projection in the initial query.)                                                   // 2437
  runInFence(function () {                                                                                             // 2438
    coll.update(docId, {$set: {'b.0': 2, c: 'bar'}});                                                                  // 2439
  });                                                                                                                  // 2440
  test.length(output, 1);                                                                                              // 2441
  test.equal(output.shift(), ['changed', docId, {c: 'bar'}]);                                                          // 2442
                                                                                                                       // 2443
  handle.stop();                                                                                                       // 2444
});                                                                                                                    // 2445
                                                                                                                       // 2446
Meteor.isServer && Tinytest.add("mongo-livedata - oplog - transform", function (test) {                                // 2447
  var collName = "oplogTransform" + Random.id();                                                                       // 2448
  var coll = new Meteor.Collection(collName);                                                                          // 2449
                                                                                                                       // 2450
  var docId = coll.insert({a: 25, x: {x: 5, y: 9}});                                                                   // 2451
  test.isTrue(docId);                                                                                                  // 2452
                                                                                                                       // 2453
  // Wait until we've processed the insert oplog entry. (If the insert shows up                                        // 2454
  // during the observeChanges, the bug in question is not consistently                                                // 2455
  // reproduced.) We don't have to do this for polling observe (eg                                                     // 2456
  // --disable-oplog).                                                                                                 // 2457
  waitUntilOplogCaughtUp();                                                                                            // 2458
                                                                                                                       // 2459
  var cursor = coll.find({}, {transform: function (doc) {                                                              // 2460
    return doc.x;                                                                                                      // 2461
  }});                                                                                                                 // 2462
                                                                                                                       // 2463
  var changesOutput = [];                                                                                              // 2464
  var changesHandle = cursor.observeChanges({                                                                          // 2465
    added: function (id, fields) {                                                                                     // 2466
      changesOutput.push(['added', fields]);                                                                           // 2467
    }                                                                                                                  // 2468
  });                                                                                                                  // 2469
  // We should get untransformed fields via observeChanges.                                                            // 2470
  test.length(changesOutput, 1);                                                                                       // 2471
  test.equal(changesOutput.shift(), ['added', {a: 25, x: {x: 5, y: 9}}]);                                              // 2472
  changesHandle.stop();                                                                                                // 2473
                                                                                                                       // 2474
  var transformedOutput = [];                                                                                          // 2475
  var transformedHandle = cursor.observe({                                                                             // 2476
    added: function (doc) {                                                                                            // 2477
      transformedOutput.push(['added', doc]);                                                                          // 2478
    }                                                                                                                  // 2479
  });                                                                                                                  // 2480
  test.length(transformedOutput, 1);                                                                                   // 2481
  test.equal(transformedOutput.shift(), ['added', {x: 5, y: 9}]);                                                      // 2482
  transformedHandle.stop();                                                                                            // 2483
});                                                                                                                    // 2484
                                                                                                                       // 2485
                                                                                                                       // 2486
Meteor.isServer && Tinytest.add("mongo-livedata - oplog - drop collection", function (test) {                          // 2487
  var collName = "dropCollection" + Random.id();                                                                       // 2488
  var coll = new Meteor.Collection(collName);                                                                          // 2489
                                                                                                                       // 2490
  var doc1Id = coll.insert({a: 'foo', c: 1});                                                                          // 2491
  var doc2Id = coll.insert({b: 'bar'});                                                                                // 2492
  var doc3Id = coll.insert({a: 'foo', c: 2});                                                                          // 2493
  var tmp;                                                                                                             // 2494
                                                                                                                       // 2495
  var output = [];                                                                                                     // 2496
  var handle = coll.find({a: 'foo'}).observeChanges({                                                                  // 2497
    added: function (id, fields) {                                                                                     // 2498
      output.push(['added', id, fields]);                                                                              // 2499
    },                                                                                                                 // 2500
    changed: function (id) {                                                                                           // 2501
      output.push(['changed']);                                                                                        // 2502
    },                                                                                                                 // 2503
    removed: function (id) {                                                                                           // 2504
      output.push(['removed', id]);                                                                                    // 2505
    }                                                                                                                  // 2506
  });                                                                                                                  // 2507
  test.length(output, 2);                                                                                              // 2508
  // make order consistent                                                                                             // 2509
  if (output.length === 2 && output[0][1] === doc3Id) {                                                                // 2510
    tmp = output[0];                                                                                                   // 2511
    output[0] = output[1];                                                                                             // 2512
    output[1] = tmp;                                                                                                   // 2513
  }                                                                                                                    // 2514
  test.equal(output.shift(), ['added', doc1Id, {a: 'foo', c: 1}]);                                                     // 2515
  test.equal(output.shift(), ['added', doc3Id, {a: 'foo', c: 2}]);                                                     // 2516
                                                                                                                       // 2517
  // Wait until we've processed the insert oplog entry, so that we are in a                                            // 2518
  // steady state (and we don't see the dropped docs because we are FETCHING).                                         // 2519
  waitUntilOplogCaughtUp();                                                                                            // 2520
                                                                                                                       // 2521
  // Drop the collection. Should remove all docs.                                                                      // 2522
  runInFence(function () {                                                                                             // 2523
    coll._dropCollection();                                                                                            // 2524
  });                                                                                                                  // 2525
                                                                                                                       // 2526
  test.length(output, 2);                                                                                              // 2527
  // make order consistent                                                                                             // 2528
  if (output.length === 2 && output[0][1] === doc3Id) {                                                                // 2529
    tmp = output[0];                                                                                                   // 2530
    output[0] = output[1];                                                                                             // 2531
    output[1] = tmp;                                                                                                   // 2532
  }                                                                                                                    // 2533
  test.equal(output.shift(), ['removed', doc1Id]);                                                                     // 2534
  test.equal(output.shift(), ['removed', doc3Id]);                                                                     // 2535
                                                                                                                       // 2536
  // Put something back in.                                                                                            // 2537
  var doc4Id;                                                                                                          // 2538
  runInFence(function () {                                                                                             // 2539
    doc4Id = coll.insert({a: 'foo', c: 3});                                                                            // 2540
  });                                                                                                                  // 2541
                                                                                                                       // 2542
  test.length(output, 1);                                                                                              // 2543
  test.equal(output.shift(), ['added', doc4Id, {a: 'foo', c: 3}]);                                                     // 2544
                                                                                                                       // 2545
  handle.stop();                                                                                                       // 2546
});                                                                                                                    // 2547
                                                                                                                       // 2548
var TestCustomType = function (head, tail) {                                                                           // 2549
  // use different field names on the object than in JSON, to ensure we are                                            // 2550
  // actually treating this as an opaque object.                                                                       // 2551
  this.myHead = head;                                                                                                  // 2552
  this.myTail = tail;                                                                                                  // 2553
};                                                                                                                     // 2554
_.extend(TestCustomType.prototype, {                                                                                   // 2555
  clone: function () {                                                                                                 // 2556
    return new TestCustomType(this.myHead, this.myTail);                                                               // 2557
  },                                                                                                                   // 2558
  equals: function (other) {                                                                                           // 2559
    return other instanceof TestCustomType                                                                             // 2560
      && EJSON.equals(this.myHead, other.myHead)                                                                       // 2561
      && EJSON.equals(this.myTail, other.myTail);                                                                      // 2562
  },                                                                                                                   // 2563
  typeName: function () {                                                                                              // 2564
    return 'someCustomType';                                                                                           // 2565
  },                                                                                                                   // 2566
  toJSONValue: function () {                                                                                           // 2567
    return {head: this.myHead, tail: this.myTail};                                                                     // 2568
  }                                                                                                                    // 2569
});                                                                                                                    // 2570
                                                                                                                       // 2571
EJSON.addType('someCustomType', function (json) {                                                                      // 2572
  return new TestCustomType(json.head, json.tail);                                                                     // 2573
});                                                                                                                    // 2574
                                                                                                                       // 2575
testAsyncMulti("mongo-livedata - oplog - update EJSON", [                                                              // 2576
  function (test, expect) {                                                                                            // 2577
    var self = this;                                                                                                   // 2578
    var collectionName = "ejson" + Random.id();                                                                        // 2579
    if (Meteor.isClient) {                                                                                             // 2580
      Meteor.call('createInsecureCollection', collectionName);                                                         // 2581
      Meteor.subscribe('c-' + collectionName);                                                                         // 2582
    }                                                                                                                  // 2583
                                                                                                                       // 2584
    self.collection = new Meteor.Collection(collectionName);                                                           // 2585
    self.date = new Date;                                                                                              // 2586
    self.objId = new Meteor.Collection.ObjectID;                                                                       // 2587
                                                                                                                       // 2588
    self.id = self.collection.insert(                                                                                  // 2589
      {d: self.date, oi: self.objId,                                                                                   // 2590
       custom: new TestCustomType('a', 'b')},                                                                          // 2591
      expect(function (err, res) {                                                                                     // 2592
        test.isFalse(err);                                                                                             // 2593
        test.equal(self.id, res);                                                                                      // 2594
      }));                                                                                                             // 2595
  },                                                                                                                   // 2596
  function (test, expect) {                                                                                            // 2597
    var self = this;                                                                                                   // 2598
    self.changes = [];                                                                                                 // 2599
    self.handle = self.collection.find({}).observeChanges({                                                            // 2600
      added: function (id, fields) {                                                                                   // 2601
        self.changes.push(['a', id, fields]);                                                                          // 2602
      },                                                                                                               // 2603
      changed: function (id, fields) {                                                                                 // 2604
        self.changes.push(['c', id, fields]);                                                                          // 2605
      },                                                                                                               // 2606
      removed: function (id) {                                                                                         // 2607
        self.changes.push(['r', id]);                                                                                  // 2608
      }                                                                                                                // 2609
    });                                                                                                                // 2610
    test.length(self.changes, 1);                                                                                      // 2611
    test.equal(self.changes.shift(),                                                                                   // 2612
               ['a', self.id,                                                                                          // 2613
                {d: self.date, oi: self.objId,                                                                         // 2614
                 custom: new TestCustomType('a', 'b')}]);                                                              // 2615
                                                                                                                       // 2616
    // First, replace the entire custom object.                                                                        // 2617
    // (runInFence is useful for the server, using expect() is useful for the                                          // 2618
    // client)                                                                                                         // 2619
    runInFence(function () {                                                                                           // 2620
      self.collection.update(                                                                                          // 2621
        self.id, {$set: {custom: new TestCustomType('a', 'c')}},                                                       // 2622
        expect(function (err) {                                                                                        // 2623
          test.isFalse(err);                                                                                           // 2624
        }));                                                                                                           // 2625
    });                                                                                                                // 2626
  },                                                                                                                   // 2627
  function (test, expect) {                                                                                            // 2628
    var self = this;                                                                                                   // 2629
    test.length(self.changes, 1);                                                                                      // 2630
    test.equal(self.changes.shift(),                                                                                   // 2631
               ['c', self.id, {custom: new TestCustomType('a', 'c')}]);                                                // 2632
                                                                                                                       // 2633
    // Now, sneakily replace just a piece of it. Meteor won't do this, but                                             // 2634
    // perhaps you are accessing Mongo directly.                                                                       // 2635
    runInFence(function () {                                                                                           // 2636
      self.collection.update(                                                                                          // 2637
        self.id, {$set: {'custom.EJSON$value.EJSONtail': 'd'}},                                                        // 2638
      expect(function (err) {                                                                                          // 2639
        test.isFalse(err);                                                                                             // 2640
      }));                                                                                                             // 2641
    });                                                                                                                // 2642
  },                                                                                                                   // 2643
  function (test, expect) {                                                                                            // 2644
    var self = this;                                                                                                   // 2645
    test.length(self.changes, 1);                                                                                      // 2646
    test.equal(self.changes.shift(),                                                                                   // 2647
               ['c', self.id, {custom: new TestCustomType('a', 'd')}]);                                                // 2648
                                                                                                                       // 2649
    // Update a date and an ObjectID too.                                                                              // 2650
    self.date2 = new Date(self.date.valueOf() + 1000);                                                                 // 2651
    self.objId2 = new Meteor.Collection.ObjectID;                                                                      // 2652
    runInFence(function () {                                                                                           // 2653
      self.collection.update(                                                                                          // 2654
        self.id, {$set: {d: self.date2, oi: self.objId2}},                                                             // 2655
      expect(function (err) {                                                                                          // 2656
        test.isFalse(err);                                                                                             // 2657
      }));                                                                                                             // 2658
    });                                                                                                                // 2659
  },                                                                                                                   // 2660
  function (test, expect) {                                                                                            // 2661
    var self = this;                                                                                                   // 2662
    test.length(self.changes, 1);                                                                                      // 2663
    test.equal(self.changes.shift(),                                                                                   // 2664
               ['c', self.id, {d: self.date2, oi: self.objId2}]);                                                      // 2665
                                                                                                                       // 2666
    self.handle.stop();                                                                                                // 2667
  }                                                                                                                    // 2668
]);                                                                                                                    // 2669
                                                                                                                       // 2670
                                                                                                                       // 2671
var waitUntilOplogCaughtUp = function () {                                                                             // 2672
  var oplogHandle =                                                                                                    // 2673
        MongoInternals.defaultRemoteCollectionDriver().mongo._oplogHandle;                                             // 2674
  if (oplogHandle)                                                                                                     // 2675
    oplogHandle.waitUntilCaughtUp();                                                                                   // 2676
};                                                                                                                     // 2677
                                                                                                                       // 2678
                                                                                                                       // 2679
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/allow_tests.js                                                                              //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
if (Meteor.isServer) {                                                                                                 // 1
  // Set up allow/deny rules for test collections                                                                      // 2
                                                                                                                       // 3
  var allowCollections = {};                                                                                           // 4
                                                                                                                       // 5
  // We create the collections in the publisher (instead of using a method or                                          // 6
  // something) because if we made them with a method, we'd need to follow the                                         // 7
  // method with some subscribes, and it's possible that the method call would                                         // 8
  // be delayed by a wait method and the subscribe messages would be sent before                                       // 9
  // it and fail due to the collection not yet existing. So we are very hacky                                          // 10
  // and use a publish.                                                                                                // 11
  Meteor.publish("allowTests", function (nonce, idGeneration) {                                                        // 12
    check(nonce, String);                                                                                              // 13
    check(idGeneration, String);                                                                                       // 14
    var cursors = [];                                                                                                  // 15
    var needToConfigure = undefined;                                                                                   // 16
                                                                                                                       // 17
    // helper for defining a collection. we are careful to create just one                                             // 18
    // Meteor.Collection even if the sub body is rerun, by caching them.                                               // 19
    var defineCollection = function(name, insecure, transform) {                                                       // 20
      var fullName = name + idGeneration + nonce;                                                                      // 21
                                                                                                                       // 22
      var collection;                                                                                                  // 23
      if (_.has(allowCollections, fullName)) {                                                                         // 24
        collection = allowCollections[fullName];                                                                       // 25
        if (needToConfigure === true)                                                                                  // 26
          throw new Error("collections inconsistently exist");                                                         // 27
        needToConfigure = false;                                                                                       // 28
      } else {                                                                                                         // 29
        collection = new Meteor.Collection(                                                                            // 30
          fullName, {idGeneration: idGeneration, transform: transform});                                               // 31
        allowCollections[fullName] = collection;                                                                       // 32
        if (needToConfigure === false)                                                                                 // 33
          throw new Error("collections inconsistently don't exist");                                                   // 34
        needToConfigure = true;                                                                                        // 35
        collection._insecure = insecure;                                                                               // 36
        var m = {};                                                                                                    // 37
        m["clear-collection-" + fullName] = function() {                                                               // 38
          collection.remove({});                                                                                       // 39
        };                                                                                                             // 40
        Meteor.methods(m);                                                                                             // 41
      }                                                                                                                // 42
                                                                                                                       // 43
      cursors.push(collection.find());                                                                                 // 44
      return collection;                                                                                               // 45
    };                                                                                                                 // 46
                                                                                                                       // 47
    var insecureCollection = defineCollection(                                                                         // 48
      "collection-insecure", true /*insecure*/);                                                                       // 49
    // totally locked down collection                                                                                  // 50
    var lockedDownCollection = defineCollection(                                                                       // 51
      "collection-locked-down", false /*insecure*/);                                                                   // 52
    // resticted collection with same allowed modifications, both with and                                             // 53
    // without the `insecure` package                                                                                  // 54
    var restrictedCollectionDefaultSecure = defineCollection(                                                          // 55
      "collection-restrictedDefaultSecure", false /*insecure*/);                                                       // 56
    var restrictedCollectionDefaultInsecure = defineCollection(                                                        // 57
      "collection-restrictedDefaultInsecure", true /*insecure*/);                                                      // 58
    var restrictedCollectionForUpdateOptionsTest = defineCollection(                                                   // 59
      "collection-restrictedForUpdateOptionsTest", true /*insecure*/);                                                 // 60
    var restrictedCollectionForPartialAllowTest = defineCollection(                                                    // 61
      "collection-restrictedForPartialAllowTest", true /*insecure*/);                                                  // 62
    var restrictedCollectionForPartialDenyTest = defineCollection(                                                     // 63
      "collection-restrictedForPartialDenyTest", true /*insecure*/);                                                   // 64
    var restrictedCollectionForFetchTest = defineCollection(                                                           // 65
      "collection-restrictedForFetchTest", true /*insecure*/);                                                         // 66
    var restrictedCollectionForFetchAllTest = defineCollection(                                                        // 67
      "collection-restrictedForFetchAllTest", true /*insecure*/);                                                      // 68
    var restrictedCollectionWithTransform = defineCollection(                                                          // 69
      "withTransform", false, function (doc) {                                                                         // 70
        return doc.a;                                                                                                  // 71
      });                                                                                                              // 72
    var restrictedCollectionForInvalidTransformTest = defineCollection(                                                // 73
      "collection-restictedForInvalidTransform", false /*insecure*/);                                                  // 74
                                                                                                                       // 75
    if (needToConfigure) {                                                                                             // 76
      restrictedCollectionWithTransform.allow({                                                                        // 77
        insert: function (userId, doc) {                                                                               // 78
          return doc.foo === "foo";                                                                                    // 79
        },                                                                                                             // 80
        update: function (userId, doc) {                                                                               // 81
          return doc.foo === "foo";                                                                                    // 82
        },                                                                                                             // 83
        remove: function (userId, doc) {                                                                               // 84
          return doc.bar === "bar";                                                                                    // 85
        }                                                                                                              // 86
      });                                                                                                              // 87
      restrictedCollectionWithTransform.allow({                                                                        // 88
        // transform: null means that doc here is the top level, not the 'a'                                           // 89
        // element.                                                                                                    // 90
        transform: null,                                                                                               // 91
        insert: function (userId, doc) {                                                                               // 92
          return !!doc.topLevelField;                                                                                  // 93
        }                                                                                                              // 94
      });                                                                                                              // 95
      restrictedCollectionForInvalidTransformTest.allow({                                                              // 96
        // transform must return an object which is not a mongo id                                                     // 97
        transform: function (doc) { return doc._id; },                                                                 // 98
        insert: function () { return true; }                                                                           // 99
      });                                                                                                              // 100
                                                                                                                       // 101
      // two calls to allow to verify that either validator is sufficient.                                             // 102
      var allows = [{                                                                                                  // 103
        insert: function(userId, doc) {                                                                                // 104
          return doc.canInsert;                                                                                        // 105
        },                                                                                                             // 106
        update: function(userId, doc) {                                                                                // 107
          return doc.canUpdate;                                                                                        // 108
        },                                                                                                             // 109
        remove: function (userId, doc) {                                                                               // 110
          return doc.canRemove;                                                                                        // 111
        }                                                                                                              // 112
      }, {                                                                                                             // 113
        insert: function(userId, doc) {                                                                                // 114
          return doc.canInsert2;                                                                                       // 115
        },                                                                                                             // 116
        update: function(userId, doc, fields, modifier) {                                                              // 117
          return -1 !== _.indexOf(fields, 'canUpdate2');                                                               // 118
        },                                                                                                             // 119
        remove: function(userId, doc) {                                                                                // 120
          return doc.canRemove2;                                                                                       // 121
        }                                                                                                              // 122
      }];                                                                                                              // 123
                                                                                                                       // 124
      // two calls to deny to verify that either one blocks the change.                                                // 125
      var denies = [{                                                                                                  // 126
        insert: function(userId, doc) {                                                                                // 127
          return doc.cantInsert;                                                                                       // 128
        },                                                                                                             // 129
        remove: function (userId, doc) {                                                                               // 130
          return doc.cantRemove;                                                                                       // 131
        }                                                                                                              // 132
      }, {                                                                                                             // 133
        insert: function(userId, doc) {                                                                                // 134
          return doc.cantInsert2;                                                                                      // 135
        },                                                                                                             // 136
        update: function(userId, doc, fields, modifier) {                                                              // 137
          return -1 !== _.indexOf(fields, 'verySecret');                                                               // 138
        }                                                                                                              // 139
      }];                                                                                                              // 140
                                                                                                                       // 141
      _.each([                                                                                                         // 142
        restrictedCollectionDefaultSecure,                                                                             // 143
        restrictedCollectionDefaultInsecure,                                                                           // 144
        restrictedCollectionForUpdateOptionsTest                                                                       // 145
      ], function (collection) {                                                                                       // 146
        _.each(allows, function (allow) {                                                                              // 147
          collection.allow(allow);                                                                                     // 148
        });                                                                                                            // 149
        _.each(denies, function (deny) {                                                                               // 150
          collection.deny(deny);                                                                                       // 151
        });                                                                                                            // 152
      });                                                                                                              // 153
                                                                                                                       // 154
      // just restrict one operation so that we can verify that others                                                 // 155
      // fail                                                                                                          // 156
      restrictedCollectionForPartialAllowTest.allow({                                                                  // 157
        insert: function() {}                                                                                          // 158
      });                                                                                                              // 159
      restrictedCollectionForPartialDenyTest.deny({                                                                    // 160
        insert: function() {}                                                                                          // 161
      });                                                                                                              // 162
                                                                                                                       // 163
      // verify that we only fetch the fields specified - we should                                                    // 164
      // be fetching just field1, field2, and field3.                                                                  // 165
      restrictedCollectionForFetchTest.allow({                                                                         // 166
        insert: function() { return true; },                                                                           // 167
        update: function(userId, doc) {                                                                                // 168
          // throw fields in doc so that we can inspect them in test                                                   // 169
          throw new Meteor.Error(                                                                                      // 170
            999, "Test: Fields in doc: " + _.keys(doc).join(','));                                                     // 171
        },                                                                                                             // 172
        remove: function(userId, doc) {                                                                                // 173
          // throw fields in doc so that we can inspect them in test                                                   // 174
          throw new Meteor.Error(                                                                                      // 175
            999, "Test: Fields in doc: " + _.keys(doc).join(','));                                                     // 176
        },                                                                                                             // 177
        fetch: ['field1']                                                                                              // 178
      });                                                                                                              // 179
      restrictedCollectionForFetchTest.allow({                                                                         // 180
        fetch: ['field2']                                                                                              // 181
      });                                                                                                              // 182
      restrictedCollectionForFetchTest.deny({                                                                          // 183
        fetch: ['field3']                                                                                              // 184
      });                                                                                                              // 185
                                                                                                                       // 186
      // verify that not passing fetch to one of the calls to allow                                                    // 187
      // causes all fields to be fetched                                                                               // 188
      restrictedCollectionForFetchAllTest.allow({                                                                      // 189
        insert: function() { return true; },                                                                           // 190
        update: function(userId, doc) {                                                                                // 191
          // throw fields in doc so that we can inspect them in test                                                   // 192
          throw new Meteor.Error(                                                                                      // 193
            999, "Test: Fields in doc: " + _.keys(doc).join(','));                                                     // 194
        },                                                                                                             // 195
        remove: function(userId, doc) {                                                                                // 196
          // throw fields in doc so that we can inspect them in test                                                   // 197
          throw new Meteor.Error(                                                                                      // 198
            999, "Test: Fields in doc: " + _.keys(doc).join(','));                                                     // 199
        },                                                                                                             // 200
        fetch: ['field1']                                                                                              // 201
      });                                                                                                              // 202
      restrictedCollectionForFetchAllTest.allow({                                                                      // 203
        update: function() { return true; }                                                                            // 204
      });                                                                                                              // 205
    }                                                                                                                  // 206
                                                                                                                       // 207
    return cursors;                                                                                                    // 208
  });                                                                                                                  // 209
}                                                                                                                      // 210
                                                                                                                       // 211
if (Meteor.isClient) {                                                                                                 // 212
  _.each(['STRING', 'MONGO'], function (idGeneration) {                                                                // 213
    // Set up a bunch of test collections... on the client! They match the ones                                        // 214
    // created by setUpAllowTestsCollections.                                                                          // 215
                                                                                                                       // 216
    var nonce = Random.id();                                                                                           // 217
    // Tell the server to make, configure, and publish a set of collections unique                                     // 218
    // to our test run. Since the method does not unblock, this will complete                                          // 219
    // running on the server before anything else happens.                                                             // 220
    Meteor.subscribe('allowTests', nonce, idGeneration);                                                               // 221
                                                                                                                       // 222
    // helper for defining a collection, subscribing to it, and defining                                               // 223
    // a method to clear it                                                                                            // 224
    var defineCollection = function(name, transform) {                                                                 // 225
      var fullName = name + idGeneration + nonce;                                                                      // 226
      var collection = new Meteor.Collection(                                                                          // 227
        fullName, {idGeneration: idGeneration, transform: transform});                                                 // 228
                                                                                                                       // 229
      collection.callClearMethod = function (callback) {                                                               // 230
        Meteor.call("clear-collection-" + fullName, callback);                                                         // 231
      };                                                                                                               // 232
      collection.unnoncedName = name + idGeneration;                                                                   // 233
      return collection;                                                                                               // 234
    };                                                                                                                 // 235
                                                                                                                       // 236
    // totally insecure collection                                                                                     // 237
    var insecureCollection = defineCollection("collection-insecure");                                                  // 238
                                                                                                                       // 239
    // totally locked down collection                                                                                  // 240
    var lockedDownCollection = defineCollection("collection-locked-down");                                             // 241
                                                                                                                       // 242
    // resticted collection with same allowed modifications, both with and                                             // 243
    // without the `insecure` package                                                                                  // 244
    var restrictedCollectionDefaultSecure = defineCollection(                                                          // 245
      "collection-restrictedDefaultSecure");                                                                           // 246
    var restrictedCollectionDefaultInsecure = defineCollection(                                                        // 247
      "collection-restrictedDefaultInsecure");                                                                         // 248
    var restrictedCollectionForUpdateOptionsTest = defineCollection(                                                   // 249
      "collection-restrictedForUpdateOptionsTest");                                                                    // 250
    var restrictedCollectionForPartialAllowTest = defineCollection(                                                    // 251
      "collection-restrictedForPartialAllowTest");                                                                     // 252
    var restrictedCollectionForPartialDenyTest = defineCollection(                                                     // 253
      "collection-restrictedForPartialDenyTest");                                                                      // 254
    var restrictedCollectionForFetchTest = defineCollection(                                                           // 255
      "collection-restrictedForFetchTest");                                                                            // 256
    var restrictedCollectionForFetchAllTest = defineCollection(                                                        // 257
      "collection-restrictedForFetchAllTest");                                                                         // 258
    var restrictedCollectionWithTransform = defineCollection(                                                          // 259
      "withTransform", function (doc) {                                                                                // 260
        return doc.a;                                                                                                  // 261
      });                                                                                                              // 262
    var restrictedCollectionForInvalidTransformTest = defineCollection(                                                // 263
      "collection-restictedForInvalidTransform");                                                                      // 264
                                                                                                                       // 265
    // test that if allow is called once then the collection is                                                        // 266
    // restricted, and that other mutations aren't allowed                                                             // 267
    testAsyncMulti("collection - partial allow, " + idGeneration, [                                                    // 268
      function (test, expect) {                                                                                        // 269
        restrictedCollectionForPartialAllowTest.update(                                                                // 270
          'foo', {$set: {updated: true}}, expect(function (err, res) {                                                 // 271
            test.equal(err.error, 403);                                                                                // 272
          }));                                                                                                         // 273
      }                                                                                                                // 274
    ]);                                                                                                                // 275
                                                                                                                       // 276
    // test that if deny is called once then the collection is                                                         // 277
    // restricted, and that other mutations aren't allowed                                                             // 278
    testAsyncMulti("collection - partial deny, " + idGeneration, [                                                     // 279
      function (test, expect) {                                                                                        // 280
        restrictedCollectionForPartialDenyTest.update(                                                                 // 281
          'foo', {$set: {updated: true}}, expect(function (err, res) {                                                 // 282
            test.equal(err.error, 403);                                                                                // 283
          }));                                                                                                         // 284
      }                                                                                                                // 285
    ]);                                                                                                                // 286
                                                                                                                       // 287
                                                                                                                       // 288
    // test that we only fetch the fields specified                                                                    // 289
    testAsyncMulti("collection - fetch, " + idGeneration, [                                                            // 290
      function (test, expect) {                                                                                        // 291
        var fetchId = restrictedCollectionForFetchTest.insert(                                                         // 292
          {field1: 1, field2: 1, field3: 1, field4: 1});                                                               // 293
        var fetchAllId = restrictedCollectionForFetchAllTest.insert(                                                   // 294
          {field1: 1, field2: 1, field3: 1, field4: 1});                                                               // 295
        restrictedCollectionForFetchTest.update(                                                                       // 296
          fetchId, {$set: {updated: true}}, expect(function (err, res) {                                               // 297
            test.equal(err.reason,                                                                                     // 298
                       "Test: Fields in doc: field1,field2,field3,_id");                                               // 299
          }));                                                                                                         // 300
        restrictedCollectionForFetchTest.remove(                                                                       // 301
          fetchId, expect(function (err, res) {                                                                        // 302
            test.equal(err.reason,                                                                                     // 303
                       "Test: Fields in doc: field1,field2,field3,_id");                                               // 304
          }));                                                                                                         // 305
                                                                                                                       // 306
        restrictedCollectionForFetchAllTest.update(                                                                    // 307
          fetchAllId, {$set: {updated: true}}, expect(function (err, res) {                                            // 308
            test.equal(err.reason,                                                                                     // 309
                       "Test: Fields in doc: field1,field2,field3,field4,_id");                                        // 310
          }));                                                                                                         // 311
        restrictedCollectionForFetchAllTest.remove(                                                                    // 312
          fetchAllId, expect(function (err, res) {                                                                     // 313
            test.equal(err.reason,                                                                                     // 314
                       "Test: Fields in doc: field1,field2,field3,field4,_id");                                        // 315
          }));                                                                                                         // 316
      }                                                                                                                // 317
    ]);                                                                                                                // 318
                                                                                                                       // 319
    (function(){                                                                                                       // 320
      var item1;                                                                                                       // 321
      var item2;                                                                                                       // 322
      testAsyncMulti("collection - restricted factories " + idGeneration, [                                            // 323
        function (test, expect) {                                                                                      // 324
          restrictedCollectionWithTransform.callClearMethod(expect(function () {                                       // 325
            test.equal(restrictedCollectionWithTransform.find().count(), 0);                                           // 326
          }));                                                                                                         // 327
        },                                                                                                             // 328
        function (test, expect) {                                                                                      // 329
          restrictedCollectionWithTransform.insert({                                                                   // 330
            a: {foo: "foo", bar: "bar", baz: "baz"}                                                                    // 331
          }, expect(function (e, res) {                                                                                // 332
            test.isFalse(e);                                                                                           // 333
            test.isTrue(res);                                                                                          // 334
            item1 = res;                                                                                               // 335
          }));                                                                                                         // 336
          restrictedCollectionWithTransform.insert({                                                                   // 337
            a: {foo: "foo", bar: "quux", baz: "quux"},                                                                 // 338
            b: "potato"                                                                                                // 339
          }, expect(function (e, res) {                                                                                // 340
            test.isFalse(e);                                                                                           // 341
            test.isTrue(res);                                                                                          // 342
            item2 = res;                                                                                               // 343
          }));                                                                                                         // 344
          restrictedCollectionWithTransform.insert({                                                                   // 345
            a: {foo: "adsfadf", bar: "quux", baz: "quux"},                                                             // 346
            b: "potato"                                                                                                // 347
          }, expect(function (e, res) {                                                                                // 348
            test.isTrue(e);                                                                                            // 349
          }));                                                                                                         // 350
          restrictedCollectionWithTransform.insert({                                                                   // 351
            a: {foo: "bar"},                                                                                           // 352
            topLevelField: true                                                                                        // 353
          }, expect(function (e, res) {                                                                                // 354
            test.isFalse(e);                                                                                           // 355
            test.isTrue(res);                                                                                          // 356
          }));                                                                                                         // 357
        },                                                                                                             // 358
        function (test, expect) {                                                                                      // 359
          test.equal(                                                                                                  // 360
            _.omit(restrictedCollectionWithTransform.findOne({"a.bar": "bar"}), '_id'),                                // 361
            {foo: "foo", bar: "bar", baz: "baz"});                                                                     // 362
          restrictedCollectionWithTransform.remove(item1, expect(function (e, res) {                                   // 363
            test.isFalse(e);                                                                                           // 364
          }));                                                                                                         // 365
          restrictedCollectionWithTransform.remove(item2, expect(function (e, res) {                                   // 366
            test.isTrue(e);                                                                                            // 367
          }));                                                                                                         // 368
        }                                                                                                              // 369
      ]);                                                                                                              // 370
    })();                                                                                                              // 371
                                                                                                                       // 372
    testAsyncMulti("collection - insecure, " + idGeneration, [                                                         // 373
      function (test, expect) {                                                                                        // 374
        insecureCollection.callClearMethod(expect(function () {                                                        // 375
          test.equal(insecureCollection.find().count(), 0);                                                            // 376
        }));                                                                                                           // 377
      },                                                                                                               // 378
      function (test, expect) {                                                                                        // 379
        var id = insecureCollection.insert({foo: 'bar'}, expect(function(err, res) {                                   // 380
          test.equal(res, id);                                                                                         // 381
          test.equal(insecureCollection.find(id).count(), 1);                                                          // 382
          test.equal(insecureCollection.findOne(id).foo, 'bar');                                                       // 383
        }));                                                                                                           // 384
        test.equal(insecureCollection.find(id).count(), 1);                                                            // 385
        test.equal(insecureCollection.findOne(id).foo, 'bar');                                                         // 386
      }                                                                                                                // 387
    ]);                                                                                                                // 388
                                                                                                                       // 389
    testAsyncMulti("collection - locked down, " + idGeneration, [                                                      // 390
      function (test, expect) {                                                                                        // 391
        lockedDownCollection.callClearMethod(expect(function() {                                                       // 392
          test.equal(lockedDownCollection.find().count(), 0);                                                          // 393
        }));                                                                                                           // 394
      },                                                                                                               // 395
      function (test, expect) {                                                                                        // 396
        lockedDownCollection.insert({foo: 'bar'}, expect(function (err, res) {                                         // 397
          test.equal(err.error, 403);                                                                                  // 398
          test.equal(lockedDownCollection.find().count(), 0);                                                          // 399
        }));                                                                                                           // 400
      }                                                                                                                // 401
    ]);                                                                                                                // 402
                                                                                                                       // 403
    (function () {                                                                                                     // 404
      var collection = restrictedCollectionForUpdateOptionsTest;                                                       // 405
      var id1, id2;                                                                                                    // 406
      testAsyncMulti("collection - update options, " + idGeneration, [                                                 // 407
        // init                                                                                                        // 408
        function (test, expect) {                                                                                      // 409
          collection.callClearMethod(expect(function () {                                                              // 410
            test.equal(collection.find().count(), 0);                                                                  // 411
          }));                                                                                                         // 412
        },                                                                                                             // 413
        // put a few objects                                                                                           // 414
        function (test, expect) {                                                                                      // 415
          var doc = {canInsert: true, canUpdate: true};                                                                // 416
          id1 = collection.insert(doc);                                                                                // 417
          id2 = collection.insert(doc);                                                                                // 418
          collection.insert(doc);                                                                                      // 419
          collection.insert(doc, expect(function (err, res) {                                                          // 420
            test.isFalse(err);                                                                                         // 421
            test.equal(collection.find().count(), 4);                                                                  // 422
          }));                                                                                                         // 423
        },                                                                                                             // 424
        // update by id                                                                                                // 425
        function (test, expect) {                                                                                      // 426
          collection.update(                                                                                           // 427
            id1,                                                                                                       // 428
            {$set: {updated: true}},                                                                                   // 429
            expect(function (err, res) {                                                                               // 430
              test.isFalse(err);                                                                                       // 431
              test.equal(res, 1);                                                                                      // 432
              test.equal(collection.find({updated: true}).count(), 1);                                                 // 433
            }));                                                                                                       // 434
        },                                                                                                             // 435
        // update by id in an object                                                                                   // 436
        function (test, expect) {                                                                                      // 437
          collection.update(                                                                                           // 438
            {_id: id2},                                                                                                // 439
            {$set: {updated: true}},                                                                                   // 440
            expect(function (err, res) {                                                                               // 441
              test.isFalse(err);                                                                                       // 442
              test.equal(res, 1);                                                                                      // 443
              test.equal(collection.find({updated: true}).count(), 2);                                                 // 444
            }));                                                                                                       // 445
        },                                                                                                             // 446
        // update with replacement operator not allowed, and has nice error.                                           // 447
        function (test, expect) {                                                                                      // 448
          collection.update(                                                                                           // 449
            {_id: id2},                                                                                                // 450
            {_id: id2, updated: true},                                                                                 // 451
            expect(function (err, res) {                                                                               // 452
              test.equal(err.error, 403);                                                                              // 453
              test.matches(err.reason, /In a restricted/);                                                             // 454
              // unchanged                                                                                             // 455
              test.equal(collection.find({updated: true}).count(), 2);                                                 // 456
            }));                                                                                                       // 457
        },                                                                                                             // 458
        // upsert not allowed, and has nice error.                                                                     // 459
        function (test, expect) {                                                                                      // 460
          collection.update(                                                                                           // 461
            {_id: id2},                                                                                                // 462
            {$set: { upserted: true }},                                                                                // 463
            { upsert: true },                                                                                          // 464
            expect(function (err, res) {                                                                               // 465
              test.equal(err.error, 403);                                                                              // 466
              test.matches(err.reason, /in a restricted/);                                                             // 467
              test.equal(collection.find({ upserted: true }).count(), 0);                                              // 468
            }));                                                                                                       // 469
        },                                                                                                             // 470
        // update with rename operator not allowed, and has nice error.                                                // 471
        function (test, expect) {                                                                                      // 472
          collection.update(                                                                                           // 473
            {_id: id2},                                                                                                // 474
            {$rename: {updated: 'asdf'}},                                                                              // 475
            expect(function (err, res) {                                                                               // 476
              test.equal(err.error, 403);                                                                              // 477
              test.matches(err.reason, /not allowed/);                                                                 // 478
              // unchanged                                                                                             // 479
              test.equal(collection.find({updated: true}).count(), 2);                                                 // 480
            }));                                                                                                       // 481
        },                                                                                                             // 482
        // update method with a non-ID selector is not allowed                                                         // 483
        function (test, expect) {                                                                                      // 484
          // We shouldn't even send the method...                                                                      // 485
          test.throws(function () {                                                                                    // 486
            collection.update(                                                                                         // 487
              {updated: {$exists: false}},                                                                             // 488
              {$set: {updated: true}});                                                                                // 489
          });                                                                                                          // 490
          // ... but if we did, the server would reject it too.                                                        // 491
          Meteor.call(                                                                                                 // 492
            '/' + collection._name + '/update',                                                                        // 493
            {updated: {$exists: false}},                                                                               // 494
            {$set: {updated: true}},                                                                                   // 495
            expect(function (err, res) {                                                                               // 496
              test.equal(err.error, 403);                                                                              // 497
              // unchanged                                                                                             // 498
              test.equal(collection.find({updated: true}).count(), 2);                                                 // 499
            }));                                                                                                       // 500
        },                                                                                                             // 501
        // make sure it doesn't think that {_id: 'foo', something: else} is ok.                                        // 502
        function (test, expect) {                                                                                      // 503
          test.throws(function () {                                                                                    // 504
            collection.update(                                                                                         // 505
              {_id: id1, updated: {$exists: false}},                                                                   // 506
              {$set: {updated: true}});                                                                                // 507
          });                                                                                                          // 508
        },                                                                                                             // 509
        // remove method with a non-ID selector is not allowed                                                         // 510
        function (test, expect) {                                                                                      // 511
          // We shouldn't even send the method...                                                                      // 512
          test.throws(function () {                                                                                    // 513
            collection.remove({updated: true});                                                                        // 514
          });                                                                                                          // 515
          // ... but if we did, the server would reject it too.                                                        // 516
          Meteor.call(                                                                                                 // 517
            '/' + collection._name + '/remove',                                                                        // 518
            {updated: true},                                                                                           // 519
            expect(function (err, res) {                                                                               // 520
              test.equal(err.error, 403);                                                                              // 521
              // unchanged                                                                                             // 522
              test.equal(collection.find({updated: true}).count(), 2);                                                 // 523
            }));                                                                                                       // 524
        }                                                                                                              // 525
      ]);                                                                                                              // 526
    }) ();                                                                                                             // 527
                                                                                                                       // 528
    _.each(                                                                                                            // 529
      [restrictedCollectionDefaultInsecure, restrictedCollectionDefaultSecure],                                        // 530
      function(collection) {                                                                                           // 531
        var canUpdateId, canRemoveId;                                                                                  // 532
                                                                                                                       // 533
        testAsyncMulti("collection - " + collection.unnoncedName, [                                                    // 534
          // init                                                                                                      // 535
          function (test, expect) {                                                                                    // 536
            collection.callClearMethod(expect(function () {                                                            // 537
              test.equal(collection.find().count(), 0);                                                                // 538
            }));                                                                                                       // 539
          },                                                                                                           // 540
                                                                                                                       // 541
          // insert with no allows passing. request is denied.                                                         // 542
          function (test, expect) {                                                                                    // 543
            collection.insert(                                                                                         // 544
              {},                                                                                                      // 545
              expect(function (err, res) {                                                                             // 546
                test.equal(err.error, 403);                                                                            // 547
                test.equal(collection.find().count(), 0);                                                              // 548
              }));                                                                                                     // 549
          },                                                                                                           // 550
          // insert with one allow and one deny. denied.                                                               // 551
          function (test, expect) {                                                                                    // 552
            collection.insert(                                                                                         // 553
              {canInsert: true, cantInsert: true},                                                                     // 554
              expect(function (err, res) {                                                                             // 555
                test.equal(err.error, 403);                                                                            // 556
                test.equal(collection.find().count(), 0);                                                              // 557
              }));                                                                                                     // 558
          },                                                                                                           // 559
          // insert with one allow and other deny. denied.                                                             // 560
          function (test, expect) {                                                                                    // 561
            collection.insert(                                                                                         // 562
              {canInsert: true, cantInsert2: true},                                                                    // 563
              expect(function (err, res) {                                                                             // 564
                test.equal(err.error, 403);                                                                            // 565
                test.equal(collection.find().count(), 0);                                                              // 566
              }));                                                                                                     // 567
          },                                                                                                           // 568
          // insert one allow passes. allowed.                                                                         // 569
          function (test, expect) {                                                                                    // 570
            collection.insert(                                                                                         // 571
              {canInsert: true},                                                                                       // 572
              expect(function (err, res) {                                                                             // 573
                test.isFalse(err);                                                                                     // 574
                test.equal(collection.find().count(), 1);                                                              // 575
              }));                                                                                                     // 576
          },                                                                                                           // 577
          // insert other allow passes. allowed.                                                                       // 578
          // includes canUpdate for later.                                                                             // 579
          function (test, expect) {                                                                                    // 580
            canUpdateId = collection.insert(                                                                           // 581
              {canInsert2: true, canUpdate: true},                                                                     // 582
              expect(function (err, res) {                                                                             // 583
                test.isFalse(err);                                                                                     // 584
                test.equal(collection.find().count(), 2);                                                              // 585
              }));                                                                                                     // 586
          },                                                                                                           // 587
          // yet a third insert executes. this one has canRemove and                                                   // 588
          // cantRemove set for later.                                                                                 // 589
          function (test, expect) {                                                                                    // 590
            canRemoveId = collection.insert(                                                                           // 591
              {canInsert: true, canRemove: true, cantRemove: true},                                                    // 592
              expect(function (err, res) {                                                                             // 593
                test.isFalse(err);                                                                                     // 594
                test.equal(collection.find().count(), 3);                                                              // 595
              }));                                                                                                     // 596
          },                                                                                                           // 597
                                                                                                                       // 598
          // can't update with a non-operator mutation                                                                 // 599
          function (test, expect) {                                                                                    // 600
            collection.update(                                                                                         // 601
              canUpdateId, {newObject: 1},                                                                             // 602
              expect(function (err, res) {                                                                             // 603
                test.equal(err.error, 403);                                                                            // 604
                test.equal(collection.find().count(), 3);                                                              // 605
              }));                                                                                                     // 606
          },                                                                                                           // 607
                                                                                                                       // 608
          // updating dotted fields works as if we are changing their                                                  // 609
          // top part                                                                                                  // 610
          function (test, expect) {                                                                                    // 611
            collection.update(                                                                                         // 612
              canUpdateId, {$set: {"dotted.field": 1}},                                                                // 613
              expect(function (err, res) {                                                                             // 614
                test.isFalse(err);                                                                                     // 615
                test.equal(res, 1);                                                                                    // 616
                test.equal(collection.findOne(canUpdateId).dotted.field, 1);                                           // 617
              }));                                                                                                     // 618
          },                                                                                                           // 619
          function (test, expect) {                                                                                    // 620
            collection.update(                                                                                         // 621
              canUpdateId, {$set: {"verySecret.field": 1}},                                                            // 622
              expect(function (err, res) {                                                                             // 623
                test.equal(err.error, 403);                                                                            // 624
                test.equal(collection.find({verySecret: {$exists: true}}).count(), 0);                                 // 625
              }));                                                                                                     // 626
          },                                                                                                           // 627
                                                                                                                       // 628
          // update doesn't do anything if no docs match                                                               // 629
          function (test, expect) {                                                                                    // 630
            collection.update(                                                                                         // 631
              "doesn't exist",                                                                                         // 632
              {$set: {updated: true}},                                                                                 // 633
              expect(function (err, res) {                                                                             // 634
                test.isFalse(err);                                                                                     // 635
                test.equal(res, 0);                                                                                    // 636
                // nothing has changed                                                                                 // 637
                test.equal(collection.find().count(), 3);                                                              // 638
                test.equal(collection.find({updated: true}).count(), 0);                                               // 639
              }));                                                                                                     // 640
          },                                                                                                           // 641
          // update fails when access is denied trying to set `verySecret`                                             // 642
          function (test, expect) {                                                                                    // 643
            collection.update(                                                                                         // 644
              canUpdateId, {$set: {verySecret: true}},                                                                 // 645
              expect(function (err, res) {                                                                             // 646
                test.equal(err.error, 403);                                                                            // 647
                // nothing has changed                                                                                 // 648
                test.equal(collection.find().count(), 3);                                                              // 649
                test.equal(collection.find({updated: true}).count(), 0);                                               // 650
              }));                                                                                                     // 651
          },                                                                                                           // 652
          // update fails when trying to set two fields, one of which is                                               // 653
          // `verySecret`                                                                                              // 654
          function (test, expect) {                                                                                    // 655
            collection.update(                                                                                         // 656
              canUpdateId, {$set: {updated: true, verySecret: true}},                                                  // 657
              expect(function (err, res) {                                                                             // 658
                test.equal(err.error, 403);                                                                            // 659
                // nothing has changed                                                                                 // 660
                test.equal(collection.find().count(), 3);                                                              // 661
                test.equal(collection.find({updated: true}).count(), 0);                                               // 662
              }));                                                                                                     // 663
          },                                                                                                           // 664
          // update fails when trying to modify docs that don't                                                        // 665
          // have `canUpdate` set                                                                                      // 666
          function (test, expect) {                                                                                    // 667
            collection.update(                                                                                         // 668
              canRemoveId,                                                                                             // 669
              {$set: {updated: true}},                                                                                 // 670
              expect(function (err, res) {                                                                             // 671
                test.equal(err.error, 403);                                                                            // 672
                // nothing has changed                                                                                 // 673
                test.equal(collection.find().count(), 3);                                                              // 674
                test.equal(collection.find({updated: true}).count(), 0);                                               // 675
              }));                                                                                                     // 676
          },                                                                                                           // 677
          // update executes when it should                                                                            // 678
          function (test, expect) {                                                                                    // 679
            collection.update(                                                                                         // 680
              canUpdateId,                                                                                             // 681
              {$set: {updated: true}},                                                                                 // 682
              expect(function (err, res) {                                                                             // 683
                test.isFalse(err);                                                                                     // 684
                test.equal(res, 1);                                                                                    // 685
                test.equal(collection.find({updated: true}).count(), 1);                                               // 686
              }));                                                                                                     // 687
          },                                                                                                           // 688
                                                                                                                       // 689
          // remove fails when trying to modify a doc with no `canRemove` set                                          // 690
          function (test, expect) {                                                                                    // 691
            collection.remove(canUpdateId,                                                                             // 692
                              expect(function (err, res) {                                                             // 693
              test.equal(err.error, 403);                                                                              // 694
              // nothing has changed                                                                                   // 695
              test.equal(collection.find().count(), 3);                                                                // 696
            }));                                                                                                       // 697
          },                                                                                                           // 698
          // remove fails when trying to modify an doc with `cantRemove`                                               // 699
          // set                                                                                                       // 700
          function (test, expect) {                                                                                    // 701
            collection.remove(canRemoveId,                                                                             // 702
                              expect(function (err, res) {                                                             // 703
              test.equal(err.error, 403);                                                                              // 704
              // nothing has changed                                                                                   // 705
              test.equal(collection.find().count(), 3);                                                                // 706
            }));                                                                                                       // 707
          },                                                                                                           // 708
                                                                                                                       // 709
          // update the doc to remove cantRemove.                                                                      // 710
          function (test, expect) {                                                                                    // 711
            collection.update(                                                                                         // 712
              canRemoveId,                                                                                             // 713
              {$set: {cantRemove: false, canUpdate2: true}},                                                           // 714
              expect(function (err, res) {                                                                             // 715
                test.isFalse(err);                                                                                     // 716
                test.equal(res, 1);                                                                                    // 717
                test.equal(collection.find({cantRemove: true}).count(), 0);                                            // 718
              }));                                                                                                     // 719
          },                                                                                                           // 720
                                                                                                                       // 721
          // now remove can remove it.                                                                                 // 722
          function (test, expect) {                                                                                    // 723
            collection.remove(canRemoveId,                                                                             // 724
                              expect(function (err, res) {                                                             // 725
              test.isFalse(err);                                                                                       // 726
              test.equal(res, 1);                                                                                      // 727
              // successfully removed                                                                                  // 728
              test.equal(collection.find().count(), 2);                                                                // 729
            }));                                                                                                       // 730
          },                                                                                                           // 731
                                                                                                                       // 732
          // try to remove a doc that doesn't exist. see we remove no docs.                                            // 733
          function (test, expect) {                                                                                    // 734
            collection.remove('some-random-id-that-never-matches',                                                     // 735
                              expect(function (err, res) {                                                             // 736
              test.isFalse(err);                                                                                       // 737
              test.equal(res, 0);                                                                                      // 738
              // nothing removed                                                                                       // 739
              test.equal(collection.find().count(), 2);                                                                // 740
            }));                                                                                                       // 741
          },                                                                                                           // 742
                                                                                                                       // 743
          // methods can still bypass restrictions                                                                     // 744
          function (test, expect) {                                                                                    // 745
            collection.callClearMethod(                                                                                // 746
              expect(function (err, res) {                                                                             // 747
                test.isFalse(err);                                                                                     // 748
                // successfully removed                                                                                // 749
                test.equal(collection.find().count(), 0);                                                              // 750
            }));                                                                                                       // 751
          }                                                                                                            // 752
        ]);                                                                                                            // 753
      });                                                                                                              // 754
    testAsyncMulti(                                                                                                    // 755
      "collection - allow/deny transform must return object, " + idGeneration,                                         // 756
      [function (test, expect) {                                                                                       // 757
        restrictedCollectionForInvalidTransformTest.insert({}, expect(function (err, res) {                            // 758
          test.isTrue(err);                                                                                            // 759
        }));                                                                                                           // 760
      }]);                                                                                                             // 761
  });  // end idGeneration loop                                                                                        // 762
}  // end if isClient                                                                                                  // 763
                                                                                                                       // 764
                                                                                                                       // 765
                                                                                                                       // 766
// A few simple server-only tests which don't need to coordinate collections                                           // 767
// with the client..                                                                                                   // 768
if (Meteor.isServer) {                                                                                                 // 769
  Tinytest.add("collection - allow and deny validate options", function (test) {                                       // 770
    var collection = new Meteor.Collection(null);                                                                      // 771
                                                                                                                       // 772
    test.throws(function () {                                                                                          // 773
      collection.allow({invalidOption: true});                                                                         // 774
    });                                                                                                                // 775
    test.throws(function () {                                                                                          // 776
      collection.deny({invalidOption: true});                                                                          // 777
    });                                                                                                                // 778
                                                                                                                       // 779
    _.each(['insert', 'update', 'remove', 'fetch'], function (key) {                                                   // 780
      var options = {};                                                                                                // 781
      options[key] = true;                                                                                             // 782
      test.throws(function () {                                                                                        // 783
        collection.allow(options);                                                                                     // 784
      });                                                                                                              // 785
      test.throws(function () {                                                                                        // 786
        collection.deny(options);                                                                                      // 787
      });                                                                                                              // 788
    });                                                                                                                // 789
                                                                                                                       // 790
    _.each(['insert', 'update', 'remove'], function (key) {                                                            // 791
      var options = {};                                                                                                // 792
      options[key] = ['an array']; // this should be a function, not an array                                          // 793
      test.throws(function () {                                                                                        // 794
        collection.allow(options);                                                                                     // 795
      });                                                                                                              // 796
      test.throws(function () {                                                                                        // 797
        collection.deny(options);                                                                                      // 798
      });                                                                                                              // 799
    });                                                                                                                // 800
                                                                                                                       // 801
    test.throws(function () {                                                                                          // 802
      collection.allow({fetch: function () {}}); // this should be an array                                            // 803
    });                                                                                                                // 804
  });                                                                                                                  // 805
                                                                                                                       // 806
  Tinytest.add("collection - calling allow restricts", function (test) {                                               // 807
    var collection = new Meteor.Collection(null);                                                                      // 808
    test.equal(collection._restricted, false);                                                                         // 809
    collection.allow({                                                                                                 // 810
      insert: function() {}                                                                                            // 811
    });                                                                                                                // 812
    test.equal(collection._restricted, true);                                                                          // 813
  });                                                                                                                  // 814
                                                                                                                       // 815
  Tinytest.add("collection - global insecure", function (test) {                                                       // 816
    // note: This test alters the global insecure status, by sneakily hacking                                          // 817
    // the global Package object! This may collide with itself if run multiple                                         // 818
    // times (but is better than the old test which had the same problem)                                              // 819
    var insecurePackage = Package.insecure;                                                                            // 820
                                                                                                                       // 821
    Package.insecure = {};                                                                                             // 822
    var collection = new Meteor.Collection(null);                                                                      // 823
    test.equal(collection._isInsecure(), true);                                                                        // 824
                                                                                                                       // 825
    Package.insecure = undefined;                                                                                      // 826
    test.equal(collection._isInsecure(), false);                                                                       // 827
                                                                                                                       // 828
    delete Package.insecure;                                                                                           // 829
    test.equal(collection._isInsecure(), false);                                                                       // 830
                                                                                                                       // 831
    collection._insecure = true;                                                                                       // 832
    test.equal(collection._isInsecure(), true);                                                                        // 833
                                                                                                                       // 834
    if (insecurePackage)                                                                                               // 835
      Package.insecure = insecurePackage;                                                                              // 836
    else                                                                                                               // 837
      delete Package.insecure;                                                                                         // 838
  });                                                                                                                  // 839
}                                                                                                                      // 840
                                                                                                                       // 841
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/collection_tests.js                                                                         //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
Tinytest.add(                                                                                                          // 1
  'collection - call Meteor.Collection without new',                                                                   // 2
  function (test) {                                                                                                    // 3
    test.throws(                                                                                                       // 4
      function () {                                                                                                    // 5
        Meteor.Collection(null);                                                                                       // 6
      },                                                                                                               // 7
      /use "new" to construct a Meteor\.Collection/                                                                    // 8
    );                                                                                                                 // 9
  }                                                                                                                    // 10
);                                                                                                                     // 11
                                                                                                                       // 12
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/observe_changes_tests.js                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var makeCollection = function () {                                                                                     // 1
  if (Meteor.isServer)                                                                                                 // 2
    return new Meteor.Collection(Random.id());                                                                         // 3
  else                                                                                                                 // 4
    return new Meteor.Collection(null);                                                                                // 5
};                                                                                                                     // 6
                                                                                                                       // 7
_.each ([{added:'added', forceOrdered: true},                                                                          // 8
         {added:'added', forceOrdered: false},                                                                         // 9
         {added: 'addedBefore', forceOrdered: false}], function (options) {                                            // 10
           var added = options.added;                                                                                  // 11
           var forceOrdered = options.forceOrdered;                                                                    // 12
  Tinytest.addAsync("observeChanges - single id - basics "                                                             // 13
                    + added                                                                                            // 14
                    + (forceOrdered ? " force ordered" : ""),                                                          // 15
                    function (test, onComplete) {                                                                      // 16
    var c = makeCollection();                                                                                          // 17
    var counter = 0;                                                                                                   // 18
    var callbacks = [added, "changed", "removed"];                                                                     // 19
    if (forceOrdered)                                                                                                  // 20
      callbacks.push("movedBefore");                                                                                   // 21
    withCallbackLogger(test,                                                                                           // 22
                       callbacks,                                                                                      // 23
                       Meteor.isServer,                                                                                // 24
                       function (logger) {                                                                             // 25
    var barid = c.insert({thing: "stuff"});                                                                            // 26
    var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                               // 27
                                                                                                                       // 28
    var handle = c.find(fooid).observeChanges(logger);                                                                 // 29
    if (added === 'added')                                                                                             // 30
      logger.expectResult(added, [fooid, {noodles: "good", bacon: "bad",apples: "ok"}]);                               // 31
    else                                                                                                               // 32
      logger.expectResult(added,                                                                                       // 33
                          [fooid, {noodles: "good", bacon: "bad", apples: "ok"}, null]);                               // 34
    c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok"});                                            // 35
    logger.expectResult("changed",                                                                                     // 36
                        [fooid, {noodles: "alright", potatoes: "tasty", bacon: undefined}]);                           // 37
                                                                                                                       // 38
    c.remove(fooid);                                                                                                   // 39
    logger.expectResult("removed", [fooid]);                                                                           // 40
                                                                                                                       // 41
    c.remove(barid);                                                                                                   // 42
                                                                                                                       // 43
    c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                           // 44
    logger.expectNoResult();                                                                                           // 45
    handle.stop();                                                                                                     // 46
                                                                                                                       // 47
    var badCursor = c.find({}, {fields: {noodles: 1, _id: false}});                                                    // 48
    test.throws(function () {                                                                                          // 49
      badCursor.observeChanges(logger);                                                                                // 50
    });                                                                                                                // 51
                                                                                                                       // 52
    onComplete();                                                                                                      // 53
    });                                                                                                                // 54
  });                                                                                                                  // 55
});                                                                                                                    // 56
                                                                                                                       // 57
Tinytest.addAsync("observeChanges - callback isolation", function (test, onComplete) {                                 // 58
  var c = makeCollection();                                                                                            // 59
  withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                       // 60
    var handles = [];                                                                                                  // 61
    var cursor = c.find();                                                                                             // 62
    handles.push(cursor.observeChanges(logger));                                                                       // 63
    // fields-tampering observer                                                                                       // 64
    handles.push(cursor.observeChanges({                                                                               // 65
      added: function(id, fields) {                                                                                    // 66
        fields.apples = 'green';                                                                                       // 67
      },                                                                                                               // 68
      changed: function(id, fields) {                                                                                  // 69
        fields.apples = 'green';                                                                                       // 70
      },                                                                                                               // 71
    }));                                                                                                               // 72
                                                                                                                       // 73
    var fooid = c.insert({apples: "ok"});                                                                              // 74
    logger.expectResult("added", [fooid, {apples: "ok"}]);                                                             // 75
                                                                                                                       // 76
    c.update(fooid, {apples: "not ok"})                                                                                // 77
    logger.expectResult("changed", [fooid, {apples: "not ok"}]);                                                       // 78
                                                                                                                       // 79
    test.equal(c.findOne(fooid).apples, "not ok");                                                                     // 80
                                                                                                                       // 81
    _.each(handles, function(handle) { handle.stop(); });                                                              // 82
    onComplete();                                                                                                      // 83
  });                                                                                                                  // 84
                                                                                                                       // 85
});                                                                                                                    // 86
                                                                                                                       // 87
Tinytest.addAsync("observeChanges - single id - initial adds", function (test, onComplete) {                           // 88
  var c = makeCollection();                                                                                            // 89
  withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                       // 90
  var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                 // 91
  var handle = c.find(fooid).observeChanges(logger);                                                                   // 92
  logger.expectResult("added", [fooid, {noodles: "good", bacon: "bad", apples: "ok"}]);                                // 93
  logger.expectNoResult();                                                                                             // 94
  handle.stop();                                                                                                       // 95
  onComplete();                                                                                                        // 96
  });                                                                                                                  // 97
});                                                                                                                    // 98
                                                                                                                       // 99
                                                                                                                       // 100
                                                                                                                       // 101
Tinytest.addAsync("observeChanges - unordered - initial adds", function (test, onComplete) {                           // 102
  var c = makeCollection();                                                                                            // 103
  withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                       // 104
  var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                 // 105
  var barid = c.insert({noodles: "good", bacon: "weird", apples: "ok"});                                               // 106
  var handle = c.find().observeChanges(logger);                                                                        // 107
  logger.expectResultUnordered([                                                                                       // 108
    {callback: "added",                                                                                                // 109
     args: [fooid, {noodles: "good", bacon: "bad", apples: "ok"}]},                                                    // 110
    {callback: "added",                                                                                                // 111
     args: [barid, {noodles: "good", bacon: "weird", apples: "ok"}]}                                                   // 112
  ]);                                                                                                                  // 113
  logger.expectNoResult();                                                                                             // 114
  handle.stop();                                                                                                       // 115
  onComplete();                                                                                                        // 116
  });                                                                                                                  // 117
});                                                                                                                    // 118
                                                                                                                       // 119
Tinytest.addAsync("observeChanges - unordered - basics", function (test, onComplete) {                                 // 120
  var c = makeCollection();                                                                                            // 121
  withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                       // 122
  var handle = c.find().observeChanges(logger);                                                                        // 123
  var barid = c.insert({thing: "stuff"});                                                                              // 124
  logger.expectResultOnly("added", [barid, {thing: "stuff"}]);                                                         // 125
                                                                                                                       // 126
  var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                 // 127
                                                                                                                       // 128
  logger.expectResultOnly("added", [fooid, {noodles: "good", bacon: "bad", apples: "ok"}]);                            // 129
                                                                                                                       // 130
  c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok"});                                              // 131
  c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok"});                                              // 132
  logger.expectResultOnly("changed",                                                                                   // 133
                      [fooid, {noodles: "alright", potatoes: "tasty", bacon: undefined}]);                             // 134
  c.remove(fooid);                                                                                                     // 135
  logger.expectResultOnly("removed", [fooid]);                                                                         // 136
  c.remove(barid);                                                                                                     // 137
  logger.expectResultOnly("removed", [barid]);                                                                         // 138
                                                                                                                       // 139
  fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                     // 140
                                                                                                                       // 141
  logger.expectResult("added", [fooid, {noodles: "good", bacon: "bad", apples: "ok"}]);                                // 142
  logger.expectNoResult();                                                                                             // 143
  handle.stop();                                                                                                       // 144
  onComplete();                                                                                                        // 145
  });                                                                                                                  // 146
});                                                                                                                    // 147
                                                                                                                       // 148
if (Meteor.isServer) {                                                                                                 // 149
  Tinytest.addAsync("observeChanges - unordered - specific fields", function (test, onComplete) {                      // 150
    var c = makeCollection();                                                                                          // 151
    withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                     // 152
      var handle = c.find({}, {fields:{noodles: 1, bacon: 1}}).observeChanges(logger);                                 // 153
      var barid = c.insert({thing: "stuff"});                                                                          // 154
      logger.expectResultOnly("added", [barid, {}]);                                                                   // 155
                                                                                                                       // 156
      var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                             // 157
                                                                                                                       // 158
      logger.expectResultOnly("added", [fooid, {noodles: "good", bacon: "bad"}]);                                      // 159
                                                                                                                       // 160
      c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok"});                                          // 161
      logger.expectResultOnly("changed",                                                                               // 162
                              [fooid, {noodles: "alright", bacon: undefined}]);                                        // 163
      c.update(fooid, {noodles: "alright", potatoes: "meh", apples: "ok"});                                            // 164
      c.remove(fooid);                                                                                                 // 165
      logger.expectResultOnly("removed", [fooid]);                                                                     // 166
      c.remove(barid);                                                                                                 // 167
      logger.expectResultOnly("removed", [barid]);                                                                     // 168
                                                                                                                       // 169
      fooid = c.insert({noodles: "good", bacon: "bad"});                                                               // 170
                                                                                                                       // 171
      logger.expectResult("added", [fooid, {noodles: "good", bacon: "bad"}]);                                          // 172
      logger.expectNoResult();                                                                                         // 173
      handle.stop();                                                                                                   // 174
      onComplete();                                                                                                    // 175
    });                                                                                                                // 176
  });                                                                                                                  // 177
                                                                                                                       // 178
  Tinytest.addAsync("observeChanges - unordered - specific fields + selector on excluded fields", function (test, onComplete) {
    var c = makeCollection();                                                                                          // 180
    withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                     // 181
      var handle = c.find({ mac: 1, cheese: 2 },                                                                       // 182
                          {fields:{noodles: 1, bacon: 1, eggs: 1}}).observeChanges(logger);                            // 183
      var barid = c.insert({thing: "stuff", mac: 1, cheese: 2});                                                       // 184
      logger.expectResultOnly("added", [barid, {}]);                                                                   // 185
                                                                                                                       // 186
      var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok", mac: 1, cheese: 2});                          // 187
                                                                                                                       // 188
      logger.expectResultOnly("added", [fooid, {noodles: "good", bacon: "bad"}]);                                      // 189
                                                                                                                       // 190
      c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok", mac: 1, cheese: 2});                       // 191
      logger.expectResultOnly("changed",                                                                               // 192
                              [fooid, {noodles: "alright", bacon: undefined}]);                                        // 193
                                                                                                                       // 194
      // Doesn't get update event, since modifies only hidden fields                                                   // 195
      c.update(fooid, {noodles: "alright", potatoes: "meh", apples: "ok", mac: 1, cheese: 2});                         // 196
      logger.expectNoResult();                                                                                         // 197
                                                                                                                       // 198
      c.remove(fooid);                                                                                                 // 199
      logger.expectResultOnly("removed", [fooid]);                                                                     // 200
      c.remove(barid);                                                                                                 // 201
      logger.expectResultOnly("removed", [barid]);                                                                     // 202
                                                                                                                       // 203
      fooid = c.insert({noodles: "good", bacon: "bad", mac: 1, cheese: 2});                                            // 204
                                                                                                                       // 205
      logger.expectResult("added", [fooid, {noodles: "good", bacon: "bad"}]);                                          // 206
      logger.expectNoResult();                                                                                         // 207
      handle.stop();                                                                                                   // 208
      onComplete();                                                                                                    // 209
    });                                                                                                                // 210
  });                                                                                                                  // 211
                                                                                                                       // 212
  Tinytest.addAsync("observeChanges - unordered - specific fields + modify on excluded fields", function (test, onComplete) {
    var c = makeCollection();                                                                                          // 214
    withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                     // 215
      var handle = c.find({ mac: 1, cheese: 2 },                                                                       // 216
                          {fields:{noodles: 1, bacon: 1, eggs: 1}}).observeChanges(logger);                            // 217
      var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok", mac: 1, cheese: 2});                          // 218
                                                                                                                       // 219
      logger.expectResultOnly("added", [fooid, {noodles: "good", bacon: "bad"}]);                                      // 220
                                                                                                                       // 221
                                                                                                                       // 222
      // Noodles go into shadow, mac appears as eggs                                                                   // 223
      c.update(fooid, {$rename: { noodles: 'shadow', apples: 'eggs' }});                                               // 224
      logger.expectResultOnly("changed",                                                                               // 225
                              [fooid, {eggs:"ok", noodles: undefined}]);                                               // 226
                                                                                                                       // 227
      c.remove(fooid);                                                                                                 // 228
      logger.expectResultOnly("removed", [fooid]);                                                                     // 229
      logger.expectNoResult();                                                                                         // 230
      handle.stop();                                                                                                   // 231
      onComplete();                                                                                                    // 232
    });                                                                                                                // 233
  });                                                                                                                  // 234
}                                                                                                                      // 235
                                                                                                                       // 236
                                                                                                                       // 237
Tinytest.addAsync("observeChanges - unordered - enters and exits result set through change", function (test, onComplete) {
  var c = makeCollection();                                                                                            // 239
  withCallbackLogger(test, ["added", "changed", "removed"], Meteor.isServer, function (logger) {                       // 240
  var handle = c.find({noodles: "good"}).observeChanges(logger);                                                       // 241
  var barid = c.insert({thing: "stuff"});                                                                              // 242
                                                                                                                       // 243
  var fooid = c.insert({noodles: "good", bacon: "bad", apples: "ok"});                                                 // 244
  logger.expectResultOnly("added", [fooid, {noodles: "good", bacon: "bad", apples: "ok"}]);                            // 245
                                                                                                                       // 246
  c.update(fooid, {noodles: "alright", potatoes: "tasty", apples: "ok"});                                              // 247
  logger.expectResultOnly("removed",                                                                                   // 248
                      [fooid]);                                                                                        // 249
  c.remove(fooid);                                                                                                     // 250
  c.remove(barid);                                                                                                     // 251
                                                                                                                       // 252
  fooid = c.insert({noodles: "ok", bacon: "bad", apples: "ok"});                                                       // 253
  c.update(fooid, {noodles: "good", potatoes: "tasty", apples: "ok"});                                                 // 254
  logger.expectResult("added", [fooid, {noodles: "good", potatoes: "tasty", apples: "ok"}]);                           // 255
  logger.expectNoResult();                                                                                             // 256
  handle.stop();                                                                                                       // 257
  onComplete();                                                                                                        // 258
  });                                                                                                                  // 259
});                                                                                                                    // 260
                                                                                                                       // 261
                                                                                                                       // 262
if (Meteor.isServer) {                                                                                                 // 263
  testAsyncMulti("observeChanges - tailable", [                                                                        // 264
    function (test, expect) {                                                                                          // 265
      var self = this;                                                                                                 // 266
      var collName = "cap_" + Random.id();                                                                             // 267
      var coll = new Meteor.Collection(collName);                                                                      // 268
      coll._createCappedCollection(1000000);                                                                           // 269
      self.xs = [];                                                                                                    // 270
      self.expects = [];                                                                                               // 271
      self.insert = function (fields) {                                                                                // 272
        coll.insert(_.extend({ts: new MongoInternals.MongoTimestamp(0, 0)},                                            // 273
                             fields));                                                                                 // 274
      };                                                                                                               // 275
                                                                                                                       // 276
      // Tailable observe shouldn't show things that are in the initial                                                // 277
      // contents.                                                                                                     // 278
      self.insert({x: 1});                                                                                             // 279
      // Wait for one added call before going to the next test function.                                               // 280
      self.expects.push(expect());                                                                                     // 281
                                                                                                                       // 282
      var cursor = coll.find({y: {$ne: 7}}, {tailable: true});                                                         // 283
      self.handle = cursor.observeChanges({                                                                            // 284
        added: function (id, fields) {                                                                                 // 285
          self.xs.push(fields.x);                                                                                      // 286
          test.notEqual(self.expects.length, 0);                                                                       // 287
          self.expects.pop()();                                                                                        // 288
        },                                                                                                             // 289
        changed: function () {                                                                                         // 290
          test.fail({unexpected: "changed"});                                                                          // 291
        },                                                                                                             // 292
        removed: function () {                                                                                         // 293
          test.fail({unexpected: "removed"});                                                                          // 294
        }                                                                                                              // 295
      });                                                                                                              // 296
                                                                                                                       // 297
      // Nothing happens synchronously.                                                                                // 298
      test.equal(self.xs, []);                                                                                         // 299
    },                                                                                                                 // 300
    function (test, expect) {                                                                                          // 301
      var self = this;                                                                                                 // 302
      // The cursors sees the first element.                                                                           // 303
      test.equal(self.xs, [1]);                                                                                        // 304
      self.xs = [];                                                                                                    // 305
                                                                                                                       // 306
      self.insert({x: 2, y: 3});                                                                                       // 307
      self.insert({x: 3, y: 7});  // filtered out by the query                                                         // 308
      self.insert({x: 4});                                                                                             // 309
      // Expect two added calls to happen.                                                                             // 310
      self.expects = [expect(), expect()];                                                                             // 311
    },                                                                                                                 // 312
    function (test, expect) {                                                                                          // 313
      var self = this;                                                                                                 // 314
      test.equal(self.xs, [2, 4]);                                                                                     // 315
      self.xs = [];                                                                                                    // 316
      self.handle.stop();                                                                                              // 317
                                                                                                                       // 318
      self.insert({x: 5});                                                                                             // 319
      // XXX This timeout isn't perfect but it's pretty hard to prove that an                                          // 320
      // event WON'T happen without something like a write fence.                                                      // 321
      Meteor.setTimeout(expect(), 1000);                                                                               // 322
    },                                                                                                                 // 323
    function (test, expect) {                                                                                          // 324
      var self = this;                                                                                                 // 325
      test.equal(self.xs, []);                                                                                         // 326
    }                                                                                                                  // 327
  ]);                                                                                                                  // 328
}                                                                                                                      // 329
                                                                                                                       // 330
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/oplog_tests.js                                                                              //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var OplogCollection = new Meteor.Collection("oplog-" + Random.id());                                                   // 1
                                                                                                                       // 2
Tinytest.add("mongo-livedata - oplog - cursorSupported", function (test) {                                             // 3
  var oplogEnabled =                                                                                                   // 4
        !!MongoInternals.defaultRemoteCollectionDriver().mongo._oplogHandle;                                           // 5
                                                                                                                       // 6
  var supported = function (expected, selector, options) {                                                             // 7
    var cursor = OplogCollection.find(selector, options);                                                              // 8
    var handle = cursor.observeChanges({added: function () {}});                                                       // 9
    // If there's no oplog at all, we shouldn't ever use it.                                                           // 10
    if (!oplogEnabled)                                                                                                 // 11
      expected = false;                                                                                                // 12
    test.equal(!!handle._multiplexer._observeDriver._usesOplog, expected);                                             // 13
    handle.stop();                                                                                                     // 14
  };                                                                                                                   // 15
                                                                                                                       // 16
  supported(true, "asdf");                                                                                             // 17
  supported(true, 1234);                                                                                               // 18
  supported(true, new Meteor.Collection.ObjectID());                                                                   // 19
                                                                                                                       // 20
  supported(true, {_id: "asdf"});                                                                                      // 21
  supported(true, {_id: 1234});                                                                                        // 22
  supported(true, {_id: new Meteor.Collection.ObjectID()});                                                            // 23
                                                                                                                       // 24
  supported(true, {foo: "asdf",                                                                                        // 25
                   bar: 1234,                                                                                          // 26
                   baz: new Meteor.Collection.ObjectID(),                                                              // 27
                   eeney: true,                                                                                        // 28
                   miney: false,                                                                                       // 29
                   moe: null});                                                                                        // 30
                                                                                                                       // 31
  supported(true, {});                                                                                                 // 32
                                                                                                                       // 33
  supported(true, {$and: [{foo: "asdf"}, {bar: "baz"}]});                                                              // 34
  supported(true, {foo: {x: 1}});                                                                                      // 35
  supported(true, {foo: {$gt: 1}});                                                                                    // 36
  supported(true, {foo: [1, 2, 3]});                                                                                   // 37
                                                                                                                       // 38
  // No $where.                                                                                                        // 39
  supported(false, {$where: "xxx"});                                                                                   // 40
  supported(false, {$and: [{foo: "adsf"}, {$where: "xxx"}]});                                                          // 41
  // No geoqueries.                                                                                                    // 42
  supported(false, {x: {$near: [1,1]}});                                                                               // 43
  // Nothing Minimongo doesn't understand.  (Minimongo happens to fail to                                              // 44
  // implement $elemMatch inside $all which MongoDB supports.)                                                         // 45
  supported(false, {x: {$all: [{$elemMatch: {y: 2}}]}});                                                               // 46
                                                                                                                       // 47
  supported(true, {}, { sort: {x:1} });                                                                                // 48
  supported(true, {}, { sort: {x:1}, limit: 5 });                                                                      // 49
  supported(false, {}, { sort: {$natural:1}, limit: 5 });                                                              // 50
  supported(false, {}, { limit: 5 });                                                                                  // 51
  supported(false, {}, { skip: 2, limit: 5 });                                                                         // 52
  supported(false, {}, { skip: 2 });                                                                                   // 53
});                                                                                                                    // 54
                                                                                                                       // 55
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/mongo-livedata/doc_fetcher_tests.js                                                                        //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var Fiber = Npm.require('fibers');                                                                                     // 1
var Future = Npm.require('fibers/future');                                                                             // 2
                                                                                                                       // 3
testAsyncMulti("mongo-livedata - doc fetcher", [                                                                       // 4
  function (test, expect) {                                                                                            // 5
    var self = this;                                                                                                   // 6
    var collName = "docfetcher-" + Random.id();                                                                        // 7
    var collection = new Meteor.Collection(collName);                                                                  // 8
    var id1 = collection.insert({x: 1});                                                                               // 9
    var id2 = collection.insert({y: 2});                                                                               // 10
                                                                                                                       // 11
    var fetcher = new MongoTest.DocFetcher(                                                                            // 12
      MongoInternals.defaultRemoteCollectionDriver().mongo);                                                           // 13
                                                                                                                       // 14
    // Test basic operation.                                                                                           // 15
    fetcher.fetch(collName, id1, Random.id(), expect(null, {_id: id1, x: 1}));                                         // 16
    fetcher.fetch(collName, "nonexistent!", Random.id(), expect(null, null));                                          // 17
                                                                                                                       // 18
    var fetched = false;                                                                                               // 19
    var cacheKey = Random.id();                                                                                        // 20
    var expected = {_id: id2, y: 2};                                                                                   // 21
    fetcher.fetch(collName, id2, cacheKey, expect(function (e, d) {                                                    // 22
      fetched = true;                                                                                                  // 23
      test.isFalse(e);                                                                                                 // 24
      test.equal(d, expected);                                                                                         // 25
    }));                                                                                                               // 26
    // The fetcher yields.                                                                                             // 27
    test.isFalse(fetched);                                                                                             // 28
                                                                                                                       // 29
    // Now ask for another document with the same cache key. Because a fetch for                                       // 30
    // that cache key is in flight, we will get the other fetch's document, not                                        // 31
    // this random document.                                                                                           // 32
    fetcher.fetch(collName, Random.id(), cacheKey, expect(function (e, d) {                                            // 33
      test.isFalse(e);                                                                                                 // 34
      test.equal(d, expected);                                                                                         // 35
    }));                                                                                                               // 36
  }                                                                                                                    // 37
]);                                                                                                                    // 38
                                                                                                                       // 39
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
