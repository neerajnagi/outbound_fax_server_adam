(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/spacebars-compiler/spacebars_tests.js                                                                    //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
Tinytest.add("spacebars - stache tags", function (test) {                                                            // 1
                                                                                                                     // 2
  var run = function (input, expected) {                                                                             // 3
    if (typeof expected === "string") {                                                                              // 4
      // test for error starting with string `expected`                                                              // 5
      var msg = '';                                                                                                  // 6
      test.throws(function () {                                                                                      // 7
        try {                                                                                                        // 8
          Spacebars.TemplateTag.parse(input);                                                                        // 9
        } catch (e) {                                                                                                // 10
          msg = e.message;                                                                                           // 11
          throw e;                                                                                                   // 12
        }                                                                                                            // 13
      });                                                                                                            // 14
      test.equal(msg.slice(0, expected.length), expected);                                                           // 15
    } else {                                                                                                         // 16
      var result = Spacebars.TemplateTag.parse(input);                                                               // 17
      test.equal(result, expected);                                                                                  // 18
    }                                                                                                                // 19
  };                                                                                                                 // 20
                                                                                                                     // 21
  run('{{foo}}', {type: 'DOUBLE', path: ['foo'], args: []});                                                         // 22
  run('{{foo3}}', {type: 'DOUBLE', path: ['foo3'], args: []});                                                       // 23
  run('{{{foo}}}', {type: 'TRIPLE', path: ['foo'], args: []});                                                       // 24
  run('{{{foo}}', "Expected `}}}`");                                                                                 // 25
  run('{{{foo', "Expected");                                                                                         // 26
  run('{{foo', "Expected");                                                                                          // 27
  run('{{ {foo}}}', "Unknown stache tag");                                                                           // 28
  run('{{{{foo}}}}', "Unknown stache tag");                                                                          // 29
  run('{{{>foo}}}', "Unknown stache tag");                                                                           // 30
  run('{{>>foo}}', "Unknown stache tag");                                                                            // 31
  run('{{! asdf }}', {type: 'COMMENT', value: ' asdf '});                                                            // 32
  run('{{ ! asdf }}', {type: 'COMMENT', value: ' asdf '});                                                           // 33
  run('{{ ! asdf }asdf', "Unclosed");                                                                                // 34
  run('{{else}}', {type: 'ELSE'});                                                                                   // 35
  run('{{ else }}', {type: 'ELSE'});                                                                                 // 36
  run('{{else x}}', "Expected");                                                                                     // 37
  run('{{else_x}}', {type: 'DOUBLE', path: ['else_x'], args: []});                                                   // 38
  run('{{/if}}', {type: 'BLOCKCLOSE', path: ['if']});                                                                // 39
  run('{{ / if }}', {type: 'BLOCKCLOSE', path: ['if']});                                                             // 40
  run('{{/if x}}', "Expected");                                                                                      // 41
  run('{{#if}}', {type: 'BLOCKOPEN', path: ['if'], args: []});                                                       // 42
  run('{{ # if }}', {type: 'BLOCKOPEN', path: ['if'], args: []});                                                    // 43
  run('{{#if_3}}', {type: 'BLOCKOPEN', path: ['if_3'], args: []});                                                   // 44
  run('{{>x}}', {type: 'INCLUSION', path: ['x'], args: []});                                                         // 45
  run('{{ > x }}', {type: 'INCLUSION', path: ['x'], args: []});                                                      // 46
  run('{{>x_3}}', {type: 'INCLUSION', path: ['x_3'], args: []});                                                     // 47
                                                                                                                     // 48
                                                                                                                     // 49
                                                                                                                     // 50
  run('{{foo 3}}', {type: 'DOUBLE', path: ['foo'], args: [['NUMBER', 3]]});                                          // 51
  run('{{ foo  3 }}', {type: 'DOUBLE', path: ['foo'], args: [['NUMBER', 3]]});                                       // 52
  run('{{#foo 3}}', {type: 'BLOCKOPEN', path: ['foo'], args: [['NUMBER', 3]]});                                      // 53
  run('{{ # foo  3 }}', {type: 'BLOCKOPEN', path: ['foo'],                                                           // 54
                         args: [['NUMBER', 3]]});                                                                    // 55
  run('{{>foo 3}}', {type: 'INCLUSION', path: ['foo'], args: [['NUMBER', 3]]});                                      // 56
  run('{{ > foo  3 }}', {type: 'INCLUSION', path: ['foo'],                                                           // 57
                         args: [['NUMBER', 3]]});                                                                    // 58
  run('{{{foo 3}}}', {type: 'TRIPLE', path: ['foo'], args: [['NUMBER', 3]]});                                        // 59
                                                                                                                     // 60
  run('{{foo bar ./foo foo/bar a.b.c baz=qux x3=.}}',                                                                // 61
      {type: 'DOUBLE', path: ['foo'],                                                                                // 62
       args: [['PATH', ['bar']],                                                                                     // 63
              ['PATH', ['.', 'foo']],                                                                                // 64
              ['PATH', ['foo', 'bar']],                                                                              // 65
              ['PATH', ['a', 'b', 'c']],                                                                             // 66
              ['PATH', ['qux'], 'baz'],                                                                              // 67
              ['PATH', ['.'], 'x3']]});                                                                              // 68
                                                                                                                     // 69
  run('{{{x 0.3 [0].[3] .4 ./[4]}}}',                                                                                // 70
      {type: 'TRIPLE', path: ['x'],                                                                                  // 71
       args: [['NUMBER', 0.3],                                                                                       // 72
              ['PATH', ['0', '3']],                                                                                  // 73
              ['NUMBER', .4],                                                                                        // 74
              ['PATH', ['.', '4']]]});                                                                               // 75
                                                                                                                     // 76
  run('{{# foo this this.x null z=null}}',                                                                           // 77
      {type: 'BLOCKOPEN', path: ['foo'],                                                                             // 78
       args: [['PATH', ['.']],                                                                                       // 79
              ['PATH', ['.', 'x']],                                                                                  // 80
              ['NULL', null],                                                                                        // 81
              ['NULL', null, 'z']]});                                                                                // 82
                                                                                                                     // 83
  run('{{./foo 3}}', {type: 'DOUBLE', path: ['.', 'foo'], args: [['NUMBER', 3]]});                                   // 84
  run('{{this/foo 3}}', {type: 'DOUBLE', path: ['.', 'foo'], args: [['NUMBER', 3]]});                                // 85
  run('{{../foo 3}}', {type: 'DOUBLE', path: ['..', 'foo'], args: [['NUMBER', 3]]});                                 // 86
  run('{{../../foo 3}}', {type: 'DOUBLE', path: ['...', 'foo'], args: [['NUMBER', 3]]});                             // 87
                                                                                                                     // 88
  run('{{foo x/..}}', "Expected");                                                                                   // 89
  run('{{foo x/.}}', "Expected");                                                                                    // 90
                                                                                                                     // 91
  run('{{#a.b.c}}', {type: 'BLOCKOPEN', path: ['a', 'b', 'c'],                                                       // 92
                     args: []});                                                                                     // 93
  run('{{> a.b.c}}', {type: 'INCLUSION', path: ['a', 'b', 'c'],                                                      // 94
                      args: []});                                                                                    // 95
                                                                                                                     // 96
  run('{{foo.[]/[]}}', {type: 'DOUBLE', path: ['foo', '', ''],                                                       // 97
                        args: []});                                                                                  // 98
  run('{{[].foo}}', "Path can't start with empty string");                                                           // 99
                                                                                                                     // 100
  run('{{foo null}}', {type: 'DOUBLE', path: ['foo'],                                                                // 101
                       args: [['NULL', null]]});                                                                     // 102
  run('{{foo false}}', {type: 'DOUBLE', path: ['foo'],                                                               // 103
                       args: [['BOOLEAN', false]]});                                                                 // 104
  run('{{foo true}}', {type: 'DOUBLE', path: ['foo'],                                                                // 105
                       args: [['BOOLEAN', true]]});                                                                  // 106
  run('{{foo "bar"}}', {type: 'DOUBLE', path: ['foo'],                                                               // 107
                        args: [['STRING', 'bar']]});                                                                 // 108
  run("{{foo 'bar'}}", {type: 'DOUBLE', path: ['foo'],                                                               // 109
                        args: [['STRING', 'bar']]});                                                                 // 110
                                                                                                                     // 111
  run('{{foo -1 -2}}', {type: 'DOUBLE', path: ['foo'],                                                               // 112
                        args: [['NUMBER', -1], ['NUMBER', -2]]});                                                    // 113
                                                                                                                     // 114
  run('{{x "\'"}}', {type: 'DOUBLE', path: ['x'], args: [['STRING', "'"]]});                                         // 115
  run('{{x \'"\'}}', {type: 'DOUBLE', path: ['x'], args: [['STRING', '"']]});                                        // 116
                                                                                                                     // 117
  run('{{> foo x=1 y=2}}',                                                                                           // 118
      {type: 'INCLUSION', path: ['foo'],                                                                             // 119
       args: [['NUMBER', 1, 'x'],                                                                                    // 120
              ['NUMBER', 2, 'y']]});                                                                                 // 121
  // spaces around '=' are fine                                                                                      // 122
  run('{{> foo x = 1 y = 2}}',                                                                                       // 123
      {type: 'INCLUSION', path: ['foo'],                                                                             // 124
       args: [['NUMBER', 1, 'x'],                                                                                    // 125
              ['NUMBER', 2, 'y']]});                                                                                 // 126
  run('{{> foo with-dashes=1 another-one=2}}',                                                                       // 127
      {type: 'INCLUSION', path: ['foo'],                                                                             // 128
       args: [['NUMBER', 1, 'with-dashes'],                                                                          // 129
              ['NUMBER', 2, 'another-one']]});                                                                       // 130
  run('{{> foo 1="keyword can start with a number"}}',                                                               // 131
      {type: 'INCLUSION', path: ['foo'],                                                                             // 132
       args: [['STRING', 'keyword can start with a number', '1']]});                                                 // 133
  run('{{> foo disallow-dashes-in-posarg}}',                                                                         // 134
      "Expected");                                                                                                   // 135
  run('{{> foo disallow-#=1}}',                                                                                      // 136
      "Expected");                                                                                                   // 137
  run('{{> foo disallow->=1}}',                                                                                      // 138
      "Expected");                                                                                                   // 139
  run('{{> foo disallow-{=1}}',                                                                                      // 140
      "Expected");                                                                                                   // 141
  run('{{> foo disallow-(=1}}',                                                                                      // 142
      "Expected");                                                                                                   // 143
  run('{{> foo disallow-}=1}}',                                                                                      // 144
      "Expected");                                                                                                   // 145
  run('{{> foo disallow-)=1}}',                                                                                      // 146
      "Expected");                                                                                                   // 147
  run('{{> foo x=1 y=2 z}}',                                                                                         // 148
      "Can't have a non-keyword argument");                                                                          // 149
                                                                                                                     // 150
  run('{{true.foo}}', "Can't use");                                                                                  // 151
  run('{{foo.this}}', "Can only use");                                                                               // 152
  run('{{./this}}', "Can only use");                                                                                 // 153
  run('{{../this}}', "Can only use");                                                                                // 154
                                                                                                                     // 155
});                                                                                                                  // 156
                                                                                                                     // 157
                                                                                                                     // 158
Tinytest.add("spacebars - Spacebars.dot", function (test) {                                                          // 159
  test.equal(Spacebars.dot(null, 'foo'), null);                                                                      // 160
  test.equal(Spacebars.dot('foo', 'foo'), undefined);                                                                // 161
  test.equal(Spacebars.dot({x:1}, 'x'), 1);                                                                          // 162
  test.equal(Spacebars.dot(                                                                                          // 163
    {x:1, y: function () { return this.x+1; }}, 'y')(), 2);                                                          // 164
  test.equal(Spacebars.dot(                                                                                          // 165
    function () {                                                                                                    // 166
      return {x:1, y: function () { return this.x+1; }};                                                             // 167
    }, 'y')(), 2);                                                                                                   // 168
                                                                                                                     // 169
  var m = 1;                                                                                                         // 170
  var mget = function () {                                                                                           // 171
    return {                                                                                                         // 172
      answer: m,                                                                                                     // 173
      getAnswer: function () {                                                                                       // 174
        return this.answer;                                                                                          // 175
      }                                                                                                              // 176
    };                                                                                                               // 177
  };                                                                                                                 // 178
  var mgetDotAnswer = Spacebars.dot(mget, 'answer');                                                                 // 179
  test.equal(mgetDotAnswer, 1);                                                                                      // 180
                                                                                                                     // 181
  m = 3;                                                                                                             // 182
  var mgetDotGetAnswer = Spacebars.dot(mget, 'getAnswer');                                                           // 183
  test.equal(mgetDotGetAnswer(), 3);                                                                                 // 184
  m = 4;                                                                                                             // 185
  test.equal(mgetDotGetAnswer(), 3);                                                                                 // 186
                                                                                                                     // 187
  var closet = {                                                                                                     // 188
    mget: mget,                                                                                                      // 189
    mget2: function () {                                                                                             // 190
      return this.mget();                                                                                            // 191
    }                                                                                                                // 192
  };                                                                                                                 // 193
                                                                                                                     // 194
  m = 5;                                                                                                             // 195
  var f1 = Spacebars.dot(closet, 'mget', 'answer');                                                                  // 196
  m = 6;                                                                                                             // 197
  var f2 = Spacebars.dot(closet, 'mget2', 'answer');                                                                 // 198
  test.equal(f2, 6);                                                                                                 // 199
  m = 8;                                                                                                             // 200
  var f3 = Spacebars.dot(closet, 'mget2', 'getAnswer');                                                              // 201
  m = 9;                                                                                                             // 202
  test.equal(f3(), 8);                                                                                               // 203
                                                                                                                     // 204
  test.equal(Spacebars.dot(0, 'abc', 'def'), 0);                                                                     // 205
  test.equal(Spacebars.dot(function () { return null; }, 'abc', 'def'), null);                                       // 206
  test.equal(Spacebars.dot(function () { return 0; }, 'abc', 'def'), 0);                                             // 207
                                                                                                                     // 208
  // test that in `foo.bar`, `bar` may be a function that takes arguments.                                           // 209
  test.equal(Spacebars.dot(                                                                                          // 210
    { one: 1, inc: function (x) { return this.one + x; } }, 'inc')(6), 7);                                           // 211
  test.equal(Spacebars.dot(                                                                                          // 212
    function () {                                                                                                    // 213
      return { one: 1, inc: function (x) { return this.one + x; } };                                                 // 214
    }, 'inc')(8), 9);                                                                                                // 215
                                                                                                                     // 216
});                                                                                                                  // 217
                                                                                                                     // 218
//////////////////////////////////////////////////                                                                   // 219
                                                                                                                     // 220
Tinytest.add("spacebars - parse", function (test) {                                                                  // 221
  test.equal(HTML.toJS(Spacebars.parse('{{foo}}')),                                                                  // 222
             'HTMLTools.Special({type: "DOUBLE", path: ["foo"]})');                                                  // 223
                                                                                                                     // 224
  test.equal(HTML.toJS(Spacebars.parse('{{!foo}}')), 'null');                                                        // 225
  test.equal(HTML.toJS(Spacebars.parse('x{{!foo}}y')), '"xy"');                                                      // 226
                                                                                                                     // 227
  test.equal(HTML.toJS(Spacebars.parse('{{#foo}}x{{/foo}}')),                                                        // 228
             'HTMLTools.Special({type: "BLOCKOPEN", path: ["foo"], content: "x"})');                                 // 229
                                                                                                                     // 230
  test.equal(HTML.toJS(Spacebars.parse('{{#foo}}{{#bar}}{{/bar}}{{/foo}}')),                                         // 231
             'HTMLTools.Special({type: "BLOCKOPEN", path: ["foo"], content: HTMLTools.Special({type: "BLOCKOPEN", path: ["bar"]})})');
                                                                                                                     // 233
  test.equal(HTML.toJS(Spacebars.parse('<div>hello</div> {{#foo}}<div>{{#bar}}world{{/bar}}</div>{{/foo}}')),        // 234
             '[HTML.DIV("hello"), " ", HTMLTools.Special({type: "BLOCKOPEN", path: ["foo"], content: HTML.DIV(HTMLTools.Special({type: "BLOCKOPEN", path: ["bar"], content: "world"}))})]');
                                                                                                                     // 236
                                                                                                                     // 237
  test.throws(function () {                                                                                          // 238
    Spacebars.parse('<a {{{x}}}></a>');                                                                              // 239
  });                                                                                                                // 240
  test.throws(function () {                                                                                          // 241
    Spacebars.parse('<a {{#if x}}{{/if}}></a>');                                                                     // 242
  });                                                                                                                // 243
  test.throws(function () {                                                                                          // 244
    Spacebars.parse('<a {{k}}={[v}}></a>');                                                                          // 245
  });                                                                                                                // 246
  test.throws(function () {                                                                                          // 247
    Spacebars.parse('<a x{{y}}></a>');                                                                               // 248
  });                                                                                                                // 249
  test.throws(function () {                                                                                          // 250
    Spacebars.parse('<a x{{y}}=z></a>');                                                                             // 251
  });                                                                                                                // 252
  test.throws(function () {                                                                                          // 253
    Spacebars.parse('<a {{> x}}></a>');                                                                              // 254
  });                                                                                                                // 255
                                                                                                                     // 256
  test.equal(HTML.toJS(Spacebars.parse('<a {{! x}} b=c{{! x}} {{! x}}></a>')),                                       // 257
             'HTML.A({b: "c"})');                                                                                    // 258
                                                                                                                     // 259
  // currently, if there are only comments, the attribute is truthy.  This is                                        // 260
  // because comments are stripped during tokenization.  If we include                                               // 261
  // comments in the token stream, these cases will become falsy for selected.                                       // 262
  test.equal(HTML.toJS(Spacebars.parse('<input selected={{!foo}}>')),                                                // 263
             'HTML.INPUT({selected: ""})');                                                                          // 264
  test.equal(HTML.toJS(Spacebars.parse('<input selected={{!foo}}{{!bar}}>')),                                        // 265
             'HTML.INPUT({selected: ""})');                                                                          // 266
                                                                                                                     // 267
});                                                                                                                  // 268
                                                                                                                     // 269
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/spacebars-compiler/compile_tests.js                                                                      //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
Tinytest.add("spacebars - compiler output", function (test) {                                                        // 1
                                                                                                                     // 2
  var run = function (input, expected) {                                                                             // 3
    if (expected.fail) {                                                                                             // 4
      var expectedMessage = expected.fail;                                                                           // 5
      // test for error starting with expectedMessage                                                                // 6
      var msg = '';                                                                                                  // 7
      test.throws(function () {                                                                                      // 8
        try {                                                                                                        // 9
          Spacebars.compile(input);                                                                                  // 10
        } catch (e) {                                                                                                // 11
          msg = e.message;                                                                                           // 12
          throw e;                                                                                                   // 13
        }                                                                                                            // 14
      });                                                                                                            // 15
      test.equal(msg.slice(0, expectedMessage.length),                                                               // 16
                 expectedMessage);                                                                                   // 17
    } else {                                                                                                         // 18
      var output = Spacebars.compile(input);                                                                         // 19
      var postProcess = function (string) {                                                                          // 20
        // remove initial and trailing parens                                                                        // 21
        string = string.replace(/^\(([\S\s]*)\)$/, '$1');                                                            // 22
        if (! (Package.minifiers && Package.minifiers.UglifyJSMinify)) {                                             // 23
          // these tests work a lot better with access to beautification,                                            // 24
          // but let's at least do some sort of test without it.                                                     // 25
          // These regexes may have to be adjusted if new tests are added.                                           // 26
                                                                                                                     // 27
          // Remove single-line comments, including line nums from build system.                                     // 28
          string = string.replace(/\/\/.*$/mg, '');                                                                  // 29
          string = string.replace(/\s+/g, ''); // kill whitespace                                                    // 30
          // collapse identical consecutive parens                                                                   // 31
          string = string.replace(/\(+/g, '(').replace(/\)+/g, ')');                                                 // 32
        }                                                                                                            // 33
        return string;                                                                                               // 34
      };                                                                                                             // 35
      // compare using Function .toString()!                                                                         // 36
      test._stringEqual(                                                                                             // 37
        postProcess(output.toString()),                                                                              // 38
        postProcess(                                                                                                 // 39
          Spacebars._beautify('(' + expected.toString() + ')')),                                                     // 40
        input);                                                                                                      // 41
    }                                                                                                                // 42
  };                                                                                                                 // 43
                                                                                                                     // 44
                                                                                                                     // 45
                                                                                                                     // 46
  run("abc",                                                                                                         // 47
      function () {                                                                                                  // 48
        var self = this;                                                                                             // 49
        return "abc";                                                                                                // 50
      });                                                                                                            // 51
                                                                                                                     // 52
  run("{{foo}}",                                                                                                     // 53
      function() {                                                                                                   // 54
        var self = this;                                                                                             // 55
        return function() {                                                                                          // 56
          return Spacebars.mustache(self.lookup("foo"));                                                             // 57
        };                                                                                                           // 58
      });                                                                                                            // 59
                                                                                                                     // 60
  run("{{foo bar}}",                                                                                                 // 61
      function() {                                                                                                   // 62
        var self = this;                                                                                             // 63
        return function() {                                                                                          // 64
          return Spacebars.mustache(self.lookup("foo"), self.lookup("bar"));                                         // 65
        };                                                                                                           // 66
      });                                                                                                            // 67
                                                                                                                     // 68
  run("{{foo x=bar}}",                                                                                               // 69
      function() {                                                                                                   // 70
        var self = this;                                                                                             // 71
        return function() {                                                                                          // 72
          return Spacebars.mustache(self.lookup("foo"), Spacebars.kw({                                               // 73
            x: self.lookup("bar")                                                                                    // 74
          }));                                                                                                       // 75
        };                                                                                                           // 76
      });                                                                                                            // 77
                                                                                                                     // 78
  run("{{foo.bar baz}}",                                                                                             // 79
      function() {                                                                                                   // 80
        var self = this;                                                                                             // 81
        return function() {                                                                                          // 82
          return Spacebars.mustache(Spacebars.dot(self.lookup("foo"), "bar"), self.lookup("baz"));                   // 83
        };                                                                                                           // 84
      });                                                                                                            // 85
                                                                                                                     // 86
  run("{{foo bar.baz}}",                                                                                             // 87
      function() {                                                                                                   // 88
        var self = this;                                                                                             // 89
        return function() {                                                                                          // 90
          return Spacebars.mustache(self.lookup("foo"), Spacebars.dot(self.lookup("bar"), "baz"));                   // 91
        };                                                                                                           // 92
      });                                                                                                            // 93
                                                                                                                     // 94
  run("{{foo x=bar.baz}}",                                                                                           // 95
      function() {                                                                                                   // 96
        var self = this;                                                                                             // 97
        return function() {                                                                                          // 98
          return Spacebars.mustache(self.lookup("foo"), Spacebars.kw({                                               // 99
            x: Spacebars.dot(self.lookup("bar"), "baz")                                                              // 100
          }));                                                                                                       // 101
        };                                                                                                           // 102
      });                                                                                                            // 103
                                                                                                                     // 104
  run("{{#foo}}abc{{/foo}}",                                                                                         // 105
      function() {                                                                                                   // 106
        var self = this;                                                                                             // 107
        return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                   // 108
          var self = this;                                                                                           // 109
          return "abc";                                                                                              // 110
        }));                                                                                                         // 111
      });                                                                                                            // 112
                                                                                                                     // 113
  run("{{#if cond}}aaa{{else}}bbb{{/if}}",                                                                           // 114
      function() {                                                                                                   // 115
        var self = this;                                                                                             // 116
        return UI.If(function () {                                                                                   // 117
          return Spacebars.call(self.lookup("cond"));                                                                // 118
        }, UI.block(function() {                                                                                     // 119
          var self = this;                                                                                           // 120
          return "aaa";                                                                                              // 121
        }), UI.block(function() {                                                                                    // 122
          var self = this;                                                                                           // 123
          return "bbb";                                                                                              // 124
        }));                                                                                                         // 125
      });                                                                                                            // 126
                                                                                                                     // 127
  run("{{> foo bar}}",                                                                                               // 128
      function() {                                                                                                   // 129
        var self = this;                                                                                             // 130
        return Spacebars.TemplateWith(function() {                                                                   // 131
          return Spacebars.call(self.lookup("bar"));                                                                 // 132
        }, UI.block(function() {                                                                                     // 133
          var self = this;                                                                                           // 134
          return Spacebars.include(self.lookupTemplate("foo"));                                                      // 135
        }));                                                                                                         // 136
      });                                                                                                            // 137
                                                                                                                     // 138
  run("{{> foo x=bar}}",                                                                                             // 139
      function() {                                                                                                   // 140
        var self = this;                                                                                             // 141
        return Spacebars.TemplateWith(function() {                                                                   // 142
          return {                                                                                                   // 143
            x: Spacebars.call(self.lookup("bar"))                                                                    // 144
          };                                                                                                         // 145
        }, UI.block(function() {                                                                                     // 146
          var self = this;                                                                                           // 147
          return Spacebars.include(self.lookupTemplate("foo"));                                                      // 148
        }));                                                                                                         // 149
      });                                                                                                            // 150
                                                                                                                     // 151
  run("{{> foo bar.baz}}",                                                                                           // 152
      function() {                                                                                                   // 153
        var self = this;                                                                                             // 154
        return Spacebars.TemplateWith(function() {                                                                   // 155
          return Spacebars.call(Spacebars.dot(self.lookup("bar"), "baz"));                                           // 156
        }, UI.block(function() {                                                                                     // 157
          var self = this;                                                                                           // 158
          return Spacebars.include(self.lookupTemplate("foo"));                                                      // 159
        }));                                                                                                         // 160
      });                                                                                                            // 161
                                                                                                                     // 162
  run("{{> foo x=bar.baz}}",                                                                                         // 163
      function() {                                                                                                   // 164
        var self = this;                                                                                             // 165
        return Spacebars.TemplateWith(function() {                                                                   // 166
          return {                                                                                                   // 167
            x: Spacebars.call(Spacebars.dot(self.lookup("bar"), "baz"))                                              // 168
          };                                                                                                         // 169
        }, UI.block(function() {                                                                                     // 170
          var self = this;                                                                                           // 171
          return Spacebars.include(self.lookupTemplate("foo"));                                                      // 172
        }));                                                                                                         // 173
      });                                                                                                            // 174
                                                                                                                     // 175
  run("{{> foo bar baz}}",                                                                                           // 176
      function() {                                                                                                   // 177
        var self = this;                                                                                             // 178
        return Spacebars.TemplateWith(function() {                                                                   // 179
          return Spacebars.dataMustache(self.lookup("bar"), self.lookup("baz"));                                     // 180
        }, UI.block(function() {                                                                                     // 181
          var self = this;                                                                                           // 182
          return Spacebars.include(self.lookupTemplate("foo"));                                                      // 183
        }));                                                                                                         // 184
      });                                                                                                            // 185
                                                                                                                     // 186
  run("{{#foo bar baz}}aaa{{/foo}}",                                                                                 // 187
      function() {                                                                                                   // 188
        var self = this;                                                                                             // 189
        return Spacebars.TemplateWith(function() {                                                                   // 190
          return Spacebars.dataMustache(self.lookup("bar"), self.lookup("baz"));                                     // 191
        }, UI.block(function() {                                                                                     // 192
          var self = this;                                                                                           // 193
          return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                 // 194
            var self = this;                                                                                         // 195
            return "aaa";                                                                                            // 196
          }));                                                                                                       // 197
        }));                                                                                                         // 198
      });                                                                                                            // 199
                                                                                                                     // 200
  run("{{#foo p.q r.s}}aaa{{/foo}}",                                                                                 // 201
      function() {                                                                                                   // 202
        var self = this;                                                                                             // 203
        return Spacebars.TemplateWith(function() {                                                                   // 204
          return Spacebars.dataMustache(Spacebars.dot(self.lookup("p"), "q"), Spacebars.dot(self.lookup("r"), "s")); // 205
        }, UI.block(function() {                                                                                     // 206
          var self = this;                                                                                           // 207
          return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                 // 208
            var self = this;                                                                                         // 209
            return "aaa";                                                                                            // 210
          }));                                                                                                       // 211
        }));                                                                                                         // 212
      });                                                                                                            // 213
                                                                                                                     // 214
  run("<a {{b}}></a>",                                                                                               // 215
      function() {                                                                                                   // 216
        var self = this;                                                                                             // 217
        return HTML.A({                                                                                              // 218
          $dynamic: [ function() {                                                                                   // 219
            return Spacebars.attrMustache(self.lookup("b"));                                                         // 220
          } ]                                                                                                        // 221
        });                                                                                                          // 222
      });                                                                                                            // 223
                                                                                                                     // 224
  run("<a {{b}} c=d{{e}}f></a>",                                                                                     // 225
      function() {                                                                                                   // 226
        var self = this;                                                                                             // 227
        return HTML.A({                                                                                              // 228
          c: [ "d", function() {                                                                                     // 229
            return Spacebars.mustache(self.lookup("e"));                                                             // 230
          }, "f" ],                                                                                                  // 231
          $dynamic: [ function() {                                                                                   // 232
            return Spacebars.attrMustache(self.lookup("b"));                                                         // 233
          } ]                                                                                                        // 234
        });                                                                                                          // 235
      });                                                                                                            // 236
                                                                                                                     // 237
  run("<asdf>{{foo}}</asdf>",                                                                                        // 238
      function () {                                                                                                  // 239
        var self = this;                                                                                             // 240
        return HTML.getTag("asdf")(function () {                                                                     // 241
          return Spacebars.mustache(self.lookup("foo"));                                                             // 242
        });                                                                                                          // 243
      });                                                                                                            // 244
                                                                                                                     // 245
  run("<textarea>{{foo}}</textarea>",                                                                                // 246
      function () {                                                                                                  // 247
        var self = this;                                                                                             // 248
        return HTML.TEXTAREA(function () {                                                                           // 249
          return Spacebars.mustache(self.lookup("foo"));                                                             // 250
        });                                                                                                          // 251
      });                                                                                                            // 252
                                                                                                                     // 253
});                                                                                                                  // 254
                                                                                                                     // 255
Tinytest.add("spacebars - compiler errors", function (test) {                                                        // 256
                                                                                                                     // 257
  var getError = function (input) {                                                                                  // 258
    try {                                                                                                            // 259
      Spacebars.compile(input);                                                                                      // 260
    } catch (e) {                                                                                                    // 261
      return e.message;                                                                                              // 262
    }                                                                                                                // 263
    test.fail("Didn't throw an error: " + input);                                                                    // 264
  };                                                                                                                 // 265
                                                                                                                     // 266
  var assertStartsWith = function (a, b) {                                                                           // 267
    test.equal(a.substring(0, b.length), b);                                                                         // 268
  };                                                                                                                 // 269
                                                                                                                     // 270
  var isError = function (input, errorStart) {                                                                       // 271
    assertStartsWith(getError(input), errorStart);                                                                   // 272
  };                                                                                                                 // 273
                                                                                                                     // 274
  isError("<input></input>",                                                                                         // 275
          "Unexpected HTML close tag.  <input> should have no close tag.");                                          // 276
  isError("{{#each foo}}<input></input>{{/foo}}",                                                                    // 277
          "Unexpected HTML close tag.  <input> should have no close tag.");                                          // 278
                                                                                                                     // 279
  isError("{{#if}}{{/if}}", "#if requires an argument");                                                             // 280
  isError("{{#with}}{{/with}}", "#with requires an argument");                                                       // 281
  isError("{{#each}}{{/each}}", "#each requires an argument");                                                       // 282
  isError("{{#unless}}{{/unless}}", "#unless requires an argument");                                                 // 283
                                                                                                                     // 284
  isError("{{0 0}}", "Expected IDENTIFIER");                                                                         // 285
                                                                                                                     // 286
  isError("{{> foo 0 0}}",                                                                                           // 287
          "First argument must be a function");                                                                      // 288
  isError("{{> foo 0 x=0}}",                                                                                         // 289
          "First argument must be a function");                                                                      // 290
  isError("{{#foo 0 0}}{{/foo}}",                                                                                    // 291
          "First argument must be a function");                                                                      // 292
  isError("{{#foo 0 x=0}}{{/foo}}",                                                                                  // 293
          "First argument must be a function");                                                                      // 294
                                                                                                                     // 295
  _.each(['asdf</br>', '{{!foo}}</br>', '{{!foo}} </br>',                                                            // 296
          'asdf</a>', '{{!foo}}</a>', '{{!foo}} </a>'], function (badFrag) {                                         // 297
            isError(badFrag, "Unexpected HTML close tag");                                                           // 298
          });                                                                                                        // 299
});                                                                                                                  // 300
                                                                                                                     // 301
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/spacebars-compiler/token_tests.js                                                                        //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
Tinytest.add("spacebars - token parsers", function (test) {                                                          // 1
                                                                                                                     // 2
  var run = function (func, input, expected) {                                                                       // 3
    var scanner = new HTMLTools.Scanner('z' + input);                                                                // 4
    // make sure the parse function respects `scanner.pos`                                                           // 5
    scanner.pos = 1;                                                                                                 // 6
    var result = func(scanner);                                                                                      // 7
    if (expected === null) {                                                                                         // 8
      test.equal(scanner.pos, 1);                                                                                    // 9
      test.equal(result, null);                                                                                      // 10
    } else {                                                                                                         // 11
      test.isTrue(scanner.isEOF());                                                                                  // 12
      test.equal(result, expected);                                                                                  // 13
    }                                                                                                                // 14
  };                                                                                                                 // 15
                                                                                                                     // 16
  var runValue = function (func, input, expectedValue) {                                                             // 17
    var expected;                                                                                                    // 18
    if (expectedValue === null)                                                                                      // 19
      expected = null;                                                                                               // 20
    else                                                                                                             // 21
      expected = { text: input, value: expectedValue };                                                              // 22
    run(func, input, expected);                                                                                      // 23
  };                                                                                                                 // 24
                                                                                                                     // 25
  var parseNumber = Spacebars._$.parseNumber;                                                                        // 26
  var parseIdentifierName = Spacebars._$.parseIdentifierName;                                                        // 27
  var parseStringLiteral = Spacebars._$.parseStringLiteral;                                                          // 28
                                                                                                                     // 29
  runValue(parseNumber, "0", 0);                                                                                     // 30
  runValue(parseNumber, "-0", 0);                                                                                    // 31
  runValue(parseNumber, "-", null);                                                                                  // 32
  runValue(parseNumber, ".a", null);                                                                                 // 33
  runValue(parseNumber, ".1", 0.1);                                                                                  // 34
  runValue(parseNumber, "1.", 1);                                                                                    // 35
  runValue(parseNumber, "1.1", 1.1);                                                                                 // 36
  runValue(parseNumber, "0x", null);                                                                                 // 37
  runValue(parseNumber, "0xa", 10);                                                                                  // 38
  runValue(parseNumber, "-0xa", -10);                                                                                // 39
  runValue(parseNumber, "1e+1", 10);                                                                                 // 40
                                                                                                                     // 41
  run(parseIdentifierName, "a", "a");                                                                                // 42
  run(parseIdentifierName, "true", "true");                                                                          // 43
  run(parseIdentifierName, "null", "null");                                                                          // 44
  run(parseIdentifierName, "if", "if");                                                                              // 45
  run(parseIdentifierName, "1", null);                                                                               // 46
  run(parseIdentifierName, "1a", null);                                                                              // 47
  run(parseIdentifierName, "+a", null);                                                                              // 48
  run(parseIdentifierName, "a1", "a1");                                                                              // 49
  run(parseIdentifierName, "a1a", "a1a");                                                                            // 50
  run(parseIdentifierName, "_a8f_f8d88_", "_a8f_f8d88_");                                                            // 51
                                                                                                                     // 52
  runValue(parseStringLiteral, '"a"', 'a');                                                                          // 53
  runValue(parseStringLiteral, '"\'"', "'");                                                                         // 54
  runValue(parseStringLiteral, '\'"\'', '"');                                                                        // 55
  runValue(parseStringLiteral, '"a\\\nb"', 'ab'); // line continuation                                               // 56
  runValue(parseStringLiteral, '"a\u0062c"', 'abc');                                                                 // 57
  // Note: IE 8 doesn't correctly parse '\v' in JavaScript.                                                          // 58
  runValue(parseStringLiteral, '"\\0\\b\\f\\n\\r\\t\\v"', '\0\b\f\n\r\t\u000b');                                     // 59
  runValue(parseStringLiteral, '"\\x41"', 'A');                                                                      // 60
  runValue(parseStringLiteral, '"\\\\"', '\\');                                                                      // 61
  runValue(parseStringLiteral, '"\\\""', '\"');                                                                      // 62
  runValue(parseStringLiteral, '"\\\'"', '\'');                                                                      // 63
  runValue(parseStringLiteral, "'\\\\'", '\\');                                                                      // 64
  runValue(parseStringLiteral, "'\\\"'", '\"');                                                                      // 65
  runValue(parseStringLiteral, "'\\\''", '\'');                                                                      // 66
});                                                                                                                  // 67
                                                                                                                     // 68
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
