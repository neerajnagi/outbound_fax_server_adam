(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                              //
// packages/htmljs/htmljs_test.js                                                               //
//                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                //
                                                                                                // 1
Tinytest.add("htmljs - getTag", function (test) {                                               // 2
  var FOO = HTML.getTag('foo');                                                                 // 3
  test.isTrue(HTML.FOO === FOO);                                                                // 4
  var x = FOO();                                                                                // 5
                                                                                                // 6
  test.equal(x.tagName, 'foo');                                                                 // 7
  test.isTrue(x instanceof HTML.FOO);                                                           // 8
  test.isTrue(x instanceof HTML.Tag);                                                           // 9
  test.equal(x.children, []);                                                                   // 10
  test.equal(x.attrs, null);                                                                    // 11
                                                                                                // 12
  test.isTrue((new FOO) instanceof HTML.FOO);                                                   // 13
  test.isTrue((new FOO) instanceof HTML.Tag);                                                   // 14
  test.isFalse((new HTML.P) instanceof HTML.FOO);                                               // 15
                                                                                                // 16
  var result = HTML.ensureTag('Bar');                                                           // 17
  test.equal(typeof result, 'undefined');                                                       // 18
  var BAR = HTML.BAR;                                                                           // 19
  test.equal(BAR().tagName, 'Bar');                                                             // 20
});                                                                                             // 21
                                                                                                // 22
Tinytest.add("htmljs - construction", function (test) {                                         // 23
  var A = HTML.getTag('a');                                                                     // 24
  var B = HTML.getTag('b');                                                                     // 25
  var C = HTML.getTag('c');                                                                     // 26
                                                                                                // 27
  var a = A(0, B({q:0}, C(A(B({})), 'foo')));                                                   // 28
  test.equal(a.tagName, 'a');                                                                   // 29
  test.equal(a.attrs, null);                                                                    // 30
  test.equal(a.children.length, 2);                                                             // 31
  test.equal(a.children[0], 0);                                                                 // 32
  var b = a.children[1];                                                                        // 33
  test.equal(b.tagName, 'b');                                                                   // 34
  test.equal(b.attrs, {q:0});                                                                   // 35
  test.equal(b.children.length, 1);                                                             // 36
  var c = b.children[0];                                                                        // 37
  test.equal(c.tagName, 'c');                                                                   // 38
  test.equal(c.attrs, null);                                                                    // 39
  test.equal(c.children.length, 2);                                                             // 40
  test.equal(c.children[0].tagName, 'a');                                                       // 41
  test.equal(c.children[0].attrs, null);                                                        // 42
  test.equal(c.children[0].children.length, 1);                                                 // 43
  test.equal(c.children[0].children[0].tagName, 'b');                                           // 44
  test.equal(c.children[0].children[0].children.length, 0);                                     // 45
  test.equal(c.children[0].children[0].attrs, {});                                              // 46
  test.equal(c.children[1], 'foo');                                                             // 47
                                                                                                // 48
  var a2 = new A({m:1}, {n:2}, B(), {o:3}, 'foo');                                              // 49
  test.equal(a2.tagName, 'a');                                                                  // 50
  test.equal(a2.attrs, {m:1});                                                                  // 51
  test.equal(a2.children.length, 4);                                                            // 52
  test.equal(a2.children[0], {n:2});                                                            // 53
  test.equal(a2.children[1].tagName, 'b');                                                      // 54
  test.equal(a2.children[2], {o:3});                                                            // 55
  test.equal(a2.children[3], 'foo');                                                            // 56
                                                                                                // 57
  test.equal(A({x:1}).children.length, 0);                                                      // 58
  var f = function () {};                                                                       // 59
  test.equal(A(new f).children.length, 1);                                                      // 60
  test.equal(A(new Date).children.length, 1);                                                   // 61
                                                                                                // 62
  test.equal(HTML.toHTML(HTML.CharRef({html: '&amp;', str: '&'})), '&amp;');                    // 63
  test.throws(function () {                                                                     // 64
    HTML.CharRef({html: '&amp;'}); // no 'str'                                                  // 65
  });                                                                                           // 66
});                                                                                             // 67
                                                                                                // 68
Tinytest.add("htmljs - utils", function (test) {                                                // 69
                                                                                                // 70
  test.notEqual("\u00c9".toLowerCase(), "\u00c9");                                              // 71
  test.equal(HTMLTools.asciiLowerCase("\u00c9"), "\u00c9");                                     // 72
                                                                                                // 73
  test.equal(HTMLTools.asciiLowerCase("Hello There"), "hello there");                           // 74
                                                                                                // 75
  test.isTrue(HTML.isVoidElement("br"));                                                        // 76
  test.isFalse(HTML.isVoidElement("div"));                                                      // 77
  test.isTrue(HTML.isKnownElement("div"));                                                      // 78
                                                                                                // 79
});                                                                                             // 80
                                                                                                // 81
Tinytest.add("htmljs - attributes", function (test) {                                           // 82
  var SPAN = HTML.SPAN;                                                                         // 83
  var amp = HTML.CharRef({html: '&amp;', str: '&'});                                            // 84
                                                                                                // 85
  test.equal(HTML.toHTML(SPAN({title: ['M', amp, 'Ms']}, 'M', amp, 'M candies')),               // 86
             '<span title="M&amp;Ms">M&amp;M candies</span>');                                  // 87
                                                                                                // 88
  // test that evaluateAttributes calls functions in both normal and dynamic attributes         // 89
  test.equal(HTML.evaluateAttributes({x: function () { return 'abc'; }}),                       // 90
             { x: 'abc' });                                                                     // 91
  test.equal(HTML.evaluateAttributes({x: function () { return 'abc'; },                         // 92
                                             $dynamic: []}),                                    // 93
             { x: 'abc' });                                                                     // 94
  test.equal(HTML.evaluateAttributes({x: function () { return 'abc'; },                         // 95
                                             $dynamic: [{ x: function () { return 'def'; }}]}), // 96
             { x: 'def' });                                                                     // 97
});                                                                                             // 98
                                                                                                // 99
Tinytest.add("htmljs - details", function (test) {                                              // 100
  test.equal(HTML.toHTML(false), "false");                                                      // 101
});                                                                                             // 102
//////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
