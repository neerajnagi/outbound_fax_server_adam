(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/ui/base_tests.js                                                                                //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
/*                                                                                                          // 2
THESE TESTS ARE OUT OF DATE.                                                                                // 3
                                                                                                            // 4
TODO: WRITE TESTS AGAINST THE LATEST base.js                                                                // 5
                                                                                                            // 6
Tinytest.add("ui - Component basics", function (test) {                                                     // 7
  var Foo = UI.Component.extend();                                                                          // 8
  var Bar = Foo.extend({x: 1, y: 2});                                                                       // 9
  var Baz = Bar.extend({y: 3, z: 4});                                                                       // 10
                                                                                                            // 11
  test.equal(typeof Foo.x, 'undefined');                                                                    // 12
  test.equal(typeof Foo.y, 'undefined');                                                                    // 13
  test.equal(typeof Foo.z, 'undefined');                                                                    // 14
  test.equal(Bar.x, 1);                                                                                     // 15
  test.equal(Bar.y, 2);                                                                                     // 16
  test.equal(typeof Bar.z, 'undefined');                                                                    // 17
  test.equal(Baz.x, 1);                                                                                     // 18
  test.equal(Baz.y, 3);                                                                                     // 19
  test.equal(Baz.z, 4);                                                                                     // 20
                                                                                                            // 21
  // _super                                                                                                 // 22
                                                                                                            // 23
  test.equal(Foo._super, UI.Component);                                                                     // 24
  test.equal(Bar._super, Foo);                                                                              // 25
  test.equal(Baz._super, Bar);                                                                              // 26
                                                                                                            // 27
  // isa                                                                                                    // 28
                                                                                                            // 29
  test.isTrue(UI.isComponent(UI.Component));                                                                // 30
  test.isTrue(UI.isComponent(Foo));                                                                         // 31
  test.isTrue(UI.isComponent(Bar));                                                                         // 32
  test.isTrue(UI.isComponent(Baz));                                                                         // 33
  test.isTrue(UI.Component.isa(UI.Component));                                                              // 34
  test.isFalse(UI.Component.isa(Foo));                                                                      // 35
  test.isFalse(UI.Component.isa(Bar));                                                                      // 36
  test.isFalse(UI.Component.isa(Baz));                                                                      // 37
  test.isTrue(Foo.isa(UI.Component));                                                                       // 38
  test.isTrue(Foo.isa(Foo));                                                                                // 39
  test.isFalse(Foo.isa(Bar));                                                                               // 40
  test.isFalse(Foo.isa(Baz));                                                                               // 41
  test.isTrue(Bar.isa(UI.Component));                                                                       // 42
  test.isTrue(Bar.isa(Foo));                                                                                // 43
  test.isTrue(Bar.isa(Bar));                                                                                // 44
  test.isFalse(Bar.isa(Baz));                                                                               // 45
  test.isTrue(Baz.isa(UI.Component));                                                                       // 46
  test.isTrue(Baz.isa(Foo));                                                                                // 47
  test.isTrue(Baz.isa(Bar));                                                                                // 48
  test.isTrue(Baz.isa(Baz));                                                                                // 49
                                                                                                            // 50
  test.isFalse(UI.isComponent({}));                                                                         // 51
  test.isFalse(UI.isComponent(null));                                                                       // 52
  test.isFalse(UI.isComponent());                                                                           // 53
  test.isFalse(UI.isComponent(function () {}));                                                             // 54
  test.isFalse(Foo.isa({}));                                                                                // 55
  test.isFalse(Foo.isa(null));                                                                              // 56
  test.isFalse(Foo.isa());                                                                                  // 57
  test.isFalse(Foo.isa(function () {}));                                                                    // 58
                                                                                                            // 59
  // guid                                                                                                   // 60
                                                                                                            // 61
  var a = UI.Component.guid,                                                                                // 62
      b = Foo.guid,                                                                                         // 63
      c = Bar.guid,                                                                                         // 64
      d = Baz.guid;                                                                                         // 65
                                                                                                            // 66
  test.isTrue(a > 0);                                                                                       // 67
  test.isTrue(b > 0);                                                                                       // 68
  test.isTrue(c > 0);                                                                                       // 69
  test.isTrue(d > 0);                                                                                       // 70
  test.isTrue(a !== b);                                                                                     // 71
  test.isTrue(a !== c);                                                                                     // 72
  test.isTrue(a !== d);                                                                                     // 73
  test.isTrue(b !== c);                                                                                     // 74
  test.isTrue(b !== d);                                                                                     // 75
  test.isTrue(c !== d);                                                                                     // 76
});                                                                                                         // 77
                                                                                                            // 78
Tinytest.add("ui - Component init/destroy", function (test) {                                               // 79
  var buf = [];                                                                                             // 80
                                                                                                            // 81
  var x = UI.Component.extend({                                                                             // 82
    init: function () {                                                                                     // 83
      test.isTrue(this.isInited);                                                                           // 84
      test.isFalse(this.isAssembled);                                                                       // 85
      test.isFalse(this.isDestroyed);                                                                       // 86
      buf.push('init');                                                                                     // 87
    },                                                                                                      // 88
    destroyed: function () {                                                                                // 89
      test.isTrue(this.isInited);                                                                           // 90
      test.isFalse(this.isAssembled);                                                                       // 91
      test.isTrue(this.isDestroyed);                                                                        // 92
      buf.push('destroyed');                                                                                // 93
    }                                                                                                       // 94
  });                                                                                                       // 95
  test.isFalse(this.isInited);                                                                              // 96
  test.isFalse(this.isAssembled);                                                                           // 97
  test.isFalse(this.isDestroyed);                                                                           // 98
  test.equal(buf, []);                                                                                      // 99
  x.makeRoot();                                                                                             // 100
  test.equal(buf, ['init']);                                                                                // 101
  x.destroy();                                                                                              // 102
  test.equal(buf, ['init', 'destroyed']);                                                                   // 103
                                                                                                            // 104
  buf.length = 0;                                                                                           // 105
  x = UI.Component.extend({                                                                                 // 106
    init: function () { buf.push('init'); },                                                                // 107
    destroyed: function () { buf.push('destroyed'); }                                                       // 108
  });                                                                                                       // 109
  test.throws(function () {                                                                                 // 110
    x.destroy();                                                                                            // 111
  });                                                                                                       // 112
  x.makeRoot();                                                                                             // 113
  test.throws(function () {                                                                                 // 114
    x.makeRoot();                                                                                           // 115
  });                                                                                                       // 116
  test.throws(function () {                                                                                 // 117
    var y = x.extend();                                                                                     // 118
  });                                                                                                       // 119
  test.equal(buf, ['init']);                                                                                // 120
  x.destroy();                                                                                              // 121
  x.destroy();                                                                                              // 122
  test.equal(buf, ['init', 'destroyed']);                                                                   // 123
  test.throws(function () {                                                                                 // 124
    x.makeRoot();                                                                                           // 125
  });                                                                                                       // 126
  test.throws(function () {                                                                                 // 127
    var y = x.extend();                                                                                     // 128
  });                                                                                                       // 129
                                                                                                            // 130
  buf.length = 0;                                                                                           // 131
  x = UI.Component.extend({                                                                                 // 132
    init: function () { buf.push('init'); },                                                                // 133
    destroyed: function () { buf.push('destroyed'); }                                                       // 134
  });                                                                                                       // 135
  var y = x.extend({                                                                                        // 136
    init: function () { buf.push('init2'); },                                                               // 137
    destroyed: function () { buf.push('destroyed2'); }                                                      // 138
  });                                                                                                       // 139
  test.equal(buf, []);                                                                                      // 140
  y.makeRoot();                                                                                             // 141
  test.equal(buf, ['init', 'init2']);                                                                       // 142
  y.destroy();                                                                                              // 143
  test.equal(buf, ['init', 'init2', 'destroyed', 'destroyed2']);                                            // 144
                                                                                                            // 145
  buf.length = 0;                                                                                           // 146
  var z = x.extend();                                                                                       // 147
  z.makeRoot();                                                                                             // 148
  z.destroy();                                                                                              // 149
  test.equal(buf, ['init', 'destroyed']);                                                                   // 150
});                                                                                                         // 151
                                                                                                            // 152
Tinytest.add("ui - Component add/remove", function (test) {                                                 // 153
  var x = UI.Component.extend();                                                                            // 154
  var y = UI.Component.extend();                                                                            // 155
                                                                                                            // 156
  test.throws(function () {                                                                                 // 157
    x.add(y);                                                                                               // 158
  });                                                                                                       // 159
                                                                                                            // 160
  test.isFalse(x.isInited);                                                                                 // 161
  x.makeRoot();                                                                                             // 162
  test.isTrue(x.isInited);                                                                                  // 163
  test.isFalse(y.isInited);                                                                                 // 164
  test.isFalse(x.hasChild(y));                                                                              // 165
  test.equal(_.keys(x.children), []);                                                                       // 166
  test.equal(_.keys(y.children), []);                                                                       // 167
                                                                                                            // 168
  x.add(y);                                                                                                 // 169
  test.isTrue(y.isInited);                                                                                  // 170
  test.equal(y.parent, x);                                                                                  // 171
  test.isTrue(x.hasChild(y));                                                                               // 172
  test.isFalse(y.hasChild(x));                                                                              // 173
  test.equal(_.keys(x.children), [String(y.guid)]);                                                         // 174
  test.equal(_.keys(y.children), []);                                                                       // 175
  test.equal(x.children[y.guid], y);                                                                        // 176
                                                                                                            // 177
  var z = UI.Component.extend();                                                                            // 178
  x.add(z);                                                                                                 // 179
  test.isTrue(z.isInited);                                                                                  // 180
  test.equal(z.parent, x);                                                                                  // 181
  test.isTrue(x.hasChild(z));                                                                               // 182
  test.isFalse(z.hasChild(x));                                                                              // 183
  test.equal(_.keys(x.children).sort(),                                                                     // 184
             [String(y.guid), String(z.guid)].sort());                                                      // 185
  test.equal(_.keys(z.children), []);                                                                       // 186
  test.equal(x.children[z.guid], z);                                                                        // 187
                                                                                                            // 188
  x.remove(y);                                                                                              // 189
  z.remove();                                                                                               // 190
  test.isFalse(x.hasChild(y));                                                                              // 191
  test.isFalse(x.hasChild(z));                                                                              // 192
  test.equal(_.keys(x.children), []);                                                                       // 193
  // children are destroyed                                                                                 // 194
  test.isTrue(y.isDestroyed);                                                                               // 195
  test.isTrue(z.isDestroyed);                                                                               // 196
  // parent pointers remain                                                                                 // 197
  test.equal(y.parent, x);                                                                                  // 198
  test.equal(z.parent, x);                                                                                  // 199
  test.throws(function () {                                                                                 // 200
    y.remove();                                                                                             // 201
  });                                                                                                       // 202
  test.throws(function () {                                                                                 // 203
    z.remove();                                                                                             // 204
  });                                                                                                       // 205
  test.throws(function () {                                                                                 // 206
    x.remove(y);                                                                                            // 207
  });                                                                                                       // 208
  test.throws(function () {                                                                                 // 209
    x.remove(z);                                                                                            // 210
  });                                                                                                       // 211
});                                                                                                         // 212
                                                                                                            // 213
*/                                                                                                          // 214
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/ui/domrange_tests.js                                                                            //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
var DomRange = UI.DomRange;                                                                                 // 2
var parseHTML = UI.DomBackend.parseHTML;                                                                    // 3
                                                                                                            // 4
// fake component; DomRange host                                                                            // 5
var Comp = function (which) {                                                                               // 6
  this.which = which;                                                                                       // 7
  this.dom = new DomRange;                                                                                  // 8
  this.dom.component = this;                                                                                // 9
};                                                                                                          // 10
                                                                                                            // 11
var isStartMarker = function (n) {                                                                          // 12
  return (n.$ui && n === n.$ui.start);                                                                      // 13
};                                                                                                          // 14
                                                                                                            // 15
var isEndMarker = function (n) {                                                                            // 16
  return (n.$ui && n === n.$ui.end);                                                                        // 17
};                                                                                                          // 18
                                                                                                            // 19
var inDocument = function (range, func) {                                                                   // 20
  var onscreen = document.createElement("DIV");                                                             // 21
  onscreen.style.display = 'none';                                                                          // 22
  document.body.appendChild(onscreen);                                                                      // 23
  DomRange.insert(range, onscreen);                                                                         // 24
  try {                                                                                                     // 25
    func(range);                                                                                            // 26
  } finally {                                                                                               // 27
    document.body.removeChild(onscreen);                                                                    // 28
  }                                                                                                         // 29
};                                                                                                          // 30
                                                                                                            // 31
var htmlRange = function (html) {                                                                           // 32
  var r = new DomRange;                                                                                     // 33
  _.each(parseHTML(html), function (node) {                                                                 // 34
    r.add(node);                                                                                            // 35
  });                                                                                                       // 36
  return r;                                                                                                 // 37
};                                                                                                          // 38
                                                                                                            // 39
Tinytest.add("ui - DomRange - basic", function (test) {                                                     // 40
  var r = new DomRange;                                                                                     // 41
  r.which = 'R';                                                                                            // 42
                                                                                                            // 43
  // `r.start` and `r.end` -- accessed via                                                                  // 44
  // `r.startNode() and `r.endNode()` -- are adjacent empty                                                 // 45
  // text nodes used as markers.  They are initially created                                                // 46
  // in a DocumentFragment or other offscreen container.                                                    // 47
  // At all times, the members of a DomRange have the same                                                  // 48
  // parent element (`r.parentNode()`), though this element                                                 // 49
  // may change (typically just once when the DomRange is                                                   // 50
  // first put into the DOM).                                                                               // 51
  var rStart = r.startNode();                                                                               // 52
  var rEnd = r.endNode();                                                                                   // 53
                                                                                                            // 54
  test.isTrue(isStartMarker(rStart));                                                                       // 55
  test.isTrue(isEndMarker(rEnd));                                                                           // 56
  test.equal(rStart.nextSibling, rEnd);                                                                     // 57
  test.isTrue(rStart.parentNode);                                                                           // 58
  test.equal(r.parentNode(), rStart.parentNode);                                                            // 59
                                                                                                            // 60
  test.equal(typeof r.members, 'object');                                                                   // 61
  test.equal(_.keys(r.members).length, 0);                                                                  // 62
                                                                                                            // 63
  test.equal(rStart.$ui, r);                                                                                // 64
  test.equal(rEnd.$ui, r);                                                                                  // 65
                                                                                                            // 66
  // add a node                                                                                             // 67
  var div = document.createElement("DIV");                                                                  // 68
  r.add(div);                                                                                               // 69
                                                                                                            // 70
  test.equal(_.keys(r.members).length, 1);                                                                  // 71
  test.equal(div.previousSibling, rStart);                                                                  // 72
  test.equal(div.nextSibling, rEnd);                                                                        // 73
  test.equal(div.$ui, r);                                                                                   // 74
                                                                                                            // 75
  // add a subrange                                                                                         // 76
  var s = new DomRange;                                                                                     // 77
  s.which = 'S';                                                                                            // 78
  var span = document.createElement("SPAN");                                                                // 79
  s.add(span);                                                                                              // 80
  r.add(s);                                                                                                 // 81
  test.equal(_.keys(r.members).length, 2);                                                                  // 82
  test.isFalse(r.owner);                                                                                    // 83
  test.equal(s.owner, r);                                                                                   // 84
                                                                                                            // 85
  // DOM should go: rStart, DIV, sStart, SPAN, sEnd, rEnd.                                                  // 86
  test.equal(span.previousSibling, s.startNode());                                                          // 87
  test.equal(span.nextSibling, s.endNode());                                                                // 88
  test.equal(span.nextSibling.nextSibling, rEnd);                                                           // 89
  test.equal(span.previousSibling.previousSibling,                                                          // 90
             div);                                                                                          // 91
  test.equal(span.$ui, s);                                                                                  // 92
                                                                                                            // 93
  // eachMember                                                                                             // 94
  var buf = [];                                                                                             // 95
  r.eachMember(function (node) {                                                                            // 96
    buf.push(node.nodeName);                                                                                // 97
  }, function (range) {                                                                                     // 98
    buf.push('range ' + range.which);                                                                       // 99
  });                                                                                                       // 100
  buf.sort();                                                                                               // 101
  test.equal(buf, ['DIV', 'range S']);                                                                      // 102
                                                                                                            // 103
  // removal                                                                                                // 104
  s.remove();                                                                                               // 105
  test.isFalse(s.owner);                                                                                    // 106
  // sStart, SPAN, sEnd are gone from the DOM.                                                              // 107
  test.equal(rStart.nextSibling, div);                                                                      // 108
  test.equal(rEnd.previousSibling, div);                                                                    // 109
  // `r` still has two members                                                                              // 110
  test.equal(_.keys(r.members).length, 2);                                                                  // 111
  // until we refresh                                                                                       // 112
  r.refresh();                                                                                              // 113
  test.equal(_.keys(r.members).length, 1);                                                                  // 114
  // remove all                                                                                             // 115
  r.removeAll();                                                                                            // 116
  test.equal(rStart.nextSibling, rEnd);                                                                     // 117
  test.equal(_.keys(r.members).length, 0);                                                                  // 118
});                                                                                                         // 119
                                                                                                            // 120
Tinytest.add("ui - DomRange - shuffling", function (test) {                                                 // 121
  var r = new DomRange;                                                                                     // 122
                                                                                                            // 123
  var B = document.createElement("B");                                                                      // 124
  var I = document.createElement("I");                                                                      // 125
  var U = document.createElement("U");                                                                      // 126
                                                                                                            // 127
  r.add('B', B);                                                                                            // 128
  r.add('I', I);                                                                                            // 129
  r.add('U', U);                                                                                            // 130
                                                                                                            // 131
  var spellDom = function () {                                                                              // 132
    var frag = r.parentNode();                                                                              // 133
    var str = '';                                                                                           // 134
    _.each(frag.childNodes, function (n) {                                                                  // 135
      if (n.nodeType === 3 || isStartMarker(n) ||                                                           // 136
          isEndMarker(n)) {                                                                                 // 137
        if (isStartMarker(n))                                                                               // 138
          str += '(';                                                                                       // 139
        else if (isEndMarker(n))                                                                            // 140
          str += ')';                                                                                       // 141
        else                                                                                                // 142
          str += '-';                                                                                       // 143
      } else {                                                                                              // 144
        if (n.$ui.component && n.$ui.component.which)                                                       // 145
          str += n.$ui.component.which;                                                                     // 146
        else                                                                                                // 147
          str += (n.nodeName || '?');                                                                       // 148
      }                                                                                                     // 149
    });                                                                                                     // 150
    return str;                                                                                             // 151
  };                                                                                                        // 152
                                                                                                            // 153
  test.equal(spellDom(), '(BIU)');                                                                          // 154
  r.moveBefore('B');                                                                                        // 155
  test.equal(spellDom(), '(IUB)');                                                                          // 156
  r.moveBefore('I', 'U');                                                                                   // 157
  test.equal(spellDom(), '(IUB)');                                                                          // 158
  r.moveBefore('I', 'B');                                                                                   // 159
  test.equal(spellDom(), '(UIB)');                                                                          // 160
  r.moveBefore('B', 'U');                                                                                   // 161
  test.equal(spellDom(), '(BUI)');                                                                          // 162
  r.moveBefore('U', null);                                                                                  // 163
  test.equal(spellDom(), '(BIU)');                                                                          // 164
                                                                                                            // 165
  test.equal(B.$ui, r);                                                                                     // 166
                                                                                                            // 167
  // add some member rangers, with host objects                                                             // 168
  var X = new Comp('X');                                                                                    // 169
  var Y = new Comp('Y');                                                                                    // 170
  var Z = new Comp('Z');                                                                                    // 171
  r.add('X', X.dom, 'I');                                                                                   // 172
  X.dom.add(document.createElement("SPAN"));                                                                // 173
  Y.dom.add(document.createElement("SPAN"));                                                                // 174
  Z.dom.add(document.createElement("SPAN"));                                                                // 175
  r.add('Y', Y.dom, 'U');                                                                                   // 176
  r.add('Z', Z.dom);                                                                                        // 177
                                                                                                            // 178
  test.equal(spellDom(), '(B(X)I(Y)U(Z))');                                                                 // 179
                                                                                                            // 180
  r.add([document.createElement('A'),                                                                       // 181
         document.createElement('A')], 'X');                                                                // 182
                                                                                                            // 183
  test.equal(spellDom(), '(BAA(X)I(Y)U(Z))');                                                               // 184
                                                                                                            // 185
  r.moveBefore('I', 'X');                                                                                   // 186
  r.moveBefore('X', 'B');                                                                                   // 187
  r.moveBefore('Z', 'U');                                                                                   // 188
  r.moveBefore('U', 'Y');                                                                                   // 189
  test.equal(spellDom(), '((X)BAAIU(Y)(Z))');                                                               // 190
                                                                                                            // 191
                                                                                                            // 192
  r.moveBefore('Z', 'X');                                                                                   // 193
  r.moveBefore('Y', 'X');                                                                                   // 194
  test.equal(spellDom(), '((Z)(Y)(X)BAAIU)');                                                               // 195
                                                                                                            // 196
  test.isTrue(r.get('X') === X.dom);                                                                        // 197
  test.isTrue(r.get('Y') === Y.dom);                                                                        // 198
  test.isTrue(r.get('Z') === Z.dom);                                                                        // 199
  test.isTrue(r.get('B') === B);                                                                            // 200
  test.isTrue(r.get('I') === I);                                                                            // 201
  test.isTrue(r.get('U') === U);                                                                            // 202
                                                                                                            // 203
  test.isFalse(r.owner);                                                                                    // 204
  test.isTrue(X.dom.owner === r);                                                                           // 205
  test.isTrue(Y.dom.owner === r);                                                                           // 206
  test.isTrue(Z.dom.owner === r);                                                                           // 207
                                                                                                            // 208
  r.remove('Y');                                                                                            // 209
  test.equal(spellDom(), '((Z)(X)BAAIU)');                                                                  // 210
  test.equal(r.get('Y'), null);                                                                             // 211
                                                                                                            // 212
  r.remove('X');                                                                                            // 213
  test.equal(spellDom(), '((Z)BAAIU)');                                                                     // 214
                                                                                                            // 215
  r.removeAll();                                                                                            // 216
  test.equal(spellDom(), '()');                                                                             // 217
});                                                                                                         // 218
                                                                                                            // 219
Tinytest.add("ui - DomRange - nested", function (test) {                                                    // 220
  var r = new DomRange;                                                                                     // 221
                                                                                                            // 222
  var spellDom = function () {                                                                              // 223
    var frag = r.parentNode();                                                                              // 224
    var str = '';                                                                                           // 225
    _.each(frag.childNodes, function (n) {                                                                  // 226
      var ui = n.$ui;                                                                                       // 227
      if (isStartMarker(n))                                                                                 // 228
        str += (ui.component ? ui.component.which : '(');                                                   // 229
      else if (isEndMarker(n))                                                                              // 230
        str += (ui.component ? ui.component.which.toLowerCase() : ')');                                     // 231
      else                                                                                                  // 232
        str += '?';                                                                                         // 233
    });                                                                                                     // 234
    return str;                                                                                             // 235
  };                                                                                                        // 236
                                                                                                            // 237
  // nest empty ranges; should work even though                                                             // 238
  // there are no element nodes                                                                             // 239
  var A,B,C,D,E,F;                                                                                          // 240
                                                                                                            // 241
  test.equal(spellDom(), '()');                                                                             // 242
  r.add((A = new Comp('A')).dom);                                                                           // 243
  test.equal(spellDom(), '(Aa)');                                                                           // 244
  r.add('B', (B = new Comp('B')).dom);                                                                      // 245
  r.add('C', (C = new Comp('C')).dom, 'B');                                                                 // 246
  test.equal(spellDom(), '(AaCcBb)');                                                                       // 247
                                                                                                            // 248
  r.get('B').add('D', (D = new Comp('D')).dom);                                                             // 249
  D.dom.add('E', (E = new Comp('E')).dom);                                                                  // 250
  test.equal(spellDom(), '(AaCcBDEedb)');                                                                   // 251
  B.dom.add('F', (F = new Comp('F')).dom);                                                                  // 252
  test.equal(spellDom(), '(AaCcBDEedFfb)');                                                                 // 253
                                                                                                            // 254
  r.moveBefore('B', 'C');                                                                                   // 255
  test.equal(spellDom(), '(AaBDEedFfbCc)');                                                                 // 256
  B.dom.moveBefore('D', null);                                                                              // 257
  test.equal(spellDom(), '(AaBFfDEedbCc)');                                                                 // 258
  r.moveBefore('C', 'B');                                                                                   // 259
  test.equal(spellDom(), '(AaCcBFfDEedb)');                                                                 // 260
  D.dom.remove('E');                                                                                        // 261
  test.equal(spellDom(), '(AaCcBFfDdb)');                                                                   // 262
  r.remove('B');                                                                                            // 263
  test.equal(spellDom(), '(AaCc)');                                                                         // 264
                                                                                                            // 265
  test.isFalse(r.owner);                                                                                    // 266
  test.equal(A.dom.owner, r);                                                                               // 267
  test.equal(C.dom.owner, r);                                                                               // 268
});                                                                                                         // 269
                                                                                                            // 270
Tinytest.add("ui - DomRange - external moves", function (test) {                                            // 271
  // In this one, uppercase letters are div elements,                                                       // 272
  // lowercase letters are marker text nodes, as follows:                                                   // 273
  //                                                                                                        // 274
  // a-X-b - c-d-Y-Z-e-f - g-h-i-W-j-k-l V                                                                  // 275
  //                                                                                                        // 276
  // In other words, one DomRange containing an element (X),                                                // 277
  // then two nested DomRanges containing two elements (Y,Z),                                               // 278
  // etc.                                                                                                   // 279
                                                                                                            // 280
  var wsp = function () {                                                                                   // 281
    return document.createTextNode(' ');                                                                    // 282
  };                                                                                                        // 283
                                                                                                            // 284
  var X = document.createElement("DIV");                                                                    // 285
  X.id = 'X';                                                                                               // 286
  var Y = document.createElement("DIV");                                                                    // 287
  Y.id = 'Y';                                                                                               // 288
  var Z = document.createElement("DIV");                                                                    // 289
  Z.id = 'Z';                                                                                               // 290
  var W = document.createElement("DIV");                                                                    // 291
  W.id = 'W';                                                                                               // 292
  var V = document.createElement("DIV");                                                                    // 293
  V.id = 'V';                                                                                               // 294
                                                                                                            // 295
  var ab = new Comp('ab');                                                                                  // 296
  ab.dom.add(wsp());                                                                                        // 297
  ab.dom.add('X', X);                                                                                       // 298
  ab.dom.add(wsp());                                                                                        // 299
  var cf = new Comp('cf');                                                                                  // 300
  var de = new Comp('de');                                                                                  // 301
  de.dom.add(wsp());                                                                                        // 302
  de.dom.add('Y', Y);                                                                                       // 303
  de.dom.add(wsp());                                                                                        // 304
  de.dom.add('Z', Z);                                                                                       // 305
  de.dom.add(wsp());                                                                                        // 306
  cf.dom.add(wsp());                                                                                        // 307
  cf.dom.add('de', de.dom);                                                                                 // 308
  cf.dom.add(wsp());                                                                                        // 309
  var gl = new Comp('gl');                                                                                  // 310
  var hk = new Comp('hk');                                                                                  // 311
  var ij = new Comp('ij');                                                                                  // 312
  ij.dom.add(wsp());                                                                                        // 313
  ij.dom.add('W', W);                                                                                       // 314
  ij.dom.add(wsp());                                                                                        // 315
  // i-W-j                                                                                                  // 316
  test.equal(ij.dom.getNodes().length, 5);                                                                  // 317
  gl.dom.add(wsp());                                                                                        // 318
  gl.dom.add('hk', hk.dom);                                                                                 // 319
  gl.dom.add(wsp());                                                                                        // 320
  // g-hk-l                                                                                                 // 321
  test.equal(gl.dom.getNodes().length, 6);                                                                  // 322
  hk.dom.add(wsp());                                                                                        // 323
  hk.dom.add('ij', ij.dom);                                                                                 // 324
  hk.dom.add(wsp());                                                                                        // 325
  // h-i-W-j-k                                                                                              // 326
  test.equal(hk.dom.getNodes().length, 9);                                                                  // 327
  // g-h-i-W-j-k-l                                                                                          // 328
  test.equal(gl.dom.getNodes().length, 13);                                                                 // 329
                                                                                                            // 330
  var r = new DomRange;                                                                                     // 331
  r.add('ab', ab.dom);                                                                                      // 332
  r.add(wsp());                                                                                             // 333
  r.add('cf', cf.dom);                                                                                      // 334
  r.add(wsp());                                                                                             // 335
  r.add('gl', gl.dom);                                                                                      // 336
  r.add('V', V);                                                                                            // 337
                                                                                                            // 338
  var spellDom = function () {                                                                              // 339
    var frag = r.parentNode();                                                                              // 340
    var str = '';                                                                                           // 341
    _.each(frag.childNodes, function (n) {                                                                  // 342
      var ui = n.$ui;                                                                                       // 343
      if (isStartMarker(n))                                                                                 // 344
        str += (ui.component ? ui.component.which.charAt(0) : '(');                                         // 345
      else if (isEndMarker(n))                                                                              // 346
        str += (ui.component ? ui.component.which.charAt(1) : ')');                                         // 347
      else if (n.nodeType === 3)                                                                            // 348
        str += '-';                                                                                         // 349
      else                                                                                                  // 350
        str += (n.id || '?');                                                                               // 351
    });                                                                                                     // 352
    return str;                                                                                             // 353
  };                                                                                                        // 354
  var strip = function (str) {                                                                              // 355
    return str.replace(/[^-\w()]+/g, '');                                                                   // 356
  };                                                                                                        // 357
                                                                                                            // 358
  test.equal(spellDom(),                                                                                    // 359
             strip('(a-X-b - c-d-Y-Z-e-f - g-h-i-W-j-k-l V)'));                                             // 360
                                                                                                            // 361
  test.isTrue(ab.dom.owner === r);                                                                          // 362
  test.isTrue(cf.dom.owner === r);                                                                          // 363
  test.isTrue(de.dom.owner === cf.dom);                                                                     // 364
  test.isTrue(gl.dom.owner === r);                                                                          // 365
  test.isTrue(hk.dom.owner === gl.dom);                                                                     // 366
  test.isTrue(ij.dom.owner === hk.dom);                                                                     // 367
                                                                                                            // 368
  // all right, now let's mess around with these elements!                                                  // 369
                                                                                                            // 370
  $([Y,Z]).insertBefore(X);                                                                                 // 371
                                                                                                            // 372
  // jQuery lifted Y,Z right out and stuck them before X                                                    // 373
  test.equal(spellDom(),                                                                                    // 374
             strip('(a-YZX-b - c-d---e-f - g-h-i-W-j-k-l V)'));                                             // 375
                                                                                                            // 376
  r.moveBefore('cf', 'ab');                                                                                 // 377
                                                                                                            // 378
  // the move causes a refresh of `ab` and `cf` and their                                                   // 379
  // descendent members, re-establishing proper organization                                                // 380
  // (ignoring whitespace textnodes)                                                                        // 381
  test.equal(spellDom(),                                                                                    // 382
             strip('(- cdYZef aX-b ------- g-h-i-W-j-k-l V)'));                                             // 383
                                                                                                            // 384
  $(W).insertBefore(X);                                                                                     // 385
                                                                                                            // 386
  test.equal(spellDom(),                                                                                    // 387
             strip('(- cdYZef aWX-b ------- g-h-i--j-k-l V)'));                                             // 388
                                                                                                            // 389
  $(Z).insertBefore(W);                                                                                     // 390
                                                                                                            // 391
  test.equal(spellDom(),                                                                                    // 392
             strip('(- cdYef aZWX-b ------- g-h-i--j-k-l V)'));                                             // 393
                                                                                                            // 394
  r.moveBefore('ab', 'cf');                                                                                 // 395
                                                                                                            // 396
  // WOW!  `ab` and `cf` have been fixed.  Here's what                                                      // 397
  // happened:                                                                                              // 398
  // - Because `cf` is serving as an insertion point, it                                                    // 399
  //   is refreshed first, and it recursively refreshes                                                     // 400
  //   `de`.  This causes `e` and then `f` to move to the                                                   // 401
  //   right of `Z`.  There's still `a` floating in the middle.                                             // 402
  // - Then `ab` is refreshed.  This moves `a` to right before                                              // 403
  //   `X`.                                                                                                 // 404
  // - Finally, `aX-b` is moved before `c`.                                                                 // 405
  test.equal(spellDom(),                                                                                    // 406
             strip('(- aX-b cdYZef W ------- g-h-i--j-k-l V)'));                                            // 407
                                                                                                            // 408
  r.moveBefore('ab', 'gl');                                                                                 // 409
                                                                                                            // 410
  // Because `gl` is being used as a reference point,                                                       // 411
  // it is refreshed to contain `W`.                                                                        // 412
  // Because the `-` that was initial came from `ab`,                                                       // 413
  // it is recaptured.                                                                                      // 414
  test.equal(spellDom(),                                                                                    // 415
             strip('(cdYZef a-X-b ghiWjkl ------------- V)'));                                              // 416
                                                                                                            // 417
  $(Z).insertBefore(X);                                                                                     // 418
                                                                                                            // 419
  test.equal(spellDom(),                                                                                    // 420
             strip('(cdYef a-ZX-b ghiWjkl ------------- V)'));                                              // 421
                                                                                                            // 422
  r.moveBefore('gl', 'cf');                                                                                 // 423
                                                                                                            // 424
  // Note that the `a` is still misplaced here.                                                             // 425
  test.equal(spellDom(),                                                                                    // 426
             strip('(ghiWjkl cdY a-ZefX-b ------------- V)'));                                              // 427
                                                                                                            // 428
  r.moveBefore('cf', 'V');                                                                                  // 429
                                                                                                            // 430
  test.equal(spellDom(),                                                                                    // 431
             strip('(ghiWjkl X-b ------------- cdY a-Zef V)'));                                             // 432
                                                                                                            // 433
                                                                                                            // 434
  $(X).insertBefore(Y);                                                                                     // 435
                                                                                                            // 436
  // holy crap, now `aXb` is a mess.  Really `a` and `b`                                                    // 437
  // are in the completely wrong place.                                                                     // 438
  test.equal(spellDom(),                                                                                    // 439
             strip('(ghiWjkl -b ------------- cdXY a-Zef V)'));                                             // 440
                                                                                                            // 441
  r.moveBefore('gl', 'ab');                                                                                 // 442
                                                                                                            // 443
  // Now `c` and `d` are wrong.  It looks like `cdYZef`                                                     // 444
  // also includes `W` and `X`.                                                                             // 445
  test.equal(spellDom(),                                                                                    // 446
             strip('(-------------- cd ghiWjkl aXbY-Zef V)'));                                              // 447
                                                                                                            // 448
  // However, remove `cf` will do a refresh first.                                                          // 449
  r.remove('cf');                                                                                           // 450
                                                                                                            // 451
  test.equal(spellDom(),                                                                                    // 452
             strip('(-------------- ghiWjkl aXb V)'));                                                      // 453
                                                                                                            // 454
  $(X).insertBefore(W);                                                                                     // 455
  r.parentNode().appendChild(W);                                                                            // 456
                                                                                                            // 457
  test.equal(spellDom(),                                                                                    // 458
             strip('(-------------- ghiXjkl ab V) W'));                                                     // 459
                                                                                                            // 460
  r.moveBefore('ab', 'gl');                                                                                 // 461
                                                                                                            // 462
                                                                                                            // 463
  test.equal(spellDom(),                                                                                    // 464
             strip('(-------------- V) aXb ghiWjkl'));                                                      // 465
                                                                                                            // 466
  r.remove('V');                                                                                            // 467
                                                                                                            // 468
  test.equal(spellDom(),                                                                                    // 469
             strip('(--------------) aXb ghiWjkl'));                                                        // 470
                                                                                                            // 471
                                                                                                            // 472
  // Manual refresh is required for move-to-end                                                             // 473
  // (or add-at-end) if elements may have moved externally,                                                 // 474
  // because the `end` pointer could be totally wrong.                                                      // 475
  // Otherwise, the order of `ab` and `gl` would swap,                                                      // 476
  // meaning the DomRange operations would do something                                                     // 477
  // different from the jQuery operations.                                                                  // 478
  //                                                                                                        // 479
  // See `range.getInsertionPoint`.                                                                         // 480
                                                                                                            // 481
  // Same as `r.refresh()` but tests                                                                        // 482
  // the convenience function `DomRange.refresh(element)`:                                                  // 483
  DomRange.refresh(r.parentNode());                                                                         // 484
                                                                                                            // 485
  r.moveBefore('gl', null);                                                                                 // 486
                                                                                                            // 487
  test.equal(spellDom(),                                                                                    // 488
             strip('-------------- (aXb ghiWjkl)'));                                                        // 489
});                                                                                                         // 490
                                                                                                            // 491
Tinytest.add("ui - DomRange - tables", function (test) {                                                    // 492
  var range = function (x) {                                                                                // 493
    // create a range x.dom containing an element x.el,                                                     // 494
    // inside that element, the range x.content.dom                                                         // 495
    x.dom = new DomRange;                                                                                   // 496
    if (x.el) {                                                                                             // 497
      x.dom.add(x.el);                                                                                      // 498
      if (x.content)                                                                                        // 499
        DomRange.insert(x.content.dom, x.el);                                                               // 500
    }                                                                                                       // 501
    return x;                                                                                               // 502
  };                                                                                                        // 503
  var tr, td;                                                                                               // 504
  var table = range({                                                                                       // 505
    el: document.createElement('table'),                                                                    // 506
    content: tr = range({                                                                                   // 507
      el: document.createElement('tr'),                                                                     // 508
      content: td = range({                                                                                 // 509
        el: document.createElement('td')                                                                    // 510
      })                                                                                                    // 511
    })                                                                                                      // 512
  });                                                                                                       // 513
                                                                                                            // 514
  // TBODY got inserted automatically.                                                                      // 515
  // This tests DomRange.insert.                                                                            // 516
  test.equal(table.el.childNodes.length, 1);                                                                // 517
  test.equal(table.el.firstChild.nodeName, 'TBODY');                                                        // 518
  // TBODY contains [start, TR, end]                                                                        // 519
  test.equal(table.el.firstChild.childNodes.length, 3);                                                     // 520
  test.equal(table.el.firstChild.childNodes[1], tr.el);                                                     // 521
  test.equal(tr.el.childNodes.length, 3);                                                                   // 522
  test.equal(tr.el.childNodes[1], td.el);                                                                   // 523
                                                                                                            // 524
  // start over                                                                                             // 525
  $(table.el).empty();                                                                                      // 526
  test.equal(table.el.childNodes.length, 0);                                                                // 527
                                                                                                            // 528
  table.content = range({});                                                                                // 529
  DomRange.insert(table.content.dom, table.el);                                                             // 530
  // table has two children (start/end markers), no elements                                                // 531
  test.equal(table.el.childNodes.length, 2);                                                                // 532
  test.notEqual(table.el.firstChild.nodeType, 1);                                                           // 533
  test.notEqual(table.el.lastChild.nodeType, 1);                                                            // 534
                                                                                                            // 535
  // shazam, adding a TR should move the whole range                                                        // 536
  // into a TBODY.  This tests range.add(node).                                                             // 537
  table.content.dom.add(document.createElement('tr'));                                                      // 538
                                                                                                            // 539
  test.equal(table.el.childNodes.length, 1);                                                                // 540
  test.equal(table.el.firstChild.nodeName, 'TBODY');                                                        // 541
  test.equal(table.el.firstChild.childNodes.length, 3);                                                     // 542
  test.equal(table.el.firstChild.childNodes[1].nodeName, 'TR');                                             // 543
                                                                                                            // 544
  // start over.                                                                                            // 545
  $(table.el).empty();                                                                                      // 546
  test.equal(table.el.childNodes.length, 0);                                                                // 547
                                                                                                            // 548
  table.content = range({});                                                                                // 549
  DomRange.insert(table.content.dom, table.el);                                                             // 550
  var a1 = range({});                                                                                       // 551
  var a2 = range({});                                                                                       // 552
  a1.dom.add(a2.dom);                                                                                       // 553
  table.content.dom.add(a1.dom);                                                                            // 554
  // 6 marker nodes in table, no elements                                                                   // 555
  test.equal(table.el.childNodes.length, 6);                                                                // 556
  test.equal($(table.el).find("*").length, 0);                                                              // 557
  // shazam, adding a TR to the innermost range                                                             // 558
  // should move all the ranges into a TBODY.                                                               // 559
  a2.dom.add(document.createElement('tr'));                                                                 // 560
  test.equal(table.el.childNodes.length, 1);                                                                // 561
  test.equal(table.el.firstChild.nodeName, 'TBODY');                                                        // 562
  test.equal(table.el.firstChild.childNodes.length, 7);                                                     // 563
  test.equal(table.el.firstChild.childNodes[3].nodeName, 'TR');                                             // 564
                                                                                                            // 565
  // start over.  this time test adding a range containing                                                  // 566
  // a TR.                                                                                                  // 567
  $(table.el).empty();                                                                                      // 568
  test.equal(table.el.childNodes.length, 0);                                                                // 569
                                                                                                            // 570
  table.content = range({});                                                                                // 571
  DomRange.insert(table.content.dom, table.el);                                                             // 572
  var b1 = range({});                                                                                       // 573
  var b2 = range({});                                                                                       // 574
  table.content.dom.add(b1.dom);                                                                            // 575
  b2.dom.add(document.createElement('tr'));                                                                 // 576
  // 4 marker nodes in table, no elements                                                                   // 577
  test.equal(table.el.childNodes.length, 4);                                                                // 578
  test.equal($(table.el).find("*").length, 0);                                                              // 579
  // shazam, adding b2, which contains a TR,                                                                // 580
  // should move all the ranges into a TBODY.                                                               // 581
  b1.dom.add(b2.dom);                                                                                       // 582
  test.equal(table.el.childNodes.length, 1);                                                                // 583
  test.equal(table.el.firstChild.nodeName, 'TBODY');                                                        // 584
  test.equal(table.el.firstChild.childNodes.length, 7);                                                     // 585
  test.equal(table.el.firstChild.childNodes[3].nodeName, 'TR');                                             // 586
                                                                                                            // 587
  test.equal(b2.dom.parentNode().nodeName, 'TBODY');                                                        // 588
  test.equal(b1.dom.parentNode().nodeName, 'TBODY');                                                        // 589
  test.equal(table.content.dom.parentNode().nodeName, 'TBODY');                                             // 590
                                                                                                            // 591
                                                                                                            // 592
  // start over.  now test two TR ranges.                                                                   // 593
  $(table.el).empty();                                                                                      // 594
  test.equal(table.el.childNodes.length, 0);                                                                // 595
                                                                                                            // 596
  var c1 = range({});                                                                                       // 597
  var c2 = range({});                                                                                       // 598
  DomRange.insert(c1.dom, table.el);                                                                        // 599
  DomRange.insert(c2.dom, table.el);                                                                        // 600
  test.equal(table.el.childNodes.length, 4);                                                                // 601
  test.equal($(table.el).find("*").length, 0);                                                              // 602
  c2.dom.add(document.createElement('tr'));                                                                 // 603
  test.equal(table.el.childNodes.length, 3);                                                                // 604
  test.equal($(table.el).find("> *").length, 1);                                                            // 605
  test.equal($(table.el).find("> tbody").length, 1);                                                        // 606
  c1.dom.add(document.createElement('tr'));                                                                 // 607
  // now there should be a single TBODY with two                                                            // 608
  // ranges in it containing TRs                                                                            // 609
  test.equal(table.el.childNodes.length, 1);                                                                // 610
  test.equal(table.el.firstChild.nodeName, 'TBODY');                                                        // 611
  var tbody = table.el.firstChild;                                                                          // 612
  test.equal(tbody.childNodes.length, 6);                                                                   // 613
  test.equal($(tbody).find("> *").length, 2); // 2 elements                                                 // 614
  test.equal(tbody.childNodes[1].nodeName, 'TR');                                                           // 615
  test.equal(tbody.childNodes[4].nodeName, 'TR');                                                           // 616
});                                                                                                         // 617
                                                                                                            // 618
Tinytest.add("ui - DomRange - basic events", function (test) {                                              // 619
  // test.equal doesn't work on arrays of DOM nodes, so                                                     // 620
  // we need this.  It's `===` that descends recursively                                                    // 621
  // into any arrays.                                                                                       // 622
  var arrayEqual = function (a, b) {                                                                        // 623
    test.equal(_.isArray(a), _.isArray(b));                                                                 // 624
    if (_.isArray(a)) {                                                                                     // 625
      test.equal(a.length, b.length);                                                                       // 626
      for (var i = 0; i < a.length; i++) {                                                                  // 627
        arrayEqual(a[i], b[i]);                                                                             // 628
      }                                                                                                     // 629
    } else {                                                                                                // 630
      test.isTrue(a[i] === b[i]);                                                                           // 631
    }                                                                                                       // 632
  };                                                                                                        // 633
                                                                                                            // 634
  var q = new DomRange;                                                                                     // 635
  test.throws(function () {                                                                                 // 636
    // can't bind events before DomRange is added to                                                        // 637
    // the DOM                                                                                              // 638
    q.on('click', function (evt) {});                                                                       // 639
  });                                                                                                       // 640
                                                                                                            // 641
  inDocument(                                                                                               // 642
    htmlRange("<span>Foo</span>"),                                                                          // 643
    function (r) {                                                                                          // 644
      var buf = [];                                                                                         // 645
                                                                                                            // 646
      r.on('click', 'span', function (evt) {                                                                // 647
        buf.push([evt.type, evt.target, evt.currentTarget]);                                                // 648
      });                                                                                                   // 649
                                                                                                            // 650
      arrayEqual(buf, []);                                                                                  // 651
      var span = r.elements()[0];                                                                           // 652
      clickElement(span);                                                                                   // 653
      arrayEqual(buf, [['click', span, span]]);                                                             // 654
    });                                                                                                     // 655
                                                                                                            // 656
  inDocument(                                                                                               // 657
    htmlRange("<div><span>Foo</span></div>"),                                                               // 658
    function (r) {                                                                                          // 659
      var buf = [];                                                                                         // 660
                                                                                                            // 661
      // test click with no selector; should only                                                           // 662
      // fire on the event target.                                                                          // 663
      r.on('click', function (evt) {                                                                        // 664
        buf.push([evt.type, evt.target, evt.currentTarget]);                                                // 665
      });                                                                                                   // 666
                                                                                                            // 667
      arrayEqual(buf, []);                                                                                  // 668
      var span = r.$('span')[0];                                                                            // 669
      clickElement(span);                                                                                   // 670
      arrayEqual(buf, [['click', span, span]]);                                                             // 671
    });                                                                                                     // 672
                                                                                                            // 673
  inDocument(                                                                                               // 674
    htmlRange('<div id="yeah"><span>Foo</span></div>' +                                                     // 675
              '<div id="no">Bar</div>'),                                                                    // 676
    function (r) {                                                                                          // 677
      var buf = [];                                                                                         // 678
                                                                                                            // 679
      // test click on particular div, which is                                                             // 680
      // not the target or the bound element                                                                // 681
      r.on('click', '#yeah', function (evt) {                                                               // 682
        buf.push([evt.type, evt.target, evt.currentTarget]);                                                // 683
      });                                                                                                   // 684
                                                                                                            // 685
      arrayEqual(buf, []);                                                                                  // 686
      clickElement(r.$('#no')[0]);                                                                          // 687
      arrayEqual(buf, []);                                                                                  // 688
      var yeah = r.$('#yeah')[0];                                                                           // 689
      clickElement(yeah);                                                                                   // 690
      arrayEqual(buf, [['click', yeah, yeah]]);                                                             // 691
    });                                                                                                     // 692
                                                                                                            // 693
  inDocument(                                                                                               // 694
    new DomRange,                                                                                           // 695
    function (r) {                                                                                          // 696
      var s;                                                                                                // 697
      r.add(s = htmlRange('<div id="one"></div>'));                                                         // 698
      r.add(htmlRange('<div id="two"></div>'));                                                             // 699
      var one = r.$('#one')[0];                                                                             // 700
      var two = r.$('#two')[0];                                                                             // 701
                                                                                                            // 702
      var buf = [];                                                                                         // 703
                                                                                                            // 704
      // test that click must be in range to fire                                                           // 705
      // event handler                                                                                      // 706
      s.on('click', 'div', function (evt) {                                                                 // 707
        buf.push([evt.type, evt.target, evt.currentTarget]);                                                // 708
      });                                                                                                   // 709
                                                                                                            // 710
      arrayEqual(buf, []);                                                                                  // 711
      clickElement(two);                                                                                    // 712
      arrayEqual(buf, []);                                                                                  // 713
      clickElement(one);                                                                                    // 714
      arrayEqual(buf, [['click', one, one]]);                                                               // 715
    });                                                                                                     // 716
                                                                                                            // 717
});                                                                                                         // 718
                                                                                                            // 719
Tinytest.add("ui - DomRange - contains", function (test) {                                                  // 720
  inDocument(new DomRange, function (r) {                                                                   // 721
    var s = htmlRange('<div id="one"><span>Foo</span></div>');                                              // 722
    var t = new DomRange;                                                                                   // 723
    t.add(s);                                                                                               // 724
    r.add(t);                                                                                               // 725
    r.add(htmlRange('<div id="two"></div>'));                                                               // 726
    var one = r.$('#one')[0];                                                                               // 727
    var two = r.$('#two')[0];                                                                               // 728
    var span = r.$('span')[0];                                                                              // 729
                                                                                                            // 730
    test.isFalse(r.contains(r));                                                                            // 731
    test.isTrue(r.contains(s));                                                                             // 732
    test.isTrue(r.contains(t));                                                                             // 733
    test.isTrue(r.contains(one));                                                                           // 734
    test.isTrue(s.contains(one));                                                                           // 735
    test.isTrue(t.contains(one));                                                                           // 736
    test.isTrue(r.contains(two));                                                                           // 737
    test.isFalse(s.contains(two));                                                                          // 738
    test.isFalse(t.contains(two));                                                                          // 739
    test.isTrue(r.contains(span));                                                                          // 740
    test.isTrue(s.contains(span));                                                                          // 741
    test.isTrue(t.contains(span));                                                                          // 742
    test.isFalse(r.contains(r.parentNode()));                                                               // 743
    test.isFalse(r.contains(document.createElement("DIV")));                                                // 744
  });                                                                                                       // 745
});                                                                                                         // 746
                                                                                                            // 747
Tinytest.add("ui - DomRange - constructor", function (test) {                                               // 748
  var r = new DomRange;                                                                                     // 749
                                                                                                            // 750
  test.isTrue(r.parentNode());                                                                              // 751
                                                                                                            // 752
  test.isTrue(r.start.$ui === r);                                                                           // 753
  test.isTrue(r.end.$ui === r);                                                                             // 754
                                                                                                            // 755
  var div = document.createElement('div');                                                                  // 756
  r.add(div);                                                                                               // 757
  test.isTrue(div.$ui === r);                                                                               // 758
});                                                                                                         // 759
                                                                                                            // 760
Tinytest.add("ui - DomRange - get", function (test) {                                                       // 761
  var r = new DomRange;                                                                                     // 762
  var a = document.createElement('div');                                                                    // 763
  var b = document.createElement('div');                                                                    // 764
  var c = document.createElement('div');                                                                    // 765
  var d = document.createElement('div');                                                                    // 766
                                                                                                            // 767
  r.add(a);                                                                                                 // 768
  r.add(null, b);                                                                                           // 769
  r.add('c', c);                                                                                            // 770
  test.throws(function () {                                                                                 // 771
    r.add(0, d);                                                                                            // 772
  });                                                                                                       // 773
  test.throws(function () {                                                                                 // 774
    r.add(1, d);                                                                                            // 775
  });                                                                                                       // 776
  test.throws(function () {                                                                                 // 777
    r.add('', d);                                                                                           // 778
  });                                                                                                       // 779
                                                                                                            // 780
  test.isTrue(r.get('toString') === null);                                                                  // 781
  test.isTrue(r.get('__proto__') === null);                                                                 // 782
  test.isTrue(r.get('_proto__') === null);                                                                  // 783
  test.isTrue(r.get('blahblah') === null);                                                                  // 784
  r.add('toString', d);                                                                                     // 785
                                                                                                            // 786
  test.throws(function () {                                                                                 // 787
    r.get('');                                                                                              // 788
  });                                                                                                       // 789
  test.throws(function () {                                                                                 // 790
    r.get(null);                                                                                            // 791
  });                                                                                                       // 792
  test.throws(function () {                                                                                 // 793
    r.get(1);                                                                                               // 794
  });                                                                                                       // 795
                                                                                                            // 796
  test.equal(r.elements().length, 4);                                                                       // 797
                                                                                                            // 798
  test.isTrue(r.get('c') === c);                                                                            // 799
  test.isTrue(r.get('toString') === d);                                                                     // 800
});                                                                                                         // 801
                                                                                                            // 802
// This test targets IE 9 and 10, which allow properties                                                    // 803
// to be attached to TextNodes but may lose them over time.                                                 // 804
// Specifically, the JavaScript view of a TextNode seems to                                                 // 805
// be only weakly retained by the TextNode itself, so if you                                                // 806
// hang an object graph off a TextNode, you need some other                                                 // 807
// pointer to the TextNode or an object in the graph to                                                     // 808
// retain it.                                                                                               // 809
Tinytest.addAsync("ui - DomRange - IE TextNode GC", function (test, onComplete) {                           // 810
  var r = new DomRange;                                                                                     // 811
  var B = document.createElement('B');                                                                      // 812
  B.id = 'ie_textnode_gc_test';                                                                             // 813
  document.body.appendChild(B);                                                                             // 814
  DomRange.insert(r, B);                                                                                    // 815
  r = null;                                                                                                 // 816
  B = null;                                                                                                 // 817
                                                                                                            // 818
  // trigger GC...                                                                                          // 819
  if (typeof CollectGarbage === 'function')                                                                 // 820
    CollectGarbage();                                                                                       // 821
                                                                                                            // 822
  // come back later...                                                                                     // 823
  window.setTimeout(function () {                                                                           // 824
    var B = document.getElementById("ie_textnode_gc_test");                                                 // 825
    test.isTrue(B.firstChild.$ui);                                                                          // 826
    test.isTrue(B.lastChild.$ui);                                                                           // 827
    window.BBB = B;                                                                                         // 828
    document.body.removeChild(B);                                                                           // 829
    onComplete();                                                                                           // 830
  }, 500);                                                                                                  // 831
});                                                                                                         // 832
                                                                                                            // 833
Tinytest.add("ui - DomRange - more TBODY", function (test) {                                                // 834
  inDocument(htmlRange("<table></table>"), function (r) {                                                   // 835
    var table = r.elements()[0];                                                                            // 836
    var tableContent = new DomRange;                                                                        // 837
    var buf = [];                                                                                           // 838
    DomRange.insert(tableContent, table);                                                                   // 839
    var trRange = htmlRange("<tr><td>Hello</td></tr>");                                                     // 840
    tableContent.add(trRange);                                                                              // 841
    test.isTrue(tableContent.contains(trRange));                                                            // 842
  });                                                                                                       // 843
                                                                                                            // 844
  inDocument(htmlRange("<table></table>"), function (r) {                                                   // 845
    var table = r.elements()[0];                                                                            // 846
    var tableContent = new DomRange;                                                                        // 847
    var buf = [];                                                                                           // 848
    DomRange.insert(tableContent, table);                                                                   // 849
    var trRange = htmlRange("<tr><td>Hello</td></tr>");                                                     // 850
    var tr = trRange.elements()[0];                                                                         // 851
    tableContent.add('tr', tr);                                                                             // 852
    test.equal(_.keys(tableContent.members).length, 1);                                                     // 853
    test.isTrue(tableContent.contains(tr));                                                                 // 854
    tableContent.remove('tr');                                                                              // 855
    // bizarrely, in IE 8, the `tr` still has some                                                          // 856
    // DocumentFragment as its parent even though `removeChild`                                             // 857
    // has been called on it directly.                                                                      // 858
    test.isFalse(tr.parentNode && tr.parentNode.nodeType === 1);                                            // 859
  });                                                                                                       // 860
});                                                                                                         // 861
                                                                                                            // 862
Tinytest.add("ui - DomRange - events in tables", function (test) {                                          // 863
  inDocument(htmlRange("<table></table>"), function (r) {                                                   // 864
    var table = r.elements()[0];                                                                            // 865
    var tableContent = new DomRange;                                                                        // 866
    var buf = [];                                                                                           // 867
    DomRange.insert(tableContent, table);                                                                   // 868
    tableContent.on('click', 'tr', function (evt) {                                                         // 869
      buf.push('click ' + evt.currentTarget.nodeName);                                                      // 870
    });                                                                                                     // 871
    var trRange = htmlRange("<tr><td>Hello</td></tr>");                                                     // 872
    tableContent.add(trRange);                                                                              // 873
    var tr = trRange.elements()[0];                                                                         // 874
    test.equal(buf, []);                                                                                    // 875
    clickElement(tr);                                                                                       // 876
    test.equal(buf, ['click TR']);                                                                          // 877
    // XXX test something that would break if the event data                                                // 878
    // is on the TABLE rather than the TBODY (the new                                                       // 879
    // parentNode of `tableContent`).                                                                       // 880
  });                                                                                                       // 881
});                                                                                                         // 882
                                                                                                            // 883
Tinytest.add("ui - DomRange - nested event order", function (test) {                                        // 884
  inDocument(new DomRange, function (r) {                                                                   // 885
    var a = new DomRange;                                                                                   // 886
    var b = new DomRange;                                                                                   // 887
    var c = new DomRange;                                                                                   // 888
    var d = new DomRange;                                                                                   // 889
    r.add(a);                                                                                               // 890
    a.add(b);                                                                                               // 891
    b.add(c);                                                                                               // 892
    c.add(d);                                                                                               // 893
    var div = document.createElement("DIV");                                                                // 894
    d.add(div);                                                                                             // 895
                                                                                                            // 896
    var buf = [];                                                                                           // 897
    var appender = function (str) {                                                                         // 898
      return function (evt) {                                                                               // 899
        buf.push(str);                                                                                      // 900
      };                                                                                                    // 901
    };                                                                                                      // 902
                                                                                                            // 903
    b.on('click', 'div', appender("B"));                                                                    // 904
    a.on('click', 'div', appender("A"));                                                                    // 905
    d.on('click', appender("D"));                                                                           // 906
    c.on('click', 'div', appender("C"));                                                                    // 907
    test.equal(buf, []);                                                                                    // 908
    clickElement(div);                                                                                      // 909
    test.equal(buf, ['D', 'C', 'B', 'A']);                                                                  // 910
    buf.length = 0;                                                                                         // 911
                                                                                                            // 912
    b.on('click', appender("B2"));                                                                          // 913
    d.on('click', 'div', appender("D2"));                                                                   // 914
    clickElement(div);                                                                                      // 915
    test.equal(buf, ['D', 'D2', 'C', 'B', 'B2', 'A']);                                                      // 916
  });                                                                                                       // 917
});                                                                                                         // 918
                                                                                                            // 919
Tinytest.add("ui - DomRange - isParented", function (test) {                                                // 920
  inDocument(new DomRange, function (r) {                                                                   // 921
    test.equal(r.isParented, true);                                                                         // 922
    var a = new DomRange;                                                                                   // 923
    var b = new DomRange;                                                                                   // 924
    var c = new DomRange;                                                                                   // 925
    var d = new DomRange;                                                                                   // 926
    var e = new DomRange;                                                                                   // 927
    var abcde = function (ap, bp, cp, dp, ep) {                                                             // 928
      test.equal(!! a.isParented, !! ap);                                                                   // 929
      test.equal(!! b.isParented, !! bp);                                                                   // 930
      test.equal(!! c.isParented, !! cp);                                                                   // 931
      test.equal(!! d.isParented, !! dp);                                                                   // 932
      test.equal(!! e.isParented, !! ep);                                                                   // 933
    };                                                                                                      // 934
    var div = document.createElement("DIV");                                                                // 935
    c.add(div);                                                                                             // 936
    abcde(0, 0, 0, 0, 0);                                                                                   // 937
    d.add(e);                                                                                               // 938
    abcde(0, 0, 0, 0, 0);                                                                                   // 939
    DomRange.insert(d, div);                                                                                // 940
    abcde(0, 0, 0, 1, 1);                                                                                   // 941
    a.add(b);                                                                                               // 942
    abcde(0, 0, 0, 1, 1);                                                                                   // 943
    r.add(a);                                                                                               // 944
    abcde(1, 1, 0, 1, 1);                                                                                   // 945
    b.add(c);                                                                                               // 946
    abcde(1, 1, 1, 1, 1);                                                                                   // 947
                                                                                                            // 948
    var container = r.parentNode();                                                                         // 949
    test.equal(_.keys(container.$_uiranges).length, 1);                                                     // 950
    test.equal(_.keys(div.$_uiranges).length, 1);                                                           // 951
    d.remove();                                                                                             // 952
    test.equal(_.keys(div.$_uiranges).length, 0);                                                           // 953
    r.remove();                                                                                             // 954
    test.equal(_.keys(container.$_uiranges).length, 0);                                                     // 955
  });                                                                                                       // 956
});                                                                                                         // 957
                                                                                                            // 958
Tinytest.add("ui - DomRange - structural removal", function (test) {                                        // 959
  inDocument(new DomRange, function (r) {                                                                   // 960
    var a = new DomRange;                                                                                   // 961
    test.isFalse(a.isRemoved);                                                                              // 962
    r.add('a', a);                                                                                          // 963
    test.isFalse(a.isRemoved);                                                                              // 964
    r.remove('a');                                                                                          // 965
    test.isTrue(a.isRemoved);                                                                               // 966
                                                                                                            // 967
                                                                                                            // 968
    var b = new DomRange;                                                                                   // 969
    test.isFalse(b.isRemoved);                                                                              // 970
    r.add(b);                                                                                               // 971
    test.isFalse(b.isRemoved);                                                                              // 972
    r.removeAll();                                                                                          // 973
    test.isTrue(b.isRemoved);                                                                               // 974
                                                                                                            // 975
                                                                                                            // 976
    var c = new DomRange;                                                                                   // 977
    var d = new DomRange;                                                                                   // 978
    var e = new DomRange;                                                                                   // 979
    c.add(d);                                                                                               // 980
    d.add(e);                                                                                               // 981
    r.add('c', c);                                                                                          // 982
    test.isFalse(c.isRemoved);                                                                              // 983
    test.isFalse(d.isRemoved);                                                                              // 984
    test.isFalse(e.isRemoved);                                                                              // 985
    r.remove('c');                                                                                          // 986
    test.isTrue(c.isRemoved);                                                                               // 987
    test.isTrue(d.isRemoved);                                                                               // 988
    test.isTrue(e.isRemoved);                                                                               // 989
                                                                                                            // 990
                                                                                                            // 991
    for (var scenario = 0; scenario < 2; scenario++) {                                                      // 992
      var f = new DomRange;                                                                                 // 993
      var g = document.createElement("DIV");                                                                // 994
      var h = new DomRange;                                                                                 // 995
      var i = document.createElement("DIV");                                                                // 996
      var j = document.createElement("DIV");                                                                // 997
      var k = new DomRange;                                                                                 // 998
      r.add('f', f);                                                                                        // 999
      f.add(g);                                                                                             // 1000
      DomRange.insert(h, g);                                                                                // 1001
      h.add(i);                                                                                             // 1002
      DomRange.insert(k, j);                                                                                // 1003
      i.appendChild(j);                                                                                     // 1004
      test.isFalse(f.isRemoved);                                                                            // 1005
      test.isFalse(h.isRemoved);                                                                            // 1006
      test.isFalse(k.isRemoved);                                                                            // 1007
      if (scenario === 0)                                                                                   // 1008
        r.removeAll();                                                                                      // 1009
      else if (scenario === 1)                                                                              // 1010
        r.remove('f');                                                                                      // 1011
      test.isTrue(f.isRemoved);                                                                             // 1012
      test.isTrue(h.isRemoved);                                                                             // 1013
      test.isTrue(k.isRemoved);                                                                             // 1014
                                                                                                            // 1015
      r.removeAll();                                                                                        // 1016
    }                                                                                                       // 1017
  });                                                                                                       // 1018
});                                                                                                         // 1019
                                                                                                            // 1020
Tinytest.add("ui - DomRange - noticed removal", function (test) {                                           // 1021
  // TODO                                                                                                   // 1022
  //                                                                                                        // 1023
  // e.g. noticed via `eachMember` or `add`                                                                 // 1024
});                                                                                                         // 1025
                                                                                                            // 1026
Tinytest.add("ui - DomRange - jQuery removal", function (test) {                                            // 1027
  inDocument(htmlRange("<div></div>"), function (r) {                                                       // 1028
    for (var scenario = 0; scenario < 3; scenario++) {                                                      // 1029
      var f = document.createElement("DIV");                                                                // 1030
      var g = document.createElement("DIV");                                                                // 1031
      var h = new DomRange;                                                                                 // 1032
      var i = document.createElement("DIV");                                                                // 1033
      var j = document.createElement("DIV");                                                                // 1034
      var k = new DomRange;                                                                                 // 1035
      r.add(f);                                                                                             // 1036
      f.appendChild(g);                                                                                     // 1037
      DomRange.insert(h, g);                                                                                // 1038
      h.add(i);                                                                                             // 1039
      DomRange.insert(k, j);                                                                                // 1040
      i.appendChild(j);                                                                                     // 1041
      test.isFalse(h.isRemoved);                                                                            // 1042
      test.isFalse(k.isRemoved);                                                                            // 1043
                                                                                                            // 1044
      $(g).removeData();                                                                                    // 1045
      test.isFalse(h.isRemoved);                                                                            // 1046
      test.isFalse(k.isRemoved);                                                                            // 1047
                                                                                                            // 1048
      if (scenario === 0)                                                                                   // 1049
        $(g).remove();                                                                                      // 1050
      else if (scenario === 1)                                                                              // 1051
        $(f).empty();                                                                                       // 1052
      else if (scenario === 2)                                                                              // 1053
        $(f).html("<br>");                                                                                  // 1054
      else if (scenario === 3)                                                                              // 1055
        $(g).detach();                                                                                      // 1056
                                                                                                            // 1057
      if (scenario !== 3) {                                                                                 // 1058
        test.isTrue(h.isRemoved);                                                                           // 1059
        test.isTrue(k.isRemoved);                                                                           // 1060
      } else {                                                                                              // 1061
        // `detach` doesn't remove                                                                          // 1062
        test.isFalse(h.isRemoved);                                                                          // 1063
        test.isFalse(k.isRemoved);                                                                          // 1064
      }                                                                                                     // 1065
                                                                                                            // 1066
      r.removeAll();                                                                                        // 1067
    }                                                                                                       // 1068
  });                                                                                                       // 1069
});                                                                                                         // 1070
                                                                                                            // 1071
// TO TEST STILL:                                                                                           // 1072
// - external remove element                                                                                // 1073
// - double-add, double-remove                                                                              // 1074
// - external entire remove                                                                                 // 1075
// - element adoption during move/remove/refresh                                                            // 1076
// - first arg of add must be string, errors on `0` for example.                                            // 1077
//   same with remove and move `id` arguments.                                                              // 1078
// - can't add multiple members with id, but can add array of 1.                                            // 1079
//   can add 0 with no id.                                                                                  // 1080
// - add a node or range with the same id as an old member                                                  // 1081
//   works if that member is gone.                                                                          // 1082
// - events (and other stuff) get moved when wrapping in TBODY                                              // 1083
// - event unbinding                                                                                        // 1084
// - "noticed" removal due to `eachMembers`, `add`, etc.                                                    // 1085
                                                                                                            // 1086
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/ui/render_tests.js                                                                              //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var materialize = UI.materialize;                                                                           // 1
var toHTML = HTML.toHTML;                                                                                   // 2
var toCode = HTML.toJS;                                                                                     // 3
                                                                                                            // 4
var P = HTML.P;                                                                                             // 5
var CharRef = HTML.CharRef;                                                                                 // 6
var DIV = HTML.DIV;                                                                                         // 7
var Comment = HTML.Comment;                                                                                 // 8
var BR = HTML.BR;                                                                                           // 9
var A = HTML.A;                                                                                             // 10
var UL = HTML.UL;                                                                                           // 11
var LI = HTML.LI;                                                                                           // 12
var SPAN = HTML.SPAN;                                                                                       // 13
var HR = HTML.HR;                                                                                           // 14
var TEXTAREA = HTML.TEXTAREA;                                                                               // 15
var INPUT = HTML.INPUT;                                                                                     // 16
                                                                                                            // 17
Tinytest.add("ui - render - basic", function (test) {                                                       // 18
  var run = function (input, expectedInnerHTML, expectedHTML, expectedCode) {                               // 19
    var div = document.createElement("DIV");                                                                // 20
    materialize(input, div);                                                                                // 21
    test.equal(canonicalizeHtml(div.innerHTML), expectedInnerHTML);                                         // 22
    test.equal(toHTML(input), expectedHTML);                                                                // 23
    if (typeof expectedCode !== 'undefined')                                                                // 24
      test.equal(toCode(input), expectedCode);                                                              // 25
  };                                                                                                        // 26
                                                                                                            // 27
  run(P('Hello'),                                                                                           // 28
      '<p>Hello</p>',                                                                                       // 29
      '<p>Hello</p>',                                                                                       // 30
      'HTML.P("Hello")');                                                                                   // 31
                                                                                                            // 32
  run(null, '', '', 'null');                                                                                // 33
  run([], '', '', '[]');                                                                                    // 34
  run([null, null], '', '', '[null, null]');                                                                // 35
                                                                                                            // 36
  // Test crazy character references                                                                        // 37
                                                                                                            // 38
  // `&zopf;` is "Mathematical double-struck small z" a.k.a. "open-face z"                                  // 39
  run(P(CharRef({html: '&zopf;', str: '\ud835\udd6b'})),                                                    // 40
      '<p>\ud835\udd6b</p>',                                                                                // 41
      '<p>&zopf;</p>',                                                                                      // 42
      'HTML.P(HTML.CharRef({html: "&zopf;", str: "\\ud835\\udd6b"}))');                                     // 43
                                                                                                            // 44
  run(P({id: CharRef({html: '&zopf;', str: '\ud835\udd6b'})}, 'Hello'),                                     // 45
      '<p id="\ud835\udd6b">Hello</p>',                                                                     // 46
      '<p id="&zopf;">Hello</p>',                                                                           // 47
      'HTML.P({id: HTML.CharRef({html: "&zopf;", str: "\\ud835\\udd6b"})}, "Hello")');                      // 48
                                                                                                            // 49
  run(P({id: [CharRef({html: '&zopf;', str: '\ud835\udd6b'}), '!']}, 'Hello'),                              // 50
      '<p id="\ud835\udd6b!">Hello</p>',                                                                    // 51
      '<p id="&zopf;!">Hello</p>',                                                                          // 52
      'HTML.P({id: [HTML.CharRef({html: "&zopf;", str: "\\ud835\\udd6b"}), "!"]}, "Hello")');               // 53
                                                                                                            // 54
  // Test comments                                                                                          // 55
                                                                                                            // 56
  run(DIV(Comment('Test')),                                                                                 // 57
      '<div><!----></div>', // our innerHTML-canonicalization function kills comment contents               // 58
      '<div><!--Test--></div>',                                                                             // 59
      'HTML.DIV(HTML.Comment("Test"))');                                                                    // 60
                                                                                                            // 61
  // Test arrays                                                                                            // 62
                                                                                                            // 63
  run([P('Hello'), P('World')],                                                                             // 64
      '<p>Hello</p><p>World</p>',                                                                           // 65
      '<p>Hello</p><p>World</p>',                                                                           // 66
      '[HTML.P("Hello"), HTML.P("World")]');                                                                // 67
                                                                                                            // 68
  // Test slightly more complicated structure                                                               // 69
                                                                                                            // 70
  run(DIV({'class': 'foo'}, UL(LI(P(A({href: '#one'}, 'One'))),                                             // 71
                               LI(P('Two', BR(), 'Three')))),                                               // 72
      '<div class="foo"><ul><li><p><a href="#one">One</a></p></li><li><p>Two<br>Three</p></li></ul></div>', // 73
      '<div class="foo"><ul><li><p><a href="#one">One</a></p></li><li><p>Two<br>Three</p></li></ul></div>', // 74
      'HTML.DIV({"class": "foo"}, HTML.UL(HTML.LI(HTML.P(HTML.A({href: "#one"}, "One"))), HTML.LI(HTML.P("Two", HTML.BR(), "Three"))))');
                                                                                                            // 76
                                                                                                            // 77
  // Test nully attributes                                                                                  // 78
  run(BR({x: null,                                                                                          // 79
          y: [[], []],                                                                                      // 80
          a: [['']]}),                                                                                      // 81
      '<br a="">',                                                                                          // 82
      '<br a="">',                                                                                          // 83
      'HTML.BR({a: [[""]]})');                                                                              // 84
                                                                                                            // 85
  run(BR({                                                                                                  // 86
    x: function () { return function () { return []; }; },                                                  // 87
    a: function () { return function () { return ''; }; }}),                                                // 88
      '<br a="">',                                                                                          // 89
      '<br a="">');                                                                                         // 90
});                                                                                                         // 91
                                                                                                            // 92
// test that we correctly update the 'value' property on input fields                                       // 93
// rather than the 'value' attribute. the 'value' attribute only sets                                       // 94
// the initial value.                                                                                       // 95
Tinytest.add("ui - render - input - value", function (test) {                                               // 96
  var R = ReactiveVar("hello");                                                                             // 97
  var div = document.createElement("DIV");                                                                  // 98
  materialize(INPUT({value: function () { return R.get(); }}), div);                                        // 99
  var inputEl = div.querySelector('input');                                                                 // 100
  test.equal(inputEl.value, "hello");                                                                       // 101
  inputEl.value = "goodbye";                                                                                // 102
  R.set("hola");                                                                                            // 103
  Deps.flush();                                                                                             // 104
  test.equal(inputEl.value, "hola");                                                                        // 105
});                                                                                                         // 106
                                                                                                            // 107
// test that we correctly update the 'checked' property rather than                                         // 108
// the 'checked' attribute on input fields of type 'checkbox'. the                                          // 109
// 'checked' attribute only sets the initial value.                                                         // 110
Tinytest.add("ui - render - input - checked", function (test) {                                             // 111
  var R = ReactiveVar(null);                                                                                // 112
  var div = document.createElement("DIV");                                                                  // 113
  materialize(INPUT({type: "checkbox", checked: function () { return R.get(); }}), div);                    // 114
  var inputEl = div.querySelector('input');                                                                 // 115
  test.equal(inputEl.checked, false);                                                                       // 116
  inputEl.checked = true;                                                                                   // 117
                                                                                                            // 118
  R.set("checked");                                                                                         // 119
  Deps.flush();                                                                                             // 120
  R.set(null);                                                                                              // 121
  Deps.flush();                                                                                             // 122
  test.equal(inputEl.checked, false);                                                                       // 123
});                                                                                                         // 124
                                                                                                            // 125
Tinytest.add("ui - render - textarea", function (test) {                                                    // 126
  var run = function (optNode, text, html, code) {                                                          // 127
    if (typeof optNode === 'string') {                                                                      // 128
      // called with args (text, html, code)                                                                // 129
      code = html;                                                                                          // 130
      html = text;                                                                                          // 131
      text = optNode;                                                                                       // 132
      optNode = null;                                                                                       // 133
    }                                                                                                       // 134
    var div = document.createElement("DIV");                                                                // 135
    var node = TEXTAREA(optNode || text);                                                                   // 136
    materialize(node, div);                                                                                 // 137
                                                                                                            // 138
    var value = div.querySelector('textarea').value;                                                        // 139
    value = value.replace(/\r\n/g, "\n"); // IE8 substitutes \n with \r\n                                   // 140
    test.equal(value, text);                                                                                // 141
                                                                                                            // 142
    test.equal(toHTML(node), html);                                                                         // 143
    if (typeof code === 'string')                                                                           // 144
      test.equal(toCode(node), code);                                                                       // 145
  };                                                                                                        // 146
                                                                                                            // 147
  run('Hello',                                                                                              // 148
      '<textarea>Hello</textarea>',                                                                         // 149
      'HTML.TEXTAREA("Hello")');                                                                            // 150
                                                                                                            // 151
  run('\nHello',                                                                                            // 152
      '<textarea>\n\nHello</textarea>',                                                                     // 153
      'HTML.TEXTAREA("\\nHello")');                                                                         // 154
                                                                                                            // 155
  run('</textarea>',                                                                                        // 156
      '<textarea>&lt;/textarea></textarea>',                                                                // 157
      'HTML.TEXTAREA("</textarea>")');                                                                      // 158
                                                                                                            // 159
  run(CharRef({html: '&amp;', str: '&'}),                                                                   // 160
      '&',                                                                                                  // 161
      '<textarea>&amp;</textarea>',                                                                         // 162
      'HTML.TEXTAREA(HTML.CharRef({html: "&amp;", str: "&"}))');                                            // 163
                                                                                                            // 164
  run(['a', function () { return 'b'; }, 'c'],                                                              // 165
      'abc',                                                                                                // 166
      '<textarea>abc</textarea>');                                                                          // 167
                                                                                                            // 168
});                                                                                                         // 169
                                                                                                            // 170
Tinytest.add("ui - render - closures", function (test) {                                                    // 171
                                                                                                            // 172
  // Reactively change a text node                                                                          // 173
  (function () {                                                                                            // 174
    var R = ReactiveVar('Hello');                                                                           // 175
    var test1 = P(function () { return R.get(); });                                                         // 176
                                                                                                            // 177
    test.equal(toHTML(test1), '<p>Hello</p>');                                                              // 178
                                                                                                            // 179
    var div = document.createElement("DIV");                                                                // 180
    materialize(test1, div);                                                                                // 181
    test.equal(canonicalizeHtml(div.innerHTML), "<p>Hello</p>");                                            // 182
                                                                                                            // 183
    R.set('World');                                                                                         // 184
    Deps.flush();                                                                                           // 185
    test.equal(canonicalizeHtml(div.innerHTML), "<p>World</p>");                                            // 186
  })();                                                                                                     // 187
                                                                                                            // 188
  // Reactively change an array of text nodes                                                               // 189
  (function () {                                                                                            // 190
    var R = ReactiveVar(['Hello', ' World']);                                                               // 191
    var test1 = P(function () { return R.get(); });                                                         // 192
                                                                                                            // 193
    test.equal(toHTML(test1), '<p>Hello World</p>');                                                        // 194
                                                                                                            // 195
    var div = document.createElement("DIV");                                                                // 196
    materialize(test1, div);                                                                                // 197
    test.equal(canonicalizeHtml(div.innerHTML), "<p>Hello World</p>");                                      // 198
                                                                                                            // 199
    R.set(['Goodbye', ' World']);                                                                           // 200
    Deps.flush();                                                                                           // 201
    test.equal(canonicalizeHtml(div.innerHTML), "<p>Goodbye World</p>");                                    // 202
  })();                                                                                                     // 203
                                                                                                            // 204
});                                                                                                         // 205
                                                                                                            // 206
Tinytest.add("ui - render - closure GC", function (test) {                                                  // 207
  // test that removing parent element removes listeners and stops autoruns.                                // 208
  (function () {                                                                                            // 209
    var R = ReactiveVar('Hello');                                                                           // 210
    var test1 = P(function () { return R.get(); });                                                         // 211
                                                                                                            // 212
    var div = document.createElement("DIV");                                                                // 213
    materialize(test1, div);                                                                                // 214
    test.equal(canonicalizeHtml(div.innerHTML), "<p>Hello</p>");                                            // 215
                                                                                                            // 216
    R.set('World');                                                                                         // 217
    Deps.flush();                                                                                           // 218
    test.equal(canonicalizeHtml(div.innerHTML), "<p>World</p>");                                            // 219
                                                                                                            // 220
    test.equal(R.numListeners(), 1);                                                                        // 221
                                                                                                            // 222
    $(div).remove();                                                                                        // 223
                                                                                                            // 224
    test.equal(R.numListeners(), 0);                                                                        // 225
                                                                                                            // 226
    R.set('Steve');                                                                                         // 227
    Deps.flush();                                                                                           // 228
    // should not have changed:                                                                             // 229
    test.equal(canonicalizeHtml(div.innerHTML), "<p>World</p>");                                            // 230
  })();                                                                                                     // 231
                                                                                                            // 232
});                                                                                                         // 233
                                                                                                            // 234
Tinytest.add("ui - render - reactive attributes", function (test) {                                         // 235
  (function () {                                                                                            // 236
    var R = ReactiveVar({'class': ['david gre', CharRef({html: '&euml;', str: '\u00eb'}), 'nspan'],         // 237
                         id: 'foo'});                                                                       // 238
                                                                                                            // 239
    var spanCode = SPAN({$dynamic: [function () { return R.get(); }]});                                     // 240
                                                                                                            // 241
    test.equal(toHTML(spanCode), '<span class="david gre&euml;nspan" id="foo"></span>');                    // 242
                                                                                                            // 243
    test.equal(R.numListeners(), 0);                                                                        // 244
                                                                                                            // 245
    var div = document.createElement("DIV");                                                                // 246
    materialize(spanCode, div);                                                                             // 247
    test.equal(canonicalizeHtml(div.innerHTML), '<span class="david gre\u00ebnspan" id="foo"></span>');     // 248
                                                                                                            // 249
    test.equal(R.numListeners(), 1);                                                                        // 250
                                                                                                            // 251
    var span = div.firstChild;                                                                              // 252
    test.equal(span.nodeName, 'SPAN');                                                                      // 253
    span.className += ' blah';                                                                              // 254
                                                                                                            // 255
    R.set({'class': 'david smith', id: 'bar'});                                                             // 256
    Deps.flush();                                                                                           // 257
    test.equal(canonicalizeHtml(div.innerHTML), '<span class="david blah smith" id="bar"></span>');         // 258
    test.equal(R.numListeners(), 1);                                                                        // 259
                                                                                                            // 260
    R.set({});                                                                                              // 261
    Deps.flush();                                                                                           // 262
    test.equal(canonicalizeHtml(div.innerHTML), '<span class="blah"></span>');                              // 263
    test.equal(R.numListeners(), 1);                                                                        // 264
                                                                                                            // 265
    $(div).remove();                                                                                        // 266
                                                                                                            // 267
    test.equal(R.numListeners(), 0);                                                                        // 268
  })();                                                                                                     // 269
                                                                                                            // 270
  // Test `null`, `undefined`, and `[]` attributes                                                          // 271
  (function () {                                                                                            // 272
    var R = ReactiveVar({id: 'foo',                                                                         // 273
                         aaa: null,                                                                         // 274
                         bbb: undefined,                                                                    // 275
                         ccc: [],                                                                           // 276
                         ddd: [null],                                                                       // 277
                         eee: [undefined],                                                                  // 278
                         fff: [[]],                                                                         // 279
                         ggg: ['x', ['y', ['z']]]});                                                        // 280
                                                                                                            // 281
    var spanCode = SPAN({$dynamic: [function () { return R.get(); }]});                                     // 282
                                                                                                            // 283
    test.equal(toHTML(spanCode), '<span id="foo" ggg="xyz"></span>');                                       // 284
    test.equal(toCode(SPAN(R.get())),                                                                       // 285
               'HTML.SPAN({id: "foo", ggg: ["x", ["y", ["z"]]]})');                                         // 286
                                                                                                            // 287
    var div = document.createElement("DIV");                                                                // 288
    materialize(spanCode, div);                                                                             // 289
    var span = div.firstChild;                                                                              // 290
    test.equal(span.nodeName, 'SPAN');                                                                      // 291
                                                                                                            // 292
    test.equal(canonicalizeHtml(div.innerHTML), '<span ggg="xyz" id="foo"></span>');                        // 293
    R.set({id: 'foo', ggg: [[], [], []]});                                                                  // 294
    Deps.flush();                                                                                           // 295
    test.equal(canonicalizeHtml(div.innerHTML), '<span id="foo"></span>');                                  // 296
                                                                                                            // 297
    R.set({id: 'foo', ggg: null});                                                                          // 298
    Deps.flush();                                                                                           // 299
    test.equal(canonicalizeHtml(div.innerHTML), '<span id="foo"></span>');                                  // 300
                                                                                                            // 301
    R.set({id: 'foo', ggg: ''});                                                                            // 302
    Deps.flush();                                                                                           // 303
    test.equal(canonicalizeHtml(div.innerHTML), '<span ggg="" id="foo"></span>');                           // 304
                                                                                                            // 305
    $(span).remove();                                                                                       // 306
                                                                                                            // 307
    test.equal(R.numListeners(), 0);                                                                        // 308
  })();                                                                                                     // 309
});                                                                                                         // 310
                                                                                                            // 311
Tinytest.add("ui - render - components", function (test) {                                                  // 312
  (function () {                                                                                            // 313
    var counter = 1;                                                                                        // 314
    var buf = [];                                                                                           // 315
                                                                                                            // 316
    var myComponent = UI.Component.extend({                                                                 // 317
      init: function () {                                                                                   // 318
        // `this` is the component instance                                                                 // 319
        var number = counter++;                                                                             // 320
        this.number = number;                                                                               // 321
                                                                                                            // 322
        if (this.parent)                                                                                    // 323
          buf.push('parent of ' + this.number + ' is ' + this.parent.number);                               // 324
                                                                                                            // 325
        this.data = function () {                                                                           // 326
          return this.number;                                                                               // 327
        };                                                                                                  // 328
      },                                                                                                    // 329
      created: function () {                                                                                // 330
        // `this` is the template instance                                                                  // 331
        buf.push('created ' + this.data);                                                                   // 332
      },                                                                                                    // 333
      render: function () {                                                                                 // 334
        // `this` is the component instance                                                                 // 335
        return [String(this.number),                                                                        // 336
                                                                                                            // 337
                (this.number < 3 ? myComponent : HR())];                                                    // 338
      },                                                                                                    // 339
      rendered: function () {                                                                               // 340
        // `this` is the template instance                                                                  // 341
        var nodeDescr = function (node) {                                                                   // 342
          if (node.nodeType === 8) // comment                                                               // 343
            return '';                                                                                      // 344
          if (node.nodeType === 3) // text                                                                  // 345
            return node.nodeValue;                                                                          // 346
                                                                                                            // 347
          return node.nodeName;                                                                             // 348
        };                                                                                                  // 349
                                                                                                            // 350
        var start = this.firstNode;                                                                         // 351
        var end = this.lastNode;                                                                            // 352
        // skip marker nodes                                                                                // 353
        while (start !== end && ! nodeDescr(start))                                                         // 354
          start = start.nextSibling;                                                                        // 355
        while (end !== start && ! nodeDescr(end))                                                           // 356
          end = end.previousSibling;                                                                        // 357
                                                                                                            // 358
                                                                                                            // 359
        // `this` is the template instance                                                                  // 360
        buf.push('dom-' + this.data + ' is ' + nodeDescr(start) +'..' +                                     // 361
                 nodeDescr(end));                                                                           // 362
      },                                                                                                    // 363
      destroyed: function () {                                                                              // 364
        // `this` is the template instance                                                                  // 365
        buf.push('destroyed ' + this.data);                                                                 // 366
      }                                                                                                     // 367
    });                                                                                                     // 368
                                                                                                            // 369
    var div = document.createElement("DIV");                                                                // 370
                                                                                                            // 371
    materialize(myComponent, div);                                                                          // 372
    buf.push('---flush---');                                                                                // 373
    Deps.flush();                                                                                           // 374
    test.equal(buf, ['created 1',                                                                           // 375
                     'parent of 2 is 1',                                                                    // 376
                     'created 2',                                                                           // 377
                     'parent of 3 is 2',                                                                    // 378
                     'created 3',                                                                           // 379
                     '---flush---',                                                                         // 380
                     // (proper order for these has not be thought out:)                                    // 381
                     'dom-1 is 1..HR',                                                                      // 382
                     'dom-2 is 2..HR',                                                                      // 383
                     'dom-3 is 3..HR']);                                                                    // 384
                                                                                                            // 385
    test.equal(canonicalizeHtml(div.innerHTML), '123<hr>');                                                 // 386
                                                                                                            // 387
    buf.length = 0;                                                                                         // 388
    $(div).remove();                                                                                        // 389
    buf.sort();                                                                                             // 390
    test.equal(buf, ['destroyed 1', 'destroyed 2', 'destroyed 3']);                                         // 391
                                                                                                            // 392
    // Now use toHTML.  Should still get most of the callbacks (not `rendered`).                            // 393
                                                                                                            // 394
    buf.length = 0;                                                                                         // 395
    counter = 1;                                                                                            // 396
                                                                                                            // 397
    var html = toHTML(myComponent);                                                                         // 398
                                                                                                            // 399
    test.equal(buf, ['created 1',                                                                           // 400
                     'parent of 2 is 1',                                                                    // 401
                     'created 2',                                                                           // 402
                     'parent of 3 is 2',                                                                    // 403
                     'created 3']);                                                                         // 404
                                                                                                            // 405
    test.equal(html, '123<hr>');                                                                            // 406
  })();                                                                                                     // 407
});                                                                                                         // 408
                                                                                                            // 409
Tinytest.add("ui - render - reactive attributes 2", function (test) {                                       // 410
  var R1 = ReactiveVar(['foo']);                                                                            // 411
  var R2 = ReactiveVar(['bar']);                                                                            // 412
                                                                                                            // 413
  var spanCode = SPAN({                                                                                     // 414
    blah: function () { return R1.get(); },                                                                 // 415
    $dynamic: [function () { return { blah: [function () { return R2.get(); }] }; }]                        // 416
  });                                                                                                       // 417
                                                                                                            // 418
  var div = document.createElement("DIV");                                                                  // 419
  materialize(spanCode, div);                                                                               // 420
  var check = function (expected) {                                                                         // 421
    test.equal(toHTML(spanCode), expected);                                                                 // 422
    test.equal(canonicalizeHtml(div.innerHTML), expected);                                                  // 423
  };                                                                                                        // 424
  check('<span blah="bar"></span>');                                                                        // 425
                                                                                                            // 426
  test.equal(R1.numListeners(), 1);                                                                         // 427
  test.equal(R2.numListeners(), 1);                                                                         // 428
                                                                                                            // 429
  R2.set([[]]);                                                                                             // 430
  Deps.flush();                                                                                             // 431
  // We combine `['foo']` with what evaluates to `[[[]]]`, which is nully.                                  // 432
  test.equal(spanCode.evaluateAttributes().blah, ["foo"]);                                                  // 433
  check('<span blah="foo"></span>');                                                                        // 434
                                                                                                            // 435
  R2.set([['']]);                                                                                           // 436
  Deps.flush();                                                                                             // 437
  // We combine `['foo']` with what evaluates to `[[['']]]`, which is non-nully.                            // 438
  test.equal(spanCode.evaluateAttributes().blah, [[['']]]);                                                 // 439
  check('<span blah=""></span>');                                                                           // 440
                                                                                                            // 441
  R2.set(null);                                                                                             // 442
  Deps.flush();                                                                                             // 443
  // We combine `['foo']` with `[null]`, which is nully.                                                    // 444
  test.equal(spanCode.evaluateAttributes().blah, ['foo']);                                                  // 445
  check('<span blah="foo"></span>');                                                                        // 446
                                                                                                            // 447
  R1.set([[], []]);                                                                                         // 448
  Deps.flush();                                                                                             // 449
  // We combine two nully values.                                                                           // 450
  check('<span></span>');                                                                                   // 451
                                                                                                            // 452
  R1.set([[], ['foo']]);                                                                                    // 453
  Deps.flush();                                                                                             // 454
  check('<span blah="foo"></span>');                                                                        // 455
                                                                                                            // 456
  // clean up                                                                                               // 457
                                                                                                            // 458
  $(div).remove();                                                                                          // 459
                                                                                                            // 460
  test.equal(R1.numListeners(), 0);                                                                         // 461
  test.equal(R2.numListeners(), 0);                                                                         // 462
});                                                                                                         // 463
                                                                                                            // 464
Tinytest.add("ui - render - SVG", function (test) {                                                         // 465
  if (! document.createElementNS) {                                                                         // 466
    // IE 8                                                                                                 // 467
    return;                                                                                                 // 468
  }                                                                                                         // 469
                                                                                                            // 470
  var fillColor = ReactiveVar('red');                                                                       // 471
  var classes = ReactiveVar('one two');                                                                     // 472
                                                                                                            // 473
  var content = DIV({'class': 'container'}, HTML.SVG(                                                       // 474
    {width: 100, height: 100},                                                                              // 475
    HTML.CIRCLE({cx: 50, cy: 50, r: 40,                                                                     // 476
                 stroke: 'black', 'stroke-width': 3,                                                        // 477
                 'class': function () { return classes.get(); },                                            // 478
                 fill: function () { return fillColor.get(); }})));                                         // 479
                                                                                                            // 480
  var div = document.createElement("DIV");                                                                  // 481
  materialize(content, div);                                                                                // 482
                                                                                                            // 483
  var circle = div.querySelector('.container > svg > circle');                                              // 484
  test.equal(circle.getAttribute('fill'), 'red');                                                           // 485
  test.equal(circle.className.baseVal, 'one two');                                                          // 486
                                                                                                            // 487
  fillColor.set('green');                                                                                   // 488
  classes.set('two three');                                                                                 // 489
  Deps.flush();                                                                                             // 490
  test.equal(circle.getAttribute('fill'), 'green');                                                         // 491
  test.equal(circle.className.baseVal, 'two three');                                                        // 492
                                                                                                            // 493
  test.equal(circle.nodeName, 'circle');                                                                    // 494
  test.equal(circle.namespaceURI, "http://www.w3.org/2000/svg");                                            // 495
  test.equal(circle.parentNode.namespaceURI, "http://www.w3.org/2000/svg");                                 // 496
});                                                                                                         // 497
                                                                                                            // 498
Tinytest.add("ui - UI.render", function (test) {                                                            // 499
  var div = document.createElement("DIV");                                                                  // 500
  document.body.appendChild(div);                                                                           // 501
                                                                                                            // 502
  var R = ReactiveVar('aaa');                                                                               // 503
  var tmpl = UI.Component.extend({                                                                          // 504
    render: function () {                                                                                   // 505
      var self = this;                                                                                      // 506
      return SPAN(function () {                                                                             // 507
        return (self.get('greeting') || 'Hello') + ' ' + R.get();                                           // 508
      });                                                                                                   // 509
    }                                                                                                       // 510
  });                                                                                                       // 511
                                                                                                            // 512
  UI.insert(UI.render(tmpl), div);                                                                          // 513
  UI.insert(UI.renderWithData(tmpl, {greeting: 'Bye'}), div);                                               // 514
  test.equal(canonicalizeHtml(div.innerHTML),                                                               // 515
             "<span>Hello aaa</span><span>Bye aaa</span>");                                                 // 516
  R.set('bbb');                                                                                             // 517
  Deps.flush();                                                                                             // 518
  test.equal(canonicalizeHtml(div.innerHTML),                                                               // 519
             "<span>Hello bbb</span><span>Bye bbb</span>");                                                 // 520
                                                                                                            // 521
  document.body.removeChild(div);                                                                           // 522
});                                                                                                         // 523
                                                                                                            // 524
Tinytest.add("ui - UI.getDataContext", function (test) {                                                    // 525
  var div = document.createElement("DIV");                                                                  // 526
                                                                                                            // 527
  var tmpl = UI.Component.extend({                                                                          // 528
    render: function () {                                                                                   // 529
      return SPAN();                                                                                        // 530
    }                                                                                                       // 531
  });                                                                                                       // 532
                                                                                                            // 533
  UI.insert(UI.renderWithData(tmpl, {foo: "bar"}), div);                                                    // 534
  var span = $(div).children('SPAN')[0];                                                                    // 535
  test.isTrue(span);                                                                                        // 536
  test.equal(UI.getElementData(span), {foo: "bar"});                                                        // 537
});                                                                                                         // 538
                                                                                                            // 539
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/ui/dombackend_tests.js                                                                          //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
var runDivSpanBTest = function (func) {                                                                     // 2
  // Common code                                                                                            // 3
                                                                                                            // 4
  var div = document.createElement("DIV");                                                                  // 5
  var span = document.createElement("SPAN");                                                                // 6
  var b = document.createElement("B");                                                                      // 7
  div.appendChild(span);                                                                                    // 8
  span.appendChild(b);                                                                                      // 9
                                                                                                            // 10
  var buf = [];                                                                                             // 11
                                                                                                            // 12
  var func1 = function (elem) { buf.push(elem.nodeName + "1"); };                                           // 13
  var func2 = function (elem) { buf.push(elem.nodeName + "2"); };                                           // 14
  var func3 = function (elem) { buf.push(elem.nodeName + "3"); };                                           // 15
  var func4 = function (elem) { buf.push(elem.nodeName + "4"); };                                           // 16
                                                                                                            // 17
  func(div, span, b, buf, func1, func2, func3, func4);                                                      // 18
};                                                                                                          // 19
                                                                                                            // 20
var DomBackend =  UI.DomBackend;                                                                            // 21
                                                                                                            // 22
// Essentially test that the `node` argument has been removed from the                                      // 23
// DOM. The caveat is that in IE8, calling `removeChild` leaves the                                         // 24
// removed child with a document fragment parent, which itself has no                                       // 25
// parent.                                                                                                  // 26
var isDetachedSingleNode = function (test, node) {                                                          // 27
  if (!node.parentNode) {                                                                                   // 28
    test.ok();                                                                                              // 29
  } else {                                                                                                  // 30
    test.equal(node.parentNode.nodeName, '#document-fragment');                                             // 31
    test.equal(node.parentNode.childNodes.length, 1);                                                       // 32
    test.equal(node.parentNode.parentNode, null);                                                           // 33
  }                                                                                                         // 34
};                                                                                                          // 35
                                                                                                            // 36
Tinytest.add("ui - DomBackend - element removal", function (test) {                                         // 37
  // Test that calling removeElement on a detached element calls onRemoveElement                            // 38
  // on it and its descendents. For jQuery, `removeElement` runs `$(elem).remove()`,                        // 39
  // so it tests detecting a jQuery removal, as well as the stronger condition                              // 40
  // that clean-up still happens on the DOM tree in the detached case.                                      // 41
  runDivSpanBTest(function (div, span, b, buf, func1, func2, func3, func4) {                                // 42
    DomBackend.onRemoveElement(div, func1);                                                                 // 43
    DomBackend.onRemoveElement(span, func2);                                                                // 44
    DomBackend.onRemoveElement(b, func3);                                                                   // 45
    // test second callback on same element                                                                 // 46
    DomBackend.onRemoveElement(div, func4);                                                                 // 47
                                                                                                            // 48
    DomBackend.removeElement(div); // "remove" the (parentless) DIV                                         // 49
                                                                                                            // 50
    buf.sort();                                                                                             // 51
    test.equal(buf, ["B3", "DIV1", "DIV4", "SPAN2"]);                                                       // 52
                                                                                                            // 53
    buf.length = 0;                                                                                         // 54
    DomBackend.removeElement(div);                                                                          // 55
    test.equal(buf.length, 0);                                                                              // 56
  });                                                                                                       // 57
                                                                                                            // 58
  // Test that `removeElement` actually removes the element                                                 // 59
  // (and fires appropriate callbacks).                                                                     // 60
  runDivSpanBTest(function (div, span, b, buf, func1, func2, func3, func4) {                                // 61
    DomBackend.onRemoveElement(div, func1);                                                                 // 62
    DomBackend.onRemoveElement(span, func2);                                                                // 63
    DomBackend.onRemoveElement(b, func3);                                                                   // 64
    DomBackend.onRemoveElement(div, func4);                                                                 // 65
                                                                                                            // 66
    DomBackend.removeElement(span); // remove the SPAN                                                      // 67
                                                                                                            // 68
    test.equal(div.childNodes.length, 0);                                                                   // 69
    isDetachedSingleNode(test, span);                                                                       // 70
                                                                                                            // 71
    buf.sort();                                                                                             // 72
    test.equal(buf, ["B3", "SPAN2"]);                                                                       // 73
                                                                                                            // 74
    buf.length = 0;                                                                                         // 75
    DomBackend.removeElement(div); // remove the DIV                                                        // 76
    test.equal(buf, ["DIV1", "DIV4"]);                                                                      // 77
  });                                                                                                       // 78
                                                                                                            // 79
});                                                                                                         // 80
                                                                                                            // 81
Tinytest.add("ui - DomBackend - element removal (jQuery)", function (test) {                                // 82
                                                                                                            // 83
  // Test with `$(elem).remove()`.                                                                          // 84
  runDivSpanBTest(function (div, span, b, buf, func1, func2, func3, func4) {                                // 85
    DomBackend.onRemoveElement(div, func1);                                                                 // 86
    DomBackend.onRemoveElement(span, func2);                                                                // 87
    DomBackend.onRemoveElement(b, func3);                                                                   // 88
    DomBackend.onRemoveElement(div, func4);                                                                 // 89
                                                                                                            // 90
    $(span).remove(); // remove the SPAN                                                                    // 91
                                                                                                            // 92
    test.equal(div.childNodes.length, 0);                                                                   // 93
    isDetachedSingleNode(test, span);                                                                       // 94
                                                                                                            // 95
    buf.sort();                                                                                             // 96
    test.equal(buf, ["B3", "SPAN2"]);                                                                       // 97
                                                                                                            // 98
    buf.length = 0;                                                                                         // 99
    $(div).remove(); // "remove" the DIV                                                                    // 100
    test.equal(buf, ["DIV1", "DIV4"]);                                                                      // 101
  });                                                                                                       // 102
                                                                                                            // 103
  // Test that `$(elem).detach()` is NOT considered a removal.                                              // 104
  runDivSpanBTest(function (div, span, b, buf, func1, func2, func3, func4) {                                // 105
    DomBackend.onRemoveElement(div, func1);                                                                 // 106
    DomBackend.onRemoveElement(span, func2);                                                                // 107
    DomBackend.onRemoveElement(b, func3);                                                                   // 108
    DomBackend.onRemoveElement(div, func4);                                                                 // 109
                                                                                                            // 110
    $(span).detach(); // detach the SPAN                                                                    // 111
                                                                                                            // 112
    test.equal(div.childNodes.length, 0);                                                                   // 113
    isDetachedSingleNode(test, span);                                                                       // 114
                                                                                                            // 115
    test.equal(buf, []);                                                                                    // 116
                                                                                                            // 117
    buf.length = 0;                                                                                         // 118
    $(div).detach(); // "detach" the DIV                                                                    // 119
    test.equal(buf, []);                                                                                    // 120
  });                                                                                                       // 121
                                                                                                            // 122
});                                                                                                         // 123
                                                                                                            // 124
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
