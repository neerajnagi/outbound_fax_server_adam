(function () {

///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
// packages/browser-policy-content/browser-policy-content.js                     //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////
                                                                                 //
// By adding this package, you get the following default policy:                 // 1
// No eval or other string-to-code, and content can only be loaded from the      // 2
// same origin as the app (except for XHRs and websocket connections, which can  // 3
// go to any origin).                                                            // 4
//                                                                               // 5
// Apps should call BrowserPolicy.content.disallowInlineScripts() if they are    // 6
// not using any inline script tags and are willing to accept an extra round     // 7
// trip on page load.                                                            // 8
//                                                                               // 9
// BrowserPolicy.content functions for tweaking CSP:                             // 10
// allowInlineScripts()                                                          // 11
// disallowInlineScripts(): adds extra round-trip to page load time              // 12
// allowInlineStyles()                                                           // 13
// disallowInlineStyles()                                                        // 14
// allowEval()                                                                   // 15
// disallowEval()                                                                // 16
//                                                                               // 17
// For each type of content (script, object, image, media, font, connect,        // 18
// style), there are the following functions:                                    // 19
// allow<content type>Origin(origin): allows the type of content to be loaded    // 20
// from the given origin                                                         // 21
// allow<content type>DataUrl(): allows the content to be loaded from data: URLs // 22
// allow<content type>SameOrigin(): allows the content to be loaded from the     // 23
// same origin                                                                   // 24
// disallow<content type>(): disallows this type of content all together (can't  // 25
// be called for script)                                                         // 26
//                                                                               // 27
// The following functions allow you to set rules for all types of content at    // 28
// once:                                                                         // 29
// allowAllContentOrigin(origin)                                                 // 30
// allowAllContentDataUrl()                                                      // 31
// allowAllContentSameOrigin()                                                   // 32
// disallowAllContent()                                                          // 33
//                                                                               // 34
                                                                                 // 35
var cspSrcs;                                                                     // 36
var cachedCsp; // Avoid constructing the header out of cspSrcs when possible.    // 37
                                                                                 // 38
// CSP keywords have to be single-quoted.                                        // 39
var keywords = {                                                                 // 40
  unsafeInline: "'unsafe-inline'",                                               // 41
  unsafeEval: "'unsafe-eval'",                                                   // 42
  self: "'self'",                                                                // 43
  none: "'none'"                                                                 // 44
};                                                                               // 45
                                                                                 // 46
BrowserPolicy.content = {};                                                      // 47
                                                                                 // 48
var parseCsp = function (csp) {                                                  // 49
  var policies = csp.split("; ");                                                // 50
  cspSrcs = {};                                                                  // 51
  _.each(policies, function (policy) {                                           // 52
    if (policy[policy.length - 1] === ";")                                       // 53
      policy = policy.substring(0, policy.length - 1);                           // 54
    var srcs = policy.split(" ");                                                // 55
    var directive = srcs[0];                                                     // 56
    if (_.indexOf(srcs, keywords.none) !== -1)                                   // 57
      cspSrcs[directive] = null;                                                 // 58
    else                                                                         // 59
      cspSrcs[directive] = srcs.slice(1);                                        // 60
  });                                                                            // 61
                                                                                 // 62
  if (cspSrcs["default-src"] === undefined)                                      // 63
    throw new Error("Content Security Policies used with " +                     // 64
                    "browser-policy must specify a default-src.");               // 65
                                                                                 // 66
  // Copy default-src sources to other directives.                               // 67
  _.each(cspSrcs, function (sources, directive) {                                // 68
    cspSrcs[directive] = _.union(sources || [], cspSrcs["default-src"] || []);   // 69
  });                                                                            // 70
};                                                                               // 71
                                                                                 // 72
var removeCspSrc = function (directive, src) {                                   // 73
  cspSrcs[directive] = _.without(cspSrcs[directive] || [], src);                 // 74
};                                                                               // 75
                                                                                 // 76
// Prepare for a change to cspSrcs. Ensure that we have a key in the dictionary  // 77
// and clear any cached CSP.                                                     // 78
var prepareForCspDirective = function (directive) {                              // 79
  cspSrcs = cspSrcs || {};                                                       // 80
  cachedCsp = null;                                                              // 81
  if (! _.has(cspSrcs, directive))                                               // 82
    cspSrcs[directive] = _.clone(cspSrcs["default-src"]);                        // 83
};                                                                               // 84
                                                                                 // 85
// Add `src` to the list of allowed sources for `directive`, with the            // 86
// following modifications if `src` is an origin:                                // 87
// - If `src` does not have a protocol specified, then add both                  // 88
//   http://<src> and https://<src>. This is to mask differing                   // 89
//   cross-browser behavior; some browsers interpret an origin without a         // 90
//   protocol as http://<src> and some interpret it as both http://<src>         // 91
//   and https://<src>                                                           // 92
// - Trim trailing slashes from `src`, since some browsers interpret             // 93
//   "foo.com/" as "foo.com" and some don't.                                     // 94
var addSourceForDirective = function (directive, src) {                          // 95
  if (_.contains(_.values(keywords), src)) {                                     // 96
    cspSrcs[directive].push(src);                                                // 97
  } else {                                                                       // 98
    src = src.toLowerCase();                                                     // 99
                                                                                 // 100
    // Trim trailing slashes.                                                    // 101
    src = src.replace(/\/+$/, '');                                               // 102
                                                                                 // 103
    var toAdd = [];                                                              // 104
    // If there is no protocol, add both http:// and https://.                   // 105
    if (! /^([a-z0-9.+-]+:)/.test(src)) {                                        // 106
      toAdd.push("http://" + src);                                               // 107
      toAdd.push("https://" + src);                                              // 108
    } else {                                                                     // 109
      toAdd.push(src);                                                           // 110
    }                                                                            // 111
    _.each(toAdd, function (s) {                                                 // 112
      cspSrcs[directive].push(s);                                                // 113
    });                                                                          // 114
  }                                                                              // 115
};                                                                               // 116
                                                                                 // 117
var setDefaultPolicy = function () {                                             // 118
  // By default, unsafe inline scripts and styles are allowed, since we expect   // 119
  // many apps will use them for analytics, etc. Unsafe eval is disallowed, and  // 120
  // the only allowable content source is the same origin or data, except for    // 121
  // connect which allows anything (since meteor.com apps make websocket         // 122
  // connections to a lot of different origins).                                 // 123
  BrowserPolicy.content.setPolicy("default-src 'self'; " +                       // 124
                                  "script-src 'self' 'unsafe-inline'; " +        // 125
                                  "connect-src *; " +                            // 126
                                  "img-src data: 'self'; " +                     // 127
                                  "style-src 'self' 'unsafe-inline';");          // 128
};                                                                               // 129
                                                                                 // 130
var setWebAppInlineScripts = function (value) {                                  // 131
  if (! BrowserPolicy._runningTest())                                            // 132
    WebAppInternals.setInlineScriptsAllowed(value);                              // 133
};                                                                               // 134
                                                                                 // 135
_.extend(BrowserPolicy.content, {                                                // 136
  // Exported for tests and browser-policy-common.                               // 137
  _constructCsp: function () {                                                   // 138
    if (! cspSrcs || _.isEmpty(cspSrcs))                                         // 139
      return null;                                                               // 140
                                                                                 // 141
    if (cachedCsp)                                                               // 142
      return cachedCsp;                                                          // 143
                                                                                 // 144
    var header = _.map(cspSrcs, function (srcs, directive) {                     // 145
      srcs = srcs || [];                                                         // 146
      if (_.isEmpty(srcs))                                                       // 147
        srcs = [keywords.none];                                                  // 148
      var directiveCsp = _.uniq(srcs).join(" ");                                 // 149
      return directive + " " + directiveCsp + ";";                               // 150
    });                                                                          // 151
                                                                                 // 152
    header = header.join(" ");                                                   // 153
    cachedCsp = header;                                                          // 154
    return header;                                                               // 155
  },                                                                             // 156
  _reset: function () {                                                          // 157
    cachedCsp = null;                                                            // 158
    setDefaultPolicy();                                                          // 159
  },                                                                             // 160
                                                                                 // 161
  setPolicy: function (csp) {                                                    // 162
    cachedCsp = null;                                                            // 163
    parseCsp(csp);                                                               // 164
    setWebAppInlineScripts(                                                      // 165
      BrowserPolicy.content._keywordAllowed("script-src", keywords.unsafeInline) // 166
    );                                                                           // 167
  },                                                                             // 168
                                                                                 // 169
  _keywordAllowed: function (directive, keyword) {                               // 170
    return (cspSrcs[directive] &&                                                // 171
            _.indexOf(cspSrcs[directive], keyword) !== -1);                      // 172
  },                                                                             // 173
                                                                                 // 174
  // Helpers for creating content security policies                              // 175
                                                                                 // 176
  allowInlineScripts: function () {                                              // 177
    prepareForCspDirective("script-src");                                        // 178
    cspSrcs["script-src"].push(keywords.unsafeInline);                           // 179
    setWebAppInlineScripts(true);                                                // 180
  },                                                                             // 181
  disallowInlineScripts: function () {                                           // 182
    prepareForCspDirective("script-src");                                        // 183
    removeCspSrc("script-src", keywords.unsafeInline);                           // 184
    setWebAppInlineScripts(false);                                               // 185
  },                                                                             // 186
  allowEval: function () {                                                       // 187
    prepareForCspDirective("script-src");                                        // 188
    cspSrcs["script-src"].push(keywords.unsafeEval);                             // 189
  },                                                                             // 190
  disallowEval: function () {                                                    // 191
    prepareForCspDirective("script-src");                                        // 192
    removeCspSrc("script-src", keywords.unsafeEval);                             // 193
  },                                                                             // 194
  allowInlineStyles: function () {                                               // 195
    prepareForCspDirective("style-src");                                         // 196
    cspSrcs["style-src"].push(keywords.unsafeInline);                            // 197
  },                                                                             // 198
  disallowInlineStyles: function () {                                            // 199
    prepareForCspDirective("style-src");                                         // 200
    removeCspSrc("style-src", keywords.unsafeInline);                            // 201
  },                                                                             // 202
                                                                                 // 203
  // Functions for setting defaults                                              // 204
  allowSameOriginForAll: function () {                                           // 205
    BrowserPolicy.content.allowOriginForAll(keywords.self);                      // 206
  },                                                                             // 207
  allowDataUrlForAll: function () {                                              // 208
    BrowserPolicy.content.allowOriginForAll("data:");                            // 209
  },                                                                             // 210
  allowOriginForAll: function (origin) {                                         // 211
    prepareForCspDirective("default-src");                                       // 212
    _.each(_.keys(cspSrcs), function (directive) {                               // 213
      addSourceForDirective(directive, origin);                                  // 214
    });                                                                          // 215
  },                                                                             // 216
  disallowAll: function () {                                                     // 217
    cachedCsp = null;                                                            // 218
    cspSrcs = {                                                                  // 219
      "default-src": []                                                          // 220
    };                                                                           // 221
    setWebAppInlineScripts(false);                                               // 222
  }                                                                              // 223
});                                                                              // 224
                                                                                 // 225
// allow<Resource>Origin, allow<Resource>Data, allow<Resource>self, and          // 226
// disallow<Resource> methods for each type of resource.                         // 227
_.each(["script", "object", "img", "media",                                      // 228
        "font", "connect", "style", "frame"],                                    // 229
       function (resource) {                                                     // 230
         var directive = resource + "-src";                                      // 231
         var methodResource;                                                     // 232
         if (resource !== "img") {                                               // 233
           methodResource = resource.charAt(0).toUpperCase() +                   // 234
             resource.slice(1);                                                  // 235
         } else {                                                                // 236
           methodResource = "Image";                                             // 237
         }                                                                       // 238
         var allowMethodName = "allow" + methodResource + "Origin";              // 239
         var disallowMethodName = "disallow" + methodResource;                   // 240
         var allowDataMethodName = "allow" + methodResource + "DataUrl";         // 241
         var allowSelfMethodName = "allow" + methodResource + "SameOrigin";      // 242
                                                                                 // 243
         var disallow = function () {                                            // 244
           cachedCsp = null;                                                     // 245
           cspSrcs[directive] = [];                                              // 246
         };                                                                      // 247
                                                                                 // 248
         BrowserPolicy.content[allowMethodName] = function (src) {               // 249
           prepareForCspDirective(directive);                                    // 250
           addSourceForDirective(directive, src);                                // 251
         };                                                                      // 252
         if (resource === "script") {                                            // 253
           BrowserPolicy.content[disallowMethodName] = function () {             // 254
             disallow();                                                         // 255
             setWebAppInlineScripts(false);                                      // 256
           };                                                                    // 257
         } else {                                                                // 258
           BrowserPolicy.content[disallowMethodName] = disallow;                 // 259
         }                                                                       // 260
         BrowserPolicy.content[allowDataMethodName] = function () {              // 261
           prepareForCspDirective(directive);                                    // 262
           cspSrcs[directive].push("data:");                                     // 263
         };                                                                      // 264
         BrowserPolicy.content[allowSelfMethodName] = function () {              // 265
           prepareForCspDirective(directive);                                    // 266
           cspSrcs[directive].push(keywords.self);                               // 267
         };                                                                      // 268
       });                                                                       // 269
                                                                                 // 270
                                                                                 // 271
setDefaultPolicy();                                                              // 272
                                                                                 // 273
///////////////////////////////////////////////////////////////////////////////////

}).call(this);
