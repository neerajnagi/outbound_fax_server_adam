(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// packages/minimongo/wrap_transform_tests.js                                                                         //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
Tinytest.add("minimongo - wrapTransform", function (test) {                                                           // 1
  var wrap = LocalCollection.wrapTransform;                                                                           // 2
                                                                                                                      // 3
  // Transforming no function gives falsey.                                                                           // 4
  test.isFalse(wrap(undefined));                                                                                      // 5
  test.isFalse(wrap(null));                                                                                           // 6
                                                                                                                      // 7
  // It's OK if you don't change the ID.                                                                              // 8
  var validTransform = function (doc) {                                                                               // 9
    delete doc.x;                                                                                                     // 10
    doc.y = 42;                                                                                                       // 11
    doc.z = function () { return 43; };                                                                               // 12
    return doc;                                                                                                       // 13
  };                                                                                                                  // 14
  var transformed = wrap(validTransform)({_id: "asdf", x: 54});                                                       // 15
  test.equal(_.keys(transformed), ['_id', 'y', 'z']);                                                                 // 16
  test.equal(transformed.y, 42);                                                                                      // 17
  test.equal(transformed.z(), 43);                                                                                    // 18
                                                                                                                      // 19
  // Ensure that ObjectIDs work (even if the _ids in question are not ===-equal)                                      // 20
  var oid1 = new LocalCollection._ObjectID();                                                                         // 21
  var oid2 = new LocalCollection._ObjectID(oid1.toHexString());                                                       // 22
  test.equal(wrap(function () {return {_id: oid2};})({_id: oid1}),                                                    // 23
             {_id: oid2});                                                                                            // 24
                                                                                                                      // 25
  // transform functions must return objects                                                                          // 26
  var invalidObjects = [                                                                                              // 27
    "asdf", new LocalCollection._ObjectID(), false, null, true,                                                       // 28
    27, [123], /adsf/, new Date, function () {}, undefined                                                            // 29
  ];                                                                                                                  // 30
  _.each(invalidObjects, function (invalidObject) {                                                                   // 31
    var wrapped = wrap(function () { return invalidObject; });                                                        // 32
    test.throws(function () {                                                                                         // 33
      wrapped({_id: "asdf"});                                                                                         // 34
    });                                                                                                               // 35
  }, /transform must return object/);                                                                                 // 36
                                                                                                                      // 37
  // transform functions may not change _ids                                                                          // 38
  var wrapped = wrap(function (doc) { doc._id = 'x'; return doc; });                                                  // 39
  test.throws(function () {                                                                                           // 40
    wrapped({_id: 'y'});                                                                                              // 41
  }, /can't have different _id/);                                                                                     // 42
                                                                                                                      // 43
  // transform functions may remove _ids                                                                              // 44
  test.equal({_id: 'a', x: 2},                                                                                        // 45
             wrap(function (d) {delete d._id; return d;})({_id: 'a', x: 2}));                                         // 46
                                                                                                                      // 47
  // test that wrapped transform functions are nonreactive                                                            // 48
  var unwrapped = function (doc) {                                                                                    // 49
    test.isFalse(Deps.active);                                                                                        // 50
    return doc;                                                                                                       // 51
  };                                                                                                                  // 52
  var handle = Deps.autorun(function () {                                                                             // 53
    test.isTrue(Deps.active);                                                                                         // 54
    wrap(unwrapped)({_id: "xxx"});                                                                                    // 55
  });                                                                                                                 // 56
  handle.stop();                                                                                                      // 57
});                                                                                                                   // 58
                                                                                                                      // 59
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// packages/minimongo/minimongo_server_tests.js                                                                       //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
Tinytest.add("minimongo - modifier affects selector", function (test) {                                               // 1
  function testSelectorPaths (sel, paths, desc) {                                                                     // 2
    var matcher = new Minimongo.Matcher(sel);                                                                         // 3
    test.equal(matcher._getPaths(), paths, desc);                                                                     // 4
  }                                                                                                                   // 5
                                                                                                                      // 6
  testSelectorPaths({                                                                                                 // 7
    foo: {                                                                                                            // 8
      bar: 3,                                                                                                         // 9
      baz: 42                                                                                                         // 10
    }                                                                                                                 // 11
  }, ['foo'], "literal");                                                                                             // 12
                                                                                                                      // 13
  testSelectorPaths({                                                                                                 // 14
    foo: 42,                                                                                                          // 15
    bar: 33                                                                                                           // 16
  }, ['foo', 'bar'], "literal");                                                                                      // 17
                                                                                                                      // 18
  testSelectorPaths({                                                                                                 // 19
    foo: [ 'something' ],                                                                                             // 20
    bar: "asdf"                                                                                                       // 21
  }, ['foo', 'bar'], "literal");                                                                                      // 22
                                                                                                                      // 23
  testSelectorPaths({                                                                                                 // 24
    a: { $lt: 3 },                                                                                                    // 25
    b: "you know, literal",                                                                                           // 26
    'path.is.complicated': { $not: { $regex: 'acme.*corp' } }                                                         // 27
  }, ['a', 'b', 'path.is.complicated'], "literal + operators");                                                       // 28
                                                                                                                      // 29
  testSelectorPaths({                                                                                                 // 30
    $or: [{ 'a.b': 1 }, { 'a.b.c': { $lt: 22 } },                                                                     // 31
     {$and: [{ 'x.d': { $ne: 5, $gte: 433 } }, { 'a.b': 234 }]}]                                                      // 32
  }, ['a.b', 'a.b.c', 'x.d'], 'group operators + duplicates');                                                        // 33
                                                                                                                      // 34
  // When top-level value is an object, it is treated as a literal,                                                   // 35
  // so when you query col.find({ a: { foo: 1, bar: 2 } })                                                            // 36
  // it doesn't mean you are looking for anything that has 'a.foo' to be 1 and                                        // 37
  // 'a.bar' to be 2, instead you are looking for 'a' to be exatly that object                                        // 38
  // with exatly that order of keys. { a: { foo: 1, bar: 2, baz: 3 } } wouldn't                                       // 39
  // match it. That's why in this selector 'a' would be important key, not a.foo                                      // 40
  // and a.bar.                                                                                                       // 41
  testSelectorPaths({                                                                                                 // 42
    a: {                                                                                                              // 43
      foo: 1,                                                                                                         // 44
      bar: 2                                                                                                          // 45
    },                                                                                                                // 46
    'b.c': {                                                                                                          // 47
      literal: "object",                                                                                              // 48
      but: "we still observe any changes in 'b.c'"                                                                    // 49
    }                                                                                                                 // 50
  }, ['a', 'b.c'], "literal object");                                                                                 // 51
                                                                                                                      // 52
  // Note that a and b do NOT end up in the path list, but x and y both do.                                           // 53
  testSelectorPaths({                                                                                                 // 54
    $or: [                                                                                                            // 55
      {x: {$elemMatch: {a: 5}}},                                                                                      // 56
      {y: {$elemMatch: {b: 7}}}                                                                                       // 57
    ]                                                                                                                 // 58
  }, ['x', 'y'], "$or and elemMatch");                                                                                // 59
                                                                                                                      // 60
  function testSelectorAffectedByModifier (sel, mod, yes, desc) {                                                     // 61
    var matcher = new Minimongo.Matcher(sel);                                                                         // 62
    test.equal(matcher.affectedByModifier(mod), yes, desc);                                                           // 63
  }                                                                                                                   // 64
                                                                                                                      // 65
  function affected(sel, mod, desc) {                                                                                 // 66
    testSelectorAffectedByModifier(sel, mod, true, desc);                                                             // 67
  }                                                                                                                   // 68
  function notAffected(sel, mod, desc) {                                                                              // 69
    testSelectorAffectedByModifier(sel, mod, false, desc);                                                            // 70
  }                                                                                                                   // 71
                                                                                                                      // 72
  notAffected({ foo: 0 }, { $set: { bar: 1 } }, "simplest");                                                          // 73
  affected({ foo: 0 }, { $set: { foo: 1 } }, "simplest");                                                             // 74
  affected({ foo: 0 }, { $set: { 'foo.bar': 1 } }, "simplest");                                                       // 75
  notAffected({ 'foo.bar': 0 }, { $set: { 'foo.baz': 1 } }, "simplest");                                              // 76
  affected({ 'foo.bar': 0 }, { $set: { 'foo.1': 1 } }, "simplest");                                                   // 77
  affected({ 'foo.bar': 0 }, { $set: { 'foo.2.bar': 1 } }, "simplest");                                               // 78
                                                                                                                      // 79
  notAffected({ 'foo': 0 }, { $set: { 'foobaz': 1 } }, "correct prefix check");                                       // 80
  notAffected({ 'foobar': 0 }, { $unset: { 'foo': 1 } }, "correct prefix check");                                     // 81
  notAffected({ 'foo.bar': 0 }, { $unset: { 'foob': 1 } }, "correct prefix check");                                   // 82
                                                                                                                      // 83
  notAffected({ 'foo.Infinity.x': 0 }, { $unset: { 'foo.x': 1 } }, "we convert integer fields correctly");            // 84
  notAffected({ 'foo.1e3.x': 0 }, { $unset: { 'foo.x': 1 } }, "we convert integer fields correctly");                 // 85
                                                                                                                      // 86
  affected({ 'foo.3.bar': 0 }, { $set: { 'foo.3.bar': 1 } }, "observe for an array element");                         // 87
                                                                                                                      // 88
  notAffected({ 'foo.4.bar.baz': 0 }, { $unset: { 'foo.3.bar': 1 } }, "delicate work with numeric fields in selector");
  notAffected({ 'foo.4.bar.baz': 0 }, { $unset: { 'foo.bar': 1 } }, "delicate work with numeric fields in selector"); // 90
  affected({ 'foo.4.bar.baz': 0 }, { $unset: { 'foo.4.bar': 1 } }, "delicate work with numeric fields in selector");  // 91
  affected({ 'foo.bar.baz': 0 }, { $unset: { 'foo.3.bar': 1 } }, "delicate work with numeric fields in selector");    // 92
                                                                                                                      // 93
  affected({ 'foo.0.bar': 0 }, { $set: { 'foo.0.0.bar': 1 } }, "delicate work with nested arrays and selectors by indecies");
                                                                                                                      // 95
  affected({foo: {$elemMatch: {bar: 5}}}, {$set: {'foo.4.bar': 5}}, "$elemMatch");                                    // 96
});                                                                                                                   // 97
                                                                                                                      // 98
Tinytest.add("minimongo - selector and projection combination", function (test) {                                     // 99
  function testSelProjectionComb (sel, proj, expected, desc) {                                                        // 100
    var matcher = new Minimongo.Matcher(sel);                                                                         // 101
    test.equal(matcher.combineIntoProjection(proj), expected, desc);                                                  // 102
  }                                                                                                                   // 103
                                                                                                                      // 104
  // Test with inclusive projection                                                                                   // 105
  testSelProjectionComb({ a: 1, b: 2 }, { b: 1, c: 1, d: 1 }, { a: true, b: true, c: true, d: true }, "simplest incl");
  testSelProjectionComb({ $or: [{ a: 1234, e: {$lt: 5} }], b: 2 }, { b: 1, c: 1, d: 1 }, { a: true, b: true, c: true, d: true, e: true }, "simplest incl, branching");
  testSelProjectionComb({                                                                                             // 108
    'a.b': { $lt: 3 },                                                                                                // 109
    'y.0': -1,                                                                                                        // 110
    'a.c': 15                                                                                                         // 111
  }, {                                                                                                                // 112
    'd': 1,                                                                                                           // 113
    'z': 1                                                                                                            // 114
  }, {                                                                                                                // 115
    'a.b': true,                                                                                                      // 116
    'y': true,                                                                                                        // 117
    'a.c': true,                                                                                                      // 118
    'd': true,                                                                                                        // 119
    'z': true                                                                                                         // 120
  }, "multikey paths in selector - incl");                                                                            // 121
                                                                                                                      // 122
  testSelProjectionComb({                                                                                             // 123
    foo: 1234,                                                                                                        // 124
    $and: [{ k: -1 }, { $or: [{ b: 15 }] }]                                                                           // 125
  }, {                                                                                                                // 126
    'foo.bar': 1,                                                                                                     // 127
    'foo.zzz': 1,                                                                                                     // 128
    'b.asdf': 1                                                                                                       // 129
  }, {                                                                                                                // 130
    foo: true,                                                                                                        // 131
    b: true,                                                                                                          // 132
    k: true                                                                                                           // 133
  }, "multikey paths in fields - incl");                                                                              // 134
                                                                                                                      // 135
  testSelProjectionComb({                                                                                             // 136
    'a.b.c': 123,                                                                                                     // 137
    'a.b.d': 321,                                                                                                     // 138
    'b.c.0': 111,                                                                                                     // 139
    'a.e': 12345                                                                                                      // 140
  }, {                                                                                                                // 141
    'a.b.z': 1,                                                                                                       // 142
    'a.b.d.g': 1,                                                                                                     // 143
    'c.c.c': 1                                                                                                        // 144
  }, {                                                                                                                // 145
    'a.b.c': true,                                                                                                    // 146
    'a.b.d': true,                                                                                                    // 147
    'a.b.z': true,                                                                                                    // 148
    'b.c': true,                                                                                                      // 149
    'a.e': true,                                                                                                      // 150
    'c.c.c': true                                                                                                     // 151
  }, "multikey both paths - incl");                                                                                   // 152
                                                                                                                      // 153
  testSelProjectionComb({                                                                                             // 154
    'a.b.c.d': 123,                                                                                                   // 155
    'a.b1.c.d': 421,                                                                                                  // 156
    'a.b.c.e': 111                                                                                                    // 157
  }, {                                                                                                                // 158
    'a.b': 1                                                                                                          // 159
  }, {                                                                                                                // 160
    'a.b': true,                                                                                                      // 161
    'a.b1.c.d': true                                                                                                  // 162
  }, "shadowing one another - incl");                                                                                 // 163
                                                                                                                      // 164
  testSelProjectionComb({                                                                                             // 165
    'a.b': 123,                                                                                                       // 166
    'foo.bar': false                                                                                                  // 167
  }, {                                                                                                                // 168
    'a.b.c.d': 1,                                                                                                     // 169
    'foo': 1                                                                                                          // 170
  }, {                                                                                                                // 171
    'a.b': true,                                                                                                      // 172
    'foo': true                                                                                                       // 173
  }, "shadowing one another - incl");                                                                                 // 174
                                                                                                                      // 175
  testSelProjectionComb({                                                                                             // 176
    'a.b.c': 1                                                                                                        // 177
  }, {                                                                                                                // 178
    'a.b.c': 1                                                                                                        // 179
  }, {                                                                                                                // 180
    'a.b.c': true                                                                                                     // 181
  }, "same paths - incl");                                                                                            // 182
                                                                                                                      // 183
  testSelProjectionComb({                                                                                             // 184
    'x.4.y': 42,                                                                                                      // 185
    'z.0.1': 33                                                                                                       // 186
  }, {                                                                                                                // 187
    'x.x': 1                                                                                                          // 188
  }, {                                                                                                                // 189
    'x.x': true,                                                                                                      // 190
    'x.y': true,                                                                                                      // 191
    'z': true                                                                                                         // 192
  }, "numbered keys in selector - incl");                                                                             // 193
                                                                                                                      // 194
  testSelProjectionComb({                                                                                             // 195
    'a.b.c': 42,                                                                                                      // 196
    $where: function () { return true; }                                                                              // 197
  }, {                                                                                                                // 198
    'a.b': 1,                                                                                                         // 199
    'z.z': 1                                                                                                          // 200
  }, {}, "$where in the selector - incl");                                                                            // 201
                                                                                                                      // 202
  testSelProjectionComb({                                                                                             // 203
    $or: [                                                                                                            // 204
      {'a.b.c': 42},                                                                                                  // 205
      {$where: function () { return true; } }                                                                         // 206
    ]                                                                                                                 // 207
  }, {                                                                                                                // 208
    'a.b': 1,                                                                                                         // 209
    'z.z': 1                                                                                                          // 210
  }, {}, "$where in the selector - incl");                                                                            // 211
                                                                                                                      // 212
  // Test with exclusive projection                                                                                   // 213
  testSelProjectionComb({ a: 1, b: 2 }, { b: 0, c: 0, d: 0 }, { c: false, d: false }, "simplest excl");               // 214
  testSelProjectionComb({ $or: [{ a: 1234, e: {$lt: 5} }], b: 2 }, { b: 0, c: 0, d: 0 }, { c: false, d: false }, "simplest excl, branching");
  testSelProjectionComb({                                                                                             // 216
    'a.b': { $lt: 3 },                                                                                                // 217
    'y.0': -1,                                                                                                        // 218
    'a.c': 15                                                                                                         // 219
  }, {                                                                                                                // 220
    'd': 0,                                                                                                           // 221
    'z': 0                                                                                                            // 222
  }, {                                                                                                                // 223
    d: false,                                                                                                         // 224
    z: false                                                                                                          // 225
  }, "multikey paths in selector - excl");                                                                            // 226
                                                                                                                      // 227
  testSelProjectionComb({                                                                                             // 228
    foo: 1234,                                                                                                        // 229
    $and: [{ k: -1 }, { $or: [{ b: 15 }] }]                                                                           // 230
  }, {                                                                                                                // 231
    'foo.bar': 0,                                                                                                     // 232
    'foo.zzz': 0,                                                                                                     // 233
    'b.asdf': 0                                                                                                       // 234
  }, {                                                                                                                // 235
  }, "multikey paths in fields - excl");                                                                              // 236
                                                                                                                      // 237
  testSelProjectionComb({                                                                                             // 238
    'a.b.c': 123,                                                                                                     // 239
    'a.b.d': 321,                                                                                                     // 240
    'b.c.0': 111,                                                                                                     // 241
    'a.e': 12345                                                                                                      // 242
  }, {                                                                                                                // 243
    'a.b.z': 0,                                                                                                       // 244
    'a.b.d.g': 0,                                                                                                     // 245
    'c.c.c': 0                                                                                                        // 246
  }, {                                                                                                                // 247
    'a.b.z': false,                                                                                                   // 248
    'c.c.c': false                                                                                                    // 249
  }, "multikey both paths - excl");                                                                                   // 250
                                                                                                                      // 251
  testSelProjectionComb({                                                                                             // 252
    'a.b.c.d': 123,                                                                                                   // 253
    'a.b1.c.d': 421,                                                                                                  // 254
    'a.b.c.e': 111                                                                                                    // 255
  }, {                                                                                                                // 256
    'a.b': 0                                                                                                          // 257
  }, {                                                                                                                // 258
  }, "shadowing one another - excl");                                                                                 // 259
                                                                                                                      // 260
  testSelProjectionComb({                                                                                             // 261
    'a.b': 123,                                                                                                       // 262
    'foo.bar': false                                                                                                  // 263
  }, {                                                                                                                // 264
    'a.b.c.d': 0,                                                                                                     // 265
    'foo': 0                                                                                                          // 266
  }, {                                                                                                                // 267
  }, "shadowing one another - excl");                                                                                 // 268
                                                                                                                      // 269
  testSelProjectionComb({                                                                                             // 270
    'a.b.c': 1                                                                                                        // 271
  }, {                                                                                                                // 272
    'a.b.c': 0                                                                                                        // 273
  }, {                                                                                                                // 274
  }, "same paths - excl");                                                                                            // 275
                                                                                                                      // 276
  testSelProjectionComb({                                                                                             // 277
    'a.b': 123,                                                                                                       // 278
    'a.c.d': 222,                                                                                                     // 279
    'ddd': 123                                                                                                        // 280
  }, {                                                                                                                // 281
    'a.b': 0,                                                                                                         // 282
    'a.c.e': 0,                                                                                                       // 283
    'asdf': 0                                                                                                         // 284
  }, {                                                                                                                // 285
    'a.c.e': false,                                                                                                   // 286
    'asdf': false                                                                                                     // 287
  }, "intercept the selector path - excl");                                                                           // 288
                                                                                                                      // 289
  testSelProjectionComb({                                                                                             // 290
    'a.b.c': 14                                                                                                       // 291
  }, {                                                                                                                // 292
    'a.b.d': 0                                                                                                        // 293
  }, {                                                                                                                // 294
    'a.b.d': false                                                                                                    // 295
  }, "different branches - excl");                                                                                    // 296
                                                                                                                      // 297
  testSelProjectionComb({                                                                                             // 298
    'a.b.c.d': "124",                                                                                                 // 299
    'foo.bar.baz.que': "some value"                                                                                   // 300
  }, {                                                                                                                // 301
    'a.b.c.d.e': 0,                                                                                                   // 302
    'foo.bar': 0                                                                                                      // 303
  }, {                                                                                                                // 304
  }, "excl on incl paths - excl");                                                                                    // 305
                                                                                                                      // 306
  testSelProjectionComb({                                                                                             // 307
    'x.4.y': 42,                                                                                                      // 308
    'z.0.1': 33                                                                                                       // 309
  }, {                                                                                                                // 310
    'x.x': 0,                                                                                                         // 311
    'x.y': 0                                                                                                          // 312
  }, {                                                                                                                // 313
    'x.x': false,                                                                                                     // 314
  }, "numbered keys in selector - excl");                                                                             // 315
                                                                                                                      // 316
  testSelProjectionComb({                                                                                             // 317
    'a.b.c': 42,                                                                                                      // 318
    $where: function () { return true; }                                                                              // 319
  }, {                                                                                                                // 320
    'a.b': 0,                                                                                                         // 321
    'z.z': 0                                                                                                          // 322
  }, {}, "$where in the selector - excl");                                                                            // 323
                                                                                                                      // 324
  testSelProjectionComb({                                                                                             // 325
    $or: [                                                                                                            // 326
      {'a.b.c': 42},                                                                                                  // 327
      {$where: function () { return true; } }                                                                         // 328
    ]                                                                                                                 // 329
  }, {                                                                                                                // 330
    'a.b': 0,                                                                                                         // 331
    'z.z': 0                                                                                                          // 332
  }, {}, "$where in the selector - excl");                                                                            // 333
                                                                                                                      // 334
});                                                                                                                   // 335
                                                                                                                      // 336
Tinytest.add("minimongo - sorter and projection combination", function (test) {                                       // 337
  function testSorterProjectionComb (sortSpec, proj, expected, desc) {                                                // 338
    var sorter = new Minimongo.Sorter(sortSpec);                                                                      // 339
    test.equal(sorter.combineIntoProjection(proj), expected, desc);                                                   // 340
  }                                                                                                                   // 341
                                                                                                                      // 342
  // Test with inclusive projection                                                                                   // 343
  testSorterProjectionComb({ a: 1, b: 1 }, { b: 1, c: 1, d: 1 }, { a: true, b: true, c: true, d: true }, "simplest incl");
  testSorterProjectionComb({ a: 1, b: -1 }, { b: 1, c: 1, d: 1 }, { a: true, b: true, c: true, d: true }, "simplest incl");
  testSorterProjectionComb({ 'a.c': 1 }, { b: 1 }, { 'a.c': true, b: true }, "dot path incl");                        // 346
  testSorterProjectionComb({ 'a.1.c': 1 }, { b: 1 }, { 'a.c': true, b: true }, "dot num path incl");                  // 347
  testSorterProjectionComb({ 'a.1.c': 1 }, { b: 1, a: 1 }, { a: true, b: true }, "dot num path incl overlap");        // 348
  testSorterProjectionComb({ 'a.1.c': 1, 'a.2.b': -1 }, { b: 1 }, { 'a.c': true, 'a.b': true, b: true }, "dot num path incl");
  testSorterProjectionComb({ 'a.1.c': 1, 'a.2.b': -1 }, {}, {}, "dot num path with empty incl");                      // 350
                                                                                                                      // 351
  // Test with exclusive projection                                                                                   // 352
  testSorterProjectionComb({ a: 1, b: 1 }, { b: 0, c: 0, d: 0 }, { c: false, d: false }, "simplest excl");            // 353
  testSorterProjectionComb({ a: 1, b: -1 }, { b: 0, c: 0, d: 0 }, { c: false, d: false }, "simplest excl");           // 354
  testSorterProjectionComb({ 'a.c': 1 }, { b: 0 }, { b: false }, "dot path excl");                                    // 355
  testSorterProjectionComb({ 'a.1.c': 1 }, { b: 0 }, { b: false }, "dot num path excl");                              // 356
  testSorterProjectionComb({ 'a.1.c': 1 }, { b: 0, a: 0 }, { b: false }, "dot num path excl overlap");                // 357
  testSorterProjectionComb({ 'a.1.c': 1, 'a.2.b': -1 }, { b: 0 }, { b: false }, "dot num path excl");                 // 358
});                                                                                                                   // 359
                                                                                                                      // 360
                                                                                                                      // 361
(function () {                                                                                                        // 362
  // TODO: Tests for "can selector become true by modifier" are incomplete,                                           // 363
  // absent or test the functionality of "not ideal" implementation (test checks                                      // 364
  // that certain case always returns true as implementation is incomplete)                                           // 365
  // - tests with $and/$or/$nor/$not branches (are absent)                                                            // 366
  // - more tests with arrays fields and numeric keys (incomplete and test "not                                       // 367
  // ideal" implementation)                                                                                           // 368
  // - tests when numeric keys actually mean numeric keys, not array indexes                                          // 369
  // (are absent)                                                                                                     // 370
  // - tests with $-operators in the selector (are incomplete and test "not                                           // 371
  // ideal" implementation)                                                                                           // 372
  //  * gives up on $-operators with non-scalar values ({$ne: {x: 1}})                                                // 373
  //  * analyses $in                                                                                                  // 374
  //  * analyses $nin/$ne                                                                                             // 375
  //  * analyses $gt, $gte, $lt, $lte                                                                                 // 376
  //  * gives up on a combination of $gt/$gte/$lt/$lte and $ne/$nin                                                   // 377
  //  * doesn't support $eq properly                                                                                  // 378
                                                                                                                      // 379
  var test = null; // set this global in the beginning of every test                                                  // 380
  // T - should return true                                                                                           // 381
  // F - should return false                                                                                          // 382
  var oneTest = function (sel, mod, expected, desc) {                                                                 // 383
    var matcher = new Minimongo.Matcher(sel);                                                                         // 384
    test.equal(matcher.canBecomeTrueByModifier(mod), expected, desc);                                                 // 385
  };                                                                                                                  // 386
  function T (sel, mod, desc) {                                                                                       // 387
    oneTest(sel, mod, true, desc);                                                                                    // 388
  }                                                                                                                   // 389
  function F (sel, mod, desc) {                                                                                       // 390
    oneTest(sel, mod, false, desc);                                                                                   // 391
  }                                                                                                                   // 392
                                                                                                                      // 393
  Tinytest.add("minimongo - can selector become true by modifier - literals (structured tests)", function (t) {       // 394
    test = t;                                                                                                         // 395
                                                                                                                      // 396
    var selector = {                                                                                                  // 397
      'a.b.c': 2,                                                                                                     // 398
      'foo.bar': {                                                                                                    // 399
        z: { y: 1 }                                                                                                   // 400
      },                                                                                                              // 401
      'foo.baz': [ {ans: 42}, "string", false, undefined ],                                                           // 402
      'empty.field': null                                                                                             // 403
    };                                                                                                                // 404
                                                                                                                      // 405
    T(selector, {$set:{ 'a.b.c': 2 }});                                                                               // 406
    F(selector, {$unset:{ 'a': 1 }});                                                                                 // 407
    F(selector, {$unset:{ 'a.b': 1 }});                                                                               // 408
    F(selector, {$unset:{ 'a.b.c': 1 }});                                                                             // 409
    T(selector, {$set:{ 'a.b': { c: 2 } }});                                                                          // 410
    F(selector, {$set:{ 'a.b': {} }});                                                                                // 411
    T(selector, {$set:{ 'a.b': { c: 2, x: 5 } }});                                                                    // 412
    F(selector, {$set:{ 'a.b.c.k': 3 }});                                                                             // 413
    F(selector, {$set:{ 'a.b.c.k': {} }});                                                                            // 414
                                                                                                                      // 415
    F(selector, {$unset:{ 'foo': 1 }});                                                                               // 416
    F(selector, {$unset:{ 'foo.bar': 1 }});                                                                           // 417
    F(selector, {$unset:{ 'foo.bar.z': 1 }});                                                                         // 418
    F(selector, {$unset:{ 'foo.bar.z.y': 1 }});                                                                       // 419
    F(selector, {$set:{ 'foo.bar.x': 1 }});                                                                           // 420
    F(selector, {$set:{ 'foo.bar': {} }});                                                                            // 421
    F(selector, {$set:{ 'foo.bar': 3 }});                                                                             // 422
    T(selector, {$set:{ 'foo.bar': { z: { y: 1 } } }});                                                               // 423
    T(selector, {$set:{ 'foo.bar.z': { y: 1 } }});                                                                    // 424
    T(selector, {$set:{ 'foo.bar.z.y': 1 }});                                                                         // 425
                                                                                                                      // 426
    F(selector, {$set:{ 'empty.field': {} }});                                                                        // 427
    T(selector, {$set:{ 'empty': {} }});                                                                              // 428
    T(selector, {$set:{ 'empty.field': null }});                                                                      // 429
    T(selector, {$set:{ 'empty.field': undefined }});                                                                 // 430
    F(selector, {$set:{ 'empty.field.a': 3 }});                                                                       // 431
  });                                                                                                                 // 432
                                                                                                                      // 433
  Tinytest.add("minimongo - can selector become true by modifier - literals (adhoc tests)", function (t) {            // 434
    test = t;                                                                                                         // 435
    T({x:1}, {$set:{x:1}}, "simple set scalar");                                                                      // 436
    T({x:"a"}, {$set:{x:"a"}}, "simple set scalar");                                                                  // 437
    T({x:false}, {$set:{x:false}}, "simple set scalar");                                                              // 438
    F({x:true}, {$set:{x:false}}, "simple set scalar");                                                               // 439
    F({x:2}, {$set:{x:3}}, "simple set scalar");                                                                      // 440
                                                                                                                      // 441
    F({'foo.bar.baz': 1, x:1}, {$unset:{'foo.bar.baz': 1}, $set:{x:1}}, "simple unset of the interesting path");      // 442
    F({'foo.bar.baz': 1, x:1}, {$unset:{'foo.bar': 1}, $set:{x:1}}, "simple unset of the interesting path prefix");   // 443
    F({'foo.bar.baz': 1, x:1}, {$unset:{'foo': 1}, $set:{x:1}}, "simple unset of the interesting path prefix");       // 444
    F({'foo.bar.baz': 1}, {$unset:{'foo.baz': 1}}, "simple unset of the interesting path prefix");                    // 445
    F({'foo.bar.baz': 1}, {$unset:{'foo.bar.bar': 1}}, "simple unset of the interesting path prefix");                // 446
  });                                                                                                                 // 447
                                                                                                                      // 448
  Tinytest.add("minimongo - can selector become true by modifier - regexps", function (t) {                           // 449
    test = t;                                                                                                         // 450
                                                                                                                      // 451
    // Regexp                                                                                                         // 452
    T({ 'foo.bar': /^[0-9]+$/i }, { $set: {'foo.bar': '01233'} }, "set of regexp");                                   // 453
    // XXX this test should be False, should be fixed within improved implementation                                  // 454
    T({ 'foo.bar': /^[0-9]+$/i, x: 1 }, { $set: {'foo.bar': '0a1233', x: 1} }, "set of regexp");                      // 455
    // XXX this test should be False, should be fixed within improved implementation                                  // 456
    T({ 'foo.bar': /^[0-9]+$/i, x: 1 }, { $unset: {'foo.bar': 1}, $set: { x: 1 } }, "unset of regexp");               // 457
    T({ 'foo.bar': /^[0-9]+$/i, x: 1 }, { $set: { x: 1 } }, "don't touch regexp");                                    // 458
  });                                                                                                                 // 459
                                                                                                                      // 460
  Tinytest.add("minimongo - can selector become true by modifier - undefined/null", function (t) {                    // 461
    test = t;                                                                                                         // 462
    // Nulls / Undefined                                                                                              // 463
    T({ 'foo.bar': null }, {$set:{'foo.bar': null}}, "set of null looking for null");                                 // 464
    T({ 'foo.bar': null }, {$set:{'foo.bar': undefined}}, "set of undefined looking for null");                       // 465
    T({ 'foo.bar': undefined }, {$set:{'foo.bar': null}}, "set of null looking for undefined");                       // 466
    T({ 'foo.bar': undefined }, {$set:{'foo.bar': undefined}}, "set of undefined looking for undefined");             // 467
    T({ 'foo.bar': null }, {$set:{'foo': null}}, "set of null of parent path looking for null");                      // 468
    F({ 'foo.bar': null }, {$set:{'foo.bar.baz': null}}, "set of null of different path looking for null");           // 469
    T({ 'foo.bar': null }, { $unset: { 'foo': 1 } }, "unset the parent");                                             // 470
    T({ 'foo.bar': null }, { $unset: { 'foo.bar': 1 } }, "unset tracked path");                                       // 471
    T({ 'foo.bar': null }, { $set: { 'foo': 3 } }, "set the parent");                                                 // 472
    T({ 'foo.bar': null }, { $set: { 'foo': {baz:1} } }, "set the parent");                                           // 473
                                                                                                                      // 474
  });                                                                                                                 // 475
                                                                                                                      // 476
  Tinytest.add("minimongo - can selector become true by modifier - literals with arrays", function (t) {              // 477
    test = t;                                                                                                         // 478
    // These tests are incomplete and in theory they all should return true as we                                     // 479
    // don't support any case with numeric fields yet.                                                                // 480
    T({'a.1.b': 1, x:1}, {$unset:{'a.1.b': 1}, $set:{x:1}}, "unset of array element's field with exactly the same index as selector");
    F({'a.2.b': 1}, {$unset:{'a.1.b': 1}}, "unset of array element's field with different index as selector");        // 482
    // This is false, because if you are looking for array but in reality it is an                                    // 483
    // object, it just can't get to true.                                                                             // 484
    F({'a.2.b': 1}, {$unset:{'a.b': 1}}, "unset of field while selector is looking for index");                       // 485
    T({ 'foo.bar': null }, {$set:{'foo.1.bar': null}}, "set array's element's field to null looking for null");       // 486
    T({ 'foo.bar': null }, {$set:{'foo.0.bar': 1, 'foo.1.bar': null}}, "set array's element's field to null looking for null");
    // This is false, because there may remain other array elements that match                                        // 488
    // but we modified this test as we don't support this case yet                                                    // 489
    T({'a.b': 1}, {$unset:{'a.1.b': 1}}, "unset of array element's field");                                           // 490
  });                                                                                                                 // 491
                                                                                                                      // 492
  Tinytest.add("minimongo - can selector become true by modifier - set an object literal whose fields are selected", function (t) {
    test = t;                                                                                                         // 494
    T({ 'a.b.c': 1 }, { $set: { 'a.b': { c: 1 } } }, "a simple scalar selector and simple set");                      // 495
    F({ 'a.b.c': 1 }, { $set: { 'a.b': { c: 2 } } }, "a simple scalar selector and simple set to false");             // 496
    F({ 'a.b.c': 1 }, { $set: { 'a.b': { d: 1 } } }, "a simple scalar selector and simple set a wrong literal");      // 497
    F({ 'a.b.c': 1 }, { $set: { 'a.b': 222 } }, "a simple scalar selector and simple set a wrong type");              // 498
  });                                                                                                                 // 499
                                                                                                                      // 500
  Tinytest.add("minimongo - can selector become true by modifier - $-scalar selectors and simple tests", function (t) {
    test = t;                                                                                                         // 502
    T({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b': { c: 4 } } }, "nested $lt");                                          // 503
    F({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b': { c: 5 } } }, "nested $lt");                                          // 504
    F({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b': { c: 6 } } }, "nested $lt");                                          // 505
    F({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b.d': 7 } }, "nested $lt, the change doesn't matter");                    // 506
    F({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b': { d: 7 } } }, "nested $lt, the key disappears");                      // 507
    T({ 'a.b.c': { $lt: 5 } }, { $set: { 'a.b': { d: 7, c: -1 } } }, "nested $lt");                                   // 508
    F({ a: { $lt: 10, $gt: 3 } }, { $unset: { a: 1 } }, "unset $lt");                                                 // 509
    T({ a: { $lt: 10, $gt: 3 } }, { $set: { a: 4 } }, "set between x and y");                                         // 510
    F({ a: { $lt: 10, $gt: 3 } }, { $set: { a: 3 } }, "set between x and y");                                         // 511
    F({ a: { $lt: 10, $gt: 3 } }, { $set: { a: 10 } }, "set between x and y");                                        // 512
    F({ a: { $gt: 10, $lt: 3 } }, { $set: { a: 9 } }, "impossible statement");                                        // 513
    T({ a: { $lte: 10, $gte: 3 } }, { $set: { a: 3 } }, "set between x and y");                                       // 514
    T({ a: { $lte: 10, $gte: 3 } }, { $set: { a: 10 } }, "set between x and y");                                      // 515
    F({ a: { $lte: 10, $gte: 3 } }, { $set: { a: -10 } }, "set between x and y");                                     // 516
    T({ a: { $lte: 10, $gte: 3, $gt: 3, $lt: 10 } }, { $set: { a: 4 } }, "set between x and y");                      // 517
    F({ a: { $lte: 10, $gte: 3, $gt: 3, $lt: 10 } }, { $set: { a: 3 } }, "set between x and y");                      // 518
    F({ a: { $lte: 10, $gte: 3, $gt: 3, $lt: 10 } }, { $set: { a: 10 } }, "set between x and y");                     // 519
    F({ a: { $lte: 10, $gte: 3, $gt: 3, $lt: 10 } }, { $set: { a: Infinity } }, "set between x and y");               // 520
    T({ a: { $lte: 10, $gte: 3, $gt: 3, $lt: 10 }, x: 1 }, { $set: { x: 1 } }, "set between x and y - dummy");        // 521
    F({ a: { $lte: 10, $gte: 13, $gt: 3, $lt: 9 }, x: 1 }, { $set: { x: 1 } }, "set between x and y - dummy - impossible");
    F({ a: { $lte: 10 } }, { $set: { a: Infinity } }, "Infinity <= 10?");                                             // 523
    T({ a: { $lte: 10 } }, { $set: { a: -Infinity } }, "-Infinity <= 10?");                                           // 524
    // XXX is this sufficient?                                                                                        // 525
    T({ a: { $gt: 9.99999999999999, $lt: 10 }, x: 1 }, { $set: { x: 1 } }, "very close $gt and $lt");                 // 526
    // XXX this test should be F, but since it is so hard to be precise in                                            // 527
    // floating point math, the current implementation falls back to T                                                // 528
    T({ a: { $gt: 9.999999999999999, $lt: 10 }, x: 1 }, { $set: { x: 1 } }, "very close $gt and $lt");                // 529
    T({ a: { $ne: 5 } }, { $unset: { a: 1 } }, "unset of $ne");                                                       // 530
    T({ a: { $ne: 5 } }, { $set: { a: 1 } }, "set of $ne");                                                           // 531
    T({ a: { $ne: "some string" }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                          // 532
    T({ a: { $ne: true }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                                   // 533
    T({ a: { $ne: false }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                                  // 534
    T({ a: { $ne: null }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                                   // 535
    T({ a: { $ne: Infinity }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                               // 536
    T({ a: { $ne: 5 } }, { $set: { a: -10 } }, "set of $ne");                                                         // 537
    T({ a: { $in: [1, 3, 5, 7] } }, { $set: { a: 5 } }, "$in checks");                                                // 538
    F({ a: { $in: [1, 3, 5, 7] } }, { $set: { a: -5 } }, "$in checks");                                               // 539
    T({ a: { $in: [1, 3, 5, 7], $gt: 6 }, x: 1 }, { $set: { x: 1 } }, "$in combination with $gt");                    // 540
    F({ a: { $lte: 10, $gte: 3 } }, { $set: { 'a.b': -10 } }, "sel between x and y, set its subfield");               // 541
    F({ b: { $in: [1, 3, 5, 7] } }, { $set: { 'b.c': 2 } }, "sel $in, set subfield");                                 // 542
    T({ b: { $in: [1, 3, 5, 7] } }, { $set: { 'bd.c': 2, b: 3 } }, "sel $in, set similar subfield");                  // 543
    F({ 'b.c': { $in: [1, 3, 5, 7] } }, { $set: { b: 2 } }, "sel subfield of set scalar");                            // 544
    // If modifier tries to set a sub-field of a path expected to be a scalar.                                        // 545
    F({ 'a.b': { $gt: 5, $lt: 7}, x: 1 }, { $set: { 'a.b.c': 3, x: 1 } }, "set sub-field of $gt,$lt operator (scalar expected)");
    F({ 'a.b': { $gt: 5, $lt: 7}, x: 1 }, { $set: { x: 1 }, $unset: { 'a.b.c': 1 } }, "unset sub-field of $gt,$lt operator (scalar expected)");
  });                                                                                                                 // 548
                                                                                                                      // 549
  Tinytest.add("minimongo - can selector become true by modifier - $-nonscalar selectors and simple tests", function (t) {
    test = t;                                                                                                         // 551
    T({ a: { $ne: { x: 5 } } }, { $set: { 'a.x': 3 } }, "set of $ne");                                                // 552
    // XXX this test should be F, but it is not implemented yet                                                       // 553
    T({ a: { $ne: { x: 5 } } }, { $set: { 'a.x': 5 } }, "set of $ne");                                                // 554
    T({ a: { $in: [{ b: 1 }, { b: 3 }] } }, { $set: { a: { b: 3 } } }, "$in checks");                                 // 555
    // XXX this test should be F, but it is not implemented yet                                                       // 556
    T({ a: { $in: [{ b: 1 }, { b: 3 }] } }, { $set: { a: { v: 3 } } }, "$in checks");                                 // 557
    T({ a: { $ne: { a: 2 } }, x: 1 }, { $set: { x: 1 } }, "$ne dummy");                                               // 558
    // XXX this test should be F, but it is not implemented yet                                                       // 559
    T({ a: { $ne: { a: 2 } } }, { $set: { a: { a: 2 } } }, "$ne object");                                             // 560
  });                                                                                                                 // 561
})();                                                                                                                 // 562
                                                                                                                      // 563
                                                                                                                      // 564
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
