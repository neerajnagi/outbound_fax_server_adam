(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/minimongo/minimongo_tests.js                                                                    //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
// Hack to make LocalCollection generate ObjectIDs by default.                                              // 2
LocalCollection._useOID = true;                                                                             // 3
                                                                                                            // 4
// assert that f is a strcmp-style comparison function that puts                                            // 5
// 'values' in the provided order                                                                           // 6
                                                                                                            // 7
var assert_ordering = function (test, f, values) {                                                          // 8
  for (var i = 0; i < values.length; i++) {                                                                 // 9
    var x = f(values[i], values[i]);                                                                        // 10
    if (x !== 0) {                                                                                          // 11
      // XXX super janky                                                                                    // 12
      test.fail({type: "minimongo-ordering",                                                                // 13
                 message: "value doesn't order as equal to itself",                                         // 14
                 value: JSON.stringify(values[i]),                                                          // 15
                 should_be_zero_but_got: JSON.stringify(x)});                                               // 16
    }                                                                                                       // 17
    if (i + 1 < values.length) {                                                                            // 18
      var less = values[i];                                                                                 // 19
      var more = values[i + 1];                                                                             // 20
      var x = f(less, more);                                                                                // 21
      if (!(x < 0)) {                                                                                       // 22
        // XXX super janky                                                                                  // 23
        test.fail({type: "minimongo-ordering",                                                              // 24
                   message: "ordering test failed",                                                         // 25
                   first: JSON.stringify(less),                                                             // 26
                   second: JSON.stringify(more),                                                            // 27
                   should_be_negative_but_got: JSON.stringify(x)});                                         // 28
      }                                                                                                     // 29
      x = f(more, less);                                                                                    // 30
      if (!(x > 0)) {                                                                                       // 31
        // XXX super janky                                                                                  // 32
        test.fail({type: "minimongo-ordering",                                                              // 33
                   message: "ordering test failed",                                                         // 34
                   first: JSON.stringify(less),                                                             // 35
                   second: JSON.stringify(more),                                                            // 36
                   should_be_positive_but_got: JSON.stringify(x)});                                         // 37
      }                                                                                                     // 38
    }                                                                                                       // 39
  }                                                                                                         // 40
};                                                                                                          // 41
                                                                                                            // 42
var log_callbacks = function (operations) {                                                                 // 43
  return {                                                                                                  // 44
    addedAt: function (obj, idx, before) {                                                                  // 45
      delete obj._id;                                                                                       // 46
      operations.push(EJSON.clone(['added', obj, idx, before]));                                            // 47
    },                                                                                                      // 48
    changedAt: function (obj, old_obj, at) {                                                                // 49
      delete obj._id;                                                                                       // 50
      delete old_obj._id;                                                                                   // 51
      operations.push(EJSON.clone(['changed', obj, at, old_obj]));                                          // 52
    },                                                                                                      // 53
    movedTo: function (obj, old_at, new_at, before) {                                                       // 54
      delete obj._id;                                                                                       // 55
      operations.push(EJSON.clone(['moved', obj, old_at, new_at, before]));                                 // 56
    },                                                                                                      // 57
    removedAt: function (old_obj, at) {                                                                     // 58
      var id = old_obj._id;                                                                                 // 59
      delete old_obj._id;                                                                                   // 60
      operations.push(EJSON.clone(['removed', id, at, old_obj]));                                           // 61
    }                                                                                                       // 62
  };                                                                                                        // 63
};                                                                                                          // 64
                                                                                                            // 65
// XXX test shared structure in all MM entrypoints                                                          // 66
Tinytest.add("minimongo - basics", function (test) {                                                        // 67
  var c = new LocalCollection(),                                                                            // 68
      fluffyKitten_id,                                                                                      // 69
      count;                                                                                                // 70
                                                                                                            // 71
  fluffyKitten_id = c.insert({type: "kitten", name: "fluffy"});                                             // 72
  c.insert({type: "kitten", name: "snookums"});                                                             // 73
  c.insert({type: "cryptographer", name: "alice"});                                                         // 74
  c.insert({type: "cryptographer", name: "bob"});                                                           // 75
  c.insert({type: "cryptographer", name: "cara"});                                                          // 76
  test.equal(c.find().count(), 5);                                                                          // 77
  test.equal(c.find({type: "kitten"}).count(), 2);                                                          // 78
  test.equal(c.find({type: "cryptographer"}).count(), 3);                                                   // 79
  test.length(c.find({type: "kitten"}).fetch(), 2);                                                         // 80
  test.length(c.find({type: "cryptographer"}).fetch(), 3);                                                  // 81
  test.equal(fluffyKitten_id, c.findOne({type: "kitten", name: "fluffy"})._id);                             // 82
                                                                                                            // 83
  c.remove({name: "cara"});                                                                                 // 84
  test.equal(c.find().count(), 4);                                                                          // 85
  test.equal(c.find({type: "kitten"}).count(), 2);                                                          // 86
  test.equal(c.find({type: "cryptographer"}).count(), 2);                                                   // 87
  test.length(c.find({type: "kitten"}).fetch(), 2);                                                         // 88
  test.length(c.find({type: "cryptographer"}).fetch(), 2);                                                  // 89
                                                                                                            // 90
  count = c.update({name: "snookums"}, {$set: {type: "cryptographer"}});                                    // 91
  test.equal(count, 1);                                                                                     // 92
  test.equal(c.find().count(), 4);                                                                          // 93
  test.equal(c.find({type: "kitten"}).count(), 1);                                                          // 94
  test.equal(c.find({type: "cryptographer"}).count(), 3);                                                   // 95
  test.length(c.find({type: "kitten"}).fetch(), 1);                                                         // 96
  test.length(c.find({type: "cryptographer"}).fetch(), 3);                                                  // 97
                                                                                                            // 98
  c.remove(null);                                                                                           // 99
  c.remove(false);                                                                                          // 100
  c.remove(undefined);                                                                                      // 101
  test.equal(c.find().count(), 4);                                                                          // 102
                                                                                                            // 103
  c.remove({_id: null});                                                                                    // 104
  c.remove({_id: false});                                                                                   // 105
  c.remove({_id: undefined});                                                                               // 106
  count = c.remove();                                                                                       // 107
  test.equal(count, 0);                                                                                     // 108
  test.equal(c.find().count(), 4);                                                                          // 109
                                                                                                            // 110
  count = c.remove({});                                                                                     // 111
  test.equal(count, 4);                                                                                     // 112
  test.equal(c.find().count(), 0);                                                                          // 113
                                                                                                            // 114
  c.insert({_id: 1, name: "strawberry", tags: ["fruit", "red", "squishy"]});                                // 115
  c.insert({_id: 2, name: "apple", tags: ["fruit", "red", "hard"]});                                        // 116
  c.insert({_id: 3, name: "rose", tags: ["flower", "red", "squishy"]});                                     // 117
                                                                                                            // 118
  test.equal(c.find({tags: "flower"}).count(), 1);                                                          // 119
  test.equal(c.find({tags: "fruit"}).count(), 2);                                                           // 120
  test.equal(c.find({tags: "red"}).count(), 3);                                                             // 121
  test.length(c.find({tags: "flower"}).fetch(), 1);                                                         // 122
  test.length(c.find({tags: "fruit"}).fetch(), 2);                                                          // 123
  test.length(c.find({tags: "red"}).fetch(), 3);                                                            // 124
                                                                                                            // 125
  test.equal(c.findOne(1).name, "strawberry");                                                              // 126
  test.equal(c.findOne(2).name, "apple");                                                                   // 127
  test.equal(c.findOne(3).name, "rose");                                                                    // 128
  test.equal(c.findOne(4), undefined);                                                                      // 129
  test.equal(c.findOne("abc"), undefined);                                                                  // 130
  test.equal(c.findOne(undefined), undefined);                                                              // 131
                                                                                                            // 132
  test.equal(c.find(1).count(), 1);                                                                         // 133
  test.equal(c.find(4).count(), 0);                                                                         // 134
  test.equal(c.find("abc").count(), 0);                                                                     // 135
  test.equal(c.find(undefined).count(), 0);                                                                 // 136
  test.equal(c.find().count(), 3);                                                                          // 137
  test.equal(c.find(1, {skip: 1}).count(), 0);                                                              // 138
  test.equal(c.find({_id: 1}, {skip: 1}).count(), 0);                                                       // 139
  test.equal(c.find({}, {skip: 1}).count(), 2);                                                             // 140
  test.equal(c.find({}, {skip: 2}).count(), 1);                                                             // 141
  test.equal(c.find({}, {limit: 2}).count(), 2);                                                            // 142
  test.equal(c.find({}, {limit: 1}).count(), 1);                                                            // 143
  test.equal(c.find({}, {skip: 1, limit: 1}).count(), 1);                                                   // 144
  test.equal(c.find({tags: "fruit"}, {skip: 1}).count(), 1);                                                // 145
  test.equal(c.find({tags: "fruit"}, {limit: 1}).count(), 1);                                               // 146
  test.equal(c.find({tags: "fruit"}, {skip: 1, limit: 1}).count(), 1);                                      // 147
  test.equal(c.find(1, {sort: ['_id','desc'], skip: 1}).count(), 0);                                        // 148
  test.equal(c.find({_id: 1}, {sort: ['_id','desc'], skip: 1}).count(), 0);                                 // 149
  test.equal(c.find({}, {sort: ['_id','desc'], skip: 1}).count(), 2);                                       // 150
  test.equal(c.find({}, {sort: ['_id','desc'], skip: 2}).count(), 1);                                       // 151
  test.equal(c.find({}, {sort: ['_id','desc'], limit: 2}).count(), 2);                                      // 152
  test.equal(c.find({}, {sort: ['_id','desc'], limit: 1}).count(), 1);                                      // 153
  test.equal(c.find({}, {sort: ['_id','desc'], skip: 1, limit: 1}).count(), 1);                             // 154
  test.equal(c.find({tags: "fruit"}, {sort: ['_id','desc'], skip: 1}).count(), 1);                          // 155
  test.equal(c.find({tags: "fruit"}, {sort: ['_id','desc'], limit: 1}).count(), 1);                         // 156
  test.equal(c.find({tags: "fruit"}, {sort: ['_id','desc'], skip: 1, limit: 1}).count(), 1);                // 157
                                                                                                            // 158
  // Regression test for #455.                                                                              // 159
  c.insert({foo: {bar: 'baz'}});                                                                            // 160
  test.equal(c.find({foo: {bam: 'baz'}}).count(), 0);                                                       // 161
  test.equal(c.find({foo: {bar: 'baz'}}).count(), 1);                                                       // 162
                                                                                                            // 163
});                                                                                                         // 164
                                                                                                            // 165
Tinytest.add("minimongo - cursors", function (test) {                                                       // 166
  var c = new LocalCollection();                                                                            // 167
  var res;                                                                                                  // 168
                                                                                                            // 169
  for (var i = 0; i < 20; i++)                                                                              // 170
    c.insert({i: i});                                                                                       // 171
                                                                                                            // 172
  var q = c.find();                                                                                         // 173
  test.equal(q.count(), 20);                                                                                // 174
                                                                                                            // 175
  // fetch                                                                                                  // 176
  res = q.fetch();                                                                                          // 177
  test.length(res, 20);                                                                                     // 178
  for (var i = 0; i < 20; i++)                                                                              // 179
    test.equal(res[i].i, i);                                                                                // 180
  // everything empty                                                                                       // 181
  test.length(q.fetch(), 0);                                                                                // 182
  q.rewind();                                                                                               // 183
                                                                                                            // 184
  // forEach                                                                                                // 185
  var count = 0;                                                                                            // 186
  var context = {};                                                                                         // 187
  q.forEach(function (obj, i, cursor) {                                                                     // 188
    test.equal(obj.i, count++);                                                                             // 189
    test.equal(obj.i, i);                                                                                   // 190
    test.isTrue(context === this);                                                                          // 191
    test.isTrue(cursor === q);                                                                              // 192
  }, context);                                                                                              // 193
  test.equal(count, 20);                                                                                    // 194
  // everything empty                                                                                       // 195
  test.length(q.fetch(), 0);                                                                                // 196
  q.rewind();                                                                                               // 197
                                                                                                            // 198
  // map                                                                                                    // 199
  res = q.map(function (obj, i, cursor) {                                                                   // 200
    test.equal(obj.i, i);                                                                                   // 201
    test.isTrue(context === this);                                                                          // 202
    test.isTrue(cursor === q);                                                                              // 203
    return obj.i * 2;                                                                                       // 204
  }, context);                                                                                              // 205
  test.length(res, 20);                                                                                     // 206
  for (var i = 0; i < 20; i++)                                                                              // 207
    test.equal(res[i], i * 2);                                                                              // 208
  // everything empty                                                                                       // 209
  test.length(q.fetch(), 0);                                                                                // 210
                                                                                                            // 211
  // findOne (and no rewind first)                                                                          // 212
  test.equal(c.findOne({i: 0}).i, 0);                                                                       // 213
  test.equal(c.findOne({i: 1}).i, 1);                                                                       // 214
  var id = c.findOne({i: 2})._id;                                                                           // 215
  test.equal(c.findOne(id).i, 2);                                                                           // 216
});                                                                                                         // 217
                                                                                                            // 218
Tinytest.add("minimongo - transform", function (test) {                                                     // 219
  var c = new LocalCollection;                                                                              // 220
  c.insert({});                                                                                             // 221
  // transform functions must return objects                                                                // 222
  var invalidTransform = function (doc) { return doc._id; };                                                // 223
  test.throws(function () {                                                                                 // 224
    c.findOne({}, {transform: invalidTransform});                                                           // 225
  });                                                                                                       // 226
                                                                                                            // 227
  // transformed documents get _id field transplanted if not present                                        // 228
  var transformWithoutId = function (doc) { return _.omit(doc, '_id'); };                                   // 229
  test.equal(c.findOne({}, {transform: transformWithoutId})._id,                                            // 230
             c.findOne()._id);                                                                              // 231
});                                                                                                         // 232
                                                                                                            // 233
Tinytest.add("minimongo - misc", function (test) {                                                          // 234
  // deepcopy                                                                                               // 235
  var a = {a: [1, 2, 3], b: "x", c: true, d: {x: 12, y: [12]},                                              // 236
           f: null, g: new Date()};                                                                         // 237
  var b = EJSON.clone(a);                                                                                   // 238
  test.equal(a, b);                                                                                         // 239
  test.isTrue(LocalCollection._f._equal(a, b));                                                             // 240
  a.a.push(4);                                                                                              // 241
  test.length(b.a, 3);                                                                                      // 242
  a.c = false;                                                                                              // 243
  test.isTrue(b.c);                                                                                         // 244
  b.d.z = 15;                                                                                               // 245
  a.d.z = 14;                                                                                               // 246
  test.equal(b.d.z, 15);                                                                                    // 247
  a.d.y.push(88);                                                                                           // 248
  test.length(b.d.y, 1);                                                                                    // 249
  test.equal(a.g, b.g);                                                                                     // 250
  b.g.setDate(b.g.getDate() + 1);                                                                           // 251
  test.notEqual(a.g, b.g);                                                                                  // 252
                                                                                                            // 253
  a = {x: function () {}};                                                                                  // 254
  b = EJSON.clone(a);                                                                                       // 255
  a.x.a = 14;                                                                                               // 256
  test.equal(b.x.a, 14); // just to document current behavior                                               // 257
});                                                                                                         // 258
                                                                                                            // 259
Tinytest.add("minimongo - lookup", function (test) {                                                        // 260
  var lookupA = MinimongoTest.makeLookupFunction('a');                                                      // 261
  test.equal(lookupA({}), [{value: undefined}]);                                                            // 262
  test.equal(lookupA({a: 1}), [{value: 1}]);                                                                // 263
  test.equal(lookupA({a: [1]}), [{value: [1]}]);                                                            // 264
                                                                                                            // 265
  var lookupAX = MinimongoTest.makeLookupFunction('a.x');                                                   // 266
  test.equal(lookupAX({a: {x: 1}}), [{value: 1}]);                                                          // 267
  test.equal(lookupAX({a: {x: [1]}}), [{value: [1]}]);                                                      // 268
  test.equal(lookupAX({a: 5}), [{value: undefined}]);                                                       // 269
  test.equal(lookupAX({a: [{x: 1}, {x: [2]}, {y: 3}]}),                                                     // 270
             [{value: 1, arrayIndices: [0]},                                                                // 271
              {value: [2], arrayIndices: [1]},                                                              // 272
              {value: undefined, arrayIndices: [2]}]);                                                      // 273
                                                                                                            // 274
  var lookupA0X = MinimongoTest.makeLookupFunction('a.0.x');                                                // 275
  test.equal(lookupA0X({a: [{x: 1}]}), [                                                                    // 276
    // From interpreting '0' as "0th array element".                                                        // 277
    {value: 1, arrayIndices: [0, 'x']},                                                                     // 278
    // From interpreting '0' as "after branching in the array, look in the                                  // 279
    // object {x:1} for a field named 0".                                                                   // 280
    {value: undefined, arrayIndices: [0]}]);                                                                // 281
  test.equal(lookupA0X({a: [{x: [1]}]}), [                                                                  // 282
    {value: [1], arrayIndices: [0, 'x']},                                                                   // 283
    {value: undefined, arrayIndices: [0]}]);                                                                // 284
  test.equal(lookupA0X({a: 5}), [{value: undefined}]);                                                      // 285
  test.equal(lookupA0X({a: [{x: 1}, {x: [2]}, {y: 3}]}), [                                                  // 286
    // From interpreting '0' as "0th array element".                                                        // 287
    {value: 1, arrayIndices: [0, 'x']},                                                                     // 288
    // From interpreting '0' as "after branching in the array, look in the                                  // 289
    // object {x:1} for a field named 0".                                                                   // 290
    {value: undefined, arrayIndices: [0]},                                                                  // 291
    {value: undefined, arrayIndices: [1]},                                                                  // 292
    {value: undefined, arrayIndices: [2]}                                                                   // 293
  ]);                                                                                                       // 294
                                                                                                            // 295
  test.equal(                                                                                               // 296
    MinimongoTest.makeLookupFunction('w.x.0.z')({                                                           // 297
      w: [{x: [{z: 5}]}]}), [                                                                               // 298
        // From interpreting '0' as "0th array element".                                                    // 299
        {value: 5, arrayIndices: [0, 0, 'x']},                                                              // 300
        // From interpreting '0' as "after branching in the array, look in the                              // 301
        // object {z:5} for a field named "0".                                                              // 302
        {value: undefined, arrayIndices: [0, 0]}                                                            // 303
      ]);                                                                                                   // 304
});                                                                                                         // 305
                                                                                                            // 306
Tinytest.add("minimongo - selector_compiler", function (test) {                                             // 307
  var matches = function (shouldMatch, selector, doc) {                                                     // 308
    var doesMatch = new Minimongo.Matcher(selector).documentMatches(doc).result;                            // 309
    if (doesMatch != shouldMatch) {                                                                         // 310
      // XXX super janky                                                                                    // 311
      test.fail({message: "minimongo match failure: document " +                                            // 312
                 (shouldMatch ? "should match, but doesn't" :                                               // 313
                  "shouldn't match, but does"),                                                             // 314
                 selector: JSON.stringify(selector),                                                        // 315
                 document: JSON.stringify(doc)                                                              // 316
                });                                                                                         // 317
    }                                                                                                       // 318
  };                                                                                                        // 319
                                                                                                            // 320
  var match = _.bind(matches, null, true);                                                                  // 321
  var nomatch = _.bind(matches, null, false);                                                               // 322
                                                                                                            // 323
  // XXX blog post about what I learned while writing these tests (weird                                    // 324
  // mongo edge cases)                                                                                      // 325
                                                                                                            // 326
  // empty selectors                                                                                        // 327
  match({}, {});                                                                                            // 328
  match({}, {a: 12});                                                                                       // 329
                                                                                                            // 330
  // scalars                                                                                                // 331
  match(1, {_id: 1, a: 'foo'});                                                                             // 332
  nomatch(1, {_id: 2, a: 'foo'});                                                                           // 333
  match('a', {_id: 'a', a: 'foo'});                                                                         // 334
  nomatch('a', {_id: 'b', a: 'foo'});                                                                       // 335
                                                                                                            // 336
  // safety                                                                                                 // 337
  nomatch(undefined, {});                                                                                   // 338
  nomatch(undefined, {_id: 'foo'});                                                                         // 339
  nomatch(false, {_id: 'foo'});                                                                             // 340
  nomatch(null, {_id: 'foo'});                                                                              // 341
  nomatch({_id: undefined}, {_id: 'foo'});                                                                  // 342
  nomatch({_id: false}, {_id: 'foo'});                                                                      // 343
  nomatch({_id: null}, {_id: 'foo'});                                                                       // 344
                                                                                                            // 345
  // matching one or more keys                                                                              // 346
  nomatch({a: 12}, {});                                                                                     // 347
  match({a: 12}, {a: 12});                                                                                  // 348
  match({a: 12}, {a: 12, b: 13});                                                                           // 349
  match({a: 12, b: 13}, {a: 12, b: 13});                                                                    // 350
  match({a: 12, b: 13}, {a: 12, b: 13, c: 14});                                                             // 351
  nomatch({a: 12, b: 13, c: 14}, {a: 12, b: 13});                                                           // 352
  nomatch({a: 12, b: 13}, {b: 13, c: 14});                                                                  // 353
                                                                                                            // 354
  match({a: 12}, {a: [12]});                                                                                // 355
  match({a: 12}, {a: [11, 12, 13]});                                                                        // 356
  nomatch({a: 12}, {a: [11, 13]});                                                                          // 357
  match({a: 12, b: 13}, {a: [11, 12, 13], b: [13, 14, 15]});                                                // 358
  nomatch({a: 12, b: 13}, {a: [11, 12, 13], b: [14, 15]});                                                  // 359
                                                                                                            // 360
  // dates                                                                                                  // 361
  var date1 = new Date;                                                                                     // 362
  var date2 = new Date(date1.getTime() + 1000);                                                             // 363
  match({a: date1}, {a: date1});                                                                            // 364
  nomatch({a: date1}, {a: date2});                                                                          // 365
                                                                                                            // 366
                                                                                                            // 367
  // arrays                                                                                                 // 368
  match({a: [1,2]}, {a: [1, 2]});                                                                           // 369
  match({a: [1,2]}, {a: [[1, 2]]});                                                                         // 370
  match({a: [1,2]}, {a: [[3, 4], [1, 2]]});                                                                 // 371
  nomatch({a: [1,2]}, {a: [3, 4]});                                                                         // 372
  nomatch({a: [1,2]}, {a: [[[1, 2]]]});                                                                     // 373
                                                                                                            // 374
  // literal documents                                                                                      // 375
  match({a: {b: 12}}, {a: {b: 12}});                                                                        // 376
  nomatch({a: {b: 12, c: 13}}, {a: {b: 12}});                                                               // 377
  nomatch({a: {b: 12}}, {a: {b: 12, c: 13}});                                                               // 378
  match({a: {b: 12, c: 13}}, {a: {b: 12, c: 13}});                                                          // 379
  nomatch({a: {b: 12, c: 13}}, {a: {c: 13, b: 12}}); // tested on mongodb                                   // 380
  nomatch({a: {}}, {a: {b: 12}});                                                                           // 381
  nomatch({a: {b:12}}, {a: {}});                                                                            // 382
  match(                                                                                                    // 383
    {a: {b: 12, c: [13, true, false, 2.2, "a", null, {d: 14}]}},                                            // 384
    {a: {b: 12, c: [13, true, false, 2.2, "a", null, {d: 14}]}});                                           // 385
  match({a: {b: 12}}, {a: {b: 12}, k: 99});                                                                 // 386
                                                                                                            // 387
  match({a: {b: 12}}, {a: [{b: 12}]});                                                                      // 388
  nomatch({a: {b: 12}}, {a: [[{b: 12}]]});                                                                  // 389
  match({a: {b: 12}}, {a: [{b: 11}, {b: 12}, {b: 13}]});                                                    // 390
  nomatch({a: {b: 12}}, {a: [{b: 11}, {b: 12, c: 20}, {b: 13}]});                                           // 391
  nomatch({a: {b: 12, c: 20}}, {a: [{b: 11}, {b: 12}, {c: 20}]});                                           // 392
  match({a: {b: 12, c: 20}}, {a: [{b: 11}, {b: 12, c: 20}, {b: 13}]});                                      // 393
                                                                                                            // 394
  // null                                                                                                   // 395
  match({a: null}, {a: null});                                                                              // 396
  match({a: null}, {b: 12});                                                                                // 397
  nomatch({a: null}, {a: 12});                                                                              // 398
  match({a: null}, {a: [1, 2, null, 3]}); // tested on mongodb                                              // 399
  nomatch({a: null}, {a: [1, 2, {}, 3]}); // tested on mongodb                                              // 400
                                                                                                            // 401
  // order comparisons: $lt, $gt, $lte, $gte                                                                // 402
  match({a: {$lt: 10}}, {a: 9});                                                                            // 403
  nomatch({a: {$lt: 10}}, {a: 10});                                                                         // 404
  nomatch({a: {$lt: 10}}, {a: 11});                                                                         // 405
                                                                                                            // 406
  match({a: {$gt: 10}}, {a: 11});                                                                           // 407
  nomatch({a: {$gt: 10}}, {a: 10});                                                                         // 408
  nomatch({a: {$gt: 10}}, {a: 9});                                                                          // 409
                                                                                                            // 410
  match({a: {$lte: 10}}, {a: 9});                                                                           // 411
  match({a: {$lte: 10}}, {a: 10});                                                                          // 412
  nomatch({a: {$lte: 10}}, {a: 11});                                                                        // 413
                                                                                                            // 414
  match({a: {$gte: 10}}, {a: 11});                                                                          // 415
  match({a: {$gte: 10}}, {a: 10});                                                                          // 416
  nomatch({a: {$gte: 10}}, {a: 9});                                                                         // 417
                                                                                                            // 418
  match({a: {$lt: 10}}, {a: [11, 9, 12]});                                                                  // 419
  nomatch({a: {$lt: 10}}, {a: [11, 12]});                                                                   // 420
                                                                                                            // 421
  // (there's a full suite of ordering test elsewhere)                                                      // 422
  nomatch({a: {$lt: "null"}}, {a: null});                                                                   // 423
  match({a: {$lt: {x: [2, 3, 4]}}}, {a: {x: [1, 3, 4]}});                                                   // 424
  match({a: {$gt: {x: [2, 3, 4]}}}, {a: {x: [3, 3, 4]}});                                                   // 425
  nomatch({a: {$gt: {x: [2, 3, 4]}}}, {a: {x: [1, 3, 4]}});                                                 // 426
  nomatch({a: {$gt: {x: [2, 3, 4]}}}, {a: {x: [2, 3, 4]}});                                                 // 427
  nomatch({a: {$lt: {x: [2, 3, 4]}}}, {a: {x: [2, 3, 4]}});                                                 // 428
  match({a: {$gte: {x: [2, 3, 4]}}}, {a: {x: [2, 3, 4]}});                                                  // 429
  match({a: {$lte: {x: [2, 3, 4]}}}, {a: {x: [2, 3, 4]}});                                                  // 430
                                                                                                            // 431
  nomatch({a: {$gt: [2, 3]}}, {a: [1, 2]}); // tested against mongodb                                       // 432
                                                                                                            // 433
  // composition of two qualifiers                                                                          // 434
  nomatch({a: {$lt: 11, $gt: 9}}, {a: 8});                                                                  // 435
  nomatch({a: {$lt: 11, $gt: 9}}, {a: 9});                                                                  // 436
  match({a: {$lt: 11, $gt: 9}}, {a: 10});                                                                   // 437
  nomatch({a: {$lt: 11, $gt: 9}}, {a: 11});                                                                 // 438
  nomatch({a: {$lt: 11, $gt: 9}}, {a: 12});                                                                 // 439
                                                                                                            // 440
  match({a: {$lt: 11, $gt: 9}}, {a: [8, 9, 10, 11, 12]});                                                   // 441
  match({a: {$lt: 11, $gt: 9}}, {a: [8, 9, 11, 12]}); // tested against mongodb                             // 442
                                                                                                            // 443
  // $all                                                                                                   // 444
  match({a: {$all: [1, 2]}}, {a: [1, 2]});                                                                  // 445
  nomatch({a: {$all: [1, 2, 3]}}, {a: [1, 2]});                                                             // 446
  match({a: {$all: [1, 2]}}, {a: [3, 2, 1]});                                                               // 447
  match({a: {$all: [1, "x"]}}, {a: [3, "x", 1]});                                                           // 448
  nomatch({a: {$all: ['2']}}, {a: 2});                                                                      // 449
  nomatch({a: {$all: [2]}}, {a: '2'});                                                                      // 450
  match({a: {$all: [[1, 2], [1, 3]]}}, {a: [[1, 3], [1, 2], [1, 4]]});                                      // 451
  nomatch({a: {$all: [[1, 2], [1, 3]]}}, {a: [[1, 4], [1, 2], [1, 4]]});                                    // 452
  match({a: {$all: [2, 2]}}, {a: [2]}); // tested against mongodb                                           // 453
  nomatch({a: {$all: [2, 3]}}, {a: [2, 2]});                                                                // 454
                                                                                                            // 455
  nomatch({a: {$all: [1, 2]}}, {a: [[1, 2]]}); // tested against mongodb                                    // 456
  nomatch({a: {$all: [1, 2]}}, {}); // tested against mongodb, field doesn't exist                          // 457
  nomatch({a: {$all: [1, 2]}}, {a: {foo: 'bar'}}); // tested against mongodb, field is not an object        // 458
  nomatch({a: {$all: []}}, {a: []});                                                                        // 459
  nomatch({a: {$all: []}}, {a: [5]});                                                                       // 460
  match({a: {$all: [/i/, /e/i]}}, {a: ["foo", "bEr", "biz"]});                                              // 461
  nomatch({a: {$all: [/i/, /e/i]}}, {a: ["foo", "bar", "biz"]});                                            // 462
  match({a: {$all: [{b: 3}]}}, {a: [{b: 3}]});                                                              // 463
  // Members of $all other than regexps are *equality matches*, not document                                // 464
  // matches.                                                                                               // 465
  nomatch({a: {$all: [{b: 3}]}}, {a: [{b: 3, k: 4}]});                                                      // 466
  test.throws(function () {                                                                                 // 467
    match({a: {$all: [{$gt: 4}]}}, {});                                                                     // 468
  });                                                                                                       // 469
                                                                                                            // 470
  // $exists                                                                                                // 471
  match({a: {$exists: true}}, {a: 12});                                                                     // 472
  nomatch({a: {$exists: true}}, {b: 12});                                                                   // 473
  nomatch({a: {$exists: false}}, {a: 12});                                                                  // 474
  match({a: {$exists: false}}, {b: 12});                                                                    // 475
                                                                                                            // 476
  match({a: {$exists: true}}, {a: []});                                                                     // 477
  nomatch({a: {$exists: true}}, {b: []});                                                                   // 478
  nomatch({a: {$exists: false}}, {a: []});                                                                  // 479
  match({a: {$exists: false}}, {b: []});                                                                    // 480
                                                                                                            // 481
  match({a: {$exists: true}}, {a: [1]});                                                                    // 482
  nomatch({a: {$exists: true}}, {b: [1]});                                                                  // 483
  nomatch({a: {$exists: false}}, {a: [1]});                                                                 // 484
  match({a: {$exists: false}}, {b: [1]});                                                                   // 485
                                                                                                            // 486
  match({a: {$exists: 1}}, {a: 5});                                                                         // 487
  match({a: {$exists: 0}}, {b: 5});                                                                         // 488
                                                                                                            // 489
  nomatch({'a.x':{$exists: false}}, {a: [{}, {x: 5}]});                                                     // 490
  match({'a.x':{$exists: true}}, {a: [{}, {x: 5}]});                                                        // 491
  match({'a.x':{$exists: true}}, {a: [{}, {x: 5}]});                                                        // 492
  match({'a.x':{$exists: true}}, {a: {x: []}});                                                             // 493
  match({'a.x':{$exists: true}}, {a: {x: null}});                                                           // 494
                                                                                                            // 495
  // $mod                                                                                                   // 496
  match({a: {$mod: [10, 1]}}, {a: 11});                                                                     // 497
  nomatch({a: {$mod: [10, 1]}}, {a: 12});                                                                   // 498
  match({a: {$mod: [10, 1]}}, {a: [10, 11, 12]});                                                           // 499
  nomatch({a: {$mod: [10, 1]}}, {a: [10, 12]});                                                             // 500
  _.each([                                                                                                  // 501
    5,                                                                                                      // 502
    [10],                                                                                                   // 503
    [10, 1, 2],                                                                                             // 504
    "foo",                                                                                                  // 505
    {bar: 1},                                                                                               // 506
    []                                                                                                      // 507
  ], function (badMod) {                                                                                    // 508
    test.throws(function () {                                                                               // 509
      match({a: {$mod: badMod}}, {a: 11});                                                                  // 510
    });                                                                                                     // 511
  });                                                                                                       // 512
                                                                                                            // 513
  // $ne                                                                                                    // 514
  match({a: {$ne: 1}}, {a: 2});                                                                             // 515
  nomatch({a: {$ne: 2}}, {a: 2});                                                                           // 516
  match({a: {$ne: [1]}}, {a: [2]});                                                                         // 517
                                                                                                            // 518
  nomatch({a: {$ne: [1, 2]}}, {a: [1, 2]}); // all tested against mongodb                                   // 519
  nomatch({a: {$ne: 1}}, {a: [1, 2]});                                                                      // 520
  nomatch({a: {$ne: 2}}, {a: [1, 2]});                                                                      // 521
  match({a: {$ne: 3}}, {a: [1, 2]});                                                                        // 522
  nomatch({'a.b': {$ne: 1}}, {a: [{b: 1}, {b: 2}]});                                                        // 523
  nomatch({'a.b': {$ne: 2}}, {a: [{b: 1}, {b: 2}]});                                                        // 524
  match({'a.b': {$ne: 3}}, {a: [{b: 1}, {b: 2}]});                                                          // 525
                                                                                                            // 526
  nomatch({a: {$ne: {x: 1}}}, {a: {x: 1}});                                                                 // 527
  match({a: {$ne: {x: 1}}}, {a: {x: 2}});                                                                   // 528
  match({a: {$ne: {x: 1}}}, {a: {x: 1, y: 2}});                                                             // 529
                                                                                                            // 530
  // This query means: All 'a.b' must be non-5, and some 'a.b' must be >6.                                  // 531
  match({'a.b': {$ne: 5, $gt: 6}}, {a: [{b: 2}, {b: 10}]});                                                 // 532
  nomatch({'a.b': {$ne: 5, $gt: 6}}, {a: [{b: 2}, {b: 4}]});                                                // 533
  nomatch({'a.b': {$ne: 5, $gt: 6}}, {a: [{b: 2}, {b: 5}]});                                                // 534
  nomatch({'a.b': {$ne: 5, $gt: 6}}, {a: [{b: 10}, {b: 5}]});                                               // 535
  // Should work the same if the branch is at the bottom.                                                   // 536
  match({a: {$ne: 5, $gt: 6}}, {a: [2, 10]});                                                               // 537
  nomatch({a: {$ne: 5, $gt: 6}}, {a: [2, 4]});                                                              // 538
  nomatch({a: {$ne: 5, $gt: 6}}, {a: [2, 5]});                                                              // 539
  nomatch({a: {$ne: 5, $gt: 6}}, {a: [10, 5]});                                                             // 540
                                                                                                            // 541
  // $in                                                                                                    // 542
  match({a: {$in: [1, 2, 3]}}, {a: 2});                                                                     // 543
  nomatch({a: {$in: [1, 2, 3]}}, {a: 4});                                                                   // 544
  match({a: {$in: [[1], [2], [3]]}}, {a: [2]});                                                             // 545
  nomatch({a: {$in: [[1], [2], [3]]}}, {a: [4]});                                                           // 546
  match({a: {$in: [{b: 1}, {b: 2}, {b: 3}]}}, {a: {b: 2}});                                                 // 547
  nomatch({a: {$in: [{b: 1}, {b: 2}, {b: 3}]}}, {a: {b: 4}});                                               // 548
                                                                                                            // 549
  match({a: {$in: [1, 2, 3]}}, {a: [2]}); // tested against mongodb                                         // 550
  match({a: {$in: [{x: 1}, {x: 2}, {x: 3}]}}, {a: [{x: 2}]});                                               // 551
  match({a: {$in: [1, 2, 3]}}, {a: [4, 2]});                                                                // 552
  nomatch({a: {$in: [1, 2, 3]}}, {a: [4]});                                                                 // 553
                                                                                                            // 554
  match({a: {$in: ['x', /foo/i]}}, {a: 'x'});                                                               // 555
  match({a: {$in: ['x', /foo/i]}}, {a: 'fOo'});                                                             // 556
  match({a: {$in: ['x', /foo/i]}}, {a: ['f', 'fOo']});                                                      // 557
  nomatch({a: {$in: ['x', /foo/i]}}, {a: ['f', 'fOx']});                                                    // 558
                                                                                                            // 559
  match({a: {$in: [1, null]}}, {});                                                                         // 560
  match({'a.b': {$in: [1, null]}}, {});                                                                     // 561
  match({'a.b': {$in: [1, null]}}, {a: {}});                                                                // 562
  match({'a.b': {$in: [1, null]}}, {a: {b: null}});                                                         // 563
  nomatch({'a.b': {$in: [1, null]}}, {a: {b: 5}});                                                          // 564
  nomatch({'a.b': {$in: [1]}}, {a: {b: null}});                                                             // 565
  nomatch({'a.b': {$in: [1]}}, {a: {}});                                                                    // 566
  nomatch({'a.b': {$in: [1, null]}}, {a: [{b: 5}]});                                                        // 567
  match({'a.b': {$in: [1, null]}}, {a: [{b: 5}, {}]});                                                      // 568
  nomatch({'a.b': {$in: [1, null]}}, {a: [{b: 5}, []]});                                                    // 569
  nomatch({'a.b': {$in: [1, null]}}, {a: [{b: 5}, 5]});                                                     // 570
                                                                                                            // 571
  // $nin                                                                                                   // 572
  nomatch({a: {$nin: [1, 2, 3]}}, {a: 2});                                                                  // 573
  match({a: {$nin: [1, 2, 3]}}, {a: 4});                                                                    // 574
  nomatch({a: {$nin: [[1], [2], [3]]}}, {a: [2]});                                                          // 575
  match({a: {$nin: [[1], [2], [3]]}}, {a: [4]});                                                            // 576
  nomatch({a: {$nin: [{b: 1}, {b: 2}, {b: 3}]}}, {a: {b: 2}});                                              // 577
  match({a: {$nin: [{b: 1}, {b: 2}, {b: 3}]}}, {a: {b: 4}});                                                // 578
                                                                                                            // 579
  nomatch({a: {$nin: [1, 2, 3]}}, {a: [2]}); // tested against mongodb                                      // 580
  nomatch({a: {$nin: [{x: 1}, {x: 2}, {x: 3}]}}, {a: [{x: 2}]});                                            // 581
  nomatch({a: {$nin: [1, 2, 3]}}, {a: [4, 2]});                                                             // 582
  nomatch({'a.b': {$nin: [1, 2, 3]}}, {a: [{b:4}, {b:2}]});                                                 // 583
  match({a: {$nin: [1, 2, 3]}}, {a: [4]});                                                                  // 584
  match({'a.b': {$nin: [1, 2, 3]}}, {a: [{b:4}]});                                                          // 585
                                                                                                            // 586
  nomatch({a: {$nin: ['x', /foo/i]}}, {a: 'x'});                                                            // 587
  nomatch({a: {$nin: ['x', /foo/i]}}, {a: 'fOo'});                                                          // 588
  nomatch({a: {$nin: ['x', /foo/i]}}, {a: ['f', 'fOo']});                                                   // 589
  match({a: {$nin: ['x', /foo/i]}}, {a: ['f', 'fOx']});                                                     // 590
                                                                                                            // 591
  nomatch({a: {$nin: [1, null]}}, {});                                                                      // 592
  nomatch({'a.b': {$nin: [1, null]}}, {});                                                                  // 593
  nomatch({'a.b': {$nin: [1, null]}}, {a: {}});                                                             // 594
  nomatch({'a.b': {$nin: [1, null]}}, {a: {b: null}});                                                      // 595
  match({'a.b': {$nin: [1, null]}}, {a: {b: 5}});                                                           // 596
  match({'a.b': {$nin: [1]}}, {a: {b: null}});                                                              // 597
  match({'a.b': {$nin: [1]}}, {a: {}});                                                                     // 598
  match({'a.b': {$nin: [1, null]}}, {a: [{b: 5}]});                                                         // 599
  nomatch({'a.b': {$nin: [1, null]}}, {a: [{b: 5}, {}]});                                                   // 600
  match({'a.b': {$nin: [1, null]}}, {a: [{b: 5}, []]});                                                     // 601
  match({'a.b': {$nin: [1, null]}}, {a: [{b: 5}, 5]});                                                      // 602
                                                                                                            // 603
  // $size                                                                                                  // 604
  match({a: {$size: 0}}, {a: []});                                                                          // 605
  match({a: {$size: 1}}, {a: [2]});                                                                         // 606
  match({a: {$size: 2}}, {a: [2, 2]});                                                                      // 607
  nomatch({a: {$size: 0}}, {a: [2]});                                                                       // 608
  nomatch({a: {$size: 1}}, {a: []});                                                                        // 609
  nomatch({a: {$size: 1}}, {a: [2, 2]});                                                                    // 610
  nomatch({a: {$size: 0}}, {a: "2"});                                                                       // 611
  nomatch({a: {$size: 1}}, {a: "2"});                                                                       // 612
  nomatch({a: {$size: 2}}, {a: "2"});                                                                       // 613
                                                                                                            // 614
  nomatch({a: {$size: 2}}, {a: [[2,2]]}); // tested against mongodb                                         // 615
                                                                                                            // 616
  // $type                                                                                                  // 617
  match({a: {$type: 1}}, {a: 1.1});                                                                         // 618
  match({a: {$type: 1}}, {a: 1});                                                                           // 619
  nomatch({a: {$type: 1}}, {a: "1"});                                                                       // 620
  match({a: {$type: 2}}, {a: "1"});                                                                         // 621
  nomatch({a: {$type: 2}}, {a: 1});                                                                         // 622
  match({a: {$type: 3}}, {a: {}});                                                                          // 623
  match({a: {$type: 3}}, {a: {b: 2}});                                                                      // 624
  nomatch({a: {$type: 3}}, {a: []});                                                                        // 625
  nomatch({a: {$type: 3}}, {a: [1]});                                                                       // 626
  nomatch({a: {$type: 3}}, {a: null});                                                                      // 627
  match({a: {$type: 5}}, {a: EJSON.newBinary(0)});                                                          // 628
  match({a: {$type: 5}}, {a: EJSON.newBinary(4)});                                                          // 629
  nomatch({a: {$type: 5}}, {a: []});                                                                        // 630
  nomatch({a: {$type: 5}}, {a: [42]});                                                                      // 631
  match({a: {$type: 7}}, {a: new LocalCollection._ObjectID()});                                             // 632
  nomatch({a: {$type: 7}}, {a: "1234567890abcd1234567890"});                                                // 633
  match({a: {$type: 8}}, {a: true});                                                                        // 634
  match({a: {$type: 8}}, {a: false});                                                                       // 635
  nomatch({a: {$type: 8}}, {a: "true"});                                                                    // 636
  nomatch({a: {$type: 8}}, {a: 0});                                                                         // 637
  nomatch({a: {$type: 8}}, {a: null});                                                                      // 638
  nomatch({a: {$type: 8}}, {a: ''});                                                                        // 639
  nomatch({a: {$type: 8}}, {});                                                                             // 640
  match({a: {$type: 9}}, {a: (new Date)});                                                                  // 641
  nomatch({a: {$type: 9}}, {a: +(new Date)});                                                               // 642
  match({a: {$type: 10}}, {a: null});                                                                       // 643
  nomatch({a: {$type: 10}}, {a: false});                                                                    // 644
  nomatch({a: {$type: 10}}, {a: ''});                                                                       // 645
  nomatch({a: {$type: 10}}, {a: 0});                                                                        // 646
  nomatch({a: {$type: 10}}, {});                                                                            // 647
  match({a: {$type: 11}}, {a: /x/});                                                                        // 648
  nomatch({a: {$type: 11}}, {a: 'x'});                                                                      // 649
  nomatch({a: {$type: 11}}, {});                                                                            // 650
                                                                                                            // 651
  // The normal rule for {$type:4} (4 means array) is that it NOT good enough to                            // 652
  // just have an array that's the leaf that matches the path.  (An array inside                            // 653
  // that array is good, though.)                                                                           // 654
  nomatch({a: {$type: 4}}, {a: []});                                                                        // 655
  nomatch({a: {$type: 4}}, {a: [1]}); // tested against mongodb                                             // 656
  match({a: {$type: 1}}, {a: [1]});                                                                         // 657
  nomatch({a: {$type: 2}}, {a: [1]});                                                                       // 658
  match({a: {$type: 1}}, {a: ["1", 1]});                                                                    // 659
  match({a: {$type: 2}}, {a: ["1", 1]});                                                                    // 660
  nomatch({a: {$type: 3}}, {a: ["1", 1]});                                                                  // 661
  nomatch({a: {$type: 4}}, {a: ["1", 1]});                                                                  // 662
  nomatch({a: {$type: 1}}, {a: ["1", []]});                                                                 // 663
  match({a: {$type: 2}}, {a: ["1", []]});                                                                   // 664
  match({a: {$type: 4}}, {a: ["1", []]}); // tested against mongodb                                         // 665
  // An exception to the normal rule is that an array found via numeric index is                            // 666
  // examined itself, and its elements are not.                                                             // 667
  match({'a.0': {$type: 4}}, {a: [[0]]});                                                                   // 668
  nomatch({'a.0': {$type: 1}}, {a: [[0]]});                                                                 // 669
                                                                                                            // 670
  // regular expressions                                                                                    // 671
  match({a: /a/}, {a: 'cat'});                                                                              // 672
  nomatch({a: /a/}, {a: 'cut'});                                                                            // 673
  nomatch({a: /a/}, {a: 'CAT'});                                                                            // 674
  match({a: /a/i}, {a: 'CAT'});                                                                             // 675
  match({a: /a/}, {a: ['foo', 'bar']});  // search within array...                                          // 676
  nomatch({a: /,/}, {a: ['foo', 'bar']});  // but not by stringifying                                       // 677
  match({a: {$regex: 'a'}}, {a: ['foo', 'bar']});                                                           // 678
  nomatch({a: {$regex: ','}}, {a: ['foo', 'bar']});                                                         // 679
  match({a: {$regex: /a/}}, {a: 'cat'});                                                                    // 680
  nomatch({a: {$regex: /a/}}, {a: 'cut'});                                                                  // 681
  nomatch({a: {$regex: /a/}}, {a: 'CAT'});                                                                  // 682
  match({a: {$regex: /a/i}}, {a: 'CAT'});                                                                   // 683
  match({a: {$regex: /a/, $options: 'i'}}, {a: 'CAT'}); // tested                                           // 684
  match({a: {$regex: /a/i, $options: 'i'}}, {a: 'CAT'}); // tested                                          // 685
  nomatch({a: {$regex: /a/i, $options: ''}}, {a: 'CAT'}); // tested                                         // 686
  match({a: {$regex: 'a'}}, {a: 'cat'});                                                                    // 687
  nomatch({a: {$regex: 'a'}}, {a: 'cut'});                                                                  // 688
  nomatch({a: {$regex: 'a'}}, {a: 'CAT'});                                                                  // 689
  match({a: {$regex: 'a', $options: 'i'}}, {a: 'CAT'});                                                     // 690
  match({a: {$regex: '', $options: 'i'}}, {a: 'foo'});                                                      // 691
  nomatch({a: {$regex: '', $options: 'i'}}, {});                                                            // 692
  nomatch({a: {$regex: '', $options: 'i'}}, {a: 5});                                                        // 693
  nomatch({a: /undefined/}, {});                                                                            // 694
  nomatch({a: {$regex: 'undefined'}}, {});                                                                  // 695
  nomatch({a: /xxx/}, {});                                                                                  // 696
  nomatch({a: {$regex: 'xxx'}}, {});                                                                        // 697
                                                                                                            // 698
  test.throws(function () {                                                                                 // 699
    match({a: {$options: 'i'}}, {a: 12});                                                                   // 700
  });                                                                                                       // 701
                                                                                                            // 702
  match({a: /a/}, {a: ['dog', 'cat']});                                                                     // 703
  nomatch({a: /a/}, {a: ['dog', 'puppy']});                                                                 // 704
                                                                                                            // 705
  // we don't support regexps in minimongo very well (eg, there's no EJSON                                  // 706
  // encoding so it won't go over the wire), but run these tests anyway                                     // 707
  match({a: /a/}, {a: /a/});                                                                                // 708
  match({a: /a/}, {a: ['x', /a/]});                                                                         // 709
  nomatch({a: /a/}, {a: /a/i});                                                                             // 710
  nomatch({a: /a/m}, {a: /a/});                                                                             // 711
  nomatch({a: /a/}, {a: /b/});                                                                              // 712
  nomatch({a: /5/}, {a: 5});                                                                                // 713
  nomatch({a: /t/}, {a: true});                                                                             // 714
  match({a: /m/i}, {a: ['x', 'xM']});                                                                       // 715
                                                                                                            // 716
  test.throws(function () {                                                                                 // 717
    match({a: {$regex: /a/, $options: 'x'}}, {a: 'cat'});                                                   // 718
  });                                                                                                       // 719
  test.throws(function () {                                                                                 // 720
    match({a: {$regex: /a/, $options: 's'}}, {a: 'cat'});                                                   // 721
  });                                                                                                       // 722
                                                                                                            // 723
  // $not                                                                                                   // 724
  match({x: {$not: {$gt: 7}}}, {x: 6});                                                                     // 725
  nomatch({x: {$not: {$gt: 7}}}, {x: 8});                                                                   // 726
  match({x: {$not: {$lt: 10, $gt: 7}}}, {x: 11});                                                           // 727
  nomatch({x: {$not: {$lt: 10, $gt: 7}}}, {x: 9});                                                          // 728
  match({x: {$not: {$lt: 10, $gt: 7}}}, {x: 6});                                                            // 729
                                                                                                            // 730
  match({x: {$not: {$gt: 7}}}, {x: [2, 3, 4]});                                                             // 731
  match({'x.y': {$not: {$gt: 7}}}, {x: [{y:2}, {y:3}, {y:4}]});                                             // 732
  nomatch({x: {$not: {$gt: 7}}}, {x: [2, 3, 4, 10]});                                                       // 733
  nomatch({'x.y': {$not: {$gt: 7}}}, {x: [{y:2}, {y:3}, {y:4}, {y:10}]});                                   // 734
                                                                                                            // 735
  match({x: {$not: /a/}}, {x: "dog"});                                                                      // 736
  nomatch({x: {$not: /a/}}, {x: "cat"});                                                                    // 737
  match({x: {$not: /a/}}, {x: ["dog", "puppy"]});                                                           // 738
  nomatch({x: {$not: /a/}}, {x: ["kitten", "cat"]});                                                        // 739
                                                                                                            // 740
  // dotted keypaths: bare values                                                                           // 741
  match({"a.b": 1}, {a: {b: 1}});                                                                           // 742
  nomatch({"a.b": 1}, {a: {b: 2}});                                                                         // 743
  match({"a.b": [1,2,3]}, {a: {b: [1,2,3]}});                                                               // 744
  nomatch({"a.b": [1,2,3]}, {a: {b: [4]}});                                                                 // 745
  match({"a.b": /a/}, {a: {b: "cat"}});                                                                     // 746
  nomatch({"a.b": /a/}, {a: {b: "dog"}});                                                                   // 747
  match({"a.b.c": null}, {});                                                                               // 748
  match({"a.b.c": null}, {a: 1});                                                                           // 749
  match({"a.b": null}, {a: 1});                                                                             // 750
  match({"a.b.c": null}, {a: {b: 4}});                                                                      // 751
                                                                                                            // 752
  // dotted keypaths, nulls, numeric indices, arrays                                                        // 753
  nomatch({"a.b": null}, {a: [1]});                                                                         // 754
  match({"a.b": []}, {a: {b: []}});                                                                         // 755
  var big = {a: [{b: 1}, 2, {}, {b: [3, 4]}]};                                                              // 756
  match({"a.b": 1}, big);                                                                                   // 757
  match({"a.b": [3, 4]}, big);                                                                              // 758
  match({"a.b": 3}, big);                                                                                   // 759
  match({"a.b": 4}, big);                                                                                   // 760
  match({"a.b": null}, big);  // matches on slot 2                                                          // 761
  match({'a.1': 8}, {a: [7, 8, 9]});                                                                        // 762
  nomatch({'a.1': 7}, {a: [7, 8, 9]});                                                                      // 763
  nomatch({'a.1': null}, {a: [7, 8, 9]});                                                                   // 764
  match({'a.1': [8, 9]}, {a: [7, [8, 9]]});                                                                 // 765
  nomatch({'a.1': 6}, {a: [[6, 7], [8, 9]]});                                                               // 766
  nomatch({'a.1': 7}, {a: [[6, 7], [8, 9]]});                                                               // 767
  nomatch({'a.1': 8}, {a: [[6, 7], [8, 9]]});                                                               // 768
  nomatch({'a.1': 9}, {a: [[6, 7], [8, 9]]});                                                               // 769
  match({"a.1": 2}, {a: [0, {1: 2}, 3]});                                                                   // 770
  match({"a.1": {1: 2}}, {a: [0, {1: 2}, 3]});                                                              // 771
  match({"x.1.y": 8}, {x: [7, {y: 8}, 9]});                                                                 // 772
  // comes from trying '1' as key in the plain object                                                       // 773
  match({"x.1.y": null}, {x: [7, {y: 8}, 9]});                                                              // 774
  match({"a.1.b": 9}, {a: [7, {b: 9}, {1: {b: 'foo'}}]});                                                   // 775
  match({"a.1.b": 'foo'}, {a: [7, {b: 9}, {1: {b: 'foo'}}]});                                               // 776
  match({"a.1.b": null}, {a: [7, {b: 9}, {1: {b: 'foo'}}]});                                                // 777
  match({"a.1.b": 2}, {a: [1, [{b: 2}], 3]});                                                               // 778
  nomatch({"a.1.b": null}, {a: [1, [{b: 2}], 3]});                                                          // 779
  // this is new behavior in mongo 2.5                                                                      // 780
  nomatch({"a.0.b": null}, {a: [5]});                                                                       // 781
  match({"a.1": 4}, {a: [{1: 4}, 5]});                                                                      // 782
  match({"a.1": 5}, {a: [{1: 4}, 5]});                                                                      // 783
  nomatch({"a.1": null}, {a: [{1: 4}, 5]});                                                                 // 784
  match({"a.1.foo": 4}, {a: [{1: {foo: 4}}, {foo: 5}]});                                                    // 785
  match({"a.1.foo": 5}, {a: [{1: {foo: 4}}, {foo: 5}]});                                                    // 786
  match({"a.1.foo": null}, {a: [{1: {foo: 4}}, {foo: 5}]});                                                 // 787
                                                                                                            // 788
  // trying to access a dotted field that is undefined at some point                                        // 789
  // down the chain                                                                                         // 790
  nomatch({"a.b": 1}, {x: 2});                                                                              // 791
  nomatch({"a.b.c": 1}, {a: {x: 2}});                                                                       // 792
  nomatch({"a.b.c": 1}, {a: {b: {x: 2}}});                                                                  // 793
  nomatch({"a.b.c": 1}, {a: {b: 1}});                                                                       // 794
  nomatch({"a.b.c": 1}, {a: {b: 0}});                                                                       // 795
                                                                                                            // 796
  // dotted keypaths: literal objects                                                                       // 797
  match({"a.b": {c: 1}}, {a: {b: {c: 1}}});                                                                 // 798
  nomatch({"a.b": {c: 1}}, {a: {b: {c: 2}}});                                                               // 799
  nomatch({"a.b": {c: 1}}, {a: {b: 2}});                                                                    // 800
  match({"a.b": {c: 1, d: 2}}, {a: {b: {c: 1, d: 2}}});                                                     // 801
  nomatch({"a.b": {c: 1, d: 2}}, {a: {b: {c: 1, d: 1}}});                                                   // 802
  nomatch({"a.b": {c: 1, d: 2}}, {a: {b: {d: 2}}});                                                         // 803
                                                                                                            // 804
  // dotted keypaths: $ operators                                                                           // 805
  match({"a.b": {$in: [1, 2, 3]}}, {a: {b: [2]}}); // tested against mongodb                                // 806
  match({"a.b": {$in: [{x: 1}, {x: 2}, {x: 3}]}}, {a: {b: [{x: 2}]}});                                      // 807
  match({"a.b": {$in: [1, 2, 3]}}, {a: {b: [4, 2]}});                                                       // 808
  nomatch({"a.b": {$in: [1, 2, 3]}}, {a: {b: [4]}});                                                        // 809
                                                                                                            // 810
  // $or                                                                                                    // 811
  test.throws(function () {                                                                                 // 812
    match({$or: []}, {});                                                                                   // 813
  });                                                                                                       // 814
  test.throws(function () {                                                                                 // 815
    match({$or: [5]}, {});                                                                                  // 816
  });                                                                                                       // 817
  test.throws(function () {                                                                                 // 818
    match({$or: []}, {a: 1});                                                                               // 819
  });                                                                                                       // 820
  match({$or: [{a: 1}]}, {a: 1});                                                                           // 821
  nomatch({$or: [{b: 2}]}, {a: 1});                                                                         // 822
  match({$or: [{a: 1}, {b: 2}]}, {a: 1});                                                                   // 823
  nomatch({$or: [{c: 3}, {d: 4}]}, {a: 1});                                                                 // 824
  match({$or: [{a: 1}, {b: 2}]}, {a: [1, 2, 3]});                                                           // 825
  nomatch({$or: [{a: 1}, {b: 2}]}, {c: [1, 2, 3]});                                                         // 826
  nomatch({$or: [{a: 1}, {b: 2}]}, {a: [2, 3, 4]});                                                         // 827
  match({$or: [{a: 1}, {a: 2}]}, {a: 1});                                                                   // 828
  match({$or: [{a: 1}, {a: 2}], b: 2}, {a: 1, b: 2});                                                       // 829
  nomatch({$or: [{a: 2}, {a: 3}], b: 2}, {a: 1, b: 2});                                                     // 830
  nomatch({$or: [{a: 1}, {a: 2}], b: 3}, {a: 1, b: 2});                                                     // 831
                                                                                                            // 832
  // Combining $or with equality                                                                            // 833
  match({x: 1, $or: [{a: 1}, {b: 1}]}, {x: 1, b: 1});                                                       // 834
  match({$or: [{a: 1}, {b: 1}], x: 1}, {x: 1, b: 1});                                                       // 835
  nomatch({x: 1, $or: [{a: 1}, {b: 1}]}, {b: 1});                                                           // 836
  nomatch({x: 1, $or: [{a: 1}, {b: 1}]}, {x: 1});                                                           // 837
                                                                                                            // 838
  // $or and $lt, $lte, $gt, $gte                                                                           // 839
  match({$or: [{a: {$lte: 1}}, {a: 2}]}, {a: 1});                                                           // 840
  nomatch({$or: [{a: {$lt: 1}}, {a: 2}]}, {a: 1});                                                          // 841
  match({$or: [{a: {$gte: 1}}, {a: 2}]}, {a: 1});                                                           // 842
  nomatch({$or: [{a: {$gt: 1}}, {a: 2}]}, {a: 1});                                                          // 843
  match({$or: [{b: {$gt: 1}}, {b: {$lt: 3}}]}, {b: 2});                                                     // 844
  nomatch({$or: [{b: {$lt: 1}}, {b: {$gt: 3}}]}, {b: 2});                                                   // 845
                                                                                                            // 846
  // $or and $in                                                                                            // 847
  match({$or: [{a: {$in: [1, 2, 3]}}]}, {a: 1});                                                            // 848
  nomatch({$or: [{a: {$in: [4, 5, 6]}}]}, {a: 1});                                                          // 849
  match({$or: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {a: 1});                                                    // 850
  match({$or: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {b: 2});                                                    // 851
  nomatch({$or: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {c: 3});                                                  // 852
  match({$or: [{a: {$in: [1, 2, 3]}}, {b: {$in: [1, 2, 3]}}]}, {b: 2});                                     // 853
  nomatch({$or: [{a: {$in: [1, 2, 3]}}, {b: {$in: [4, 5, 6]}}]}, {b: 2});                                   // 854
                                                                                                            // 855
  // $or and $nin                                                                                           // 856
  nomatch({$or: [{a: {$nin: [1, 2, 3]}}]}, {a: 1});                                                         // 857
  match({$or: [{a: {$nin: [4, 5, 6]}}]}, {a: 1});                                                           // 858
  nomatch({$or: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {a: 1});                                                 // 859
  match({$or: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {b: 2});                                                   // 860
  match({$or: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {c: 3});                                                   // 861
  match({$or: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [1, 2, 3]}}]}, {b: 2});                                   // 862
  nomatch({$or: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [1, 2, 3]}}]}, {a: 1, b: 2});                           // 863
  match({$or: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [4, 5, 6]}}]}, {b: 2});                                   // 864
                                                                                                            // 865
  // $or and dot-notation                                                                                   // 866
  match({$or: [{"a.b": 1}, {"a.b": 2}]}, {a: {b: 1}});                                                      // 867
  match({$or: [{"a.b": 1}, {"a.c": 1}]}, {a: {b: 1}});                                                      // 868
  nomatch({$or: [{"a.b": 2}, {"a.c": 1}]}, {a: {b: 1}});                                                    // 869
                                                                                                            // 870
  // $or and nested objects                                                                                 // 871
  match({$or: [{a: {b: 1, c: 2}}, {a: {b: 2, c: 1}}]}, {a: {b: 1, c: 2}});                                  // 872
  nomatch({$or: [{a: {b: 1, c: 3}}, {a: {b: 2, c: 1}}]}, {a: {b: 1, c: 2}});                                // 873
                                                                                                            // 874
  // $or and regexes                                                                                        // 875
  match({$or: [{a: /a/}]}, {a: "cat"});                                                                     // 876
  nomatch({$or: [{a: /o/}]}, {a: "cat"});                                                                   // 877
  match({$or: [{a: /a/}, {a: /o/}]}, {a: "cat"});                                                           // 878
  nomatch({$or: [{a: /i/}, {a: /o/}]}, {a: "cat"});                                                         // 879
  match({$or: [{a: /i/}, {b: /o/}]}, {a: "cat", b: "dog"});                                                 // 880
                                                                                                            // 881
  // $or and $ne                                                                                            // 882
  match({$or: [{a: {$ne: 1}}]}, {});                                                                        // 883
  nomatch({$or: [{a: {$ne: 1}}]}, {a: 1});                                                                  // 884
  match({$or: [{a: {$ne: 1}}]}, {a: 2});                                                                    // 885
  match({$or: [{a: {$ne: 1}}]}, {b: 1});                                                                    // 886
  match({$or: [{a: {$ne: 1}}, {a: {$ne: 2}}]}, {a: 1});                                                     // 887
  match({$or: [{a: {$ne: 1}}, {b: {$ne: 1}}]}, {a: 1});                                                     // 888
  nomatch({$or: [{a: {$ne: 1}}, {b: {$ne: 2}}]}, {a: 1, b: 2});                                             // 889
                                                                                                            // 890
  // $or and $not                                                                                           // 891
  match({$or: [{a: {$not: {$mod: [10, 1]}}}]}, {});                                                         // 892
  nomatch({$or: [{a: {$not: {$mod: [10, 1]}}}]}, {a: 1});                                                   // 893
  match({$or: [{a: {$not: {$mod: [10, 1]}}}]}, {a: 2});                                                     // 894
  match({$or: [{a: {$not: {$mod: [10, 1]}}}, {a: {$not: {$mod: [10, 2]}}}]}, {a: 1});                       // 895
  nomatch({$or: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 1});                             // 896
  match({$or: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 2});                               // 897
  match({$or: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 3});                               // 898
  // this is possibly an open-ended task, so we stop here ...                                               // 899
                                                                                                            // 900
  // $nor                                                                                                   // 901
  test.throws(function () {                                                                                 // 902
    match({$nor: []}, {});                                                                                  // 903
  });                                                                                                       // 904
  test.throws(function () {                                                                                 // 905
    match({$nor: [5]}, {});                                                                                 // 906
  });                                                                                                       // 907
  test.throws(function () {                                                                                 // 908
    match({$nor: []}, {a: 1});                                                                              // 909
  });                                                                                                       // 910
  nomatch({$nor: [{a: 1}]}, {a: 1});                                                                        // 911
  match({$nor: [{b: 2}]}, {a: 1});                                                                          // 912
  nomatch({$nor: [{a: 1}, {b: 2}]}, {a: 1});                                                                // 913
  match({$nor: [{c: 3}, {d: 4}]}, {a: 1});                                                                  // 914
  nomatch({$nor: [{a: 1}, {b: 2}]}, {a: [1, 2, 3]});                                                        // 915
  match({$nor: [{a: 1}, {b: 2}]}, {c: [1, 2, 3]});                                                          // 916
  match({$nor: [{a: 1}, {b: 2}]}, {a: [2, 3, 4]});                                                          // 917
  nomatch({$nor: [{a: 1}, {a: 2}]}, {a: 1});                                                                // 918
                                                                                                            // 919
  // $nor and $lt, $lte, $gt, $gte                                                                          // 920
  nomatch({$nor: [{a: {$lte: 1}}, {a: 2}]}, {a: 1});                                                        // 921
  match({$nor: [{a: {$lt: 1}}, {a: 2}]}, {a: 1});                                                           // 922
  nomatch({$nor: [{a: {$gte: 1}}, {a: 2}]}, {a: 1});                                                        // 923
  match({$nor: [{a: {$gt: 1}}, {a: 2}]}, {a: 1});                                                           // 924
  nomatch({$nor: [{b: {$gt: 1}}, {b: {$lt: 3}}]}, {b: 2});                                                  // 925
  match({$nor: [{b: {$lt: 1}}, {b: {$gt: 3}}]}, {b: 2});                                                    // 926
                                                                                                            // 927
  // $nor and $in                                                                                           // 928
  nomatch({$nor: [{a: {$in: [1, 2, 3]}}]}, {a: 1});                                                         // 929
  match({$nor: [{a: {$in: [4, 5, 6]}}]}, {a: 1});                                                           // 930
  nomatch({$nor: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {a: 1});                                                 // 931
  nomatch({$nor: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {b: 2});                                                 // 932
  match({$nor: [{a: {$in: [1, 2, 3]}}, {b: 2}]}, {c: 3});                                                   // 933
  nomatch({$nor: [{a: {$in: [1, 2, 3]}}, {b: {$in: [1, 2, 3]}}]}, {b: 2});                                  // 934
  match({$nor: [{a: {$in: [1, 2, 3]}}, {b: {$in: [4, 5, 6]}}]}, {b: 2});                                    // 935
                                                                                                            // 936
  // $nor and $nin                                                                                          // 937
  match({$nor: [{a: {$nin: [1, 2, 3]}}]}, {a: 1});                                                          // 938
  nomatch({$nor: [{a: {$nin: [4, 5, 6]}}]}, {a: 1});                                                        // 939
  match({$nor: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {a: 1});                                                  // 940
  nomatch({$nor: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {b: 2});                                                // 941
  nomatch({$nor: [{a: {$nin: [1, 2, 3]}}, {b: 2}]}, {c: 3});                                                // 942
  nomatch({$nor: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [1, 2, 3]}}]}, {b: 2});                                // 943
  match({$nor: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [1, 2, 3]}}]}, {a: 1, b: 2});                            // 944
  nomatch({$nor: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [4, 5, 6]}}]}, {b: 2});                                // 945
                                                                                                            // 946
  // $nor and dot-notation                                                                                  // 947
  nomatch({$nor: [{"a.b": 1}, {"a.b": 2}]}, {a: {b: 1}});                                                   // 948
  nomatch({$nor: [{"a.b": 1}, {"a.c": 1}]}, {a: {b: 1}});                                                   // 949
  match({$nor: [{"a.b": 2}, {"a.c": 1}]}, {a: {b: 1}});                                                     // 950
                                                                                                            // 951
  // $nor and nested objects                                                                                // 952
  nomatch({$nor: [{a: {b: 1, c: 2}}, {a: {b: 2, c: 1}}]}, {a: {b: 1, c: 2}});                               // 953
  match({$nor: [{a: {b: 1, c: 3}}, {a: {b: 2, c: 1}}]}, {a: {b: 1, c: 2}});                                 // 954
                                                                                                            // 955
  // $nor and regexes                                                                                       // 956
  nomatch({$nor: [{a: /a/}]}, {a: "cat"});                                                                  // 957
  match({$nor: [{a: /o/}]}, {a: "cat"});                                                                    // 958
  nomatch({$nor: [{a: /a/}, {a: /o/}]}, {a: "cat"});                                                        // 959
  match({$nor: [{a: /i/}, {a: /o/}]}, {a: "cat"});                                                          // 960
  nomatch({$nor: [{a: /i/}, {b: /o/}]}, {a: "cat", b: "dog"});                                              // 961
                                                                                                            // 962
  // $nor and $ne                                                                                           // 963
  nomatch({$nor: [{a: {$ne: 1}}]}, {});                                                                     // 964
  match({$nor: [{a: {$ne: 1}}]}, {a: 1});                                                                   // 965
  nomatch({$nor: [{a: {$ne: 1}}]}, {a: 2});                                                                 // 966
  nomatch({$nor: [{a: {$ne: 1}}]}, {b: 1});                                                                 // 967
  nomatch({$nor: [{a: {$ne: 1}}, {a: {$ne: 2}}]}, {a: 1});                                                  // 968
  nomatch({$nor: [{a: {$ne: 1}}, {b: {$ne: 1}}]}, {a: 1});                                                  // 969
  match({$nor: [{a: {$ne: 1}}, {b: {$ne: 2}}]}, {a: 1, b: 2});                                              // 970
                                                                                                            // 971
  // $nor and $not                                                                                          // 972
  nomatch({$nor: [{a: {$not: {$mod: [10, 1]}}}]}, {});                                                      // 973
  match({$nor: [{a: {$not: {$mod: [10, 1]}}}]}, {a: 1});                                                    // 974
  nomatch({$nor: [{a: {$not: {$mod: [10, 1]}}}]}, {a: 2});                                                  // 975
  nomatch({$nor: [{a: {$not: {$mod: [10, 1]}}}, {a: {$not: {$mod: [10, 2]}}}]}, {a: 1});                    // 976
  match({$nor: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 1});                              // 977
  nomatch({$nor: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 2});                            // 978
  nomatch({$nor: [{a: {$not: {$mod: [10, 1]}}}, {a: {$mod: [10, 2]}}]}, {a: 3});                            // 979
                                                                                                            // 980
  // $and                                                                                                   // 981
                                                                                                            // 982
  test.throws(function () {                                                                                 // 983
    match({$and: []}, {});                                                                                  // 984
  });                                                                                                       // 985
  test.throws(function () {                                                                                 // 986
    match({$and: [5]}, {});                                                                                 // 987
  });                                                                                                       // 988
  test.throws(function () {                                                                                 // 989
    match({$and: []}, {a: 1});                                                                              // 990
  });                                                                                                       // 991
  match({$and: [{a: 1}]}, {a: 1});                                                                          // 992
  nomatch({$and: [{a: 1}, {a: 2}]}, {a: 1});                                                                // 993
  nomatch({$and: [{a: 1}, {b: 1}]}, {a: 1});                                                                // 994
  match({$and: [{a: 1}, {b: 2}]}, {a: 1, b: 2});                                                            // 995
  nomatch({$and: [{a: 1}, {b: 1}]}, {a: 1, b: 2});                                                          // 996
  match({$and: [{a: 1}, {b: 2}], c: 3}, {a: 1, b: 2, c: 3});                                                // 997
  nomatch({$and: [{a: 1}, {b: 2}], c: 4}, {a: 1, b: 2, c: 3});                                              // 998
                                                                                                            // 999
  // $and and regexes                                                                                       // 1000
  match({$and: [{a: /a/}]}, {a: "cat"});                                                                    // 1001
  match({$and: [{a: /a/i}]}, {a: "CAT"});                                                                   // 1002
  nomatch({$and: [{a: /o/}]}, {a: "cat"});                                                                  // 1003
  nomatch({$and: [{a: /a/}, {a: /o/}]}, {a: "cat"});                                                        // 1004
  match({$and: [{a: /a/}, {b: /o/}]}, {a: "cat", b: "dog"});                                                // 1005
  nomatch({$and: [{a: /a/}, {b: /a/}]}, {a: "cat", b: "dog"});                                              // 1006
                                                                                                            // 1007
  // $and, dot-notation, and nested objects                                                                 // 1008
  match({$and: [{"a.b": 1}]}, {a: {b: 1}});                                                                 // 1009
  match({$and: [{a: {b: 1}}]}, {a: {b: 1}});                                                                // 1010
  nomatch({$and: [{"a.b": 2}]}, {a: {b: 1}});                                                               // 1011
  nomatch({$and: [{"a.c": 1}]}, {a: {b: 1}});                                                               // 1012
  nomatch({$and: [{"a.b": 1}, {"a.b": 2}]}, {a: {b: 1}});                                                   // 1013
  nomatch({$and: [{"a.b": 1}, {a: {b: 2}}]}, {a: {b: 1}});                                                  // 1014
  match({$and: [{"a.b": 1}, {"c.d": 2}]}, {a: {b: 1}, c: {d: 2}});                                          // 1015
  nomatch({$and: [{"a.b": 1}, {"c.d": 1}]}, {a: {b: 1}, c: {d: 2}});                                        // 1016
  match({$and: [{"a.b": 1}, {c: {d: 2}}]}, {a: {b: 1}, c: {d: 2}});                                         // 1017
  nomatch({$and: [{"a.b": 1}, {c: {d: 1}}]}, {a: {b: 1}, c: {d: 2}});                                       // 1018
  nomatch({$and: [{"a.b": 2}, {c: {d: 2}}]}, {a: {b: 1}, c: {d: 2}});                                       // 1019
  match({$and: [{a: {b: 1}}, {c: {d: 2}}]}, {a: {b: 1}, c: {d: 2}});                                        // 1020
  nomatch({$and: [{a: {b: 2}}, {c: {d: 2}}]}, {a: {b: 1}, c: {d: 2}});                                      // 1021
                                                                                                            // 1022
  // $and and $in                                                                                           // 1023
  nomatch({$and: [{a: {$in: []}}]}, {});                                                                    // 1024
  match({$and: [{a: {$in: [1, 2, 3]}}]}, {a: 1});                                                           // 1025
  nomatch({$and: [{a: {$in: [4, 5, 6]}}]}, {a: 1});                                                         // 1026
  nomatch({$and: [{a: {$in: [1, 2, 3]}}, {a: {$in: [4, 5, 6]}}]}, {a: 1});                                  // 1027
  nomatch({$and: [{a: {$in: [1, 2, 3]}}, {b: {$in: [1, 2, 3]}}]}, {a: 1, b: 4});                            // 1028
  match({$and: [{a: {$in: [1, 2, 3]}}, {b: {$in: [4, 5, 6]}}]}, {a: 1, b: 4});                              // 1029
                                                                                                            // 1030
                                                                                                            // 1031
  // $and and $nin                                                                                          // 1032
  match({$and: [{a: {$nin: []}}]}, {});                                                                     // 1033
  nomatch({$and: [{a: {$nin: [1, 2, 3]}}]}, {a: 1});                                                        // 1034
  match({$and: [{a: {$nin: [4, 5, 6]}}]}, {a: 1});                                                          // 1035
  nomatch({$and: [{a: {$nin: [1, 2, 3]}}, {a: {$nin: [4, 5, 6]}}]}, {a: 1});                                // 1036
  nomatch({$and: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [1, 2, 3]}}]}, {a: 1, b: 4});                          // 1037
  nomatch({$and: [{a: {$nin: [1, 2, 3]}}, {b: {$nin: [4, 5, 6]}}]}, {a: 1, b: 4});                          // 1038
                                                                                                            // 1039
  // $and and $lt, $lte, $gt, $gte                                                                          // 1040
  match({$and: [{a: {$lt: 2}}]}, {a: 1});                                                                   // 1041
  nomatch({$and: [{a: {$lt: 1}}]}, {a: 1});                                                                 // 1042
  match({$and: [{a: {$lte: 1}}]}, {a: 1});                                                                  // 1043
  match({$and: [{a: {$gt: 0}}]}, {a: 1});                                                                   // 1044
  nomatch({$and: [{a: {$gt: 1}}]}, {a: 1});                                                                 // 1045
  match({$and: [{a: {$gte: 1}}]}, {a: 1});                                                                  // 1046
  match({$and: [{a: {$gt: 0}}, {a: {$lt: 2}}]}, {a: 1});                                                    // 1047
  nomatch({$and: [{a: {$gt: 1}}, {a: {$lt: 2}}]}, {a: 1});                                                  // 1048
  nomatch({$and: [{a: {$gt: 0}}, {a: {$lt: 1}}]}, {a: 1});                                                  // 1049
  match({$and: [{a: {$gte: 1}}, {a: {$lte: 1}}]}, {a: 1});                                                  // 1050
  nomatch({$and: [{a: {$gte: 2}}, {a: {$lte: 0}}]}, {a: 1});                                                // 1051
                                                                                                            // 1052
  // $and and $ne                                                                                           // 1053
  match({$and: [{a: {$ne: 1}}]}, {});                                                                       // 1054
  nomatch({$and: [{a: {$ne: 1}}]}, {a: 1});                                                                 // 1055
  match({$and: [{a: {$ne: 1}}]}, {a: 2});                                                                   // 1056
  nomatch({$and: [{a: {$ne: 1}}, {a: {$ne: 2}}]}, {a: 2});                                                  // 1057
  match({$and: [{a: {$ne: 1}}, {a: {$ne: 3}}]}, {a: 2});                                                    // 1058
                                                                                                            // 1059
  // $and and $not                                                                                          // 1060
  match({$and: [{a: {$not: {$gt: 2}}}]}, {a: 1});                                                           // 1061
  nomatch({$and: [{a: {$not: {$lt: 2}}}]}, {a: 1});                                                         // 1062
  match({$and: [{a: {$not: {$lt: 0}}}, {a: {$not: {$gt: 2}}}]}, {a: 1});                                    // 1063
  nomatch({$and: [{a: {$not: {$lt: 2}}}, {a: {$not: {$gt: 0}}}]}, {a: 1});                                  // 1064
                                                                                                            // 1065
  // $where                                                                                                 // 1066
  match({$where: "this.a === 1"}, {a: 1});                                                                  // 1067
  match({$where: "obj.a === 1"}, {a: 1});                                                                   // 1068
  nomatch({$where: "this.a !== 1"}, {a: 1});                                                                // 1069
  nomatch({$where: "obj.a !== 1"}, {a: 1});                                                                 // 1070
  nomatch({$where: "this.a === 1", a: 2}, {a: 1});                                                          // 1071
  match({$where: "this.a === 1", b: 2}, {a: 1, b: 2});                                                      // 1072
  match({$where: "this.a === 1 && this.b === 2"}, {a: 1, b: 2});                                            // 1073
  match({$where: "this.a instanceof Array"}, {a: []});                                                      // 1074
  nomatch({$where: "this.a instanceof Array"}, {a: 1});                                                     // 1075
                                                                                                            // 1076
  // reaching into array                                                                                    // 1077
  match({"dogs.0.name": "Fido"}, {dogs: [{name: "Fido"}, {name: "Rex"}]});                                  // 1078
  match({"dogs.1.name": "Rex"}, {dogs: [{name: "Fido"}, {name: "Rex"}]});                                   // 1079
  nomatch({"dogs.1.name": "Fido"}, {dogs: [{name: "Fido"}, {name: "Rex"}]});                                // 1080
  match({"room.1b": "bla"}, {room: {"1b": "bla"}});                                                         // 1081
                                                                                                            // 1082
  match({"dogs.name": "Fido"}, {dogs: [{name: "Fido"}, {name: "Rex"}]});                                    // 1083
  match({"dogs.name": "Rex"}, {dogs: [{name: "Fido"}, {name: "Rex"}]});                                     // 1084
  match({"animals.dogs.name": "Fido"},                                                                      // 1085
        {animals: [{dogs: [{name: "Rover"}]},                                                               // 1086
                   {},                                                                                      // 1087
                   {dogs: [{name: "Fido"}, {name: "Rex"}]}]});                                              // 1088
  match({"animals.dogs.name": "Fido"},                                                                      // 1089
        {animals: [{dogs: {name: "Rex"}},                                                                   // 1090
                   {dogs: {name: "Fido"}}]});                                                               // 1091
  match({"animals.dogs.name": "Fido"},                                                                      // 1092
        {animals: [{dogs: [{name: "Rover"}]},                                                               // 1093
                   {},                                                                                      // 1094
                   {dogs: [{name: ["Fido"]}, {name: "Rex"}]}]});                                            // 1095
  nomatch({"dogs.name": "Fido"}, {dogs: []});                                                               // 1096
                                                                                                            // 1097
  // $elemMatch                                                                                             // 1098
  match({dogs: {$elemMatch: {name: /e/}}},                                                                  // 1099
        {dogs: [{name: "Fido"}, {name: "Rex"}]});                                                           // 1100
  nomatch({dogs: {$elemMatch: {name: /a/}}},                                                                // 1101
          {dogs: [{name: "Fido"}, {name: "Rex"}]});                                                         // 1102
  match({dogs: {$elemMatch: {age: {$gt: 4}}}},                                                              // 1103
        {dogs: [{name: "Fido", age: 5}, {name: "Rex", age: 3}]});                                           // 1104
  match({dogs: {$elemMatch: {name: "Fido", age: {$gt: 4}}}},                                                // 1105
        {dogs: [{name: "Fido", age: 5}, {name: "Rex", age: 3}]});                                           // 1106
  nomatch({dogs: {$elemMatch: {name: "Fido", age: {$gt: 5}}}},                                              // 1107
          {dogs: [{name: "Fido", age: 5}, {name: "Rex", age: 3}]});                                         // 1108
  match({dogs: {$elemMatch: {name: /i/, age: {$gt: 4}}}},                                                   // 1109
        {dogs: [{name: "Fido", age: 5}, {name: "Rex", age: 3}]});                                           // 1110
  nomatch({dogs: {$elemMatch: {name: /e/, age: 5}}},                                                        // 1111
          {dogs: [{name: "Fido", age: 5}, {name: "Rex", age: 3}]});                                         // 1112
  match({x: {$elemMatch: {y: 9}}}, {x: [{y: 9}]});                                                          // 1113
  nomatch({x: {$elemMatch: {y: 9}}}, {x: [[{y: 9}]]});                                                      // 1114
  match({x: {$elemMatch: {$gt: 5, $lt: 9}}}, {x: [8]});                                                     // 1115
  nomatch({x: {$elemMatch: {$gt: 5, $lt: 9}}}, {x: [[8]]});                                                 // 1116
  match({'a.x': {$elemMatch: {y: 9}}},                                                                      // 1117
        {a: [{x: []}, {x: [{y: 9}]}]});                                                                     // 1118
  nomatch({a: {$elemMatch: {x: 5}}}, {a: {x: 5}});                                                          // 1119
  match({a: {$elemMatch: {0: {$gt: 5, $lt: 9}}}}, {a: [[6]]});                                              // 1120
  match({a: {$elemMatch: {'0.b': {$gt: 5, $lt: 9}}}}, {a: [[{b:6}]]});                                      // 1121
  match({a: {$elemMatch: {x: 1, $or: [{a: 1}, {b: 1}]}}},                                                   // 1122
        {a: [{x: 1, b: 1}]});                                                                               // 1123
  match({a: {$elemMatch: {$or: [{a: 1}, {b: 1}], x: 1}}},                                                   // 1124
        {a: [{x: 1, b: 1}]});                                                                               // 1125
  nomatch({a: {$elemMatch: {x: 1, $or: [{a: 1}, {b: 1}]}}},                                                 // 1126
          {a: [{b: 1}]});                                                                                   // 1127
  nomatch({a: {$elemMatch: {x: 1, $or: [{a: 1}, {b: 1}]}}},                                                 // 1128
          {a: [{x: 1}]});                                                                                   // 1129
  nomatch({a: {$elemMatch: {x: 1, $or: [{a: 1}, {b: 1}]}}},                                                 // 1130
          {a: [{x: 1}, {b: 1}]});                                                                           // 1131
                                                                                                            // 1132
  // $comment                                                                                               // 1133
  match({a: 5, $comment: "asdf"}, {a: 5});                                                                  // 1134
  nomatch({a: 6, $comment: "asdf"}, {a: 5});                                                                // 1135
                                                                                                            // 1136
  // XXX still needs tests:                                                                                 // 1137
  // - non-scalar arguments to $gt, $lt, etc                                                                // 1138
});                                                                                                         // 1139
                                                                                                            // 1140
Tinytest.add("minimongo - projection_compiler", function (test) {                                           // 1141
  var testProjection = function (projection, tests) {                                                       // 1142
    var projection_f = LocalCollection._compileProjection(projection);                                      // 1143
    var equalNonStrict = function (a, b, desc) {                                                            // 1144
      test.isTrue(_.isEqual(a, b), desc);                                                                   // 1145
    };                                                                                                      // 1146
                                                                                                            // 1147
    _.each(tests, function (testCase) {                                                                     // 1148
      equalNonStrict(projection_f(testCase[0]), testCase[1], testCase[2]);                                  // 1149
    });                                                                                                     // 1150
  };                                                                                                        // 1151
                                                                                                            // 1152
  testProjection({ 'foo': 1, 'bar': 1 }, [                                                                  // 1153
    [{ foo: 42, bar: "something", baz: "else" },                                                            // 1154
     { foo: 42, bar: "something" },                                                                         // 1155
     "simplest - whitelist"],                                                                               // 1156
                                                                                                            // 1157
    [{ foo: { nested: 17 }, baz: {} },                                                                      // 1158
     { foo: { nested: 17 } },                                                                               // 1159
     "nested whitelisted field"],                                                                           // 1160
                                                                                                            // 1161
    [{ _id: "uid", bazbaz: 42 },                                                                            // 1162
     { _id: "uid" },                                                                                        // 1163
     "simplest whitelist - preserve _id"]                                                                   // 1164
  ]);                                                                                                       // 1165
                                                                                                            // 1166
  testProjection({ 'foo': 0, 'bar': 0 }, [                                                                  // 1167
    [{ foo: 42, bar: "something", baz: "else" },                                                            // 1168
     { baz: "else" },                                                                                       // 1169
     "simplest - blacklist"],                                                                               // 1170
                                                                                                            // 1171
    [{ foo: { nested: 17 }, baz: { foo: "something" } },                                                    // 1172
     { baz: { foo: "something" } },                                                                         // 1173
     "nested blacklisted field"],                                                                           // 1174
                                                                                                            // 1175
    [{ _id: "uid", bazbaz: 42 },                                                                            // 1176
     { _id: "uid", bazbaz: 42 },                                                                            // 1177
     "simplest blacklist - preserve _id"]                                                                   // 1178
  ]);                                                                                                       // 1179
                                                                                                            // 1180
  testProjection({ _id: 0, foo: 1 }, [                                                                      // 1181
    [{ foo: 42, bar: 33, _id: "uid" },                                                                      // 1182
     { foo: 42 },                                                                                           // 1183
     "whitelist - _id blacklisted"]                                                                         // 1184
  ]);                                                                                                       // 1185
                                                                                                            // 1186
  testProjection({ _id: 0, foo: 0 }, [                                                                      // 1187
    [{ foo: 42, bar: 33, _id: "uid" },                                                                      // 1188
     { bar: 33 },                                                                                           // 1189
     "blacklist - _id blacklisted"]                                                                         // 1190
  ]);                                                                                                       // 1191
                                                                                                            // 1192
  testProjection({ 'foo.bar.baz': 1 }, [                                                                    // 1193
    [{ foo: { meh: "fur", bar: { baz: 42 }, tr: 1 }, bar: 33, baz: 'trolololo' },                           // 1194
     { foo: { bar: { baz: 42 } } },                                                                         // 1195
     "whitelist nested"],                                                                                   // 1196
                                                                                                            // 1197
    // Behavior of this test is looked up in actual mongo                                                   // 1198
    [{ foo: { meh: "fur", bar: "nope", tr: 1 }, bar: 33, baz: 'trolololo' },                                // 1199
     { foo: {} },                                                                                           // 1200
     "whitelist nested - path not found in doc, different type"],                                           // 1201
                                                                                                            // 1202
    // Behavior of this test is looked up in actual mongo                                                   // 1203
    [{ foo: { meh: "fur", bar: [], tr: 1 }, bar: 33, baz: 'trolololo' },                                    // 1204
     { foo: { bar: [] } },                                                                                  // 1205
     "whitelist nested - path not found in doc"]                                                            // 1206
  ]);                                                                                                       // 1207
                                                                                                            // 1208
  testProjection({ 'hope.humanity': 0, 'hope.people': 0 }, [                                                // 1209
    [{ hope: { humanity: "lost", people: 'broken', candies: 'long live!' } },                               // 1210
     { hope: { candies: 'long live!' } },                                                                   // 1211
     "blacklist nested"],                                                                                   // 1212
                                                                                                            // 1213
    [{ hope: "new" },                                                                                       // 1214
     { hope: "new" },                                                                                       // 1215
     "blacklist nested - path not found in doc"]                                                            // 1216
  ]);                                                                                                       // 1217
                                                                                                            // 1218
  testProjection({ _id: 1 }, [                                                                              // 1219
    [{ _id: 42, x: 1, y: { z: "2" } },                                                                      // 1220
     { _id: 42 },                                                                                           // 1221
     "_id whitelisted"],                                                                                    // 1222
    [{ _id: 33 },                                                                                           // 1223
     { _id: 33 },                                                                                           // 1224
     "_id whitelisted, _id only"],                                                                          // 1225
    [{ x: 1 },                                                                                              // 1226
     {},                                                                                                    // 1227
     "_id whitelisted, no _id"]                                                                             // 1228
  ]);                                                                                                       // 1229
                                                                                                            // 1230
  testProjection({ _id: 0 }, [                                                                              // 1231
    [{ _id: 42, x: 1, y: { z: "2" } },                                                                      // 1232
     { x: 1, y: { z: "2" } },                                                                               // 1233
     "_id blacklisted"],                                                                                    // 1234
    [{ _id: 33 },                                                                                           // 1235
     {},                                                                                                    // 1236
     "_id blacklisted, _id only"],                                                                          // 1237
    [{ x: 1 },                                                                                              // 1238
     { x: 1 },                                                                                              // 1239
     "_id blacklisted, no _id"]                                                                             // 1240
  ]);                                                                                                       // 1241
                                                                                                            // 1242
  testProjection({}, [                                                                                      // 1243
    [{ a: 1, b: 2, c: "3" },                                                                                // 1244
     { a: 1, b: 2, c: "3" },                                                                                // 1245
     "empty projection"]                                                                                    // 1246
  ]);                                                                                                       // 1247
                                                                                                            // 1248
  test.throws(function () {                                                                                 // 1249
    testProjection({ 'inc': 1, 'excl': 0 }, [                                                               // 1250
      [ { inc: 42, excl: 42 }, { inc: 42 }, "Can't combine incl/excl rules" ]                               // 1251
    ]);                                                                                                     // 1252
  });                                                                                                       // 1253
                                                                                                            // 1254
  test.throws(function () {                                                                                 // 1255
    testProjection({ 'a': 1, 'a.b': 1 }, [                                                                  // 1256
      [ { a: { b: 42 } }, { a: { b: 42 } }, "Can't have ambiguous rules (one is prefix of another)" ]       // 1257
    ]);                                                                                                     // 1258
  });                                                                                                       // 1259
  test.throws(function () {                                                                                 // 1260
    testProjection({ 'a.b.c': 1, 'a.b': 1, 'a': 1 }, [                                                      // 1261
      [ { a: { b: 42 } }, { a: { b: 42 } }, "Can't have ambiguous rules (one is prefix of another)" ]       // 1262
    ]);                                                                                                     // 1263
  });                                                                                                       // 1264
                                                                                                            // 1265
  test.throws(function () {                                                                                 // 1266
    testProjection("some string", [                                                                         // 1267
      [ { a: { b: 42 } }, { a: { b: 42 } }, "Projection is not a hash" ]                                    // 1268
    ]);                                                                                                     // 1269
  });                                                                                                       // 1270
});                                                                                                         // 1271
                                                                                                            // 1272
Tinytest.add("minimongo - fetch with fields", function (test) {                                             // 1273
  var c = new LocalCollection();                                                                            // 1274
  _.times(30, function (i) {                                                                                // 1275
    c.insert({                                                                                              // 1276
      something: Random.id(),                                                                               // 1277
      anything: {                                                                                           // 1278
        foo: "bar",                                                                                         // 1279
        cool: "hot"                                                                                         // 1280
      },                                                                                                    // 1281
      nothing: i,                                                                                           // 1282
      i: i                                                                                                  // 1283
    });                                                                                                     // 1284
  });                                                                                                       // 1285
                                                                                                            // 1286
  // Test just a regular fetch with some projection                                                         // 1287
  var fetchResults = c.find({}, { fields: {                                                                 // 1288
    'something': 1,                                                                                         // 1289
    'anything.foo': 1                                                                                       // 1290
  } }).fetch();                                                                                             // 1291
                                                                                                            // 1292
  test.isTrue(_.all(fetchResults, function (x) {                                                            // 1293
    return x &&                                                                                             // 1294
           x.something &&                                                                                   // 1295
           x.anything &&                                                                                    // 1296
           x.anything.foo &&                                                                                // 1297
           x.anything.foo === "bar" &&                                                                      // 1298
           !_.has(x, 'nothing') &&                                                                          // 1299
           !_.has(x.anything, 'cool');                                                                      // 1300
  }));                                                                                                      // 1301
                                                                                                            // 1302
  // Test with a selector, even field used in the selector is excluded in the                               // 1303
  // projection                                                                                             // 1304
  fetchResults = c.find({                                                                                   // 1305
    nothing: { $gte: 5 }                                                                                    // 1306
  }, {                                                                                                      // 1307
    fields: { nothing: 0 }                                                                                  // 1308
  }).fetch();                                                                                               // 1309
                                                                                                            // 1310
  test.isTrue(_.all(fetchResults, function (x) {                                                            // 1311
    return x &&                                                                                             // 1312
           x.something &&                                                                                   // 1313
           x.anything &&                                                                                    // 1314
           x.anything.foo === "bar" &&                                                                      // 1315
           x.anything.cool === "hot" &&                                                                     // 1316
           !_.has(x, 'nothing') &&                                                                          // 1317
           x.i &&                                                                                           // 1318
           x.i >= 5;                                                                                        // 1319
  }));                                                                                                      // 1320
                                                                                                            // 1321
  test.isTrue(fetchResults.length === 25);                                                                  // 1322
                                                                                                            // 1323
  // Test that we can sort, based on field excluded from the projection, use                                // 1324
  // skip and limit as well!                                                                                // 1325
  // following find will get indexes [10..20) sorted by nothing                                             // 1326
  fetchResults = c.find({}, {                                                                               // 1327
    sort: {                                                                                                 // 1328
      nothing: 1                                                                                            // 1329
    },                                                                                                      // 1330
    limit: 10,                                                                                              // 1331
    skip: 10,                                                                                               // 1332
    fields: {                                                                                               // 1333
      i: 1,                                                                                                 // 1334
      something: 1                                                                                          // 1335
    }                                                                                                       // 1336
  }).fetch();                                                                                               // 1337
                                                                                                            // 1338
  test.isTrue(_.all(fetchResults, function (x) {                                                            // 1339
    return x &&                                                                                             // 1340
           x.something &&                                                                                   // 1341
           x.i >= 10 && x.i < 20;                                                                           // 1342
  }));                                                                                                      // 1343
                                                                                                            // 1344
  _.each(fetchResults, function (x, i, arr) {                                                               // 1345
    if (!i) return;                                                                                         // 1346
    test.isTrue(x.i === arr[i-1].i + 1);                                                                    // 1347
  });                                                                                                       // 1348
                                                                                                            // 1349
  // Temporary unsupported operators                                                                        // 1350
  // queries are taken from MongoDB docs examples                                                           // 1351
  test.throws(function () {                                                                                 // 1352
    c.find({}, { fields: { 'grades.$': 1 } });                                                              // 1353
  });                                                                                                       // 1354
  test.throws(function () {                                                                                 // 1355
    c.find({}, { fields: { grades: { $elemMatch: { mean: 70 } } } });                                       // 1356
  });                                                                                                       // 1357
  test.throws(function () {                                                                                 // 1358
    c.find({}, { fields: { grades: { $slice: [20, 10] } } });                                               // 1359
  });                                                                                                       // 1360
});                                                                                                         // 1361
                                                                                                            // 1362
Tinytest.add("minimongo - fetch with projection, subarrays", function (test) {                              // 1363
  // Apparently projection of type 'foo.bar.x' for                                                          // 1364
  // { foo: [ { bar: { x: 42 } }, { bar: { x: 3 } } ] }                                                     // 1365
  // should return exactly this object. More precisely, arrays are considered as                            // 1366
  // sets and are queried separately and then merged back to result set                                     // 1367
  var c = new LocalCollection();                                                                            // 1368
                                                                                                            // 1369
  // Insert a test object with two set fields                                                               // 1370
  c.insert({                                                                                                // 1371
    setA: [{                                                                                                // 1372
      fieldA: 42,                                                                                           // 1373
      fieldB: 33                                                                                            // 1374
    }, {                                                                                                    // 1375
      fieldA: "the good",                                                                                   // 1376
      fieldB: "the bad",                                                                                    // 1377
      fieldC: "the ugly"                                                                                    // 1378
    }],                                                                                                     // 1379
    setB: [{                                                                                                // 1380
      anotherA: { },                                                                                        // 1381
      anotherB: "meh"                                                                                       // 1382
    }, {                                                                                                    // 1383
      anotherA: 1234,                                                                                       // 1384
      anotherB: 431                                                                                         // 1385
    }]                                                                                                      // 1386
  });                                                                                                       // 1387
                                                                                                            // 1388
  var equalNonStrict = function (a, b, desc) {                                                              // 1389
    test.isTrue(_.isEqual(a, b), desc);                                                                     // 1390
  };                                                                                                        // 1391
                                                                                                            // 1392
  var testForProjection = function (projection, expected) {                                                 // 1393
    var fetched = c.find({}, { fields: projection }).fetch()[0];                                            // 1394
    equalNonStrict(fetched, expected, "failed sub-set projection: " +                                       // 1395
                                      JSON.stringify(projection));                                          // 1396
  };                                                                                                        // 1397
                                                                                                            // 1398
  testForProjection({ 'setA.fieldA': 1, 'setB.anotherB': 1, _id: 0 },                                       // 1399
                    {                                                                                       // 1400
                      setA: [{ fieldA: 42 }, { fieldA: "the good" }],                                       // 1401
                      setB: [{ anotherB: "meh" }, { anotherB: 431 }]                                        // 1402
                    });                                                                                     // 1403
                                                                                                            // 1404
  testForProjection({ 'setA.fieldA': 0, 'setB.anotherA': 0, _id: 0 },                                       // 1405
                    {                                                                                       // 1406
                      setA: [{fieldB:33}, {fieldB:"the bad",fieldC:"the ugly"}],                            // 1407
                      setB: [{ anotherB: "meh" }, { anotherB: 431 }]                                        // 1408
                    });                                                                                     // 1409
                                                                                                            // 1410
  c.remove({});                                                                                             // 1411
  c.insert({a:[[{b:1,c:2},{b:2,c:4}],{b:3,c:5},[{b:4, c:9}]]});                                             // 1412
                                                                                                            // 1413
  testForProjection({ 'a.b': 1, _id: 0 },                                                                   // 1414
                    {a: [ [ { b: 1 }, { b: 2 } ], { b: 3 }, [ { b: 4 } ] ] });                              // 1415
  testForProjection({ 'a.b': 0, _id: 0 },                                                                   // 1416
                    {a: [ [ { c: 2 }, { c: 4 } ], { c: 5 }, [ { c: 9 } ] ] });                              // 1417
});                                                                                                         // 1418
                                                                                                            // 1419
Tinytest.add("minimongo - fetch with projection, deep copy", function (test) {                              // 1420
  // Compiled fields projection defines the contract: returned document doesn't                             // 1421
  // retain anything from the passed argument.                                                              // 1422
  var doc = {                                                                                               // 1423
    a: { x: 42 },                                                                                           // 1424
    b: {                                                                                                    // 1425
      y: { z: 33 }                                                                                          // 1426
    },                                                                                                      // 1427
    c: "asdf"                                                                                               // 1428
  };                                                                                                        // 1429
                                                                                                            // 1430
  var fields = {                                                                                            // 1431
    'a': 1,                                                                                                 // 1432
    'b.y': 1                                                                                                // 1433
  };                                                                                                        // 1434
                                                                                                            // 1435
  var projectionFn = LocalCollection._compileProjection(fields);                                            // 1436
  var filteredDoc = projectionFn(doc);                                                                      // 1437
  doc.a.x++;                                                                                                // 1438
  doc.b.y.z--;                                                                                              // 1439
  test.equal(filteredDoc.a.x, 42, "projection returning deep copy - including");                            // 1440
  test.equal(filteredDoc.b.y.z, 33, "projection returning deep copy - including");                          // 1441
                                                                                                            // 1442
  fields = { c: 0 };                                                                                        // 1443
  projectionFn = LocalCollection._compileProjection(fields);                                                // 1444
  filteredDoc = projectionFn(doc);                                                                          // 1445
                                                                                                            // 1446
  doc.a.x = 5;                                                                                              // 1447
  test.equal(filteredDoc.a.x, 43, "projection returning deep copy - excluding");                            // 1448
});                                                                                                         // 1449
                                                                                                            // 1450
Tinytest.add("minimongo - observe ordered with projection", function (test) {                               // 1451
  // These tests are copy-paste from "minimongo -observe ordered",                                          // 1452
  // slightly modified to test projection                                                                   // 1453
  var operations = [];                                                                                      // 1454
  var cbs = log_callbacks(operations);                                                                      // 1455
  var handle;                                                                                               // 1456
                                                                                                            // 1457
  var c = new LocalCollection();                                                                            // 1458
  handle = c.find({}, {sort: {a: 1}, fields: { a: 1 }}).observe(cbs);                                       // 1459
  test.isTrue(handle.collection === c);                                                                     // 1460
                                                                                                            // 1461
  c.insert({_id: 'foo', a:1, b:2});                                                                         // 1462
  test.equal(operations.shift(), ['added', {a:1}, 0, null]);                                                // 1463
  c.update({a:1}, {$set: {a: 2, b: 1}});                                                                    // 1464
  test.equal(operations.shift(), ['changed', {a:2}, 0, {a:1}]);                                             // 1465
  c.insert({_id: 'bar', a:10, c: 33});                                                                      // 1466
  test.equal(operations.shift(), ['added', {a:10}, 1, null]);                                               // 1467
  c.update({}, {$inc: {a: 1}}, {multi: true});                                                              // 1468
  c.update({}, {$inc: {c: 1}}, {multi: true});                                                              // 1469
  test.equal(operations.shift(), ['changed', {a:3}, 0, {a:2}]);                                             // 1470
  test.equal(operations.shift(), ['changed', {a:11}, 1, {a:10}]);                                           // 1471
  c.update({a:11}, {a:1, b:44});                                                                            // 1472
  test.equal(operations.shift(), ['changed', {a:1}, 1, {a:11}]);                                            // 1473
  test.equal(operations.shift(), ['moved', {a:1}, 1, 0, 'foo']);                                            // 1474
  c.remove({a:2});                                                                                          // 1475
  test.equal(operations.shift(), undefined);                                                                // 1476
  c.remove({a:3});                                                                                          // 1477
  test.equal(operations.shift(), ['removed', 'foo', 1, {a:3}]);                                             // 1478
                                                                                                            // 1479
  // test stop                                                                                              // 1480
  handle.stop();                                                                                            // 1481
  var idA2 = Random.id();                                                                                   // 1482
  c.insert({_id: idA2, a:2});                                                                               // 1483
  test.equal(operations.shift(), undefined);                                                                // 1484
                                                                                                            // 1485
  var cursor = c.find({}, {fields: {a: 1, _id: 0}});                                                        // 1486
  test.throws(function () {                                                                                 // 1487
    cursor.observeChanges({added: function () {}});                                                         // 1488
  });                                                                                                       // 1489
  test.throws(function () {                                                                                 // 1490
    cursor.observe({added: function () {}});                                                                // 1491
  });                                                                                                       // 1492
                                                                                                            // 1493
  // test initial inserts (and backwards sort)                                                              // 1494
  handle = c.find({}, {sort: {a: -1}, fields: { a: 1 } }).observe(cbs);                                     // 1495
  test.equal(operations.shift(), ['added', {a:2}, 0, null]);                                                // 1496
  test.equal(operations.shift(), ['added', {a:1}, 1, null]);                                                // 1497
  handle.stop();                                                                                            // 1498
                                                                                                            // 1499
  // test _suppress_initial                                                                                 // 1500
  handle = c.find({}, {sort: {a: -1}, fields: { a: 1 }}).observe(_.extend(cbs, {_suppress_initial: true})); // 1501
  test.equal(operations.shift(), undefined);                                                                // 1502
  c.insert({a:100, b: { foo: "bar" }});                                                                     // 1503
  test.equal(operations.shift(), ['added', {a:100}, 0, idA2]);                                              // 1504
  handle.stop();                                                                                            // 1505
                                                                                                            // 1506
  // test skip and limit.                                                                                   // 1507
  c.remove({});                                                                                             // 1508
  handle = c.find({}, {sort: {a: 1}, skip: 1, limit: 2, fields: { 'blacklisted': 0 }}).observe(cbs);        // 1509
  test.equal(operations.shift(), undefined);                                                                // 1510
  c.insert({a:1, blacklisted:1324});                                                                        // 1511
  test.equal(operations.shift(), undefined);                                                                // 1512
  c.insert({_id: 'foo', a:2, blacklisted:["something"]});                                                   // 1513
  test.equal(operations.shift(), ['added', {a:2}, 0, null]);                                                // 1514
  c.insert({a:3, blacklisted: { 2: 3 }});                                                                   // 1515
  test.equal(operations.shift(), ['added', {a:3}, 1, null]);                                                // 1516
  c.insert({a:4, blacklisted: 6});                                                                          // 1517
  test.equal(operations.shift(), undefined);                                                                // 1518
  c.update({a:1}, {a:0, blacklisted:4444});                                                                 // 1519
  test.equal(operations.shift(), undefined);                                                                // 1520
  c.update({a:0}, {a:5, blacklisted:11111});                                                                // 1521
  test.equal(operations.shift(), ['removed', 'foo', 0, {a:2}]);                                             // 1522
  test.equal(operations.shift(), ['added', {a:4}, 1, null]);                                                // 1523
  c.update({a:3}, {a:3.5, blacklisted:333.4444});                                                           // 1524
  test.equal(operations.shift(), ['changed', {a:3.5}, 0, {a:3}]);                                           // 1525
  handle.stop();                                                                                            // 1526
                                                                                                            // 1527
  // test _no_indices                                                                                       // 1528
                                                                                                            // 1529
  c.remove({});                                                                                             // 1530
  handle = c.find({}, {sort: {a: 1}, fields: { a: 1 }}).observe(_.extend(cbs, {_no_indices: true}));        // 1531
  c.insert({_id: 'foo', a:1, zoo: "crazy"});                                                                // 1532
  test.equal(operations.shift(), ['added', {a:1}, -1, null]);                                               // 1533
  c.update({a:1}, {$set: {a: 2, foobar: "player"}});                                                        // 1534
  test.equal(operations.shift(), ['changed', {a:2}, -1, {a:1}]);                                            // 1535
  c.insert({a:10, b:123.45});                                                                               // 1536
  test.equal(operations.shift(), ['added', {a:10}, -1, null]);                                              // 1537
  c.update({}, {$inc: {a: 1, b:2}}, {multi: true});                                                         // 1538
  test.equal(operations.shift(), ['changed', {a:3}, -1, {a:2}]);                                            // 1539
  test.equal(operations.shift(), ['changed', {a:11}, -1, {a:10}]);                                          // 1540
  c.update({a:11, b:125.45}, {a:1, b:444});                                                                 // 1541
  test.equal(operations.shift(), ['changed', {a:1}, -1, {a:11}]);                                           // 1542
  test.equal(operations.shift(), ['moved', {a:1}, -1, -1, 'foo']);                                          // 1543
  c.remove({a:2});                                                                                          // 1544
  test.equal(operations.shift(), undefined);                                                                // 1545
  c.remove({a:3});                                                                                          // 1546
  test.equal(operations.shift(), ['removed', 'foo', -1, {a:3}]);                                            // 1547
  handle.stop();                                                                                            // 1548
});                                                                                                         // 1549
                                                                                                            // 1550
                                                                                                            // 1551
Tinytest.add("minimongo - ordering", function (test) {                                                      // 1552
  var shortBinary = EJSON.newBinary(1);                                                                     // 1553
  shortBinary[0] = 128;                                                                                     // 1554
  var longBinary1 = EJSON.newBinary(2);                                                                     // 1555
  longBinary1[1] = 42;                                                                                      // 1556
  var longBinary2 = EJSON.newBinary(2);                                                                     // 1557
  longBinary2[1] = 50;                                                                                      // 1558
                                                                                                            // 1559
  var date1 = new Date;                                                                                     // 1560
  var date2 = new Date(date1.getTime() + 1000);                                                             // 1561
                                                                                                            // 1562
  // value ordering                                                                                         // 1563
  assert_ordering(test, LocalCollection._f._cmp, [                                                          // 1564
    null,                                                                                                   // 1565
    1, 2.2, 3,                                                                                              // 1566
    "03", "1", "11", "2", "a", "aaa",                                                                       // 1567
    {}, {a: 2}, {a: 3}, {a: 3, b: 4}, {b: 4}, {b: 4, a: 3},                                                 // 1568
    {b: {}}, {b: [1, 2, 3]}, {b: [1, 2, 4]},                                                                // 1569
    [], [1, 2], [1, 2, 3], [1, 2, 4], [1, 2, "4"], [1, 2, [4]],                                             // 1570
    shortBinary, longBinary1, longBinary2,                                                                  // 1571
    new LocalCollection._ObjectID("1234567890abcd1234567890"),                                              // 1572
    new LocalCollection._ObjectID("abcd1234567890abcd123456"),                                              // 1573
    false, true,                                                                                            // 1574
    date1, date2                                                                                            // 1575
  ]);                                                                                                       // 1576
                                                                                                            // 1577
  // document ordering under a sort specification                                                           // 1578
  var verify = function (sorts, docs) {                                                                     // 1579
    _.each(_.isArray(sorts) ? sorts : [sorts], function (sort) {                                            // 1580
      var sorter = new Minimongo.Sorter(sort);                                                              // 1581
      assert_ordering(test, sorter.getComparator(), docs);                                                  // 1582
    });                                                                                                     // 1583
  };                                                                                                        // 1584
                                                                                                            // 1585
  // note: [] doesn't sort with "arrays", it sorts as "undefined". the position                             // 1586
  // of arrays in _typeorder only matters for things like $lt. (This behavior                               // 1587
  // verified with MongoDB 2.2.1.) We don't define the relative order of {a: []}                            // 1588
  // and {c: 1} is undefined (MongoDB does seem to care but it's not clear how                              // 1589
  // or why).                                                                                               // 1590
  verify([{"a" : 1}, ["a"], [["a", "asc"]]],                                                                // 1591
         [{a: []}, {a: 1}, {a: {}}, {a: true}]);                                                            // 1592
  verify([{"a" : 1}, ["a"], [["a", "asc"]]],                                                                // 1593
         [{c: 1}, {a: 1}, {a: {}}, {a: true}]);                                                             // 1594
  verify([{"a" : -1}, [["a", "desc"]]],                                                                     // 1595
         [{a: true}, {a: {}}, {a: 1}, {c: 1}]);                                                             // 1596
  verify([{"a" : -1}, [["a", "desc"]]],                                                                     // 1597
         [{a: true}, {a: {}}, {a: 1}, {a: []}]);                                                            // 1598
                                                                                                            // 1599
  verify([{"a" : 1, "b": -1}, ["a", ["b", "desc"]],                                                         // 1600
          [["a", "asc"], ["b", "desc"]]],                                                                   // 1601
         [{c: 1}, {a: 1, b: 3}, {a: 1, b: 2}, {a: 2, b: 0}]);                                               // 1602
                                                                                                            // 1603
  verify([{"a" : 1, "b": 1}, ["a", "b"],                                                                    // 1604
          [["a", "asc"], ["b", "asc"]]],                                                                    // 1605
         [{c: 1}, {a: 1, b: 2}, {a: 1, b: 3}, {a: 2, b: 0}]);                                               // 1606
                                                                                                            // 1607
  test.throws(function () {                                                                                 // 1608
    new Minimongo.Sorter("a");                                                                              // 1609
  });                                                                                                       // 1610
                                                                                                            // 1611
  test.throws(function () {                                                                                 // 1612
    new Minimongo.Sorter(123);                                                                              // 1613
  });                                                                                                       // 1614
                                                                                                            // 1615
  // We don't support $natural:1 (since we don't actually have Mongo's on-disk                              // 1616
  // ordering available!)                                                                                   // 1617
  test.throws(function () {                                                                                 // 1618
    new Minimongo.Sorter({$natural: 1});                                                                    // 1619
  });                                                                                                       // 1620
                                                                                                            // 1621
  // No sort spec implies everything equal.                                                                 // 1622
  test.equal(new Minimongo.Sorter({}).getComparator()({a:1}, {a:2}), 0);                                    // 1623
                                                                                                            // 1624
  // All sorts of array edge cases!                                                                         // 1625
  // Increasing sort sorts by the smallest element it finds; 1 < 2.                                         // 1626
  verify({a: 1}, [                                                                                          // 1627
    {a: [1, 10, 20]},                                                                                       // 1628
    {a: [5, 2, 99]}                                                                                         // 1629
  ]);                                                                                                       // 1630
  // Decreasing sorts by largest it finds; 99 > 20.                                                         // 1631
  verify({a: -1}, [                                                                                         // 1632
    {a: [5, 2, 99]},                                                                                        // 1633
    {a: [1, 10, 20]}                                                                                        // 1634
  ]);                                                                                                       // 1635
  // Can also sort by specific array indices.                                                               // 1636
  verify({'a.1': 1}, [                                                                                      // 1637
    {a: [5, 2, 99]},                                                                                        // 1638
    {a: [1, 10, 20]}                                                                                        // 1639
  ]);                                                                                                       // 1640
  // We do NOT expand sub-arrays, so the minimum in the second doc is 5, not                                // 1641
  // -20. (Numbers always sort before arrays.)                                                              // 1642
  verify({a: 1}, [                                                                                          // 1643
    {a: [1, [10, 15], 20]},                                                                                 // 1644
    {a: [5, [-5, -20], 18]}                                                                                 // 1645
  ]);                                                                                                       // 1646
  // The maximum in each of these is the array, since arrays are "greater" than                             // 1647
  // numbers. And [10, 15] is greater than [-5, -20].                                                       // 1648
  verify({a: -1}, [                                                                                         // 1649
    {a: [1, [10, 15], 20]},                                                                                 // 1650
    {a: [5, [-5, -20], 18]}                                                                                 // 1651
  ]);                                                                                                       // 1652
  // 'a.0' here ONLY means "first element of a", not "first element of something                            // 1653
  // found in a", so it CANNOT find the 10 or -5.                                                           // 1654
  verify({'a.0': 1}, [                                                                                      // 1655
    {a: [1, [10, 15], 20]},                                                                                 // 1656
    {a: [5, [-5, -20], 18]}                                                                                 // 1657
  ]);                                                                                                       // 1658
  verify({'a.0': -1}, [                                                                                     // 1659
    {a: [5, [-5, -20], 18]},                                                                                // 1660
    {a: [1, [10, 15], 20]}                                                                                  // 1661
  ]);                                                                                                       // 1662
  // Similarly, this is just comparing [-5,-20] to [10, 15].                                                // 1663
  verify({'a.1': 1}, [                                                                                      // 1664
    {a: [5, [-5, -20], 18]},                                                                                // 1665
    {a: [1, [10, 15], 20]}                                                                                  // 1666
  ]);                                                                                                       // 1667
  verify({'a.1': -1}, [                                                                                     // 1668
    {a: [1, [10, 15], 20]},                                                                                 // 1669
    {a: [5, [-5, -20], 18]}                                                                                 // 1670
  ]);                                                                                                       // 1671
  // Here we are just comparing [10,15] directly to [19,3] (and NOT also                                    // 1672
  // iterating over the numbers; this is implemented by setting dontIterate in                              // 1673
  // makeLookupFunction).  So [10,15]<[19,3] even though 3 is the smallest                                  // 1674
  // number you can find there.                                                                             // 1675
  verify({'a.1': 1}, [                                                                                      // 1676
    {a: [1, [10, 15], 20]},                                                                                 // 1677
    {a: [5, [19, 3], 18]}                                                                                   // 1678
  ]);                                                                                                       // 1679
  verify({'a.1': -1}, [                                                                                     // 1680
    {a: [5, [19, 3], 18]},                                                                                  // 1681
    {a: [1, [10, 15], 20]}                                                                                  // 1682
  ]);                                                                                                       // 1683
  // Minimal elements are 1 and 5.                                                                          // 1684
  verify({a: 1}, [                                                                                          // 1685
    {a: [1, [10, 15], 20]},                                                                                 // 1686
    {a: [5, [19, 3], 18]}                                                                                   // 1687
  ]);                                                                                                       // 1688
  // Maximal elements are [19,3] and [10,15] (because arrays sort higher than                               // 1689
  // numbers), even though there's a 20 floating around.                                                    // 1690
  verify({a: -1}, [                                                                                         // 1691
    {a: [5, [19, 3], 18]},                                                                                  // 1692
    {a: [1, [10, 15], 20]}                                                                                  // 1693
  ]);                                                                                                       // 1694
  // Maximal elements are [10,15] and [3,19].  [10,15] is bigger even though 19                             // 1695
  // is the biggest number in them, because array comparison is lexicographic.                              // 1696
  verify({a: -1}, [                                                                                         // 1697
    {a: [1, [10, 15], 20]},                                                                                 // 1698
    {a: [5, [3, 19], 18]}                                                                                   // 1699
  ]);                                                                                                       // 1700
                                                                                                            // 1701
  // (0,4) < (0,5), so they go in this order.  It's not correct to consider                                 // 1702
  // (0,3) as a sort key for the second document because they come from                                     // 1703
  // different a-branches.                                                                                  // 1704
  verify({'a.x': 1, 'a.y': 1}, [                                                                            // 1705
    {a: [{x: 0, y: 4}]},                                                                                    // 1706
    {a: [{x: 0, y: 5}, {x: 1, y: 3}]}                                                                       // 1707
  ]);                                                                                                       // 1708
});                                                                                                         // 1709
                                                                                                            // 1710
Tinytest.add("minimongo - sort", function (test) {                                                          // 1711
  var c = new LocalCollection();                                                                            // 1712
  for (var i = 0; i < 50; i++)                                                                              // 1713
    for (var j = 0; j < 2; j++)                                                                             // 1714
      c.insert({a: i, b: j, _id: i + "_" + j});                                                             // 1715
                                                                                                            // 1716
  test.equal(                                                                                               // 1717
    c.find({a: {$gt: 10}}, {sort: {b: -1, a: 1}, limit: 5}).fetch(), [                                      // 1718
      {a: 11, b: 1, _id: "11_1"},                                                                           // 1719
      {a: 12, b: 1, _id: "12_1"},                                                                           // 1720
      {a: 13, b: 1, _id: "13_1"},                                                                           // 1721
      {a: 14, b: 1, _id: "14_1"},                                                                           // 1722
      {a: 15, b: 1, _id: "15_1"}]);                                                                         // 1723
                                                                                                            // 1724
  test.equal(                                                                                               // 1725
    c.find({a: {$gt: 10}}, {sort: {b: -1, a: 1}, skip: 3, limit: 5}).fetch(), [                             // 1726
      {a: 14, b: 1, _id: "14_1"},                                                                           // 1727
      {a: 15, b: 1, _id: "15_1"},                                                                           // 1728
      {a: 16, b: 1, _id: "16_1"},                                                                           // 1729
      {a: 17, b: 1, _id: "17_1"},                                                                           // 1730
      {a: 18, b: 1, _id: "18_1"}]);                                                                         // 1731
                                                                                                            // 1732
  test.equal(                                                                                               // 1733
    c.find({a: {$gte: 20}}, {sort: {a: 1, b: -1}, skip: 50, limit: 5}).fetch(), [                           // 1734
      {a: 45, b: 1, _id: "45_1"},                                                                           // 1735
      {a: 45, b: 0, _id: "45_0"},                                                                           // 1736
      {a: 46, b: 1, _id: "46_1"},                                                                           // 1737
      {a: 46, b: 0, _id: "46_0"},                                                                           // 1738
      {a: 47, b: 1, _id: "47_1"}]);                                                                         // 1739
});                                                                                                         // 1740
                                                                                                            // 1741
Tinytest.add("minimongo - subkey sort", function (test) {                                                   // 1742
  var c = new LocalCollection();                                                                            // 1743
                                                                                                            // 1744
  // normal case                                                                                            // 1745
  c.insert({a: {b: 2}});                                                                                    // 1746
  c.insert({a: {b: 1}});                                                                                    // 1747
  c.insert({a: {b: 3}});                                                                                    // 1748
  test.equal(                                                                                               // 1749
    _.pluck(c.find({}, {sort: {'a.b': -1}}).fetch(), 'a'),                                                  // 1750
    [{b: 3}, {b: 2}, {b: 1}]);                                                                              // 1751
                                                                                                            // 1752
  // isn't an object                                                                                        // 1753
  c.insert({a: 1});                                                                                         // 1754
  test.equal(                                                                                               // 1755
    _.pluck(c.find({}, {sort: {'a.b': 1}}).fetch(), 'a'),                                                   // 1756
    [1, {b: 1}, {b: 2}, {b: 3}]);                                                                           // 1757
                                                                                                            // 1758
  // complex object                                                                                         // 1759
  c.insert({a: {b: {c: 1}}});                                                                               // 1760
  test.equal(                                                                                               // 1761
    _.pluck(c.find({}, {sort: {'a.b': -1}}).fetch(), 'a'),                                                  // 1762
    [{b: {c: 1}}, {b: 3}, {b: 2}, {b: 1}, 1]);                                                              // 1763
                                                                                                            // 1764
  // no such top level prop                                                                                 // 1765
  c.insert({c: 1});                                                                                         // 1766
  test.equal(                                                                                               // 1767
    _.pluck(c.find({}, {sort: {'a.b': -1}}).fetch(), 'a'),                                                  // 1768
    [{b: {c: 1}}, {b: 3}, {b: 2}, {b: 1}, 1, undefined]);                                                   // 1769
                                                                                                            // 1770
  // no such mid level prop. just test that it doesn't throw.                                               // 1771
  test.equal(c.find({}, {sort: {'a.nope.c': -1}}).count(), 6);                                              // 1772
});                                                                                                         // 1773
                                                                                                            // 1774
Tinytest.add("minimongo - array sort", function (test) {                                                    // 1775
  var c = new LocalCollection();                                                                            // 1776
                                                                                                            // 1777
  // "up" and "down" are the indices that the docs should have when sorted                                  // 1778
  // ascending and descending by "a.x" respectively. They are not reverses of                               // 1779
  // each other: when sorting ascending, you use the minimum value you can find                             // 1780
  // in the document, and when sorting descending, you use the maximum value you                            // 1781
  // can find. So [1, 4] shows up in the 1 slot when sorting ascending and the 4                            // 1782
  // slot when sorting descending.                                                                          // 1783
  //                                                                                                        // 1784
  // Similarly, "selected" is the index that the doc should have in the query                               // 1785
  // that sorts ascending on "a.x" and selects {'a.x': {$gt: 1}}. In this case,                             // 1786
  // the 1 in [1, 4] may not be used as a sort key.                                                         // 1787
  c.insert({up: 1, down: 1, selected: 2, a: {x: [1, 4]}});                                                  // 1788
  c.insert({up: 2, down: 2, selected: 0, a: [{x: [2]}, {x: 3}]});                                           // 1789
  c.insert({up: 0, down: 4,              a: {x: 0}});                                                       // 1790
  c.insert({up: 3, down: 3, selected: 1, a: {x: 2.5}});                                                     // 1791
  c.insert({up: 4, down: 0, selected: 3, a: {x: 5}});                                                       // 1792
                                                                                                            // 1793
  // Test that the the documents in "cursor" contain values with the name                                   // 1794
  // "field" running from 0 to the max value of that name in the collection.                                // 1795
  var testCursorMatchesField = function (cursor, field) {                                                   // 1796
    var fieldValues = [];                                                                                   // 1797
    c.find().forEach(function (doc) {                                                                       // 1798
      if (_.has(doc, field))                                                                                // 1799
        fieldValues.push(doc[field]);                                                                       // 1800
    });                                                                                                     // 1801
    test.equal(_.pluck(cursor.fetch(), field),                                                              // 1802
               _.range(_.max(fieldValues) + 1));                                                            // 1803
  };                                                                                                        // 1804
                                                                                                            // 1805
  testCursorMatchesField(c.find({}, {sort: {'a.x': 1}}), 'up');                                             // 1806
  testCursorMatchesField(c.find({}, {sort: {'a.x': -1}}), 'down');                                          // 1807
  testCursorMatchesField(c.find({'a.x': {$gt: 1}}, {sort: {'a.x': 1}}),                                     // 1808
                         'selected');                                                                       // 1809
});                                                                                                         // 1810
                                                                                                            // 1811
Tinytest.add("minimongo - sort keys", function (test) {                                                     // 1812
  var keyListToObject = function (keyList) {                                                                // 1813
    var obj = {};                                                                                           // 1814
    _.each(keyList, function (key) {                                                                        // 1815
      obj[EJSON.stringify(key)] = true;                                                                     // 1816
    });                                                                                                     // 1817
    return obj;                                                                                             // 1818
  };                                                                                                        // 1819
                                                                                                            // 1820
  var testKeys = function (sortSpec, doc, expectedKeyList) {                                                // 1821
    var expectedKeys = keyListToObject(expectedKeyList);                                                    // 1822
    var sorter = new Minimongo.Sorter(sortSpec);                                                            // 1823
                                                                                                            // 1824
    var actualKeyList = [];                                                                                 // 1825
    sorter._generateKeysFromDoc(doc, function (key) {                                                       // 1826
      actualKeyList.push(key);                                                                              // 1827
    });                                                                                                     // 1828
    var actualKeys = keyListToObject(actualKeyList);                                                        // 1829
    test.equal(actualKeys, expectedKeys);                                                                   // 1830
  };                                                                                                        // 1831
                                                                                                            // 1832
  var testParallelError = function (sortSpec, doc) {                                                        // 1833
    var sorter = new Minimongo.Sorter(sortSpec);                                                            // 1834
    test.throws(function () {                                                                               // 1835
      sorter._generateKeysFromDoc(doc, function (){});                                                      // 1836
    }, /parallel arrays/);                                                                                  // 1837
  };                                                                                                        // 1838
                                                                                                            // 1839
  // Just non-array fields.                                                                                 // 1840
  testKeys({'a.x': 1, 'a.y': 1},                                                                            // 1841
           {a: {x: 0, y: 5}},                                                                               // 1842
           [[0,5]]);                                                                                        // 1843
                                                                                                            // 1844
  // Ensure that we don't get [0,3] and [1,5].                                                              // 1845
  testKeys({'a.x': 1, 'a.y': 1},                                                                            // 1846
           {a: [{x: 0, y: 5}, {x: 1, y: 3}]},                                                               // 1847
           [[0,5], [1,3]]);                                                                                 // 1848
                                                                                                            // 1849
  // Ensure we can combine "array fields" with "non-array fields".                                          // 1850
  testKeys({'a.x': 1, 'a.y': 1, b: -1},                                                                     // 1851
           {a: [{x: 0, y: 5}, {x: 1, y: 3}], b: 42},                                                        // 1852
           [[0,5,42], [1,3,42]]);                                                                           // 1853
  testKeys({b: -1, 'a.x': 1, 'a.y': 1},                                                                     // 1854
           {a: [{x: 0, y: 5}, {x: 1, y: 3}], b: 42},                                                        // 1855
           [[42,0,5], [42,1,3]]);                                                                           // 1856
  testKeys({'a.x': 1, b: -1, 'a.y': 1},                                                                     // 1857
           {a: [{x: 0, y: 5}, {x: 1, y: 3}], b: 42},                                                        // 1858
           [[0,42,5], [1,42,3]]);                                                                           // 1859
  testKeys({a: 1, b: 1},                                                                                    // 1860
           {a: [1, 2, 3], b: 42},                                                                           // 1861
           [[1,42], [2,42], [3,42]]);                                                                       // 1862
                                                                                                            // 1863
  // Don't support multiple arrays at the same level.                                                       // 1864
  testParallelError({a: 1, b: 1},                                                                           // 1865
                    {a: [1, 2, 3], b: [42]});                                                               // 1866
                                                                                                            // 1867
  // We are MORE STRICT than Mongo here; Mongo supports this!                                               // 1868
  // XXX support this too  #NestedArraySort                                                                 // 1869
  testParallelError({'a.x': 1, 'a.y': 1},                                                                   // 1870
                    {a: [{x: 1, y: [2, 3]},                                                                 // 1871
                         {x: 2, y: [4, 5]}]});                                                              // 1872
});                                                                                                         // 1873
                                                                                                            // 1874
Tinytest.add("minimongo - sort key filter", function (test) {                                               // 1875
  var testOrder = function (sortSpec, selector, doc1, doc2) {                                               // 1876
    var matcher = new Minimongo.Matcher(selector);                                                          // 1877
    var sorter = new Minimongo.Sorter(sortSpec, {matcher: matcher});                                        // 1878
    var comparator = sorter.getComparator();                                                                // 1879
    var comparison = comparator(doc1, doc2);                                                                // 1880
    test.isTrue(comparison < 0);                                                                            // 1881
  };                                                                                                        // 1882
                                                                                                            // 1883
  testOrder({'a.x': 1}, {'a.x': {$gt: 1}},                                                                  // 1884
            {a: {x: 3}},                                                                                    // 1885
            {a: {x: [1, 4]}});                                                                              // 1886
  testOrder({'a.x': 1}, {'a.x': {$gt: 0}},                                                                  // 1887
            {a: {x: [1, 4]}},                                                                               // 1888
            {a: {x: 3}});                                                                                   // 1889
                                                                                                            // 1890
  var keyCompatible = function (sortSpec, selector, key, compatible) {                                      // 1891
    var matcher = new Minimongo.Matcher(selector);                                                          // 1892
    var sorter = new Minimongo.Sorter(sortSpec, {matcher: matcher});                                        // 1893
    var actual = sorter._keyCompatibleWithSelector(key);                                                    // 1894
    test.equal(actual, compatible);                                                                         // 1895
  };                                                                                                        // 1896
                                                                                                            // 1897
  keyCompatible({a: 1}, {a: 5}, [5], true);                                                                 // 1898
  keyCompatible({a: 1}, {a: 5}, [8], false);                                                                // 1899
  keyCompatible({a: 1}, {a: {x: 5}}, [{x: 5}], true);                                                       // 1900
  keyCompatible({a: 1}, {a: {x: 5}}, [{x: 5, y: 9}], false);                                                // 1901
  keyCompatible({'a.x': 1}, {a: {x: 5}}, [5], true);                                                        // 1902
  // To confirm this:                                                                                       // 1903
  //   > db.x.insert({_id: "q", a: [{x:1}, {x:5}], b: 2})                                                   // 1904
  //   > db.x.insert({_id: "w", a: [{x:5}, {x:10}], b: 1})                                                  // 1905
  //   > db.x.find({}).sort({'a.x': 1, b: 1})                                                               // 1906
  //   { "_id" : "q", "a" : [  {  "x" : 1 },  {  "x" : 5 } ], "b" : 2 }                                     // 1907
  //   { "_id" : "w", "a" : [  {  "x" : 5 },  {  "x" : 10 } ], "b" : 1 }                                    // 1908
  //   > db.x.find({a: {x:5}}).sort({'a.x': 1, b: 1})                                                       // 1909
  //   { "_id" : "q", "a" : [  {  "x" : 1 },  {  "x" : 5 } ], "b" : 2 }                                     // 1910
  //   { "_id" : "w", "a" : [  {  "x" : 5 },  {  "x" : 10 } ], "b" : 1 }                                    // 1911
  //   > db.x.find({'a.x': 5}).sort({'a.x': 1, b: 1})                                                       // 1912
  //   { "_id" : "w", "a" : [  {  "x" : 5 },  {  "x" : 10 } ], "b" : 1 }                                    // 1913
  //   { "_id" : "q", "a" : [  {  "x" : 1 },  {  "x" : 5 } ], "b" : 2 }                                     // 1914
  // ie, only the last one manages to trigger the key compatibility code,                                   // 1915
  // not the previous one.  (The "b" sort is necessary because when the key                                 // 1916
  // compatibility code *does* kick in, both documents only end up with "5"                                 // 1917
  // for the first field as their only sort key, and we need to differentiate                               // 1918
  // somehow...)                                                                                            // 1919
  keyCompatible({'a.x': 1}, {a: {x: 5}}, [1], true);                                                        // 1920
  keyCompatible({'a.x': 1}, {'a.x': 5}, [5], true);                                                         // 1921
  keyCompatible({'a.x': 1}, {'a.x': 5}, [1], false);                                                        // 1922
                                                                                                            // 1923
  // Regex key check.                                                                                       // 1924
  keyCompatible({a: 1}, {a: /^foo+/}, ['foo'], true);                                                       // 1925
  keyCompatible({a: 1}, {a: /^foo+/}, ['foooo'], true);                                                     // 1926
  keyCompatible({a: 1}, {a: /^foo+/}, ['foooobar'], true);                                                  // 1927
  keyCompatible({a: 1}, {a: /^foo+/}, ['afoooo'], false);                                                   // 1928
  keyCompatible({a: 1}, {a: /^foo+/}, [''], false);                                                         // 1929
  keyCompatible({a: 1}, {a: {$regex: "^foo+"}}, ['foo'], true);                                             // 1930
  keyCompatible({a: 1}, {a: {$regex: "^foo+"}}, ['foooo'], true);                                           // 1931
  keyCompatible({a: 1}, {a: {$regex: "^foo+"}}, ['foooobar'], true);                                        // 1932
  keyCompatible({a: 1}, {a: {$regex: "^foo+"}}, ['afoooo'], false);                                         // 1933
  keyCompatible({a: 1}, {a: {$regex: "^foo+"}}, [''], false);                                               // 1934
                                                                                                            // 1935
  keyCompatible({a: 1}, {a: /^foo+/i}, ['foo'], true);                                                      // 1936
  // Key compatibility check appears to be turned off for regexps with flags.                               // 1937
  keyCompatible({a: 1}, {a: /^foo+/i}, ['bar'], true);                                                      // 1938
  keyCompatible({a: 1}, {a: /^foo+/m}, ['bar'], true);                                                      // 1939
  keyCompatible({a: 1}, {a: {$regex: "^foo+", $options: "i"}}, ['bar'], true);                              // 1940
  keyCompatible({a: 1}, {a: {$regex: "^foo+", $options: "m"}}, ['bar'], true);                              // 1941
                                                                                                            // 1942
  // Multiple keys!                                                                                         // 1943
  keyCompatible({a: 1, b: 1, c: 1},                                                                         // 1944
                {a: {$gt: 5}, c: {$lt: 3}}, [6, "bla", 2], true);                                           // 1945
  keyCompatible({a: 1, b: 1, c: 1},                                                                         // 1946
                {a: {$gt: 5}, c: {$lt: 3}}, [6, "bla", 4], false);                                          // 1947
  keyCompatible({a: 1, b: 1, c: 1},                                                                         // 1948
                {a: {$gt: 5}, c: {$lt: 3}}, [3, "bla", 1], false);                                          // 1949
  // No filtering is done (ie, all keys are compatible) if the first key isn't                              // 1950
  // constrained.                                                                                           // 1951
  keyCompatible({a: 1, b: 1, c: 1},                                                                         // 1952
                {c: {$lt: 3}}, [3, "bla", 4], true);                                                        // 1953
});                                                                                                         // 1954
                                                                                                            // 1955
Tinytest.add("minimongo - binary search", function (test) {                                                 // 1956
  var forwardCmp = function (a, b) {                                                                        // 1957
    return a - b;                                                                                           // 1958
  };                                                                                                        // 1959
                                                                                                            // 1960
  var backwardCmp = function (a, b) {                                                                       // 1961
    return -1 * forwardCmp(a, b);                                                                           // 1962
  };                                                                                                        // 1963
                                                                                                            // 1964
  var checkSearch = function (cmp, array, value, expected, message) {                                       // 1965
    var actual = LocalCollection._binarySearch(cmp, array, value);                                          // 1966
    if (expected != actual) {                                                                               // 1967
      test.fail({type: "minimongo-binary-search",                                                           // 1968
                 message: message + " : Expected index " + expected +                                       // 1969
                 " but had " + actual                                                                       // 1970
      });                                                                                                   // 1971
    }                                                                                                       // 1972
  };                                                                                                        // 1973
                                                                                                            // 1974
  var checkSearchForward = function (array, value, expected, message) {                                     // 1975
    checkSearch(forwardCmp, array, value, expected, message);                                               // 1976
  };                                                                                                        // 1977
  var checkSearchBackward = function (array, value, expected, message) {                                    // 1978
    checkSearch(backwardCmp, array, value, expected, message);                                              // 1979
  };                                                                                                        // 1980
                                                                                                            // 1981
  checkSearchForward([1, 2, 5, 7], 4, 2, "Inner insert");                                                   // 1982
  checkSearchForward([1, 2, 3, 4], 3, 3, "Inner insert, equal value");                                      // 1983
  checkSearchForward([1, 2, 5], 4, 2, "Inner insert, odd length");                                          // 1984
  checkSearchForward([1, 3, 5, 6], 9, 4, "End insert");                                                     // 1985
  checkSearchForward([1, 3, 5, 6], 0, 0, "Beginning insert");                                               // 1986
  checkSearchForward([1], 0, 0, "Single array, less than.");                                                // 1987
  checkSearchForward([1], 1, 1, "Single array, equal.");                                                    // 1988
  checkSearchForward([1], 2, 1, "Single array, greater than.");                                             // 1989
  checkSearchForward([], 1, 0, "Empty array");                                                              // 1990
  checkSearchForward([1, 1, 1, 2, 2, 2, 2], 1, 3, "Highly degenerate array, lower");                        // 1991
  checkSearchForward([1, 1, 1, 2, 2, 2, 2], 2, 7, "Highly degenerate array, upper");                        // 1992
  checkSearchForward([2, 2, 2, 2, 2, 2, 2], 1, 0, "Highly degenerate array, lower");                        // 1993
  checkSearchForward([2, 2, 2, 2, 2, 2, 2], 2, 7, "Highly degenerate array, equal");                        // 1994
  checkSearchForward([2, 2, 2, 2, 2, 2, 2], 3, 7, "Highly degenerate array, upper");                        // 1995
                                                                                                            // 1996
  checkSearchBackward([7, 5, 2, 1], 4, 2, "Backward: Inner insert");                                        // 1997
  checkSearchBackward([4, 3, 2, 1], 3, 2, "Backward: Inner insert, equal value");                           // 1998
  checkSearchBackward([5, 2, 1], 4, 1, "Backward: Inner insert, odd length");                               // 1999
  checkSearchBackward([6, 5, 3, 1], 9, 0, "Backward: Beginning insert");                                    // 2000
  checkSearchBackward([6, 5, 3, 1], 0, 4, "Backward: End insert");                                          // 2001
  checkSearchBackward([1], 0, 1, "Backward: Single array, less than.");                                     // 2002
  checkSearchBackward([1], 1, 1, "Backward: Single array, equal.");                                         // 2003
  checkSearchBackward([1], 2, 0, "Backward: Single array, greater than.");                                  // 2004
  checkSearchBackward([], 1, 0, "Backward: Empty array");                                                   // 2005
  checkSearchBackward([2, 2, 2, 2, 1, 1, 1], 1, 7, "Backward: Degenerate array, lower");                    // 2006
  checkSearchBackward([2, 2, 2, 2, 1, 1, 1], 2, 4, "Backward: Degenerate array, upper");                    // 2007
  checkSearchBackward([2, 2, 2, 2, 2, 2, 2], 1, 7, "Backward: Highly degenerate array, upper");             // 2008
  checkSearchBackward([2, 2, 2, 2, 2, 2, 2], 2, 7, "Backward: Highly degenerate array, upper");             // 2009
  checkSearchBackward([2, 2, 2, 2, 2, 2, 2], 3, 0, "Backward: Highly degenerate array, upper");             // 2010
});                                                                                                         // 2011
                                                                                                            // 2012
Tinytest.add("minimongo - modify", function (test) {                                                        // 2013
  var modifyWithQuery = function (doc, query, mod, expected) {                                              // 2014
    var coll = new LocalCollection;                                                                         // 2015
    coll.insert(doc);                                                                                       // 2016
    // The query is relevant for 'a.$.b'.                                                                   // 2017
    coll.update(query, mod);                                                                                // 2018
    var actual = coll.findOne();                                                                            // 2019
    delete actual._id;  // added by insert                                                                  // 2020
    test.equal(actual, expected, EJSON.stringify({input: doc, mod: mod}));                                  // 2021
  };                                                                                                        // 2022
  var modify = function (doc, mod, expected) {                                                              // 2023
    modifyWithQuery(doc, {}, mod, expected);                                                                // 2024
  };                                                                                                        // 2025
  var exceptionWithQuery = function (doc, query, mod) {                                                     // 2026
    var coll = new LocalCollection;                                                                         // 2027
    coll.insert(doc);                                                                                       // 2028
    test.throws(function () {                                                                               // 2029
      coll.update(query, mod);                                                                              // 2030
    });                                                                                                     // 2031
  };                                                                                                        // 2032
  var exception = function (doc, mod) {                                                                     // 2033
    exceptionWithQuery(doc, {}, mod);                                                                       // 2034
  };                                                                                                        // 2035
                                                                                                            // 2036
  // document replacement                                                                                   // 2037
  modify({}, {}, {});                                                                                       // 2038
  modify({a: 12}, {}, {}); // tested against mongodb                                                        // 2039
  modify({a: 12}, {a: 13}, {a:13});                                                                         // 2040
  modify({a: 12, b: 99}, {a: 13}, {a:13});                                                                  // 2041
  exception({a: 12}, {a: 13, $set: {b: 13}});                                                               // 2042
  exception({a: 12}, {$set: {b: 13}, a: 13});                                                               // 2043
                                                                                                            // 2044
  // keys                                                                                                   // 2045
  modify({}, {$set: {'a': 12}}, {a: 12});                                                                   // 2046
  modify({}, {$set: {'a.b': 12}}, {a: {b: 12}});                                                            // 2047
  modify({}, {$set: {'a.b.c': 12}}, {a: {b: {c: 12}}});                                                     // 2048
  modify({a: {d: 99}}, {$set: {'a.b.c': 12}}, {a: {d: 99, b: {c: 12}}});                                    // 2049
  modify({}, {$set: {'a.b.3.c': 12}}, {a: {b: {3: {c: 12}}}});                                              // 2050
  modify({a: {b: []}}, {$set: {'a.b.3.c': 12}}, {                                                           // 2051
    a: {b: [null, null, null, {c: 12}]}});                                                                  // 2052
  exception({a: [null, null, null]}, {$set: {'a.1.b': 12}});                                                // 2053
  exception({a: [null, 1, null]}, {$set: {'a.1.b': 12}});                                                   // 2054
  exception({a: [null, "x", null]}, {$set: {'a.1.b': 12}});                                                 // 2055
  exception({a: [null, [], null]}, {$set: {'a.1.b': 12}});                                                  // 2056
  modify({a: [null, null, null]}, {$set: {'a.3.b': 12}}, {                                                  // 2057
    a: [null, null, null, {b: 12}]});                                                                       // 2058
  exception({a: []}, {$set: {'a.b': 12}});                                                                  // 2059
  exception({a: 12}, {$set: {'a.b': 99}}); // tested on mongo                                               // 2060
  exception({a: 'x'}, {$set: {'a.b': 99}});                                                                 // 2061
  exception({a: true}, {$set: {'a.b': 99}});                                                                // 2062
  exception({a: null}, {$set: {'a.b': 99}});                                                                // 2063
  modify({a: {}}, {$set: {'a.3': 12}}, {a: {'3': 12}});                                                     // 2064
  modify({a: []}, {$set: {'a.3': 12}}, {a: [null, null, null, 12]});                                        // 2065
  modify({}, {$set: {'': 12}}, {'': 12}); // tested on mongo                                                // 2066
  exception({}, {$set: {'.': 12}}); // tested on mongo                                                      // 2067
  modify({}, {$set: {'. ': 12}}, {'': {' ': 12}}); // tested on mongo                                       // 2068
  modify({}, {$inc: {'... ': 12}}, {'': {'': {'': {' ': 12}}}}); // tested                                  // 2069
  modify({}, {$set: {'a..b': 12}}, {a: {'': {b: 12}}});                                                     // 2070
  modify({a: [1,2,3]}, {$set: {'a.01': 99}}, {a: [1, 99, 3]});                                              // 2071
  modify({a: [1,{a: 98},3]}, {$set: {'a.01.b': 99}}, {a: [1,{a:98, b: 99},3]});                             // 2072
  modify({}, {$set: {'2.a.b': 12}}, {'2': {'a': {'b': 12}}}); // tested                                     // 2073
  modify({x: []}, {$set: {'x.2..a': 99}}, {x: [null, null, {'': {a: 99}}]});                                // 2074
  modify({x: [null, null]}, {$set: {'x.2.a': 1}}, {x: [null, null, {a: 1}]});                               // 2075
  exception({x: [null, null]}, {$set: {'x.1.a': 1}});                                                       // 2076
                                                                                                            // 2077
  // a.$.b                                                                                                  // 2078
  modifyWithQuery({a: [{x: 2}, {x: 4}]}, {'a.x': 4}, {$set: {'a.$.z': 9}},                                  // 2079
                  {a: [{x: 2}, {x: 4, z: 9}]});                                                             // 2080
  exception({a: [{x: 2}, {x: 4}]}, {$set: {'a.$.z': 9}});                                                   // 2081
  exceptionWithQuery({a: [{x: 2}, {x: 4}], b: 5}, {b: 5}, {$set: {'a.$.z': 9}});                            // 2082
  // can't have two $                                                                                       // 2083
  exceptionWithQuery({a: [{x: [2]}]}, {'a.x': 2}, {$set: {'a.$.x.$': 9}});                                  // 2084
  modifyWithQuery({a: [5, 6, 7]}, {a: 6}, {$set: {'a.$': 9}}, {a: [5, 9, 7]});                              // 2085
  modifyWithQuery({a: [{b: [{c: 9}, {c: 10}]}, {b: {c: 11}}]}, {'a.b.c': 10},                               // 2086
                  {$unset: {'a.$.b': 1}}, {a: [{}, {b: {c: 11}}]});                                         // 2087
  modifyWithQuery({a: [{b: [{c: 9}, {c: 10}]}, {b: {c: 11}}]}, {'a.b.c': 11},                               // 2088
                  {$unset: {'a.$.b': 1}},                                                                   // 2089
                  {a: [{b: [{c: 9}, {c: 10}]}, {}]});                                                       // 2090
  modifyWithQuery({a: [1]}, {'a.0': 1}, {$set: {'a.$': 5}}, {a: [5]});                                      // 2091
  modifyWithQuery({a: [9]}, {a: {$mod: [2, 1]}}, {$set: {'a.$': 5}}, {a: [5]});                             // 2092
  // Negatives don't set '$'.                                                                               // 2093
  exceptionWithQuery({a: [1]}, {$not: {a: 2}}, {$set: {'a.$': 5}});                                         // 2094
  exceptionWithQuery({a: [1]}, {'a.0': {$ne: 2}}, {$set: {'a.$': 5}});                                      // 2095
  // One $or clause works.                                                                                  // 2096
  modifyWithQuery({a: [{x: 2}, {x: 4}]},                                                                    // 2097
                  {$or: [{'a.x': 4}]}, {$set: {'a.$.z': 9}},                                                // 2098
                  {a: [{x: 2}, {x: 4, z: 9}]});                                                             // 2099
  // More $or clauses throw.                                                                                // 2100
  exceptionWithQuery({a: [{x: 2}, {x: 4}]},                                                                 // 2101
                     {$or: [{'a.x': 4}, {'a.x': 4}]},                                                       // 2102
                     {$set: {'a.$.z': 9}});                                                                 // 2103
  // $and uses the last one.                                                                                // 2104
  modifyWithQuery({a: [{x: 1}, {x: 3}]},                                                                    // 2105
                  {$and: [{'a.x': 1}, {'a.x': 3}]},                                                         // 2106
                  {$set: {'a.$.x': 5}},                                                                     // 2107
                  {a: [{x: 1}, {x: 5}]});                                                                   // 2108
  modifyWithQuery({a: [{x: 1}, {x: 3}]},                                                                    // 2109
                  {$and: [{'a.x': 3}, {'a.x': 1}]},                                                         // 2110
                  {$set: {'a.$.x': 5}},                                                                     // 2111
                  {a: [{x: 5}, {x: 3}]});                                                                   // 2112
  // Same goes for the implicit AND of a document selector.                                                 // 2113
  modifyWithQuery({a: [{x: 1}, {y: 3}]},                                                                    // 2114
                  {'a.x': 1, 'a.y': 3},                                                                     // 2115
                  {$set: {'a.$.z': 5}},                                                                     // 2116
                  {a: [{x: 1}, {y: 3, z: 5}]});                                                             // 2117
  // with $near, make sure it finds the closest one                                                         // 2118
  modifyWithQuery({a: [{b: [1,1]},                                                                          // 2119
                       {b: [ [3,3], [4,4] ]},                                                               // 2120
                       {b: [9,9]}]},                                                                        // 2121
                  {'a.b': {$near: [5, 5]}},                                                                 // 2122
                  {$set: {'a.$.b': 'k'}},                                                                   // 2123
                  {a: [{b: [1,1]}, {b: 'k'}, {b: [9,9]}]});                                                 // 2124
  modifyWithQuery({a: [{x: 1}, {y: 1}, {x: 1, y: 1}]},                                                      // 2125
                  {a: {$elemMatch: {x: 1, y: 1}}},                                                          // 2126
                  {$set: {'a.$.x': 2}},                                                                     // 2127
                  {a: [{x: 1}, {y: 1}, {x: 2, y: 1}]});                                                     // 2128
  modifyWithQuery({a: [{b: [{x: 1}, {y: 1}, {x: 1, y: 1}]}]},                                               // 2129
                  {'a.b': {$elemMatch: {x: 1, y: 1}}},                                                      // 2130
                  {$set: {'a.$.b': 3}},                                                                     // 2131
                  {a: [{b: 3}]});                                                                           // 2132
                                                                                                            // 2133
  // $inc                                                                                                   // 2134
  modify({a: 1, b: 2}, {$inc: {a: 10}}, {a: 11, b: 2});                                                     // 2135
  modify({a: 1, b: 2}, {$inc: {c: 10}}, {a: 1, b: 2, c: 10});                                               // 2136
  exception({a: 1}, {$inc: {a: '10'}});                                                                     // 2137
  exception({a: 1}, {$inc: {a: true}});                                                                     // 2138
  exception({a: 1}, {$inc: {a: [10]}});                                                                     // 2139
  exception({a: '1'}, {$inc: {a: 10}});                                                                     // 2140
  exception({a: [1]}, {$inc: {a: 10}});                                                                     // 2141
  exception({a: {}}, {$inc: {a: 10}});                                                                      // 2142
  exception({a: false}, {$inc: {a: 10}});                                                                   // 2143
  exception({a: null}, {$inc: {a: 10}});                                                                    // 2144
  modify({a: [1, 2]}, {$inc: {'a.1': 10}}, {a: [1, 12]});                                                   // 2145
  modify({a: [1, 2]}, {$inc: {'a.2': 10}}, {a: [1, 2, 10]});                                                // 2146
  modify({a: [1, 2]}, {$inc: {'a.3': 10}}, {a: [1, 2, null, 10]});                                          // 2147
  modify({a: {b: 2}}, {$inc: {'a.b': 10}}, {a: {b: 12}});                                                   // 2148
  modify({a: {b: 2}}, {$inc: {'a.c': 10}}, {a: {b: 2, c: 10}});                                             // 2149
  exception({}, {$inc: {_id: 1}});                                                                          // 2150
                                                                                                            // 2151
  // $set                                                                                                   // 2152
  modify({a: 1, b: 2}, {$set: {a: 10}}, {a: 10, b: 2});                                                     // 2153
  modify({a: 1, b: 2}, {$set: {c: 10}}, {a: 1, b: 2, c: 10});                                               // 2154
  modify({a: 1, b: 2}, {$set: {a: {c: 10}}}, {a: {c: 10}, b: 2});                                           // 2155
  modify({a: [1, 2], b: 2}, {$set: {a: [3, 4]}}, {a: [3, 4], b: 2});                                        // 2156
  modify({a: [1, 2, 3], b: 2}, {$set: {'a.1': [3, 4]}},                                                     // 2157
         {a: [1, [3, 4], 3], b:2});                                                                         // 2158
  modify({a: [1], b: 2}, {$set: {'a.1': 9}}, {a: [1, 9], b: 2});                                            // 2159
  modify({a: [1], b: 2}, {$set: {'a.2': 9}}, {a: [1, null, 9], b: 2});                                      // 2160
  modify({a: {b: 1}}, {$set: {'a.c': 9}}, {a: {b: 1, c: 9}});                                               // 2161
  modify({}, {$set: {'x._id': 4}}, {x: {_id: 4}});                                                          // 2162
  exception({}, {$set: {_id: 4}});                                                                          // 2163
  exception({_id: 4}, {$set: {_id: 4}});  // even not-changing _id is bad                                   // 2164
                                                                                                            // 2165
  // $unset                                                                                                 // 2166
  modify({}, {$unset: {a: 1}}, {});                                                                         // 2167
  modify({a: 1}, {$unset: {a: 1}}, {});                                                                     // 2168
  modify({a: 1, b: 2}, {$unset: {a: 1}}, {b: 2});                                                           // 2169
  modify({a: 1, b: 2}, {$unset: {a: 0}}, {b: 2});                                                           // 2170
  modify({a: 1, b: 2}, {$unset: {a: false}}, {b: 2});                                                       // 2171
  modify({a: 1, b: 2}, {$unset: {a: null}}, {b: 2});                                                        // 2172
  modify({a: 1, b: 2}, {$unset: {a: [1]}}, {b: 2});                                                         // 2173
  modify({a: 1, b: 2}, {$unset: {a: {}}}, {b: 2});                                                          // 2174
  modify({a: {b: 2, c: 3}}, {$unset: {'a.b': 1}}, {a: {c: 3}});                                             // 2175
  modify({a: [1, 2, 3]}, {$unset: {'a.1': 1}}, {a: [1, null, 3]}); // tested                                // 2176
  modify({a: [1, 2, 3]}, {$unset: {'a.2': 1}}, {a: [1, 2, null]}); // tested                                // 2177
  modify({a: [1, 2, 3]}, {$unset: {'a.x': 1}}, {a: [1, 2, 3]}); // tested                                   // 2178
  modify({a: {b: 1}}, {$unset: {'a.b.c.d': 1}}, {a: {b: 1}});                                               // 2179
  modify({a: {b: 1}}, {$unset: {'a.x.c.d': 1}}, {a: {b: 1}});                                               // 2180
  modify({a: {b: {c: 1}}}, {$unset: {'a.b.c': 1}}, {a: {b: {}}});                                           // 2181
  exception({}, {$unset: {_id: 1}});                                                                        // 2182
                                                                                                            // 2183
  // $push                                                                                                  // 2184
  modify({}, {$push: {a: 1}}, {a: [1]});                                                                    // 2185
  modify({a: []}, {$push: {a: 1}}, {a: [1]});                                                               // 2186
  modify({a: [1]}, {$push: {a: 2}}, {a: [1, 2]});                                                           // 2187
  exception({a: true}, {$push: {a: 1}});                                                                    // 2188
  modify({a: [1]}, {$push: {a: [2]}}, {a: [1, [2]]});                                                       // 2189
  modify({a: []}, {$push: {'a.1': 99}}, {a: [null, [99]]}); // tested                                       // 2190
  modify({a: {}}, {$push: {'a.x': 99}}, {a: {x: [99]}});                                                    // 2191
  modify({}, {$push: {a: {$each: [1, 2, 3]}}},                                                              // 2192
         {a: [1, 2, 3]});                                                                                   // 2193
  modify({a: []}, {$push: {a: {$each: [1, 2, 3]}}},                                                         // 2194
         {a: [1, 2, 3]});                                                                                   // 2195
  modify({a: [true]}, {$push: {a: {$each: [1, 2, 3]}}},                                                     // 2196
         {a: [true, 1, 2, 3]});                                                                             // 2197
  // No positive numbers for $slice                                                                         // 2198
  exception({}, {$push: {a: {$each: [], $slice: 5}}});                                                      // 2199
  modify({a: [true]}, {$push: {a: {$each: [1, 2, 3], $slice: -2}}},                                         // 2200
         {a: [2, 3]});                                                                                      // 2201
  modify({a: [false, true]}, {$push: {a: {$each: [1], $slice: -2}}},                                        // 2202
         {a: [true, 1]});                                                                                   // 2203
  modify(                                                                                                   // 2204
    {a: [{x: 3}, {x: 1}]},                                                                                  // 2205
    {$push: {a: {                                                                                           // 2206
      $each: [{x: 4}, {x: 2}],                                                                              // 2207
      $slice: -2,                                                                                           // 2208
      $sort: {x: 1}                                                                                         // 2209
    }}},                                                                                                    // 2210
    {a: [{x: 3}, {x: 4}]});                                                                                 // 2211
  modify({}, {$push: {a: {$each: [1, 2, 3], $slice: 0}}}, {a: []});                                         // 2212
  modify({a: [1, 2]}, {$push: {a: {$each: [1, 2, 3], $slice: 0}}}, {a: []});                                // 2213
                                                                                                            // 2214
  // $pushAll                                                                                               // 2215
  modify({}, {$pushAll: {a: [1]}}, {a: [1]});                                                               // 2216
  modify({a: []}, {$pushAll: {a: [1]}}, {a: [1]});                                                          // 2217
  modify({a: [1]}, {$pushAll: {a: [2]}}, {a: [1, 2]});                                                      // 2218
  modify({}, {$pushAll: {a: [1, 2]}}, {a: [1, 2]});                                                         // 2219
  modify({a: []}, {$pushAll: {a: [1, 2]}}, {a: [1, 2]});                                                    // 2220
  modify({a: [1]}, {$pushAll: {a: [2, 3]}}, {a: [1, 2, 3]});                                                // 2221
  modify({}, {$pushAll: {a: []}}, {a: []});                                                                 // 2222
  modify({a: []}, {$pushAll: {a: []}}, {a: []});                                                            // 2223
  modify({a: [1]}, {$pushAll: {a: []}}, {a: [1]});                                                          // 2224
  exception({a: true}, {$pushAll: {a: [1]}});                                                               // 2225
  exception({a: []}, {$pushAll: {a: 1}});                                                                   // 2226
  modify({a: []}, {$pushAll: {'a.1': [99]}}, {a: [null, [99]]});                                            // 2227
  modify({a: []}, {$pushAll: {'a.1': []}}, {a: [null, []]});                                                // 2228
  modify({a: {}}, {$pushAll: {'a.x': [99]}}, {a: {x: [99]}});                                               // 2229
  modify({a: {}}, {$pushAll: {'a.x': []}}, {a: {x: []}});                                                   // 2230
                                                                                                            // 2231
  // $addToSet                                                                                              // 2232
  modify({}, {$addToSet: {a: 1}}, {a: [1]});                                                                // 2233
  modify({a: []}, {$addToSet: {a: 1}}, {a: [1]});                                                           // 2234
  modify({a: [1]}, {$addToSet: {a: 2}}, {a: [1, 2]});                                                       // 2235
  modify({a: [1, 2]}, {$addToSet: {a: 1}}, {a: [1, 2]});                                                    // 2236
  modify({a: [1, 2]}, {$addToSet: {a: 2}}, {a: [1, 2]});                                                    // 2237
  modify({a: [1, 2]}, {$addToSet: {a: 3}}, {a: [1, 2, 3]});                                                 // 2238
  exception({a: true}, {$addToSet: {a: 1}});                                                                // 2239
  modify({a: [1]}, {$addToSet: {a: [2]}}, {a: [1, [2]]});                                                   // 2240
  modify({}, {$addToSet: {a: {x: 1}}}, {a: [{x: 1}]});                                                      // 2241
  modify({a: [{x: 1}]}, {$addToSet: {a: {x: 1}}}, {a: [{x: 1}]});                                           // 2242
  modify({a: [{x: 1}]}, {$addToSet: {a: {x: 2}}}, {a: [{x: 1}, {x: 2}]});                                   // 2243
  modify({a: [{x: 1, y: 2}]}, {$addToSet: {a: {x: 1, y: 2}}},                                               // 2244
         {a: [{x: 1, y: 2}]});                                                                              // 2245
  modify({a: [{x: 1, y: 2}]}, {$addToSet: {a: {y: 2, x: 1}}},                                               // 2246
         {a: [{x: 1, y: 2}, {y: 2, x: 1}]});                                                                // 2247
  modify({a: [1, 2]}, {$addToSet: {a: {$each: [3, 1, 4]}}}, {a: [1, 2, 3, 4]});                             // 2248
  modify({a: [1, 2]}, {$addToSet: {a: {$each: [3, 1, 4], b: 12}}},                                          // 2249
         {a: [1, 2, 3, 4]}); // tested                                                                      // 2250
  modify({a: [1, 2]}, {$addToSet: {a: {b: 12, $each: [3, 1, 4]}}},                                          // 2251
         {a: [1, 2, {b: 12, $each: [3, 1, 4]}]}); // tested                                                 // 2252
  modify({a: []}, {$addToSet: {'a.1': 99}}, {a: [null, [99]]});                                             // 2253
  modify({a: {}}, {$addToSet: {'a.x': 99}}, {a: {x: [99]}});                                                // 2254
                                                                                                            // 2255
  // $pop                                                                                                   // 2256
  modify({}, {$pop: {a: 1}}, {}); // tested                                                                 // 2257
  modify({}, {$pop: {a: -1}}, {}); // tested                                                                // 2258
  modify({a: []}, {$pop: {a: 1}}, {a: []});                                                                 // 2259
  modify({a: []}, {$pop: {a: -1}}, {a: []});                                                                // 2260
  modify({a: [1, 2, 3]}, {$pop: {a: 1}}, {a: [1, 2]});                                                      // 2261
  modify({a: [1, 2, 3]}, {$pop: {a: 10}}, {a: [1, 2]});                                                     // 2262
  modify({a: [1, 2, 3]}, {$pop: {a: .001}}, {a: [1, 2]});                                                   // 2263
  modify({a: [1, 2, 3]}, {$pop: {a: 0}}, {a: [1, 2]});                                                      // 2264
  modify({a: [1, 2, 3]}, {$pop: {a: "stuff"}}, {a: [1, 2]});                                                // 2265
  modify({a: [1, 2, 3]}, {$pop: {a: -1}}, {a: [2, 3]});                                                     // 2266
  modify({a: [1, 2, 3]}, {$pop: {a: -10}}, {a: [2, 3]});                                                    // 2267
  modify({a: [1, 2, 3]}, {$pop: {a: -.001}}, {a: [2, 3]});                                                  // 2268
  exception({a: true}, {$pop: {a: 1}});                                                                     // 2269
  exception({a: true}, {$pop: {a: -1}});                                                                    // 2270
  modify({a: []}, {$pop: {'a.1': 1}}, {a: []}); // tested                                                   // 2271
  modify({a: [1, [2, 3], 4]}, {$pop: {'a.1': 1}}, {a: [1, [2], 4]});                                        // 2272
  modify({a: {}}, {$pop: {'a.x': 1}}, {a: {}}); // tested                                                   // 2273
  modify({a: {x: [2, 3]}}, {$pop: {'a.x': 1}}, {a: {x: [2]}});                                              // 2274
                                                                                                            // 2275
  // $pull                                                                                                  // 2276
  modify({}, {$pull: {a: 1}}, {});                                                                          // 2277
  modify({}, {$pull: {'a.x': 1}}, {});                                                                      // 2278
  modify({a: {}}, {$pull: {'a.x': 1}}, {a: {}});                                                            // 2279
  exception({a: true}, {$pull: {a: 1}});                                                                    // 2280
  modify({a: [2, 1, 2]}, {$pull: {a: 1}}, {a: [2, 2]});                                                     // 2281
  modify({a: [2, 1, 2]}, {$pull: {a: 2}}, {a: [1]});                                                        // 2282
  modify({a: [2, 1, 2]}, {$pull: {a: 3}}, {a: [2, 1, 2]});                                                  // 2283
  modify({a: []}, {$pull: {a: 3}}, {a: []});                                                                // 2284
  modify({a: [[2], [2, 1], [3]]}, {$pull: {a: [2, 1]}},                                                     // 2285
         {a: [[2], [3]]}); // tested                                                                        // 2286
  modify({a: [{b: 1, c: 2}, {b: 2, c: 2}]}, {$pull: {a: {b: 1}}},                                           // 2287
         {a: [{b: 2, c: 2}]});                                                                              // 2288
  modify({a: [{b: 1, c: 2}, {b: 2, c: 2}]}, {$pull: {a: {c: 2}}},                                           // 2289
         {a: []});                                                                                          // 2290
  // XXX implement this functionality!                                                                      // 2291
  // probably same refactoring as $elemMatch?                                                               // 2292
  // modify({a: [1, 2, 3, 4]}, {$pull: {$gt: 2}}, {a: [1,2]}); fails!                                       // 2293
                                                                                                            // 2294
  // $pullAll                                                                                               // 2295
  modify({}, {$pullAll: {a: [1]}}, {});                                                                     // 2296
  modify({a: [1, 2, 3]}, {$pullAll: {a: []}}, {a: [1, 2, 3]});                                              // 2297
  modify({a: [1, 2, 3]}, {$pullAll: {a: [2]}}, {a: [1, 3]});                                                // 2298
  modify({a: [1, 2, 3]}, {$pullAll: {a: [2, 1]}}, {a: [3]});                                                // 2299
  modify({a: [1, 2, 3]}, {$pullAll: {a: [1, 2]}}, {a: [3]});                                                // 2300
  modify({}, {$pullAll: {'a.b.c': [2]}}, {});                                                               // 2301
  exception({a: true}, {$pullAll: {a: [1]}});                                                               // 2302
  exception({a: [1, 2, 3]}, {$pullAll: {a: 1}});                                                            // 2303
  modify({x: [{a: 1}, {a: 1, b: 2}]}, {$pullAll: {x: [{a: 1}]}},                                            // 2304
         {x: [{a: 1, b: 2}]});                                                                              // 2305
                                                                                                            // 2306
  // $rename                                                                                                // 2307
  modify({}, {$rename: {a: 'b'}}, {});                                                                      // 2308
  modify({a: [12]}, {$rename: {a: 'b'}}, {b: [12]});                                                        // 2309
  modify({a: {b: 12}}, {$rename: {a: 'c'}}, {c: {b: 12}});                                                  // 2310
  modify({a: {b: 12}}, {$rename: {'a.b': 'a.c'}}, {a: {c: 12}});                                            // 2311
  modify({a: {b: 12}}, {$rename: {'a.b': 'x'}}, {a: {}, x: 12}); // tested                                  // 2312
  modify({a: {b: 12}}, {$rename: {'a.b': 'q.r'}}, {a: {}, q: {r: 12}});                                     // 2313
  modify({a: {b: 12}}, {$rename: {'a.b': 'q.2.r'}}, {a: {}, q: {2: {r: 12}}});                              // 2314
  modify({a: {b: 12}, q: {}}, {$rename: {'a.b': 'q.2.r'}},                                                  // 2315
         {a: {}, q: {2: {r: 12}}});                                                                         // 2316
  exception({a: {b: 12}, q: []}, {$rename: {'a.b': 'q.2'}}); // tested                                      // 2317
  exception({a: {b: 12}, q: []}, {$rename: {'a.b': 'q.2.r'}}); // tested                                    // 2318
  // These strange MongoDB behaviors throw.                                                                 // 2319
  // modify({a: {b: 12}, q: []}, {$rename: {'q.1': 'x'}},                                                   // 2320
  //        {a: {b: 12}, x: []}); // tested                                                                 // 2321
  // modify({a: {b: 12}, q: []}, {$rename: {'q.1.j': 'x'}},                                                 // 2322
  //        {a: {b: 12}, x: []}); // tested                                                                 // 2323
  exception({}, {$rename: {'a': 'a'}});                                                                     // 2324
  exception({}, {$rename: {'a.b': 'a.b'}});                                                                 // 2325
  modify({a: 12, b: 13}, {$rename: {a: 'b'}}, {b: 12});                                                     // 2326
                                                                                                            // 2327
  // $bit                                                                                                   // 2328
  // unimplemented                                                                                          // 2329
                                                                                                            // 2330
  // XXX test case sensitivity of modops                                                                    // 2331
  // XXX for each (most) modop, test that it performs a deep copy                                           // 2332
});                                                                                                         // 2333
                                                                                                            // 2334
// XXX test update() (selecting docs, multi, upsert..)                                                      // 2335
                                                                                                            // 2336
Tinytest.add("minimongo - observe ordered", function (test) {                                               // 2337
  var operations = [];                                                                                      // 2338
  var cbs = log_callbacks(operations);                                                                      // 2339
  var handle;                                                                                               // 2340
                                                                                                            // 2341
  var c = new LocalCollection();                                                                            // 2342
  handle = c.find({}, {sort: {a: 1}}).observe(cbs);                                                         // 2343
  test.isTrue(handle.collection === c);                                                                     // 2344
                                                                                                            // 2345
  c.insert({_id: 'foo', a:1});                                                                              // 2346
  test.equal(operations.shift(), ['added', {a:1}, 0, null]);                                                // 2347
  c.update({a:1}, {$set: {a: 2}});                                                                          // 2348
  test.equal(operations.shift(), ['changed', {a:2}, 0, {a:1}]);                                             // 2349
  c.insert({a:10});                                                                                         // 2350
  test.equal(operations.shift(), ['added', {a:10}, 1, null]);                                               // 2351
  c.update({}, {$inc: {a: 1}}, {multi: true});                                                              // 2352
  test.equal(operations.shift(), ['changed', {a:3}, 0, {a:2}]);                                             // 2353
  test.equal(operations.shift(), ['changed', {a:11}, 1, {a:10}]);                                           // 2354
  c.update({a:11}, {a:1});                                                                                  // 2355
  test.equal(operations.shift(), ['changed', {a:1}, 1, {a:11}]);                                            // 2356
  test.equal(operations.shift(), ['moved', {a:1}, 1, 0, 'foo']);                                            // 2357
  c.remove({a:2});                                                                                          // 2358
  test.equal(operations.shift(), undefined);                                                                // 2359
  c.remove({a:3});                                                                                          // 2360
  test.equal(operations.shift(), ['removed', 'foo', 1, {a:3}]);                                             // 2361
                                                                                                            // 2362
  // test stop                                                                                              // 2363
  handle.stop();                                                                                            // 2364
  var idA2 = Random.id();                                                                                   // 2365
  c.insert({_id: idA2, a:2});                                                                               // 2366
  test.equal(operations.shift(), undefined);                                                                // 2367
                                                                                                            // 2368
  // test initial inserts (and backwards sort)                                                              // 2369
  handle = c.find({}, {sort: {a: -1}}).observe(cbs);                                                        // 2370
  test.equal(operations.shift(), ['added', {a:2}, 0, null]);                                                // 2371
  test.equal(operations.shift(), ['added', {a:1}, 1, null]);                                                // 2372
  handle.stop();                                                                                            // 2373
                                                                                                            // 2374
  // test _suppress_initial                                                                                 // 2375
  handle = c.find({}, {sort: {a: -1}}).observe(_.extend({                                                   // 2376
    _suppress_initial: true}, cbs));                                                                        // 2377
  test.equal(operations.shift(), undefined);                                                                // 2378
  c.insert({a:100});                                                                                        // 2379
  test.equal(operations.shift(), ['added', {a:100}, 0, idA2]);                                              // 2380
  handle.stop();                                                                                            // 2381
                                                                                                            // 2382
  // test skip and limit.                                                                                   // 2383
  c.remove({});                                                                                             // 2384
  handle = c.find({}, {sort: {a: 1}, skip: 1, limit: 2}).observe(cbs);                                      // 2385
  test.equal(operations.shift(), undefined);                                                                // 2386
  c.insert({a:1});                                                                                          // 2387
  test.equal(operations.shift(), undefined);                                                                // 2388
  c.insert({_id: 'foo', a:2});                                                                              // 2389
  test.equal(operations.shift(), ['added', {a:2}, 0, null]);                                                // 2390
  c.insert({a:3});                                                                                          // 2391
  test.equal(operations.shift(), ['added', {a:3}, 1, null]);                                                // 2392
  c.insert({a:4});                                                                                          // 2393
  test.equal(operations.shift(), undefined);                                                                // 2394
  c.update({a:1}, {a:0});                                                                                   // 2395
  test.equal(operations.shift(), undefined);                                                                // 2396
  c.update({a:0}, {a:5});                                                                                   // 2397
  test.equal(operations.shift(), ['removed', 'foo', 0, {a:2}]);                                             // 2398
  test.equal(operations.shift(), ['added', {a:4}, 1, null]);                                                // 2399
  c.update({a:3}, {a:3.5});                                                                                 // 2400
  test.equal(operations.shift(), ['changed', {a:3.5}, 0, {a:3}]);                                           // 2401
  handle.stop();                                                                                            // 2402
                                                                                                            // 2403
  // test observe limit with pre-existing docs                                                              // 2404
  c.remove({});                                                                                             // 2405
  c.insert({a: 1});                                                                                         // 2406
  c.insert({_id: 'two', a: 2});                                                                             // 2407
  c.insert({a: 3});                                                                                         // 2408
  handle = c.find({}, {sort: {a: 1}, limit: 2}).observe(cbs);                                               // 2409
  test.equal(operations.shift(), ['added', {a:1}, 0, null]);                                                // 2410
  test.equal(operations.shift(), ['added', {a:2}, 1, null]);                                                // 2411
  test.equal(operations.shift(), undefined);                                                                // 2412
  c.remove({a: 2});                                                                                         // 2413
  test.equal(operations.shift(), ['removed', 'two', 1, {a:2}]);                                             // 2414
  test.equal(operations.shift(), ['added', {a:3}, 1, null]);                                                // 2415
  test.equal(operations.shift(), undefined);                                                                // 2416
  handle.stop();                                                                                            // 2417
                                                                                                            // 2418
  // test _no_indices                                                                                       // 2419
                                                                                                            // 2420
  c.remove({});                                                                                             // 2421
  handle = c.find({}, {sort: {a: 1}}).observe(_.extend(cbs, {_no_indices: true}));                          // 2422
  c.insert({_id: 'foo', a:1});                                                                              // 2423
  test.equal(operations.shift(), ['added', {a:1}, -1, null]);                                               // 2424
  c.update({a:1}, {$set: {a: 2}});                                                                          // 2425
  test.equal(operations.shift(), ['changed', {a:2}, -1, {a:1}]);                                            // 2426
  c.insert({a:10});                                                                                         // 2427
  test.equal(operations.shift(), ['added', {a:10}, -1, null]);                                              // 2428
  c.update({}, {$inc: {a: 1}}, {multi: true});                                                              // 2429
  test.equal(operations.shift(), ['changed', {a:3}, -1, {a:2}]);                                            // 2430
  test.equal(operations.shift(), ['changed', {a:11}, -1, {a:10}]);                                          // 2431
  c.update({a:11}, {a:1});                                                                                  // 2432
  test.equal(operations.shift(), ['changed', {a:1}, -1, {a:11}]);                                           // 2433
  test.equal(operations.shift(), ['moved', {a:1}, -1, -1, 'foo']);                                          // 2434
  c.remove({a:2});                                                                                          // 2435
  test.equal(operations.shift(), undefined);                                                                // 2436
  c.remove({a:3});                                                                                          // 2437
  test.equal(operations.shift(), ['removed', 'foo', -1, {a:3}]);                                            // 2438
  handle.stop();                                                                                            // 2439
});                                                                                                         // 2440
                                                                                                            // 2441
_.each([true, false], function (ordered) {                                                                  // 2442
  Tinytest.add("minimongo - observe ordered: " + ordered, function (test) {                                 // 2443
    var c = new LocalCollection();                                                                          // 2444
                                                                                                            // 2445
    var ev = "";                                                                                            // 2446
    var makecb = function (tag) {                                                                           // 2447
      var ret = {};                                                                                         // 2448
      _.each(["added", "changed", "removed"], function (fn) {                                               // 2449
        var fnName = ordered ? fn + "At" : fn;                                                              // 2450
        ret[fnName] = function (doc) {                                                                      // 2451
          ev = (ev + fn.substr(0, 1) + tag + doc._id + "_");                                                // 2452
        };                                                                                                  // 2453
      });                                                                                                   // 2454
      return ret;                                                                                           // 2455
    };                                                                                                      // 2456
    var expect = function (x) {                                                                             // 2457
      test.equal(ev, x);                                                                                    // 2458
      ev = "";                                                                                              // 2459
    };                                                                                                      // 2460
                                                                                                            // 2461
    c.insert({_id: 1, name: "strawberry", tags: ["fruit", "red", "squishy"]});                              // 2462
    c.insert({_id: 2, name: "apple", tags: ["fruit", "red", "hard"]});                                      // 2463
    c.insert({_id: 3, name: "rose", tags: ["flower", "red", "squishy"]});                                   // 2464
                                                                                                            // 2465
    // This should work equally well for ordered and unordered observations                                 // 2466
    // (because the callbacks don't look at indices and there's no 'moved'                                  // 2467
    // callback).                                                                                           // 2468
    var handle = c.find({tags: "flower"}).observe(makecb('a'));                                             // 2469
    expect("aa3_");                                                                                         // 2470
    c.update({name: "rose"}, {$set: {tags: ["bloom", "red", "squishy"]}});                                  // 2471
    expect("ra3_");                                                                                         // 2472
    c.update({name: "rose"}, {$set: {tags: ["flower", "red", "squishy"]}});                                 // 2473
    expect("aa3_");                                                                                         // 2474
    c.update({name: "rose"}, {$set: {food: false}});                                                        // 2475
    expect("ca3_");                                                                                         // 2476
    c.remove({});                                                                                           // 2477
    expect("ra3_");                                                                                         // 2478
    c.insert({_id: 4, name: "daisy", tags: ["flower"]});                                                    // 2479
    expect("aa4_");                                                                                         // 2480
    handle.stop();                                                                                          // 2481
    // After calling stop, no more callbacks are called.                                                    // 2482
    c.insert({_id: 5, name: "iris", tags: ["flower"]});                                                     // 2483
    expect("");                                                                                             // 2484
                                                                                                            // 2485
    // Test that observing a lookup by ID works.                                                            // 2486
    handle = c.find(4).observe(makecb('b'));                                                                // 2487
    expect('ab4_');                                                                                         // 2488
    c.update(4, {$set: {eek: 5}});                                                                          // 2489
    expect('cb4_');                                                                                         // 2490
    handle.stop();                                                                                          // 2491
                                                                                                            // 2492
    // Test observe with reactive: false.                                                                   // 2493
    handle = c.find({tags: "flower"}, {reactive: false}).observe(makecb('c'));                              // 2494
    expect('ac4_ac5_');                                                                                     // 2495
    // This insert shouldn't trigger a callback because it's not reactive.                                  // 2496
    c.insert({_id: 6, name: "river", tags: ["flower"]});                                                    // 2497
    expect('');                                                                                             // 2498
    handle.stop();                                                                                          // 2499
  });                                                                                                       // 2500
});                                                                                                         // 2501
                                                                                                            // 2502
                                                                                                            // 2503
Tinytest.add("minimongo - diff changes ordering", function (test) {                                         // 2504
  var makeDocs = function (ids) {                                                                           // 2505
    return _.map(ids, function (id) { return {_id: id};});                                                  // 2506
  };                                                                                                        // 2507
  var testMutation = function (a, b) {                                                                      // 2508
    var aa = makeDocs(a);                                                                                   // 2509
    var bb = makeDocs(b);                                                                                   // 2510
    var aaCopy = EJSON.clone(aa);                                                                           // 2511
    LocalCollection._diffQueryOrderedChanges(aa, bb, {                                                      // 2512
                                                                                                            // 2513
      addedBefore: function (id, doc, before) {                                                             // 2514
        if (before === null) {                                                                              // 2515
          aaCopy.push( _.extend({_id: id}, doc));                                                           // 2516
          return;                                                                                           // 2517
        }                                                                                                   // 2518
        for (var i = 0; i < aaCopy.length; i++) {                                                           // 2519
          if (aaCopy[i]._id === before) {                                                                   // 2520
            aaCopy.splice(i, 0, _.extend({_id: id}, doc));                                                  // 2521
            return;                                                                                         // 2522
          }                                                                                                 // 2523
        }                                                                                                   // 2524
      },                                                                                                    // 2525
      movedBefore: function (id, before) {                                                                  // 2526
        var found;                                                                                          // 2527
        for (var i = 0; i < aaCopy.length; i++) {                                                           // 2528
          if (aaCopy[i]._id === id) {                                                                       // 2529
            found = aaCopy[i];                                                                              // 2530
            aaCopy.splice(i, 1);                                                                            // 2531
          }                                                                                                 // 2532
        }                                                                                                   // 2533
        if (before === null) {                                                                              // 2534
          aaCopy.push( _.extend({_id: id}, found));                                                         // 2535
          return;                                                                                           // 2536
        }                                                                                                   // 2537
        for (i = 0; i < aaCopy.length; i++) {                                                               // 2538
          if (aaCopy[i]._id === before) {                                                                   // 2539
            aaCopy.splice(i, 0, _.extend({_id: id}, found));                                                // 2540
            return;                                                                                         // 2541
          }                                                                                                 // 2542
        }                                                                                                   // 2543
      },                                                                                                    // 2544
      removed: function (id) {                                                                              // 2545
        var found;                                                                                          // 2546
        for (var i = 0; i < aaCopy.length; i++) {                                                           // 2547
          if (aaCopy[i]._id === id) {                                                                       // 2548
            found = aaCopy[i];                                                                              // 2549
            aaCopy.splice(i, 1);                                                                            // 2550
          }                                                                                                 // 2551
        }                                                                                                   // 2552
      }                                                                                                     // 2553
    });                                                                                                     // 2554
    test.equal(aaCopy, bb);                                                                                 // 2555
  };                                                                                                        // 2556
                                                                                                            // 2557
  var testBothWays = function (a, b) {                                                                      // 2558
    testMutation(a, b);                                                                                     // 2559
    testMutation(b, a);                                                                                     // 2560
  };                                                                                                        // 2561
                                                                                                            // 2562
  testBothWays(["a", "b", "c"], ["c", "b", "a"]);                                                           // 2563
  testBothWays(["a", "b", "c"], []);                                                                        // 2564
  testBothWays(["a", "b", "c"], ["e","f"]);                                                                 // 2565
  testBothWays(["a", "b", "c", "d"], ["c", "b", "a"]);                                                      // 2566
  testBothWays(['A','B','C','D','E','F','G','H','I'],                                                       // 2567
               ['A','B','F','G','C','D','I','L','M','N','H']);                                              // 2568
  testBothWays(['A','B','C','D','E','F','G','H','I'],['A','B','C','D','F','G','H','E','I']);                // 2569
});                                                                                                         // 2570
                                                                                                            // 2571
Tinytest.add("minimongo - diff", function (test) {                                                          // 2572
                                                                                                            // 2573
  // test correctness                                                                                       // 2574
                                                                                                            // 2575
  var diffTest = function(origLen, newOldIdx) {                                                             // 2576
    var oldResults = new Array(origLen);                                                                    // 2577
    for (var i = 1; i <= origLen; i++)                                                                      // 2578
      oldResults[i-1] = {_id: i};                                                                           // 2579
                                                                                                            // 2580
    var newResults = _.map(newOldIdx, function(n) {                                                         // 2581
      var doc = {_id: Math.abs(n)};                                                                         // 2582
      if (n < 0)                                                                                            // 2583
        doc.changed = true;                                                                                 // 2584
      return doc;                                                                                           // 2585
    });                                                                                                     // 2586
    var find = function (arr, id) {                                                                         // 2587
      for (var i = 0; i < arr.length; i++) {                                                                // 2588
        if (EJSON.equals(arr[i]._id, id))                                                                   // 2589
          return i;                                                                                         // 2590
      }                                                                                                     // 2591
      return -1;                                                                                            // 2592
    };                                                                                                      // 2593
                                                                                                            // 2594
    var results = _.clone(oldResults);                                                                      // 2595
    var observer = {                                                                                        // 2596
      addedBefore: function(id, fields, before) {                                                           // 2597
        var before_idx;                                                                                     // 2598
        if (before === null)                                                                                // 2599
          before_idx = results.length;                                                                      // 2600
        else                                                                                                // 2601
          before_idx = find (results, before);                                                              // 2602
        var doc = _.extend({_id: id}, fields);                                                              // 2603
        test.isFalse(before_idx < 0 || before_idx > results.length);                                        // 2604
        results.splice(before_idx, 0, doc);                                                                 // 2605
      },                                                                                                    // 2606
      removed: function(id) {                                                                               // 2607
        var at_idx = find (results, id);                                                                    // 2608
        test.isFalse(at_idx < 0 || at_idx >= results.length);                                               // 2609
        results.splice(at_idx, 1);                                                                          // 2610
      },                                                                                                    // 2611
      changed: function(id, fields) {                                                                       // 2612
        var at_idx = find (results, id);                                                                    // 2613
        var oldDoc = results[at_idx];                                                                       // 2614
        var doc = EJSON.clone(oldDoc);                                                                      // 2615
        LocalCollection._applyChanges(doc, fields);                                                         // 2616
        test.isFalse(at_idx < 0 || at_idx >= results.length);                                               // 2617
        test.equal(doc._id, oldDoc._id);                                                                    // 2618
        results[at_idx] = doc;                                                                              // 2619
      },                                                                                                    // 2620
      movedBefore: function(id, before) {                                                                   // 2621
        var old_idx = find(results, id);                                                                    // 2622
        var new_idx;                                                                                        // 2623
        if (before === null)                                                                                // 2624
          new_idx = results.length;                                                                         // 2625
        else                                                                                                // 2626
          new_idx = find (results, before);                                                                 // 2627
        if (new_idx > old_idx)                                                                              // 2628
          new_idx--;                                                                                        // 2629
        test.isFalse(old_idx < 0 || old_idx >= results.length);                                             // 2630
        test.isFalse(new_idx < 0 || new_idx >= results.length);                                             // 2631
        results.splice(new_idx, 0, results.splice(old_idx, 1)[0]);                                          // 2632
      }                                                                                                     // 2633
    };                                                                                                      // 2634
                                                                                                            // 2635
    LocalCollection._diffQueryOrderedChanges(oldResults, newResults, observer);                             // 2636
    test.equal(results, newResults);                                                                        // 2637
  };                                                                                                        // 2638
                                                                                                            // 2639
  // edge cases and cases run into during debugging                                                         // 2640
  diffTest(5, [5, 1, 2, 3, 4]);                                                                             // 2641
  diffTest(0, [1, 2, 3, 4]);                                                                                // 2642
  diffTest(4, []);                                                                                          // 2643
  diffTest(7, [4, 5, 6, 7, 1, 2, 3]);                                                                       // 2644
  diffTest(7, [5, 6, 7, 1, 2, 3, 4]);                                                                       // 2645
  diffTest(10, [7, 4, 11, 6, 12, 1, 5]);                                                                    // 2646
  diffTest(3, [3, 2, 1]);                                                                                   // 2647
  diffTest(10, [2, 7, 4, 6, 11, 3, 8, 9]);                                                                  // 2648
  diffTest(0, []);                                                                                          // 2649
  diffTest(1, []);                                                                                          // 2650
  diffTest(0, [1]);                                                                                         // 2651
  diffTest(1, [1]);                                                                                         // 2652
  diffTest(5, [1, 2, 3, 4, 5]);                                                                             // 2653
                                                                                                            // 2654
  // interaction between "changed" and other ops                                                            // 2655
  diffTest(5, [-5, -1, 2, -3, 4]);                                                                          // 2656
  diffTest(7, [-4, -5, 6, 7, -1, 2, 3]);                                                                    // 2657
  diffTest(7, [5, 6, -7, 1, 2, -3, 4]);                                                                     // 2658
  diffTest(10, [7, -4, 11, 6, 12, -1, 5]);                                                                  // 2659
  diffTest(3, [-3, -2, -1]);                                                                                // 2660
  diffTest(10, [-2, 7, 4, 6, 11, -3, -8, 9]);                                                               // 2661
});                                                                                                         // 2662
                                                                                                            // 2663
                                                                                                            // 2664
Tinytest.add("minimongo - saveOriginals", function (test) {                                                 // 2665
  // set up some data                                                                                       // 2666
  var c = new LocalCollection(),                                                                            // 2667
      count;                                                                                                // 2668
  c.insert({_id: 'foo', x: 'untouched'});                                                                   // 2669
  c.insert({_id: 'bar', x: 'updateme'});                                                                    // 2670
  c.insert({_id: 'baz', x: 'updateme'});                                                                    // 2671
  c.insert({_id: 'quux', y: 'removeme'});                                                                   // 2672
  c.insert({_id: 'whoa', y: 'removeme'});                                                                   // 2673
                                                                                                            // 2674
  // Save originals and make some changes.                                                                  // 2675
  c.saveOriginals();                                                                                        // 2676
  c.insert({_id: "hooray", z: 'insertme'});                                                                 // 2677
  c.remove({y: 'removeme'});                                                                                // 2678
  count = c.update({x: 'updateme'}, {$set: {z: 5}}, {multi: true});                                         // 2679
  c.update('bar', {$set: {k: 7}});  // update same doc twice                                                // 2680
                                                                                                            // 2681
  // Verify returned count is correct                                                                       // 2682
  test.equal(count, 2);                                                                                     // 2683
                                                                                                            // 2684
  // Verify the originals.                                                                                  // 2685
  var originals = c.retrieveOriginals();                                                                    // 2686
  var affected = ['bar', 'baz', 'quux', 'whoa', 'hooray'];                                                  // 2687
  test.equal(originals.size(), _.size(affected));                                                           // 2688
  _.each(affected, function (id) {                                                                          // 2689
    test.isTrue(originals.has(id));                                                                         // 2690
  });                                                                                                       // 2691
  test.equal(originals.get('bar'), {_id: 'bar', x: 'updateme'});                                            // 2692
  test.equal(originals.get('baz'), {_id: 'baz', x: 'updateme'});                                            // 2693
  test.equal(originals.get('quux'), {_id: 'quux', y: 'removeme'});                                          // 2694
  test.equal(originals.get('whoa'), {_id: 'whoa', y: 'removeme'});                                          // 2695
  test.equal(originals.get('hooray'), undefined);                                                           // 2696
                                                                                                            // 2697
  // Verify that changes actually occured.                                                                  // 2698
  test.equal(c.find().count(), 4);                                                                          // 2699
  test.equal(c.findOne('foo'), {_id: 'foo', x: 'untouched'});                                               // 2700
  test.equal(c.findOne('bar'), {_id: 'bar', x: 'updateme', z: 5, k: 7});                                    // 2701
  test.equal(c.findOne('baz'), {_id: 'baz', x: 'updateme', z: 5});                                          // 2702
  test.equal(c.findOne('hooray'), {_id: 'hooray', z: 'insertme'});                                          // 2703
                                                                                                            // 2704
  // The next call doesn't get the same originals again.                                                    // 2705
  c.saveOriginals();                                                                                        // 2706
  originals = c.retrieveOriginals();                                                                        // 2707
  test.isTrue(originals);                                                                                   // 2708
  test.isTrue(originals.empty());                                                                           // 2709
                                                                                                            // 2710
  // Insert and remove a document during the period.                                                        // 2711
  c.saveOriginals();                                                                                        // 2712
  c.insert({_id: 'temp', q: 8});                                                                            // 2713
  c.remove('temp');                                                                                         // 2714
  originals = c.retrieveOriginals();                                                                        // 2715
  test.equal(originals.size(), 1);                                                                          // 2716
  test.isTrue(originals.has('temp'));                                                                       // 2717
  test.equal(originals.get('temp'), undefined);                                                             // 2718
});                                                                                                         // 2719
                                                                                                            // 2720
Tinytest.add("minimongo - saveOriginals errors", function (test) {                                          // 2721
  var c = new LocalCollection();                                                                            // 2722
  // Can't call retrieve before save.                                                                       // 2723
  test.throws(function () { c.retrieveOriginals(); });                                                      // 2724
  c.saveOriginals();                                                                                        // 2725
  // Can't call save twice.                                                                                 // 2726
  test.throws(function () { c.saveOriginals(); });                                                          // 2727
});                                                                                                         // 2728
                                                                                                            // 2729
Tinytest.add("minimongo - objectid transformation", function (test) {                                       // 2730
  var testId = function (item) {                                                                            // 2731
    test.equal(item, LocalCollection._idParse(LocalCollection._idStringify(item)));                         // 2732
  };                                                                                                        // 2733
  var randomOid = new LocalCollection._ObjectID();                                                          // 2734
  testId(randomOid);                                                                                        // 2735
  testId("FOO");                                                                                            // 2736
  testId("ffffffffffff");                                                                                   // 2737
  testId("0987654321abcdef09876543");                                                                       // 2738
  testId(new LocalCollection._ObjectID());                                                                  // 2739
  testId("--a string");                                                                                     // 2740
                                                                                                            // 2741
  test.equal("ffffffffffff", LocalCollection._idParse(LocalCollection._idStringify("ffffffffffff")));       // 2742
});                                                                                                         // 2743
                                                                                                            // 2744
                                                                                                            // 2745
Tinytest.add("minimongo - objectid", function (test) {                                                      // 2746
  var randomOid = new LocalCollection._ObjectID();                                                          // 2747
  var anotherRandomOid = new LocalCollection._ObjectID();                                                   // 2748
  test.notEqual(randomOid, anotherRandomOid);                                                               // 2749
  test.throws(function() { new LocalCollection._ObjectID("qqqqqqqqqqqqqqqqqqqqqqqq");});                    // 2750
  test.throws(function() { new LocalCollection._ObjectID("ABCDEF"); });                                     // 2751
  test.equal(randomOid, new LocalCollection._ObjectID(randomOid.valueOf()));                                // 2752
});                                                                                                         // 2753
                                                                                                            // 2754
Tinytest.add("minimongo - pause", function (test) {                                                         // 2755
  var operations = [];                                                                                      // 2756
  var cbs = log_callbacks(operations);                                                                      // 2757
                                                                                                            // 2758
  var c = new LocalCollection();                                                                            // 2759
  var h = c.find({}).observe(cbs);                                                                          // 2760
                                                                                                            // 2761
  // remove and add cancel out.                                                                             // 2762
  c.insert({_id: 1, a: 1});                                                                                 // 2763
  test.equal(operations.shift(), ['added', {a:1}, 0, null]);                                                // 2764
                                                                                                            // 2765
  c.pauseObservers();                                                                                       // 2766
                                                                                                            // 2767
  c.remove({_id: 1});                                                                                       // 2768
  test.length(operations, 0);                                                                               // 2769
  c.insert({_id: 1, a: 1});                                                                                 // 2770
  test.length(operations, 0);                                                                               // 2771
                                                                                                            // 2772
  c.resumeObservers();                                                                                      // 2773
  test.length(operations, 0);                                                                               // 2774
                                                                                                            // 2775
                                                                                                            // 2776
  // two modifications become one                                                                           // 2777
  c.pauseObservers();                                                                                       // 2778
                                                                                                            // 2779
  c.update({_id: 1}, {a: 2});                                                                               // 2780
  c.update({_id: 1}, {a: 3});                                                                               // 2781
                                                                                                            // 2782
  c.resumeObservers();                                                                                      // 2783
  test.equal(operations.shift(), ['changed', {a:3}, 0, {a:1}]);                                             // 2784
  test.length(operations, 0);                                                                               // 2785
                                                                                                            // 2786
  // test special case for remove({})                                                                       // 2787
  c.pauseObservers();                                                                                       // 2788
  test.equal(c.remove({}), 1);                                                                              // 2789
  test.length(operations, 0);                                                                               // 2790
  c.resumeObservers();                                                                                      // 2791
  test.equal(operations.shift(), ['removed', 1, 0, {a:3}]);                                                 // 2792
  test.length(operations, 0);                                                                               // 2793
                                                                                                            // 2794
  h.stop();                                                                                                 // 2795
});                                                                                                         // 2796
                                                                                                            // 2797
Tinytest.add("minimongo - ids matched by selector", function (test) {                                       // 2798
  var check = function (selector, ids) {                                                                    // 2799
    var idsFromSelector = LocalCollection._idsMatchedBySelector(selector);                                  // 2800
    // XXX normalize order, in a way that also works for ObjectIDs?                                         // 2801
    test.equal(idsFromSelector, ids);                                                                       // 2802
  };                                                                                                        // 2803
  check("foo", ["foo"]);                                                                                    // 2804
  check({_id: "foo"}, ["foo"]);                                                                             // 2805
  var oid1 = new LocalCollection._ObjectID();                                                               // 2806
  check(oid1, [oid1]);                                                                                      // 2807
  check({_id: oid1}, [oid1]);                                                                               // 2808
  check({_id: "foo", x: 42}, ["foo"]);                                                                      // 2809
  check({}, null);                                                                                          // 2810
  check({_id: {$in: ["foo", oid1]}}, ["foo", oid1]);                                                        // 2811
  check({_id: {$ne: "foo"}}, null);                                                                         // 2812
  // not actually valid, but works for now...                                                               // 2813
  check({$and: ["foo"]}, ["foo"]);                                                                          // 2814
  check({$and: [{x: 42}, {_id: oid1}]}, [oid1]);                                                            // 2815
  check({$and: [{x: 42}, {_id: {$in: [oid1]}}]}, [oid1]);                                                   // 2816
});                                                                                                         // 2817
                                                                                                            // 2818
Tinytest.add("minimongo - reactive stop", function (test) {                                                 // 2819
  var coll = new LocalCollection();                                                                         // 2820
  coll.insert({_id: 'A'});                                                                                  // 2821
  coll.insert({_id: 'B'});                                                                                  // 2822
  coll.insert({_id: 'C'});                                                                                  // 2823
                                                                                                            // 2824
  var addBefore = function (str, newChar, before) {                                                         // 2825
    var idx = str.indexOf(before);                                                                          // 2826
    if (idx === -1)                                                                                         // 2827
      return str + newChar;                                                                                 // 2828
    return str.slice(0, idx) + newChar + str.slice(idx);                                                    // 2829
  };                                                                                                        // 2830
                                                                                                            // 2831
  var x, y;                                                                                                 // 2832
  var sortOrder = ReactiveVar(1);                                                                           // 2833
                                                                                                            // 2834
  var c = Deps.autorun(function () {                                                                        // 2835
    var q = coll.find({}, {sort: {_id: sortOrder.get()}});                                                  // 2836
    x = "";                                                                                                 // 2837
    q.observe({ addedAt: function (doc, atIndex, before) {                                                  // 2838
      x = addBefore(x, doc._id, before);                                                                    // 2839
    }});                                                                                                    // 2840
    y = "";                                                                                                 // 2841
    q.observeChanges({ addedBefore: function (id, fields, before) {                                         // 2842
      y = addBefore(y, id, before);                                                                         // 2843
    }});                                                                                                    // 2844
  });                                                                                                       // 2845
                                                                                                            // 2846
  test.equal(x, "ABC");                                                                                     // 2847
  test.equal(y, "ABC");                                                                                     // 2848
                                                                                                            // 2849
  sortOrder.set(-1);                                                                                        // 2850
  test.equal(x, "ABC");                                                                                     // 2851
  test.equal(y, "ABC");                                                                                     // 2852
  Deps.flush();                                                                                             // 2853
  test.equal(x, "CBA");                                                                                     // 2854
  test.equal(y, "CBA");                                                                                     // 2855
                                                                                                            // 2856
  coll.insert({_id: 'D'});                                                                                  // 2857
  coll.insert({_id: 'E'});                                                                                  // 2858
  test.equal(x, "EDCBA");                                                                                   // 2859
  test.equal(y, "EDCBA");                                                                                   // 2860
                                                                                                            // 2861
  c.stop();                                                                                                 // 2862
  // stopping kills the observes immediately                                                                // 2863
  coll.insert({_id: 'F'});                                                                                  // 2864
  test.equal(x, "EDCBA");                                                                                   // 2865
  test.equal(y, "EDCBA");                                                                                   // 2866
});                                                                                                         // 2867
                                                                                                            // 2868
Tinytest.add("minimongo - immediate invalidate", function (test) {                                          // 2869
  var coll = new LocalCollection();                                                                         // 2870
  coll.insert({_id: 'A'});                                                                                  // 2871
                                                                                                            // 2872
  // This has two separate findOnes.  findOne() uses skip/limit, which means                                // 2873
  // that its response to an update() call involves a recompute. We used to have                            // 2874
  // a bug where we would first calculate all the calls that need to be                                     // 2875
  // recomputed, then recompute them one by one, without checking to see if the                             // 2876
  // callbacks from recomputing one query stopped the second query, which                                   // 2877
  // crashed.                                                                                               // 2878
  var c = Deps.autorun(function () {                                                                        // 2879
    coll.findOne('A');                                                                                      // 2880
    coll.findOne('A');                                                                                      // 2881
  });                                                                                                       // 2882
                                                                                                            // 2883
  coll.update('A', {$set: {x: 42}});                                                                        // 2884
                                                                                                            // 2885
  c.stop();                                                                                                 // 2886
});                                                                                                         // 2887
                                                                                                            // 2888
                                                                                                            // 2889
Tinytest.add("minimongo - count on cursor with limit", function(test){                                      // 2890
  var coll = new LocalCollection(), count;                                                                  // 2891
                                                                                                            // 2892
  coll.insert({_id: 'A'});                                                                                  // 2893
  coll.insert({_id: 'B'});                                                                                  // 2894
  coll.insert({_id: 'C'});                                                                                  // 2895
  coll.insert({_id: 'D'});                                                                                  // 2896
                                                                                                            // 2897
  var c = Deps.autorun(function (c) {                                                                       // 2898
    var cursor = coll.find({_id: {$exists: true}}, {sort: {_id: 1}, limit: 3});                             // 2899
    count = cursor.count();                                                                                 // 2900
  });                                                                                                       // 2901
                                                                                                            // 2902
  test.equal(count, 3);                                                                                     // 2903
                                                                                                            // 2904
  coll.remove('A'); // still 3 in the collection                                                            // 2905
  Deps.flush();                                                                                             // 2906
  test.equal(count, 3);                                                                                     // 2907
                                                                                                            // 2908
  coll.remove('B'); // expect count now 2                                                                   // 2909
  Deps.flush();                                                                                             // 2910
  test.equal(count, 2);                                                                                     // 2911
                                                                                                            // 2912
                                                                                                            // 2913
  coll.insert({_id: 'A'}); // now 3 again                                                                   // 2914
  Deps.flush();                                                                                             // 2915
  test.equal(count, 3);                                                                                     // 2916
                                                                                                            // 2917
  coll.insert({_id: 'B'}); // now 4 entries, but count should be 3 still                                    // 2918
  Deps.flush();                                                                                             // 2919
  test.equal(count, 3);                                                                                     // 2920
                                                                                                            // 2921
  c.stop();                                                                                                 // 2922
                                                                                                            // 2923
});                                                                                                         // 2924
                                                                                                            // 2925
Tinytest.add("minimongo - $near operator tests", function (test) {                                          // 2926
  var coll = new LocalCollection();                                                                         // 2927
  coll.insert({ rest: { loc: [2, 3] } });                                                                   // 2928
  coll.insert({ rest: { loc: [-3, 3] } });                                                                  // 2929
  coll.insert({ rest: { loc: [5, 5] } });                                                                   // 2930
                                                                                                            // 2931
  test.equal(coll.find({ 'rest.loc': { $near: [0, 0], $maxDistance: 30 } }).count(), 3);                    // 2932
  test.equal(coll.find({ 'rest.loc': { $near: [0, 0], $maxDistance: 4 } }).count(), 1);                     // 2933
  var points = coll.find({ 'rest.loc': { $near: [0, 0], $maxDistance: 6 } }).fetch();                       // 2934
  _.each(points, function (point, i, points) {                                                              // 2935
    test.isTrue(!i || distance([0, 0], point.rest.loc) >= distance([0, 0], points[i - 1].rest.loc));        // 2936
  });                                                                                                       // 2937
                                                                                                            // 2938
  function distance(a, b) {                                                                                 // 2939
    var x = a[0] - b[0];                                                                                    // 2940
    var y = a[1] - b[1];                                                                                    // 2941
    return Math.sqrt(x * x + y * y);                                                                        // 2942
  }                                                                                                         // 2943
                                                                                                            // 2944
  // GeoJSON tests                                                                                          // 2945
  coll = new LocalCollection();                                                                             // 2946
  var data = [{ "category" : "BURGLARY", "descript" : "BURGLARY OF STORE, FORCIBLE ENTRY", "address" : "100 Block of 10TH ST", "location" : { "type" : "Point", "coordinates" : [  -122.415449723856,  37.7749518087273 ] } },
    { "category" : "WEAPON LAWS", "descript" : "POSS OF PROHIBITED WEAPON", "address" : "900 Block of MINNA ST", "location" : { "type" : "Point", "coordinates" : [  -122.415386041221,  37.7747879744156 ] } },
    { "category" : "LARCENY/THEFT", "descript" : "GRAND THEFT OF PROPERTY", "address" : "900 Block of MINNA ST", "location" : { "type" : "Point", "coordinates" : [  -122.41538270191,  37.774683628213 ] } },
    { "category" : "LARCENY/THEFT", "descript" : "PETTY THEFT FROM LOCKED AUTO", "address" : "900 Block of MINNA ST", "location" : { "type" : "Point", "coordinates" : [  -122.415396041221,  37.7747879744156 ] } },
    { "category" : "OTHER OFFENSES", "descript" : "POSSESSION OF BURGLARY TOOLS", "address" : "900 Block of MINNA ST", "location" : { "type" : "Point", "coordinates" : [  -122.415386041221,  37.7747879734156 ] } }
  ];                                                                                                        // 2952
                                                                                                            // 2953
  _.each(data, function (x, i) { coll.insert(_.extend(x, { x: i })); });                                    // 2954
                                                                                                            // 2955
  var close15 = coll.find({ location: { $near: {                                                            // 2956
    $geometry: { type: "Point",                                                                             // 2957
                 coordinates: [-122.4154282, 37.7746115] },                                                 // 2958
    $maxDistance: 15 } } }).fetch();                                                                        // 2959
  test.length(close15, 1);                                                                                  // 2960
  test.equal(close15[0].descript, "GRAND THEFT OF PROPERTY");                                               // 2961
                                                                                                            // 2962
  var close20 = coll.find({ location: { $near: {                                                            // 2963
    $geometry: { type: "Point",                                                                             // 2964
                 coordinates: [-122.4154282, 37.7746115] },                                                 // 2965
    $maxDistance: 20 } } }).fetch();                                                                        // 2966
  test.length(close20, 4);                                                                                  // 2967
  test.equal(close20[0].descript, "GRAND THEFT OF PROPERTY");                                               // 2968
  test.equal(close20[1].descript, "PETTY THEFT FROM LOCKED AUTO");                                          // 2969
  test.equal(close20[2].descript, "POSSESSION OF BURGLARY TOOLS");                                          // 2970
  test.equal(close20[3].descript, "POSS OF PROHIBITED WEAPON");                                             // 2971
                                                                                                            // 2972
  // Any combinations of $near with $or/$and/$nor/$not should throw an error                                // 2973
  test.throws(function () {                                                                                 // 2974
    coll.find({ location: {                                                                                 // 2975
      $not: {                                                                                               // 2976
        $near: {                                                                                            // 2977
          $geometry: {                                                                                      // 2978
            type: "Point",                                                                                  // 2979
            coordinates: [-122.4154282, 37.7746115]                                                         // 2980
          }, $maxDistance: 20 } } } });                                                                     // 2981
  });                                                                                                       // 2982
  test.throws(function () {                                                                                 // 2983
    coll.find({                                                                                             // 2984
      $and: [ { location: { $near: { $geometry: { type: "Point", coordinates: [-122.4154282, 37.7746115] }, $maxDistance: 20 }}},
              { x: 0 }]                                                                                     // 2986
    });                                                                                                     // 2987
  });                                                                                                       // 2988
  test.throws(function () {                                                                                 // 2989
    coll.find({                                                                                             // 2990
      $or: [ { location: { $near: { $geometry: { type: "Point", coordinates: [-122.4154282, 37.7746115] }, $maxDistance: 20 }}},
             { x: 0 }]                                                                                      // 2992
    });                                                                                                     // 2993
  });                                                                                                       // 2994
  test.throws(function () {                                                                                 // 2995
    coll.find({                                                                                             // 2996
      $nor: [ { location: { $near: { $geometry: { type: "Point", coordinates: [-122.4154282, 37.7746115] }, $maxDistance: 1 }}},
              { x: 0 }]                                                                                     // 2998
    });                                                                                                     // 2999
  });                                                                                                       // 3000
  test.throws(function () {                                                                                 // 3001
    coll.find({                                                                                             // 3002
      $and: [{                                                                                              // 3003
        $and: [{                                                                                            // 3004
          location: {                                                                                       // 3005
            $near: {                                                                                        // 3006
              $geometry: {                                                                                  // 3007
                type: "Point",                                                                              // 3008
                coordinates: [-122.4154282, 37.7746115]                                                     // 3009
              },                                                                                            // 3010
              $maxDistance: 1                                                                               // 3011
            }                                                                                               // 3012
          }                                                                                                 // 3013
        }]                                                                                                  // 3014
      }]                                                                                                    // 3015
    });                                                                                                     // 3016
  });                                                                                                       // 3017
                                                                                                            // 3018
  // array tests                                                                                            // 3019
  coll = new LocalCollection();                                                                             // 3020
  coll.insert({                                                                                             // 3021
    _id: "x",                                                                                               // 3022
    k: 9,                                                                                                   // 3023
    a: [                                                                                                    // 3024
      {b: [                                                                                                 // 3025
        [100, 100],                                                                                         // 3026
        [1,  1]]},                                                                                          // 3027
      {b: [150,  150]}]});                                                                                  // 3028
  coll.insert({                                                                                             // 3029
    _id: "y",                                                                                               // 3030
    k: 9,                                                                                                   // 3031
    a: {b: [5, 5]}});                                                                                       // 3032
  var testNear = function (near, md, expected) {                                                            // 3033
    test.equal(                                                                                             // 3034
      _.pluck(                                                                                              // 3035
        coll.find({'a.b': {$near: near, $maxDistance: md}}).fetch(), '_id'),                                // 3036
      expected);                                                                                            // 3037
  };                                                                                                        // 3038
  testNear([149, 149], 4, ['x']);                                                                           // 3039
  testNear([149, 149], 1000, ['x', 'y']);                                                                   // 3040
  // It's important that we figure out that 'x' is closer than 'y' to [2,2] even                            // 3041
  // though the first within-1000 point in 'x' (ie, [100,100]) is farther than                              // 3042
  // 'y'.                                                                                                   // 3043
  testNear([2, 2], 1000, ['x', 'y']);                                                                       // 3044
                                                                                                            // 3045
  // Ensure that distance is used as a tie-breaker for sort.                                                // 3046
  test.equal(                                                                                               // 3047
    _.pluck(coll.find({'a.b': {$near: [1, 1]}}, {sort: {k: 1}}).fetch(), '_id'),                            // 3048
    ['x', 'y']);                                                                                            // 3049
  test.equal(                                                                                               // 3050
    _.pluck(coll.find({'a.b': {$near: [5, 5]}}, {sort: {k: 1}}).fetch(), '_id'),                            // 3051
    ['y', 'x']);                                                                                            // 3052
                                                                                                            // 3053
  var operations = [];                                                                                      // 3054
  var cbs = log_callbacks(operations);                                                                      // 3055
  var handle = coll.find({'a.b': {$near: [7,7]}}).observe(cbs);                                             // 3056
                                                                                                            // 3057
  test.length(operations, 2);                                                                               // 3058
  test.equal(operations.shift(), ['added', {k:9, a:{b:[5,5]}}, 0, null]);                                   // 3059
  test.equal(operations.shift(),                                                                            // 3060
             ['added', {k: 9, a:[{b:[[100,100],[1,1]]},{b:[150,150]}]},                                     // 3061
              1, null]);                                                                                    // 3062
  // This needs to be inserted in the MIDDLE of the two existing ones.                                      // 3063
  coll.insert({a: {b: [3,3]}});                                                                             // 3064
  test.length(operations, 1);                                                                               // 3065
  test.equal(operations.shift(), ['added', {a: {b: [3, 3]}}, 1, 'x']);                                      // 3066
                                                                                                            // 3067
  handle.stop();                                                                                            // 3068
});                                                                                                         // 3069
                                                                                                            // 3070
                                                                                                            // 3071
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// packages/minimongo/wrap_transform_tests.js                                                               //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
Tinytest.add("minimongo - wrapTransform", function (test) {                                                 // 1
  var wrap = LocalCollection.wrapTransform;                                                                 // 2
                                                                                                            // 3
  // Transforming no function gives falsey.                                                                 // 4
  test.isFalse(wrap(undefined));                                                                            // 5
  test.isFalse(wrap(null));                                                                                 // 6
                                                                                                            // 7
  // It's OK if you don't change the ID.                                                                    // 8
  var validTransform = function (doc) {                                                                     // 9
    delete doc.x;                                                                                           // 10
    doc.y = 42;                                                                                             // 11
    doc.z = function () { return 43; };                                                                     // 12
    return doc;                                                                                             // 13
  };                                                                                                        // 14
  var transformed = wrap(validTransform)({_id: "asdf", x: 54});                                             // 15
  test.equal(_.keys(transformed), ['_id', 'y', 'z']);                                                       // 16
  test.equal(transformed.y, 42);                                                                            // 17
  test.equal(transformed.z(), 43);                                                                          // 18
                                                                                                            // 19
  // Ensure that ObjectIDs work (even if the _ids in question are not ===-equal)                            // 20
  var oid1 = new LocalCollection._ObjectID();                                                               // 21
  var oid2 = new LocalCollection._ObjectID(oid1.toHexString());                                             // 22
  test.equal(wrap(function () {return {_id: oid2};})({_id: oid1}),                                          // 23
             {_id: oid2});                                                                                  // 24
                                                                                                            // 25
  // transform functions must return objects                                                                // 26
  var invalidObjects = [                                                                                    // 27
    "asdf", new LocalCollection._ObjectID(), false, null, true,                                             // 28
    27, [123], /adsf/, new Date, function () {}, undefined                                                  // 29
  ];                                                                                                        // 30
  _.each(invalidObjects, function (invalidObject) {                                                         // 31
    var wrapped = wrap(function () { return invalidObject; });                                              // 32
    test.throws(function () {                                                                               // 33
      wrapped({_id: "asdf"});                                                                               // 34
    });                                                                                                     // 35
  }, /transform must return object/);                                                                       // 36
                                                                                                            // 37
  // transform functions may not change _ids                                                                // 38
  var wrapped = wrap(function (doc) { doc._id = 'x'; return doc; });                                        // 39
  test.throws(function () {                                                                                 // 40
    wrapped({_id: 'y'});                                                                                    // 41
  }, /can't have different _id/);                                                                           // 42
                                                                                                            // 43
  // transform functions may remove _ids                                                                    // 44
  test.equal({_id: 'a', x: 2},                                                                              // 45
             wrap(function (d) {delete d._id; return d;})({_id: 'a', x: 2}));                               // 46
                                                                                                            // 47
  // test that wrapped transform functions are nonreactive                                                  // 48
  var unwrapped = function (doc) {                                                                          // 49
    test.isFalse(Deps.active);                                                                              // 50
    return doc;                                                                                             // 51
  };                                                                                                        // 52
  var handle = Deps.autorun(function () {                                                                   // 53
    test.isTrue(Deps.active);                                                                               // 54
    wrap(unwrapped)({_id: "xxx"});                                                                          // 55
  });                                                                                                       // 56
  handle.stop();                                                                                            // 57
});                                                                                                         // 58
                                                                                                            // 59
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
