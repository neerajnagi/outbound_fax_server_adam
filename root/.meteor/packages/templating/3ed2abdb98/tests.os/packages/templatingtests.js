(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/templating/plugin/html_scanner.js                                                     //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
html_scanner = {                                                                                  // 1
  // Scan a template file for <head>, <body>, and <template>                                      // 2
  // tags and extract their contents.                                                             // 3
  //                                                                                              // 4
  // This is a primitive, regex-based scanner.  It scans                                          // 5
  // top-level tags, which are allowed to have attributes,                                        // 6
  // and ignores top-level HTML comments.                                                         // 7
                                                                                                  // 8
  // Has fields 'message', 'line', 'file'                                                         // 9
  ParseError: function () {                                                                       // 10
  },                                                                                              // 11
                                                                                                  // 12
  scan: function (contents, source_name) {                                                        // 13
    var rest = contents;                                                                          // 14
    var index = 0;                                                                                // 15
                                                                                                  // 16
    var advance = function(amount) {                                                              // 17
      rest = rest.substring(amount);                                                              // 18
      index += amount;                                                                            // 19
    };                                                                                            // 20
                                                                                                  // 21
    var throwParseError = function (msg, overrideIndex) {                                         // 22
      var ret = new html_scanner.ParseError;                                                      // 23
      ret.message = msg || "bad formatting in HTML template";                                     // 24
      ret.file = source_name;                                                                     // 25
      var theIndex = (typeof overrideIndex === 'number' ? overrideIndex : index);                 // 26
      ret.line = contents.substring(0, theIndex).split('\n').length;                              // 27
      throw ret;                                                                                  // 28
    };                                                                                            // 29
                                                                                                  // 30
    var results = html_scanner._initResults();                                                    // 31
    var rOpenTag = /^((<(template|head|body)\b)|(<!--)|(<!DOCTYPE|{{!)|$)/i;                      // 32
                                                                                                  // 33
    while (rest) {                                                                                // 34
      // skip whitespace first (for better line numbers)                                          // 35
      advance(rest.match(/^\s*/)[0].length);                                                      // 36
                                                                                                  // 37
      var match = rOpenTag.exec(rest);                                                            // 38
      if (! match)                                                                                // 39
        throwParseError(); // unknown text encountered                                            // 40
                                                                                                  // 41
      var matchToken = match[1];                                                                  // 42
      var matchTokenTagName =  match[3];                                                          // 43
      var matchTokenComment = match[4];                                                           // 44
      var matchTokenUnsupported = match[5];                                                       // 45
                                                                                                  // 46
      var tagStartIndex = index;                                                                  // 47
      advance(match.index + match[0].length);                                                     // 48
                                                                                                  // 49
      if (! matchToken)                                                                           // 50
        break; // matched $ (end of file)                                                         // 51
      if (matchTokenComment === '<!--') {                                                         // 52
        // top-level HTML comment                                                                 // 53
        var commentEnd = /--\s*>/.exec(rest);                                                     // 54
        if (! commentEnd)                                                                         // 55
          throwParseError("unclosed HTML comment");                                               // 56
        advance(commentEnd.index + commentEnd[0].length);                                         // 57
        continue;                                                                                 // 58
      }                                                                                           // 59
      if (matchTokenUnsupported) {                                                                // 60
        switch (matchTokenUnsupported.toLowerCase()) {                                            // 61
        case '<!doctype':                                                                         // 62
          throwParseError(                                                                        // 63
            "Can't set DOCTYPE here.  (Meteor sets <!DOCTYPE html> for you)");                    // 64
        case '{{!':                                                                               // 65
          throwParseError(                                                                        // 66
            "Can't use '{{! }}' outside a template.  Use '<!-- -->'.");                           // 67
        }                                                                                         // 68
        throwParseError();                                                                        // 69
      }                                                                                           // 70
                                                                                                  // 71
      // otherwise, a <tag>                                                                       // 72
      var tagName = matchTokenTagName.toLowerCase();                                              // 73
      var tagAttribs = {}; // bare name -> value dict                                             // 74
      var rTagPart = /^\s*((([a-zA-Z0-9:_-]+)\s*=\s*(["'])(.*?)\4)|(>))/;                         // 75
      var attr;                                                                                   // 76
      // read attributes                                                                          // 77
      while ((attr = rTagPart.exec(rest))) {                                                      // 78
        var attrToken = attr[1];                                                                  // 79
        var attrKey = attr[3];                                                                    // 80
        var attrValue = attr[5];                                                                  // 81
        advance(attr.index + attr[0].length);                                                     // 82
        if (attrToken === '>')                                                                    // 83
          break;                                                                                  // 84
        // XXX we don't HTML unescape the attribute value                                         // 85
        // (e.g. to allow "abcd&quot;efg") or protect against                                     // 86
        // collisions with methods of tagAttribs (e.g. for                                        // 87
        // a property named toString)                                                             // 88
        attrValue = attrValue.match(/^\s*([\s\S]*?)\s*$/)[1]; // trim                             // 89
        tagAttribs[attrKey] = attrValue;                                                          // 90
      }                                                                                           // 91
      if (! attr) // didn't end on '>'                                                            // 92
        throwParseError("Parse error in tag");                                                    // 93
      // find </tag>                                                                              // 94
      var end = (new RegExp('</'+tagName+'\\s*>', 'i')).exec(rest);                               // 95
      if (! end)                                                                                  // 96
        throwParseError("unclosed <"+tagName+">");                                                // 97
      var tagContents = rest.slice(0, end.index);                                                 // 98
      var contentsStartIndex = index;                                                             // 99
                                                                                                  // 100
      // act on the tag                                                                           // 101
      html_scanner._handleTag(results, tagName, tagAttribs, tagContents,                          // 102
                              throwParseError, contentsStartIndex,                                // 103
                              tagStartIndex);                                                     // 104
                                                                                                  // 105
      // advance afterwards, so that line numbers in errors are correct                           // 106
      advance(end.index + end[0].length);                                                         // 107
    }                                                                                             // 108
                                                                                                  // 109
    return results;                                                                               // 110
  },                                                                                              // 111
                                                                                                  // 112
  _initResults: function() {                                                                      // 113
    var results = {};                                                                             // 114
    results.head = '';                                                                            // 115
    results.body = '';                                                                            // 116
    results.js = '';                                                                              // 117
    return results;                                                                               // 118
  },                                                                                              // 119
                                                                                                  // 120
  _handleTag: function (results, tag, attribs, contents, throwParseError,                         // 121
                        contentsStartIndex, tagStartIndex) {                                      // 122
                                                                                                  // 123
    // trim the tag contents.                                                                     // 124
    // this is a courtesy and is also relied on by some unit tests.                               // 125
    var m = contents.match(/^([ \t\r\n]*)([\s\S]*?)[ \t\r\n]*$/);                                 // 126
    contentsStartIndex += m[1].length;                                                            // 127
    contents = m[2];                                                                              // 128
                                                                                                  // 129
    // do we have 1 or more attribs?                                                              // 130
    var hasAttribs = false;                                                                       // 131
    for(var k in attribs) {                                                                       // 132
      if (attribs.hasOwnProperty(k)) {                                                            // 133
        hasAttribs = true;                                                                        // 134
        break;                                                                                    // 135
      }                                                                                           // 136
    }                                                                                             // 137
                                                                                                  // 138
    if (tag === "head") {                                                                         // 139
      if (hasAttribs)                                                                             // 140
        throwParseError("Attributes on <head> not supported");                                    // 141
      results.head += contents;                                                                   // 142
      return;                                                                                     // 143
    }                                                                                             // 144
                                                                                                  // 145
                                                                                                  // 146
    // <body> or <template>                                                                       // 147
                                                                                                  // 148
    try {                                                                                         // 149
      if (tag === "template") {                                                                   // 150
        var name = attribs.name;                                                                  // 151
        if (! name)                                                                               // 152
          throwParseError("Template has no 'name' attribute");                                    // 153
                                                                                                  // 154
        if (Spacebars.isReservedName(name))                                                       // 155
          throwParseError("Template can't be named \"" + name + "\"");                            // 156
                                                                                                  // 157
        var renderFuncCode = Spacebars.compile(                                                   // 158
          contents, {                                                                             // 159
            isTemplate: true,                                                                     // 160
            sourceName: 'Template "' + name + '"'                                                 // 161
          });                                                                                     // 162
                                                                                                  // 163
        results.js += "\nTemplate.__define__(" + JSON.stringify(name) +                           // 164
          ", " + renderFuncCode + ");\n";                                                         // 165
      } else {                                                                                    // 166
        // <body>                                                                                 // 167
        if (hasAttribs)                                                                           // 168
          throwParseError("Attributes on <body> not supported");                                  // 169
                                                                                                  // 170
        var renderFuncCode = Spacebars.compile(                                                   // 171
          contents, {                                                                             // 172
            isBody: true,                                                                         // 173
            sourceName: "<body>"                                                                  // 174
          });                                                                                     // 175
                                                                                                  // 176
        // We may be one of many `<body>` tags.                                                   // 177
        results.js += "\nUI.body.contentParts.push(UI.Component.extend({render: " + renderFuncCode + "}));\nMeteor.startup(function () { if (! UI.body.INSTANTIATED) { UI.body.INSTANTIATED = true; UI.DomRange.insert(UI.render(UI.body).dom, document.body); } });\n";
      }                                                                                           // 179
    } catch (e) {                                                                                 // 180
      if (e.scanner) {                                                                            // 181
        // The error came from Spacebars                                                          // 182
        throwParseError(e.message, contentsStartIndex + e.offset);                                // 183
      } else {                                                                                    // 184
        throw e;                                                                                  // 185
      }                                                                                           // 186
    }                                                                                             // 187
  }                                                                                               // 188
};                                                                                                // 189
                                                                                                  // 190
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/templating/scanner_tests.js                                                           //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
Tinytest.add("templating - html scanner", function (test) {                                       // 1
  var testInString = function(actualStr, wantedContents) {                                        // 2
    if (actualStr.indexOf(wantedContents) >= 0)                                                   // 3
      test.ok();                                                                                  // 4
    else                                                                                          // 5
      test.fail("Expected "+JSON.stringify(wantedContents)+                                       // 6
                " in "+JSON.stringify(actualStr));                                                // 7
  };                                                                                              // 8
                                                                                                  // 9
  var checkError = function(f, msgText, lineNum) {                                                // 10
    try {                                                                                         // 11
      f();                                                                                        // 12
    } catch (e) {                                                                                 // 13
      if (e.line === lineNum)                                                                     // 14
        test.ok();                                                                                // 15
      else                                                                                        // 16
        test.fail("Error should have been on line " + lineNum + ", not " +                        // 17
                  e.line);                                                                        // 18
      testInString(e.message, msgText);                                                           // 19
      return;                                                                                     // 20
    }                                                                                             // 21
    test.fail("Parse error didn't throw exception");                                              // 22
  };                                                                                              // 23
                                                                                                  // 24
  // returns the appropriate code to put content in the body,                                     // 25
  // where content is something simple like the string "Hello"                                    // 26
  // (passed in as a source string including the quotes).                                         // 27
  var simpleBody = function (content) {                                                           // 28
    return "\nUI.body.contentParts.push(UI.Component.extend({render: (function() {\n  var self = this;\n  return " + content + ";\n})}));\nMeteor.startup(function () { if (! UI.body.INSTANTIATED) { UI.body.INSTANTIATED = true; UI.DomRange.insert(UI.render(UI.body).dom, document.body); } });\n";
  };                                                                                              // 30
                                                                                                  // 31
  // arguments are quoted strings like '"hello"'                                                  // 32
  var simpleTemplate = function (templateName, content) {                                         // 33
    return '\nTemplate.__define__(' + templateName + ', (function() {\n  var self = this;\n  var template = this;\n  return ' + content + ';\n}));\n';
  };                                                                                              // 35
                                                                                                  // 36
  var checkResults = function(results, expectJs, expectHead) {                                    // 37
    test.equal(results.body, '');                                                                 // 38
    test.equal(results.js, expectJs || '');                                                       // 39
    test.equal(results.head, expectHead || '');                                                   // 40
  };                                                                                              // 41
                                                                                                  // 42
  checkError(function() {                                                                         // 43
    return html_scanner.scan("asdf");                                                             // 44
  }, "formatting in HTML template", 1);                                                           // 45
                                                                                                  // 46
  // body all on one line                                                                         // 47
  checkResults(                                                                                   // 48
    html_scanner.scan("<body>Hello</body>"),                                                      // 49
    simpleBody('"Hello"'));                                                                       // 50
                                                                                                  // 51
  // multi-line body, contents trimmed                                                            // 52
  checkResults(                                                                                   // 53
    html_scanner.scan("\n\n\n<body>\n\nHello\n\n</body>\n\n\n"),                                  // 54
    simpleBody('"Hello"'));                                                                       // 55
                                                                                                  // 56
  // same as previous, but with various HTML comments                                             // 57
  checkResults(                                                                                   // 58
    html_scanner.scan("\n<!--\n\nfoo\n-->\n<!-- -->\n"+                                           // 59
                      "<body>\n\nHello\n\n</body>\n\n<!----\n>\n\n"),                             // 60
    simpleBody('"Hello"'));                                                                       // 61
                                                                                                  // 62
  // head and body                                                                                // 63
  checkResults(                                                                                   // 64
    html_scanner.scan("<head>\n<title>Hello</title>\n</head>\n\n<body>World</body>\n\n"),         // 65
    simpleBody('"World"'),                                                                        // 66
    "<title>Hello</title>");                                                                      // 67
                                                                                                  // 68
  // head and body with tag whitespace                                                            // 69
  checkResults(                                                                                   // 70
    html_scanner.scan("<head\n>\n<title>Hello</title>\n</head  >\n\n<body>World</body\n\n>\n\n"), // 71
    simpleBody('"World"'),                                                                        // 72
    "<title>Hello</title>");                                                                      // 73
                                                                                                  // 74
  // head, body, and template                                                                     // 75
  checkResults(                                                                                   // 76
    html_scanner.scan("<head>\n<title>Hello</title>\n</head>\n\n<body>World</body>\n\n"+          // 77
                      '<template name="favoritefood">\n  pizza\n</template>\n'),                  // 78
    simpleBody('"World"') + simpleTemplate('"favoritefood"', '"pizza"'),                          // 79
    "<title>Hello</title>");                                                                      // 80
                                                                                                  // 81
  // one-line template                                                                            // 82
  checkResults(                                                                                   // 83
    html_scanner.scan('<template name="favoritefood">pizza</template>'),                          // 84
    simpleTemplate('"favoritefood"', '"pizza"'));                                                 // 85
                                                                                                  // 86
  // template with other attributes                                                               // 87
  checkResults(                                                                                   // 88
    html_scanner.scan('<template foo="bar" name="favoritefood" baz="qux">'+                       // 89
                      'pizza</template>'),                                                        // 90
    simpleTemplate('"favoritefood"', '"pizza"'));                                                 // 91
                                                                                                  // 92
  // whitespace around '=' in attributes and at end of tag                                        // 93
  checkResults(                                                                                   // 94
    html_scanner.scan('<template foo = "bar" name  ="favoritefood" baz= "qux"  >'+                // 95
                      'pizza</template\n\n>'),                                                    // 96
    simpleTemplate('"favoritefood"', '"pizza"'));                                                 // 97
                                                                                                  // 98
  // whitespace around template name                                                              // 99
  checkResults(                                                                                   // 100
    html_scanner.scan('<template name=" favoritefood  ">pizza</template>'),                       // 101
    simpleTemplate('"favoritefood"', '"pizza"'));                                                 // 102
                                                                                                  // 103
  // single quotes around template name                                                           // 104
  checkResults(                                                                                   // 105
    html_scanner.scan('<template name=\'the "cool" template\'>'+                                  // 106
                      'pizza</template>'),                                                        // 107
    simpleTemplate('"the \\"cool\\" template"', '"pizza"'));                                      // 108
                                                                                                  // 109
  // error cases; exact line numbers are not critical, these just reflect                         // 110
  // the current implementation                                                                   // 111
                                                                                                  // 112
  // unclosed body (error mentions body)                                                          // 113
  checkError(function() {                                                                         // 114
    return html_scanner.scan("\n\n<body>\n  Hello\n</body");                                      // 115
  }, "body", 3);                                                                                  // 116
                                                                                                  // 117
  // bad open tag                                                                                 // 118
  checkError(function() {                                                                         // 119
    return html_scanner.scan("\n\n\n<bodyd>\n  Hello\n</body>");                                  // 120
  }, "formatting in HTML template", 4);                                                           // 121
  checkError(function() {                                                                         // 122
    return html_scanner.scan("\n\n\n\n<body foo=>\n  Hello\n</body>");                            // 123
  }, "error in tag", 5);                                                                          // 124
                                                                                                  // 125
  // unclosed tag                                                                                 // 126
  checkError(function() {                                                                         // 127
    return html_scanner.scan("\n<body>Hello");                                                    // 128
  }, "nclosed", 2);                                                                               // 129
                                                                                                  // 130
  // unnamed template                                                                             // 131
  checkError(function() {                                                                         // 132
    return html_scanner.scan(                                                                     // 133
      "\n\n<template>Hi</template>\n\n<template>Hi</template>");                                  // 134
  }, "name", 3);                                                                                  // 135
                                                                                                  // 136
  // helpful doctype message                                                                      // 137
  checkError(function() {                                                                         // 138
    return html_scanner.scan(                                                                     // 139
      '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '+                                       // 140
        '"http://www.w3.org/TR/html4/strict.dtd">'+                                               // 141
        '\n\n<head>\n</head>');                                                                   // 142
  }, "DOCTYPE", 1);                                                                               // 143
                                                                                                  // 144
  // lowercase basic doctype                                                                      // 145
  checkError(function() {                                                                         // 146
    return html_scanner.scan(                                                                     // 147
      '<!doctype html>');                                                                         // 148
  }, "DOCTYPE", 1);                                                                               // 149
                                                                                                  // 150
  // attributes on body not supported                                                             // 151
  checkError(function() {                                                                         // 152
    return html_scanner.scan('<body foo="bar">\n  Hello\n</body>');                               // 153
  }, "<body>", 1);                                                                                // 154
                                                                                                  // 155
  // attributes on head not supported                                                             // 156
  checkError(function() {                                                                         // 157
    return html_scanner.scan('<head foo="bar">\n  Hello\n</head>');                               // 158
  }, "<head>", 1);                                                                                // 159
                                                                                                  // 160
  // can't mismatch quotes                                                                        // 161
  checkError(function() {                                                                         // 162
    return html_scanner.scan('<template name="foo\'>'+                                            // 163
                             'pizza</template>');                                                 // 164
  }, "error in tag", 1);                                                                          // 165
                                                                                                  // 166
});                                                                                               // 167
                                                                                                  // 168
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
