(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// packages/templating/global_template_object.js                                                                      //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
// Create an empty template object. Packages and apps add templates on                                                // 1
// to this object.                                                                                                    // 2
Template = {};                                                                                                        // 3
                                                                                                                      // 4
Template.__define__ = function (templateName, renderFunc) {                                                           // 5
  if (Template.hasOwnProperty(templateName))                                                                          // 6
    throw new Error("There are multiple templates named '" + templateName + "'. Each template needs a unique name."); // 7
                                                                                                                      // 8
  Template[templateName] = UI.Component.extend({                                                                      // 9
    kind: "Template_" + templateName,                                                                                 // 10
    render: renderFunc,                                                                                               // 11
    __helperHost: true                                                                                                // 12
  });                                                                                                                 // 13
};                                                                                                                    // 14
                                                                                                                      // 15
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
