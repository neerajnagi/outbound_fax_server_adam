(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var _ = Package.underscore._;

/* Package-scope variables */
var CssTools, UglifyJSMinify, MinifyAst;

(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/minifiers/minification.js                                                                                //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
                                                                                                                     // 1
// Stringifier based on css-stringify                                                                                // 2
var emit = function (str) {                                                                                          // 3
  return str.toString();                                                                                             // 4
};                                                                                                                   // 5
                                                                                                                     // 6
var visit = function (node, last) {                                                                                  // 7
  return traverse[node.type](node, last);                                                                            // 8
};                                                                                                                   // 9
                                                                                                                     // 10
var mapVisit = function (nodes) {                                                                                    // 11
  var buf = "";                                                                                                      // 12
                                                                                                                     // 13
  for (var i = 0, length = nodes.length; i < length; i++) {                                                          // 14
    buf += visit(nodes[i], i === length - 1);                                                                        // 15
  }                                                                                                                  // 16
                                                                                                                     // 17
  return buf;                                                                                                        // 18
};                                                                                                                   // 19
                                                                                                                     // 20
MinifyAst = function(node) {                                                                                         // 21
  return node.stylesheet                                                                                             // 22
    .rules.map(function (rule) { return visit(rule); })                                                              // 23
    .join('');                                                                                                       // 24
};                                                                                                                   // 25
                                                                                                                     // 26
var traverse = {};                                                                                                   // 27
                                                                                                                     // 28
traverse.comment = function(node) {                                                                                  // 29
  return emit('', node.position);                                                                                    // 30
};                                                                                                                   // 31
                                                                                                                     // 32
traverse.import = function(node) {                                                                                   // 33
  return emit('@import ' + node.import + ';', node.position);                                                        // 34
};                                                                                                                   // 35
                                                                                                                     // 36
traverse.media = function(node) {                                                                                    // 37
  return emit('@media ' + node.media, node.position, true)                                                           // 38
    + emit('{')                                                                                                      // 39
    + mapVisit(node.rules)                                                                                           // 40
    + emit('}');                                                                                                     // 41
};                                                                                                                   // 42
                                                                                                                     // 43
traverse.document = function(node) {                                                                                 // 44
  var doc = '@' + (node.vendor || '') + 'document ' + node.document;                                                 // 45
                                                                                                                     // 46
  return emit(doc, node.position, true)                                                                              // 47
    + emit('{')                                                                                                      // 48
    + mapVisit(node.rules)                                                                                           // 49
    + emit('}');                                                                                                     // 50
};                                                                                                                   // 51
                                                                                                                     // 52
traverse.charset = function(node) {                                                                                  // 53
  return emit('@charset ' + node.charset + ';', node.position);                                                      // 54
};                                                                                                                   // 55
                                                                                                                     // 56
traverse.namespace = function(node) {                                                                                // 57
  return emit('@namespace ' + node.namespace + ';', node.position);                                                  // 58
};                                                                                                                   // 59
                                                                                                                     // 60
traverse.supports = function(node){                                                                                  // 61
  return emit('@supports ' + node.supports, node.position, true)                                                     // 62
    + emit('{')                                                                                                      // 63
    + mapVisit(node.rules)                                                                                           // 64
    + emit('}');                                                                                                     // 65
};                                                                                                                   // 66
                                                                                                                     // 67
traverse.keyframes = function(node) {                                                                                // 68
  return emit('@'                                                                                                    // 69
    + (node.vendor || '')                                                                                            // 70
    + 'keyframes '                                                                                                   // 71
    + node.name, node.position, true)                                                                                // 72
    + emit('{')                                                                                                      // 73
    + mapVisit(node.keyframes)                                                                                       // 74
    + emit('}');                                                                                                     // 75
};                                                                                                                   // 76
                                                                                                                     // 77
traverse.keyframe = function(node) {                                                                                 // 78
  var decls = node.declarations;                                                                                     // 79
                                                                                                                     // 80
  return emit(node.values.join(','), node.position, true)                                                            // 81
    + emit('{')                                                                                                      // 82
    + mapVisit(decls)                                                                                                // 83
    + emit('}');                                                                                                     // 84
};                                                                                                                   // 85
                                                                                                                     // 86
traverse.page = function(node) {                                                                                     // 87
  var sel = node.selectors.length                                                                                    // 88
    ? node.selectors.join(', ')                                                                                      // 89
    : '';                                                                                                            // 90
                                                                                                                     // 91
  return emit('@page ' + sel, node.position, true)                                                                   // 92
    + emit('{')                                                                                                      // 93
    + mapVisit(node.declarations)                                                                                    // 94
    + emit('}');                                                                                                     // 95
};                                                                                                                   // 96
                                                                                                                     // 97
traverse.rule = function(node) {                                                                                     // 98
  var decls = node.declarations;                                                                                     // 99
  if (!decls.length) return '';                                                                                      // 100
                                                                                                                     // 101
  var selectors = node.selectors.map(function (selector) {                                                           // 102
    // removes universal selectors like *.class => .class                                                            // 103
    // removes optional whitespace around '>' and '+'                                                                // 104
    return selector.replace(/\*\./, '.')                                                                             // 105
                   .replace(/\s*>\s*/g, '>')                                                                         // 106
                   .replace(/\s*\+\s*/g, '+');                                                                       // 107
  });                                                                                                                // 108
  return emit(selectors.join(','), node.position, true)                                                              // 109
    + emit('{')                                                                                                      // 110
    + mapVisit(decls)                                                                                                // 111
    + emit('}');                                                                                                     // 112
};                                                                                                                   // 113
                                                                                                                     // 114
traverse.declaration = function(node, last) {                                                                        // 115
  var value = node.value;                                                                                            // 116
                                                                                                                     // 117
  // remove optional quotes around font name                                                                         // 118
  if (node.property === 'font') {                                                                                    // 119
    value = value.replace(/\'[^\']+\'/g, function (m) {                                                              // 120
      if (m.indexOf(' ') !== -1)                                                                                     // 121
        return m;                                                                                                    // 122
      return m.replace(/\'/g, '');                                                                                   // 123
    });                                                                                                              // 124
    value = value.replace(/\"[^\"]+\"/g, function (m) {                                                              // 125
      if (m.indexOf(' ') !== -1)                                                                                     // 126
        return m;                                                                                                    // 127
      return m.replace(/\"/g, '');                                                                                   // 128
    });                                                                                                              // 129
  }                                                                                                                  // 130
  // remove url quotes if possible                                                                                   // 131
  // in case it is the last declaration, we can omit the semicolon                                                   // 132
  return emit(node.property + ':' + value, node.position)                                                            // 133
         + (last ? '' : emit(';'));                                                                                  // 134
};                                                                                                                   // 135
                                                                                                                     // 136
                                                                                                                     // 137
                                                                                                                     // 138
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/minifiers/minifiers.js                                                                                   //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
UglifyJSMinify = Npm.require('uglify-js').minify;                                                                    // 1
                                                                                                                     // 2
var cssParse = Npm.require('css-parse');                                                                             // 3
var cssStringify = Npm.require('css-stringify');                                                                     // 4
                                                                                                                     // 5
CssTools = {                                                                                                         // 6
  parseCss: cssParse,                                                                                                // 7
  stringifyCss: cssStringify,                                                                                        // 8
  minifyCss: function (cssText) {                                                                                    // 9
    return CssTools.minifyCssAst(cssParse(cssText));                                                                 // 10
  },                                                                                                                 // 11
  minifyCssAst: function (cssAst) {                                                                                  // 12
    return MinifyAst(cssAst);                                                                                        // 13
  },                                                                                                                 // 14
  mergeCssAsts: function (cssAsts, warnCb) {                                                                         // 15
    var rulesPredicate = function (rules) {                                                                          // 16
      if (! _.isArray(rules))                                                                                        // 17
        rules = [rules];                                                                                             // 18
      return function (node) {                                                                                       // 19
        return _.contains(rules, node.type);                                                                         // 20
      }                                                                                                              // 21
    };                                                                                                               // 22
                                                                                                                     // 23
    // Simple concatenation of CSS files would break @import rules                                                   // 24
    // located in the beginning of a file. Before concatenation, pull them to                                        // 25
    // the beginning of a new syntax tree so they always precede other rules.                                        // 26
    var newAst = {                                                                                                   // 27
      type: 'stylesheet',                                                                                            // 28
      stylesheet: { rules: [] }                                                                                      // 29
    };                                                                                                               // 30
                                                                                                                     // 31
    _.each(cssAsts, function (ast) {                                                                                 // 32
      // Pick only the imports from the beginning of file ignoring @charset                                          // 33
      // rules as every file is assumed to be in UTF-8.                                                              // 34
      var charsetRules = _.filter(ast.stylesheet.rules,                                                              // 35
                                  rulesPredicate("charset"));                                                        // 36
                                                                                                                     // 37
      if (_.any(charsetRules, function (rule) {                                                                      // 38
        // According to MDN, only 'UTF-8' and "UTF-8" are the correct encoding                                       // 39
        // directives representing UTF-8.                                                                            // 40
        return ! /^(['"])UTF-8\1$/.test(rule.charset);                                                               // 41
      })) {                                                                                                          // 42
        warnCb(ast.filename, "@charset rules in this file will be ignored as UTF-8 is the only encoding supported"); // 43
      }                                                                                                              // 44
                                                                                                                     // 45
      ast.stylesheet.rules = _.reject(ast.stylesheet.rules,                                                          // 46
                                      rulesPredicate("charset"));                                                    // 47
      var importCount = 0;                                                                                           // 48
      for (var i = 0; i < ast.stylesheet.rules.length; i++)                                                          // 49
        if (! rulesPredicate(["import", "comment"])(ast.stylesheet.rules[i])) {                                      // 50
          importCount = i;                                                                                           // 51
          break;                                                                                                     // 52
        }                                                                                                            // 53
                                                                                                                     // 54
      var imports = ast.stylesheet.rules.splice(0, importCount);                                                     // 55
      newAst.stylesheet.rules = newAst.stylesheet.rules.concat(imports);                                             // 56
                                                                                                                     // 57
      // if there are imports left in the middle of file, warn user as it might                                      // 58
      // be a potential bug (imports are valid only in the beginning of file).                                       // 59
      if (_.any(ast.stylesheet.rules, rulesPredicate("import"))) {                                                   // 60
        // XXX make this an error?                                                                                   // 61
        warnCb(ast.filename, "there are some @import rules those are not taking effect as they are required to be in the beginning of the file");
      }                                                                                                              // 63
                                                                                                                     // 64
    });                                                                                                              // 65
                                                                                                                     // 66
    // Now we can put the rest of CSS rules into new AST                                                             // 67
    _.each(cssAsts, function (ast) {                                                                                 // 68
      newAst.stylesheet.rules =                                                                                      // 69
        newAst.stylesheet.rules.concat(ast.stylesheet.rules);                                                        // 70
    });                                                                                                              // 71
                                                                                                                     // 72
    return newAst;                                                                                                   // 73
  }                                                                                                                  // 74
};                                                                                                                   // 75
                                                                                                                     // 76
                                                                                                                     // 77
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.minifiers = {
  CssTools: CssTools,
  UglifyJSMinify: UglifyJSMinify
};

})();
