(function () {

/* Imports */
var Spacebars = Package.spacebars.Spacebars;
var HTMLTools = Package['html-tools'].HTMLTools;

/* Package-scope variables */
var html_scanner;

(function () {

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
// plugin/html_scanner.js                                                         //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////
                                                                                  //
html_scanner = {                                                                  // 1
  // Scan a template file for <head>, <body>, and <template>                      // 2
  // tags and extract their contents.                                             // 3
  //                                                                              // 4
  // This is a primitive, regex-based scanner.  It scans                          // 5
  // top-level tags, which are allowed to have attributes,                        // 6
  // and ignores top-level HTML comments.                                         // 7
                                                                                  // 8
  // Has fields 'message', 'line', 'file'                                         // 9
  ParseError: function () {                                                       // 10
  },                                                                              // 11
                                                                                  // 12
  scan: function (contents, source_name) {                                        // 13
    var rest = contents;                                                          // 14
    var index = 0;                                                                // 15
                                                                                  // 16
    var advance = function(amount) {                                              // 17
      rest = rest.substring(amount);                                              // 18
      index += amount;                                                            // 19
    };                                                                            // 20
                                                                                  // 21
    var throwParseError = function (msg, overrideIndex) {                         // 22
      var ret = new html_scanner.ParseError;                                      // 23
      ret.message = msg || "bad formatting in HTML template";                     // 24
      ret.file = source_name;                                                     // 25
      var theIndex = (typeof overrideIndex === 'number' ? overrideIndex : index); // 26
      ret.line = contents.substring(0, theIndex).split('\n').length;              // 27
      throw ret;                                                                  // 28
    };                                                                            // 29
                                                                                  // 30
    var results = html_scanner._initResults();                                    // 31
    var rOpenTag = /^((<(template|head|body)\b)|(<!--)|(<!DOCTYPE|{{!)|$)/i;      // 32
                                                                                  // 33
    while (rest) {                                                                // 34
      // skip whitespace first (for better line numbers)                          // 35
      advance(rest.match(/^\s*/)[0].length);                                      // 36
                                                                                  // 37
      var match = rOpenTag.exec(rest);                                            // 38
      if (! match)                                                                // 39
        throwParseError(); // unknown text encountered                            // 40
                                                                                  // 41
      var matchToken = match[1];                                                  // 42
      var matchTokenTagName =  match[3];                                          // 43
      var matchTokenComment = match[4];                                           // 44
      var matchTokenUnsupported = match[5];                                       // 45
                                                                                  // 46
      var tagStartIndex = index;                                                  // 47
      advance(match.index + match[0].length);                                     // 48
                                                                                  // 49
      if (! matchToken)                                                           // 50
        break; // matched $ (end of file)                                         // 51
      if (matchTokenComment === '<!--') {                                         // 52
        // top-level HTML comment                                                 // 53
        var commentEnd = /--\s*>/.exec(rest);                                     // 54
        if (! commentEnd)                                                         // 55
          throwParseError("unclosed HTML comment");                               // 56
        advance(commentEnd.index + commentEnd[0].length);                         // 57
        continue;                                                                 // 58
      }                                                                           // 59
      if (matchTokenUnsupported) {                                                // 60
        switch (matchTokenUnsupported.toLowerCase()) {                            // 61
        case '<!doctype':                                                         // 62
          throwParseError(                                                        // 63
            "Can't set DOCTYPE here.  (Meteor sets <!DOCTYPE html> for you)");    // 64
        case '{{!':                                                               // 65
          throwParseError(                                                        // 66
            "Can't use '{{! }}' outside a template.  Use '<!-- -->'.");           // 67
        }                                                                         // 68
        throwParseError();                                                        // 69
      }                                                                           // 70
                                                                                  // 71
      // otherwise, a <tag>                                                       // 72
      var tagName = matchTokenTagName.toLowerCase();                              // 73
      var tagAttribs = {}; // bare name -> value dict                             // 74
      var rTagPart = /^\s*((([a-zA-Z0-9:_-]+)\s*=\s*(["'])(.*?)\4)|(>))/;         // 75
      var attr;                                                                   // 76
      // read attributes                                                          // 77
      while ((attr = rTagPart.exec(rest))) {                                      // 78
        var attrToken = attr[1];                                                  // 79
        var attrKey = attr[3];                                                    // 80
        var attrValue = attr[5];                                                  // 81
        advance(attr.index + attr[0].length);                                     // 82
        if (attrToken === '>')                                                    // 83
          break;                                                                  // 84
        // XXX we don't HTML unescape the attribute value                         // 85
        // (e.g. to allow "abcd&quot;efg") or protect against                     // 86
        // collisions with methods of tagAttribs (e.g. for                        // 87
        // a property named toString)                                             // 88
        attrValue = attrValue.match(/^\s*([\s\S]*?)\s*$/)[1]; // trim             // 89
        tagAttribs[attrKey] = attrValue;                                          // 90
      }                                                                           // 91
      if (! attr) // didn't end on '>'                                            // 92
        throwParseError("Parse error in tag");                                    // 93
      // find </tag>                                                              // 94
      var end = (new RegExp('</'+tagName+'\\s*>', 'i')).exec(rest);               // 95
      if (! end)                                                                  // 96
        throwParseError("unclosed <"+tagName+">");                                // 97
      var tagContents = rest.slice(0, end.index);                                 // 98
      var contentsStartIndex = index;                                             // 99
                                                                                  // 100
      // act on the tag                                                           // 101
      html_scanner._handleTag(results, tagName, tagAttribs, tagContents,          // 102
                              throwParseError, contentsStartIndex,                // 103
                              tagStartIndex);                                     // 104
                                                                                  // 105
      // advance afterwards, so that line numbers in errors are correct           // 106
      advance(end.index + end[0].length);                                         // 107
    }                                                                             // 108
                                                                                  // 109
    return results;                                                               // 110
  },                                                                              // 111
                                                                                  // 112
  _initResults: function() {                                                      // 113
    var results = {};                                                             // 114
    results.head = '';                                                            // 115
    results.body = '';                                                            // 116
    results.js = '';                                                              // 117
    return results;                                                               // 118
  },                                                                              // 119
                                                                                  // 120
  _handleTag: function (results, tag, attribs, contents, throwParseError,         // 121
                        contentsStartIndex, tagStartIndex) {                      // 122
                                                                                  // 123
    // trim the tag contents.                                                     // 124
    // this is a courtesy and is also relied on by some unit tests.               // 125
    var m = contents.match(/^([ \t\r\n]*)([\s\S]*?)[ \t\r\n]*$/);                 // 126
    contentsStartIndex += m[1].length;                                            // 127
    contents = m[2];                                                              // 128
                                                                                  // 129
    // do we have 1 or more attribs?                                              // 130
    var hasAttribs = false;                                                       // 131
    for(var k in attribs) {                                                       // 132
      if (attribs.hasOwnProperty(k)) {                                            // 133
        hasAttribs = true;                                                        // 134
        break;                                                                    // 135
      }                                                                           // 136
    }                                                                             // 137
                                                                                  // 138
    if (tag === "head") {                                                         // 139
      if (hasAttribs)                                                             // 140
        throwParseError("Attributes on <head> not supported");                    // 141
      results.head += contents;                                                   // 142
      return;                                                                     // 143
    }                                                                             // 144
                                                                                  // 145
                                                                                  // 146
    // <body> or <template>                                                       // 147
                                                                                  // 148
    try {                                                                         // 149
      if (tag === "template") {                                                   // 150
        var name = attribs.name;                                                  // 151
        if (! name)                                                               // 152
          throwParseError("Template has no 'name' attribute");                    // 153
                                                                                  // 154
        if (Spacebars.isReservedName(name))                                       // 155
          throwParseError("Template can't be named \"" + name + "\"");            // 156
                                                                                  // 157
        var renderFuncCode = Spacebars.compile(                                   // 158
          contents, {                                                             // 159
            isTemplate: true,                                                     // 160
            sourceName: 'Template "' + name + '"'                                 // 161
          });                                                                     // 162
                                                                                  // 163
        results.js += "\nTemplate.__define__(" + JSON.stringify(name) +           // 164
          ", " + renderFuncCode + ");\n";                                         // 165
      } else {                                                                    // 166
        // <body>                                                                 // 167
        if (hasAttribs)                                                           // 168
          throwParseError("Attributes on <body> not supported");                  // 169
                                                                                  // 170
        var renderFuncCode = Spacebars.compile(                                   // 171
          contents, {                                                             // 172
            isBody: true,                                                         // 173
            sourceName: "<body>"                                                  // 174
          });                                                                     // 175
                                                                                  // 176
        // We may be one of many `<body>` tags.                                   // 177
        results.js += "\nUI.body.contentParts.push(UI.Component.extend({render: " + renderFuncCode + "}));\nMeteor.startup(function () { if (! UI.body.INSTANTIATED) { UI.body.INSTANTIATED = true; UI.DomRange.insert(UI.render(UI.body).dom, document.body); } });\n";
      }                                                                           // 179
    } catch (e) {                                                                 // 180
      if (e.scanner) {                                                            // 181
        // The error came from Spacebars                                          // 182
        throwParseError(e.message, contentsStartIndex + e.offset);                // 183
      } else {                                                                    // 184
        throw e;                                                                  // 185
      }                                                                           // 186
    }                                                                             // 187
  }                                                                               // 188
};                                                                                // 189
                                                                                  // 190
////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
// plugin/compile-templates.js                                                    //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////
                                                                                  //
var path = Npm.require('path');                                                   // 1
                                                                                  // 2
var doHTMLScanning = function (compileStep, htmlScanner) {                        // 3
                                                                                  // 4
  // XXX use archinfo rather than rolling our own                                 // 5
  if (! compileStep.arch.match(/^browser(\.|$)/))                                 // 6
    // XXX might be nice to throw an error here, but then we'd have to            // 7
    // make it so that packages.js ignores html files that appear in              // 8
    // the server directories in an app tree.. or, it might be nice to            // 9
    // make html files actually work on the server (against jsdom or              // 10
    // something)                                                                 // 11
    return;                                                                       // 12
                                                                                  // 13
  // XXX the way we deal with encodings here is sloppy .. should get              // 14
  // religion on that                                                             // 15
  var contents = compileStep.read().toString('utf8');                             // 16
  try {                                                                           // 17
    var results = htmlScanner.scan(contents, compileStep.inputPath);              // 18
  } catch (e) {                                                                   // 19
    if (e instanceof htmlScanner.ParseError) {                                    // 20
      compileStep.error({                                                         // 21
        message: e.message,                                                       // 22
        sourcePath: compileStep.inputPath,                                        // 23
        line: e.line                                                              // 24
      });                                                                         // 25
      return;                                                                     // 26
    } else                                                                        // 27
      throw e;                                                                    // 28
  }                                                                               // 29
                                                                                  // 30
  if (results.head)                                                               // 31
    compileStep.appendDocument({ section: "head", data: results.head });          // 32
                                                                                  // 33
  if (results.body)                                                               // 34
    compileStep.appendDocument({ section: "body", data: results.body });          // 35
                                                                                  // 36
  if (results.js) {                                                               // 37
    var path_part = path.dirname(compileStep.inputPath);                          // 38
    if (path_part === '.')                                                        // 39
      path_part = '';                                                             // 40
    if (path_part.length && path_part !== path.sep)                               // 41
      path_part = path_part + path.sep;                                           // 42
    var ext = path.extname(compileStep.inputPath);                                // 43
    var basename = path.basename(compileStep.inputPath, ext);                     // 44
                                                                                  // 45
    // XXX generate a source map                                                  // 46
                                                                                  // 47
    compileStep.addJavaScript({                                                   // 48
      path: path.join(path_part, "template." + basename + ".js"),                 // 49
      sourcePath: compileStep.inputPath,                                          // 50
      data: results.js                                                            // 51
    });                                                                           // 52
  }                                                                               // 53
};                                                                                // 54
                                                                                  // 55
Plugin.registerSourceHandler("html", function (compileStep) {                     // 56
  doHTMLScanning(compileStep, html_scanner);                                      // 57
});                                                                               // 58
                                                                                  // 59
////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.compileTemplates = {};

})();
