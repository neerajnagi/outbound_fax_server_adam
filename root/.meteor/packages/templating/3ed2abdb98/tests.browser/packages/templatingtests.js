(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/templating/templating_tests.js                                                                      //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
// render and put in the document                                                                               // 1
var renderToDiv = function (comp) {                                                                             // 2
  var div = document.createElement("DIV");                                                                      // 3
  UI.materialize(comp, div);                                                                                    // 4
  return div;                                                                                                   // 5
};                                                                                                              // 6
                                                                                                                // 7
// for events to bubble an element needs to be in the DOM.                                                      // 8
// @return {Function} call this for cleanup                                                                     // 9
var addToBody = function (el) {                                                                                 // 10
  el.style.display = "none";                                                                                    // 11
  document.body.appendChild(el);                                                                                // 12
  return function () {                                                                                          // 13
    document.body.removeChild(el);                                                                              // 14
  };                                                                                                            // 15
};                                                                                                              // 16
                                                                                                                // 17
                                                                                                                // 18
Tinytest.add("templating - assembly", function (test) {                                                         // 19
                                                                                                                // 20
  // Test for a bug that made it to production -- after a replacement,                                          // 21
  // we need to also check the newly replaced node for replacements                                             // 22
  var div = renderToDiv(Template.test_assembly_a0);                                                             // 23
  test.equal(canonicalizeHtml(div.innerHTML),                                                                   // 24
               "Hi");                                                                                           // 25
                                                                                                                // 26
  // Another production bug -- we must use LiveRange to replace the                                             // 27
  // placeholder, or risk breaking other LiveRanges                                                             // 28
  Session.set("stuff", true); // XXX bad form to use Session in a test?                                         // 29
  Template.test_assembly_b1.stuff = function () {                                                               // 30
    return Session.get("stuff");                                                                                // 31
  };                                                                                                            // 32
  var onscreen = renderToDiv(Template.test_assembly_b0);                                                        // 33
  test.equal(canonicalizeHtml(onscreen.innerHTML), "xyhi");                                                     // 34
  Session.set("stuff", false);                                                                                  // 35
  Deps.flush();                                                                                                 // 36
  test.equal(canonicalizeHtml(onscreen.innerHTML), "xhi");                                                      // 37
  Deps.flush();                                                                                                 // 38
});                                                                                                             // 39
                                                                                                                // 40
// Test that if a template throws an error, then pending_partials is                                            // 41
// cleaned up properly (that template rendering doesn't break..)                                                // 42
                                                                                                                // 43
                                                                                                                // 44
                                                                                                                // 45
                                                                                                                // 46
                                                                                                                // 47
                                                                                                                // 48
Tinytest.add("templating - table assembly", function(test) {                                                    // 49
  var childWithTag = function(node, tag) {                                                                      // 50
    return _.find(node.childNodes, function(n) {                                                                // 51
      return n.nodeName === tag;                                                                                // 52
    });                                                                                                         // 53
  };                                                                                                            // 54
                                                                                                                // 55
  var table;                                                                                                    // 56
  table = childWithTag(renderToDiv(Template.test_table_a0), "TABLE");                                           // 57
                                                                                                                // 58
  // table.rows is a great test, as it fails not only when TR/TD tags are                                       // 59
  // stripped due to improper html-to-fragment, but also when they are present                                  // 60
  // but don't show up because we didn't create a TBODY for IE.                                                 // 61
  test.equal(table.rows.length, 3);                                                                             // 62
                                                                                                                // 63
  // this time with an explicit TBODY                                                                           // 64
  table = childWithTag(renderToDiv(Template.test_table_b0), "TABLE");                                           // 65
  test.equal(table.rows.length, 3);                                                                             // 66
                                                                                                                // 67
  var c = new LocalCollection();                                                                                // 68
  c.insert({bar:'a'});                                                                                          // 69
  c.insert({bar:'b'});                                                                                          // 70
  c.insert({bar:'c'});                                                                                          // 71
  var onscreen = renderToDiv(Template.test_table_each.extend({data: {foo: c.find()}}));                         // 72
  table = childWithTag(onscreen, "TABLE");                                                                      // 73
                                                                                                                // 74
  test.equal(table.rows.length, 3, table.parentNode.innerHTML);                                                 // 75
  var tds = onscreen.getElementsByTagName("TD");                                                                // 76
  test.equal(tds.length, 3);                                                                                    // 77
  test.equal(canonicalizeHtml(tds[0].innerHTML), "a");                                                          // 78
  test.equal(canonicalizeHtml(tds[1].innerHTML), "b");                                                          // 79
  test.equal(canonicalizeHtml(tds[2].innerHTML), "c");                                                          // 80
                                                                                                                // 81
  Deps.flush();                                                                                                 // 82
});                                                                                                             // 83
                                                                                                                // 84
Tinytest.add("templating - event handler this", function(test) {                                                // 85
                                                                                                                // 86
  Template.test_event_data_with.ONE = {str: "one"};                                                             // 87
  Template.test_event_data_with.TWO = {str: "two"};                                                             // 88
  Template.test_event_data_with.THREE = {str: "three"};                                                         // 89
                                                                                                                // 90
  Template.test_event_data_with.events({                                                                        // 91
    'click': function(event, template) {                                                                        // 92
      test.isTrue(this.str);                                                                                    // 93
      test.equal(template.data.str, "one");                                                                     // 94
      event_buf.push(this.str);                                                                                 // 95
    }                                                                                                           // 96
  });                                                                                                           // 97
                                                                                                                // 98
  var event_buf = [];                                                                                           // 99
  var containerDiv = renderToDiv(Template.test_event_data_with.extend({data:                                    // 100
    Template.test_event_data_with.ONE}));                                                                       // 101
  var cleanupDiv = addToBody(containerDiv);                                                                     // 102
                                                                                                                // 103
  var divs = containerDiv.getElementsByTagName("div");                                                          // 104
  test.equal(3, divs.length);                                                                                   // 105
                                                                                                                // 106
  clickElement(divs[0]);                                                                                        // 107
  test.equal(event_buf, ['one']);                                                                               // 108
  event_buf.length = 0;                                                                                         // 109
                                                                                                                // 110
  clickElement(divs[1]);                                                                                        // 111
  test.equal(event_buf, ['two']);                                                                               // 112
  event_buf.length = 0;                                                                                         // 113
                                                                                                                // 114
  clickElement(divs[2]);                                                                                        // 115
  test.equal(event_buf, ['three']);                                                                             // 116
  event_buf.length = 0;                                                                                         // 117
                                                                                                                // 118
  cleanupDiv();                                                                                                 // 119
  Deps.flush();                                                                                                 // 120
});                                                                                                             // 121
                                                                                                                // 122
                                                                                                                // 123
if (document.addEventListener) {                                                                                // 124
  // Only run this test on browsers with support for event                                                      // 125
  // capturing. A more detailed analysis can be found at                                                        // 126
  // https://www.meteor.com/blog/2013/09/06/browser-events-bubbling-capturing-and-delegation                    // 127
                                                                                                                // 128
  // This is related to issue at https://gist.github.com/mquandalle/8157017                                     // 129
  // Tests two situations related to events that can only be captured, not bubbled:                             // 130
  // 1. Event should only fire the handler that matches the selector given                                      // 131
  // 2. Event should work on every element in the selector and not just the first element                       // 132
  // This test isn't written against mouseenter because it is synthesized by jQuery,                            // 133
  // the bug also happened with the play event                                                                  // 134
  Tinytest.add("templating - capturing events", function (test) {                                               // 135
    var video1Played = 0,                                                                                       // 136
        video2Played = 0;                                                                                       // 137
                                                                                                                // 138
    Template.test_capture_events.events({                                                                       // 139
      'play .video1': function () {                                                                             // 140
        video1Played++;                                                                                         // 141
      },                                                                                                        // 142
      'play .video2': function () {                                                                             // 143
        video2Played++;                                                                                         // 144
      }                                                                                                         // 145
    });                                                                                                         // 146
                                                                                                                // 147
    // add to body or else events don't actually fire                                                           // 148
    var containerDiv = renderToDiv(Template.test_capture_events);                                               // 149
    var cleanupDiv = addToBody(containerDiv);                                                                   // 150
                                                                                                                // 151
    var checkAndResetEvents = function(video1, video2) {                                                        // 152
      test.equal(video1Played, video1);                                                                         // 153
      test.equal(video2Played, video2);                                                                         // 154
                                                                                                                // 155
      video1Played = 0;                                                                                         // 156
      video2Played = 0;                                                                                         // 157
    };                                                                                                          // 158
                                                                                                                // 159
    simulateEvent($(containerDiv).find(".video1").get(0),                                                       // 160
                  "play", {}, {bubbles: false});                                                                // 161
    checkAndResetEvents(1, 0);                                                                                  // 162
                                                                                                                // 163
    simulateEvent($(containerDiv).find(".video2").get(0),                                                       // 164
                  "play", {}, {bubbles: false});                                                                // 165
    checkAndResetEvents(0, 1);                                                                                  // 166
                                                                                                                // 167
    simulateEvent($(containerDiv).find(".video2").get(1),                                                       // 168
                  "play", {}, {bubbles: false});                                                                // 169
    checkAndResetEvents(0, 1);                                                                                  // 170
                                                                                                                // 171
    // clean up DOM                                                                                             // 172
    cleanupDiv();                                                                                               // 173
    Deps.flush();                                                                                               // 174
  });                                                                                                           // 175
}                                                                                                               // 176
                                                                                                                // 177
Tinytest.add("templating - safestring", function(test) {                                                        // 178
                                                                                                                // 179
  Template.test_safestring_a.foo = function() {                                                                 // 180
    return "<br>";                                                                                              // 181
  };                                                                                                            // 182
  Template.test_safestring_a.bar = function() {                                                                 // 183
    return new Spacebars.SafeString("<hr>");                                                                    // 184
  };                                                                                                            // 185
                                                                                                                // 186
  var obj = {fooprop: "<br>",                                                                                   // 187
             barprop: new Spacebars.SafeString("<hr>")};                                                        // 188
  var html = canonicalizeHtml(                                                                                  // 189
    renderToDiv(Template.test_safestring_a.extend({data: obj})).innerHTML);                                     // 190
                                                                                                                // 191
  test.equal(html,                                                                                              // 192
             "&lt;br&gt;<br><hr><hr>"+                                                                          // 193
             "&lt;br&gt;<br><hr><hr>");                                                                         // 194
                                                                                                                // 195
});                                                                                                             // 196
                                                                                                                // 197
Tinytest.add("templating - helpers and dots", function(test) {                                                  // 198
  UI.registerHelper("platypus", function() {                                                                    // 199
    return "eggs";                                                                                              // 200
  });                                                                                                           // 201
  UI.registerHelper("watermelon", function() {                                                                  // 202
    return "seeds";                                                                                             // 203
  });                                                                                                           // 204
                                                                                                                // 205
  UI.registerHelper("daisygetter", function() {                                                                 // 206
    return this.daisy;                                                                                          // 207
  });                                                                                                           // 208
                                                                                                                // 209
  // XXX for debugging                                                                                          // 210
  UI.registerHelper("debugger", function() {                                                                    // 211
    debugger;                                                                                                   // 212
  });                                                                                                           // 213
                                                                                                                // 214
  var getFancyObject = function() {                                                                             // 215
    return {                                                                                                    // 216
      foo: 'bar',                                                                                               // 217
      apple: {banana: 'smoothie'},                                                                              // 218
      currentFruit: function() {                                                                                // 219
        return 'guava';                                                                                         // 220
      },                                                                                                        // 221
      currentCountry: function() {                                                                              // 222
        return {name: 'Iceland',                                                                                // 223
                _pop: 321007,                                                                                   // 224
                population: function() {                                                                        // 225
                  return this._pop;                                                                             // 226
                },                                                                                              // 227
                unicorns: 0, // falsy value                                                                     // 228
                daisyGetter: function() {                                                                       // 229
                  return this.daisy;                                                                            // 230
                }                                                                                               // 231
               };                                                                                               // 232
      }                                                                                                         // 233
    };                                                                                                          // 234
  };                                                                                                            // 235
                                                                                                                // 236
  UI.registerHelper("fancyhelper", getFancyObject);                                                             // 237
                                                                                                                // 238
  Template.test_helpers_a.platypus = 'bill';                                                                    // 239
  Template.test_helpers_a.warthog = function() {                                                                // 240
    return 'snout';                                                                                             // 241
  };                                                                                                            // 242
                                                                                                                // 243
  var listFour = function(a, b, c, d, options) {                                                                // 244
    test.isTrue(options instanceof Spacebars.kw);                                                               // 245
    var keywordArgs = _.map(_.keys(options.hash), function(k) {                                                 // 246
      var val = options.hash[k];                                                                                // 247
      return k+':'+val;                                                                                         // 248
    });                                                                                                         // 249
    return [a, b, c, d].concat(keywordArgs).join(' ');                                                          // 250
  };                                                                                                            // 251
                                                                                                                // 252
  var dataObj = {                                                                                               // 253
    zero: 0,                                                                                                    // 254
    platypus: 'weird',                                                                                          // 255
    watermelon: 'rind',                                                                                         // 256
    daisy: 'petal',                                                                                             // 257
    tree: function() { return 'leaf'; },                                                                        // 258
    thisTest: function() { return this.tree(); },                                                               // 259
    getNull: function() { return null; },                                                                       // 260
    getUndefined: function () { return; },                                                                      // 261
    fancy: getFancyObject(),                                                                                    // 262
    methodListFour: listFour                                                                                    // 263
  };                                                                                                            // 264
                                                                                                                // 265
  var html;                                                                                                     // 266
  html = canonicalizeHtml(                                                                                      // 267
    renderToDiv(Template.test_helpers_a.extend({data: dataObj})).innerHTML);                                    // 268
  test.equal(html.match(/\S+/g), [                                                                              // 269
    'platypus=bill', // helpers on Template object take first priority                                          // 270
    'watermelon=seeds', // global helpers take second priority                                                  // 271
    'daisy=petal', // unshadowed object property                                                                // 272
    'tree=leaf', // function object property                                                                    // 273
    'warthog=snout' // function Template property                                                               // 274
  ]);                                                                                                           // 275
                                                                                                                // 276
  html = canonicalizeHtml(                                                                                      // 277
    renderToDiv(Template.test_helpers_b.extend({data: dataObj})).innerHTML);                                    // 278
  test.equal(html.match(/\S+/g), [                                                                              // 279
    // unknown properties silently fail                                                                         // 280
    'unknown=',                                                                                                 // 281
    // falsy property comes through                                                                             // 282
    'zero=0'                                                                                                    // 283
  ]);                                                                                                           // 284
                                                                                                                // 285
  html = canonicalizeHtml(                                                                                      // 286
    renderToDiv(Template.test_helpers_c.extend({data: dataObj})).innerHTML);                                    // 287
  test.equal(html.match(/\S+/g), [                                                                              // 288
    // property gets are supposed to silently fail                                                              // 289
    'platypus.X=',                                                                                              // 290
    'watermelon.X=',                                                                                            // 291
    'daisy.X=',                                                                                                 // 292
    'tree.X=',                                                                                                  // 293
    'warthog.X=',                                                                                               // 294
    'getNull.X=',                                                                                               // 295
    'getUndefined.X=',                                                                                          // 296
    'getUndefined.X.Y='                                                                                         // 297
  ]);                                                                                                           // 298
                                                                                                                // 299
  html = canonicalizeHtml(                                                                                      // 300
    renderToDiv(Template.test_helpers_d.extend({data: dataObj})).innerHTML);                                    // 301
  test.equal(html.match(/\S+/g), [                                                                              // 302
    // helpers should get current data context in `this`                                                        // 303
    'daisygetter=petal',                                                                                        // 304
    // object methods should get object in `this`                                                               // 305
    'thisTest=leaf',                                                                                            // 306
    // nesting inside {{#with fancy}} shouldn't affect                                                          // 307
    // method                                                                                                   // 308
    '../thisTest=leaf',                                                                                         // 309
    // combine .. and .                                                                                         // 310
    '../fancy.currentFruit=guava'                                                                               // 311
  ]);                                                                                                           // 312
                                                                                                                // 313
  html = canonicalizeHtml(                                                                                      // 314
    renderToDiv(Template.test_helpers_e.extend({data: dataObj})).innerHTML);                                    // 315
  test.equal(html.match(/\S+/g), [                                                                              // 316
    'fancy.foo=bar',                                                                                            // 317
    'fancy.apple.banana=smoothie',                                                                              // 318
    'fancy.currentFruit=guava',                                                                                 // 319
    'fancy.currentCountry.name=Iceland',                                                                        // 320
    'fancy.currentCountry.population=321007',                                                                   // 321
    'fancy.currentCountry.unicorns=0'                                                                           // 322
  ]);                                                                                                           // 323
                                                                                                                // 324
  html = canonicalizeHtml(                                                                                      // 325
    renderToDiv(Template.test_helpers_f.extend({data: dataObj})).innerHTML);                                    // 326
  test.equal(html.match(/\S+/g), [                                                                              // 327
    'fancyhelper.foo=bar',                                                                                      // 328
    'fancyhelper.apple.banana=smoothie',                                                                        // 329
    'fancyhelper.currentFruit=guava',                                                                           // 330
    'fancyhelper.currentCountry.name=Iceland',                                                                  // 331
    'fancyhelper.currentCountry.population=321007',                                                             // 332
    'fancyhelper.currentCountry.unicorns=0'                                                                     // 333
  ]);                                                                                                           // 334
                                                                                                                // 335
  // test significance of 'this', which prevents helper from                                                    // 336
  // shadowing property                                                                                         // 337
  html = canonicalizeHtml(                                                                                      // 338
    renderToDiv(Template.test_helpers_g.extend({data: dataObj})).innerHTML);                                    // 339
  test.equal(html.match(/\S+/g), [                                                                              // 340
    'platypus=eggs',                                                                                            // 341
    'this.platypus=weird'                                                                                       // 342
  ]);                                                                                                           // 343
                                                                                                                // 344
  // test interpretation of arguments                                                                           // 345
                                                                                                                // 346
  Template.test_helpers_h.helperListFour = listFour;                                                            // 347
                                                                                                                // 348
  html = canonicalizeHtml(                                                                                      // 349
    renderToDiv(Template.test_helpers_h.extend({data: dataObj})).innerHTML);                                    // 350
  var trials =                                                                                                  // 351
        html.match(/\(.*?\)/g);                                                                                 // 352
  test.equal(trials[0],                                                                                         // 353
             '(methodListFour 6 7 8 9=6 7 8 9)');                                                               // 354
  test.equal(trials[1],                                                                                         // 355
             '(methodListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns=eggs leaf guava 0)');
  test.equal(trials[2],                                                                                         // 357
             '(methodListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns a=platypus b=thisTest c=fancyhelper.currentFruit d=fancyhelper.currentCountry.unicorns=eggs leaf guava 0 a:eggs b:leaf c:guava d:0)');
  test.equal(trials[3],                                                                                         // 359
             '(helperListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns=eggs leaf guava 0)');
  test.equal(trials[4],                                                                                         // 361
             '(helperListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns a=platypus b=thisTest c=fancyhelper.currentFruit d=fancyhelper.currentCountry.unicorns=eggs leaf guava 0 a:eggs b:leaf c:guava d:0)');
  test.equal(trials.length, 5);                                                                                 // 363
                                                                                                                // 364
});                                                                                                             // 365
                                                                                                                // 366
                                                                                                                // 367
Tinytest.add("templating - rendered template", function(test) {                                                 // 368
  var R = ReactiveVar('foo');                                                                                   // 369
  Template.test_render_a.foo = function() {                                                                     // 370
    R.get();                                                                                                    // 371
    return this.x + 1;                                                                                          // 372
  };                                                                                                            // 373
                                                                                                                // 374
  var div = renderToDiv(Template.test_render_a.extend({data: {x: 123}}));                                       // 375
  test.equal($(div).text().match(/\S+/)[0], "124");                                                             // 376
                                                                                                                // 377
  var br1 = div.getElementsByTagName('br')[0];                                                                  // 378
  var hr1 = div.getElementsByTagName('hr')[0];                                                                  // 379
  test.isTrue(br1);                                                                                             // 380
  test.isTrue(hr1);                                                                                             // 381
                                                                                                                // 382
  R.set('bar');                                                                                                 // 383
  Deps.flush();                                                                                                 // 384
  var br2 = div.getElementsByTagName('br')[0];                                                                  // 385
  var hr2 = div.getElementsByTagName('hr')[0];                                                                  // 386
  test.isTrue(br2);                                                                                             // 387
  test.isTrue(br1 === br2);                                                                                     // 388
  test.isTrue(hr2);                                                                                             // 389
  test.isTrue(hr1 === hr2);                                                                                     // 390
                                                                                                                // 391
  Deps.flush();                                                                                                 // 392
                                                                                                                // 393
  /////                                                                                                         // 394
                                                                                                                // 395
  R = ReactiveVar('foo');                                                                                       // 396
                                                                                                                // 397
  Template.test_render_b.foo = function() {                                                                     // 398
    R.get();                                                                                                    // 399
    return (+this) + 1;                                                                                         // 400
  };                                                                                                            // 401
                                                                                                                // 402
  div = renderToDiv(Template.test_render_b.extend({data: {x: 123}}));                                           // 403
  test.equal($(div).text().match(/\S+/)[0], "201");                                                             // 404
                                                                                                                // 405
  var br1 = div.getElementsByTagName('br')[0];                                                                  // 406
  var hr1 = div.getElementsByTagName('hr')[0];                                                                  // 407
  test.isTrue(br1);                                                                                             // 408
  test.isTrue(hr1);                                                                                             // 409
                                                                                                                // 410
  R.set('bar');                                                                                                 // 411
  Deps.flush();                                                                                                 // 412
  var br2 = div.getElementsByTagName('br')[0];                                                                  // 413
  var hr2 = div.getElementsByTagName('hr')[0];                                                                  // 414
  test.isTrue(br2);                                                                                             // 415
  test.isTrue(br1 === br2);                                                                                     // 416
  test.isTrue(hr2);                                                                                             // 417
  test.isTrue(hr1 === hr2);                                                                                     // 418
                                                                                                                // 419
  Deps.flush();                                                                                                 // 420
                                                                                                                // 421
});                                                                                                             // 422
                                                                                                                // 423
Tinytest.add("templating - template arg", function (test) {                                                     // 424
  Template.test_template_arg_a.events({                                                                         // 425
    click: function (event, template) {                                                                         // 426
      template.firstNode.innerHTML = 'Hello';                                                                   // 427
      template.lastNode.innerHTML = 'World';                                                                    // 428
      template.find('i').innerHTML =                                                                            // 429
        (template.findAll('*').length)+"-element";                                                              // 430
      template.lastNode.innerHTML += ' (the secret is '+                                                        // 431
        template.secret+')';                                                                                    // 432
    }                                                                                                           // 433
  });                                                                                                           // 434
                                                                                                                // 435
  Template.test_template_arg_a.created = function() {                                                           // 436
    var self = this;                                                                                            // 437
    test.isFalse(self.firstNode);                                                                               // 438
    test.isFalse(self.lastNode);                                                                                // 439
    test.throws(function () { return self.find("*"); });                                                        // 440
    test.throws(function () { return self.findAll("*"); });                                                     // 441
  };                                                                                                            // 442
                                                                                                                // 443
  Template.test_template_arg_a.rendered = function () {                                                         // 444
    var template = this;                                                                                        // 445
    template.firstNode.innerHTML = 'Greetings';                                                                 // 446
    template.lastNode.innerHTML = 'Line';                                                                       // 447
    template.find('i').innerHTML =                                                                              // 448
      (template.findAll('b').length)+"-bold";                                                                   // 449
    template.secret = "strawberry "+template.data.food;                                                         // 450
  };                                                                                                            // 451
                                                                                                                // 452
  Template.test_template_arg_a.destroyed = function() {                                                         // 453
    var self = this;                                                                                            // 454
    test.isFalse(self.firstNode);                                                                               // 455
    test.isFalse(self.lastNode);                                                                                // 456
    test.throws(function () { return self.find("*"); });                                                        // 457
    test.throws(function () { return self.findAll("*"); });                                                     // 458
  };                                                                                                            // 459
                                                                                                                // 460
  var div = renderToDiv(Template.test_template_arg_a.extend({data: {food: "pie"}}));                            // 461
  var cleanupDiv = addToBody(div);                                                                              // 462
  Deps.flush(); // cause `rendered` to be called                                                                // 463
  test.equal($(div).text(), "Greetings 1-bold Line");                                                           // 464
  clickElement(div.querySelector('i'));                                                                         // 465
  test.equal($(div).text(), "Hello 3-element World (the secret is strawberry pie)");                            // 466
                                                                                                                // 467
  cleanupDiv();                                                                                                 // 468
  Deps.flush();                                                                                                 // 469
});                                                                                                             // 470
                                                                                                                // 471
Tinytest.add("templating - helpers", function (test) {                                                          // 472
  var tmpl = Template.test_template_helpers_a;                                                                  // 473
                                                                                                                // 474
  tmpl.foo = 'z';                                                                                               // 475
  tmpl.helpers({bar: 'b'});                                                                                     // 476
  // helpers(...) takes precendence of assigned helper                                                          // 477
  tmpl.helpers({foo: 'a', baz: function() { return 'c'; }});                                                    // 478
                                                                                                                // 479
  var div = renderToDiv(tmpl);                                                                                  // 480
  test.equal($(div).text().match(/\S+/)[0], 'abc');                                                             // 481
  Deps.flush();                                                                                                 // 482
                                                                                                                // 483
  tmpl = Template.test_template_helpers_b;                                                                      // 484
                                                                                                                // 485
  tmpl.helpers({                                                                                                // 486
    'name': 'A',                                                                                                // 487
    'arity': 'B',                                                                                               // 488
    'toString': 'C',                                                                                            // 489
    'length': 4,                                                                                                // 490
    'var': 'D'                                                                                                  // 491
  });                                                                                                           // 492
                                                                                                                // 493
  div = renderToDiv(tmpl);                                                                                      // 494
  var txt = $(div).text();                                                                                      // 495
  txt = txt.replace('[object Object]', 'X'); // IE 8                                                            // 496
  txt = txt.match(/\S+/)[0];                                                                                    // 497
  test.isTrue(txt.match(/^AB[CX]4D$/));                                                                         // 498
  // We don't make helpers with names like toString work in IE 8.                                               // 499
  test.expect_fail();                                                                                           // 500
  test.equal(txt, 'ABC4D');                                                                                     // 501
  Deps.flush();                                                                                                 // 502
                                                                                                                // 503
  // test that helpers don't "leak"                                                                             // 504
  tmpl = Template.test_template_helpers_c;                                                                      // 505
  div = renderToDiv(tmpl);                                                                                      // 506
  test.equal($(div).text(), 'x');                                                                               // 507
  Deps.flush();                                                                                                 // 508
});                                                                                                             // 509
                                                                                                                // 510
Tinytest.add("templating - events", function (test) {                                                           // 511
  var tmpl = Template.test_template_events_a;                                                                   // 512
                                                                                                                // 513
  var buf = [];                                                                                                 // 514
                                                                                                                // 515
  // old style                                                                                                  // 516
  tmpl.events = {                                                                                               // 517
    'click b': function () { buf.push('b'); }                                                                   // 518
  };                                                                                                            // 519
                                                                                                                // 520
  var div = renderToDiv(tmpl);                                                                                  // 521
  var cleanupDiv = addToBody(div);                                                                              // 522
  clickElement($(div).find('b')[0]);                                                                            // 523
  test.equal(buf, ['b']);                                                                                       // 524
  cleanupDiv();                                                                                                 // 525
  Deps.flush();                                                                                                 // 526
                                                                                                                // 527
  ///                                                                                                           // 528
                                                                                                                // 529
  tmpl = Template.test_template_events_b;                                                                       // 530
  buf = [];                                                                                                     // 531
  // new style                                                                                                  // 532
  tmpl.events({                                                                                                 // 533
    'click u': function () { buf.push('u'); }                                                                   // 534
  });                                                                                                           // 535
  tmpl.events({                                                                                                 // 536
    'click i': function () { buf.push('i'); }                                                                   // 537
  });                                                                                                           // 538
                                                                                                                // 539
  div = renderToDiv(tmpl);                                                                                      // 540
  cleanupDiv = addToBody(div);                                                                                  // 541
  clickElement($(div).find('u')[0]);                                                                            // 542
  clickElement($(div).find('i')[0]);                                                                            // 543
  test.equal(buf, ['u', 'i']);                                                                                  // 544
  cleanupDiv();                                                                                                 // 545
  Deps.flush();                                                                                                 // 546
                                                                                                                // 547
  //Test for identical callbacks for issue #650                                                                 // 548
  tmpl = Template.test_template_events_c;                                                                       // 549
  buf = [];                                                                                                     // 550
  tmpl.events({                                                                                                 // 551
    'click u': function () { buf.push('a'); }                                                                   // 552
  });                                                                                                           // 553
  tmpl.events({                                                                                                 // 554
    'click u': function () { buf.push('b'); }                                                                   // 555
  });                                                                                                           // 556
                                                                                                                // 557
  div = renderToDiv(tmpl);                                                                                      // 558
  cleanupDiv = addToBody(div);                                                                                  // 559
  clickElement($(div).find('u')[0]);                                                                            // 560
  test.equal(buf.length, 2);                                                                                    // 561
  test.isTrue(_.contains(buf, 'a'));                                                                            // 562
  test.isTrue(_.contains(buf, 'b'));                                                                            // 563
  cleanupDiv();                                                                                                 // 564
  Deps.flush();                                                                                                 // 565
});                                                                                                             // 566
                                                                                                                // 567
                                                                                                                // 568
Tinytest.add('templating - helper typecast Issue #617', function (test) {                                       // 569
                                                                                                                // 570
  UI.registerHelper('testTypeCasting', function (/*arguments*/) {                                               // 571
    // Return a string representing the arguments passed to this                                                // 572
    // function, including types. eg:                                                                           // 573
    // (1, true) -> "[number,1][boolean,true]"                                                                  // 574
    return _.reduce(_.toArray(arguments), function (memo, arg) {                                                // 575
      if (typeof arg === 'object')                                                                              // 576
        return memo + "[object]";                                                                               // 577
      return memo + "[" + typeof arg + "," + arg + "]";                                                         // 578
    }, "");                                                                                                     // 579
    return x;                                                                                                   // 580
  });                                                                                                           // 581
                                                                                                                // 582
  var div = renderToDiv(Template.test_type_casting);                                                            // 583
  var result = canonicalizeHtml(div.innerHTML);                                                                 // 584
  test.equal(                                                                                                   // 585
    result,                                                                                                     // 586
    // This corresponds to entries in templating_tests.html.                                                    // 587
    // true/faslse                                                                                              // 588
    "[string,true][string,false][boolean,true][boolean,false]" +                                                // 589
      // numbers                                                                                                // 590
      "[number,0][number,1][number,-1][number,10][number,-10]" +                                                // 591
      // handlebars 'options' argument. appended to args of all helpers.                                        // 592
      "[object]");                                                                                              // 593
});                                                                                                             // 594
                                                                                                                // 595
Tinytest.add('templating - each falsy Issue #801', function (test) {                                            // 596
  //Minor test for issue #801 (#each over array containing nulls)                                               // 597
  Template.test_template_issue801.values = function() { return [0,1,2,null,undefined,false]; };                 // 598
  var div = renderToDiv(Template.test_template_issue801);                                                       // 599
  test.equal(canonicalizeHtml(div.innerHTML), "012");                                                           // 600
});                                                                                                             // 601
                                                                                                                // 602
Tinytest.add('templating - duplicate template error', function (test) {                                         // 603
  Template.__define__("test_duplicate_template", function () {});                                               // 604
  test.throws(function () {                                                                                     // 605
    Template.__define__("test_duplicate_template", function () {});                                             // 606
  });                                                                                                           // 607
});                                                                                                             // 608
                                                                                                                // 609
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/templating/template.templating_tests.js                                                             //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
                                                                                                                // 1
Template.__define__("test_assembly_a0", (function() {                                                           // 2
  var self = this;                                                                                              // 3
  var template = this;                                                                                          // 4
  return Spacebars.include(self.lookupTemplate("test_assembly_a1"));                                            // 5
}));                                                                                                            // 6
                                                                                                                // 7
Template.__define__("test_assembly_a1", (function() {                                                           // 8
  var self = this;                                                                                              // 9
  var template = this;                                                                                          // 10
  return Spacebars.include(self.lookupTemplate("test_assembly_a2"));                                            // 11
}));                                                                                                            // 12
                                                                                                                // 13
Template.__define__("test_assembly_a2", (function() {                                                           // 14
  var self = this;                                                                                              // 15
  var template = this;                                                                                          // 16
  return Spacebars.include(self.lookupTemplate("test_assembly_a3"));                                            // 17
}));                                                                                                            // 18
                                                                                                                // 19
Template.__define__("test_assembly_a3", (function() {                                                           // 20
  var self = this;                                                                                              // 21
  var template = this;                                                                                          // 22
  return "Hi";                                                                                                  // 23
}));                                                                                                            // 24
                                                                                                                // 25
Template.__define__("test_assembly_b0", (function() {                                                           // 26
  var self = this;                                                                                              // 27
  var template = this;                                                                                          // 28
  return Spacebars.include(self.lookupTemplate("test_assembly_b1"));                                            // 29
}));                                                                                                            // 30
                                                                                                                // 31
Template.__define__("test_assembly_b1", (function() {                                                           // 32
  var self = this;                                                                                              // 33
  var template = this;                                                                                          // 34
  return [ "x", UI.If(function() {                                                                              // 35
    return Spacebars.call(self.lookup("stuff"));                                                                // 36
  }, UI.block(function() {                                                                                      // 37
    var self = this;                                                                                            // 38
    return "y";                                                                                                 // 39
  })), Spacebars.include(self.lookupTemplate("test_assembly_b2")) ];                                            // 40
}));                                                                                                            // 41
                                                                                                                // 42
Template.__define__("test_assembly_b2", (function() {                                                           // 43
  var self = this;                                                                                              // 44
  var template = this;                                                                                          // 45
  return "hi";                                                                                                  // 46
}));                                                                                                            // 47
                                                                                                                // 48
Template.__define__("test_table_a0", (function() {                                                              // 49
  var self = this;                                                                                              // 50
  var template = this;                                                                                          // 51
  return HTML.TABLE("\n    ", Spacebars.include(self.lookupTemplate("test_table_a1")), "\n    ", Spacebars.include(self.lookupTemplate("test_table_a1")), "\n    ", Spacebars.include(self.lookupTemplate("test_table_a1")), "\n  ");
}));                                                                                                            // 53
                                                                                                                // 54
Template.__define__("test_table_a1", (function() {                                                              // 55
  var self = this;                                                                                              // 56
  var template = this;                                                                                          // 57
  return HTML.TR("\n    ", Spacebars.include(self.lookupTemplate("test_table_a2")), "\n  ");                    // 58
}));                                                                                                            // 59
                                                                                                                // 60
Template.__define__("test_table_a2", (function() {                                                              // 61
  var self = this;                                                                                              // 62
  var template = this;                                                                                          // 63
  return HTML.TD("\n    ", Spacebars.include(self.lookupTemplate("test_table_a3")), "\n  ");                    // 64
}));                                                                                                            // 65
                                                                                                                // 66
Template.__define__("test_table_a3", (function() {                                                              // 67
  var self = this;                                                                                              // 68
  var template = this;                                                                                          // 69
  return "Foo.";                                                                                                // 70
}));                                                                                                            // 71
                                                                                                                // 72
Template.__define__("test_table_b0", (function() {                                                              // 73
  var self = this;                                                                                              // 74
  var template = this;                                                                                          // 75
  return HTML.TABLE("\n    ", HTML.TBODY("\n      ", Spacebars.include(self.lookupTemplate("test_table_b1")), "\n      ", Spacebars.include(self.lookupTemplate("test_table_b1")), "\n      ", Spacebars.include(self.lookupTemplate("test_table_b1")), "\n    "), "\n  ");
}));                                                                                                            // 77
                                                                                                                // 78
Template.__define__("test_table_b1", (function() {                                                              // 79
  var self = this;                                                                                              // 80
  var template = this;                                                                                          // 81
  return HTML.TR("\n    ", Spacebars.include(self.lookupTemplate("test_table_b2")), "\n  ");                    // 82
}));                                                                                                            // 83
                                                                                                                // 84
Template.__define__("test_table_b2", (function() {                                                              // 85
  var self = this;                                                                                              // 86
  var template = this;                                                                                          // 87
  return HTML.TD("\n    ", Spacebars.include(self.lookupTemplate("test_table_b3")), "\n  ");                    // 88
}));                                                                                                            // 89
                                                                                                                // 90
Template.__define__("test_table_b3", (function() {                                                              // 91
  var self = this;                                                                                              // 92
  var template = this;                                                                                          // 93
  return "Foo.";                                                                                                // 94
}));                                                                                                            // 95
                                                                                                                // 96
Template.__define__("test_table_each", (function() {                                                            // 97
  var self = this;                                                                                              // 98
  var template = this;                                                                                          // 99
  return HTML.TABLE("\n    ", UI.Each(function() {                                                              // 100
    return Spacebars.call(self.lookup("foo"));                                                                  // 101
  }, UI.block(function() {                                                                                      // 102
    var self = this;                                                                                            // 103
    return [ "\n      ", HTML.TR(HTML.TD(function() {                                                           // 104
      return Spacebars.mustache(self.lookup("bar"));                                                            // 105
    })), "\n    " ];                                                                                            // 106
  })), "\n  ");                                                                                                 // 107
}));                                                                                                            // 108
                                                                                                                // 109
Template.__define__("test_event_data_with", (function() {                                                       // 110
  var self = this;                                                                                              // 111
  var template = this;                                                                                          // 112
  return HTML.DIV("\n  xxx\n  ", Spacebars.With(function() {                                                    // 113
    return Spacebars.call(self.lookup("TWO"));                                                                  // 114
  }, UI.block(function() {                                                                                      // 115
    var self = this;                                                                                            // 116
    return [ "\n    ", HTML.DIV("\n      xxx\n      ", Spacebars.With(function() {                              // 117
      return Spacebars.call(self.lookup("THREE"));                                                              // 118
    }, UI.block(function() {                                                                                    // 119
      var self = this;                                                                                          // 120
      return [ "\n        ", HTML.DIV("\n          xxx\n        "), "\n      " ];                               // 121
    })), "\n    "), "\n  " ];                                                                                   // 122
  })), "\n");                                                                                                   // 123
}));                                                                                                            // 124
                                                                                                                // 125
Template.__define__("test_capture_events", (function() {                                                        // 126
  var self = this;                                                                                              // 127
  var template = this;                                                                                          // 128
  return HTML.Raw('<video class="video1">\n    <source id="mp4" src="http://media.w3.org/2010/05/sintel/trailer.mp4" type="video/mp4">\n  </video>\n  <video class="video2">\n    <source id="mp4" src="http://media.w3.org/2010/05/sintel/trailer.mp4" type="video/mp4">\n  </video>\n  <video class="video2">\n    <source id="mp4" src="http://media.w3.org/2010/05/sintel/trailer.mp4" type="video/mp4">\n  </video>');
}));                                                                                                            // 130
                                                                                                                // 131
Template.__define__("test_safestring_a", (function() {                                                          // 132
  var self = this;                                                                                              // 133
  var template = this;                                                                                          // 134
  return [ function() {                                                                                         // 135
    return Spacebars.mustache(self.lookup("foo"));                                                              // 136
  }, " ", function() {                                                                                          // 137
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("foo")));                                           // 138
  }, " ", function() {                                                                                          // 139
    return Spacebars.mustache(self.lookup("bar"));                                                              // 140
  }, " ", function() {                                                                                          // 141
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("bar")));                                           // 142
  }, "\n  ", function() {                                                                                       // 143
    return Spacebars.mustache(self.lookup("fooprop"));                                                          // 144
  }, " ", function() {                                                                                          // 145
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("fooprop")));                                       // 146
  }, " ", function() {                                                                                          // 147
    return Spacebars.mustache(self.lookup("barprop"));                                                          // 148
  }, " ", function() {                                                                                          // 149
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("barprop")));                                       // 150
  } ];                                                                                                          // 151
}));                                                                                                            // 152
                                                                                                                // 153
Template.__define__("test_helpers_a", (function() {                                                             // 154
  var self = this;                                                                                              // 155
  var template = this;                                                                                          // 156
  return [ "platypus=", function() {                                                                            // 157
    return Spacebars.mustache(self.lookup("platypus"));                                                         // 158
  }, "\n  watermelon=", function() {                                                                            // 159
    return Spacebars.mustache(self.lookup("watermelon"));                                                       // 160
  }, "\n  daisy=", function() {                                                                                 // 161
    return Spacebars.mustache(self.lookup("daisy"));                                                            // 162
  }, "\n  tree=", function() {                                                                                  // 163
    return Spacebars.mustache(self.lookup("tree"));                                                             // 164
  }, "\n  warthog=", function() {                                                                               // 165
    return Spacebars.mustache(self.lookup("warthog"));                                                          // 166
  } ];                                                                                                          // 167
}));                                                                                                            // 168
                                                                                                                // 169
Template.__define__("test_helpers_b", (function() {                                                             // 170
  var self = this;                                                                                              // 171
  var template = this;                                                                                          // 172
  return [ "unknown=", function() {                                                                             // 173
    return Spacebars.mustache(self.lookup("unknown"));                                                          // 174
  }, "\n  zero=", function() {                                                                                  // 175
    return Spacebars.mustache(self.lookup("zero"));                                                             // 176
  } ];                                                                                                          // 177
}));                                                                                                            // 178
                                                                                                                // 179
Template.__define__("test_helpers_c", (function() {                                                             // 180
  var self = this;                                                                                              // 181
  var template = this;                                                                                          // 182
  return [ "platypus.X=", function() {                                                                          // 183
    return Spacebars.mustache(Spacebars.dot(self.lookup("platypus"), "X"));                                     // 184
  }, "\n  watermelon.X=", function() {                                                                          // 185
    return Spacebars.mustache(Spacebars.dot(self.lookup("watermelon"), "X"));                                   // 186
  }, "\n  daisy.X=", function() {                                                                               // 187
    return Spacebars.mustache(Spacebars.dot(self.lookup("daisy"), "X"));                                        // 188
  }, "\n  tree.X=", function() {                                                                                // 189
    return Spacebars.mustache(Spacebars.dot(self.lookup("tree"), "X"));                                         // 190
  }, "\n  warthog.X=", function() {                                                                             // 191
    return Spacebars.mustache(Spacebars.dot(self.lookup("warthog"), "X"));                                      // 192
  }, "\n  getNull.X=", function() {                                                                             // 193
    return Spacebars.mustache(Spacebars.dot(self.lookup("getNull"), "X"));                                      // 194
  }, "\n  getUndefined.X=", function() {                                                                        // 195
    return Spacebars.mustache(Spacebars.dot(self.lookup("getUndefined"), "X"));                                 // 196
  }, "\n  getUndefined.X.Y=", function() {                                                                      // 197
    return Spacebars.mustache(Spacebars.dot(self.lookup("getUndefined"), "X", "Y"));                            // 198
  } ];                                                                                                          // 199
}));                                                                                                            // 200
                                                                                                                // 201
Template.__define__("test_helpers_d", (function() {                                                             // 202
  var self = this;                                                                                              // 203
  var template = this;                                                                                          // 204
  return [ "daisygetter=", function() {                                                                         // 205
    return Spacebars.mustache(self.lookup("daisygetter"));                                                      // 206
  }, "\n  thisTest=", function() {                                                                              // 207
    return Spacebars.mustache(self.lookup("thisTest"));                                                         // 208
  }, "\n  ", Spacebars.With(function() {                                                                        // 209
    return Spacebars.call(self.lookup("fancy"));                                                                // 210
  }, UI.block(function() {                                                                                      // 211
    var self = this;                                                                                            // 212
    return [ "\n    ../thisTest=", function() {                                                                 // 213
      return Spacebars.mustache(Spacebars.dot(self.lookup(".."), "thisTest"));                                  // 214
    }, "\n  " ];                                                                                                // 215
  })), "\n  ", Spacebars.With(function() {                                                                      // 216
    return "foo";                                                                                               // 217
  }, UI.block(function() {                                                                                      // 218
    var self = this;                                                                                            // 219
    return [ "\n    ../fancy.currentFruit=", function() {                                                       // 220
      return Spacebars.mustache(Spacebars.dot(self.lookup(".."), "fancy", "currentFruit"));                     // 221
    }, "\n  " ];                                                                                                // 222
  })) ];                                                                                                        // 223
}));                                                                                                            // 224
                                                                                                                // 225
Template.__define__("test_helpers_e", (function() {                                                             // 226
  var self = this;                                                                                              // 227
  var template = this;                                                                                          // 228
  return [ "fancy.foo=", function() {                                                                           // 229
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "foo"));                                      // 230
  }, "\n  fancy.apple.banana=", function() {                                                                    // 231
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "apple", "banana"));                          // 232
  }, "\n  fancy.currentFruit=", function() {                                                                    // 233
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "currentFruit"));                             // 234
  }, "\n  fancy.currentCountry.name=", function() {                                                             // 235
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "currentCountry", "name"));                   // 236
  }, "\n  fancy.currentCountry.population=", function() {                                                       // 237
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "currentCountry", "population"));             // 238
  }, "\n  fancy.currentCountry.unicorns=", function() {                                                         // 239
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancy"), "currentCountry", "unicorns"));               // 240
  } ];                                                                                                          // 241
}));                                                                                                            // 242
                                                                                                                // 243
Template.__define__("test_helpers_f", (function() {                                                             // 244
  var self = this;                                                                                              // 245
  var template = this;                                                                                          // 246
  return [ "fancyhelper.foo=", function() {                                                                     // 247
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "foo"));                                // 248
  }, "\n  fancyhelper.apple.banana=", function() {                                                              // 249
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "apple", "banana"));                    // 250
  }, "\n  fancyhelper.currentFruit=", function() {                                                              // 251
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"));                       // 252
  }, "\n  fancyhelper.currentCountry.name=", function() {                                                       // 253
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "name"));             // 254
  }, "\n  fancyhelper.currentCountry.population=", function() {                                                 // 255
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "population"));       // 256
  }, "\n  fancyhelper.currentCountry.unicorns=", function() {                                                   // 257
    return Spacebars.mustache(Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns"));         // 258
  } ];                                                                                                          // 259
}));                                                                                                            // 260
                                                                                                                // 261
Template.__define__("test_helpers_g", (function() {                                                             // 262
  var self = this;                                                                                              // 263
  var template = this;                                                                                          // 264
  return [ "platypus=", function() {                                                                            // 265
    return Spacebars.mustache(self.lookup("platypus"));                                                         // 266
  }, "\n  this.platypus=", function() {                                                                         // 267
    return Spacebars.mustache(Spacebars.dot(self.lookup("."), "platypus"));                                     // 268
  } ];                                                                                                          // 269
}));                                                                                                            // 270
                                                                                                                // 271
Template.__define__("test_helpers_h", (function() {                                                             // 272
  var self = this;                                                                                              // 273
  var template = this;                                                                                          // 274
  return [ "(methodListFour 6 7 8 9=", function() {                                                             // 275
    return Spacebars.mustache(self.lookup("methodListFour"), 6, 7, 8, 9);                                       // 276
  }, ")\n  (methodListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns=", function() {
    return Spacebars.mustache(self.lookup("methodListFour"), self.lookup("platypus"), self.lookup("thisTest"), Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"), Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns"));
  }, ")\n  (methodListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns a=platypus b=thisTest c=fancyhelper.currentFruit d=fancyhelper.currentCountry.unicorns=", function() {
    return Spacebars.mustache(self.lookup("methodListFour"), self.lookup("platypus"), self.lookup("thisTest"), Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"), Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns"), Spacebars.kw({
      a: self.lookup("platypus"),                                                                               // 281
      b: self.lookup("thisTest"),                                                                               // 282
      c: Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"),                                             // 283
      d: Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns")                                // 284
    }));                                                                                                        // 285
  }, ")\n  (helperListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns=", function() {
    return Spacebars.mustache(self.lookup("helperListFour"), self.lookup("platypus"), self.lookup("thisTest"), Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"), Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns"));
  }, ")\n  (helperListFour platypus thisTest fancyhelper.currentFruit fancyhelper.currentCountry.unicorns a=platypus b=thisTest c=fancyhelper.currentFruit d=fancyhelper.currentCountry.unicorns=", function() {
    return Spacebars.mustache(self.lookup("helperListFour"), self.lookup("platypus"), self.lookup("thisTest"), Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"), Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns"), Spacebars.kw({
      a: self.lookup("platypus"),                                                                               // 290
      b: self.lookup("thisTest"),                                                                               // 291
      c: Spacebars.dot(self.lookup("fancyhelper"), "currentFruit"),                                             // 292
      d: Spacebars.dot(self.lookup("fancyhelper"), "currentCountry", "unicorns")                                // 293
    }));                                                                                                        // 294
  }, ")" ];                                                                                                     // 295
}));                                                                                                            // 296
                                                                                                                // 297
Template.__define__("test_render_a", (function() {                                                              // 298
  var self = this;                                                                                              // 299
  var template = this;                                                                                          // 300
  return [ function() {                                                                                         // 301
    return Spacebars.mustache(self.lookup("foo"));                                                              // 302
  }, HTML.Raw("<br><hr>") ];                                                                                    // 303
}));                                                                                                            // 304
                                                                                                                // 305
Template.__define__("test_render_b", (function() {                                                              // 306
  var self = this;                                                                                              // 307
  var template = this;                                                                                          // 308
  return Spacebars.With(function() {                                                                            // 309
    return 200;                                                                                                 // 310
  }, UI.block(function() {                                                                                      // 311
    var self = this;                                                                                            // 312
    return [ function() {                                                                                       // 313
      return Spacebars.mustache(self.lookup("foo"));                                                            // 314
    }, HTML.BR(), HTML.HR() ];                                                                                  // 315
  }));                                                                                                          // 316
}));                                                                                                            // 317
                                                                                                                // 318
Template.__define__("test_render_c", (function() {                                                              // 319
  var self = this;                                                                                              // 320
  var template = this;                                                                                          // 321
  return HTML.Raw("<br><hr>");                                                                                  // 322
}));                                                                                                            // 323
                                                                                                                // 324
Template.__define__("test_template_arg_a", (function() {                                                        // 325
  var self = this;                                                                                              // 326
  var template = this;                                                                                          // 327
  return HTML.Raw("<b>Foo</b> <i>Bar</i> <u>Baz</u>");                                                          // 328
}));                                                                                                            // 329
                                                                                                                // 330
Template.__define__("test_template_helpers_a", (function() {                                                    // 331
  var self = this;                                                                                              // 332
  var template = this;                                                                                          // 333
  return [ function() {                                                                                         // 334
    return Spacebars.mustache(self.lookup("foo"));                                                              // 335
  }, function() {                                                                                               // 336
    return Spacebars.mustache(self.lookup("bar"));                                                              // 337
  }, function() {                                                                                               // 338
    return Spacebars.mustache(self.lookup("baz"));                                                              // 339
  } ];                                                                                                          // 340
}));                                                                                                            // 341
                                                                                                                // 342
Template.__define__("test_template_helpers_b", (function() {                                                    // 343
  var self = this;                                                                                              // 344
  var template = this;                                                                                          // 345
  return [ function() {                                                                                         // 346
    return Spacebars.mustache(self.lookup("name"));                                                             // 347
  }, function() {                                                                                               // 348
    return Spacebars.mustache(self.lookup("arity"));                                                            // 349
  }, function() {                                                                                               // 350
    return Spacebars.mustache(self.lookup("toString"));                                                         // 351
  }, function() {                                                                                               // 352
    return Spacebars.mustache(self.lookup("length"));                                                           // 353
  }, function() {                                                                                               // 354
    return Spacebars.mustache(self.lookup("var"));                                                              // 355
  } ];                                                                                                          // 356
}));                                                                                                            // 357
                                                                                                                // 358
Template.__define__("test_template_helpers_c", (function() {                                                    // 359
  var self = this;                                                                                              // 360
  var template = this;                                                                                          // 361
  return [ function() {                                                                                         // 362
    return Spacebars.mustache(self.lookup("name"));                                                             // 363
  }, function() {                                                                                               // 364
    return Spacebars.mustache(self.lookup("arity"));                                                            // 365
  }, function() {                                                                                               // 366
    return Spacebars.mustache(self.lookup("length"));                                                           // 367
  }, function() {                                                                                               // 368
    return Spacebars.mustache(self.lookup("var"));                                                              // 369
  }, "x" ];                                                                                                     // 370
}));                                                                                                            // 371
                                                                                                                // 372
Template.__define__("test_template_events_a", (function() {                                                     // 373
  var self = this;                                                                                              // 374
  var template = this;                                                                                          // 375
  return HTML.Raw("<b>foo</b><u>bar</u><i>baz</i>");                                                            // 376
}));                                                                                                            // 377
                                                                                                                // 378
Template.__define__("test_template_events_b", (function() {                                                     // 379
  var self = this;                                                                                              // 380
  var template = this;                                                                                          // 381
  return HTML.Raw("<b>foo</b><u>bar</u><i>baz</i>");                                                            // 382
}));                                                                                                            // 383
                                                                                                                // 384
Template.__define__("test_template_events_c", (function() {                                                     // 385
  var self = this;                                                                                              // 386
  var template = this;                                                                                          // 387
  return HTML.Raw("<b>foo</b><u>bar</u><i>baz</i>");                                                            // 388
}));                                                                                                            // 389
                                                                                                                // 390
Template.__define__("test_type_casting", (function() {                                                          // 391
  var self = this;                                                                                              // 392
  var template = this;                                                                                          // 393
  return function() {                                                                                           // 394
    return Spacebars.mustache(self.lookup("testTypeCasting"), "true", "false", true, false, 0, 1, -1, 10, -10); // 395
  };                                                                                                            // 396
}));                                                                                                            // 397
                                                                                                                // 398
Template.__define__("test_template_issue801", (function() {                                                     // 399
  var self = this;                                                                                              // 400
  var template = this;                                                                                          // 401
  return UI.Each(function() {                                                                                   // 402
    return Spacebars.call(self.lookup("values"));                                                               // 403
  }, UI.block(function() {                                                                                      // 404
    var self = this;                                                                                            // 405
    return function() {                                                                                         // 406
      return Spacebars.mustache(self.lookup("."));                                                              // 407
    };                                                                                                          // 408
  }));                                                                                                          // 409
}));                                                                                                            // 410
                                                                                                                // 411
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
