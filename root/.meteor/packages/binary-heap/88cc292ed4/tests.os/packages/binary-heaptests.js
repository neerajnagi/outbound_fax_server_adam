(function () {

////////////////////////////////////////////////////////////////////////////
//                                                                        //
// packages/binary-heap/binary-heap-tests.js                              //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
                                                                          //
Tinytest.add("binary-heap - simple max-heap tests", function (test) {     // 1
  var h = new MaxHeap(function (a, b) { return a-b; });                   // 2
  h.set("a", 1);                                                          // 3
  h.set("b", 233);                                                        // 4
  h.set("c", -122);                                                       // 5
  h.set("d", 0);                                                          // 6
  h.set("e", 0);                                                          // 7
                                                                          // 8
  test.equal(h.size(), 5);                                                // 9
  test.equal(h.maxElementId(), "b");                                      // 10
  test.equal(h.get("b"), 233);                                            // 11
                                                                          // 12
  h.remove("b");                                                          // 13
  test.equal(h.size(), 4);                                                // 14
  test.equal(h.maxElementId(), "a");                                      // 15
  h.set("e", 44);                                                         // 16
  test.equal(h.maxElementId(), "e");                                      // 17
  test.equal(h.get("b"), null);                                           // 18
  test.isTrue(h.has("a"));                                                // 19
  test.isFalse(h.has("dd"));                                              // 20
                                                                          // 21
  h.clear();                                                              // 22
  test.isFalse(h.has("a"));                                               // 23
  test.equal(h.size(), 0);                                                // 24
  test.equal(h.setDefault("a", 12345), 12345);                            // 25
  test.equal(h.setDefault("a", 55555), 12345);                            // 26
  test.equal(h.size(), 1);                                                // 27
  test.equal(h.maxElementId(), "a");                                      // 28
});                                                                       // 29
                                                                          // 30
Tinytest.add("binary-heap - big test for max-heap", function (test) {     // 31
  var positiveNumbers = _.shuffle(_.range(1, 41));                        // 32
  var negativeNumbers = _.shuffle(_.range(-1, -41, -1));                  // 33
  var allNumbers = negativeNumbers.concat(positiveNumbers);               // 34
                                                                          // 35
  var heap = new MaxHeap(function (a, b) { return a-b; });                // 36
  var output = [];                                                        // 37
                                                                          // 38
  _.each(allNumbers, function (n) { heap.set(n, n); });                   // 39
                                                                          // 40
  _.times(positiveNumbers.length + negativeNumbers.length, function () {  // 41
    var maxId = heap.maxElementId();                                      // 42
    output.push(heap.get(maxId));                                         // 43
    heap.remove(maxId);                                                   // 44
  });                                                                     // 45
                                                                          // 46
  allNumbers.sort(function (a, b) { return b-a; });                       // 47
                                                                          // 48
  test.equal(output, allNumbers);                                         // 49
});                                                                       // 50
                                                                          // 51
Tinytest.add("binary-heap - min-max heap tests", function (test) {        // 52
  var h = new MinMaxHeap(function (a, b) { return a-b; });                // 53
  h.set("a", 1);                                                          // 54
  h.set("b", 233);                                                        // 55
  h.set("c", -122);                                                       // 56
  h.set("d", 0);                                                          // 57
  h.set("e", 0);                                                          // 58
                                                                          // 59
  test.equal(h.size(), 5);                                                // 60
  test.equal(h.maxElementId(), "b");                                      // 61
  test.equal(h.get("b"), 233);                                            // 62
  test.equal(h.minElementId(), "c");                                      // 63
                                                                          // 64
  h.remove("b");                                                          // 65
  test.equal(h.size(), 4);                                                // 66
  test.equal(h.minElementId(), "c");                                      // 67
  h.set("e", -123);                                                       // 68
  test.equal(h.minElementId(), "e");                                      // 69
  test.equal(h.get("b"), null);                                           // 70
  test.isTrue(h.has("a"));                                                // 71
  test.isFalse(h.has("dd"));                                              // 72
                                                                          // 73
  h.clear();                                                              // 74
  test.isFalse(h.has("a"));                                               // 75
  test.equal(h.size(), 0);                                                // 76
  test.equal(h.setDefault("a", 12345), 12345);                            // 77
  test.equal(h.setDefault("a", 55555), 12345);                            // 78
  test.equal(h.size(), 1);                                                // 79
  test.equal(h.maxElementId(), "a");                                      // 80
  test.equal(h.minElementId(), "a");                                      // 81
});                                                                       // 82
                                                                          // 83
Tinytest.add("binary-heap - big test for min-max-heap", function (test) { // 84
  var N = 500;                                                            // 85
  var positiveNumbers = _.shuffle(_.range(1, N + 1));                     // 86
  var negativeNumbers = _.shuffle(_.range(-1, -N - 1, -1));               // 87
  var allNumbers = positiveNumbers.concat(negativeNumbers);               // 88
                                                                          // 89
  var heap = new MinMaxHeap(function (a, b) { return a-b; });             // 90
  var output = [];                                                        // 91
                                                                          // 92
  var initialSets = _.clone(allNumbers);                                  // 93
  _.each(allNumbers, function (n) {                                       // 94
    heap.set(n, n);                                                       // 95
    heap._selfCheck();                                                    // 96
    heap._minHeap._selfCheck();                                           // 97
  });                                                                     // 98
                                                                          // 99
  allNumbers = _.shuffle(allNumbers);                                     // 100
  var secondarySets = _.clone(allNumbers);                                // 101
                                                                          // 102
  _.each(allNumbers, function (n) {                                       // 103
    heap.set(-n, n);                                                      // 104
    heap._selfCheck();                                                    // 105
    heap._minHeap._selfCheck();                                           // 106
  });                                                                     // 107
                                                                          // 108
  _.times(positiveNumbers.length + negativeNumbers.length, function () {  // 109
    var minId = heap.minElementId();                                      // 110
    output.push(heap.get(minId));                                         // 111
    heap.remove(minId);                                                   // 112
    heap._selfCheck(); heap._minHeap._selfCheck();                        // 113
  });                                                                     // 114
                                                                          // 115
  test.equal(heap.size(), 0);                                             // 116
                                                                          // 117
  allNumbers.sort(function (a, b) { return a-b; });                       // 118
                                                                          // 119
  var initialTestText = "initial sets: " + initialSets.toString() +       // 120
    "; secondary sets: " + secondarySets.toString();                      // 121
  test.equal(output, allNumbers, initialTestText);                        // 122
                                                                          // 123
  _.each(initialSets, function (n) { heap.set(n, n); })                   // 124
  _.each(secondarySets, function (n) { heap.set(-n, n); });               // 125
                                                                          // 126
  allNumbers.sort(function (a, b) { return b-a; });                       // 127
  output = [];                                                            // 128
  _.times(positiveNumbers.length + negativeNumbers.length, function () {  // 129
    var maxId = heap.maxElementId();                                      // 130
    output.push(heap.get(maxId));                                         // 131
    heap.remove(maxId);                                                   // 132
    heap._selfCheck(); heap._minHeap._selfCheck();                        // 133
  });                                                                     // 134
                                                                          // 135
  test.equal(output, allNumbers, initialTestText);                        // 136
});                                                                       // 137
                                                                          // 138
                                                                          // 139
////////////////////////////////////////////////////////////////////////////

}).call(this);
