(function () {

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
// packages/observe-sequence/observe_sequence_tests.js                                               //
//                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                     //
// Run a function named `run` which modifies a sequence. While it                                    // 1
// executes, observe changes to the sequence and accumulate them in an                               // 2
// array, canonicalizing as necessary. Then make sure the results are                                // 3
// the same as passed in `expectedCallbacks`. In items in                                            // 4
// `expectedCallbacks`, allow for special values of the form {NOT:                                   // 5
// "foo"}, which match anything other than "foo".                                                    // 6
//                                                                                                   // 7
// @param test {Object} as passed to Tinytest.add                                                    // 8
// @param sequenceFunc {Function(): sequence type}                                                   // 9
// @param run {Function()} modify the sequence or cause sequenceFunc                                 // 10
//     to be recomupted                                                                              // 11
// @param expectedCallbacks {Array}                                                                  // 12
//     elements are objects eg {addedAt: [array of arguments]}                                       // 13
// @param numExpectedWarnings {Number}                                                               // 14
runOneObserveSequenceTestCase = function (test, sequenceFunc,                                        // 15
                                          run, expectedCallbacks,                                    // 16
                                          numExpectedWarnings) {                                     // 17
  if (numExpectedWarnings)                                                                           // 18
    ObserveSequence._suppressWarnings += numExpectedWarnings;                                        // 19
                                                                                                     // 20
  var firedCallbacks = [];                                                                           // 21
  var handle = ObserveSequence.observe(sequenceFunc, {                                               // 22
    addedAt: function () {                                                                           // 23
      firedCallbacks.push({addedAt: _.toArray(arguments)});                                          // 24
    },                                                                                               // 25
    changed: function () {                                                                           // 26
      var obj = {changed: _.toArray(arguments)};                                                     // 27
                                                                                                     // 28
      // Browsers are inconsistent about the order in which 'changed'                                // 29
      // callbacks fire. To ensure consistent behavior of these tests,                               // 30
      // we can't simply push `obj` at the end of `firedCallbacks` as                                // 31
      // we do for the other callbacks. Instead, we use insertion sort                               // 32
      // to place `obj` in a canonical position within the chunk of                                  // 33
      // contiguously recently fired 'changed' callbacks.                                            // 34
      for (var i = firedCallbacks.length; i > 0; i--) {                                              // 35
                                                                                                     // 36
        var compareTo = firedCallbacks[i - 1];                                                       // 37
        if (!compareTo.changed)                                                                      // 38
          break;                                                                                     // 39
                                                                                                     // 40
        if (EJSON.stringify(compareTo, {canonical: true}) <                                          // 41
            EJSON.stringify(obj, {canonical: true}))                                                 // 42
          break;                                                                                     // 43
      }                                                                                              // 44
                                                                                                     // 45
      firedCallbacks.splice(i, 0, obj);                                                              // 46
    },                                                                                               // 47
    removed: function () {                                                                           // 48
      firedCallbacks.push({removed: _.toArray(arguments)});                                          // 49
    },                                                                                               // 50
    movedTo: function () {                                                                           // 51
      firedCallbacks.push({movedTo: _.toArray(arguments)});                                          // 52
    }                                                                                                // 53
  });                                                                                                // 54
                                                                                                     // 55
  run();                                                                                             // 56
  Deps.flush();                                                                                      // 57
  handle.stop();                                                                                     // 58
                                                                                                     // 59
  test.equal(ObserveSequence._suppressWarnings, 0);                                                  // 60
  test.equal(ObserveSequence._loggedWarnings, 0);                                                    // 61
  ObserveSequence._loggedWarnings = 0;                                                               // 62
                                                                                                     // 63
  // any expected argument this is `{NOT: "foo"}`, should match any                                  // 64
  // corresponding value in the fired callbacks other than "foo". so,                                // 65
  // assert non-equality and then replace the appropriate entries in                                 // 66
  // the 'firedCallbacks' array with `{NOT: "foo"}` before calling                                   // 67
  // `test.equal` below.                                                                             // 68
  var commonLength = Math.min(firedCallbacks.length, expectedCallbacks.length);                      // 69
  for (var i = 0; i < commonLength; i++) {                                                           // 70
    var callback = expectedCallbacks[i];                                                             // 71
    if (_.keys(callback).length !== 1)                                                               // 72
      throw new Error("Callbacks should be objects with one key, eg `addedAt`");                     // 73
    var callbackName = _.keys(callback)[0];                                                          // 74
    var args = _.values(callback)[0];                                                                // 75
    _.each(args, function (arg, argIndex) {                                                          // 76
      if (arg && typeof arg === 'object' &&                                                          // 77
          'NOT' in arg &&                                                                            // 78
          firedCallbacks[i][callbackName]) {                                                         // 79
        test.notEqual(firedCallbacks[i][callbackName][argIndex],                                     // 80
                      arg.NOT, "Should be NOT " + arg.NOT);                                          // 81
        firedCallbacks[i][callbackName][argIndex] = arg;                                             // 82
      }                                                                                              // 83
    });                                                                                              // 84
  }                                                                                                  // 85
                                                                                                     // 86
  test.equal(EJSON.stringify(firedCallbacks, {canonical: true}),                                     // 87
             EJSON.stringify(expectedCallbacks, {canonical: true}));                                 // 88
};                                                                                                   // 89
                                                                                                     // 90
Tinytest.add('observe sequence - initial data for all sequence types', function (test) {             // 91
  runOneObserveSequenceTestCase(test, function () {                                                  // 92
    return null;                                                                                     // 93
  }, function () {}, []);                                                                            // 94
                                                                                                     // 95
  runOneObserveSequenceTestCase(test, function () {                                                  // 96
    return [];                                                                                       // 97
  }, function () {}, []);                                                                            // 98
                                                                                                     // 99
  runOneObserveSequenceTestCase(test, function () {                                                  // 100
    return [{foo: 1}, {bar: 2}];                                                                     // 101
  }, function () {}, [                                                                               // 102
    {addedAt: [0, {foo: 1}, 0, null]},                                                               // 103
    {addedAt: [1, {bar: 2}, 1, null]}                                                                // 104
  ]);                                                                                                // 105
                                                                                                     // 106
  runOneObserveSequenceTestCase(test, function () {                                                  // 107
    return [{_id: "13", foo: 1}, {_id: "37", bar: 2}];                                               // 108
  }, function () {}, [                                                                               // 109
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 110
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]}                                                  // 111
  ]);                                                                                                // 112
                                                                                                     // 113
  runOneObserveSequenceTestCase(test, function () {                                                  // 114
    var coll = new Meteor.Collection(null);                                                          // 115
    coll.insert({_id: "13", foo: 1});                                                                // 116
    coll.insert({_id: "37", bar: 2});                                                                // 117
    var cursor = coll.find({}, {sort: {_id: 1}});                                                    // 118
    return cursor;                                                                                   // 119
  }, function () {}, [                                                                               // 120
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 121
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]}                                                  // 122
  ]);                                                                                                // 123
                                                                                                     // 124
  // shouldn't break on array with duplicate _id's, and the ids sent                                 // 125
  // in the callbacks should be distinct                                                             // 126
  runOneObserveSequenceTestCase(test, function () {                                                  // 127
    return [                                                                                         // 128
      {_id: "13", foo: 1},                                                                           // 129
      {_id: "13", foo: 2}                                                                            // 130
    ];                                                                                               // 131
  }, function () {}, [                                                                               // 132
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 133
    {addedAt: [{NOT: "13"}, {_id: "13", foo: 2}, 1, null]}                                           // 134
  ], /*numExpectedWarnings = */1);                                                                   // 135
});                                                                                                  // 136
                                                                                                     // 137
Tinytest.add('observe sequence - array to other array', function (test) {                            // 138
  var dep = new Deps.Dependency;                                                                     // 139
  var seq = [{_id: "13", foo: 1}, {_id: "37", bar: 2}];                                              // 140
                                                                                                     // 141
  runOneObserveSequenceTestCase(test, function () {                                                  // 142
    dep.depend();                                                                                    // 143
    return seq;                                                                                      // 144
  }, function () {                                                                                   // 145
    seq = [{_id: "13", foo: 1}, {_id: "38", bar: 2}];                                                // 146
    dep.changed();                                                                                   // 147
  }, [                                                                                               // 148
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 149
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 150
    {removed: ["37", {_id: "37", bar: 2}]},                                                          // 151
    {addedAt: ["38", {_id: "38", bar: 2}, 1, null]},                                                 // 152
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]}                                      // 153
  ]);                                                                                                // 154
});                                                                                                  // 155
                                                                                                     // 156
Tinytest.add('observe sequence - array to other array, strings', function (test) {                   // 157
  var dep = new Deps.Dependency;                                                                     // 158
  var seq = ["A", "B"];                                                                              // 159
                                                                                                     // 160
  runOneObserveSequenceTestCase(test, function () {                                                  // 161
    dep.depend();                                                                                    // 162
    return seq;                                                                                      // 163
  }, function () {                                                                                   // 164
    seq = ["B", "C"];                                                                                // 165
    dep.changed();                                                                                   // 166
  }, [                                                                                               // 167
    {addedAt: ["-A", "A", 0, null]},                                                                 // 168
    {addedAt: ["-B", "B", 1, null]},                                                                 // 169
    {removed: ["-A", "A"]},                                                                          // 170
    {addedAt: ["-C", "C", 1, null]}                                                                  // 171
  ]);                                                                                                // 172
});                                                                                                  // 173
                                                                                                     // 174
Tinytest.add('observe sequence - array to other array, objects without ids', function (test) {       // 175
  var dep = new Deps.Dependency;                                                                     // 176
  var seq = [{foo: 1}, {bar: 2}];                                                                    // 177
                                                                                                     // 178
  runOneObserveSequenceTestCase(test, function () {                                                  // 179
    dep.depend();                                                                                    // 180
    return seq;                                                                                      // 181
  }, function () {                                                                                   // 182
    seq = [{foo: 2}];                                                                                // 183
    dep.changed();                                                                                   // 184
  }, [                                                                                               // 185
    {addedAt: [0, {foo: 1}, 0, null]},                                                               // 186
    {addedAt: [1, {bar: 2}, 1, null]},                                                               // 187
    {removed: [1, {bar: 2}]},                                                                        // 188
    {changed: [0, {foo: 2}, {foo: 1}]}                                                               // 189
  ]);                                                                                                // 190
});                                                                                                  // 191
                                                                                                     // 192
Tinytest.add('observe sequence - array to other array, changes', function (test) {                   // 193
  var dep = new Deps.Dependency;                                                                     // 194
  var seq = [{_id: "13", foo: 1}, {_id: "37", bar: 2}, {_id: "42", baz: 42}];                        // 195
                                                                                                     // 196
  runOneObserveSequenceTestCase(test, function () {                                                  // 197
    dep.depend();                                                                                    // 198
    return seq;                                                                                      // 199
  }, function () {                                                                                   // 200
    seq = [{_id: "13", foo: 1}, {_id: "38", bar: 2}, {_id: "42", baz: 43}];                          // 201
    dep.changed();                                                                                   // 202
  }, [                                                                                               // 203
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 204
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 205
    {addedAt: ["42", {_id: "42", baz: 42}, 2, null]},                                                // 206
    {removed: ["37", {_id: "37", bar: 2}]},                                                          // 207
    {addedAt: ["38", {_id: "38", bar: 2}, 1, "42"]},                                                 // 208
    // change fires for all elements, because we don't diff the actual                               // 209
    // objects.                                                                                      // 210
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]},                                     // 211
    {changed: ["42", {_id: "42", baz: 43}, {_id: "42", baz: 42}]}                                    // 212
  ]);                                                                                                // 213
});                                                                                                  // 214
                                                                                                     // 215
Tinytest.add('observe sequence - array to other array, movedTo', function (test) {                   // 216
  var dep = new Deps.Dependency;                                                                     // 217
  var seq = [{_id: "13", foo: 1}, {_id: "37", bar: 2}, {_id: "42", baz: 42}];                        // 218
                                                                                                     // 219
  runOneObserveSequenceTestCase(test, function () {                                                  // 220
    dep.depend();                                                                                    // 221
    return seq;                                                                                      // 222
  }, function () {                                                                                   // 223
    seq = [{_id: "37", bar: 2}, {_id: "13", foo: 1}, {_id: "42", baz: 42}];                          // 224
    dep.changed();                                                                                   // 225
  }, [                                                                                               // 226
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 227
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 228
    {addedAt: ["42", {_id: "42", baz: 42}, 2, null]},                                                // 229
    // XXX it could have been the "13" moving but it's a detail of implementation                    // 230
    {movedTo: ["37", {_id: "37", bar: 2}, 1, 0, "13"]},                                              // 231
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]},                                     // 232
    {changed: ["37", {_id: "37", bar: 2}, {_id: "37", bar: 2}]},                                     // 233
    {changed: ["42", {_id: "42", baz: 42}, {_id: "42", baz: 42}]}                                    // 234
  ]);                                                                                                // 235
});                                                                                                  // 236
                                                                                                     // 237
Tinytest.add('observe sequence - array to null', function (test) {                                   // 238
  var dep = new Deps.Dependency;                                                                     // 239
  var seq = [{_id: "13", foo: 1}, {_id: "37", bar: 2}];                                              // 240
                                                                                                     // 241
  runOneObserveSequenceTestCase(test, function () {                                                  // 242
    dep.depend();                                                                                    // 243
    return seq;                                                                                      // 244
  }, function () {                                                                                   // 245
    seq = null;                                                                                      // 246
    dep.changed();                                                                                   // 247
  }, [                                                                                               // 248
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 249
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 250
    {removed: ["13", {_id: "13", foo: 1}]},                                                          // 251
    {removed: ["37", {_id: "37", bar: 2}]}                                                           // 252
  ]);                                                                                                // 253
});                                                                                                  // 254
                                                                                                     // 255
Tinytest.add('observe sequence - array to cursor', function (test) {                                 // 256
  var dep = new Deps.Dependency;                                                                     // 257
  var seq = [{_id: "13", foo: 1}, {_id: "37", bar: 2}];                                              // 258
                                                                                                     // 259
  runOneObserveSequenceTestCase(test, function () {                                                  // 260
    dep.depend();                                                                                    // 261
    return seq;                                                                                      // 262
  }, function () {                                                                                   // 263
    var coll = new Meteor.Collection(null);                                                          // 264
    coll.insert({_id: "13", foo: 1});                                                                // 265
    coll.insert({_id: "38", bar: 2});                                                                // 266
    var cursor = coll.find({}, {sort: {_id: 1}});                                                    // 267
    seq = cursor;                                                                                    // 268
    dep.changed();                                                                                   // 269
  }, [                                                                                               // 270
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 271
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 272
    {removed: ["37", {_id: "37", bar: 2}]},                                                          // 273
    {addedAt: ["38", {_id: "38", bar: 2}, 1, null]},                                                 // 274
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]}                                      // 275
  ]);                                                                                                // 276
});                                                                                                  // 277
                                                                                                     // 278
                                                                                                     // 279
Tinytest.add('observe sequence - cursor to null', function (test) {                                  // 280
  var dep = new Deps.Dependency;                                                                     // 281
  var coll = new Meteor.Collection(null);                                                            // 282
  coll.insert({_id: "13", foo: 1});                                                                  // 283
  coll.insert({_id: "37", bar: 2});                                                                  // 284
  var cursor = coll.find({}, {sort: {_id: 1}});                                                      // 285
  var seq = cursor;                                                                                  // 286
                                                                                                     // 287
  runOneObserveSequenceTestCase(test, function () {                                                  // 288
    dep.depend();                                                                                    // 289
    return seq;                                                                                      // 290
  }, function () {                                                                                   // 291
    seq = null;                                                                                      // 292
    dep.changed();                                                                                   // 293
  }, [                                                                                               // 294
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 295
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 296
    {removed: ["13", {_id: "13", foo: 1}]},                                                          // 297
    {removed: ["37", {_id: "37", bar: 2}]}                                                           // 298
  ]);                                                                                                // 299
});                                                                                                  // 300
                                                                                                     // 301
Tinytest.add('observe sequence - cursor to array', function (test) {                                 // 302
  var dep = new Deps.Dependency;                                                                     // 303
  var coll = new Meteor.Collection(null);                                                            // 304
  coll.insert({_id: "13", foo: 1});                                                                  // 305
  var cursor = coll.find({}, {sort: {_id: 1}});                                                      // 306
  var seq = cursor;                                                                                  // 307
                                                                                                     // 308
  runOneObserveSequenceTestCase(test, function () {                                                  // 309
    dep.depend();                                                                                    // 310
    return seq;                                                                                      // 311
  }, function () {                                                                                   // 312
    coll.insert({_id: "37", bar: 2});                                                                // 313
    seq = [{_id: "13", foo: 1}, {_id: "38", bar: 2}];                                                // 314
    dep.changed();                                                                                   // 315
  }, [                                                                                               // 316
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 317
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 318
    {removed: ["37", {_id: "37", bar: 2}]},                                                          // 319
    {addedAt: ["38", {_id: "38", bar: 2}, 1, null]},                                                 // 320
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]}                                      // 321
  ]);                                                                                                // 322
});                                                                                                  // 323
                                                                                                     // 324
Tinytest.add('observe sequence - cursor', function (test) {                                          // 325
  var coll = new Meteor.Collection(null);                                                            // 326
  coll.insert({_id: "13", rank: 1});                                                                 // 327
  var cursor = coll.find({}, {sort: {rank: 1}});                                                     // 328
  var seq = cursor;                                                                                  // 329
                                                                                                     // 330
  runOneObserveSequenceTestCase(test, function () {                                                  // 331
    return seq;                                                                                      // 332
  }, function () {                                                                                   // 333
    coll.insert({_id: "37", rank: 2});                                                               // 334
    coll.insert({_id: "77", rank: 3});                                                               // 335
    coll.remove({_id: "37"});                           // should fire a 'remove' callback           // 336
    coll.insert({_id: "11", rank: 0});                  // should fire an 'insert' callback          // 337
    coll.update({_id: "13"}, {$set: {updated: true}});  // should fire an 'changed' callback         // 338
    coll.update({_id: "77"}, {$set: {rank: -1}});       // should fire 'changed' and 'move' callback // 339
  }, [                                                                                               // 340
    // this case must not fire spurious calls as the array to array                                  // 341
    // case does. otherwise, the entire power of cursors is lost in                                  // 342
    // meteor ui.                                                                                    // 343
    {addedAt: ["13", {_id: "13", rank: 1}, 0, null]},                                                // 344
    {addedAt: ["37", {_id: "37", rank: 2}, 1, null]},                                                // 345
    {addedAt: ["77", {_id: "77", rank: 3}, 2, null]},                                                // 346
    {removed: ["37", {_id: "37", rank: 2}]},                                                         // 347
    {addedAt: ["11", {_id: "11", rank: 0}, 0, "13"]},                                                // 348
    {changed: ["13", {_id: "13", rank: 1, updated: true}, {_id: "13", rank: 1}]},                    // 349
    {changed: ["77", {_id: "77", rank: -1}, {_id: "77", rank: 3}]},                                  // 350
    {movedTo: ["77", {_id: "77", rank: -1}, 2, 0, "11"]}                                             // 351
  ]);                                                                                                // 352
});                                                                                                  // 353
                                                                                                     // 354
Tinytest.add('observe sequence - cursor to other cursor', function (test) {                          // 355
  var dep = new Deps.Dependency;                                                                     // 356
  var coll = new Meteor.Collection(null);                                                            // 357
  coll.insert({_id: "13", foo: 1});                                                                  // 358
  var cursor = coll.find({}, {sort: {_id: 1}});                                                      // 359
  var seq = cursor;                                                                                  // 360
                                                                                                     // 361
  runOneObserveSequenceTestCase(test, function () {                                                  // 362
    dep.depend();                                                                                    // 363
    return seq;                                                                                      // 364
  }, function () {                                                                                   // 365
    coll.insert({_id: "37", bar: 2});                                                                // 366
                                                                                                     // 367
    var newColl = new Meteor.Collection(null);                                                       // 368
    newColl.insert({_id: "13", foo: 1});                                                             // 369
    newColl.insert({_id: "38", bar: 2});                                                             // 370
    var newCursor = newColl.find({}, {sort: {_id: 1}});                                              // 371
    seq = newCursor;                                                                                 // 372
    dep.changed();                                                                                   // 373
  }, [                                                                                               // 374
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 375
    {addedAt: ["37", {_id: "37", bar: 2}, 1, null]},                                                 // 376
    {removed: ["37", {_id: "37", bar: 2}]},                                                          // 377
    {addedAt: ["38", {_id: "38", bar: 2}, 1, null]},                                                 // 378
    {changed: ["13", {_id: "13", foo: 1}, {_id: "13", foo: 1}]}                                      // 379
  ]);                                                                                                // 380
});                                                                                                  // 381
                                                                                                     // 382
Tinytest.add('observe sequence - cursor to other cursor with transform', function (test) {           // 383
  var dep = new Deps.Dependency;                                                                     // 384
  var transform = function(doc) {                                                                    // 385
    return _.extend({idCopy: doc._id}, doc);                                                         // 386
  };                                                                                                 // 387
                                                                                                     // 388
  var coll = new Meteor.Collection(null, {transform: transform});                                    // 389
  coll.insert({_id: "13", foo: 1});                                                                  // 390
  var cursor = coll.find({}, {sort: {_id: 1}});                                                      // 391
  var seq = cursor;                                                                                  // 392
                                                                                                     // 393
  runOneObserveSequenceTestCase(test, function () {                                                  // 394
    dep.depend();                                                                                    // 395
    return seq;                                                                                      // 396
  }, function () {                                                                                   // 397
    coll.insert({_id: "37", bar: 2});                                                                // 398
                                                                                                     // 399
    var newColl = new Meteor.Collection(null, {transform: transform});                               // 400
    newColl.insert({_id: "13", foo: 1});                                                             // 401
    newColl.insert({_id: "38", bar: 2});                                                             // 402
    var newCursor = newColl.find({}, {sort: {_id: 1}});                                              // 403
    seq = newCursor;                                                                                 // 404
    dep.changed();                                                                                   // 405
  }, [                                                                                               // 406
    {addedAt: ["13", {_id: "13", foo: 1, idCopy: "13"}, 0, null]},                                   // 407
    {addedAt: ["37", {_id: "37", bar: 2, idCopy: "37"}, 1, null]},                                   // 408
    {removed: ["37", {_id: "37", bar: 2, idCopy: "37"}]},                                            // 409
    {addedAt: ["38", {_id: "38", bar: 2, idCopy: "38"}, 1, null]},                                   // 410
    {changed: ["13", {_id: "13", foo: 1, idCopy: "13"}, {_id: "13", foo: 1, idCopy: "13"}]}          // 411
  ]);                                                                                                // 412
});                                                                                                  // 413
                                                                                                     // 414
Tinytest.add('observe sequence - cursor to same cursor', function (test) {                           // 415
  var coll = new Meteor.Collection(null);                                                            // 416
  coll.insert({_id: "13", rank: 1});                                                                 // 417
  var cursor = coll.find({}, {sort: {rank: 1}});                                                     // 418
  var seq = cursor;                                                                                  // 419
  var dep = new Deps.Dependency;                                                                     // 420
                                                                                                     // 421
  runOneObserveSequenceTestCase(test, function () {                                                  // 422
    dep.depend();                                                                                    // 423
    return seq;                                                                                      // 424
  }, function () {                                                                                   // 425
    coll.insert({_id: "24", rank: 2});                                                               // 426
    dep.changed();                                                                                   // 427
    Deps.flush();                                                                                    // 428
    coll.insert({_id: "78", rank: 3});                                                               // 429
  }, [                                                                                               // 430
    {addedAt: ["13", {_id: "13", rank: 1}, 0, null]},                                                // 431
    {addedAt: ["24", {_id: "24", rank: 2}, 1, null]},                                                // 432
    // even if the cursor changes to the same cursor, we diff to see if we                           // 433
    // missed anything during the invalidation, which leads to these                                 // 434
    // "changed" events.                                                                             // 435
    {changed: ["13", {_id: "13", rank: 1}, {_id: "13", rank: 1}]},                                   // 436
    {changed: ["24", {_id: "24", rank: 2}, {_id: "24", rank: 2}]},                                   // 437
    {addedAt: ["78", {_id: "78", rank: 3}, 2, null]}                                                 // 438
  ]);                                                                                                // 439
});                                                                                                  // 440
                                                                                                     // 441
Tinytest.add('observe sequence - string arrays', function (test) {                                   // 442
  var seq = ['A', 'B'];                                                                              // 443
  var dep = new Deps.Dependency;                                                                     // 444
                                                                                                     // 445
  runOneObserveSequenceTestCase(test, function () {                                                  // 446
    dep.depend();                                                                                    // 447
    return seq;                                                                                      // 448
  }, function () {                                                                                   // 449
    seq = ['B', 'C'];                                                                                // 450
    dep.changed();                                                                                   // 451
  }, [                                                                                               // 452
    {addedAt: ['-A', 'A', 0, null]},                                                                 // 453
    {addedAt: ['-B', 'B', 1, null]},                                                                 // 454
    {removed: ['-A', 'A']},                                                                          // 455
    {addedAt: ['-C', 'C', 1, null]}                                                                  // 456
  ]);                                                                                                // 457
});                                                                                                  // 458
                                                                                                     // 459
Tinytest.add('observe sequence - number arrays', function (test) {                                   // 460
  var seq = [1, 1, 2];                                                                               // 461
  var dep = new Deps.Dependency;                                                                     // 462
                                                                                                     // 463
  runOneObserveSequenceTestCase(test, function () {                                                  // 464
    dep.depend();                                                                                    // 465
    return seq;                                                                                      // 466
  }, function () {                                                                                   // 467
    seq = [1, 3, 2, 3];                                                                              // 468
    dep.changed();                                                                                   // 469
  }, [                                                                                               // 470
    {addedAt: [1, 1, 0, null]},                                                                      // 471
    {addedAt: [{NOT: 1}, 1, 1, null]},                                                               // 472
    {addedAt: [2, 2, 2, null]},                                                                      // 473
    {removed: [{NOT: 1}, 1]},                                                                        // 474
    {addedAt: [3, 3, 1, 2]},                                                                         // 475
    {addedAt: [{NOT: 3}, 3, 3, null]}                                                                // 476
  ], /*numExpectedWarnings = */2);                                                                   // 477
});                                                                                                  // 478
                                                                                                     // 479
Tinytest.add('observe sequence - cursor to other cursor, same collection', function (test) {         // 480
  var dep = new Deps.Dependency;                                                                     // 481
  var coll = new Meteor.Collection(null);                                                            // 482
  coll.insert({_id: "13", foo: 1});                                                                  // 483
  coll.insert({_id: "37", foo: 2});                                                                  // 484
  var cursor = coll.find({foo: 1});                                                                  // 485
  var seq = cursor;                                                                                  // 486
                                                                                                     // 487
  runOneObserveSequenceTestCase(test, function () {                                                  // 488
    dep.depend();                                                                                    // 489
    return seq;                                                                                      // 490
  }, function () {                                                                                   // 491
    var newCursor = coll.find({foo: 2});                                                             // 492
    seq = newCursor;                                                                                 // 493
    dep.changed();                                                                                   // 494
    Deps.flush();                                                                                    // 495
    coll.insert({_id: "38", foo: 1});                                                                // 496
    coll.insert({_id: "39", foo: 2});                                                                // 497
  }, [                                                                                               // 498
    {addedAt: ["13", {_id: "13", foo: 1}, 0, null]},                                                 // 499
    {removed: ["13", {_id: "13", foo: 1}]},                                                          // 500
    {addedAt: ["37", {_id: "37", foo: 2}, 0, null]},                                                 // 501
    {addedAt: ["39", {_id: "39", foo: 2}, 1, null]}                                                  // 502
  ]);                                                                                                // 503
});                                                                                                  // 504
                                                                                                     // 505
///////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
