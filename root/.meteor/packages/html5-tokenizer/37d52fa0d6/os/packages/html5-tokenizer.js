(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/entities.js                                                                            //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
HTML5 = (typeof HTML5 === 'undefined' ? {} : HTML5);                                                               // 1
                                                                                                                   // 2
HTML5.ENTITIES = {                                                                                                 // 3
  "AElig": "\u00C6",                                                                                               // 4
  "AElig;": "\u00C6",                                                                                              // 5
  "AMP": "&",                                                                                                      // 6
  "AMP;": "&",                                                                                                     // 7
  "Aacute": "\u00C1",                                                                                              // 8
  "Aacute;": "\u00C1",                                                                                             // 9
  "Abreve;": "\u0102",                                                                                             // 10
  "Acirc": "\u00C2",                                                                                               // 11
  "Acirc;": "\u00C2",                                                                                              // 12
  "Acy;": "\u0410",                                                                                                // 13
  "Afr;": "\u1D504",                                                                                               // 14
  "Agrave": "\u00C0",                                                                                              // 15
  "Agrave;": "\u00C0",                                                                                             // 16
  "Alpha;": "\u0391",                                                                                              // 17
  "Amacr;": "\u0100",                                                                                              // 18
  "And;": "\u2A53",                                                                                                // 19
  "Aogon;": "\u0104",                                                                                              // 20
  "Aopf;": "\u1D538",                                                                                              // 21
  "ApplyFunction;": "\u2061",                                                                                      // 22
  "Aring": "\u00C5",                                                                                               // 23
  "Aring;": "\u00C5",                                                                                              // 24
  "Ascr;": "\u1D49C",                                                                                              // 25
  "Assign;": "\u2254",                                                                                             // 26
  "Atilde": "\u00C3",                                                                                              // 27
  "Atilde;": "\u00C3",                                                                                             // 28
  "Auml": "\u00C4",                                                                                                // 29
  "Auml;": "\u00C4",                                                                                               // 30
  "Backslash;": "\u2216",                                                                                          // 31
  "Barv;": "\u2AE7",                                                                                               // 32
  "Barwed;": "\u2306",                                                                                             // 33
  "Bcy;": "\u0411",                                                                                                // 34
  "Because;": "\u2235",                                                                                            // 35
  "Bernoullis;": "\u212C",                                                                                         // 36
  "Beta;": "\u0392",                                                                                               // 37
  "Bfr;": "\u1D505",                                                                                               // 38
  "Bopf;": "\u1D539",                                                                                              // 39
  "Breve;": "\u02D8",                                                                                              // 40
  "Bscr;": "\u212C",                                                                                               // 41
  "Bumpeq;": "\u224E",                                                                                             // 42
  "CHcy;": "\u0427",                                                                                               // 43
  "COPY": "\u00A9",                                                                                                // 44
  "COPY;": "\u00A9",                                                                                               // 45
  "Cacute;": "\u0106",                                                                                             // 46
  "Cap;": "\u22D2",                                                                                                // 47
  "CapitalDifferentialD;": "\u2145",                                                                               // 48
  "Cayleys;": "\u212D",                                                                                            // 49
  "Ccaron;": "\u010C",                                                                                             // 50
  "Ccedil": "\u00C7",                                                                                              // 51
  "Ccedil;": "\u00C7",                                                                                             // 52
  "Ccirc;": "\u0108",                                                                                              // 53
  "Cconint;": "\u2230",                                                                                            // 54
  "Cdot;": "\u010A",                                                                                               // 55
  "Cedilla;": "\u00B8",                                                                                            // 56
  "CenterDot;": "\u00B7",                                                                                          // 57
  "Cfr;": "\u212D",                                                                                                // 58
  "Chi;": "\u03A7",                                                                                                // 59
  "CircleDot;": "\u2299",                                                                                          // 60
  "CircleMinus;": "\u2296",                                                                                        // 61
  "CirclePlus;": "\u2295",                                                                                         // 62
  "CircleTimes;": "\u2297",                                                                                        // 63
  "ClockwiseContourIntegral;": "\u2232",                                                                           // 64
  "CloseCurlyDoubleQuote;": "\u201D",                                                                              // 65
  "CloseCurlyQuote;": "\u2019",                                                                                    // 66
  "Colon;": "\u2237",                                                                                              // 67
  "Colone;": "\u2A74",                                                                                             // 68
  "Congruent;": "\u2261",                                                                                          // 69
  "Conint;": "\u222F",                                                                                             // 70
  "ContourIntegral;": "\u222E",                                                                                    // 71
  "Copf;": "\u2102",                                                                                               // 72
  "Coproduct;": "\u2210",                                                                                          // 73
  "CounterClockwiseContourIntegral;": "\u2233",                                                                    // 74
  "Cross;": "\u2A2F",                                                                                              // 75
  "Cscr;": "\u1D49E",                                                                                              // 76
  "Cup;": "\u22D3",                                                                                                // 77
  "CupCap;": "\u224D",                                                                                             // 78
  "DD;": "\u2145",                                                                                                 // 79
  "DDotrahd;": "\u2911",                                                                                           // 80
  "DJcy;": "\u0402",                                                                                               // 81
  "DScy;": "\u0405",                                                                                               // 82
  "DZcy;": "\u040F",                                                                                               // 83
  "Dagger;": "\u2021",                                                                                             // 84
  "Darr;": "\u21A1",                                                                                               // 85
  "Dashv;": "\u2AE4",                                                                                              // 86
  "Dcaron;": "\u010E",                                                                                             // 87
  "Dcy;": "\u0414",                                                                                                // 88
  "Del;": "\u2207",                                                                                                // 89
  "Delta;": "\u0394",                                                                                              // 90
  "Dfr;": "\u1D507",                                                                                               // 91
  "DiacriticalAcute;": "\u00B4",                                                                                   // 92
  "DiacriticalDot;": "\u02D9",                                                                                     // 93
  "DiacriticalDoubleAcute;": "\u02DD",                                                                             // 94
  "DiacriticalGrave;": "`",                                                                                        // 95
  "DiacriticalTilde;": "\u02DC",                                                                                   // 96
  "Diamond;": "\u22C4",                                                                                            // 97
  "DifferentialD;": "\u2146",                                                                                      // 98
  "Dopf;": "\u1D53B",                                                                                              // 99
  "Dot;": "\u00A8",                                                                                                // 100
  "DotDot;": "\u20DC",                                                                                             // 101
  "DotEqual;": "\u2250",                                                                                           // 102
  "DoubleContourIntegral;": "\u222F",                                                                              // 103
  "DoubleDot;": "\u00A8",                                                                                          // 104
  "DoubleDownArrow;": "\u21D3",                                                                                    // 105
  "DoubleLeftArrow;": "\u21D0",                                                                                    // 106
  "DoubleLeftRightArrow;": "\u21D4",                                                                               // 107
  "DoubleLeftTee;": "\u2AE4",                                                                                      // 108
  "DoubleLongLeftArrow;": "\u27F8",                                                                                // 109
  "DoubleLongLeftRightArrow;": "\u27FA",                                                                           // 110
  "DoubleLongRightArrow;": "\u27F9",                                                                               // 111
  "DoubleRightArrow;": "\u21D2",                                                                                   // 112
  "DoubleRightTee;": "\u22A8",                                                                                     // 113
  "DoubleUpArrow;": "\u21D1",                                                                                      // 114
  "DoubleUpDownArrow;": "\u21D5",                                                                                  // 115
  "DoubleVerticalBar;": "\u2225",                                                                                  // 116
  "DownArrow;": "\u2193",                                                                                          // 117
  "DownArrowBar;": "\u2913",                                                                                       // 118
  "DownArrowUpArrow;": "\u21F5",                                                                                   // 119
  "DownBreve;": "\u0311",                                                                                          // 120
  "DownLeftRightVector;": "\u2950",                                                                                // 121
  "DownLeftTeeVector;": "\u295E",                                                                                  // 122
  "DownLeftVector;": "\u21BD",                                                                                     // 123
  "DownLeftVectorBar;": "\u2956",                                                                                  // 124
  "DownRightTeeVector;": "\u295F",                                                                                 // 125
  "DownRightVector;": "\u21C1",                                                                                    // 126
  "DownRightVectorBar;": "\u2957",                                                                                 // 127
  "DownTee;": "\u22A4",                                                                                            // 128
  "DownTeeArrow;": "\u21A7",                                                                                       // 129
  "Downarrow;": "\u21D3",                                                                                          // 130
  "Dscr;": "\u1D49F",                                                                                              // 131
  "Dstrok;": "\u0110",                                                                                             // 132
  "ENG;": "\u014A",                                                                                                // 133
  "ETH": "\u00D0",                                                                                                 // 134
  "ETH;": "\u00D0",                                                                                                // 135
  "Eacute": "\u00C9",                                                                                              // 136
  "Eacute;": "\u00C9",                                                                                             // 137
  "Ecaron;": "\u011A",                                                                                             // 138
  "Ecirc": "\u00CA",                                                                                               // 139
  "Ecirc;": "\u00CA",                                                                                              // 140
  "Ecy;": "\u042D",                                                                                                // 141
  "Edot;": "\u0116",                                                                                               // 142
  "Efr;": "\u1D508",                                                                                               // 143
  "Egrave": "\u00C8",                                                                                              // 144
  "Egrave;": "\u00C8",                                                                                             // 145
  "Element;": "\u2208",                                                                                            // 146
  "Emacr;": "\u0112",                                                                                              // 147
  "EmptySmallSquare;": "\u25FB",                                                                                   // 148
  "EmptyVerySmallSquare;": "\u25AB",                                                                               // 149
  "Eogon;": "\u0118",                                                                                              // 150
  "Eopf;": "\u1D53C",                                                                                              // 151
  "Epsilon;": "\u0395",                                                                                            // 152
  "Equal;": "\u2A75",                                                                                              // 153
  "EqualTilde;": "\u2242",                                                                                         // 154
  "Equilibrium;": "\u21CC",                                                                                        // 155
  "Escr;": "\u2130",                                                                                               // 156
  "Esim;": "\u2A73",                                                                                               // 157
  "Eta;": "\u0397",                                                                                                // 158
  "Euml": "\u00CB",                                                                                                // 159
  "Euml;": "\u00CB",                                                                                               // 160
  "Exists;": "\u2203",                                                                                             // 161
  "ExponentialE;": "\u2147",                                                                                       // 162
  "Fcy;": "\u0424",                                                                                                // 163
  "Ffr;": "\u1D509",                                                                                               // 164
  "FilledSmallSquare;": "\u25FC",                                                                                  // 165
  "FilledVerySmallSquare;": "\u25AA",                                                                              // 166
  "Fopf;": "\u1D53D",                                                                                              // 167
  "ForAll;": "\u2200",                                                                                             // 168
  "Fouriertrf;": "\u2131",                                                                                         // 169
  "Fscr;": "\u2131",                                                                                               // 170
  "GJcy;": "\u0403",                                                                                               // 171
  "GT": ">",                                                                                                       // 172
  "GT;": ">",                                                                                                      // 173
  "Gamma;": "\u0393",                                                                                              // 174
  "Gammad;": "\u03DC",                                                                                             // 175
  "Gbreve;": "\u011E",                                                                                             // 176
  "Gcedil;": "\u0122",                                                                                             // 177
  "Gcirc;": "\u011C",                                                                                              // 178
  "Gcy;": "\u0413",                                                                                                // 179
  "Gdot;": "\u0120",                                                                                               // 180
  "Gfr;": "\u1D50A",                                                                                               // 181
  "Gg;": "\u22D9",                                                                                                 // 182
  "Gopf;": "\u1D53E",                                                                                              // 183
  "GreaterEqual;": "\u2265",                                                                                       // 184
  "GreaterEqualLess;": "\u22DB",                                                                                   // 185
  "GreaterFullEqual;": "\u2267",                                                                                   // 186
  "GreaterGreater;": "\u2AA2",                                                                                     // 187
  "GreaterLess;": "\u2277",                                                                                        // 188
  "GreaterSlantEqual;": "\u2A7E",                                                                                  // 189
  "GreaterTilde;": "\u2273",                                                                                       // 190
  "Gscr;": "\u1D4A2",                                                                                              // 191
  "Gt;": "\u226B",                                                                                                 // 192
  "HARDcy;": "\u042A",                                                                                             // 193
  "Hacek;": "\u02C7",                                                                                              // 194
  "Hat;": "^",                                                                                                     // 195
  "Hcirc;": "\u0124",                                                                                              // 196
  "Hfr;": "\u210C",                                                                                                // 197
  "HilbertSpace;": "\u210B",                                                                                       // 198
  "Hopf;": "\u210D",                                                                                               // 199
  "HorizontalLine;": "\u2500",                                                                                     // 200
  "Hscr;": "\u210B",                                                                                               // 201
  "Hstrok;": "\u0126",                                                                                             // 202
  "HumpDownHump;": "\u224E",                                                                                       // 203
  "HumpEqual;": "\u224F",                                                                                          // 204
  "IEcy;": "\u0415",                                                                                               // 205
  "IJlig;": "\u0132",                                                                                              // 206
  "IOcy;": "\u0401",                                                                                               // 207
  "Iacute": "\u00CD",                                                                                              // 208
  "Iacute;": "\u00CD",                                                                                             // 209
  "Icirc": "\u00CE",                                                                                               // 210
  "Icirc;": "\u00CE",                                                                                              // 211
  "Icy;": "\u0418",                                                                                                // 212
  "Idot;": "\u0130",                                                                                               // 213
  "Ifr;": "\u2111",                                                                                                // 214
  "Igrave": "\u00CC",                                                                                              // 215
  "Igrave;": "\u00CC",                                                                                             // 216
  "Im;": "\u2111",                                                                                                 // 217
  "Imacr;": "\u012A",                                                                                              // 218
  "ImaginaryI;": "\u2148",                                                                                         // 219
  "Implies;": "\u21D2",                                                                                            // 220
  "Int;": "\u222C",                                                                                                // 221
  "Integral;": "\u222B",                                                                                           // 222
  "Intersection;": "\u22C2",                                                                                       // 223
  "InvisibleComma;": "\u2063",                                                                                     // 224
  "InvisibleTimes;": "\u2062",                                                                                     // 225
  "Iogon;": "\u012E",                                                                                              // 226
  "Iopf;": "\u1D540",                                                                                              // 227
  "Iota;": "\u0399",                                                                                               // 228
  "Iscr;": "\u2110",                                                                                               // 229
  "Itilde;": "\u0128",                                                                                             // 230
  "Iukcy;": "\u0406",                                                                                              // 231
  "Iuml": "\u00CF",                                                                                                // 232
  "Iuml;": "\u00CF",                                                                                               // 233
  "Jcirc;": "\u0134",                                                                                              // 234
  "Jcy;": "\u0419",                                                                                                // 235
  "Jfr;": "\u1D50D",                                                                                               // 236
  "Jopf;": "\u1D541",                                                                                              // 237
  "Jscr;": "\u1D4A5",                                                                                              // 238
  "Jsercy;": "\u0408",                                                                                             // 239
  "Jukcy;": "\u0404",                                                                                              // 240
  "KHcy;": "\u0425",                                                                                               // 241
  "KJcy;": "\u040C",                                                                                               // 242
  "Kappa;": "\u039A",                                                                                              // 243
  "Kcedil;": "\u0136",                                                                                             // 244
  "Kcy;": "\u041A",                                                                                                // 245
  "Kfr;": "\u1D50E",                                                                                               // 246
  "Kopf;": "\u1D542",                                                                                              // 247
  "Kscr;": "\u1D4A6",                                                                                              // 248
  "LJcy;": "\u0409",                                                                                               // 249
  "LT": "<",                                                                                                       // 250
  "LT;": "<",                                                                                                      // 251
  "Lacute;": "\u0139",                                                                                             // 252
  "Lambda;": "\u039B",                                                                                             // 253
  "Lang;": "\u27EA",                                                                                               // 254
  "Laplacetrf;": "\u2112",                                                                                         // 255
  "Larr;": "\u219E",                                                                                               // 256
  "Lcaron;": "\u013D",                                                                                             // 257
  "Lcedil;": "\u013B",                                                                                             // 258
  "Lcy;": "\u041B",                                                                                                // 259
  "LeftAngleBracket;": "\u27E8",                                                                                   // 260
  "LeftArrow;": "\u2190",                                                                                          // 261
  "LeftArrowBar;": "\u21E4",                                                                                       // 262
  "LeftArrowRightArrow;": "\u21C6",                                                                                // 263
  "LeftCeiling;": "\u2308",                                                                                        // 264
  "LeftDoubleBracket;": "\u27E6",                                                                                  // 265
  "LeftDownTeeVector;": "\u2961",                                                                                  // 266
  "LeftDownVector;": "\u21C3",                                                                                     // 267
  "LeftDownVectorBar;": "\u2959",                                                                                  // 268
  "LeftFloor;": "\u230A",                                                                                          // 269
  "LeftRightArrow;": "\u2194",                                                                                     // 270
  "LeftRightVector;": "\u294E",                                                                                    // 271
  "LeftTee;": "\u22A3",                                                                                            // 272
  "LeftTeeArrow;": "\u21A4",                                                                                       // 273
  "LeftTeeVector;": "\u295A",                                                                                      // 274
  "LeftTriangle;": "\u22B2",                                                                                       // 275
  "LeftTriangleBar;": "\u29CF",                                                                                    // 276
  "LeftTriangleEqual;": "\u22B4",                                                                                  // 277
  "LeftUpDownVector;": "\u2951",                                                                                   // 278
  "LeftUpTeeVector;": "\u2960",                                                                                    // 279
  "LeftUpVector;": "\u21BF",                                                                                       // 280
  "LeftUpVectorBar;": "\u2958",                                                                                    // 281
  "LeftVector;": "\u21BC",                                                                                         // 282
  "LeftVectorBar;": "\u2952",                                                                                      // 283
  "Leftarrow;": "\u21D0",                                                                                          // 284
  "Leftrightarrow;": "\u21D4",                                                                                     // 285
  "LessEqualGreater;": "\u22DA",                                                                                   // 286
  "LessFullEqual;": "\u2266",                                                                                      // 287
  "LessGreater;": "\u2276",                                                                                        // 288
  "LessLess;": "\u2AA1",                                                                                           // 289
  "LessSlantEqual;": "\u2A7D",                                                                                     // 290
  "LessTilde;": "\u2272",                                                                                          // 291
  "Lfr;": "\u1D50F",                                                                                               // 292
  "Ll;": "\u22D8",                                                                                                 // 293
  "Lleftarrow;": "\u21DA",                                                                                         // 294
  "Lmidot;": "\u013F",                                                                                             // 295
  "LongLeftArrow;": "\u27F5",                                                                                      // 296
  "LongLeftRightArrow;": "\u27F7",                                                                                 // 297
  "LongRightArrow;": "\u27F6",                                                                                     // 298
  "Longleftarrow;": "\u27F8",                                                                                      // 299
  "Longleftrightarrow;": "\u27FA",                                                                                 // 300
  "Longrightarrow;": "\u27F9",                                                                                     // 301
  "Lopf;": "\u1D543",                                                                                              // 302
  "LowerLeftArrow;": "\u2199",                                                                                     // 303
  "LowerRightArrow;": "\u2198",                                                                                    // 304
  "Lscr;": "\u2112",                                                                                               // 305
  "Lsh;": "\u21B0",                                                                                                // 306
  "Lstrok;": "\u0141",                                                                                             // 307
  "Lt;": "\u226A",                                                                                                 // 308
  "Map;": "\u2905",                                                                                                // 309
  "Mcy;": "\u041C",                                                                                                // 310
  "MediumSpace;": "\u205F",                                                                                        // 311
  "Mellintrf;": "\u2133",                                                                                          // 312
  "Mfr;": "\u1D510",                                                                                               // 313
  "MinusPlus;": "\u2213",                                                                                          // 314
  "Mopf;": "\u1D544",                                                                                              // 315
  "Mscr;": "\u2133",                                                                                               // 316
  "Mu;": "\u039C",                                                                                                 // 317
  "NJcy;": "\u040A",                                                                                               // 318
  "Nacute;": "\u0143",                                                                                             // 319
  "Ncaron;": "\u0147",                                                                                             // 320
  "Ncedil;": "\u0145",                                                                                             // 321
  "Ncy;": "\u041D",                                                                                                // 322
  "NegativeMediumSpace;": "\u200B",                                                                                // 323
  "NegativeThickSpace;": "\u200B",                                                                                 // 324
  "NegativeThinSpace;": "\u200B",                                                                                  // 325
  "NegativeVeryThinSpace;": "\u200B",                                                                              // 326
  "NestedGreaterGreater;": "\u226B",                                                                               // 327
  "NestedLessLess;": "\u226A",                                                                                     // 328
  "NewLine;": "\u000A",                                                                                            // 329
  "Nfr;": "\u1D511",                                                                                               // 330
  "NoBreak;": "\u2060",                                                                                            // 331
  "NonBreakingSpace;": "\u00A0",                                                                                   // 332
  "Nopf;": "\u2115",                                                                                               // 333
  "Not;": "\u2AEC",                                                                                                // 334
  "NotCongruent;": "\u2262",                                                                                       // 335
  "NotCupCap;": "\u226D",                                                                                          // 336
  "NotDoubleVerticalBar;": "\u2226",                                                                               // 337
  "NotElement;": "\u2209",                                                                                         // 338
  "NotEqual;": "\u2260",                                                                                           // 339
  "NotEqualTilde;": "\u2242\u0338",                                                                                // 340
  "NotExists;": "\u2204",                                                                                          // 341
  "NotGreater;": "\u226F",                                                                                         // 342
  "NotGreaterEqual;": "\u2271",                                                                                    // 343
  "NotGreaterFullEqual;": "\u2267\u0338",                                                                          // 344
  "NotGreaterGreater;": "\u226B\u0338",                                                                            // 345
  "NotGreaterLess;": "\u2279",                                                                                     // 346
  "NotGreaterSlantEqual;": "\u2A7E\u0338",                                                                         // 347
  "NotGreaterTilde;": "\u2275",                                                                                    // 348
  "NotHumpDownHump;": "\u224E\u0338",                                                                              // 349
  "NotHumpEqual;": "\u224F\u0338",                                                                                 // 350
  "NotLeftTriangle;": "\u22EA",                                                                                    // 351
  "NotLeftTriangleBar;": "\u29CF\u0338",                                                                           // 352
  "NotLeftTriangleEqual;": "\u22EC",                                                                               // 353
  "NotLess;": "\u226E",                                                                                            // 354
  "NotLessEqual;": "\u2270",                                                                                       // 355
  "NotLessGreater;": "\u2278",                                                                                     // 356
  "NotLessLess;": "\u226A\u0338",                                                                                  // 357
  "NotLessSlantEqual;": "\u2A7D\u0338",                                                                            // 358
  "NotLessTilde;": "\u2274",                                                                                       // 359
  "NotNestedGreaterGreater;": "\u2AA2\u0338",                                                                      // 360
  "NotNestedLessLess;": "\u2AA1\u0338",                                                                            // 361
  "NotPrecedes;": "\u2280",                                                                                        // 362
  "NotPrecedesEqual;": "\u2AAF\u0338",                                                                             // 363
  "NotPrecedesSlantEqual;": "\u22E0",                                                                              // 364
  "NotReverseElement;": "\u220C",                                                                                  // 365
  "NotRightTriangle;": "\u22EB",                                                                                   // 366
  "NotRightTriangleBar;": "\u29D0\u0338",                                                                          // 367
  "NotRightTriangleEqual;": "\u22ED",                                                                              // 368
  "NotSquareSubset;": "\u228F\u0338",                                                                              // 369
  "NotSquareSubsetEqual;": "\u22E2",                                                                               // 370
  "NotSquareSuperset;": "\u2290\u0338",                                                                            // 371
  "NotSquareSupersetEqual;": "\u22E3",                                                                             // 372
  "NotSubset;": "\u2282\u20D2",                                                                                    // 373
  "NotSubsetEqual;": "\u2288",                                                                                     // 374
  "NotSucceeds;": "\u2281",                                                                                        // 375
  "NotSucceedsEqual;": "\u2AB0\u0338",                                                                             // 376
  "NotSucceedsSlantEqual;": "\u22E1",                                                                              // 377
  "NotSucceedsTilde;": "\u227F\u0338",                                                                             // 378
  "NotSuperset;": "\u2283\u20D2",                                                                                  // 379
  "NotSupersetEqual;": "\u2289",                                                                                   // 380
  "NotTilde;": "\u2241",                                                                                           // 381
  "NotTildeEqual;": "\u2244",                                                                                      // 382
  "NotTildeFullEqual;": "\u2247",                                                                                  // 383
  "NotTildeTilde;": "\u2249",                                                                                      // 384
  "NotVerticalBar;": "\u2224",                                                                                     // 385
  "Nscr;": "\u1D4A9",                                                                                              // 386
  "Ntilde": "\u00D1",                                                                                              // 387
  "Ntilde;": "\u00D1",                                                                                             // 388
  "Nu;": "\u039D",                                                                                                 // 389
  "OElig;": "\u0152",                                                                                              // 390
  "Oacute": "\u00D3",                                                                                              // 391
  "Oacute;": "\u00D3",                                                                                             // 392
  "Ocirc": "\u00D4",                                                                                               // 393
  "Ocirc;": "\u00D4",                                                                                              // 394
  "Ocy;": "\u041E",                                                                                                // 395
  "Odblac;": "\u0150",                                                                                             // 396
  "Ofr;": "\u1D512",                                                                                               // 397
  "Ograve": "\u00D2",                                                                                              // 398
  "Ograve;": "\u00D2",                                                                                             // 399
  "Omacr;": "\u014C",                                                                                              // 400
  "Omega;": "\u03A9",                                                                                              // 401
  "Omicron;": "\u039F",                                                                                            // 402
  "Oopf;": "\u1D546",                                                                                              // 403
  "OpenCurlyDoubleQuote;": "\u201C",                                                                               // 404
  "OpenCurlyQuote;": "\u2018",                                                                                     // 405
  "Or;": "\u2A54",                                                                                                 // 406
  "Oscr;": "\u1D4AA",                                                                                              // 407
  "Oslash": "\u00D8",                                                                                              // 408
  "Oslash;": "\u00D8",                                                                                             // 409
  "Otilde": "\u00D5",                                                                                              // 410
  "Otilde;": "\u00D5",                                                                                             // 411
  "Otimes;": "\u2A37",                                                                                             // 412
  "Ouml": "\u00D6",                                                                                                // 413
  "Ouml;": "\u00D6",                                                                                               // 414
  "OverBar;": "\u203E",                                                                                            // 415
  "OverBrace;": "\u23DE",                                                                                          // 416
  "OverBracket;": "\u23B4",                                                                                        // 417
  "OverParenthesis;": "\u23DC",                                                                                    // 418
  "PartialD;": "\u2202",                                                                                           // 419
  "Pcy;": "\u041F",                                                                                                // 420
  "Pfr;": "\u1D513",                                                                                               // 421
  "Phi;": "\u03A6",                                                                                                // 422
  "Pi;": "\u03A0",                                                                                                 // 423
  "PlusMinus;": "\u00B1",                                                                                          // 424
  "Poincareplane;": "\u210C",                                                                                      // 425
  "Popf;": "\u2119",                                                                                               // 426
  "Pr;": "\u2ABB",                                                                                                 // 427
  "Precedes;": "\u227A",                                                                                           // 428
  "PrecedesEqual;": "\u2AAF",                                                                                      // 429
  "PrecedesSlantEqual;": "\u227C",                                                                                 // 430
  "PrecedesTilde;": "\u227E",                                                                                      // 431
  "Prime;": "\u2033",                                                                                              // 432
  "Product;": "\u220F",                                                                                            // 433
  "Proportion;": "\u2237",                                                                                         // 434
  "Proportional;": "\u221D",                                                                                       // 435
  "Pscr;": "\u1D4AB",                                                                                              // 436
  "Psi;": "\u03A8",                                                                                                // 437
  "QUOT": "\u0022",                                                                                                // 438
  "QUOT;": "\u0022",                                                                                               // 439
  "Qfr;": "\u1D514",                                                                                               // 440
  "Qopf;": "\u211A",                                                                                               // 441
  "Qscr;": "\u1D4AC",                                                                                              // 442
  "RBarr;": "\u2910",                                                                                              // 443
  "REG": "\u00AE",                                                                                                 // 444
  "REG;": "\u00AE",                                                                                                // 445
  "Racute;": "\u0154",                                                                                             // 446
  "Rang;": "\u27EB",                                                                                               // 447
  "Rarr;": "\u21A0",                                                                                               // 448
  "Rarrtl;": "\u2916",                                                                                             // 449
  "Rcaron;": "\u0158",                                                                                             // 450
  "Rcedil;": "\u0156",                                                                                             // 451
  "Rcy;": "\u0420",                                                                                                // 452
  "Re;": "\u211C",                                                                                                 // 453
  "ReverseElement;": "\u220B",                                                                                     // 454
  "ReverseEquilibrium;": "\u21CB",                                                                                 // 455
  "ReverseUpEquilibrium;": "\u296F",                                                                               // 456
  "Rfr;": "\u211C",                                                                                                // 457
  "Rho;": "\u03A1",                                                                                                // 458
  "RightAngleBracket;": "\u27E9",                                                                                  // 459
  "RightArrow;": "\u2192",                                                                                         // 460
  "RightArrowBar;": "\u21E5",                                                                                      // 461
  "RightArrowLeftArrow;": "\u21C4",                                                                                // 462
  "RightCeiling;": "\u2309",                                                                                       // 463
  "RightDoubleBracket;": "\u27E7",                                                                                 // 464
  "RightDownTeeVector;": "\u295D",                                                                                 // 465
  "RightDownVector;": "\u21C2",                                                                                    // 466
  "RightDownVectorBar;": "\u2955",                                                                                 // 467
  "RightFloor;": "\u230B",                                                                                         // 468
  "RightTee;": "\u22A2",                                                                                           // 469
  "RightTeeArrow;": "\u21A6",                                                                                      // 470
  "RightTeeVector;": "\u295B",                                                                                     // 471
  "RightTriangle;": "\u22B3",                                                                                      // 472
  "RightTriangleBar;": "\u29D0",                                                                                   // 473
  "RightTriangleEqual;": "\u22B5",                                                                                 // 474
  "RightUpDownVector;": "\u294F",                                                                                  // 475
  "RightUpTeeVector;": "\u295C",                                                                                   // 476
  "RightUpVector;": "\u21BE",                                                                                      // 477
  "RightUpVectorBar;": "\u2954",                                                                                   // 478
  "RightVector;": "\u21C0",                                                                                        // 479
  "RightVectorBar;": "\u2953",                                                                                     // 480
  "Rightarrow;": "\u21D2",                                                                                         // 481
  "Ropf;": "\u211D",                                                                                               // 482
  "RoundImplies;": "\u2970",                                                                                       // 483
  "Rrightarrow;": "\u21DB",                                                                                        // 484
  "Rscr;": "\u211B",                                                                                               // 485
  "Rsh;": "\u21B1",                                                                                                // 486
  "RuleDelayed;": "\u29F4",                                                                                        // 487
  "SHCHcy;": "\u0429",                                                                                             // 488
  "SHcy;": "\u0428",                                                                                               // 489
  "SOFTcy;": "\u042C",                                                                                             // 490
  "Sacute;": "\u015A",                                                                                             // 491
  "Sc;": "\u2ABC",                                                                                                 // 492
  "Scaron;": "\u0160",                                                                                             // 493
  "Scedil;": "\u015E",                                                                                             // 494
  "Scirc;": "\u015C",                                                                                              // 495
  "Scy;": "\u0421",                                                                                                // 496
  "Sfr;": "\u1D516",                                                                                               // 497
  "ShortDownArrow;": "\u2193",                                                                                     // 498
  "ShortLeftArrow;": "\u2190",                                                                                     // 499
  "ShortRightArrow;": "\u2192",                                                                                    // 500
  "ShortUpArrow;": "\u2191",                                                                                       // 501
  "Sigma;": "\u03A3",                                                                                              // 502
  "SmallCircle;": "\u2218",                                                                                        // 503
  "Sopf;": "\u1D54A",                                                                                              // 504
  "Sqrt;": "\u221A",                                                                                               // 505
  "Square;": "\u25A1",                                                                                             // 506
  "SquareIntersection;": "\u2293",                                                                                 // 507
  "SquareSubset;": "\u228F",                                                                                       // 508
  "SquareSubsetEqual;": "\u2291",                                                                                  // 509
  "SquareSuperset;": "\u2290",                                                                                     // 510
  "SquareSupersetEqual;": "\u2292",                                                                                // 511
  "SquareUnion;": "\u2294",                                                                                        // 512
  "Sscr;": "\u1D4AE",                                                                                              // 513
  "Star;": "\u22C6",                                                                                               // 514
  "Sub;": "\u22D0",                                                                                                // 515
  "Subset;": "\u22D0",                                                                                             // 516
  "SubsetEqual;": "\u2286",                                                                                        // 517
  "Succeeds;": "\u227B",                                                                                           // 518
  "SucceedsEqual;": "\u2AB0",                                                                                      // 519
  "SucceedsSlantEqual;": "\u227D",                                                                                 // 520
  "SucceedsTilde;": "\u227F",                                                                                      // 521
  "SuchThat;": "\u220B",                                                                                           // 522
  "Sum;": "\u2211",                                                                                                // 523
  "Sup;": "\u22D1",                                                                                                // 524
  "Superset;": "\u2283",                                                                                           // 525
  "SupersetEqual;": "\u2287",                                                                                      // 526
  "Supset;": "\u22D1",                                                                                             // 527
  "THORN": "\u00DE",                                                                                               // 528
  "THORN;": "\u00DE",                                                                                              // 529
  "TRADE;": "\u2122",                                                                                              // 530
  "TSHcy;": "\u040B",                                                                                              // 531
  "TScy;": "\u0426",                                                                                               // 532
  "Tab;": "\u0009",                                                                                                // 533
  "Tau;": "\u03A4",                                                                                                // 534
  "Tcaron;": "\u0164",                                                                                             // 535
  "Tcedil;": "\u0162",                                                                                             // 536
  "Tcy;": "\u0422",                                                                                                // 537
  "Tfr;": "\u1D517",                                                                                               // 538
  "Therefore;": "\u2234",                                                                                          // 539
  "Theta;": "\u0398",                                                                                              // 540
  "ThickSpace;": "\u205F\u200A",                                                                                   // 541
  "ThinSpace;": "\u2009",                                                                                          // 542
  "Tilde;": "\u223C",                                                                                              // 543
  "TildeEqual;": "\u2243",                                                                                         // 544
  "TildeFullEqual;": "\u2245",                                                                                     // 545
  "TildeTilde;": "\u2248",                                                                                         // 546
  "Topf;": "\u1D54B",                                                                                              // 547
  "TripleDot;": "\u20DB",                                                                                          // 548
  "Tscr;": "\u1D4AF",                                                                                              // 549
  "Tstrok;": "\u0166",                                                                                             // 550
  "Uacute": "\u00DA",                                                                                              // 551
  "Uacute;": "\u00DA",                                                                                             // 552
  "Uarr;": "\u219F",                                                                                               // 553
  "Uarrocir;": "\u2949",                                                                                           // 554
  "Ubrcy;": "\u040E",                                                                                              // 555
  "Ubreve;": "\u016C",                                                                                             // 556
  "Ucirc": "\u00DB",                                                                                               // 557
  "Ucirc;": "\u00DB",                                                                                              // 558
  "Ucy;": "\u0423",                                                                                                // 559
  "Udblac;": "\u0170",                                                                                             // 560
  "Ufr;": "\u1D518",                                                                                               // 561
  "Ugrave": "\u00D9",                                                                                              // 562
  "Ugrave;": "\u00D9",                                                                                             // 563
  "Umacr;": "\u016A",                                                                                              // 564
  "UnderBar;": "_",                                                                                                // 565
  "UnderBrace;": "\u23DF",                                                                                         // 566
  "UnderBracket;": "\u23B5",                                                                                       // 567
  "UnderParenthesis;": "\u23DD",                                                                                   // 568
  "Union;": "\u22C3",                                                                                              // 569
  "UnionPlus;": "\u228E",                                                                                          // 570
  "Uogon;": "\u0172",                                                                                              // 571
  "Uopf;": "\u1D54C",                                                                                              // 572
  "UpArrow;": "\u2191",                                                                                            // 573
  "UpArrowBar;": "\u2912",                                                                                         // 574
  "UpArrowDownArrow;": "\u21C5",                                                                                   // 575
  "UpDownArrow;": "\u2195",                                                                                        // 576
  "UpEquilibrium;": "\u296E",                                                                                      // 577
  "UpTee;": "\u22A5",                                                                                              // 578
  "UpTeeArrow;": "\u21A5",                                                                                         // 579
  "Uparrow;": "\u21D1",                                                                                            // 580
  "Updownarrow;": "\u21D5",                                                                                        // 581
  "UpperLeftArrow;": "\u2196",                                                                                     // 582
  "UpperRightArrow;": "\u2197",                                                                                    // 583
  "Upsi;": "\u03D2",                                                                                               // 584
  "Upsilon;": "\u03A5",                                                                                            // 585
  "Uring;": "\u016E",                                                                                              // 586
  "Uscr;": "\u1D4B0",                                                                                              // 587
  "Utilde;": "\u0168",                                                                                             // 588
  "Uuml": "\u00DC",                                                                                                // 589
  "Uuml;": "\u00DC",                                                                                               // 590
  "VDash;": "\u22AB",                                                                                              // 591
  "Vbar;": "\u2AEB",                                                                                               // 592
  "Vcy;": "\u0412",                                                                                                // 593
  "Vdash;": "\u22A9",                                                                                              // 594
  "Vdashl;": "\u2AE6",                                                                                             // 595
  "Vee;": "\u22C1",                                                                                                // 596
  "Verbar;": "\u2016",                                                                                             // 597
  "Vert;": "\u2016",                                                                                               // 598
  "VerticalBar;": "\u2223",                                                                                        // 599
  "VerticalLine;": "|",                                                                                            // 600
  "VerticalSeparator;": "\u2758",                                                                                  // 601
  "VerticalTilde;": "\u2240",                                                                                      // 602
  "VeryThinSpace;": "\u200A",                                                                                      // 603
  "Vfr;": "\u1D519",                                                                                               // 604
  "Vopf;": "\u1D54D",                                                                                              // 605
  "Vscr;": "\u1D4B1",                                                                                              // 606
  "Vvdash;": "\u22AA",                                                                                             // 607
  "Wcirc;": "\u0174",                                                                                              // 608
  "Wedge;": "\u22C0",                                                                                              // 609
  "Wfr;": "\u1D51A",                                                                                               // 610
  "Wopf;": "\u1D54E",                                                                                              // 611
  "Wscr;": "\u1D4B2",                                                                                              // 612
  "Xfr;": "\u1D51B",                                                                                               // 613
  "Xi;": "\u039E",                                                                                                 // 614
  "Xopf;": "\u1D54F",                                                                                              // 615
  "Xscr;": "\u1D4B3",                                                                                              // 616
  "YAcy;": "\u042F",                                                                                               // 617
  "YIcy;": "\u0407",                                                                                               // 618
  "YUcy;": "\u042E",                                                                                               // 619
  "Yacute": "\u00DD",                                                                                              // 620
  "Yacute;": "\u00DD",                                                                                             // 621
  "Ycirc;": "\u0176",                                                                                              // 622
  "Ycy;": "\u042B",                                                                                                // 623
  "Yfr;": "\u1D51C",                                                                                               // 624
  "Yopf;": "\u1D550",                                                                                              // 625
  "Yscr;": "\u1D4B4",                                                                                              // 626
  "Yuml;": "\u0178",                                                                                               // 627
  "ZHcy;": "\u0416",                                                                                               // 628
  "Zacute;": "\u0179",                                                                                             // 629
  "Zcaron;": "\u017D",                                                                                             // 630
  "Zcy;": "\u0417",                                                                                                // 631
  "Zdot;": "\u017B",                                                                                               // 632
  "ZeroWidthSpace;": "\u200B",                                                                                     // 633
  "Zeta;": "\u0396",                                                                                               // 634
  "Zfr;": "\u2128",                                                                                                // 635
  "Zopf;": "\u2124",                                                                                               // 636
  "Zscr;": "\u1D4B5",                                                                                              // 637
  "aacute": "\u00E1",                                                                                              // 638
  "aacute;": "\u00E1",                                                                                             // 639
  "abreve;": "\u0103",                                                                                             // 640
  "ac;": "\u223E",                                                                                                 // 641
  "acE;": "\u223E\u0333",                                                                                          // 642
  "acd;": "\u223F",                                                                                                // 643
  "acirc": "\u00E2",                                                                                               // 644
  "acirc;": "\u00E2",                                                                                              // 645
  "acute": "\u00B4",                                                                                               // 646
  "acute;": "\u00B4",                                                                                              // 647
  "acy;": "\u0430",                                                                                                // 648
  "aelig": "\u00E6",                                                                                               // 649
  "aelig;": "\u00E6",                                                                                              // 650
  "af;": "\u2061",                                                                                                 // 651
  "afr;": "\u1D51E",                                                                                               // 652
  "agrave": "\u00E0",                                                                                              // 653
  "agrave;": "\u00E0",                                                                                             // 654
  "alefsym;": "\u2135",                                                                                            // 655
  "aleph;": "\u2135",                                                                                              // 656
  "alpha;": "\u03B1",                                                                                              // 657
  "amacr;": "\u0101",                                                                                              // 658
  "amalg;": "\u2A3F",                                                                                              // 659
  "amp": "&",                                                                                                      // 660
  "amp;": "&",                                                                                                     // 661
  "and;": "\u2227",                                                                                                // 662
  "andand;": "\u2A55",                                                                                             // 663
  "andd;": "\u2A5C",                                                                                               // 664
  "andslope;": "\u2A58",                                                                                           // 665
  "andv;": "\u2A5A",                                                                                               // 666
  "ang;": "\u2220",                                                                                                // 667
  "ange;": "\u29A4",                                                                                               // 668
  "angle;": "\u2220",                                                                                              // 669
  "angmsd;": "\u2221",                                                                                             // 670
  "angmsdaa;": "\u29A8",                                                                                           // 671
  "angmsdab;": "\u29A9",                                                                                           // 672
  "angmsdac;": "\u29AA",                                                                                           // 673
  "angmsdad;": "\u29AB",                                                                                           // 674
  "angmsdae;": "\u29AC",                                                                                           // 675
  "angmsdaf;": "\u29AD",                                                                                           // 676
  "angmsdag;": "\u29AE",                                                                                           // 677
  "angmsdah;": "\u29AF",                                                                                           // 678
  "angrt;": "\u221F",                                                                                              // 679
  "angrtvb;": "\u22BE",                                                                                            // 680
  "angrtvbd;": "\u299D",                                                                                           // 681
  "angsph;": "\u2222",                                                                                             // 682
  "angst;": "\u00C5",                                                                                              // 683
  "angzarr;": "\u237C",                                                                                            // 684
  "aogon;": "\u0105",                                                                                              // 685
  "aopf;": "\u1D552",                                                                                              // 686
  "ap;": "\u2248",                                                                                                 // 687
  "apE;": "\u2A70",                                                                                                // 688
  "apacir;": "\u2A6F",                                                                                             // 689
  "ape;": "\u224A",                                                                                                // 690
  "apid;": "\u224B",                                                                                               // 691
  "apos;": "\u0027",                                                                                               // 692
  "approx;": "\u2248",                                                                                             // 693
  "approxeq;": "\u224A",                                                                                           // 694
  "aring": "\u00E5",                                                                                               // 695
  "aring;": "\u00E5",                                                                                              // 696
  "ascr;": "\u1D4B6",                                                                                              // 697
  "ast;": "*",                                                                                                     // 698
  "asymp;": "\u2248",                                                                                              // 699
  "asympeq;": "\u224D",                                                                                            // 700
  "atilde": "\u00E3",                                                                                              // 701
  "atilde;": "\u00E3",                                                                                             // 702
  "auml": "\u00E4",                                                                                                // 703
  "auml;": "\u00E4",                                                                                               // 704
  "awconint;": "\u2233",                                                                                           // 705
  "awint;": "\u2A11",                                                                                              // 706
  "bNot;": "\u2AED",                                                                                               // 707
  "backcong;": "\u224C",                                                                                           // 708
  "backepsilon;": "\u03F6",                                                                                        // 709
  "backprime;": "\u2035",                                                                                          // 710
  "backsim;": "\u223D",                                                                                            // 711
  "backsimeq;": "\u22CD",                                                                                          // 712
  "barvee;": "\u22BD",                                                                                             // 713
  "barwed;": "\u2305",                                                                                             // 714
  "barwedge;": "\u2305",                                                                                           // 715
  "bbrk;": "\u23B5",                                                                                               // 716
  "bbrktbrk;": "\u23B6",                                                                                           // 717
  "bcong;": "\u224C",                                                                                              // 718
  "bcy;": "\u0431",                                                                                                // 719
  "bdquo;": "\u201E",                                                                                              // 720
  "becaus;": "\u2235",                                                                                             // 721
  "because;": "\u2235",                                                                                            // 722
  "bemptyv;": "\u29B0",                                                                                            // 723
  "bepsi;": "\u03F6",                                                                                              // 724
  "bernou;": "\u212C",                                                                                             // 725
  "beta;": "\u03B2",                                                                                               // 726
  "beth;": "\u2136",                                                                                               // 727
  "between;": "\u226C",                                                                                            // 728
  "bfr;": "\u1D51F",                                                                                               // 729
  "bigcap;": "\u22C2",                                                                                             // 730
  "bigcirc;": "\u25EF",                                                                                            // 731
  "bigcup;": "\u22C3",                                                                                             // 732
  "bigodot;": "\u2A00",                                                                                            // 733
  "bigoplus;": "\u2A01",                                                                                           // 734
  "bigotimes;": "\u2A02",                                                                                          // 735
  "bigsqcup;": "\u2A06",                                                                                           // 736
  "bigstar;": "\u2605",                                                                                            // 737
  "bigtriangledown;": "\u25BD",                                                                                    // 738
  "bigtriangleup;": "\u25B3",                                                                                      // 739
  "biguplus;": "\u2A04",                                                                                           // 740
  "bigvee;": "\u22C1",                                                                                             // 741
  "bigwedge;": "\u22C0",                                                                                           // 742
  "bkarow;": "\u290D",                                                                                             // 743
  "blacklozenge;": "\u29EB",                                                                                       // 744
  "blacksquare;": "\u25AA",                                                                                        // 745
  "blacktriangle;": "\u25B4",                                                                                      // 746
  "blacktriangledown;": "\u25BE",                                                                                  // 747
  "blacktriangleleft;": "\u25C2",                                                                                  // 748
  "blacktriangleright;": "\u25B8",                                                                                 // 749
  "blank;": "\u2423",                                                                                              // 750
  "blk12;": "\u2592",                                                                                              // 751
  "blk14;": "\u2591",                                                                                              // 752
  "blk34;": "\u2593",                                                                                              // 753
  "block;": "\u2588",                                                                                              // 754
  "bne;": "\u003D\u20E5",                                                                                          // 755
  "bnequiv;": "\u2261\u20E5",                                                                                      // 756
  "bnot;": "\u2310",                                                                                               // 757
  "bopf;": "\u1D553",                                                                                              // 758
  "bot;": "\u22A5",                                                                                                // 759
  "bottom;": "\u22A5",                                                                                             // 760
  "bowtie;": "\u22C8",                                                                                             // 761
  "boxDL;": "\u2557",                                                                                              // 762
  "boxDR;": "\u2554",                                                                                              // 763
  "boxDl;": "\u2556",                                                                                              // 764
  "boxDr;": "\u2553",                                                                                              // 765
  "boxH;": "\u2550",                                                                                               // 766
  "boxHD;": "\u2566",                                                                                              // 767
  "boxHU;": "\u2569",                                                                                              // 768
  "boxHd;": "\u2564",                                                                                              // 769
  "boxHu;": "\u2567",                                                                                              // 770
  "boxUL;": "\u255D",                                                                                              // 771
  "boxUR;": "\u255A",                                                                                              // 772
  "boxUl;": "\u255C",                                                                                              // 773
  "boxUr;": "\u2559",                                                                                              // 774
  "boxV;": "\u2551",                                                                                               // 775
  "boxVH;": "\u256C",                                                                                              // 776
  "boxVL;": "\u2563",                                                                                              // 777
  "boxVR;": "\u2560",                                                                                              // 778
  "boxVh;": "\u256B",                                                                                              // 779
  "boxVl;": "\u2562",                                                                                              // 780
  "boxVr;": "\u255F",                                                                                              // 781
  "boxbox;": "\u29C9",                                                                                             // 782
  "boxdL;": "\u2555",                                                                                              // 783
  "boxdR;": "\u2552",                                                                                              // 784
  "boxdl;": "\u2510",                                                                                              // 785
  "boxdr;": "\u250C",                                                                                              // 786
  "boxh;": "\u2500",                                                                                               // 787
  "boxhD;": "\u2565",                                                                                              // 788
  "boxhU;": "\u2568",                                                                                              // 789
  "boxhd;": "\u252C",                                                                                              // 790
  "boxhu;": "\u2534",                                                                                              // 791
  "boxminus;": "\u229F",                                                                                           // 792
  "boxplus;": "\u229E",                                                                                            // 793
  "boxtimes;": "\u22A0",                                                                                           // 794
  "boxuL;": "\u255B",                                                                                              // 795
  "boxuR;": "\u2558",                                                                                              // 796
  "boxul;": "\u2518",                                                                                              // 797
  "boxur;": "\u2514",                                                                                              // 798
  "boxv;": "\u2502",                                                                                               // 799
  "boxvH;": "\u256A",                                                                                              // 800
  "boxvL;": "\u2561",                                                                                              // 801
  "boxvR;": "\u255E",                                                                                              // 802
  "boxvh;": "\u253C",                                                                                              // 803
  "boxvl;": "\u2524",                                                                                              // 804
  "boxvr;": "\u251C",                                                                                              // 805
  "bprime;": "\u2035",                                                                                             // 806
  "breve;": "\u02D8",                                                                                              // 807
  "brvbar": "\u00A6",                                                                                              // 808
  "brvbar;": "\u00A6",                                                                                             // 809
  "bscr;": "\u1D4B7",                                                                                              // 810
  "bsemi;": "\u204F",                                                                                              // 811
  "bsim;": "\u223D",                                                                                               // 812
  "bsime;": "\u22CD",                                                                                              // 813
  "bsol;": "\u005C",                                                                                               // 814
  "bsolb;": "\u29C5",                                                                                              // 815
  "bsolhsub;": "\u27C8",                                                                                           // 816
  "bull;": "\u2022",                                                                                               // 817
  "bullet;": "\u2022",                                                                                             // 818
  "bump;": "\u224E",                                                                                               // 819
  "bumpE;": "\u2AAE",                                                                                              // 820
  "bumpe;": "\u224F",                                                                                              // 821
  "bumpeq;": "\u224F",                                                                                             // 822
  "cacute;": "\u0107",                                                                                             // 823
  "cap;": "\u2229",                                                                                                // 824
  "capand;": "\u2A44",                                                                                             // 825
  "capbrcup;": "\u2A49",                                                                                           // 826
  "capcap;": "\u2A4B",                                                                                             // 827
  "capcup;": "\u2A47",                                                                                             // 828
  "capdot;": "\u2A40",                                                                                             // 829
  "caps;": "\u2229\uFE00",                                                                                         // 830
  "caret;": "\u2041",                                                                                              // 831
  "caron;": "\u02C7",                                                                                              // 832
  "ccaps;": "\u2A4D",                                                                                              // 833
  "ccaron;": "\u010D",                                                                                             // 834
  "ccedil": "\u00E7",                                                                                              // 835
  "ccedil;": "\u00E7",                                                                                             // 836
  "ccirc;": "\u0109",                                                                                              // 837
  "ccups;": "\u2A4C",                                                                                              // 838
  "ccupssm;": "\u2A50",                                                                                            // 839
  "cdot;": "\u010B",                                                                                               // 840
  "cedil": "\u00B8",                                                                                               // 841
  "cedil;": "\u00B8",                                                                                              // 842
  "cemptyv;": "\u29B2",                                                                                            // 843
  "cent": "\u00A2",                                                                                                // 844
  "cent;": "\u00A2",                                                                                               // 845
  "centerdot;": "\u00B7",                                                                                          // 846
  "cfr;": "\u1D520",                                                                                               // 847
  "chcy;": "\u0447",                                                                                               // 848
  "check;": "\u2713",                                                                                              // 849
  "checkmark;": "\u2713",                                                                                          // 850
  "chi;": "\u03C7",                                                                                                // 851
  "cir;": "\u25CB",                                                                                                // 852
  "cirE;": "\u29C3",                                                                                               // 853
  "circ;": "\u02C6",                                                                                               // 854
  "circeq;": "\u2257",                                                                                             // 855
  "circlearrowleft;": "\u21BA",                                                                                    // 856
  "circlearrowright;": "\u21BB",                                                                                   // 857
  "circledR;": "\u00AE",                                                                                           // 858
  "circledS;": "\u24C8",                                                                                           // 859
  "circledast;": "\u229B",                                                                                         // 860
  "circledcirc;": "\u229A",                                                                                        // 861
  "circleddash;": "\u229D",                                                                                        // 862
  "cire;": "\u2257",                                                                                               // 863
  "cirfnint;": "\u2A10",                                                                                           // 864
  "cirmid;": "\u2AEF",                                                                                             // 865
  "cirscir;": "\u29C2",                                                                                            // 866
  "clubs;": "\u2663",                                                                                              // 867
  "clubsuit;": "\u2663",                                                                                           // 868
  "colon;": ":",                                                                                                   // 869
  "colone;": "\u2254",                                                                                             // 870
  "coloneq;": "\u2254",                                                                                            // 871
  "comma;": ",",                                                                                                   // 872
  "commat;": "@",                                                                                                  // 873
  "comp;": "\u2201",                                                                                               // 874
  "compfn;": "\u2218",                                                                                             // 875
  "complement;": "\u2201",                                                                                         // 876
  "complexes;": "\u2102",                                                                                          // 877
  "cong;": "\u2245",                                                                                               // 878
  "congdot;": "\u2A6D",                                                                                            // 879
  "conint;": "\u222E",                                                                                             // 880
  "copf;": "\u1D554",                                                                                              // 881
  "coprod;": "\u2210",                                                                                             // 882
  "copy": "\u00A9",                                                                                                // 883
  "copy;": "\u00A9",                                                                                               // 884
  "copysr;": "\u2117",                                                                                             // 885
  "crarr;": "\u21B5",                                                                                              // 886
  "cross;": "\u2717",                                                                                              // 887
  "cscr;": "\u1D4B8",                                                                                              // 888
  "csub;": "\u2ACF",                                                                                               // 889
  "csube;": "\u2AD1",                                                                                              // 890
  "csup;": "\u2AD0",                                                                                               // 891
  "csupe;": "\u2AD2",                                                                                              // 892
  "ctdot;": "\u22EF",                                                                                              // 893
  "cudarrl;": "\u2938",                                                                                            // 894
  "cudarrr;": "\u2935",                                                                                            // 895
  "cuepr;": "\u22DE",                                                                                              // 896
  "cuesc;": "\u22DF",                                                                                              // 897
  "cularr;": "\u21B6",                                                                                             // 898
  "cularrp;": "\u293D",                                                                                            // 899
  "cup;": "\u222A",                                                                                                // 900
  "cupbrcap;": "\u2A48",                                                                                           // 901
  "cupcap;": "\u2A46",                                                                                             // 902
  "cupcup;": "\u2A4A",                                                                                             // 903
  "cupdot;": "\u228D",                                                                                             // 904
  "cupor;": "\u2A45",                                                                                              // 905
  "cups;": "\u222A\uFE00",                                                                                         // 906
  "curarr;": "\u21B7",                                                                                             // 907
  "curarrm;": "\u293C",                                                                                            // 908
  "curlyeqprec;": "\u22DE",                                                                                        // 909
  "curlyeqsucc;": "\u22DF",                                                                                        // 910
  "curlyvee;": "\u22CE",                                                                                           // 911
  "curlywedge;": "\u22CF",                                                                                         // 912
  "curren": "\u00A4",                                                                                              // 913
  "curren;": "\u00A4",                                                                                             // 914
  "curvearrowleft;": "\u21B6",                                                                                     // 915
  "curvearrowright;": "\u21B7",                                                                                    // 916
  "cuvee;": "\u22CE",                                                                                              // 917
  "cuwed;": "\u22CF",                                                                                              // 918
  "cwconint;": "\u2232",                                                                                           // 919
  "cwint;": "\u2231",                                                                                              // 920
  "cylcty;": "\u232D",                                                                                             // 921
  "dArr;": "\u21D3",                                                                                               // 922
  "dHar;": "\u2965",                                                                                               // 923
  "dagger;": "\u2020",                                                                                             // 924
  "daleth;": "\u2138",                                                                                             // 925
  "darr;": "\u2193",                                                                                               // 926
  "dash;": "\u2010",                                                                                               // 927
  "dashv;": "\u22A3",                                                                                              // 928
  "dbkarow;": "\u290F",                                                                                            // 929
  "dblac;": "\u02DD",                                                                                              // 930
  "dcaron;": "\u010F",                                                                                             // 931
  "dcy;": "\u0434",                                                                                                // 932
  "dd;": "\u2146",                                                                                                 // 933
  "ddagger;": "\u2021",                                                                                            // 934
  "ddarr;": "\u21CA",                                                                                              // 935
  "ddotseq;": "\u2A77",                                                                                            // 936
  "deg": "\u00B0",                                                                                                 // 937
  "deg;": "\u00B0",                                                                                                // 938
  "delta;": "\u03B4",                                                                                              // 939
  "demptyv;": "\u29B1",                                                                                            // 940
  "dfisht;": "\u297F",                                                                                             // 941
  "dfr;": "\u1D521",                                                                                               // 942
  "dharl;": "\u21C3",                                                                                              // 943
  "dharr;": "\u21C2",                                                                                              // 944
  "diam;": "\u22C4",                                                                                               // 945
  "diamond;": "\u22C4",                                                                                            // 946
  "diamondsuit;": "\u2666",                                                                                        // 947
  "diams;": "\u2666",                                                                                              // 948
  "die;": "\u00A8",                                                                                                // 949
  "digamma;": "\u03DD",                                                                                            // 950
  "disin;": "\u22F2",                                                                                              // 951
  "div;": "\u00F7",                                                                                                // 952
  "divide": "\u00F7",                                                                                              // 953
  "divide;": "\u00F7",                                                                                             // 954
  "divideontimes;": "\u22C7",                                                                                      // 955
  "divonx;": "\u22C7",                                                                                             // 956
  "djcy;": "\u0452",                                                                                               // 957
  "dlcorn;": "\u231E",                                                                                             // 958
  "dlcrop;": "\u230D",                                                                                             // 959
  "dollar;": "$",                                                                                                  // 960
  "dopf;": "\u1D555",                                                                                              // 961
  "dot;": "\u02D9",                                                                                                // 962
  "doteq;": "\u2250",                                                                                              // 963
  "doteqdot;": "\u2251",                                                                                           // 964
  "dotminus;": "\u2238",                                                                                           // 965
  "dotplus;": "\u2214",                                                                                            // 966
  "dotsquare;": "\u22A1",                                                                                          // 967
  "doublebarwedge;": "\u2306",                                                                                     // 968
  "downarrow;": "\u2193",                                                                                          // 969
  "downdownarrows;": "\u21CA",                                                                                     // 970
  "downharpoonleft;": "\u21C3",                                                                                    // 971
  "downharpoonright;": "\u21C2",                                                                                   // 972
  "drbkarow;": "\u2910",                                                                                           // 973
  "drcorn;": "\u231F",                                                                                             // 974
  "drcrop;": "\u230C",                                                                                             // 975
  "dscr;": "\u1D4B9",                                                                                              // 976
  "dscy;": "\u0455",                                                                                               // 977
  "dsol;": "\u29F6",                                                                                               // 978
  "dstrok;": "\u0111",                                                                                             // 979
  "dtdot;": "\u22F1",                                                                                              // 980
  "dtri;": "\u25BF",                                                                                               // 981
  "dtrif;": "\u25BE",                                                                                              // 982
  "duarr;": "\u21F5",                                                                                              // 983
  "duhar;": "\u296F",                                                                                              // 984
  "dwangle;": "\u29A6",                                                                                            // 985
  "dzcy;": "\u045F",                                                                                               // 986
  "dzigrarr;": "\u27FF",                                                                                           // 987
  "eDDot;": "\u2A77",                                                                                              // 988
  "eDot;": "\u2251",                                                                                               // 989
  "eacute": "\u00E9",                                                                                              // 990
  "eacute;": "\u00E9",                                                                                             // 991
  "easter;": "\u2A6E",                                                                                             // 992
  "ecaron;": "\u011B",                                                                                             // 993
  "ecir;": "\u2256",                                                                                               // 994
  "ecirc": "\u00EA",                                                                                               // 995
  "ecirc;": "\u00EA",                                                                                              // 996
  "ecolon;": "\u2255",                                                                                             // 997
  "ecy;": "\u044D",                                                                                                // 998
  "edot;": "\u0117",                                                                                               // 999
  "ee;": "\u2147",                                                                                                 // 1000
  "efDot;": "\u2252",                                                                                              // 1001
  "efr;": "\u1D522",                                                                                               // 1002
  "eg;": "\u2A9A",                                                                                                 // 1003
  "egrave": "\u00E8",                                                                                              // 1004
  "egrave;": "\u00E8",                                                                                             // 1005
  "egs;": "\u2A96",                                                                                                // 1006
  "egsdot;": "\u2A98",                                                                                             // 1007
  "el;": "\u2A99",                                                                                                 // 1008
  "elinters;": "\u23E7",                                                                                           // 1009
  "ell;": "\u2113",                                                                                                // 1010
  "els;": "\u2A95",                                                                                                // 1011
  "elsdot;": "\u2A97",                                                                                             // 1012
  "emacr;": "\u0113",                                                                                              // 1013
  "empty;": "\u2205",                                                                                              // 1014
  "emptyset;": "\u2205",                                                                                           // 1015
  "emptyv;": "\u2205",                                                                                             // 1016
  "emsp13;": "\u2004",                                                                                             // 1017
  "emsp14;": "\u2005",                                                                                             // 1018
  "emsp;": "\u2003",                                                                                               // 1019
  "eng;": "\u014B",                                                                                                // 1020
  "ensp;": "\u2002",                                                                                               // 1021
  "eogon;": "\u0119",                                                                                              // 1022
  "eopf;": "\u1D556",                                                                                              // 1023
  "epar;": "\u22D5",                                                                                               // 1024
  "eparsl;": "\u29E3",                                                                                             // 1025
  "eplus;": "\u2A71",                                                                                              // 1026
  "epsi;": "\u03B5",                                                                                               // 1027
  "epsilon;": "\u03B5",                                                                                            // 1028
  "epsiv;": "\u03F5",                                                                                              // 1029
  "eqcirc;": "\u2256",                                                                                             // 1030
  "eqcolon;": "\u2255",                                                                                            // 1031
  "eqsim;": "\u2242",                                                                                              // 1032
  "eqslantgtr;": "\u2A96",                                                                                         // 1033
  "eqslantless;": "\u2A95",                                                                                        // 1034
  "equals;": "=",                                                                                                  // 1035
  "equest;": "\u225F",                                                                                             // 1036
  "equiv;": "\u2261",                                                                                              // 1037
  "equivDD;": "\u2A78",                                                                                            // 1038
  "eqvparsl;": "\u29E5",                                                                                           // 1039
  "erDot;": "\u2253",                                                                                              // 1040
  "erarr;": "\u2971",                                                                                              // 1041
  "escr;": "\u212F",                                                                                               // 1042
  "esdot;": "\u2250",                                                                                              // 1043
  "esim;": "\u2242",                                                                                               // 1044
  "eta;": "\u03B7",                                                                                                // 1045
  "eth": "\u00F0",                                                                                                 // 1046
  "eth;": "\u00F0",                                                                                                // 1047
  "euml": "\u00EB",                                                                                                // 1048
  "euml;": "\u00EB",                                                                                               // 1049
  "euro;": "\u20AC",                                                                                               // 1050
  "excl;": "!",                                                                                                    // 1051
  "exist;": "\u2203",                                                                                              // 1052
  "expectation;": "\u2130",                                                                                        // 1053
  "exponentiale;": "\u2147",                                                                                       // 1054
  "fallingdotseq;": "\u2252",                                                                                      // 1055
  "fcy;": "\u0444",                                                                                                // 1056
  "female;": "\u2640",                                                                                             // 1057
  "ffilig;": "\uFB03",                                                                                             // 1058
  "fflig;": "\uFB00",                                                                                              // 1059
  "ffllig;": "\uFB04",                                                                                             // 1060
  "ffr;": "\u1D523",                                                                                               // 1061
  "filig;": "\uFB01",                                                                                              // 1062
  "fjlig;": "\u0066",                                                                                              // 1063
  "flat;": "\u266D",                                                                                               // 1064
  "fllig;": "\uFB02",                                                                                              // 1065
  "fltns;": "\u25B1",                                                                                              // 1066
  "fnof;": "\u0192",                                                                                               // 1067
  "fopf;": "\u1D557",                                                                                              // 1068
  "forall;": "\u2200",                                                                                             // 1069
  "fork;": "\u22D4",                                                                                               // 1070
  "forkv;": "\u2AD9",                                                                                              // 1071
  "fpartint;": "\u2A0D",                                                                                           // 1072
  "frac12": "\u00BD",                                                                                              // 1073
  "frac12;": "\u00BD",                                                                                             // 1074
  "frac13;": "\u2153",                                                                                             // 1075
  "frac14": "\u00BC",                                                                                              // 1076
  "frac14;": "\u00BC",                                                                                             // 1077
  "frac15;": "\u2155",                                                                                             // 1078
  "frac16;": "\u2159",                                                                                             // 1079
  "frac18;": "\u215B",                                                                                             // 1080
  "frac23;": "\u2154",                                                                                             // 1081
  "frac25;": "\u2156",                                                                                             // 1082
  "frac34": "\u00BE",                                                                                              // 1083
  "frac34;": "\u00BE",                                                                                             // 1084
  "frac35;": "\u2157",                                                                                             // 1085
  "frac38;": "\u215C",                                                                                             // 1086
  "frac45;": "\u2158",                                                                                             // 1087
  "frac56;": "\u215A",                                                                                             // 1088
  "frac58;": "\u215D",                                                                                             // 1089
  "frac78;": "\u215E",                                                                                             // 1090
  "frasl;": "\u2044",                                                                                              // 1091
  "frown;": "\u2322",                                                                                              // 1092
  "fscr;": "\u1D4BB",                                                                                              // 1093
  "gE;": "\u2267",                                                                                                 // 1094
  "gEl;": "\u2A8C",                                                                                                // 1095
  "gacute;": "\u01F5",                                                                                             // 1096
  "gamma;": "\u03B3",                                                                                              // 1097
  "gammad;": "\u03DD",                                                                                             // 1098
  "gap;": "\u2A86",                                                                                                // 1099
  "gbreve;": "\u011F",                                                                                             // 1100
  "gcirc;": "\u011D",                                                                                              // 1101
  "gcy;": "\u0433",                                                                                                // 1102
  "gdot;": "\u0121",                                                                                               // 1103
  "ge;": "\u2265",                                                                                                 // 1104
  "gel;": "\u22DB",                                                                                                // 1105
  "geq;": "\u2265",                                                                                                // 1106
  "geqq;": "\u2267",                                                                                               // 1107
  "geqslant;": "\u2A7E",                                                                                           // 1108
  "ges;": "\u2A7E",                                                                                                // 1109
  "gescc;": "\u2AA9",                                                                                              // 1110
  "gesdot;": "\u2A80",                                                                                             // 1111
  "gesdoto;": "\u2A82",                                                                                            // 1112
  "gesdotol;": "\u2A84",                                                                                           // 1113
  "gesl;": "\u22DB\uFE00",                                                                                         // 1114
  "gesles;": "\u2A94",                                                                                             // 1115
  "gfr;": "\u1D524",                                                                                               // 1116
  "gg;": "\u226B",                                                                                                 // 1117
  "ggg;": "\u22D9",                                                                                                // 1118
  "gimel;": "\u2137",                                                                                              // 1119
  "gjcy;": "\u0453",                                                                                               // 1120
  "gl;": "\u2277",                                                                                                 // 1121
  "glE;": "\u2A92",                                                                                                // 1122
  "gla;": "\u2AA5",                                                                                                // 1123
  "glj;": "\u2AA4",                                                                                                // 1124
  "gnE;": "\u2269",                                                                                                // 1125
  "gnap;": "\u2A8A",                                                                                               // 1126
  "gnapprox;": "\u2A8A",                                                                                           // 1127
  "gne;": "\u2A88",                                                                                                // 1128
  "gneq;": "\u2A88",                                                                                               // 1129
  "gneqq;": "\u2269",                                                                                              // 1130
  "gnsim;": "\u22E7",                                                                                              // 1131
  "gopf;": "\u1D558",                                                                                              // 1132
  "grave;": "`",                                                                                                   // 1133
  "gscr;": "\u210A",                                                                                               // 1134
  "gsim;": "\u2273",                                                                                               // 1135
  "gsime;": "\u2A8E",                                                                                              // 1136
  "gsiml;": "\u2A90",                                                                                              // 1137
  "gt": ">",                                                                                                       // 1138
  "gt;": ">",                                                                                                      // 1139
  "gtcc;": "\u2AA7",                                                                                               // 1140
  "gtcir;": "\u2A7A",                                                                                              // 1141
  "gtdot;": "\u22D7",                                                                                              // 1142
  "gtlPar;": "\u2995",                                                                                             // 1143
  "gtquest;": "\u2A7C",                                                                                            // 1144
  "gtrapprox;": "\u2A86",                                                                                          // 1145
  "gtrarr;": "\u2978",                                                                                             // 1146
  "gtrdot;": "\u22D7",                                                                                             // 1147
  "gtreqless;": "\u22DB",                                                                                          // 1148
  "gtreqqless;": "\u2A8C",                                                                                         // 1149
  "gtrless;": "\u2277",                                                                                            // 1150
  "gtrsim;": "\u2273",                                                                                             // 1151
  "gvertneqq;": "\u2269\uFE00",                                                                                    // 1152
  "gvnE;": "\u2269\uFE00",                                                                                         // 1153
  "hArr;": "\u21D4",                                                                                               // 1154
  "hairsp;": "\u200A",                                                                                             // 1155
  "half;": "\u00BD",                                                                                               // 1156
  "hamilt;": "\u210B",                                                                                             // 1157
  "hardcy;": "\u044A",                                                                                             // 1158
  "harr;": "\u2194",                                                                                               // 1159
  "harrcir;": "\u2948",                                                                                            // 1160
  "harrw;": "\u21AD",                                                                                              // 1161
  "hbar;": "\u210F",                                                                                               // 1162
  "hcirc;": "\u0125",                                                                                              // 1163
  "hearts;": "\u2665",                                                                                             // 1164
  "heartsuit;": "\u2665",                                                                                          // 1165
  "hellip;": "\u2026",                                                                                             // 1166
  "hercon;": "\u22B9",                                                                                             // 1167
  "hfr;": "\u1D525",                                                                                               // 1168
  "hksearow;": "\u2925",                                                                                           // 1169
  "hkswarow;": "\u2926",                                                                                           // 1170
  "hoarr;": "\u21FF",                                                                                              // 1171
  "homtht;": "\u223B",                                                                                             // 1172
  "hookleftarrow;": "\u21A9",                                                                                      // 1173
  "hookrightarrow;": "\u21AA",                                                                                     // 1174
  "hopf;": "\u1D559",                                                                                              // 1175
  "horbar;": "\u2015",                                                                                             // 1176
  "hscr;": "\u1D4BD",                                                                                              // 1177
  "hslash;": "\u210F",                                                                                             // 1178
  "hstrok;": "\u0127",                                                                                             // 1179
  "hybull;": "\u2043",                                                                                             // 1180
  "hyphen;": "\u2010",                                                                                             // 1181
  "iacute": "\u00ED",                                                                                              // 1182
  "iacute;": "\u00ED",                                                                                             // 1183
  "ic;": "\u2063",                                                                                                 // 1184
  "icirc": "\u00EE",                                                                                               // 1185
  "icirc;": "\u00EE",                                                                                              // 1186
  "icy;": "\u0438",                                                                                                // 1187
  "iecy;": "\u0435",                                                                                               // 1188
  "iexcl": "\u00A1",                                                                                               // 1189
  "iexcl;": "\u00A1",                                                                                              // 1190
  "iff;": "\u21D4",                                                                                                // 1191
  "ifr;": "\u1D526",                                                                                               // 1192
  "igrave": "\u00EC",                                                                                              // 1193
  "igrave;": "\u00EC",                                                                                             // 1194
  "ii;": "\u2148",                                                                                                 // 1195
  "iiiint;": "\u2A0C",                                                                                             // 1196
  "iiint;": "\u222D",                                                                                              // 1197
  "iinfin;": "\u29DC",                                                                                             // 1198
  "iiota;": "\u2129",                                                                                              // 1199
  "ijlig;": "\u0133",                                                                                              // 1200
  "imacr;": "\u012B",                                                                                              // 1201
  "image;": "\u2111",                                                                                              // 1202
  "imagline;": "\u2110",                                                                                           // 1203
  "imagpart;": "\u2111",                                                                                           // 1204
  "imath;": "\u0131",                                                                                              // 1205
  "imof;": "\u22B7",                                                                                               // 1206
  "imped;": "\u01B5",                                                                                              // 1207
  "in;": "\u2208",                                                                                                 // 1208
  "incare;": "\u2105",                                                                                             // 1209
  "infin;": "\u221E",                                                                                              // 1210
  "infintie;": "\u29DD",                                                                                           // 1211
  "inodot;": "\u0131",                                                                                             // 1212
  "int;": "\u222B",                                                                                                // 1213
  "intcal;": "\u22BA",                                                                                             // 1214
  "integers;": "\u2124",                                                                                           // 1215
  "intercal;": "\u22BA",                                                                                           // 1216
  "intlarhk;": "\u2A17",                                                                                           // 1217
  "intprod;": "\u2A3C",                                                                                            // 1218
  "iocy;": "\u0451",                                                                                               // 1219
  "iogon;": "\u012F",                                                                                              // 1220
  "iopf;": "\u1D55A",                                                                                              // 1221
  "iota;": "\u03B9",                                                                                               // 1222
  "iprod;": "\u2A3C",                                                                                              // 1223
  "iquest": "\u00BF",                                                                                              // 1224
  "iquest;": "\u00BF",                                                                                             // 1225
  "iscr;": "\u1D4BE",                                                                                              // 1226
  "isin;": "\u2208",                                                                                               // 1227
  "isinE;": "\u22F9",                                                                                              // 1228
  "isindot;": "\u22F5",                                                                                            // 1229
  "isins;": "\u22F4",                                                                                              // 1230
  "isinsv;": "\u22F3",                                                                                             // 1231
  "isinv;": "\u2208",                                                                                              // 1232
  "it;": "\u2062",                                                                                                 // 1233
  "itilde;": "\u0129",                                                                                             // 1234
  "iukcy;": "\u0456",                                                                                              // 1235
  "iuml": "\u00EF",                                                                                                // 1236
  "iuml;": "\u00EF",                                                                                               // 1237
  "jcirc;": "\u0135",                                                                                              // 1238
  "jcy;": "\u0439",                                                                                                // 1239
  "jfr;": "\u1D527",                                                                                               // 1240
  "jmath;": "\u0237",                                                                                              // 1241
  "jopf;": "\u1D55B",                                                                                              // 1242
  "jscr;": "\u1D4BF",                                                                                              // 1243
  "jsercy;": "\u0458",                                                                                             // 1244
  "jukcy;": "\u0454",                                                                                              // 1245
  "kappa;": "\u03BA",                                                                                              // 1246
  "kappav;": "\u03F0",                                                                                             // 1247
  "kcedil;": "\u0137",                                                                                             // 1248
  "kcy;": "\u043A",                                                                                                // 1249
  "kfr;": "\u1D528",                                                                                               // 1250
  "kgreen;": "\u0138",                                                                                             // 1251
  "khcy;": "\u0445",                                                                                               // 1252
  "kjcy;": "\u045C",                                                                                               // 1253
  "kopf;": "\u1D55C",                                                                                              // 1254
  "kscr;": "\u1D4C0",                                                                                              // 1255
  "lAarr;": "\u21DA",                                                                                              // 1256
  "lArr;": "\u21D0",                                                                                               // 1257
  "lAtail;": "\u291B",                                                                                             // 1258
  "lBarr;": "\u290E",                                                                                              // 1259
  "lE;": "\u2266",                                                                                                 // 1260
  "lEg;": "\u2A8B",                                                                                                // 1261
  "lHar;": "\u2962",                                                                                               // 1262
  "lacute;": "\u013A",                                                                                             // 1263
  "laemptyv;": "\u29B4",                                                                                           // 1264
  "lagran;": "\u2112",                                                                                             // 1265
  "lambda;": "\u03BB",                                                                                             // 1266
  "lang;": "\u27E8",                                                                                               // 1267
  "langd;": "\u2991",                                                                                              // 1268
  "langle;": "\u27E8",                                                                                             // 1269
  "lap;": "\u2A85",                                                                                                // 1270
  "laquo": "\u00AB",                                                                                               // 1271
  "laquo;": "\u00AB",                                                                                              // 1272
  "larr;": "\u2190",                                                                                               // 1273
  "larrb;": "\u21E4",                                                                                              // 1274
  "larrbfs;": "\u291F",                                                                                            // 1275
  "larrfs;": "\u291D",                                                                                             // 1276
  "larrhk;": "\u21A9",                                                                                             // 1277
  "larrlp;": "\u21AB",                                                                                             // 1278
  "larrpl;": "\u2939",                                                                                             // 1279
  "larrsim;": "\u2973",                                                                                            // 1280
  "larrtl;": "\u21A2",                                                                                             // 1281
  "lat;": "\u2AAB",                                                                                                // 1282
  "latail;": "\u2919",                                                                                             // 1283
  "late;": "\u2AAD",                                                                                               // 1284
  "lates;": "\u2AAD\uFE00",                                                                                        // 1285
  "lbarr;": "\u290C",                                                                                              // 1286
  "lbbrk;": "\u2772",                                                                                              // 1287
  "lbrace;": "{",                                                                                                  // 1288
  "lbrack;": "[",                                                                                                  // 1289
  "lbrke;": "\u298B",                                                                                              // 1290
  "lbrksld;": "\u298F",                                                                                            // 1291
  "lbrkslu;": "\u298D",                                                                                            // 1292
  "lcaron;": "\u013E",                                                                                             // 1293
  "lcedil;": "\u013C",                                                                                             // 1294
  "lceil;": "\u2308",                                                                                              // 1295
  "lcub;": "{",                                                                                                    // 1296
  "lcy;": "\u043B",                                                                                                // 1297
  "ldca;": "\u2936",                                                                                               // 1298
  "ldquo;": "\u201C",                                                                                              // 1299
  "ldquor;": "\u201E",                                                                                             // 1300
  "ldrdhar;": "\u2967",                                                                                            // 1301
  "ldrushar;": "\u294B",                                                                                           // 1302
  "ldsh;": "\u21B2",                                                                                               // 1303
  "le;": "\u2264",                                                                                                 // 1304
  "leftarrow;": "\u2190",                                                                                          // 1305
  "leftarrowtail;": "\u21A2",                                                                                      // 1306
  "leftharpoondown;": "\u21BD",                                                                                    // 1307
  "leftharpoonup;": "\u21BC",                                                                                      // 1308
  "leftleftarrows;": "\u21C7",                                                                                     // 1309
  "leftrightarrow;": "\u2194",                                                                                     // 1310
  "leftrightarrows;": "\u21C6",                                                                                    // 1311
  "leftrightharpoons;": "\u21CB",                                                                                  // 1312
  "leftrightsquigarrow;": "\u21AD",                                                                                // 1313
  "leftthreetimes;": "\u22CB",                                                                                     // 1314
  "leg;": "\u22DA",                                                                                                // 1315
  "leq;": "\u2264",                                                                                                // 1316
  "leqq;": "\u2266",                                                                                               // 1317
  "leqslant;": "\u2A7D",                                                                                           // 1318
  "les;": "\u2A7D",                                                                                                // 1319
  "lescc;": "\u2AA8",                                                                                              // 1320
  "lesdot;": "\u2A7F",                                                                                             // 1321
  "lesdoto;": "\u2A81",                                                                                            // 1322
  "lesdotor;": "\u2A83",                                                                                           // 1323
  "lesg;": "\u22DA\uFE00",                                                                                         // 1324
  "lesges;": "\u2A93",                                                                                             // 1325
  "lessapprox;": "\u2A85",                                                                                         // 1326
  "lessdot;": "\u22D6",                                                                                            // 1327
  "lesseqgtr;": "\u22DA",                                                                                          // 1328
  "lesseqqgtr;": "\u2A8B",                                                                                         // 1329
  "lessgtr;": "\u2276",                                                                                            // 1330
  "lesssim;": "\u2272",                                                                                            // 1331
  "lfisht;": "\u297C",                                                                                             // 1332
  "lfloor;": "\u230A",                                                                                             // 1333
  "lfr;": "\u1D529",                                                                                               // 1334
  "lg;": "\u2276",                                                                                                 // 1335
  "lgE;": "\u2A91",                                                                                                // 1336
  "lhard;": "\u21BD",                                                                                              // 1337
  "lharu;": "\u21BC",                                                                                              // 1338
  "lharul;": "\u296A",                                                                                             // 1339
  "lhblk;": "\u2584",                                                                                              // 1340
  "ljcy;": "\u0459",                                                                                               // 1341
  "ll;": "\u226A",                                                                                                 // 1342
  "llarr;": "\u21C7",                                                                                              // 1343
  "llcorner;": "\u231E",                                                                                           // 1344
  "llhard;": "\u296B",                                                                                             // 1345
  "lltri;": "\u25FA",                                                                                              // 1346
  "lmidot;": "\u0140",                                                                                             // 1347
  "lmoust;": "\u23B0",                                                                                             // 1348
  "lmoustache;": "\u23B0",                                                                                         // 1349
  "lnE;": "\u2268",                                                                                                // 1350
  "lnap;": "\u2A89",                                                                                               // 1351
  "lnapprox;": "\u2A89",                                                                                           // 1352
  "lne;": "\u2A87",                                                                                                // 1353
  "lneq;": "\u2A87",                                                                                               // 1354
  "lneqq;": "\u2268",                                                                                              // 1355
  "lnsim;": "\u22E6",                                                                                              // 1356
  "loang;": "\u27EC",                                                                                              // 1357
  "loarr;": "\u21FD",                                                                                              // 1358
  "lobrk;": "\u27E6",                                                                                              // 1359
  "longleftarrow;": "\u27F5",                                                                                      // 1360
  "longleftrightarrow;": "\u27F7",                                                                                 // 1361
  "longmapsto;": "\u27FC",                                                                                         // 1362
  "longrightarrow;": "\u27F6",                                                                                     // 1363
  "looparrowleft;": "\u21AB",                                                                                      // 1364
  "looparrowright;": "\u21AC",                                                                                     // 1365
  "lopar;": "\u2985",                                                                                              // 1366
  "lopf;": "\u1D55D",                                                                                              // 1367
  "loplus;": "\u2A2D",                                                                                             // 1368
  "lotimes;": "\u2A34",                                                                                            // 1369
  "lowast;": "\u2217",                                                                                             // 1370
  "lowbar;": "_",                                                                                                  // 1371
  "loz;": "\u25CA",                                                                                                // 1372
  "lozenge;": "\u25CA",                                                                                            // 1373
  "lozf;": "\u29EB",                                                                                               // 1374
  "lpar;": "(",                                                                                                    // 1375
  "lparlt;": "\u2993",                                                                                             // 1376
  "lrarr;": "\u21C6",                                                                                              // 1377
  "lrcorner;": "\u231F",                                                                                           // 1378
  "lrhar;": "\u21CB",                                                                                              // 1379
  "lrhard;": "\u296D",                                                                                             // 1380
  "lrm;": "\u200E",                                                                                                // 1381
  "lrtri;": "\u22BF",                                                                                              // 1382
  "lsaquo;": "\u2039",                                                                                             // 1383
  "lscr;": "\u1D4C1",                                                                                              // 1384
  "lsh;": "\u21B0",                                                                                                // 1385
  "lsim;": "\u2272",                                                                                               // 1386
  "lsime;": "\u2A8D",                                                                                              // 1387
  "lsimg;": "\u2A8F",                                                                                              // 1388
  "lsqb;": "[",                                                                                                    // 1389
  "lsquo;": "\u2018",                                                                                              // 1390
  "lsquor;": "\u201A",                                                                                             // 1391
  "lstrok;": "\u0142",                                                                                             // 1392
  "lt": "<",                                                                                                       // 1393
  "lt;": "<",                                                                                                      // 1394
  "ltcc;": "\u2AA6",                                                                                               // 1395
  "ltcir;": "\u2A79",                                                                                              // 1396
  "ltdot;": "\u22D6",                                                                                              // 1397
  "lthree;": "\u22CB",                                                                                             // 1398
  "ltimes;": "\u22C9",                                                                                             // 1399
  "ltlarr;": "\u2976",                                                                                             // 1400
  "ltquest;": "\u2A7B",                                                                                            // 1401
  "ltrPar;": "\u2996",                                                                                             // 1402
  "ltri;": "\u25C3",                                                                                               // 1403
  "ltrie;": "\u22B4",                                                                                              // 1404
  "ltrif;": "\u25C2",                                                                                              // 1405
  "lurdshar;": "\u294A",                                                                                           // 1406
  "luruhar;": "\u2966",                                                                                            // 1407
  "lvertneqq;": "\u2268\uFE00",                                                                                    // 1408
  "lvnE;": "\u2268\uFE00",                                                                                         // 1409
  "mDDot;": "\u223A",                                                                                              // 1410
  "macr": "\u00AF",                                                                                                // 1411
  "macr;": "\u00AF",                                                                                               // 1412
  "male;": "\u2642",                                                                                               // 1413
  "malt;": "\u2720",                                                                                               // 1414
  "maltese;": "\u2720",                                                                                            // 1415
  "map;": "\u21A6",                                                                                                // 1416
  "mapsto;": "\u21A6",                                                                                             // 1417
  "mapstodown;": "\u21A7",                                                                                         // 1418
  "mapstoleft;": "\u21A4",                                                                                         // 1419
  "mapstoup;": "\u21A5",                                                                                           // 1420
  "marker;": "\u25AE",                                                                                             // 1421
  "mcomma;": "\u2A29",                                                                                             // 1422
  "mcy;": "\u043C",                                                                                                // 1423
  "mdash;": "\u2014",                                                                                              // 1424
  "measuredangle;": "\u2221",                                                                                      // 1425
  "mfr;": "\u1D52A",                                                                                               // 1426
  "mho;": "\u2127",                                                                                                // 1427
  "micro": "\u00B5",                                                                                               // 1428
  "micro;": "\u00B5",                                                                                              // 1429
  "mid;": "\u2223",                                                                                                // 1430
  "midast;": "*",                                                                                                  // 1431
  "midcir;": "\u2AF0",                                                                                             // 1432
  "middot": "\u00B7",                                                                                              // 1433
  "middot;": "\u00B7",                                                                                             // 1434
  "minus;": "\u2212",                                                                                              // 1435
  "minusb;": "\u229F",                                                                                             // 1436
  "minusd;": "\u2238",                                                                                             // 1437
  "minusdu;": "\u2A2A",                                                                                            // 1438
  "mlcp;": "\u2ADB",                                                                                               // 1439
  "mldr;": "\u2026",                                                                                               // 1440
  "mnplus;": "\u2213",                                                                                             // 1441
  "models;": "\u22A7",                                                                                             // 1442
  "mopf;": "\u1D55E",                                                                                              // 1443
  "mp;": "\u2213",                                                                                                 // 1444
  "mscr;": "\u1D4C2",                                                                                              // 1445
  "mstpos;": "\u223E",                                                                                             // 1446
  "mu;": "\u03BC",                                                                                                 // 1447
  "multimap;": "\u22B8",                                                                                           // 1448
  "mumap;": "\u22B8",                                                                                              // 1449
  "nGg;": "\u22D9\u0338",                                                                                          // 1450
  "nGt;": "\u226B\u20D2",                                                                                          // 1451
  "nGtv;": "\u226B\u0338",                                                                                         // 1452
  "nLeftarrow;": "\u21CD",                                                                                         // 1453
  "nLeftrightarrow;": "\u21CE",                                                                                    // 1454
  "nLl;": "\u22D8\u0338",                                                                                          // 1455
  "nLt;": "\u226A\u20D2",                                                                                          // 1456
  "nLtv;": "\u226A\u0338",                                                                                         // 1457
  "nRightarrow;": "\u21CF",                                                                                        // 1458
  "nVDash;": "\u22AF",                                                                                             // 1459
  "nVdash;": "\u22AE",                                                                                             // 1460
  "nabla;": "\u2207",                                                                                              // 1461
  "nacute;": "\u0144",                                                                                             // 1462
  "nang;": "\u2220\u20D2",                                                                                         // 1463
  "nap;": "\u2249",                                                                                                // 1464
  "napE;": "\u2A70\u0338",                                                                                         // 1465
  "napid;": "\u224B\u0338",                                                                                        // 1466
  "napos;": "\u0149",                                                                                              // 1467
  "napprox;": "\u2249",                                                                                            // 1468
  "natur;": "\u266E",                                                                                              // 1469
  "natural;": "\u266E",                                                                                            // 1470
  "naturals;": "\u2115",                                                                                           // 1471
  "nbsp": "\u00A0",                                                                                                // 1472
  "nbsp;": "\u00A0",                                                                                               // 1473
  "nbump;": "\u224E\u0338",                                                                                        // 1474
  "nbumpe;": "\u224F\u0338",                                                                                       // 1475
  "ncap;": "\u2A43",                                                                                               // 1476
  "ncaron;": "\u0148",                                                                                             // 1477
  "ncedil;": "\u0146",                                                                                             // 1478
  "ncong;": "\u2247\u0338",                                                                                        // 1479
  "ncongdot;": "\u2A6D",                                                                                           // 1480
  "ncup;": "\u2A42",                                                                                               // 1481
  "ncy;": "\u043D",                                                                                                // 1482
  "ndash;": "\u2013",                                                                                              // 1483
  "ne;": "\u2260",                                                                                                 // 1484
  "neArr;": "\u21D7",                                                                                              // 1485
  "nearhk;": "\u2924",                                                                                             // 1486
  "nearr;": "\u2197",                                                                                              // 1487
  "nearrow;": "\u2197",                                                                                            // 1488
  "nedot;": "\u2250\u0338",                                                                                        // 1489
  "nequiv;": "\u2262",                                                                                             // 1490
  "nesear;": "\u2928",                                                                                             // 1491
  "nesim;": "\u2242\u0338",                                                                                        // 1492
  "nexist;": "\u2204",                                                                                             // 1493
  "nexists;": "\u2204",                                                                                            // 1494
  "nfr;": "\u1D52B",                                                                                               // 1495
  "ngE;": "\u2267\u0338",                                                                                          // 1496
  "nge;": "\u2271",                                                                                                // 1497
  "ngeq;": "\u2271",                                                                                               // 1498
  "ngeqq;": "\u2267\u0338",                                                                                        // 1499
  "ngeqslant;": "\u2A7E\u0338",                                                                                    // 1500
  "nges;": "\u2A7E\u0338",                                                                                         // 1501
  "ngsim;": "\u2275",                                                                                              // 1502
  "ngt;": "\u226F",                                                                                                // 1503
  "ngtr;": "\u226F",                                                                                               // 1504
  "nhArr;": "\u21CE",                                                                                              // 1505
  "nharr;": "\u21AE",                                                                                              // 1506
  "nhpar;": "\u2AF2",                                                                                              // 1507
  "ni;": "\u220B",                                                                                                 // 1508
  "nis;": "\u22FC",                                                                                                // 1509
  "nisd;": "\u22FA",                                                                                               // 1510
  "niv;": "\u220B",                                                                                                // 1511
  "njcy;": "\u045A",                                                                                               // 1512
  "nlArr;": "\u21CD",                                                                                              // 1513
  "nlE;": "\u2266\u0338",                                                                                          // 1514
  "nlarr;": "\u219A",                                                                                              // 1515
  "nldr;": "\u2025",                                                                                               // 1516
  "nle;": "\u2270",                                                                                                // 1517
  "nleftarrow;": "\u219A",                                                                                         // 1518
  "nleftrightarrow;": "\u21AE",                                                                                    // 1519
  "nleq;": "\u2270",                                                                                               // 1520
  "nleqq;": "\u2266\u0338",                                                                                        // 1521
  "nleqslant;": "\u2A7D\u0338",                                                                                    // 1522
  "nles;": "\u2A7D\u0338",                                                                                         // 1523
  "nless;": "\u226E",                                                                                              // 1524
  "nlsim;": "\u2274",                                                                                              // 1525
  "nlt;": "\u226E",                                                                                                // 1526
  "nltri;": "\u22EA",                                                                                              // 1527
  "nltrie;": "\u22EC",                                                                                             // 1528
  "nmid;": "\u2224",                                                                                               // 1529
  "nopf;": "\u1D55F",                                                                                              // 1530
  "not": "\u00AC",                                                                                                 // 1531
  "not;": "\u00AC",                                                                                                // 1532
  "notin;": "\u2209",                                                                                              // 1533
  "notinE;": "\u22F9\u0338",                                                                                       // 1534
  "notindot;": "\u22F5\u0338",                                                                                     // 1535
  "notinva;": "\u2209",                                                                                            // 1536
  "notinvb;": "\u22F7",                                                                                            // 1537
  "notinvc;": "\u22F6",                                                                                            // 1538
  "notni;": "\u220C",                                                                                              // 1539
  "notniva;": "\u220C",                                                                                            // 1540
  "notnivb;": "\u22FE",                                                                                            // 1541
  "notnivc;": "\u22FD",                                                                                            // 1542
  "npar;": "\u2226",                                                                                               // 1543
  "nparallel;": "\u2226",                                                                                          // 1544
  "nparsl;": "\u2AFD\u20E5",                                                                                       // 1545
  "npart;": "\u2202\u0338",                                                                                        // 1546
  "npolint;": "\u2A14",                                                                                            // 1547
  "npr;": "\u2280",                                                                                                // 1548
  "nprcue;": "\u22E0",                                                                                             // 1549
  "npre;": "\u2AAF",                                                                                               // 1550
  "nprec;": "\u2280",                                                                                              // 1551
  "npreceq;": "\u2AAF",                                                                                            // 1552
  "nrArr;": "\u21CF",                                                                                              // 1553
  "nrarr;": "\u219B",                                                                                              // 1554
  "nrarrc;": "\u2933\u0338",                                                                                       // 1555
  "nrarrw;": "\u219D\u0338",                                                                                       // 1556
  "nrightarrow;": "\u219B",                                                                                        // 1557
  "nrtri;": "\u22EB",                                                                                              // 1558
  "nrtrie;": "\u22ED",                                                                                             // 1559
  "nsc;": "\u2281",                                                                                                // 1560
  "nsccue;": "\u22E1",                                                                                             // 1561
  "nsce;": "\u2AB0\u0338",                                                                                         // 1562
  "nscr;": "\u1D4C3",                                                                                              // 1563
  "nshortmid;": "\u2224",                                                                                          // 1564
  "nshortparallel;": "\u2226",                                                                                     // 1565
  "nsim;": "\u2241",                                                                                               // 1566
  "nsime;": "\u2244",                                                                                              // 1567
  "nsimeq;": "\u2244",                                                                                             // 1568
  "nsmid;": "\u2224",                                                                                              // 1569
  "nspar;": "\u2226",                                                                                              // 1570
  "nsqsube;": "\u22E2",                                                                                            // 1571
  "nsqsupe;": "\u22E3",                                                                                            // 1572
  "nsub;": "\u2284",                                                                                               // 1573
  "nsubE;": "\u2AC5\u0338",                                                                                        // 1574
  "nsube;": "\u2288",                                                                                              // 1575
  "nsubset;": "\u2282\u0338",                                                                                      // 1576
  "nsubseteq;": "\u2288",                                                                                          // 1577
  "nsubseteqq;": "\u2AC5\u0338",                                                                                   // 1578
  "nsucc;": "\u2281",                                                                                              // 1579
  "nsucceq;": "\u2AB0\u0338",                                                                                      // 1580
  "nsup;": "\u2285",                                                                                               // 1581
  "nsupE;": "\u2AC6",                                                                                              // 1582
  "nsupe;": "\u2289",                                                                                              // 1583
  "nsupset;": "\u2283\u0338",                                                                                      // 1584
  "nsupseteq;": "\u2289",                                                                                          // 1585
  "nsupseteqq;": "\u2AC6\u0338",                                                                                   // 1586
  "ntgl;": "\u2279",                                                                                               // 1587
  "ntilde": "\u00F1",                                                                                              // 1588
  "ntilde;": "\u00F1",                                                                                             // 1589
  "ntlg;": "\u2278",                                                                                               // 1590
  "ntriangleleft;": "\u22EA",                                                                                      // 1591
  "ntrianglelefteq;": "\u22EC",                                                                                    // 1592
  "ntriangleright;": "\u22EB",                                                                                     // 1593
  "ntrianglerighteq;": "\u22ED",                                                                                   // 1594
  "nu;": "\u03BD",                                                                                                 // 1595
  "num;": "#",                                                                                                     // 1596
  "numero;": "\u2116",                                                                                             // 1597
  "numsp;": "\u2007",                                                                                              // 1598
  "nvDash;": "\u22AD",                                                                                             // 1599
  "nvHarr;": "\u2904",                                                                                             // 1600
  "nvap;": "\u224D\u20D2",                                                                                         // 1601
  "nvdash;": "\u22AC",                                                                                             // 1602
  "nvge;": "\u2265\u20D2",                                                                                         // 1603
  "nvgt;": "\u003E\u20D2",                                                                                         // 1604
  "nvinfin;": "\u29DE",                                                                                            // 1605
  "nvlArr;": "\u2902",                                                                                             // 1606
  "nvle;": "\u2264\u20D2",                                                                                         // 1607
  "nvlt;": "\u003C\u20D2",                                                                                         // 1608
  "nvltrie;": "\u22B4\u20D2",                                                                                      // 1609
  "nvrArr;": "\u2903",                                                                                             // 1610
  "nvrtrie;": "\u22B5\u20D2",                                                                                      // 1611
  "nvsim;": "\u223C\u20D2",                                                                                        // 1612
  "nwArr;": "\u21D6",                                                                                              // 1613
  "nwarhk;": "\u2923",                                                                                             // 1614
  "nwarr;": "\u2196",                                                                                              // 1615
  "nwarrow;": "\u2196",                                                                                            // 1616
  "nwnear;": "\u2927",                                                                                             // 1617
  "oS;": "\u24C8",                                                                                                 // 1618
  "oacute": "\u00F3",                                                                                              // 1619
  "oacute;": "\u00F3",                                                                                             // 1620
  "oast;": "\u229B",                                                                                               // 1621
  "ocir;": "\u229A",                                                                                               // 1622
  "ocirc": "\u00F4",                                                                                               // 1623
  "ocirc;": "\u00F4",                                                                                              // 1624
  "ocy;": "\u043E",                                                                                                // 1625
  "odash;": "\u229D",                                                                                              // 1626
  "odblac;": "\u0151",                                                                                             // 1627
  "odiv;": "\u2A38",                                                                                               // 1628
  "odot;": "\u2299",                                                                                               // 1629
  "odsold;": "\u29BC",                                                                                             // 1630
  "oelig;": "\u0153",                                                                                              // 1631
  "ofcir;": "\u29BF",                                                                                              // 1632
  "ofr;": "\u1D52C",                                                                                               // 1633
  "ogon;": "\u02DB",                                                                                               // 1634
  "ograve": "\u00F2",                                                                                              // 1635
  "ograve;": "\u00F2",                                                                                             // 1636
  "ogt;": "\u29C1",                                                                                                // 1637
  "ohbar;": "\u29B5",                                                                                              // 1638
  "ohm;": "\u03A9",                                                                                                // 1639
  "oint;": "\u222E",                                                                                               // 1640
  "olarr;": "\u21BA",                                                                                              // 1641
  "olcir;": "\u29BE",                                                                                              // 1642
  "olcross;": "\u29BB",                                                                                            // 1643
  "oline;": "\u203E",                                                                                              // 1644
  "olt;": "\u29C0",                                                                                                // 1645
  "omacr;": "\u014D",                                                                                              // 1646
  "omega;": "\u03C9",                                                                                              // 1647
  "omicron;": "\u03BF",                                                                                            // 1648
  "omid;": "\u29B6",                                                                                               // 1649
  "ominus;": "\u2296",                                                                                             // 1650
  "oopf;": "\u1D560",                                                                                              // 1651
  "opar;": "\u29B7",                                                                                               // 1652
  "operp;": "\u29B9",                                                                                              // 1653
  "oplus;": "\u2295",                                                                                              // 1654
  "or;": "\u2228",                                                                                                 // 1655
  "orarr;": "\u21BB",                                                                                              // 1656
  "ord;": "\u2A5D",                                                                                                // 1657
  "order;": "\u2134",                                                                                              // 1658
  "orderof;": "\u2134",                                                                                            // 1659
  "ordf": "\u00AA",                                                                                                // 1660
  "ordf;": "\u00AA",                                                                                               // 1661
  "ordm": "\u00BA",                                                                                                // 1662
  "ordm;": "\u00BA",                                                                                               // 1663
  "origof;": "\u22B6",                                                                                             // 1664
  "oror;": "\u2A56",                                                                                               // 1665
  "orslope;": "\u2A57",                                                                                            // 1666
  "orv;": "\u2A5B",                                                                                                // 1667
  "oscr;": "\u2134",                                                                                               // 1668
  "oslash": "\u00F8",                                                                                              // 1669
  "oslash;": "\u00F8",                                                                                             // 1670
  "osol;": "\u2298",                                                                                               // 1671
  "otilde": "\u00F5",                                                                                              // 1672
  "otilde;": "\u00F5",                                                                                             // 1673
  "otimes;": "\u2297",                                                                                             // 1674
  "otimesas;": "\u2A36",                                                                                           // 1675
  "ouml": "\u00F6",                                                                                                // 1676
  "ouml;": "\u00F6",                                                                                               // 1677
  "ovbar;": "\u233D",                                                                                              // 1678
  "par;": "\u2225",                                                                                                // 1679
  "para": "\u00B6",                                                                                                // 1680
  "para;": "\u00B6",                                                                                               // 1681
  "parallel;": "\u2225",                                                                                           // 1682
  "parsim;": "\u2AF3",                                                                                             // 1683
  "parsl;": "\u2AFD",                                                                                              // 1684
  "part;": "\u2202",                                                                                               // 1685
  "pcy;": "\u043F",                                                                                                // 1686
  "percnt;": "%",                                                                                                  // 1687
  "period;": ".",                                                                                                  // 1688
  "permil;": "\u2030",                                                                                             // 1689
  "perp;": "\u22A5",                                                                                               // 1690
  "pertenk;": "\u2031",                                                                                            // 1691
  "pfr;": "\u1D52D",                                                                                               // 1692
  "phi;": "\u03C6",                                                                                                // 1693
  "phiv;": "\u03D5",                                                                                               // 1694
  "phmmat;": "\u2133",                                                                                             // 1695
  "phone;": "\u260E",                                                                                              // 1696
  "pi;": "\u03C0",                                                                                                 // 1697
  "pitchfork;": "\u22D4",                                                                                          // 1698
  "piv;": "\u03D6",                                                                                                // 1699
  "planck;": "\u210F",                                                                                             // 1700
  "planckh;": "\u210E",                                                                                            // 1701
  "plankv;": "\u210F",                                                                                             // 1702
  "plus;": "+",                                                                                                    // 1703
  "plusacir;": "\u2A23",                                                                                           // 1704
  "plusb;": "\u229E",                                                                                              // 1705
  "pluscir;": "\u2A22",                                                                                            // 1706
  "plusdo;": "\u2214",                                                                                             // 1707
  "plusdu;": "\u2A25",                                                                                             // 1708
  "pluse;": "\u2A72",                                                                                              // 1709
  "plusmn": "\u00B1",                                                                                              // 1710
  "plusmn;": "\u00B1",                                                                                             // 1711
  "plussim;": "\u2A26",                                                                                            // 1712
  "plustwo;": "\u2A27",                                                                                            // 1713
  "pm;": "\u00B1",                                                                                                 // 1714
  "pointint;": "\u2A15",                                                                                           // 1715
  "popf;": "\u1D561",                                                                                              // 1716
  "pound": "\u00A3",                                                                                               // 1717
  "pound;": "\u00A3",                                                                                              // 1718
  "pr;": "\u227A",                                                                                                 // 1719
  "prE;": "\u2AB3",                                                                                                // 1720
  "prap;": "\u2AB7",                                                                                               // 1721
  "prcue;": "\u227C",                                                                                              // 1722
  "pre;": "\u2AAF",                                                                                                // 1723
  "prec;": "\u227A",                                                                                               // 1724
  "precapprox;": "\u2AB7",                                                                                         // 1725
  "preccurlyeq;": "\u227C",                                                                                        // 1726
  "preceq;": "\u2AAF",                                                                                             // 1727
  "precnapprox;": "\u2AB9",                                                                                        // 1728
  "precneqq;": "\u2AB5",                                                                                           // 1729
  "precnsim;": "\u22E8",                                                                                           // 1730
  "precsim;": "\u227E",                                                                                            // 1731
  "prime;": "\u2032",                                                                                              // 1732
  "primes;": "\u2119",                                                                                             // 1733
  "prnE;": "\u2AB5",                                                                                               // 1734
  "prnap;": "\u2AB9",                                                                                              // 1735
  "prnsim;": "\u22E8",                                                                                             // 1736
  "prod;": "\u220F",                                                                                               // 1737
  "profalar;": "\u232E",                                                                                           // 1738
  "profline;": "\u2312",                                                                                           // 1739
  "profsurf;": "\u2313",                                                                                           // 1740
  "prop;": "\u221D",                                                                                               // 1741
  "propto;": "\u221D",                                                                                             // 1742
  "prsim;": "\u227E",                                                                                              // 1743
  "prurel;": "\u22B0",                                                                                             // 1744
  "pscr;": "\u1D4C5",                                                                                              // 1745
  "psi;": "\u03C8",                                                                                                // 1746
  "puncsp;": "\u2008",                                                                                             // 1747
  "qfr;": "\u1D52E",                                                                                               // 1748
  "qint;": "\u2A0C",                                                                                               // 1749
  "qopf;": "\u1D562",                                                                                              // 1750
  "qprime;": "\u2057",                                                                                             // 1751
  "qscr;": "\u1D4C6",                                                                                              // 1752
  "quaternions;": "\u210D",                                                                                        // 1753
  "quatint;": "\u2A16",                                                                                            // 1754
  "quest;": "?",                                                                                                   // 1755
  "questeq;": "\u225F",                                                                                            // 1756
  "quot": "\u0022",                                                                                                // 1757
  "quot;": "\u0022",                                                                                               // 1758
  "rAarr;": "\u21DB",                                                                                              // 1759
  "rArr;": "\u21D2",                                                                                               // 1760
  "rAtail;": "\u291C",                                                                                             // 1761
  "rBarr;": "\u290F",                                                                                              // 1762
  "rHar;": "\u2964",                                                                                               // 1763
  "race;": "\u223D\u0331",                                                                                         // 1764
  "racute;": "\u0155",                                                                                             // 1765
  "radic;": "\u221A",                                                                                              // 1766
  "raemptyv;": "\u29B3",                                                                                           // 1767
  "rang;": "\u27E9",                                                                                               // 1768
  "rangd;": "\u2992",                                                                                              // 1769
  "range;": "\u29A5",                                                                                              // 1770
  "rangle;": "\u27E9",                                                                                             // 1771
  "raquo": "\u00BB",                                                                                               // 1772
  "raquo;": "\u00BB",                                                                                              // 1773
  "rarr;": "\u2192",                                                                                               // 1774
  "rarrap;": "\u2975",                                                                                             // 1775
  "rarrb;": "\u21E5",                                                                                              // 1776
  "rarrbfs;": "\u2920",                                                                                            // 1777
  "rarrc;": "\u2933",                                                                                              // 1778
  "rarrfs;": "\u291E",                                                                                             // 1779
  "rarrhk;": "\u21AA",                                                                                             // 1780
  "rarrlp;": "\u21AC",                                                                                             // 1781
  "rarrpl;": "\u2945",                                                                                             // 1782
  "rarrsim;": "\u2974",                                                                                            // 1783
  "rarrtl;": "\u21A3",                                                                                             // 1784
  "rarrw;": "\u219D",                                                                                              // 1785
  "ratail;": "\u291A",                                                                                             // 1786
  "ratio;": "\u2236",                                                                                              // 1787
  "rationals;": "\u211A",                                                                                          // 1788
  "rbarr;": "\u290D",                                                                                              // 1789
  "rbbrk;": "\u2773",                                                                                              // 1790
  "rbrace;": "}",                                                                                                  // 1791
  "rbrack;": "]",                                                                                                  // 1792
  "rbrke;": "\u298C",                                                                                              // 1793
  "rbrksld;": "\u298E",                                                                                            // 1794
  "rbrkslu;": "\u2990",                                                                                            // 1795
  "rcaron;": "\u0159",                                                                                             // 1796
  "rcedil;": "\u0157",                                                                                             // 1797
  "rceil;": "\u2309",                                                                                              // 1798
  "rcub;": "}",                                                                                                    // 1799
  "rcy;": "\u0440",                                                                                                // 1800
  "rdca;": "\u2937",                                                                                               // 1801
  "rdldhar;": "\u2969",                                                                                            // 1802
  "rdquo;": "\u201D",                                                                                              // 1803
  "rdquor;": "\u201D",                                                                                             // 1804
  "rdsh;": "\u21B3",                                                                                               // 1805
  "real;": "\u211C",                                                                                               // 1806
  "realine;": "\u211B",                                                                                            // 1807
  "realpart;": "\u211C",                                                                                           // 1808
  "reals;": "\u211D",                                                                                              // 1809
  "rect;": "\u25AD",                                                                                               // 1810
  "reg": "\u00AE",                                                                                                 // 1811
  "reg;": "\u00AE",                                                                                                // 1812
  "rfisht;": "\u297D",                                                                                             // 1813
  "rfloor;": "\u230B",                                                                                             // 1814
  "rfr;": "\u1D52F",                                                                                               // 1815
  "rhard;": "\u21C1",                                                                                              // 1816
  "rharu;": "\u21C0",                                                                                              // 1817
  "rharul;": "\u296C",                                                                                             // 1818
  "rho;": "\u03C1",                                                                                                // 1819
  "rhov;": "\u03F1",                                                                                               // 1820
  "rightarrow;": "\u2192",                                                                                         // 1821
  "rightarrowtail;": "\u21A3",                                                                                     // 1822
  "rightharpoondown;": "\u21C1",                                                                                   // 1823
  "rightharpoonup;": "\u21C0",                                                                                     // 1824
  "rightleftarrows;": "\u21C4",                                                                                    // 1825
  "rightleftharpoons;": "\u21CC",                                                                                  // 1826
  "rightrightarrows;": "\u21C9",                                                                                   // 1827
  "rightsquigarrow;": "\u219D",                                                                                    // 1828
  "rightthreetimes;": "\u22CC",                                                                                    // 1829
  "ring;": "\u02DA",                                                                                               // 1830
  "risingdotseq;": "\u2253",                                                                                       // 1831
  "rlarr;": "\u21C4",                                                                                              // 1832
  "rlhar;": "\u21CC",                                                                                              // 1833
  "rlm;": "\u200F",                                                                                                // 1834
  "rmoust;": "\u23B1",                                                                                             // 1835
  "rmoustache;": "\u23B1",                                                                                         // 1836
  "rnmid;": "\u2AEE",                                                                                              // 1837
  "roang;": "\u27ED",                                                                                              // 1838
  "roarr;": "\u21FE",                                                                                              // 1839
  "robrk;": "\u27E7",                                                                                              // 1840
  "ropar;": "\u2986",                                                                                              // 1841
  "ropf;": "\u1D563",                                                                                              // 1842
  "roplus;": "\u2A2E",                                                                                             // 1843
  "rotimes;": "\u2A35",                                                                                            // 1844
  "rpar;": ")",                                                                                                    // 1845
  "rpargt;": "\u2994",                                                                                             // 1846
  "rppolint;": "\u2A12",                                                                                           // 1847
  "rrarr;": "\u21C9",                                                                                              // 1848
  "rsaquo;": "\u203A",                                                                                             // 1849
  "rscr;": "\u1D4C7",                                                                                              // 1850
  "rsh;": "\u21B1",                                                                                                // 1851
  "rsqb;": "]",                                                                                                    // 1852
  "rsquo;": "\u2019",                                                                                              // 1853
  "rsquor;": "\u2019",                                                                                             // 1854
  "rthree;": "\u22CC",                                                                                             // 1855
  "rtimes;": "\u22CA",                                                                                             // 1856
  "rtri;": "\u25B9",                                                                                               // 1857
  "rtrie;": "\u22B5",                                                                                              // 1858
  "rtrif;": "\u25B8",                                                                                              // 1859
  "rtriltri;": "\u29CE",                                                                                           // 1860
  "ruluhar;": "\u2968",                                                                                            // 1861
  "rx;": "\u211E",                                                                                                 // 1862
  "sacute;": "\u015B",                                                                                             // 1863
  "sbquo;": "\u201A",                                                                                              // 1864
  "sc;": "\u227B",                                                                                                 // 1865
  "scE;": "\u2AB4",                                                                                                // 1866
  "scap;": "\u2AB8",                                                                                               // 1867
  "scaron;": "\u0161",                                                                                             // 1868
  "sccue;": "\u227D",                                                                                              // 1869
  "sce;": "\u2AB0",                                                                                                // 1870
  "scedil;": "\u015F",                                                                                             // 1871
  "scirc;": "\u015D",                                                                                              // 1872
  "scnE;": "\u2AB6",                                                                                               // 1873
  "scnap;": "\u2ABA",                                                                                              // 1874
  "scnsim;": "\u22E9",                                                                                             // 1875
  "scpolint;": "\u2A13",                                                                                           // 1876
  "scsim;": "\u227F",                                                                                              // 1877
  "scy;": "\u0441",                                                                                                // 1878
  "sdot;": "\u22C5",                                                                                               // 1879
  "sdotb;": "\u22A1",                                                                                              // 1880
  "sdote;": "\u2A66",                                                                                              // 1881
  "seArr;": "\u21D8",                                                                                              // 1882
  "searhk;": "\u2925",                                                                                             // 1883
  "searr;": "\u2198",                                                                                              // 1884
  "searrow;": "\u2198",                                                                                            // 1885
  "sect": "\u00A7",                                                                                                // 1886
  "sect;": "\u00A7",                                                                                               // 1887
  "semi;": ";",                                                                                                    // 1888
  "seswar;": "\u2929",                                                                                             // 1889
  "setminus;": "\u2216",                                                                                           // 1890
  "setmn;": "\u2216",                                                                                              // 1891
  "sext;": "\u2736",                                                                                               // 1892
  "sfr;": "\u1D530",                                                                                               // 1893
  "sfrown;": "\u2322",                                                                                             // 1894
  "sharp;": "\u266F",                                                                                              // 1895
  "shchcy;": "\u0449",                                                                                             // 1896
  "shcy;": "\u0448",                                                                                               // 1897
  "shortmid;": "\u2223",                                                                                           // 1898
  "shortparallel;": "\u2225",                                                                                      // 1899
  "shy": "\u00AD",                                                                                                 // 1900
  "shy;": "\u00AD",                                                                                                // 1901
  "sigma;": "\u03C3",                                                                                              // 1902
  "sigmaf;": "\u03C2",                                                                                             // 1903
  "sigmav;": "\u03C2",                                                                                             // 1904
  "sim;": "\u223C",                                                                                                // 1905
  "simdot;": "\u2A6A",                                                                                             // 1906
  "sime;": "\u2243",                                                                                               // 1907
  "simeq;": "\u2243",                                                                                              // 1908
  "simg;": "\u2A9E",                                                                                               // 1909
  "simgE;": "\u2AA0",                                                                                              // 1910
  "siml;": "\u2A9D",                                                                                               // 1911
  "simlE;": "\u2A9F",                                                                                              // 1912
  "simne;": "\u2246",                                                                                              // 1913
  "simplus;": "\u2A24",                                                                                            // 1914
  "simrarr;": "\u2972",                                                                                            // 1915
  "slarr;": "\u2190",                                                                                              // 1916
  "smallsetminus;": "\u2216",                                                                                      // 1917
  "smashp;": "\u2A33",                                                                                             // 1918
  "smeparsl;": "\u29E4",                                                                                           // 1919
  "smid;": "\u2223",                                                                                               // 1920
  "smile;": "\u2323",                                                                                              // 1921
  "smt;": "\u2AAA",                                                                                                // 1922
  "smte;": "\u2AAC",                                                                                               // 1923
  "smtes;": "\u2AAC\uFE00",                                                                                        // 1924
  "softcy;": "\u044C",                                                                                             // 1925
  "sol;": "/",                                                                                                     // 1926
  "solb;": "\u29C4",                                                                                               // 1927
  "solbar;": "\u233F",                                                                                             // 1928
  "sopf;": "\u1D564",                                                                                              // 1929
  "spades;": "\u2660",                                                                                             // 1930
  "spadesuit;": "\u2660",                                                                                          // 1931
  "spar;": "\u2225",                                                                                               // 1932
  "sqcap;": "\u2293",                                                                                              // 1933
  "sqcaps;": "\u2293\uFE00",                                                                                       // 1934
  "sqcup;": "\u2294",                                                                                              // 1935
  "sqcups;": "\u2294\uFE00",                                                                                       // 1936
  "sqsub;": "\u228F",                                                                                              // 1937
  "sqsube;": "\u2291",                                                                                             // 1938
  "sqsubset;": "\u228F",                                                                                           // 1939
  "sqsubseteq;": "\u2291",                                                                                         // 1940
  "sqsup;": "\u2290",                                                                                              // 1941
  "sqsupe;": "\u2292",                                                                                             // 1942
  "sqsupset;": "\u2290",                                                                                           // 1943
  "sqsupseteq;": "\u2292",                                                                                         // 1944
  "squ;": "\u25A1",                                                                                                // 1945
  "square;": "\u25A1",                                                                                             // 1946
  "squarf;": "\u25AA",                                                                                             // 1947
  "squf;": "\u25AA",                                                                                               // 1948
  "srarr;": "\u2192",                                                                                              // 1949
  "sscr;": "\u1D4C8",                                                                                              // 1950
  "ssetmn;": "\u2216",                                                                                             // 1951
  "ssmile;": "\u2323",                                                                                             // 1952
  "sstarf;": "\u22C6",                                                                                             // 1953
  "star;": "\u2606",                                                                                               // 1954
  "starf;": "\u2605",                                                                                              // 1955
  "straightepsilon;": "\u03F5",                                                                                    // 1956
  "straightphi;": "\u03D5",                                                                                        // 1957
  "strns;": "\u00AF",                                                                                              // 1958
  "sub;": "\u2282",                                                                                                // 1959
  "subE;": "\u2AC5",                                                                                               // 1960
  "subdot;": "\u2ABD",                                                                                             // 1961
  "sube;": "\u2286",                                                                                               // 1962
  "subedot;": "\u2AC3",                                                                                            // 1963
  "submult;": "\u2AC1",                                                                                            // 1964
  "subnE;": "\u2ACB",                                                                                              // 1965
  "subne;": "\u228A",                                                                                              // 1966
  "subplus;": "\u2ABF",                                                                                            // 1967
  "subrarr;": "\u2979",                                                                                            // 1968
  "subset;": "\u2282",                                                                                             // 1969
  "subseteq;": "\u2286",                                                                                           // 1970
  "subseteqq;": "\u2AC5",                                                                                          // 1971
  "subsetneq;": "\u228A",                                                                                          // 1972
  "subsetneqq;": "\u2ACB",                                                                                         // 1973
  "subsim;": "\u2AC7",                                                                                             // 1974
  "subsub;": "\u2AD5",                                                                                             // 1975
  "subsup;": "\u2AD3",                                                                                             // 1976
  "succ;": "\u227B",                                                                                               // 1977
  "succapprox;": "\u2AB8",                                                                                         // 1978
  "succcurlyeq;": "\u227D",                                                                                        // 1979
  "succeq;": "\u2AB0",                                                                                             // 1980
  "succnapprox;": "\u2ABA",                                                                                        // 1981
  "succneqq;": "\u2AB6",                                                                                           // 1982
  "succnsim;": "\u22E9",                                                                                           // 1983
  "succsim;": "\u227F",                                                                                            // 1984
  "sum;": "\u2211",                                                                                                // 1985
  "sung;": "\u266A",                                                                                               // 1986
  "sup1": "\u00B9",                                                                                                // 1987
  "sup1;": "\u00B9",                                                                                               // 1988
  "sup2": "\u00B2",                                                                                                // 1989
  "sup2;": "\u00B2",                                                                                               // 1990
  "sup3": "\u00B3",                                                                                                // 1991
  "sup3;": "\u00B3",                                                                                               // 1992
  "sup;": "\u2283",                                                                                                // 1993
  "supE;": "\u2AC6",                                                                                               // 1994
  "supdot;": "\u2ABE",                                                                                             // 1995
  "supdsub;": "\u2AD8",                                                                                            // 1996
  "supe;": "\u2287",                                                                                               // 1997
  "supedot;": "\u2AC4",                                                                                            // 1998
  "suphsol;": "\u27C9",                                                                                            // 1999
  "suphsub;": "\u2AD7",                                                                                            // 2000
  "suplarr;": "\u297B",                                                                                            // 2001
  "supmult;": "\u2AC2",                                                                                            // 2002
  "supnE;": "\u2ACC",                                                                                              // 2003
  "supne;": "\u228B",                                                                                              // 2004
  "supplus;": "\u2AC0",                                                                                            // 2005
  "supset;": "\u2283",                                                                                             // 2006
  "supseteq;": "\u2287",                                                                                           // 2007
  "supseteqq;": "\u2AC6",                                                                                          // 2008
  "supsetneq;": "\u228B",                                                                                          // 2009
  "supsetneqq;": "\u2ACC",                                                                                         // 2010
  "supsim;": "\u2AC8",                                                                                             // 2011
  "supsub;": "\u2AD4",                                                                                             // 2012
  "supsup;": "\u2AD6",                                                                                             // 2013
  "swArr;": "\u21D9",                                                                                              // 2014
  "swarhk;": "\u2926",                                                                                             // 2015
  "swarr;": "\u2199",                                                                                              // 2016
  "swarrow;": "\u2199",                                                                                            // 2017
  "swnwar;": "\u292A",                                                                                             // 2018
  "szlig": "\u00DF",                                                                                               // 2019
  "szlig;": "\u00DF",                                                                                              // 2020
  "target;": "\u2316",                                                                                             // 2021
  "tau;": "\u03C4",                                                                                                // 2022
  "tbrk;": "\u23B4",                                                                                               // 2023
  "tcaron;": "\u0165",                                                                                             // 2024
  "tcedil;": "\u0163",                                                                                             // 2025
  "tcy;": "\u0442",                                                                                                // 2026
  "tdot;": "\u20DB",                                                                                               // 2027
  "telrec;": "\u2315",                                                                                             // 2028
  "tfr;": "\u1D531",                                                                                               // 2029
  "there4;": "\u2234",                                                                                             // 2030
  "therefore;": "\u2234",                                                                                          // 2031
  "theta;": "\u03B8",                                                                                              // 2032
  "thetasym;": "\u03D1",                                                                                           // 2033
  "thetav;": "\u03D1",                                                                                             // 2034
  "thickapprox;": "\u2248",                                                                                        // 2035
  "thicksim;": "\u223C",                                                                                           // 2036
  "thinsp;": "\u2009",                                                                                             // 2037
  "thkap;": "\u2248",                                                                                              // 2038
  "thksim;": "\u223C",                                                                                             // 2039
  "thorn": "\u00FE",                                                                                               // 2040
  "thorn;": "\u00FE",                                                                                              // 2041
  "tilde;": "\u02DC",                                                                                              // 2042
  "times": "\u00D7",                                                                                               // 2043
  "times;": "\u00D7",                                                                                              // 2044
  "timesb;": "\u22A0",                                                                                             // 2045
  "timesbar;": "\u2A31",                                                                                           // 2046
  "timesd;": "\u2A30",                                                                                             // 2047
  "tint;": "\u222D",                                                                                               // 2048
  "toea;": "\u2928",                                                                                               // 2049
  "top;": "\u22A4",                                                                                                // 2050
  "topbot;": "\u2336",                                                                                             // 2051
  "topcir;": "\u2AF1",                                                                                             // 2052
  "topf;": "\u1D565",                                                                                              // 2053
  "topfork;": "\u2ADA",                                                                                            // 2054
  "tosa;": "\u2929",                                                                                               // 2055
  "tprime;": "\u2034",                                                                                             // 2056
  "trade;": "\u2122",                                                                                              // 2057
  "triangle;": "\u25B5",                                                                                           // 2058
  "triangledown;": "\u25BF",                                                                                       // 2059
  "triangleleft;": "\u25C3",                                                                                       // 2060
  "trianglelefteq;": "\u22B4",                                                                                     // 2061
  "triangleq;": "\u225C",                                                                                          // 2062
  "triangleright;": "\u25B9",                                                                                      // 2063
  "trianglerighteq;": "\u22B5",                                                                                    // 2064
  "tridot;": "\u25EC",                                                                                             // 2065
  "trie;": "\u225C",                                                                                               // 2066
  "triminus;": "\u2A3A",                                                                                           // 2067
  "triplus;": "\u2A39",                                                                                            // 2068
  "trisb;": "\u29CD",                                                                                              // 2069
  "tritime;": "\u2A3B",                                                                                            // 2070
  "trpezium;": "\u23E2",                                                                                           // 2071
  "tscr;": "\u1D4C9",                                                                                              // 2072
  "tscy;": "\u0446",                                                                                               // 2073
  "tshcy;": "\u045B",                                                                                              // 2074
  "tstrok;": "\u0167",                                                                                             // 2075
  "twixt;": "\u226C",                                                                                              // 2076
  "twoheadleftarrow;": "\u219E",                                                                                   // 2077
  "twoheadrightarrow;": "\u21A0",                                                                                  // 2078
  "uArr;": "\u21D1",                                                                                               // 2079
  "uHar;": "\u2963",                                                                                               // 2080
  "uacute": "\u00FA",                                                                                              // 2081
  "uacute;": "\u00FA",                                                                                             // 2082
  "uarr;": "\u2191",                                                                                               // 2083
  "ubrcy;": "\u045E",                                                                                              // 2084
  "ubreve;": "\u016D",                                                                                             // 2085
  "ucirc": "\u00FB",                                                                                               // 2086
  "ucirc;": "\u00FB",                                                                                              // 2087
  "ucy;": "\u0443",                                                                                                // 2088
  "udarr;": "\u21C5",                                                                                              // 2089
  "udblac;": "\u0171",                                                                                             // 2090
  "udhar;": "\u296E",                                                                                              // 2091
  "ufisht;": "\u297E",                                                                                             // 2092
  "ufr;": "\u1D532",                                                                                               // 2093
  "ugrave": "\u00F9",                                                                                              // 2094
  "ugrave;": "\u00F9",                                                                                             // 2095
  "uharl;": "\u21BF",                                                                                              // 2096
  "uharr;": "\u21BE",                                                                                              // 2097
  "uhblk;": "\u2580",                                                                                              // 2098
  "ulcorn;": "\u231C",                                                                                             // 2099
  "ulcorner;": "\u231C",                                                                                           // 2100
  "ulcrop;": "\u230F",                                                                                             // 2101
  "ultri;": "\u25F8",                                                                                              // 2102
  "umacr;": "\u016B",                                                                                              // 2103
  "uml": "\u00A8",                                                                                                 // 2104
  "uml;": "\u00A8",                                                                                                // 2105
  "uogon;": "\u0173",                                                                                              // 2106
  "uopf;": "\u1D566",                                                                                              // 2107
  "uparrow;": "\u2191",                                                                                            // 2108
  "updownarrow;": "\u2195",                                                                                        // 2109
  "upharpoonleft;": "\u21BF",                                                                                      // 2110
  "upharpoonright;": "\u21BE",                                                                                     // 2111
  "uplus;": "\u228E",                                                                                              // 2112
  "upsi;": "\u03C5",                                                                                               // 2113
  "upsih;": "\u03D2",                                                                                              // 2114
  "upsilon;": "\u03C5",                                                                                            // 2115
  "upuparrows;": "\u21C8",                                                                                         // 2116
  "urcorn;": "\u231D",                                                                                             // 2117
  "urcorner;": "\u231D",                                                                                           // 2118
  "urcrop;": "\u230E",                                                                                             // 2119
  "uring;": "\u016F",                                                                                              // 2120
  "urtri;": "\u25F9",                                                                                              // 2121
  "uscr;": "\u1D4CA",                                                                                              // 2122
  "utdot;": "\u22F0",                                                                                              // 2123
  "utilde;": "\u0169",                                                                                             // 2124
  "utri;": "\u25B5",                                                                                               // 2125
  "utrif;": "\u25B4",                                                                                              // 2126
  "uuarr;": "\u21C8",                                                                                              // 2127
  "uuml": "\u00FC",                                                                                                // 2128
  "uuml;": "\u00FC",                                                                                               // 2129
  "uwangle;": "\u29A7",                                                                                            // 2130
  "vArr;": "\u21D5",                                                                                               // 2131
  "vBar;": "\u2AE8",                                                                                               // 2132
  "vBarv;": "\u2AE9",                                                                                              // 2133
  "vDash;": "\u22A8",                                                                                              // 2134
  "vangrt;": "\u299C",                                                                                             // 2135
  "varepsilon;": "\u03F5",                                                                                         // 2136
  "varkappa;": "\u03F0",                                                                                           // 2137
  "varnothing;": "\u2205",                                                                                         // 2138
  "varphi;": "\u03D5",                                                                                             // 2139
  "varpi;": "\u03D6",                                                                                              // 2140
  "varpropto;": "\u221D",                                                                                          // 2141
  "varr;": "\u2195",                                                                                               // 2142
  "varrho;": "\u03F1",                                                                                             // 2143
  "varsigma;": "\u03C2",                                                                                           // 2144
  "varsubsetneq;": "\u228A\uFE00",                                                                                 // 2145
  "varsubsetneqq;": "\u2ACB\uFE00",                                                                                // 2146
  "varsupsetneq;": "\u228B\uFE00",                                                                                 // 2147
  "varsupsetneqq;": "\u2ACC\uFE00",                                                                                // 2148
  "vartheta;": "\u03D1",                                                                                           // 2149
  "vartriangleleft;": "\u22B2",                                                                                    // 2150
  "vartriangleright;": "\u22B3",                                                                                   // 2151
  "vcy;": "\u0432",                                                                                                // 2152
  "vdash;": "\u22A2",                                                                                              // 2153
  "vee;": "\u2228",                                                                                                // 2154
  "veebar;": "\u22BB",                                                                                             // 2155
  "veeeq;": "\u225A",                                                                                              // 2156
  "vellip;": "\u22EE",                                                                                             // 2157
  "verbar;": "|",                                                                                                  // 2158
  "vert;": "|",                                                                                                    // 2159
  "vfr;": "\u1D533",                                                                                               // 2160
  "vltri;": "\u22B2",                                                                                              // 2161
  "vnsub;": "\u2282\u20D2",                                                                                        // 2162
  "vnsup;": "\u2283\u20D2",                                                                                        // 2163
  "vopf;": "\u1D567",                                                                                              // 2164
  "vprop;": "\u221D",                                                                                              // 2165
  "vrtri;": "\u22B3",                                                                                              // 2166
  "vscr;": "\u1D4CB",                                                                                              // 2167
  "vsubnE;": "\u2ACB\uFE00",                                                                                       // 2168
  "vsubne;": "\u228A\uFE00",                                                                                       // 2169
  "vsupnE;": "\u2ACC\uFE00",                                                                                       // 2170
  "vsupne;": "\u228B\uFE00",                                                                                       // 2171
  "vzigzag;": "\u299A",                                                                                            // 2172
  "wcirc;": "\u0175",                                                                                              // 2173
  "wedbar;": "\u2A5F",                                                                                             // 2174
  "wedge;": "\u2227",                                                                                              // 2175
  "wedgeq;": "\u2259",                                                                                             // 2176
  "weierp;": "\u2118",                                                                                             // 2177
  "wfr;": "\u1D534",                                                                                               // 2178
  "wopf;": "\u1D568",                                                                                              // 2179
  "wp;": "\u2118",                                                                                                 // 2180
  "wr;": "\u2240",                                                                                                 // 2181
  "wreath;": "\u2240",                                                                                             // 2182
  "wscr;": "\u1D4CC",                                                                                              // 2183
  "xcap;": "\u22C2",                                                                                               // 2184
  "xcirc;": "\u25EF",                                                                                              // 2185
  "xcup;": "\u22C3",                                                                                               // 2186
  "xdtri;": "\u25BD",                                                                                              // 2187
  "xfr;": "\u1D535",                                                                                               // 2188
  "xhArr;": "\u27FA",                                                                                              // 2189
  "xharr;": "\u27F7",                                                                                              // 2190
  "xi;": "\u03BE",                                                                                                 // 2191
  "xlArr;": "\u27F8",                                                                                              // 2192
  "xlarr;": "\u27F5",                                                                                              // 2193
  "xmap;": "\u27FC",                                                                                               // 2194
  "xnis;": "\u22FB",                                                                                               // 2195
  "xodot;": "\u2A00",                                                                                              // 2196
  "xopf;": "\u1D569",                                                                                              // 2197
  "xoplus;": "\u2A01",                                                                                             // 2198
  "xotime;": "\u2A02",                                                                                             // 2199
  "xrArr;": "\u27F9",                                                                                              // 2200
  "xrarr;": "\u27F6",                                                                                              // 2201
  "xscr;": "\u1D4CD",                                                                                              // 2202
  "xsqcup;": "\u2A06",                                                                                             // 2203
  "xuplus;": "\u2A04",                                                                                             // 2204
  "xutri;": "\u25B3",                                                                                              // 2205
  "xvee;": "\u22C1",                                                                                               // 2206
  "xwedge;": "\u22C0",                                                                                             // 2207
  "yacute": "\u00FD",                                                                                              // 2208
  "yacute;": "\u00FD",                                                                                             // 2209
  "yacy;": "\u044F",                                                                                               // 2210
  "ycirc;": "\u0177",                                                                                              // 2211
  "ycy;": "\u044B",                                                                                                // 2212
  "yen": "\u00A5",                                                                                                 // 2213
  "yen;": "\u00A5",                                                                                                // 2214
  "yfr;": "\u1D536",                                                                                               // 2215
  "yicy;": "\u0457",                                                                                               // 2216
  "yopf;": "\u1D56A",                                                                                              // 2217
  "yscr;": "\u1D4CE",                                                                                              // 2218
  "yucy;": "\u044E",                                                                                               // 2219
  "yuml": "\u00FF",                                                                                                // 2220
  "yuml;": "\u00FF",                                                                                               // 2221
  "zacute;": "\u017A",                                                                                             // 2222
  "zcaron;": "\u017E",                                                                                             // 2223
  "zcy;": "\u0437",                                                                                                // 2224
  "zdot;": "\u017C",                                                                                               // 2225
  "zeetrf;": "\u2128",                                                                                             // 2226
  "zeta;": "\u03B6",                                                                                               // 2227
  "zfr;": "\u1D537",                                                                                               // 2228
  "zhcy;": "\u0436",                                                                                               // 2229
  "zigrarr;": "\u21DD",                                                                                            // 2230
  "zopf;": "\u1D56B",                                                                                              // 2231
  "zscr;": "\u1D4CF",                                                                                              // 2232
  "zwj;": "\u200D",                                                                                                // 2233
  "zwnj;": "\u200C"                                                                                                // 2234
};                                                                                                                 // 2235
                                                                                                                   // 2236
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/constants.js                                                                           //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
HTML5 = (typeof HTML5 === 'undefined' ? {} : HTML5);                                                               // 1
                                                                                                                   // 2
HTML5.CONTENT_MODEL_FLAGS = [                                                                                      // 3
  'PCDATA',                                                                                                        // 4
  'RCDATA',                                                                                                        // 5
  'CDATA',                                                                                                         // 6
  'SCRIPT_CDATA',                                                                                                  // 7
  'PLAINTEXT'                                                                                                      // 8
];                                                                                                                 // 9
                                                                                                                   // 10
HTML5.Marker = {type: 'Marker', data: 'this is a marker token'};                                                   // 11
                                                                                                                   // 12
                                                                                                                   // 13
(function() {                                                                                                      // 14
  function EOF() {                                                                                                 // 15
  }                                                                                                                // 16
                                                                                                                   // 17
  EOF.prototype = {                                                                                                // 18
    toString: function() { throw new Error("EOF added as string"); }                                               // 19
  };                                                                                                               // 20
  HTML5.EOF = new EOF();                                                                                           // 21
})();                                                                                                              // 22
                                                                                                                   // 23
                                                                                                                   // 24
HTML5.EOF_TOK = {type: 'EOF', data: 'End of File' };                                                               // 25
HTML5.DRAIN = -2;                                                                                                  // 26
                                                                                                                   // 27
HTML5.SCOPING_ELEMENTS = [                                                                                         // 28
  'applet', 'caption', 'html', 'table', 'td', 'th',                                                                // 29
  'marquee', 'object', 'math:mi', 'math:mo', 'math:mn', 'math:ms', 'math:mtext',                                   // 30
  'math:annotation-xml', 'svg:foreignObject', 'svg:desc', 'svg:title'                                              // 31
];                                                                                                                 // 32
                                                                                                                   // 33
HTML5.LIST_SCOPING_ELEMENTS = [                                                                                    // 34
  'ol', 'ul',                                                                                                      // 35
  'applet', 'caption', 'html', 'table', 'td', 'th',                                                                // 36
  'marquee', 'object', 'math:mi', 'math:mo', 'math:mn', 'math:ms', 'math:mtext',                                   // 37
  'math:annotation-xml', 'svg:foreignObject', 'svg:desc', 'svg:title'                                              // 38
];                                                                                                                 // 39
HTML5.BUTTON_SCOPING_ELEMENTS = [                                                                                  // 40
  'button',                                                                                                        // 41
  'applet', 'caption', 'html', 'table', 'td', 'th',                                                                // 42
  'marquee', 'object', 'math:mi', 'math:mo', 'math:mn', 'math:ms', 'math:mtext',                                   // 43
  'math:annotation-xml', 'svg:foreignObject', 'svg:desc', 'svg:title'                                              // 44
];                                                                                                                 // 45
HTML5.TABLE_SCOPING_ELEMENTS = [                                                                                   // 46
  'table', 'html'                                                                                                  // 47
];                                                                                                                 // 48
HTML5.SELECT_SCOPING_ELEMENTS = [                                                                                  // 49
  'option', 'optgroup'                                                                                             // 50
];                                                                                                                 // 51
HTML5.FORMATTING_ELEMENTS = [                                                                                      // 52
  'a',                                                                                                             // 53
  'b',                                                                                                             // 54
  'big',                                                                                                           // 55
  'code',                                                                                                          // 56
  'em',                                                                                                            // 57
  'font',                                                                                                          // 58
  'i',                                                                                                             // 59
  'nobr',                                                                                                          // 60
  's',                                                                                                             // 61
  'small',                                                                                                         // 62
  'strike',                                                                                                        // 63
  'strong',                                                                                                        // 64
  'tt',                                                                                                            // 65
  'u'                                                                                                              // 66
];                                                                                                                 // 67
HTML5.SPECIAL_ELEMENTS = [                                                                                         // 68
  'address',                                                                                                       // 69
  'area',                                                                                                          // 70
  'base',                                                                                                          // 71
  'basefont',                                                                                                      // 72
  'bgsound',                                                                                                       // 73
  'blockquote',                                                                                                    // 74
  'body',                                                                                                          // 75
  'br',                                                                                                            // 76
  'center',                                                                                                        // 77
  'col',                                                                                                           // 78
  'colgroup',                                                                                                      // 79
  'dd',                                                                                                            // 80
  'dir',                                                                                                           // 81
  'div',                                                                                                           // 82
  'dl',                                                                                                            // 83
  'dt',                                                                                                            // 84
  'embed',                                                                                                         // 85
  'fieldset',                                                                                                      // 86
  'form',                                                                                                          // 87
  'frame',                                                                                                         // 88
  'frameset',                                                                                                      // 89
  'h1',                                                                                                            // 90
  'h2',                                                                                                            // 91
  'h3',                                                                                                            // 92
  'h4',                                                                                                            // 93
  'h5',                                                                                                            // 94
  'h6',                                                                                                            // 95
  'head',                                                                                                          // 96
  'hr',                                                                                                            // 97
  'iframe',                                                                                                        // 98
  'image',                                                                                                         // 99
  'img',                                                                                                           // 100
  'input',                                                                                                         // 101
  'isindex',                                                                                                       // 102
  'li',                                                                                                            // 103
  'link',                                                                                                          // 104
  'listing',                                                                                                       // 105
  'menu',                                                                                                          // 106
  'meta',                                                                                                          // 107
  'noembed',                                                                                                       // 108
  'noframes',                                                                                                      // 109
  'noscript',                                                                                                      // 110
  'ol',                                                                                                            // 111
  'optgroup',                                                                                                      // 112
  'option',                                                                                                        // 113
  'p',                                                                                                             // 114
  'param',                                                                                                         // 115
  'plaintext',                                                                                                     // 116
  'pre',                                                                                                           // 117
  'script',                                                                                                        // 118
  'select',                                                                                                        // 119
  'spacer',                                                                                                        // 120
  'style',                                                                                                         // 121
  'tbody',                                                                                                         // 122
  'textarea',                                                                                                      // 123
  'tfoot',                                                                                                         // 124
  'thead',                                                                                                         // 125
  'title',                                                                                                         // 126
  'tr',                                                                                                            // 127
  'ul',                                                                                                            // 128
  'wbr'                                                                                                            // 129
];                                                                                                                 // 130
HTML5.SPACE_CHARACTERS_IN = "\t\n\x0B\x0C\x20\u0012\r";                                                            // 131
HTML5.SPACE_CHARACTERS = "[\t\n\x0B\x0C\x20\r]";                                                                   // 132
HTML5.SPACE_CHARACTERS_R = /^[\t\n\x0B\x0C \r]/;                                                                   // 133
                                                                                                                   // 134
HTML5.TABLE_INSERT_MODE_ELEMENTS = [                                                                               // 135
  'table',                                                                                                         // 136
  'tbody',                                                                                                         // 137
  'tfoot',                                                                                                         // 138
  'thead',                                                                                                         // 139
  'tr'                                                                                                             // 140
];                                                                                                                 // 141
                                                                                                                   // 142
HTML5.ASCII_LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';                                                              // 143
HTML5.ASCII_UPPERCASE = HTML5.ASCII_LOWERCASE.toUpperCase();                                                       // 144
HTML5.ASCII_LETTERS = "[a-zA-Z]";                                                                                  // 145
HTML5.ASCII_LETTERS_R = /^[a-zA-Z]/;                                                                               // 146
HTML5.DIGITS = '0123456789';                                                                                       // 147
HTML5.DIGITS_R = new RegExp('^[0123456789]');                                                                      // 148
HTML5.HEX_DIGITS = HTML5.DIGITS + 'abcdefABCDEF';                                                                  // 149
HTML5.HEX_DIGITS_R = new RegExp('^[' + HTML5.DIGITS + 'abcdefABCDEF' +']' );                                       // 150
                                                                                                                   // 151
// Heading elements need to be ordered                                                                             // 152
HTML5.HEADING_ELEMENTS = [                                                                                         // 153
  'h1',                                                                                                            // 154
  'h2',                                                                                                            // 155
  'h3',                                                                                                            // 156
  'h4',                                                                                                            // 157
  'h5',                                                                                                            // 158
  'h6'                                                                                                             // 159
];                                                                                                                 // 160
                                                                                                                   // 161
HTML5.VOID_ELEMENTS = [                                                                                            // 162
  'base',                                                                                                          // 163
  'link',                                                                                                          // 164
  'meta',                                                                                                          // 165
  'hr',                                                                                                            // 166
  'br',                                                                                                            // 167
  'img',                                                                                                           // 168
  'embed',                                                                                                         // 169
  'param',                                                                                                         // 170
  'area',                                                                                                          // 171
  'col',                                                                                                           // 172
  'input'                                                                                                          // 173
];                                                                                                                 // 174
                                                                                                                   // 175
HTML5.CDATA_ELEMENTS = [                                                                                           // 176
  'title',                                                                                                         // 177
  'textarea'                                                                                                       // 178
];                                                                                                                 // 179
                                                                                                                   // 180
HTML5.RCDATA_ELEMENTS = [                                                                                          // 181
  'style',                                                                                                         // 182
  'script',                                                                                                        // 183
  'xmp',                                                                                                           // 184
  'iframe',                                                                                                        // 185
  'noembed',                                                                                                       // 186
  'noframes',                                                                                                      // 187
  'noscript'                                                                                                       // 188
];                                                                                                                 // 189
                                                                                                                   // 190
HTML5.BOOLEAN_ATTRIBUTES = {                                                                                       // 191
  '_global': ['irrelevant'],                                                                                       // 192
  // Fixme?                                                                                                        // 193
  'style': ['scoped'],                                                                                             // 194
  'img': ['ismap'],                                                                                                // 195
  'audio': ['autoplay', 'controls'],                                                                               // 196
  'video': ['autoplay', 'controls'],                                                                               // 197
  'script': ['defer', 'async'],                                                                                    // 198
  'details': ['open'],                                                                                             // 199
  'datagrid': ['multiple', 'disabled'],                                                                            // 200
  'command': ['hidden', 'disabled', 'checked', 'default'],                                                         // 201
  'menu': ['autosubmit'],                                                                                          // 202
  'fieldset': ['disabled', 'readonly'],                                                                            // 203
  'option': ['disabled', 'readonly', 'selected'],                                                                  // 204
  'optgroup': ['disabled', 'readonly'],                                                                            // 205
  'button': ['disabled', 'autofocus'],                                                                             // 206
  'input': ['disabled', 'readonly', 'required', 'autofocus', 'checked', 'ismap'],                                  // 207
  'select': ['disabled', 'readonly', 'autofocus', 'multiple'],                                                     // 208
  'output': ['disabled', 'readonly']                                                                               // 209
};                                                                                                                 // 210
                                                                                                                   // 211
HTML5.ENCODINGS = [                                                                                                // 212
  'ansi_x3.4-1968',                                                                                                // 213
  'iso-ir-6',                                                                                                      // 214
  'ansi_x3.4-1986',                                                                                                // 215
  'iso_646.irv:1991',                                                                                              // 216
  'ascii',                                                                                                         // 217
  'iso646-us',                                                                                                     // 218
  'us-ascii',                                                                                                      // 219
  'us',                                                                                                            // 220
  'ibm367',                                                                                                        // 221
  'cp367',                                                                                                         // 222
  'csascii',                                                                                                       // 223
  'ks_c_5601-1987',                                                                                                // 224
  'korean',                                                                                                        // 225
  'iso-2022-kr',                                                                                                   // 226
  'csiso2022kr',                                                                                                   // 227
  'euc-kr',                                                                                                        // 228
  'iso-2022-jp',                                                                                                   // 229
  'csiso2022jp',                                                                                                   // 230
  'iso-2022-jp-2',                                                                                                 // 231
  '',                                                                                                              // 232
  'iso-ir-58',                                                                                                     // 233
  'chinese',                                                                                                       // 234
  'csiso58gb231280',                                                                                               // 235
  'iso_8859-1:1987',                                                                                               // 236
  'iso-ir-100',                                                                                                    // 237
  'iso_8859-1',                                                                                                    // 238
  'iso-8859-1',                                                                                                    // 239
  'latin1',                                                                                                        // 240
  'l1',                                                                                                            // 241
  'ibm819',                                                                                                        // 242
  'cp819',                                                                                                         // 243
  'csisolatin1',                                                                                                   // 244
  'iso_8859-2:1987',                                                                                               // 245
  'iso-ir-101',                                                                                                    // 246
  'iso_8859-2',                                                                                                    // 247
  'iso-8859-2',                                                                                                    // 248
  'latin2',                                                                                                        // 249
  'l2',                                                                                                            // 250
  'csisolatin2',                                                                                                   // 251
  'iso_8859-3:1988',                                                                                               // 252
  'iso-ir-109',                                                                                                    // 253
  'iso_8859-3',                                                                                                    // 254
  'iso-8859-3',                                                                                                    // 255
  'latin3',                                                                                                        // 256
  'l3',                                                                                                            // 257
  'csisolatin3',                                                                                                   // 258
  'iso_8859-4:1988',                                                                                               // 259
  'iso-ir-110',                                                                                                    // 260
  'iso_8859-4',                                                                                                    // 261
  'iso-8859-4',                                                                                                    // 262
  'latin4',                                                                                                        // 263
  'l4',                                                                                                            // 264
  'csisolatin4',                                                                                                   // 265
  'iso_8859-6:1987',                                                                                               // 266
  'iso-ir-127',                                                                                                    // 267
  'iso_8859-6',                                                                                                    // 268
  'iso-8859-6',                                                                                                    // 269
  'ecma-114',                                                                                                      // 270
  'asmo-708',                                                                                                      // 271
  'arabic',                                                                                                        // 272
  'csisolatinarabic',                                                                                              // 273
  'iso_8859-7:1987',                                                                                               // 274
  'iso-ir-126',                                                                                                    // 275
  'iso_8859-7',                                                                                                    // 276
  'iso-8859-7',                                                                                                    // 277
  'elot_928',                                                                                                      // 278
  'ecma-118',                                                                                                      // 279
  'greek',                                                                                                         // 280
  'greek8',                                                                                                        // 281
  'csisolatingreek',                                                                                               // 282
  'iso_8859-8:1988',                                                                                               // 283
  'iso-ir-138',                                                                                                    // 284
  'iso_8859-8',                                                                                                    // 285
  'iso-8859-8',                                                                                                    // 286
  'hebrew',                                                                                                        // 287
  'csisolatinhebrew',                                                                                              // 288
  'iso_8859-5:1988',                                                                                               // 289
  'iso-ir-144',                                                                                                    // 290
  'iso_8859-5',                                                                                                    // 291
  'iso-8859-5',                                                                                                    // 292
  'cyrillic',                                                                                                      // 293
  'csisolatincyrillic',                                                                                            // 294
  'iso_8859-9:1989',                                                                                               // 295
  'iso-ir-148',                                                                                                    // 296
  'iso_8859-9',                                                                                                    // 297
  'iso-8859-9',                                                                                                    // 298
  'latin5',                                                                                                        // 299
  'l5',                                                                                                            // 300
  'csisolatin5',                                                                                                   // 301
  'iso-8859-10',                                                                                                   // 302
  'iso-ir-157',                                                                                                    // 303
  'l6',                                                                                                            // 304
  'iso_8859-10:1992',                                                                                              // 305
  'csisolatin6',                                                                                                   // 306
  'latin6',                                                                                                        // 307
  'hp-roman8',                                                                                                     // 308
  'roman8',                                                                                                        // 309
  'r8',                                                                                                            // 310
  'ibm037',                                                                                                        // 311
  'cp037',                                                                                                         // 312
  'csibm037',                                                                                                      // 313
  'ibm424',                                                                                                        // 314
  'cp424',                                                                                                         // 315
  'csibm424',                                                                                                      // 316
  'ibm437',                                                                                                        // 317
  'cp437',                                                                                                         // 318
  '437',                                                                                                           // 319
  'cspc8codepage437',                                                                                              // 320
  'ibm500',                                                                                                        // 321
  'cp500',                                                                                                         // 322
  'csibm500',                                                                                                      // 323
  'ibm775',                                                                                                        // 324
  'cp775',                                                                                                         // 325
  'cspc775baltic',                                                                                                 // 326
  'ibm850',                                                                                                        // 327
  'cp850',                                                                                                         // 328
  '850',                                                                                                           // 329
  'cspc850multilingual',                                                                                           // 330
  'ibm852',                                                                                                        // 331
  'cp852',                                                                                                         // 332
  '852',                                                                                                           // 333
  'cspcp852',                                                                                                      // 334
  'ibm855',                                                                                                        // 335
  'cp855',                                                                                                         // 336
  '855',                                                                                                           // 337
  'csibm855',                                                                                                      // 338
  'ibm857',                                                                                                        // 339
  'cp857',                                                                                                         // 340
  '857',                                                                                                           // 341
  'csibm857',                                                                                                      // 342
  'ibm860',                                                                                                        // 343
  'cp860',                                                                                                         // 344
  '860',                                                                                                           // 345
  'csibm860',                                                                                                      // 346
  'ibm861',                                                                                                        // 347
  'cp861',                                                                                                         // 348
  '861',                                                                                                           // 349
  'cp-is',                                                                                                         // 350
  'csibm861',                                                                                                      // 351
  'ibm862',                                                                                                        // 352
  'cp862',                                                                                                         // 353
  '862',                                                                                                           // 354
  'cspc862latinhebrew',                                                                                            // 355
  'ibm863',                                                                                                        // 356
  'cp863',                                                                                                         // 357
  '863',                                                                                                           // 358
  'csibm863',                                                                                                      // 359
  'ibm864',                                                                                                        // 360
  'cp864',                                                                                                         // 361
  'csibm864',                                                                                                      // 362
  'ibm865',                                                                                                        // 363
  'cp865',                                                                                                         // 364
  '865',                                                                                                           // 365
  'csibm865',                                                                                                      // 366
  'ibm866',                                                                                                        // 367
  'cp866',                                                                                                         // 368
  '866',                                                                                                           // 369
  'csibm866',                                                                                                      // 370
  'ibm869',                                                                                                        // 371
  'cp869',                                                                                                         // 372
  '869',                                                                                                           // 373
  'cp-gr',                                                                                                         // 374
  'csibm869',                                                                                                      // 375
  'ibm1026',                                                                                                       // 376
  'cp1026',                                                                                                        // 377
  'csibm1026',                                                                                                     // 378
  'koi8-r',                                                                                                        // 379
  'cskoi8r',                                                                                                       // 380
  'koi8-u',                                                                                                        // 381
  'big5-hkscs',                                                                                                    // 382
  'ptcp154',                                                                                                       // 383
  'csptcp154',                                                                                                     // 384
  'pt154',                                                                                                         // 385
  'cp154',                                                                                                         // 386
  'utf-7',                                                                                                         // 387
  'utf-16be',                                                                                                      // 388
  'utf-16le',                                                                                                      // 389
  'utf-16',                                                                                                        // 390
  'utf-8',                                                                                                         // 391
  'iso-8859-13',                                                                                                   // 392
  'iso-8859-14',                                                                                                   // 393
  'iso-ir-199',                                                                                                    // 394
  'iso_8859-14:1998',                                                                                              // 395
  'iso_8859-14',                                                                                                   // 396
  'latin8',                                                                                                        // 397
  'iso-celtic',                                                                                                    // 398
  'l8',                                                                                                            // 399
  'iso-8859-15',                                                                                                   // 400
  'iso_8859-15',                                                                                                   // 401
  'iso-8859-16',                                                                                                   // 402
  'iso-ir-226',                                                                                                    // 403
  'iso_8859-16:2001',                                                                                              // 404
  'iso_8859-16',                                                                                                   // 405
  'latin10',                                                                                                       // 406
  'l10',                                                                                                           // 407
  'gbk',                                                                                                           // 408
  'cp936',                                                                                                         // 409
  'ms936',                                                                                                         // 410
  'gb18030',                                                                                                       // 411
  'shift_jis',                                                                                                     // 412
  'ms_kanji',                                                                                                      // 413
  'csshiftjis',                                                                                                    // 414
  'euc-jp',                                                                                                        // 415
  'gb2312',                                                                                                        // 416
  'big5',                                                                                                          // 417
  'csbig5',                                                                                                        // 418
  'windows-1250',                                                                                                  // 419
  'windows-1251',                                                                                                  // 420
  'windows-1252',                                                                                                  // 421
  'windows-1253',                                                                                                  // 422
  'windows-1254',                                                                                                  // 423
  'windows-1255',                                                                                                  // 424
  'windows-1256',                                                                                                  // 425
  'windows-1257',                                                                                                  // 426
  'windows-1258',                                                                                                  // 427
  'tis-620',                                                                                                       // 428
  'hz-gb-2312'                                                                                                     // 429
];                                                                                                                 // 430
                                                                                                                   // 431
HTML5.E = {                                                                                                        // 432
  "null-character":                                                                                                // 433
  "Null character in input stream, replaced with U+FFFD.",                                                         // 434
  "incorrectly-placed-solidus":                                                                                    // 435
  "Solidus (/) incorrectly placed in tag.",                                                                        // 436
  "incorrect-cr-newline-entity":                                                                                   // 437
  "Incorrect CR newline entity, replaced with LF.",                                                                // 438
  "illegal-windows-1252-entity":                                                                                   // 439
  "Entity used with illegal number (windows-1252 reference).",                                                     // 440
  "cant-convert-numeric-entity":                                                                                   // 441
  "Numeric entity couldn't be converted to character " +                                                           // 442
    "(codepoint U+%(charAsInt)08x).",                                                                              // 443
  "illegal-codepoint-for-numeric-entity":                                                                          // 444
  "Numeric entity represents an illegal codepoint=> " +                                                            // 445
    "U+%(charAsInt)08x.",                                                                                          // 446
  "numeric-entity-without-semicolon":                                                                              // 447
  "Numeric entity didn't end with ';'.",                                                                           // 448
  "expected-numeric-entity-but-got-eof":                                                                           // 449
  "Numeric entity expected. Got end of file instead.",                                                             // 450
  "expected-numeric-entity":                                                                                       // 451
  "Numeric entity expected but none found.",                                                                       // 452
  "named-entity-without-semicolon":                                                                                // 453
  "Named entity didn't end with ';'.",                                                                             // 454
  "expected-named-entity":                                                                                         // 455
  "Named entity expected. Got none.",                                                                              // 456
  "attributes-in-end-tag":                                                                                         // 457
  "End tag contains unexpected attributes.",                                                                       // 458
  "expected-tag-name-but-got-right-bracket":                                                                       // 459
  "Expected tag name. Got '>' instead.",                                                                           // 460
  "expected-tag-name-but-got-question-mark":                                                                       // 461
  "Expected tag name. Got '?' instead. (HTML doesn't " +                                                           // 462
    "support processing instructions.)",                                                                           // 463
  "expected-tag-name":                                                                                             // 464
  "Expected tag name. Got something else instead",                                                                 // 465
  "expected-closing-tag-but-got-right-bracket":                                                                    // 466
  "Expected closing tag. Got '>' instead. Ignoring '</>'.",                                                        // 467
  "expected-closing-tag-but-got-eof":                                                                              // 468
  "Expected closing tag. Unexpected end of file.",                                                                 // 469
  "expected-closing-tag-but-got-char":                                                                             // 470
  "Expected closing tag. Unexpected character '%(data)' found.",                                                   // 471
  "eof-in-tag-name":                                                                                               // 472
  "Unexpected end of file in the tag name.",                                                                       // 473
  "expected-attribute-name-but-got-eof":                                                                           // 474
  "Unexpected end of file. Expected attribute name instead.",                                                      // 475
  "eof-in-attribute-name":                                                                                         // 476
  "Unexpected end of file in attribute name.",                                                                     // 477
  "duplicate-attribute":                                                                                           // 478
  "Dropped duplicate attribute on tag.",                                                                           // 479
  "expected-end-of-tag-name-but-got-eof":                                                                          // 480
  "Unexpected end of file. Expected = or end of tag.",                                                             // 481
  "expected-attribute-value-but-got-eof":                                                                          // 482
  "Unexpected end of file. Expected attribute value.",                                                             // 483
  "eof-in-attribute-value-double-quote":                                                                           // 484
  "Unexpected end of file in attribute value (\").",                                                               // 485
  "eof-in-attribute-value-single-quote":                                                                           // 486
  "Unexpected end of file in attribute value (').",                                                                // 487
  "eof-in-attribute-value-no-quotes":                                                                              // 488
  "Unexpected end of file in attribute value.",                                                                    // 489
  "expected-dashes-or-doctype":                                                                                    // 490
  "Expected '--' or 'DOCTYPE'. Not found.",                                                                        // 491
  "incorrect-comment":                                                                                             // 492
  "Incorrect comment.",                                                                                            // 493
  "eof-in-comment":                                                                                                // 494
  "Unexpected end of file in comment.",                                                                            // 495
  "eof-in-comment-end-dash":                                                                                       // 496
  "Unexpected end of file in comment (-)",                                                                         // 497
  "unexpected-dash-after-double-dash-in-comment":                                                                  // 498
  "Unexpected '-' after '--' found in comment.",                                                                   // 499
  "eof-in-comment-double-dash":                                                                                    // 500
  "Unexpected end of file in comment (--).",                                                                       // 501
  "unexpected-char-in-comment":                                                                                    // 502
  "Unexpected character in comment found.",                                                                        // 503
  "need-space-after-doctype":                                                                                      // 504
  "No space after literal string 'DOCTYPE'.",                                                                      // 505
  "expected-doctype-name-but-got-right-bracket":                                                                   // 506
  "Unexpected > character. Expected DOCTYPE name.",                                                                // 507
  "expected-doctype-name-but-got-eof":                                                                             // 508
  "Unexpected end of file. Expected DOCTYPE name.",                                                                // 509
  "eof-in-doctype-name":                                                                                           // 510
  "Unexpected end of file in DOCTYPE name.",                                                                       // 511
  "eof-in-doctype":                                                                                                // 512
  "Unexpected end of file in DOCTYPE.",                                                                            // 513
  "expected-space-or-right-bracket-in-doctype":                                                                    // 514
  "Expected space or '>'. Got '%(data)'",                                                                          // 515
  "unexpected-end-of-doctype":                                                                                     // 516
  "Unexpected end of DOCTYPE.",                                                                                    // 517
  "unexpected-char-in-doctype":                                                                                    // 518
  "Unexpected character in DOCTYPE.",                                                                              // 519
  "eof-in-bogus-doctype":                                                                                          // 520
  "Unexpected end of file in bogus doctype.",                                                                      // 521
  "eof-in-innerhtml":                                                                                              // 522
  "Unexpected EOF in inner html mode.",                                                                            // 523
  "unexpected-doctype":                                                                                            // 524
  "Unexpected DOCTYPE. Ignored.",                                                                                  // 525
  "non-html-root":                                                                                                 // 526
  "html needs to be the first start tag.",                                                                         // 527
  "expected-doctype-but-got-eof":                                                                                  // 528
  "Unexpected End of file. Expected DOCTYPE.",                                                                     // 529
  "unknown-doctype":                                                                                               // 530
  "Erroneous DOCTYPE.",                                                                                            // 531
  "expected-doctype-but-got-chars":                                                                                // 532
  "Unexpected non-space characters. Expected DOCTYPE.",                                                            // 533
  "expected-doctype-but-got-start-tag":                                                                            // 534
  "Unexpected start tag (%(name)). Expected DOCTYPE.",                                                             // 535
  "expected-doctype-but-got-end-tag":                                                                              // 536
  "Unexpected end tag (%(name)). Expected DOCTYPE.",                                                               // 537
  "end-tag-after-implied-root":                                                                                    // 538
  "Unexpected end tag (%(name)) after the (implied) root element.",                                                // 539
  "expected-named-closing-tag-but-got-eof":                                                                        // 540
  "Unexpected end of file. Expected end tag (%(name)).",                                                           // 541
  "two-heads-are-not-better-than-one":                                                                             // 542
  "Unexpected start tag head in existing head. Ignored.",                                                          // 543
  "unexpected-end-tag":                                                                                            // 544
  "Unexpected end tag (%(name)). Ignored.",                                                                        // 545
  "unexpected-start-tag-out-of-my-head":                                                                           // 546
  "Unexpected start tag (%(name)) that can be in head. Moved.",                                                    // 547
  "unexpected-start-tag":                                                                                          // 548
  "Unexpected start tag (%(name)).",                                                                               // 549
  "missing-end-tag":                                                                                               // 550
  "Missing end tag (%(name)).",                                                                                    // 551
  "missing-end-tags":                                                                                              // 552
  "Missing end tags (%(name)).",                                                                                   // 553
  "unexpected-start-tag-implies-end-tag":                                                                          // 554
  "Unexpected start tag (%(startName)) " +                                                                         // 555
    "implies end tag (%(endName)).",                                                                               // 556
  "unexpected-start-tag-treated-as":                                                                               // 557
  "Unexpected start tag (%(originalName)). Treated as %(newName).",                                                // 558
  "deprecated-tag":                                                                                                // 559
  "Unexpected start tag %(name). Don't use it!",                                                                   // 560
  "unexpected-start-tag-ignored":                                                                                  // 561
  "Unexpected start tag %(name). Ignored.",                                                                        // 562
  "expected-one-end-tag-but-got-another":                                                                          // 563
  "Unexpected end tag (%(gotName). " +                                                                             // 564
    "Missing end tag (%(expectedName)).",                                                                          // 565
  "end-tag-too-early":                                                                                             // 566
  "End tag (%(name)) seen too early. Expected other end tag.",                                                     // 567
  "end-tag-too-early-named":                                                                                       // 568
  "Unexpected end tag (%(gotName)). Expected end tag (%(expectedName).",                                           // 569
  "end-tag-too-early-ignored":                                                                                     // 570
  "End tag (%(name)) seen too early. Ignored.",                                                                    // 571
  "adoption-agency-1.1":                                                                                           // 572
  "End tag (%(name) violates step 1, " +                                                                           // 573
    "paragraph 1 of the adoption agency algorithm.",                                                               // 574
  "adoption-agency-1.2":                                                                                           // 575
  "End tag (%(name) violates step 1, " +                                                                           // 576
    "paragraph 2 of the adoption agency algorithm.",                                                               // 577
  "adoption-agency-1.3":                                                                                           // 578
  "End tag (%(name) violates step 1, " +                                                                           // 579
    "paragraph 3 of the adoption agency algorithm.",                                                               // 580
  "unexpected-end-tag-treated-as":                                                                                 // 581
  "Unexpected end tag (%(originalName)). Treated as %(newName).",                                                  // 582
  "no-end-tag":                                                                                                    // 583
  "This element (%(name)) has no end tag.",                                                                        // 584
  "unexpected-implied-end-tag-in-table":                                                                           // 585
  "Unexpected implied end tag (%(name)) in the table phase.",                                                      // 586
  "unexpected-implied-end-tag-in-table-body":                                                                      // 587
  "Unexpected implied end tag (%(name)) in the table body phase.",                                                 // 588
  "unexpected-char-implies-table-voodoo":                                                                          // 589
  "Unexpected non-space characters in " +                                                                          // 590
    "table context caused voodoo mode.",                                                                           // 591
  "unpexted-hidden-input-in-table":                                                                                // 592
  "Unexpected input with type hidden in table context.",                                                           // 593
  "unexpected-start-tag-implies-table-voodoo":                                                                     // 594
  "Unexpected start tag (%(name)) in " +                                                                           // 595
    "table context caused voodoo mode.",                                                                           // 596
  "unexpected-end-tag-implies-table-voodoo":                                                                       // 597
  "Unexpected end tag (%(name)) in " +                                                                             // 598
    "table context caused voodoo mode.",                                                                           // 599
  "unexpected-cell-in-table-body":                                                                                 // 600
  "Unexpected table cell start tag (%(name)) " +                                                                   // 601
    "in the table body phase.",                                                                                    // 602
  "unexpected-cell-end-tag":                                                                                       // 603
  "Got table cell end tag (%(name)) " +                                                                            // 604
    "while required end tags are missing.",                                                                        // 605
  "unexpected-end-tag-in-table-body":                                                                              // 606
  "Unexpected end tag (%(name)) in the table body phase. Ignored.",                                                // 607
  "unexpected-implied-end-tag-in-table-row":                                                                       // 608
  "Unexpected implied end tag (%(name)) in the table row phase.",                                                  // 609
  "unexpected-end-tag-in-table-row":                                                                               // 610
  "Unexpected end tag (%(name)) in the table row phase. Ignored.",                                                 // 611
  "unexpected-select-in-select":                                                                                   // 612
  "Unexpected select start tag in the select phase " +                                                             // 613
    "treated as select end tag.",                                                                                  // 614
  "unexpected-input-in-select":                                                                                    // 615
  "Unexpected input start tag in the select phase.",                                                               // 616
  "unexpected-start-tag-in-select":                                                                                // 617
  "Unexpected start tag token (%(name)) in the select phase. " +                                                   // 618
    "Ignored.",                                                                                                    // 619
  "unexpected-end-tag-in-select":                                                                                  // 620
  "Unexpected end tag (%(name)) in the select phase. Ignored.",                                                    // 621
  "unexpected-table-element-start-tag-in-select-in-table":                                                         // 622
  "Unexpected table element start tag (%(name))s in the select in table phase.",                                   // 623
  "unexpected-table-element-end-tag-in-select-in-table":                                                           // 624
  "Unexpected table element end tag (%(name))s in the select in table phase.",                                     // 625
  "unexpected-char-after-body":                                                                                    // 626
  "Unexpected non-space characters in the after body phase.",                                                      // 627
  "unexpected-start-tag-after-body":                                                                               // 628
  "Unexpected start tag token (%(name))" +                                                                         // 629
    "in the after body phase.",                                                                                    // 630
  "unexpected-end-tag-after-body":                                                                                 // 631
  "Unexpected end tag token (%(name))" +                                                                           // 632
    " in the after body phase.",                                                                                   // 633
  "unexpected-char-in-frameset":                                                                                   // 634
  "Unepxected characters in the frameset phase. Characters ignored.",                                              // 635
  "unexpected-start-tag-in-frameset":                                                                              // 636
  "Unexpected start tag token (%(name))" +                                                                         // 637
    " in the frameset phase. Ignored.",                                                                            // 638
  "unexpected-frameset-in-frameset-innerhtml":                                                                     // 639
  "Unexpected end tag token (frameset " +                                                                          // 640
    "in the frameset phase (innerHTML).",                                                                          // 641
  "unexpected-end-tag-in-frameset":                                                                                // 642
  "Unexpected end tag token (%(name))" +                                                                           // 643
    " in the frameset phase. Ignored.",                                                                            // 644
  "unexpected-char-after-frameset":                                                                                // 645
  "Unexpected non-space characters in the " +                                                                      // 646
    "after frameset phase. Ignored.",                                                                              // 647
  "unexpected-start-tag-after-frameset":                                                                           // 648
  "Unexpected start tag (%(name))" +                                                                               // 649
    " in the after frameset phase. Ignored.",                                                                      // 650
  "unexpected-end-tag-after-frameset":                                                                             // 651
  "Unexpected end tag (%(name))" +                                                                                 // 652
    " in the after frameset phase. Ignored.",                                                                      // 653
  "expected-eof-but-got-char":                                                                                     // 654
  "Unexpected non-space characters. Expected end of file.",                                                        // 655
  "expected-eof-but-got-start-tag":                                                                                // 656
  "Unexpected start tag (%(name))" +                                                                               // 657
    ". Expected end of file.",                                                                                     // 658
  "expected-eof-but-got-end-tag":                                                                                  // 659
  "Unexpected end tag (%(name))" +                                                                                 // 660
    ". Expected end of file.",                                                                                     // 661
  "unexpected-end-table-in-caption":                                                                               // 662
  "Unexpected end table tag in caption. Generates implied end caption.",                                           // 663
  "end-html-in-innerhtml":                                                                                         // 664
  "Unexpected html end tag in inner html mode.",                                                                   // 665
  "expected-self-closing-tag":                                                                                     // 666
  "Expected a > after the /.",                                                                                     // 667
  "self-closing-end-tag":                                                                                          // 668
  "Self closing end tag.",                                                                                         // 669
  "eof-in-table":                                                                                                  // 670
  "Unexpected end of file. Expected table content.",                                                               // 671
  "html-in-foreign-content":                                                                                       // 672
  "HTML start tag \"%(name)\" in a foreign namespace context.",                                                    // 673
  "unexpected-start-tag-in-table":                                                                                 // 674
  "Unexpected %(name). Expected table content."                                                                    // 675
};                                                                                                                 // 676
                                                                                                                   // 677
HTML5.Models = {PCDATA: 'PCDATA', RCDATA: 'RCDATA', CDATA: 'CDATA', SCRIPT_CDATA: 'SCRIPT_CDATA'};                 // 678
                                                                                                                   // 679
HTML5.TAGMODES = {                                                                                                 // 680
  select: 'inSelect',                                                                                              // 681
  td: 'inCell',                                                                                                    // 682
  th: 'inCell',                                                                                                    // 683
  tr: 'inRow',                                                                                                     // 684
  tbody: 'inTableBody',                                                                                            // 685
  thead: 'inTableBody',                                                                                            // 686
  tfoot: 'inTableBody',                                                                                            // 687
  caption: 'inCaption',                                                                                            // 688
  colgroup: 'inColumnGroup',                                                                                       // 689
  table: 'inTable',                                                                                                // 690
  head: 'inBody',                                                                                                  // 691
  body: 'inBody',                                                                                                  // 692
  frameset: 'inFrameset'                                                                                           // 693
};                                                                                                                 // 694
                                                                                                                   // 695
HTML5.SVGAttributeMap = {                                                                                          // 696
  attributename:	'attributeName',                                                                                  // 697
  attributetype:	'attributeType',                                                                                  // 698
  basefrequency:	'baseFrequency',                                                                                  // 699
  baseprofile:	'baseProfile',                                                                                      // 700
  calcmode:	'calcMode',                                                                                            // 701
  clippathunits:	'clipPathUnits',                                                                                  // 702
  contentscripttype:	'contentScriptType',                                                                          // 703
  contentstyletype:	'contentStyleType',                                                                            // 704
  diffuseconstant:	'diffuseConstant',                                                                              // 705
  edgemode:	'edgeMode',                                                                                            // 706
  externalresourcesrequired:	'externalResourcesRequired',                                                          // 707
  filterres:	'filterRes',                                                                                          // 708
  filterunits:	'filterUnits',                                                                                      // 709
  glyphref:	'glyphRef',                                                                                            // 710
  gradienttransform:	'gradientTransform',                                                                          // 711
  gradientunits:	'gradientUnits',                                                                                  // 712
  kernelmatrix:	'kernelMatrix',                                                                                    // 713
  kernelunitlength:	'kernelUnitLength',                                                                            // 714
  keypoints:	'keyPoints',                                                                                          // 715
  keysplines:	'keySplines',                                                                                        // 716
  keytimes:	'keyTimes',                                                                                            // 717
  lengthadjust:	'lengthAdjust',                                                                                    // 718
  limitingconeangle:	'limitingConeAngle',                                                                          // 719
  markerheight:	'markerHeight',                                                                                    // 720
  markerunits:	'markerUnits',                                                                                      // 721
  markerwidth:	'markerWidth',                                                                                      // 722
  maskcontentunits:	'maskContentUnits',                                                                            // 723
  maskunits:	'maskUnits',                                                                                          // 724
  numoctaves:	'numOctaves',                                                                                        // 725
  pathlength:	'pathLength',                                                                                        // 726
  patterncontentunits:	'patternContentUnits',                                                                      // 727
  patterntransform:	'patternTransform',                                                                            // 728
  patternunits:	'patternUnits',                                                                                    // 729
  pointsatx:	'pointsAtX',                                                                                          // 730
  pointsaty:	'pointsAtY',                                                                                          // 731
  pointsatz:	'pointsAtZ',                                                                                          // 732
  preservealpha:	'preserveAlpha',                                                                                  // 733
  preserveaspectratio:	'preserveAspectRatio',                                                                      // 734
  primitiveunits:	'primitiveUnits',                                                                                // 735
  refx:	'refX',                                                                                                    // 736
  refy:	'refY',                                                                                                    // 737
  repeatcount:	'repeatCount',                                                                                      // 738
  repeatdur:	'repeatDur',                                                                                          // 739
  requiredextensions:	'requiredExtensions',                                                                        // 740
  requiredfeatures:	'requiredFeatures',                                                                            // 741
  specularconstant:	'specularConstant',                                                                            // 742
  specularexponent:	'specularExponent',                                                                            // 743
  spreadmethod:	'spreadMethod',                                                                                    // 744
  startoffset:	'startOffset',                                                                                      // 745
  stddeviation:	'stdDeviation',                                                                                    // 746
  stitchtiles:	'stitchTiles',                                                                                      // 747
  surfacescale:	'surfaceScale',                                                                                    // 748
  systemlanguage:	'systemLanguage',                                                                                // 749
  tablevalues:	'tableValues',                                                                                      // 750
  targetx:	'targetX',                                                                                              // 751
  targety:	'targetY',                                                                                              // 752
  textlength:	'textLength',                                                                                        // 753
  viewbox:	'viewBox',                                                                                              // 754
  viewtarget:	'viewTarget',                                                                                        // 755
  xchannelselector:	'xChannelSelector',                                                                            // 756
  ychannelselector:	'yChannelSelector',                                                                            // 757
  zoomandpan:	'zoomAndPan'                                                                                         // 758
};                                                                                                                 // 759
                                                                                                                   // 760
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/buffer.js                                                                              //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var Buffer = HTML5.Buffer = function Buffer() {                                                                    // 1
  this.data = '';                                                                                                  // 2
  this.start = 0;                                                                                                  // 3
  this.committed = 0;                                                                                              // 4
  this.eof = false;                                                                                                // 5
};                                                                                                                 // 6
                                                                                                                   // 7
Buffer.prototype = {                                                                                               // 8
  slice: function() {                                                                                              // 9
    if(this.start >= this.data.length) {                                                                           // 10
      if(!this.eof) throw HTML5.DRAIN;                                                                             // 11
      return HTML5.EOF;                                                                                            // 12
    }                                                                                                              // 13
    return this.data.slice(this.start, this.data.length);                                                          // 14
  },                                                                                                               // 15
  char: function() {                                                                                               // 16
    if(!this.eof && this.start >= this.data.length - 1) throw HTML5.DRAIN;                                         // 17
    if(this.start >= this.data.length) {                                                                           // 18
      return HTML5.EOF;                                                                                            // 19
    }                                                                                                              // 20
    return this.data[this.start++];                                                                                // 21
  },                                                                                                               // 22
  advance: function(amount) {                                                                                      // 23
    this.start += amount;                                                                                          // 24
    if(this.start >= this.data.length) {                                                                           // 25
      if(!this.eof) throw HTML5.DRAIN;                                                                             // 26
      return HTML5.EOF;                                                                                            // 27
    } else {                                                                                                       // 28
      if(this.committed > this.data.length / 2) {                                                                  // 29
	// Sliiiide                                                                                                       // 30
	this.data = this.data.slice(this.committed);                                                                      // 31
	this.start = this.start - this.committed;                                                                         // 32
	this.committed = 0;                                                                                               // 33
      }                                                                                                            // 34
    }                                                                                                              // 35
  },                                                                                                               // 36
  matchWhile: function(re) {                                                                                       // 37
    if(this.eof && this.start >= this.data.length ) return '';                                                     // 38
    var r = new RegExp("^"+re+"+");                                                                                // 39
    var m = r.exec(this.slice());                                                                                  // 40
    if(m) {                                                                                                        // 41
      if(!this.eof && m[0].length == this.data.length - this.start) throw HTML5.DRAIN;                             // 42
      this.advance(m[0].length);                                                                                   // 43
      return m[0];                                                                                                 // 44
    } else {                                                                                                       // 45
      return '';                                                                                                   // 46
    }                                                                                                              // 47
  },                                                                                                               // 48
  matchUntil: function(re) {                                                                                       // 49
    var m, s;                                                                                                      // 50
    s = this.slice();                                                                                              // 51
    if(s === HTML5.EOF) {                                                                                          // 52
      return '';                                                                                                   // 53
    } else if(m = new RegExp(re + (this.eof ? "|\0|$" : "|\0")).exec(this.slice())) {                              // 54
      var t = this.data.slice(this.start, this.start + m.index);                                                   // 55
      this.advance(m.index);                                                                                       // 56
      return t.toString();                                                                                         // 57
    } else {                                                                                                       // 58
      throw HTML5.DRAIN;                                                                                           // 59
    }                                                                                                              // 60
  },                                                                                                               // 61
  append: function(data) {                                                                                         // 62
    this.data += data;                                                                                             // 63
  },                                                                                                               // 64
  shift: function(n) {                                                                                             // 65
    if(!this.eof && this.start + n >= this.data.length) throw HTML5.DRAIN;                                         // 66
    if(this.eof && this.start >= this.data.length) return HTML5.EOF;                                               // 67
    var d = this.data.slice(this.start, this.start + n).toString();                                                // 68
    this.advance(Math.min(n, this.data.length - this.start));                                                      // 69
    return d;                                                                                                      // 70
  },                                                                                                               // 71
  peek: function(n) {                                                                                              // 72
    if(!this.eof && this.start + n >= this.data.length) throw HTML5.DRAIN;                                         // 73
    if(this.eof && this.start >= this.data.length) return HTML5.EOF;                                               // 74
    return this.data.slice(this.start, Math.min(this.start + n, this.data.length)).toString();                     // 75
  },                                                                                                               // 76
  length: function() {                                                                                             // 77
    return this.data.length - this.start - 1;                                                                      // 78
  },                                                                                                               // 79
  unget: function(d) {                                                                                             // 80
    if(d === HTML5.EOF) return;                                                                                    // 81
    this.start -= (d.length);                                                                                      // 82
  },                                                                                                               // 83
  undo: function() {                                                                                               // 84
    this.start = this.committed;                                                                                   // 85
  },                                                                                                               // 86
  commit: function() {                                                                                             // 87
    this.committed = this.start;                                                                                   // 88
  }                                                                                                                // 89
};                                                                                                                 // 90
                                                                                                                   // 91
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/events.js                                                                              //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
// dgreenspan's minimal implementation of events.EventEmitter                                                      // 1
                                                                                                                   // 2
toyevents = {                                                                                                      // 3
  EventEmitter: function EventEmitter() {                                                                          // 4
    this._listeners = {};                                                                                          // 5
  }                                                                                                                // 6
};                                                                                                                 // 7
                                                                                                                   // 8
toyevents.EventEmitter.prototype.addListener = function (type, f) {                                                // 9
  if (! f)                                                                                                         // 10
    return;                                                                                                        // 11
  this._listeners[type] = this._listeners[type] || [];                                                             // 12
  this._listeners[type].push(f);                                                                                   // 13
};                                                                                                                 // 14
                                                                                                                   // 15
toyevents.EventEmitter.prototype.emit = function (type, data) {                                                    // 16
  var funcs = this._listeners[type];                                                                               // 17
  if (! funcs)                                                                                                     // 18
    return;                                                                                                        // 19
                                                                                                                   // 20
  for (var i = 0, f; f = funcs[i]; i++)                                                                            // 21
    f(data);                                                                                                       // 22
};                                                                                                                 // 23
                                                                                                                   // 24
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/tokenizer.js                                                                           //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var Models = HTML5.Models;                                                                                         // 1
var events = toyevents;                                                                                            // 2
                                                                                                                   // 3
function keys(h) {                                                                                                 // 4
  var r = [];                                                                                                      // 5
  for(var k in h) {                                                                                                // 6
    r.push(k);                                                                                                     // 7
  }                                                                                                                // 8
  return r;                                                                                                        // 9
}                                                                                                                  // 10
                                                                                                                   // 11
var last = function (array) {                                                                                      // 12
  return array[array.length - 1];                                                                                  // 13
};                                                                                                                 // 14
                                                                                                                   // 15
HTML5.debug = function () {};                                                                                      // 16
                                                                                                                   // 17
var ENTITY_KEYS = keys(HTML5.ENTITIES);                                                                            // 18
                                                                                                                   // 19
var t = HTML5.Tokenizer = function HTML5Tokenizer(input, document, tree) {                                         // 20
  var state;                                                                                                       // 21
  var buffer = new HTML5.Buffer();                                                                                 // 22
  var escapeFlag = false;                                                                                          // 23
  var lastFourChars = '';                                                                                          // 24
  var current_token = null;                                                                                        // 25
  var script_buffer = null;                                                                                        // 26
  var content_model = Models.PCDATA;                                                                               // 27
  var source;                                                                                                      // 28
                                                                                                                   // 29
  tree = tree || {open_elements: []};                                                                              // 30
                                                                                                                   // 31
  function data_state(buffer) {                                                                                    // 32
    var c = buffer.char();                                                                                         // 33
    if (c !== HTML5.EOF && (content_model == Models.CDATA || content_model == Models.RCDATA || content_model == Models.SCRIPT_CDATA)) {
      lastFourChars += c;                                                                                          // 35
      if (lastFourChars.length >= 4) {                                                                             // 36
	lastFourChars = lastFourChars.substr(-4);                                                                         // 37
      }                                                                                                            // 38
    }                                                                                                              // 39
                                                                                                                   // 40
    if (content_model == Models.SCRIPT_CDATA) {                                                                    // 41
      if (script_buffer === null) {                                                                                // 42
	script_buffer = '';                                                                                               // 43
      }                                                                                                            // 44
    }                                                                                                              // 45
                                                                                                                   // 46
    if (c === HTML5.EOF) {                                                                                         // 47
      emitToken(HTML5.EOF_TOK);                                                                                    // 48
      buffer.commit();                                                                                             // 49
      return false;                                                                                                // 50
    } else if (c === '\0' && (content_model == Models.SCRIPT_CDATA || content_model == Models.PLAINTEXT || content_model == Models.RAWTEXT || content_model == Models.RCDATA)) {
      emitToken({type: 'Characters', data: "\ufffd"});                                                             // 52
      buffer.commit();                                                                                             // 53
    } else if (c == '&' && (content_model == Models.PCDATA || content_model == Models.RCDATA) && !escapeFlag) {    // 54
      newState(entity_data_state);                                                                                 // 55
    } else if (c == '-' && (content_model == Models.CDATA || content_model == Models.RCDATA || content_model == Models.SCRIPT_CDATA) && !escapeFlag && lastFourChars == '<!--') {
      escapeFlag = true;                                                                                           // 57
      emitToken({type: 'Characters', data: c});                                                                    // 58
      buffer.commit();                                                                                             // 59
    } else if (c == '<' && !escapeFlag && (content_model == Models.PCDATA || content_model == Models.RCDATA || content_model == Models.CDATA || content_model == Models.SCRIPT_CDATA)) {
      newState(tag_open_state);                                                                                    // 61
    } else if (c == '>' && escapeFlag && (content_model == Models.CDATA || content_model == Models.RCDATA || content_model == Models.SCRIPT_CDATA) && lastFourChars.match(/-->$/)) {
      escapeFlag = false;                                                                                          // 63
      emitToken({type: 'Characters', data: c});                                                                    // 64
      buffer.commit();                                                                                             // 65
    } else if (HTML5.SPACE_CHARACTERS_R.test(c)) {                                                                 // 66
      emitToken({type: 'SpaceCharacters', data: c + buffer.matchWhile(HTML5.SPACE_CHARACTERS)});                   // 67
      buffer.commit();                                                                                             // 68
    } else {                                                                                                       // 69
      var o = buffer.matchUntil("[&<>-]");                                                                         // 70
      if (o !== HTML5.EOF) {                                                                                       // 71
	c = c + o;                                                                                                        // 72
      }                                                                                                            // 73
      emitToken({type: 'Characters', data: c});                                                                    // 74
      lastFourChars += c;                                                                                          // 75
      lastFourChars = lastFourChars.slice(-4);                                                                     // 76
      buffer.commit();                                                                                             // 77
    }                                                                                                              // 78
    return true;                                                                                                   // 79
  }                                                                                                                // 80
                                                                                                                   // 81
  var entity_data_state = function entity_data_state(buffer) {                                                     // 82
    var entity = consume_entity(buffer);                                                                           // 83
    if (entity) {                                                                                                  // 84
      emitToken({type: 'Characters', data: entity});                                                               // 85
    } else {                                                                                                       // 86
      emitToken({type: 'Characters', data: '&'});                                                                  // 87
    }                                                                                                              // 88
    newState(data_state);                                                                                          // 89
    return true;                                                                                                   // 90
  };                                                                                                               // 91
                                                                                                                   // 92
  this.tokenize = function() {                                                                                     // 93
    if (this.pump) this.pump();                                                                                    // 94
  };                                                                                                               // 95
                                                                                                                   // 96
  var emitToken = function emitToken(tok) {                                                                        // 97
    tok = normalize_token(tok);                                                                                    // 98
    if (content_model == Models.SCRIPT_CDATA && (tok.type == 'Characters' || tok.type == 'SpaceCharacters') && !buffer.eof) {
      HTML5.debug('tokenizer.addScriptData', tok);                                                                 // 100
      script_buffer += tok.data;                                                                                   // 101
    } else {                                                                                                       // 102
      HTML5.debug('tokenizer.token', tok);                                                                         // 103
      this.emit('token', tok);                                                                                     // 104
    }                                                                                                              // 105
  }.bind(this);                                                                                                    // 106
                                                                                                                   // 107
  function consume_entity(buffer, from_attr) {                                                                     // 108
    var char = null;                                                                                               // 109
    var chars = buffer.char();                                                                                     // 110
    var c;                                                                                                         // 111
    if (chars === HTML5.EOF) return false;                                                                         // 112
    if (chars.match(HTML5.SPACE_CHARACTERS) || chars == '<' || chars == '&') {                                     // 113
      buffer.unget(chars);                                                                                         // 114
    } else if (chars[0] == '#') { // Maybe a numeric entity                                                        // 115
      c = buffer.shift(2);                                                                                         // 116
      if (c === HTML5.EOF) {                                                                                       // 117
	buffer.unget(chars);                                                                                              // 118
	return false;                                                                                                     // 119
      }                                                                                                            // 120
      chars += c;                                                                                                  // 121
      if (chars[1] && chars[1].toLowerCase() == 'x' && HTML5.HEX_DIGITS_R.test(chars[2])) {                        // 122
	// Hex entity                                                                                                     // 123
	buffer.unget(chars[2]);                                                                                           // 124
	char = consume_numeric_entity(buffer, true);                                                                      // 125
      } else if (chars[1] && HTML5.DIGITS_R.test(chars[1])) {                                                      // 126
	// Decimal entity                                                                                                 // 127
	buffer.unget(chars.slice(1));                                                                                     // 128
	char = consume_numeric_entity(buffer, false);                                                                     // 129
      } else {                                                                                                     // 130
	// Not numeric                                                                                                    // 131
	buffer.unget(chars);                                                                                              // 132
	parse_error("expected-numeric-entity");                                                                           // 133
      }                                                                                                            // 134
    } else {                                                                                                       // 135
      var filteredEntityList = ENTITY_KEYS.filter(function(e) {                                                    // 136
	return e[0] == chars[0];                                                                                          // 137
      });                                                                                                          // 138
      var entityName = null;                                                                                       // 139
      var matches = function(e) {                                                                                  // 140
	return e.indexOf(chars) === 0;                                                                                    // 141
      };                                                                                                           // 142
      while(true) {                                                                                                // 143
	if (filteredEntityList.some(matches)) {                                                                           // 144
	  filteredEntityList = filteredEntityList.filter(matches);                                                        // 145
	  c = buffer.char();                                                                                              // 146
	  if (c !== HTML5.EOF) {                                                                                          // 147
	    chars += c;                                                                                                   // 148
	  } else {                                                                                                        // 149
	    break;                                                                                                        // 150
	  }                                                                                                               // 151
	} else {                                                                                                          // 152
	  break;                                                                                                          // 153
	}                                                                                                                 // 154
                                                                                                                   // 155
	if (HTML5.ENTITIES[chars]) {                                                                                      // 156
	  entityName = chars;                                                                                             // 157
	  if (entityName[entityName.length - 1] == ';') break;                                                            // 158
	}                                                                                                                 // 159
      }                                                                                                            // 160
                                                                                                                   // 161
      if (entityName) {                                                                                            // 162
	char = HTML5.ENTITIES[entityName];                                                                                // 163
                                                                                                                   // 164
	if (entityName[entityName.length - 1] != ';' && this.from_attribute && (HTML5.ASCII_LETTERS_R.test(chars.substr(entityName.length, 1) || HTML5.DIGITS.test(chars.substr(entityName.length, 1))))) {
	  buffer.unget(chars);                                                                                            // 166
	  char = '&';                                                                                                     // 167
	} else {                                                                                                          // 168
	  buffer.unget(chars.slice(entityName.length));                                                                   // 169
	}                                                                                                                 // 170
      } else {                                                                                                     // 171
	parse_error("expected-named-entity");                                                                             // 172
	buffer.unget(chars);                                                                                              // 173
      }                                                                                                            // 174
    }                                                                                                              // 175
                                                                                                                   // 176
    return char;                                                                                                   // 177
  }                                                                                                                // 178
                                                                                                                   // 179
  function replaceEntityNumbers(c) {                                                                               // 180
    switch(c) {                                                                                                    // 181
    case 0x00: return 0xFFFD; // REPLACEMENT CHARACTER                                                             // 182
    case 0x13: return 0x0010; // Carriage return                                                                   // 183
    case 0x80: return 0x20AC; // EURO SIGN                                                                         // 184
    case 0x81: return 0x0081; // <control>                                                                         // 185
    case 0x82: return 0x201A; // SINGLE LOW-9 QUOTATION MARK                                                       // 186
    case 0x83: return 0x0192; // LATIN SMALL LETTER F WITH HOOK                                                    // 187
    case 0x84: return 0x201E; // DOUBLE LOW-9 QUOTATION MARK                                                       // 188
    case 0x85: return 0x2026; // HORIZONTAL ELLIPSIS                                                               // 189
    case 0x86: return 0x2020; // DAGGER                                                                            // 190
    case 0x87: return 0x2021; // DOUBLE DAGGER                                                                     // 191
    case 0x88: return 0x02C6; // MODIFIER LETTER CIRCUMFLEX ACCENT                                                 // 192
    case 0x89: return 0x2030; // PER MILLE SIGN                                                                    // 193
    case 0x8A: return 0x0160; // LATIN CAPITAL LETTER S WITH CARON                                                 // 194
    case 0x8B: return 0x2039; // SINGLE LEFT-POINTING ANGLE QUOTATION MARK                                         // 195
    case 0x8C: return 0x0152; // LATIN CAPITAL LIGATURE OE                                                         // 196
    case 0x8D: return 0x008D; // <control>                                                                         // 197
    case 0x8E: return 0x017D; // LATIN CAPITAL LETTER Z WITH CARON                                                 // 198
    case 0x8F: return 0x008F; // <control>                                                                         // 199
    case 0x90: return 0x0090; // <control>                                                                         // 200
    case 0x91: return 0x2018; // LEFT SINGLE QUOTATION MARK                                                        // 201
    case 0x92: return 0x2019; // RIGHT SINGLE QUOTATION MARK                                                       // 202
    case 0x93: return 0x201C; // LEFT DOUBLE QUOTATION MARK                                                        // 203
    case 0x94: return 0x201D; // RIGHT DOUBLE QUOTATION MARK                                                       // 204
    case 0x95: return 0x2022; // BULLET                                                                            // 205
    case 0x96: return 0x2013; // EN DASH                                                                           // 206
    case 0x97: return 0x2014; // EM DASH                                                                           // 207
    case 0x98: return 0x02DC; // SMALL TILDE                                                                       // 208
    case 0x99: return 0x2122; // TRADE MARK SIGN                                                                   // 209
    case 0x9A: return 0x0161; // LATIN SMALL LETTER S WITH CARON                                                   // 210
    case 0x9B: return 0x203A; // SINGLE RIGHT-POINTING ANGLE QUOTATION MARK                                        // 211
    case 0x9C: return 0x0153; // LATIN SMALL LIGATURE OE                                                           // 212
    case 0x9D: return 0x009D; // <control>                                                                         // 213
    case 0x9E: return 0x017E; // LATIN SMALL LETTER Z WITH CARON                                                   // 214
    case 0x9F: return 0x0178; // LATIN CAPITAL LETTER Y WITH DIAERESIS                                             // 215
    default:                                                                                                       // 216
      if ((c >= 0xD800 && c <= 0xDFFF) || c >= 0x10FFFF) { /// @todo. The spec says > 0x10FFFF, not >=. Section 8.2.4.69.
	return 0xFFFD;                                                                                                    // 218
      } else if ((c >= 0x0001 && c <= 0x0008) || (c >= 0x000E && c <= 0x001F) ||                                   // 219
		 (c >= 0x007F && c <= 0x009F) || (c >= 0xFDD0 && c <= 0xFDEF) ||                                                 // 220
		 c == 0x000B || c == 0xFFFE || c == 0x1FFFE || c == 0x2FFFFE ||                                                  // 221
		 c == 0x2FFFF || c == 0x3FFFE || c == 0x3FFFF || c == 0x4FFFE ||                                                 // 222
		 c == 0x4FFFF || c == 0x5FFFE || c == 0x5FFFF || c == 0x6FFFE ||                                                 // 223
		 c == 0x6FFFF || c == 0x7FFFE || c == 0x7FFFF || c == 0x8FFFE ||                                                 // 224
		 c == 0x8FFFF || c == 0x9FFFE || c == 0x9FFFF || c == 0xAFFFE ||                                                 // 225
		 c == 0xAFFFF || c == 0xBFFFE || c == 0xBFFFF || c == 0xCFFFE ||                                                 // 226
		 c == 0xCFFFF || c == 0xDFFFE || c == 0xDFFFF || c == 0xEFFFE ||                                                 // 227
		 c == 0xEFFFF || c == 0xFFFFE || c == 0xFFFFF || c == 0x10FFFE ||                                                // 228
		 c == 0x10FFFF) {                                                                                                // 229
	return c;                                                                                                         // 230
      }                                                                                                            // 231
    }                                                                                                              // 232
  }                                                                                                                // 233
                                                                                                                   // 234
  function consume_numeric_entity(buffer, hex) {                                                                   // 235
    var allowed, radix;                                                                                            // 236
    if (hex) {                                                                                                     // 237
      allowed = HTML5.HEX_DIGITS_R;                                                                                // 238
      radix = 16;                                                                                                  // 239
    } else {                                                                                                       // 240
      allowed = HTML5.DIGITS_R;                                                                                    // 241
      radix = 10;                                                                                                  // 242
    }                                                                                                              // 243
                                                                                                                   // 244
    var chars = '';                                                                                                // 245
                                                                                                                   // 246
    var c = buffer.char();                                                                                         // 247
    while(c !== HTML5.EOF && allowed.test(c)) {                                                                    // 248
      chars = chars + c;                                                                                           // 249
      c = buffer.char();                                                                                           // 250
    }                                                                                                              // 251
                                                                                                                   // 252
    var charAsInt = parseInt(chars, radix);                                                                        // 253
                                                                                                                   // 254
    var replacement = replaceEntityNumbers(charAsInt);                                                             // 255
    if (replacement) {                                                                                             // 256
      parse_error("invalid-numeric-entity-replaced", {old: charAsInt, 'new': replacement});                        // 257
      charAsInt = replacement;                                                                                     // 258
    }                                                                                                              // 259
                                                                                                                   // 260
    var char = String.fromCharCode(charAsInt);                                                                     // 261
    /*if (charAsInt <= 0x10FFFF && !(charAsInt >= 0xD800 && charAsInt <= 0xDFFF)) {                                // 262
     } else {                                                                                                      // 263
     char = String.fromCharCode(0xFFFD);                                                                           // 264
     parse_error("cant-convert-numeric-entity");                                                                   // 265
     } */                                                                                                          // 266
                                                                                                                   // 267
    if (c !== ';') {                                                                                               // 268
      parse_error("numeric-entity-without-semicolon");                                                             // 269
      buffer.unget(c);                                                                                             // 270
    }                                                                                                              // 271
                                                                                                                   // 272
    return char;                                                                                                   // 273
  }                                                                                                                // 274
                                                                                                                   // 275
  function process_entity_in_attribute(buffer) {                                                                   // 276
    var entity = consume_entity(buffer);                                                                           // 277
    if (entity) {                                                                                                  // 278
      last(current_token.data).nodeValue += entity;                                                                // 279
    } else {                                                                                                       // 280
      last(current_token.data).nodeValue += '&';                                                                   // 281
    }                                                                                                              // 282
  }                                                                                                                // 283
                                                                                                                   // 284
  function process_solidus_in_tag(buffer) {                                                                        // 285
    var data = buffer.peek(1);                                                                                     // 286
    if (current_token.type == 'StartTag' && data == '>') {                                                         // 287
      current_token.type = 'EmptyTag';                                                                             // 288
      return true;                                                                                                 // 289
    } else {                                                                                                       // 290
      parse_error("incorrectly-placed-solidus");                                                                   // 291
      return false;                                                                                                // 292
    }                                                                                                              // 293
  }                                                                                                                // 294
                                                                                                                   // 295
  function tag_open_state(buffer) {                                                                                // 296
    var data = buffer.char();                                                                                      // 297
    if (content_model == Models.PCDATA) {                                                                          // 298
      if (data === HTML5.EOF) {                                                                                    // 299
	parse_error("bare-less-than-sign-at-eof");                                                                        // 300
	emitToken({type: 'Characters', data: '<'});                                                                       // 301
	newState(data_state);                                                                                             // 302
      } else if (data !== HTML5.EOF && HTML5.ASCII_LETTERS_R.test(data)) {                                         // 303
	current_token = {type: 'StartTag', name: data, data: []};                                                         // 304
	newState(tag_name_state);                                                                                         // 305
      } else if (data == '!') {                                                                                    // 306
	newState(markup_declaration_open_state);                                                                          // 307
      } else if (data == '/') {                                                                                    // 308
	newState(close_tag_open_state);                                                                                   // 309
      } else if (data == '>') {                                                                                    // 310
	// XXX In theory it could be something besides a tag name. But                                                    // 311
	// do we really care?                                                                                             // 312
	parse_error("expected-tag-name-but-got-right-bracket");                                                           // 313
	emitToken({type: 'Characters', data: "<>"});                                                                      // 314
	newState(data_state);                                                                                             // 315
      } else if (data == '?') {                                                                                    // 316
	// XXX In theory it could be something besides a tag name. But                                                    // 317
	// do we really care?                                                                                             // 318
	parse_error("expected-tag-name-but-got-question-mark");                                                           // 319
	buffer.unget(data);                                                                                               // 320
	newState(bogus_comment_state);                                                                                    // 321
      } else {                                                                                                     // 322
	// XXX                                                                                                            // 323
	parse_error("expected-tag-name");                                                                                 // 324
	emitToken({type: 'Characters', data: "<"});                                                                       // 325
	buffer.unget(data);                                                                                               // 326
	newState(data_state);                                                                                             // 327
      }                                                                                                            // 328
    } else {                                                                                                       // 329
      // We know the content model flag is set to either RCDATA or CDATA or SCRIPT_CDATA                           // 330
      // now because this state can never be entered with the PLAINTEXT                                            // 331
      // flag.                                                                                                     // 332
      if (data === '/') {                                                                                          // 333
	newState(close_tag_open_state);                                                                                   // 334
      } else {                                                                                                     // 335
	emitToken({type: 'Characters', data: "<"});                                                                       // 336
	buffer.unget(data);                                                                                               // 337
	newState(data_state);                                                                                             // 338
      }                                                                                                            // 339
    }                                                                                                              // 340
    return true;                                                                                                   // 341
  }                                                                                                                // 342
                                                                                                                   // 343
  function close_tag_open_state(buffer) {                                                                          // 344
    if (content_model == Models.RCDATA || content_model == Models.CDATA || content_model == Models.SCRIPT_CDATA) { // 345
      var chars = '';                                                                                              // 346
      if (current_token) {                                                                                         // 347
	for(var i = 0; i <= current_token.name.length; i++) {                                                             // 348
	  var c = buffer.char();                                                                                          // 349
	  if (c === HTML5.EOF) break;                                                                                     // 350
	  chars += c;                                                                                                     // 351
	}                                                                                                                 // 352
	buffer.unget(chars);                                                                                              // 353
      }                                                                                                            // 354
                                                                                                                   // 355
      if (current_token &&                                                                                         // 356
	  current_token.name.toLowerCase() == chars.slice(0, current_token.name.length).toLowerCase() &&                  // 357
	  (chars.length > current_token.name.length ? new RegExp('[' + HTML5.SPACE_CHARACTERS_IN + '></\0]').test(chars.substr(-1)) : true)
	 ) {                                                                                                              // 359
	   content_model = Models.PCDATA;                                                                                 // 360
	 } else {                                                                                                         // 361
	   emitToken({type: 'Characters', data: '</'});                                                                   // 362
	   newState(data_state);                                                                                          // 363
	   return true;                                                                                                   // 364
	 }                                                                                                                // 365
    }                                                                                                              // 366
                                                                                                                   // 367
    var data = buffer.char();                                                                                      // 368
    if (data === HTML5.EOF) {                                                                                      // 369
      parse_error("expected-closing-tag-but-got-eof");                                                             // 370
      emitToken({type: 'Characters', data: '</'});                                                                 // 371
      buffer.unget(data);                                                                                          // 372
      newState(data_state);                                                                                        // 373
    } else if (HTML5.ASCII_LETTERS_R.test(data)) {                                                                 // 374
      current_token = {type: 'EndTag', name: data, data: []};                                                      // 375
      newState(tag_name_state);                                                                                    // 376
    } else if (data == '>') {                                                                                      // 377
      parse_error("expected-closing-tag-but-got-right-bracket");                                                   // 378
      newState(data_state);                                                                                        // 379
    } else {                                                                                                       // 380
      parse_error("expected-closing-tag-but-got-char", {data: data}); // param 1 is datavars:                      // 381
      buffer.unget(data);                                                                                          // 382
      newState(bogus_comment_state);                                                                               // 383
    }                                                                                                              // 384
    return true;                                                                                                   // 385
  }                                                                                                                // 386
                                                                                                                   // 387
  function tag_name_state(buffer) {                                                                                // 388
    var data = buffer.char();                                                                                      // 389
    if (data === HTML5.EOF) {                                                                                      // 390
      parse_error('eof-in-tag-name');                                                                              // 391
      emit_current_token();                                                                                        // 392
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 393
      newState(before_attribute_name_state);                                                                       // 394
    } else if (HTML5.ASCII_LETTERS_R.test(data)) {                                                                 // 395
      var c = buffer.matchWhile(HTML5.ASCII_LETTERS);                                                              // 396
      if (c !== HTML5.EOF) {                                                                                       // 397
	current_token.name += data + c;                                                                                   // 398
      } else {                                                                                                     // 399
	current_token.name += data;                                                                                       // 400
	buffer.unget(c);                                                                                                  // 401
	newState(data_state);                                                                                             // 402
      }                                                                                                            // 403
    } else if (data == '>') {                                                                                      // 404
      emit_current_token();                                                                                        // 405
    } else if (data == '/') {                                                                                      // 406
      process_solidus_in_tag(buffer);                                                                              // 407
      newState(self_closing_tag_state);                                                                            // 408
    } else {                                                                                                       // 409
      current_token.name += data;                                                                                  // 410
    }                                                                                                              // 411
    buffer.commit();                                                                                               // 412
                                                                                                                   // 413
    return true;                                                                                                   // 414
  }                                                                                                                // 415
                                                                                                                   // 416
  function before_attribute_name_state(buffer) {                                                                   // 417
    var data = buffer.shift(1);                                                                                    // 418
    if (data === HTML5.EOF) {                                                                                      // 419
      parse_error("expected-attribute-name-but-got-eof");                                                          // 420
      emit_current_token();                                                                                        // 421
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 422
      buffer.matchWhile(HTML5.SPACE_CHARACTERS);                                                                   // 423
    } else if (HTML5.ASCII_LETTERS_R.test(data)) {                                                                 // 424
      current_token.data.push({nodeName: data, nodeValue: ""});                                                    // 425
      newState(attribute_name_state);                                                                              // 426
    } else if (data == '>') {                                                                                      // 427
      emit_current_token();                                                                                        // 428
    } else if (data == '/') {                                                                                      // 429
      newState(self_closing_tag_state);                                                                            // 430
    } else if (data == "'" || data == '"' || data == '=') {                                                        // 431
      parse_error("invalid-character-in-attribute-name");                                                          // 432
      current_token.data.push({nodeName: data, nodeValue: ""});                                                    // 433
      newState(attribute_name_state);                                                                              // 434
    } else {                                                                                                       // 435
      current_token.data.push({nodeName: data, nodeValue: ""});                                                    // 436
      newState(attribute_name_state);                                                                              // 437
    }                                                                                                              // 438
    return true;                                                                                                   // 439
  }                                                                                                                // 440
                                                                                                                   // 441
  function attribute_name_state(buffer) {                                                                          // 442
    var data = buffer.shift(1);                                                                                    // 443
    var leavingThisState = true;                                                                                   // 444
    var emitToken = false;                                                                                         // 445
    if (data === HTML5.EOF) {                                                                                      // 446
      parse_error("eof-in-attribute-name");                                                                        // 447
      newState(data_state);                                                                                        // 448
      emitToken = true;                                                                                            // 449
    } else if (data == '=') {                                                                                      // 450
      newState(before_attribute_value_state);                                                                      // 451
    } else if (HTML5.ASCII_LETTERS_R.test(data)) {                                                                 // 452
      last(current_token.data).nodeName += data + buffer.matchWhile(HTML5.ASCII_LETTERS);                          // 453
      leavingThisState = false;                                                                                    // 454
    } else if (data == '>') {                                                                                      // 455
      // XXX If we emit here the attributes are converted to a dict                                                // 456
      // without being checked and when the code below runs we error                                               // 457
      // because data is a dict not a list                                                                         // 458
      emitToken = true;                                                                                            // 459
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 460
      newState(after_attribute_name_state);                                                                        // 461
    } else if (data == '/') {                                                                                      // 462
      if (!process_solidus_in_tag(buffer)) {                                                                       // 463
	newState(before_attribute_name_state);                                                                            // 464
      }                                                                                                            // 465
    } else if (data == "'" || data == '"') {                                                                       // 466
      parse_error("invalid-character-in-attribute-name");                                                          // 467
      last(current_token.data).nodeName += data;                                                                   // 468
      leavingThisState = false;                                                                                    // 469
    } else {                                                                                                       // 470
      last(current_token.data).nodeName += data;                                                                   // 471
      leavingThisState = false;                                                                                    // 472
    }                                                                                                              // 473
                                                                                                                   // 474
    if (leavingThisState) {                                                                                        // 475
      // Attributes are not dropped at this stage. That happens when the                                           // 476
      // start tag token is emitted so values can still be safely appended                                         // 477
      // to attributes, but we do want to report the parse error in time.                                          // 478
      if (this.lowercase_attr_name) {                                                                              // 479
	last(current_token.data).nodeName = last(current_token.data).nodeName.toLowerCase();                              // 480
      }                                                                                                            // 481
      for (var k in current_token.data.slice(0, -1)) {                                                             // 482
	// FIXME this is a fucking mess.                                                                                  // 483
	if (current_token.data.slice(-1)[0] == current_token.data.slice(0, -1)[k].name) {                                 // 484
	  parse_error("duplicate-attribute");                                                                             // 485
	  break; // Don't emit more than one of these errors                                                              // 486
	}                                                                                                                 // 487
      }                                                                                                            // 488
      if (emitToken) emit_current_token();                                                                         // 489
    } else {                                                                                                       // 490
      buffer.commit();                                                                                             // 491
    }                                                                                                              // 492
    return true;                                                                                                   // 493
  }                                                                                                                // 494
                                                                                                                   // 495
  function after_attribute_name_state(buffer) {                                                                    // 496
    var data = buffer.shift(1);                                                                                    // 497
    if (data === HTML5.EOF) {                                                                                      // 498
      parse_error("expected-end-of-tag-but-got-eof");                                                              // 499
      emit_current_token();                                                                                        // 500
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 501
      buffer.matchWhile(HTML5.SPACE_CHARACTERS);                                                                   // 502
    } else if (data == '=') {                                                                                      // 503
      newState(before_attribute_value_state);                                                                      // 504
    } else if (data == '>') {                                                                                      // 505
      emit_current_token();                                                                                        // 506
    } else if (HTML5.ASCII_LETTERS_R.test(data)) {                                                                 // 507
      current_token.data.push({nodeName: data, nodeValue: ""});                                                    // 508
      newState(attribute_name_state);                                                                              // 509
    } else if (data == '/') {                                                                                      // 510
      newState(self_closing_tag_state);                                                                            // 511
    } else {                                                                                                       // 512
      current_token.data.push({nodeName: data, nodeValue: ""});                                                    // 513
      newState(attribute_name_state);                                                                              // 514
    }                                                                                                              // 515
    return true;                                                                                                   // 516
  }                                                                                                                // 517
                                                                                                                   // 518
  function before_attribute_value_state(buffer) {                                                                  // 519
    var data = buffer.shift(1);                                                                                    // 520
    if (data === HTML5.EOF) {                                                                                      // 521
      parse_error("expected-attribute-value-but-got-eof");                                                         // 522
      emit_current_token();                                                                                        // 523
      newState(attribute_value_unquoted_state);                                                                    // 524
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 525
      buffer.matchWhile(HTML5.SPACE_CHARACTERS);                                                                   // 526
    } else if (data == '"') {                                                                                      // 527
      newState(attribute_value_double_quoted_state);                                                               // 528
    } else if (data == '&') {                                                                                      // 529
      newState(attribute_value_unquoted_state);                                                                    // 530
      buffer.unget(data);                                                                                          // 531
    } else if (data == "'") {                                                                                      // 532
      newState(attribute_value_single_quoted_state);                                                               // 533
    } else if (data == '>') {                                                                                      // 534
      emit_current_token();                                                                                        // 535
    } else if (data == '=') {                                                                                      // 536
      parse_error("equals-in-unquoted-attribute-value");                                                           // 537
      last(current_token.data).nodeValue += data;                                                                  // 538
      newState(attribute_value_unquoted_state);                                                                    // 539
    } else {                                                                                                       // 540
      last(current_token.data).nodeValue += data;                                                                  // 541
      newState(attribute_value_unquoted_state);                                                                    // 542
    }                                                                                                              // 543
                                                                                                                   // 544
    return true;                                                                                                   // 545
  }                                                                                                                // 546
                                                                                                                   // 547
  function attribute_value_double_quoted_state(buffer) {                                                           // 548
    var data = buffer.shift(1);                                                                                    // 549
    if (data === HTML5.EOF) {                                                                                      // 550
      parse_error("eof-in-attribute-value-double-quote");                                                          // 551
      newState(data_state);                                                                                        // 552
    } else if (data == '"') {                                                                                      // 553
      newState(after_attribute_value_state);                                                                       // 554
    } else if (data == '&') {                                                                                      // 555
      process_entity_in_attribute(buffer);                                                                         // 556
    } else {                                                                                                       // 557
      var s = buffer.matchUntil('["&]');                                                                           // 558
      if (s !== HTML5.EOF) data = data + s;                                                                        // 559
      last(current_token.data).nodeValue += data;                                                                  // 560
    }                                                                                                              // 561
    return true;                                                                                                   // 562
  }                                                                                                                // 563
                                                                                                                   // 564
  function attribute_value_single_quoted_state(buffer) {                                                           // 565
    var data = buffer.shift(1);                                                                                    // 566
    if (data === HTML5.EOF) {                                                                                      // 567
      parse_error("eof-in-attribute-value-single-quote");                                                          // 568
      emit_current_token();                                                                                        // 569
    } else if (data == "'") {                                                                                      // 570
      newState(after_attribute_value_state);                                                                       // 571
    } else if (data == '&') {                                                                                      // 572
      process_entity_in_attribute(buffer);                                                                         // 573
    } else {                                                                                                       // 574
      last(current_token.data).nodeValue += data + buffer.matchUntil("['&]");                                      // 575
    }                                                                                                              // 576
    return true;                                                                                                   // 577
  }                                                                                                                // 578
                                                                                                                   // 579
  function attribute_value_unquoted_state(buffer) {                                                                // 580
    var data = buffer.shift(1);                                                                                    // 581
    if (data === HTML5.EOF) {                                                                                      // 582
      parse_error("eof-in-attribute-value-no-quotes");                                                             // 583
      buffer.commit();                                                                                             // 584
      emit_current_token();                                                                                        // 585
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 586
      newState(before_attribute_name_state);                                                                       // 587
    } else if (data == '&') {                                                                                      // 588
      process_entity_in_attribute(buffer);                                                                         // 589
    } else if (data == '>') {                                                                                      // 590
      emit_current_token();                                                                                        // 591
    } else if (data == '"' || data == "'" || data == '=') {                                                        // 592
      parse_error("unexpected-character-in-unquoted-attribute-value");                                             // 593
      last(current_token.data).nodeValue += data;                                                                  // 594
    } else {                                                                                                       // 595
      var o = buffer.matchUntil("["+ HTML5.SPACE_CHARACTERS_IN + '&<>' +"]");                                      // 596
      if (o === HTML5.EOF) {                                                                                       // 597
	parse_error("eof-in-attribute-value-no-quotes");                                                                  // 598
	emit_current_token();                                                                                             // 599
      }                                                                                                            // 600
      // Commit here since this state is re-enterable and its outcome won't change with more data.                 // 601
      buffer.commit();                                                                                             // 602
      last(current_token.data).nodeValue += data + o;                                                              // 603
    }                                                                                                              // 604
    return true;                                                                                                   // 605
  }                                                                                                                // 606
                                                                                                                   // 607
  function after_attribute_value_state(buffer) {                                                                   // 608
    var data = buffer.shift(1);                                                                                    // 609
    if (data === HTML5.EOF) {                                                                                      // 610
      parse_error( "unexpected-EOF-after-attribute-value");                                                        // 611
      emit_current_token();                                                                                        // 612
      buffer.unget(data);                                                                                          // 613
      newState(data_state);                                                                                        // 614
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 615
      newState(before_attribute_name_state);                                                                       // 616
    } else if (data == '>') {                                                                                      // 617
      emit_current_token();                                                                                        // 618
      newState(data_state);                                                                                        // 619
    } else if (data == '/') {                                                                                      // 620
      newState(self_closing_tag_state);                                                                            // 621
    } else {                                                                                                       // 622
      emitToken({type: 'ParseError', data: "unexpected-character-after-attribute-value"});                         // 623
      buffer.unget(data);                                                                                          // 624
      newState(before_attribute_name_state);                                                                       // 625
    }                                                                                                              // 626
    return true;                                                                                                   // 627
  }                                                                                                                // 628
                                                                                                                   // 629
  function self_closing_tag_state(buffer) {                                                                        // 630
    var c = buffer.shift(1);                                                                                       // 631
    if (c === HTML5.EOF) {                                                                                         // 632
      parse_error("eof-in-tag-name");                                                                              // 633
      buffer.unget(c);                                                                                             // 634
      newState(data_state);                                                                                        // 635
    } else if (c == '>') {                                                                                         // 636
      current_token.self_closing = true;                                                                           // 637
      emit_current_token();                                                                                        // 638
      newState(data_state);                                                                                        // 639
    } else {                                                                                                       // 640
      parse_error("expected-self-closing-tag");                                                                    // 641
      buffer.unget(c);                                                                                             // 642
      newState(before_attribute_name_state);                                                                       // 643
    }                                                                                                              // 644
    return true;                                                                                                   // 645
  }                                                                                                                // 646
                                                                                                                   // 647
  function bogus_comment_state(buffer) {                                                                           // 648
    var s = buffer.matchUntil('>');                                                                                // 649
    if (s === HTML5.EOF) {                                                                                         // 650
      s = '';                                                                                                      // 651
    }                                                                                                              // 652
    var tok = {type: 'Comment', data: s};                                                                          // 653
    buffer.char();                                                                                                 // 654
    emitToken(tok);                                                                                                // 655
    newState(data_state);                                                                                          // 656
    return true;                                                                                                   // 657
  }                                                                                                                // 658
                                                                                                                   // 659
  function markup_declaration_open_state(buffer) {                                                                 // 660
    var chars = buffer.shift(2);                                                                                   // 661
    if (chars === '--') {                                                                                          // 662
      current_token = {type: 'Comment', data: ''};                                                                 // 663
      newState(comment_start_state);                                                                               // 664
    } else {                                                                                                       // 665
      var newchars = buffer.shift(5);                                                                              // 666
      if (newchars === HTML5.EOF || chars === HTML5.EOF) {                                                         // 667
	parse_error("expected-dashes-or-doctype");                                                                        // 668
	newState(bogus_comment_state);                                                                                    // 669
	buffer.unget(chars);                                                                                              // 670
	return true;                                                                                                      // 671
      }                                                                                                            // 672
                                                                                                                   // 673
      chars += newchars;                                                                                           // 674
      if (chars.toUpperCase() == 'DOCTYPE') {                                                                      // 675
	current_token = {type: 'Doctype', name: '', publicId: null, systemId: null, correct: true};                       // 676
	newState(doctype_state);                                                                                          // 677
      } else if (last(tree.open_elements) && last(tree.open_elements).namespace && chars == '[CDATA[') {           // 678
	newState(cdata_section_state);                                                                                    // 679
      } else {                                                                                                     // 680
	parse_error("expected-dashes-or-doctype");                                                                        // 681
	buffer.unget(chars);                                                                                              // 682
	newState(bogus_comment_state);                                                                                    // 683
      }                                                                                                            // 684
    }                                                                                                              // 685
    return true;                                                                                                   // 686
  }                                                                                                                // 687
                                                                                                                   // 688
  function cdata_section_state(buffer) {                                                                           // 689
    var data = buffer.matchUntil(/\]\]>/);                                                                         // 690
    var slice;                                                                                                     // 691
    if (/\]\]>$/.match(data)) {                                                                                    // 692
      slice = 4;                                                                                                   // 693
    } else {                                                                                                       // 694
      slice = 0;                                                                                                   // 695
    }                                                                                                              // 696
                                                                                                                   // 697
    emitToken({type: 'Characters', data: data.slice(0, data.length - slice)});                                     // 698
    newState(data_state);                                                                                          // 699
  }                                                                                                                // 700
                                                                                                                   // 701
  function comment_start_state(buffer) {                                                                           // 702
    var data = buffer.shift(1);                                                                                    // 703
    if (data === HTML5.EOF) {                                                                                      // 704
      parse_error("eof-in-comment");                                                                               // 705
      emitToken(current_token);                                                                                    // 706
      newState(data_state);                                                                                        // 707
    } else if (data == '-') {                                                                                      // 708
      newState(comment_start_dash_state);                                                                          // 709
    } else if (data == '>') {                                                                                      // 710
      parse_error("incorrect comment");                                                                            // 711
      emitToken(current_token);                                                                                    // 712
      newState(data_state);                                                                                        // 713
    } else {                                                                                                       // 714
      current_token.data += data + buffer.matchUntil('-');                                                         // 715
      newState(comment_state);                                                                                     // 716
    }                                                                                                              // 717
    return true;                                                                                                   // 718
  }                                                                                                                // 719
                                                                                                                   // 720
  function comment_start_dash_state(buffer) {                                                                      // 721
    var data = buffer.shift(1);                                                                                    // 722
    if (data === HTML5.EOF) {                                                                                      // 723
      parse_error("eof-in-comment");                                                                               // 724
      emitToken(current_token);                                                                                    // 725
      newState(data_state);                                                                                        // 726
    } else if (data == '-') {                                                                                      // 727
      newState(comment_end_state);                                                                                 // 728
    } else if (data == '>') {                                                                                      // 729
      parse_error("incorrect-comment");                                                                            // 730
      emitToken(current_token);                                                                                    // 731
      newState(data_state);                                                                                        // 732
    } else {                                                                                                       // 733
      var s = buffer.matchUntil('-');                                                                              // 734
      if (s !== HTML5.EOF) data = data + s;                                                                        // 735
      current_token.data += '-' + data;                                                                            // 736
      newState(comment_state);                                                                                     // 737
    }                                                                                                              // 738
    return true;                                                                                                   // 739
  }                                                                                                                // 740
                                                                                                                   // 741
  function comment_state(buffer) {                                                                                 // 742
    var data = buffer.shift(1);                                                                                    // 743
    if (data === HTML5.EOF) {                                                                                      // 744
      parse_error("eof-in-comment");                                                                               // 745
      emitToken(current_token);                                                                                    // 746
      newState(data_state);                                                                                        // 747
    } else if (data == '-') {                                                                                      // 748
      newState(comment_end_dash_state);                                                                            // 749
    } else {                                                                                                       // 750
      current_token.data += data + buffer.matchUntil('-');                                                         // 751
    }                                                                                                              // 752
    return true;                                                                                                   // 753
  }                                                                                                                // 754
                                                                                                                   // 755
  function comment_end_dash_state(buffer) {                                                                        // 756
    var data = buffer.char();                                                                                      // 757
    if (data === HTML5.EOF) {                                                                                      // 758
      parse_error("eof-in-comment-end-dash");                                                                      // 759
      emitToken(current_token);                                                                                    // 760
      newState(data_state);                                                                                        // 761
    } else if (data == '-') {                                                                                      // 762
      newState(comment_end_state);                                                                                 // 763
    } else {                                                                                                       // 764
      current_token.data += '-' + data + buffer.matchUntil('-');                                                   // 765
      // Consume the next character which is either a "-" or an :EOF as                                            // 766
      // well so if there's a "-" directly after the "-" we go nicely to                                           // 767
      // the "comment end state" without emitting a ParseError there.                                              // 768
      buffer.char();                                                                                               // 769
    }                                                                                                              // 770
    return true;                                                                                                   // 771
  }                                                                                                                // 772
                                                                                                                   // 773
  function comment_end_state(buffer) {                                                                             // 774
    var data = buffer.shift(1);                                                                                    // 775
    if (data === HTML5.EOF) {                                                                                      // 776
      parse_error("eof-in-comment-double-dash");                                                                   // 777
      emitToken(current_token);                                                                                    // 778
      newState(data_state);                                                                                        // 779
    } else if (data == '>') {                                                                                      // 780
      emitToken(current_token);                                                                                    // 781
      newState(data_state);                                                                                        // 782
    } else if (data == '-') {                                                                                      // 783
      parse_error("unexpected-dash-after-double-dash-in-comment");                                                 // 784
      current_token.data += data;                                                                                  // 785
    } else {                                                                                                       // 786
      // XXX                                                                                                       // 787
      parse_error("unexpected-char-in-comment");                                                                   // 788
      current_token.data += '--' + data;                                                                           // 789
      newState(comment_state);                                                                                     // 790
    }                                                                                                              // 791
    return true;                                                                                                   // 792
  }                                                                                                                // 793
                                                                                                                   // 794
  function doctype_state(buffer) {                                                                                 // 795
    var data = buffer.shift(1);                                                                                    // 796
    if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                                     // 797
      newState(before_doctype_name_state);                                                                         // 798
    } else {                                                                                                       // 799
      parse_error("need-space-after-doctype");                                                                     // 800
      buffer.unget(data);                                                                                          // 801
      newState(before_doctype_name_state);                                                                         // 802
    }                                                                                                              // 803
    return true;                                                                                                   // 804
  }                                                                                                                // 805
                                                                                                                   // 806
  function before_doctype_name_state(buffer) {                                                                     // 807
    var data = buffer.shift(1);                                                                                    // 808
    if (data === HTML5.EOF) {                                                                                      // 809
      parse_error("expected-doctype-name-but-got-eof");                                                            // 810
      current_token.correct = false;                                                                               // 811
      emit_current_token();                                                                                        // 812
      newState(data_state);                                                                                        // 813
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 814
    } else if (data == '>') {                                                                                      // 815
      parse_error("expected-doctype-name-but-got-right-bracket");                                                  // 816
      current_token.correct = false;                                                                               // 817
      emit_current_token();                                                                                        // 818
      newState(data_state);                                                                                        // 819
    } else {                                                                                                       // 820
      current_token.name = data.toLowerCase();                                                                     // 821
      newState(doctype_name_state);                                                                                // 822
    }                                                                                                              // 823
    return true;                                                                                                   // 824
  }                                                                                                                // 825
                                                                                                                   // 826
  function doctype_name_state(buffer) {                                                                            // 827
    var data = buffer.shift(1);                                                                                    // 828
    if (data === HTML5.EOF) {                                                                                      // 829
      current_token.correct = false;                                                                               // 830
      buffer.unget(data);                                                                                          // 831
      parse_error("eof-in-doctype");                                                                               // 832
      emit_current_token();                                                                                        // 833
      newState(data_state);                                                                                        // 834
    } else if (HTML5.SPACE_CHARACTERS_R.test(data)) {                                                              // 835
      newState(bogus_doctype_state);                                                                               // 836
    } else if (data == '>') {                                                                                      // 837
      emit_current_token();                                                                                        // 838
      newState(data_state);                                                                                        // 839
    } else {                                                                                                       // 840
      current_token.name += data.toLowerCase();                                                                    // 841
    }                                                                                                              // 842
    buffer.commit();                                                                                               // 843
    return true;                                                                                                   // 844
  }                                                                                                                // 845
                                                                                                                   // 846
  function bogus_doctype_state(buffer) {                                                                           // 847
    var data = buffer.shift(1);                                                                                    // 848
    current_token.correct = false;                                                                                 // 849
    if (data === HTML5.EOF) {                                                                                      // 850
      throw(new Error("Unimplemented!"));                                                                          // 851
    } else if (data == '>') {                                                                                      // 852
      emit_current_token();                                                                                        // 853
      newState(data_state);                                                                                        // 854
    }                                                                                                              // 855
    return true;                                                                                                   // 856
  }                                                                                                                // 857
                                                                                                                   // 858
  function parse_error(message, context) {                                                                         // 859
    emitToken({type: 'ParseError', data: message});                                                                // 860
    HTML5.debug('tokenizer.parseError', message, context);                                                         // 861
  }                                                                                                                // 862
                                                                                                                   // 863
  function emit_current_token() {                                                                                  // 864
    var tok = current_token;                                                                                       // 865
    switch(tok.type) {                                                                                             // 866
    case 'StartTag':                                                                                               // 867
    case 'EndTag':                                                                                                 // 868
    case 'EmptyTag':                                                                                               // 869
      if (tok.type == 'EndTag' && tok.self_closing) {                                                              // 870
	parse_error('self-closing-end-tag');                                                                              // 871
      }                                                                                                            // 872
      break;                                                                                                       // 873
    }                                                                                                              // 874
    if (current_token.name.toLowerCase() == "script" && tok.type == 'EndTag' && script_buffer) {                   // 875
      emitToken({ type: 'Characters', data: script_buffer });                                                      // 876
      script_buffer = null;                                                                                        // 877
    }                                                                                                              // 878
    emitToken(tok);                                                                                                // 879
    newState(data_state);                                                                                          // 880
  }                                                                                                                // 881
                                                                                                                   // 882
  function normalize_token(token) {                                                                                // 883
    if (token.type == 'EmptyTag') {                                                                                // 884
      if (HTML5.VOID_ELEMENTS.indexOf(token.name) == -1) {                                                         // 885
	parse_error('incorrectly-placed-solidus');                                                                        // 886
      }                                                                                                            // 887
      token.type = 'StartTag';                                                                                     // 888
    }                                                                                                              // 889
                                                                                                                   // 890
    if (token.type == 'StartTag') {                                                                                // 891
      token.name = token.name.toLowerCase();                                                                       // 892
      if (token.data.length !== 0) {                                                                               // 893
	var data = {};                                                                                                    // 894
	// the first value for each key wins                                                                              // 895
	token.data.reverse();                                                                                             // 896
	token.data.forEach(function(e) {                                                                                  // 897
	  data[e.nodeName.toLowerCase()] = e.nodeValue;                                                                   // 898
	});                                                                                                               // 899
	token.data = [];                                                                                                  // 900
	for(var k in data) {                                                                                              // 901
	  token.data.push({nodeName: k, nodeValue: data[k]});                                                             // 902
	}                                                                                                                 // 903
	// restore original attribute order                                                                               // 904
	token.data.reverse();                                                                                             // 905
      }                                                                                                            // 906
    } else if (token.type == 'EndTag') {                                                                           // 907
      if (token.data.length !== 0) parse_error('attributes-in-end-tag');                                           // 908
      token.name = token.name.toLowerCase();                                                                       // 909
    }                                                                                                              // 910
                                                                                                                   // 911
    return token;                                                                                                  // 912
  }                                                                                                                // 913
                                                                                                                   // 914
  if (typeof input === 'undefined') throw(new Error("No input given"));                                            // 915
  this.document = document;                                                                                        // 916
  this.__defineSetter__('content_model', function(model) {                                                         // 917
    HTML5.debug('tokenizer.content_model=', model);                                                                // 918
    content_model = model;                                                                                         // 919
  });                                                                                                              // 920
  this.__defineGetter__('content_model', function() {                                                              // 921
    return content_model;                                                                                          // 922
  });                                                                                                              // 923
  function newState(newstate) {                                                                                    // 924
    HTML5.debug('tokenizer.state=', newstate.name);                                                                // 925
    state = newstate;                                                                                              // 926
    buffer.commit();                                                                                               // 927
  }                                                                                                                // 928
                                                                                                                   // 929
  newState(data_state);                                                                                            // 930
                                                                                                                   // 931
  if (input instanceof events.EventEmitter) {                                                                      // 932
    source = input;                                                                                                // 933
    this.pump = null;                                                                                              // 934
  } else {                                                                                                         // 935
    source = new events.EventEmitter();                                                                            // 936
    this.pump = function() {                                                                                       // 937
      source.emit('data', input);                                                                                  // 938
      source.emit('end');                                                                                          // 939
    };                                                                                                             // 940
  }                                                                                                                // 941
                                                                                                                   // 942
  source.addListener('data', function(data) {                                                                      // 943
    if (typeof data !== 'string') data = data.toString();                                                          // 944
    buffer.append(data);                                                                                           // 945
    try {                                                                                                          // 946
      while(state(buffer));                                                                                        // 947
    } catch(e) {                                                                                                   // 948
      if (e != HTML5.DRAIN) {                                                                                      // 949
	throw(e);                                                                                                         // 950
      } else {                                                                                                     // 951
	HTML5.debug('tokenizer.drain', 'Drain');                                                                          // 952
	buffer.undo();                                                                                                    // 953
      }                                                                                                            // 954
    }                                                                                                              // 955
  });                                                                                                              // 956
  source.addListener('end', function() {                                                                           // 957
    buffer.eof = true;                                                                                             // 958
    while(state(buffer));                                                                                          // 959
    this.emit('end');                                                                                              // 960
  }.bind(this));                                                                                                   // 961
                                                                                                                   // 962
};                                                                                                                 // 963
                                                                                                                   // 964
t.prototype = new events.EventEmitter();                                                                           // 965
                                                                                                                   // 966
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/html5-tokenizer/html5_tokenizer.js                                                                     //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
HTML5Tokenizer = {                                                                                                 // 1
  tokenize: function (inputString) {                                                                               // 2
    var tokens = [];                                                                                               // 3
    var tokenizer = new HTML5.Tokenizer(inputString);                                                              // 4
    tokenizer.addListener('token', function (tok) {                                                                // 5
      tokens.push(tok);                                                                                            // 6
    });                                                                                                            // 7
    tokenizer.tokenize();                                                                                          // 8
    return tokens;                                                                                                 // 9
  }                                                                                                                // 10
  // Incremental tokenization turns out not to be useful                                                           // 11
  // for inspecting intermediate tokenizer state, just                                                             // 12
  // for async streaming.                                                                                          // 13
  //                                                                                                               // 14
  // tokenizeIncremental: function (tokenFunc) {                                                                   // 15
  //   var emitter = new toyevents.EventEmitter();                                                                 // 16
  //   var tokenizer = new HTML5.Tokenizer(emitter);                                                               // 17
  //   tokenizer.addListener('token', tokenFunc);                                                                  // 18
  //   return {                                                                                                    // 19
  //     add: function (str) {                                                                                     // 20
  //       emitter.emit('data', str);                                                                              // 21
  //     },                                                                                                        // 22
  //     finish: function () {                                                                                     // 23
  //       emitter.emit('end');                                                                                    // 24
  //     }                                                                                                         // 25
  //   };                                                                                                          // 26
  // }                                                                                                             // 27
};                                                                                                                 // 28
                                                                                                                   // 29
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
