(function () {

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/html5-tokenizer/tokenizer_tests.js                       //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
Tinytest.add("html5-tokenizer - basic", function (test) {            // 1
                                                                     // 2
  var run = function (input, expectedTokens) {                       // 3
    test.equal(HTML5Tokenizer.tokenize(input),                       // 4
               expectedTokens);                                      // 5
  };                                                                 // 6
                                                                     // 7
  run('<p>foo',                                                      // 8
      [ { type: 'StartTag', name: 'p', data: [] },                   // 9
        { type: 'Characters', data: 'foo' },                         // 10
        { type: 'EOF', data: 'End of File' } ]);                     // 11
                                                                     // 12
  run('<!DOCTYPE html>',                                             // 13
      [ { type: 'Doctype', name: 'html', correct: true,              // 14
          publicId: null, systemId: null },                          // 15
        { type: 'EOF', data: 'End of File' } ]);                     // 16
                                                                     // 17
  run('<a b c=d> </a>',                                              // 18
      [ { type: 'StartTag', name: 'a',                               // 19
          data: [{nodeName: 'b', nodeValue: ''},                     // 20
                 {nodeName: 'c', nodeValue: 'd'}] },                 // 21
        { type: 'SpaceCharacters', data: ' ' },                      // 22
        { type: 'EndTag', name: 'a', data: [] },                     // 23
        { type: 'EOF', data: 'End of File' } ]);                     // 24
                                                                     // 25
  run('<3',                                                          // 26
      [{ type: 'ParseError', data: 'expected-tag-name' },            // 27
       { type: 'Characters', data: '<' },                            // 28
       { type: 'Characters', data: '3' },                            // 29
       { type: 'EOF', data: 'End of File' } ]);                      // 30
                                                                     // 31
  run('<!--foo-->',                                                  // 32
      [{ type: 'Comment', data: 'foo' },                             // 33
       { type: 'EOF', data: 'End of File' } ]);                      // 34
                                                                     // 35
});                                                                  // 36
///////////////////////////////////////////////////////////////////////

}).call(this);
