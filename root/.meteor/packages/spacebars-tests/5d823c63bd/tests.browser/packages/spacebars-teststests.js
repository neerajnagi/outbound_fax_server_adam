(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// packages/spacebars-tests/template.template_tests.js                                                                //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
                                                                                                                      // 1
Template.__define__("spacebars_template_test_aaa", (function() {                                                      // 2
  var self = this;                                                                                                    // 3
  var template = this;                                                                                                // 4
  return "aaa";                                                                                                       // 5
}));                                                                                                                  // 6
                                                                                                                      // 7
Template.__define__("spacebars_template_test_bbb", (function() {                                                      // 8
  var self = this;                                                                                                    // 9
  var template = this;                                                                                                // 10
  return "bbb";                                                                                                       // 11
}));                                                                                                                  // 12
                                                                                                                      // 13
Template.__define__("spacebars_template_test_bracketed_this", (function() {                                           // 14
  var self = this;                                                                                                    // 15
  var template = this;                                                                                                // 16
  return [ "[", function() {                                                                                          // 17
    return Spacebars.mustache(self.lookup("."));                                                                      // 18
  }, "]" ];                                                                                                           // 19
}));                                                                                                                  // 20
                                                                                                                      // 21
Template.__define__("spacebars_template_test_span_this", (function() {                                                // 22
  var self = this;                                                                                                    // 23
  var template = this;                                                                                                // 24
  return HTML.SPAN(function() {                                                                                       // 25
    return Spacebars.mustache(self.lookup("."));                                                                      // 26
  });                                                                                                                 // 27
}));                                                                                                                  // 28
                                                                                                                      // 29
Template.__define__("spacebars_template_test_content", (function() {                                                  // 30
  var self = this;                                                                                                    // 31
  var template = this;                                                                                                // 32
  return UI.InTemplateScope(template, Spacebars.include(function() {                                                  // 33
    return template.__content;                                                                                        // 34
  }));                                                                                                                // 35
}));                                                                                                                  // 36
                                                                                                                      // 37
Template.__define__("spacebars_template_test_elsecontent", (function() {                                              // 38
  var self = this;                                                                                                    // 39
  var template = this;                                                                                                // 40
  return UI.InTemplateScope(template, Spacebars.include(function() {                                                  // 41
    return template.__elseContent;                                                                                    // 42
  }));                                                                                                                // 43
}));                                                                                                                  // 44
                                                                                                                      // 45
Template.__define__("spacebars_template_test_iftemplate", (function() {                                               // 46
  var self = this;                                                                                                    // 47
  var template = this;                                                                                                // 48
  return UI.If(function() {                                                                                           // 49
    return Spacebars.call(self.lookup("condition"));                                                                  // 50
  }, UI.block(function() {                                                                                            // 51
    var self = this;                                                                                                  // 52
    return [ "\n    ", UI.InTemplateScope(template, Spacebars.include(function() {                                    // 53
      return template.__content;                                                                                      // 54
    })), "\n  " ];                                                                                                    // 55
  }), UI.block(function() {                                                                                           // 56
    var self = this;                                                                                                  // 57
    return [ "\n    ", UI.InTemplateScope(template, Spacebars.include(function() {                                    // 58
      return template.__elseContent;                                                                                  // 59
    })), "\n  " ];                                                                                                    // 60
  }));                                                                                                                // 61
}));                                                                                                                  // 62
                                                                                                                      // 63
Template.__define__("spacebars_template_test_simple_helper", (function() {                                            // 64
  var self = this;                                                                                                    // 65
  var template = this;                                                                                                // 66
  return function() {                                                                                                 // 67
    return Spacebars.mustache(self.lookup("foo"), self.lookup("bar"));                                                // 68
  };                                                                                                                  // 69
}));                                                                                                                  // 70
                                                                                                                      // 71
Template.__define__("spacebars_template_test_dynamic_template", (function() {                                         // 72
  var self = this;                                                                                                    // 73
  var template = this;                                                                                                // 74
  return Spacebars.include(self.lookupTemplate("foo"));                                                               // 75
}));                                                                                                                  // 76
                                                                                                                      // 77
Template.__define__("spacebars_template_test_interpolate_attribute", (function() {                                    // 78
  var self = this;                                                                                                    // 79
  var template = this;                                                                                                // 80
  return HTML.DIV({                                                                                                   // 81
    "class": [ "aaa", function() {                                                                                    // 82
      return Spacebars.mustache(self.lookup("foo"), self.lookup("bar"));                                              // 83
    }, "zzz" ]                                                                                                        // 84
  });                                                                                                                 // 85
}));                                                                                                                  // 86
                                                                                                                      // 87
Template.__define__("spacebars_template_test_dynamic_attrs", (function() {                                            // 88
  var self = this;                                                                                                    // 89
  var template = this;                                                                                                // 90
  return HTML.SPAN({                                                                                                  // 91
    $dynamic: [ function() {                                                                                          // 92
      return Spacebars.attrMustache(self.lookup("attrsObj"));                                                         // 93
    }, function() {                                                                                                   // 94
      return Spacebars.attrMustache(self.lookup("singleAttr"));                                                       // 95
    }, function() {                                                                                                   // 96
      return Spacebars.attrMustache(self.lookup("nonexistent"));                                                      // 97
    } ]                                                                                                               // 98
  }, "hi");                                                                                                           // 99
}));                                                                                                                  // 100
                                                                                                                      // 101
Template.__define__("spacebars_template_test_triple", (function() {                                                   // 102
  var self = this;                                                                                                    // 103
  var template = this;                                                                                                // 104
  return function() {                                                                                                 // 105
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("html")));                                                // 106
  };                                                                                                                  // 107
}));                                                                                                                  // 108
                                                                                                                      // 109
Template.__define__("spacebars_template_test_triple2", (function() {                                                  // 110
  var self = this;                                                                                                    // 111
  var template = this;                                                                                                // 112
  return [ "x", function() {                                                                                          // 113
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("html")));                                                // 114
  }, function() {                                                                                                     // 115
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("html2")));                                               // 116
  }, function() {                                                                                                     // 117
    return Spacebars.makeRaw(Spacebars.mustache(self.lookup("html3")));                                               // 118
  }, "y" ];                                                                                                           // 119
}));                                                                                                                  // 120
                                                                                                                      // 121
Template.__define__("spacebars_template_test_inclusion_args", (function() {                                           // 122
  var self = this;                                                                                                    // 123
  var template = this;                                                                                                // 124
  return Spacebars.TemplateWith(function() {                                                                          // 125
    return Spacebars.call(self.lookup("bar"));                                                                        // 126
  }, UI.block(function() {                                                                                            // 127
    var self = this;                                                                                                  // 128
    return Spacebars.include(self.lookupTemplate("foo"));                                                             // 129
  }));                                                                                                                // 130
}));                                                                                                                  // 131
                                                                                                                      // 132
Template.__define__("spacebars_template_test_inclusion_args2", (function() {                                          // 133
  var self = this;                                                                                                    // 134
  var template = this;                                                                                                // 135
  return Spacebars.TemplateWith(function() {                                                                          // 136
    return Spacebars.dataMustache(self.lookup("bar"), Spacebars.kw({                                                  // 137
      q: self.lookup("baz")                                                                                           // 138
    }));                                                                                                              // 139
  }, UI.block(function() {                                                                                            // 140
    var self = this;                                                                                                  // 141
    return Spacebars.include(self.lookupTemplate("foo"));                                                             // 142
  }));                                                                                                                // 143
}));                                                                                                                  // 144
                                                                                                                      // 145
Template.__define__("spacebars_template_test_inclusion_dotted_args", (function() {                                    // 146
  var self = this;                                                                                                    // 147
  var template = this;                                                                                                // 148
  return Spacebars.TemplateWith(function() {                                                                          // 149
    return Spacebars.call(Spacebars.dot(self.lookup("bar"), "baz"));                                                  // 150
  }, UI.block(function() {                                                                                            // 151
    var self = this;                                                                                                  // 152
    return Spacebars.include(self.lookupTemplate("foo"));                                                             // 153
  }));                                                                                                                // 154
}));                                                                                                                  // 155
                                                                                                                      // 156
Template.__define__("spacebars_template_test_inclusion_slashed_args", (function() {                                   // 157
  var self = this;                                                                                                    // 158
  var template = this;                                                                                                // 159
  return Spacebars.TemplateWith(function() {                                                                          // 160
    return Spacebars.call(Spacebars.dot(self.lookup("bar"), "baz"));                                                  // 161
  }, UI.block(function() {                                                                                            // 162
    var self = this;                                                                                                  // 163
    return Spacebars.include(self.lookupTemplate("foo"));                                                             // 164
  }));                                                                                                                // 165
}));                                                                                                                  // 166
                                                                                                                      // 167
Template.__define__("spacebars_template_test_block_helper", (function() {                                             // 168
  var self = this;                                                                                                    // 169
  var template = this;                                                                                                // 170
  return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                          // 171
    var self = this;                                                                                                  // 172
    return "\n    bar\n  ";                                                                                           // 173
  }), UI.block(function() {                                                                                           // 174
    var self = this;                                                                                                  // 175
    return "\n    baz\n  ";                                                                                           // 176
  }));                                                                                                                // 177
}));                                                                                                                  // 178
                                                                                                                      // 179
Template.__define__("spacebars_template_test_block_helper_function_one_string_arg", (function() {                     // 180
  var self = this;                                                                                                    // 181
  var template = this;                                                                                                // 182
  return Spacebars.TemplateWith(function() {                                                                          // 183
    return "bar";                                                                                                     // 184
  }, UI.block(function() {                                                                                            // 185
    var self = this;                                                                                                  // 186
    return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                        // 187
      var self = this;                                                                                                // 188
      return "\n    content\n  ";                                                                                     // 189
    }));                                                                                                              // 190
  }));                                                                                                                // 191
}));                                                                                                                  // 192
                                                                                                                      // 193
Template.__define__("spacebars_template_test_block_helper_function_one_helper_arg", (function() {                     // 194
  var self = this;                                                                                                    // 195
  var template = this;                                                                                                // 196
  return Spacebars.TemplateWith(function() {                                                                          // 197
    return Spacebars.call(self.lookup("bar"));                                                                        // 198
  }, UI.block(function() {                                                                                            // 199
    var self = this;                                                                                                  // 200
    return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                        // 201
      var self = this;                                                                                                // 202
      return "\n    content\n  ";                                                                                     // 203
    }));                                                                                                              // 204
  }));                                                                                                                // 205
}));                                                                                                                  // 206
                                                                                                                      // 207
Template.__define__("spacebars_template_test_block_helper_component_one_helper_arg", (function() {                    // 208
  var self = this;                                                                                                    // 209
  var template = this;                                                                                                // 210
  return UI.If(function() {                                                                                           // 211
    return Spacebars.call(self.lookup("bar"));                                                                        // 212
  }, UI.block(function() {                                                                                            // 213
    var self = this;                                                                                                  // 214
    return "\n    content\n  ";                                                                                       // 215
  }));                                                                                                                // 216
}));                                                                                                                  // 217
                                                                                                                      // 218
Template.__define__("spacebars_template_test_block_helper_component_three_helper_args", (function() {                 // 219
  var self = this;                                                                                                    // 220
  var template = this;                                                                                                // 221
  return UI.If(function() {                                                                                           // 222
    return Spacebars.dataMustache(self.lookup("equals"), self.lookup("bar_or_baz"), "bar");                           // 223
  }, UI.block(function() {                                                                                            // 224
    var self = this;                                                                                                  // 225
    return "\n    content\n  ";                                                                                       // 226
  }));                                                                                                                // 227
}));                                                                                                                  // 228
                                                                                                                      // 229
Template.__define__("spacebars_template_test_block_helper_dotted_arg", (function() {                                  // 230
  var self = this;                                                                                                    // 231
  var template = this;                                                                                                // 232
  return Spacebars.TemplateWith(function() {                                                                          // 233
    return Spacebars.dataMustache(Spacebars.dot(self.lookup("bar"), "baz"), self.lookup("qux"));                      // 234
  }, UI.block(function() {                                                                                            // 235
    var self = this;                                                                                                  // 236
    return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                        // 237
      var self = this;                                                                                                // 238
      return null;                                                                                                    // 239
    }));                                                                                                              // 240
  }));                                                                                                                // 241
}));                                                                                                                  // 242
                                                                                                                      // 243
Template.__define__("spacebars_template_test_nested_content", (function() {                                           // 244
  var self = this;                                                                                                    // 245
  var template = this;                                                                                                // 246
  return Spacebars.TemplateWith(function() {                                                                          // 247
    return {                                                                                                          // 248
      condition: Spacebars.call(self.lookup("flag"))                                                                  // 249
    };                                                                                                                // 250
  }, UI.block(function() {                                                                                            // 251
    var self = this;                                                                                                  // 252
    return Spacebars.include(self.lookupTemplate("spacebars_template_test_iftemplate"), UI.block(function() {         // 253
      var self = this;                                                                                                // 254
      return "\n    hello\n  ";                                                                                       // 255
    }), UI.block(function() {                                                                                         // 256
      var self = this;                                                                                                // 257
      return "\n    world\n  ";                                                                                       // 258
    }));                                                                                                              // 259
  }));                                                                                                                // 260
}));                                                                                                                  // 261
                                                                                                                      // 262
Template.__define__("spacebars_template_test_iftemplate2", (function() {                                              // 263
  var self = this;                                                                                                    // 264
  var template = this;                                                                                                // 265
  return Spacebars.TemplateWith(function() {                                                                          // 266
    return {                                                                                                          // 267
      condition: Spacebars.call(self.lookup("flag"))                                                                  // 268
    };                                                                                                                // 269
  }, UI.block(function() {                                                                                            // 270
    var self = this;                                                                                                  // 271
    return Spacebars.include(self.lookupTemplate("spacebars_template_test_iftemplate"), UI.block(function() {         // 272
      var self = this;                                                                                                // 273
      return [ "\n    ", UI.InTemplateScope(template, Spacebars.include(function() {                                  // 274
        return template.__content;                                                                                    // 275
      })), "\n  " ];                                                                                                  // 276
    }), UI.block(function() {                                                                                         // 277
      var self = this;                                                                                                // 278
      return [ "\n    ", UI.InTemplateScope(template, Spacebars.include(function() {                                  // 279
        return template.__elseContent;                                                                                // 280
      })), "\n  " ];                                                                                                  // 281
    }));                                                                                                              // 282
  }));                                                                                                                // 283
}));                                                                                                                  // 284
                                                                                                                      // 285
Template.__define__("spacebars_template_test_nested_content2", (function() {                                          // 286
  var self = this;                                                                                                    // 287
  var template = this;                                                                                                // 288
  return Spacebars.TemplateWith(function() {                                                                          // 289
    return {                                                                                                          // 290
      flag: Spacebars.call(self.lookup("x"))                                                                          // 291
    };                                                                                                                // 292
  }, UI.block(function() {                                                                                            // 293
    var self = this;                                                                                                  // 294
    return Spacebars.include(self.lookupTemplate("spacebars_template_test_iftemplate2"), UI.block(function() {        // 295
      var self = this;                                                                                                // 296
      return "\n    hello\n  ";                                                                                       // 297
    }), UI.block(function() {                                                                                         // 298
      var self = this;                                                                                                // 299
      return "\n    world\n  ";                                                                                       // 300
    }));                                                                                                              // 301
  }));                                                                                                                // 302
}));                                                                                                                  // 303
                                                                                                                      // 304
Template.__define__("spacebars_template_test_if", (function() {                                                       // 305
  var self = this;                                                                                                    // 306
  var template = this;                                                                                                // 307
  return UI.If(function() {                                                                                           // 308
    return Spacebars.call(self.lookup("foo"));                                                                        // 309
  }, UI.block(function() {                                                                                            // 310
    var self = this;                                                                                                  // 311
    return [ "\n    ", function() {                                                                                   // 312
      return Spacebars.mustache(self.lookup("bar"));                                                                  // 313
    }, "\n  " ];                                                                                                      // 314
  }), UI.block(function() {                                                                                           // 315
    var self = this;                                                                                                  // 316
    return [ "\n    ", function() {                                                                                   // 317
      return Spacebars.mustache(self.lookup("baz"));                                                                  // 318
    }, "\n  " ];                                                                                                      // 319
  }));                                                                                                                // 320
}));                                                                                                                  // 321
                                                                                                                      // 322
Template.__define__("spacebars_template_test_if_in_with", (function() {                                               // 323
  var self = this;                                                                                                    // 324
  var template = this;                                                                                                // 325
  return Spacebars.With(function() {                                                                                  // 326
    return Spacebars.call(self.lookup("foo"));                                                                        // 327
  }, UI.block(function() {                                                                                            // 328
    var self = this;                                                                                                  // 329
    return [ "\n    ", function() {                                                                                   // 330
      return Spacebars.mustache(self.lookup("bar"));                                                                  // 331
    }, "\n    ", UI.If(function() {                                                                                   // 332
      return true;                                                                                                    // 333
    }, UI.block(function() {                                                                                          // 334
      var self = this;                                                                                                // 335
      return [ "\n      ", function() {                                                                               // 336
        return Spacebars.mustache(self.lookup("bar"));                                                                // 337
      }, "\n    " ];                                                                                                  // 338
    })), "\n  " ];                                                                                                    // 339
  }));                                                                                                                // 340
}));                                                                                                                  // 341
                                                                                                                      // 342
Template.__define__("spacebars_template_test_each", (function() {                                                     // 343
  var self = this;                                                                                                    // 344
  var template = this;                                                                                                // 345
  return UI.Each(function() {                                                                                         // 346
    return Spacebars.call(self.lookup("items"));                                                                      // 347
  }, UI.block(function() {                                                                                            // 348
    var self = this;                                                                                                  // 349
    return [ "\n    ", function() {                                                                                   // 350
      return Spacebars.mustache(self.lookup("text"));                                                                 // 351
    }, "\n  " ];                                                                                                      // 352
  }), UI.block(function() {                                                                                           // 353
    var self = this;                                                                                                  // 354
    return "\n    else-clause\n  ";                                                                                   // 355
  }));                                                                                                                // 356
}));                                                                                                                  // 357
                                                                                                                      // 358
Template.__define__("spacebars_template_test_dots", (function() {                                                     // 359
  var self = this;                                                                                                    // 360
  var template = this;                                                                                                // 361
  return Spacebars.With(function() {                                                                                  // 362
    return Spacebars.call(self.lookup("foo"));                                                                        // 363
  }, UI.block(function() {                                                                                            // 364
    var self = this;                                                                                                  // 365
    return [ "\n    A\n    ", Spacebars.With(function() {                                                             // 366
      return Spacebars.call(self.lookup("bar"));                                                                      // 367
    }, UI.block(function() {                                                                                          // 368
      var self = this;                                                                                                // 369
      return [ "\n      B\n      \n      ", UI.If(function() {                                                        // 370
        return true;                                                                                                  // 371
      }, UI.block(function() {                                                                                        // 372
        var self = this;                                                                                              // 373
        return [ "\n        C\n        ", UI.Each(function() {                                                        // 374
          return Spacebars.call(self.lookup("items"));                                                                // 375
        }, UI.block(function() {                                                                                      // 376
          var self = this;                                                                                            // 377
          return [ "\n          D\n          \n          ", Spacebars.include(self.lookupTemplate("spacebars_template_test_dots_subtemplate")), "\n          ", Spacebars.TemplateWith(function() {
            return Spacebars.call(self.lookup(".."));                                                                 // 379
          }, UI.block(function() {                                                                                    // 380
            var self = this;                                                                                          // 381
            return Spacebars.include(self.lookupTemplate("spacebars_template_test_dots_subtemplate"));                // 382
          })), "\n        " ];                                                                                        // 383
        })), "\n      " ];                                                                                            // 384
      })), "\n    " ];                                                                                                // 385
    })), "\n  " ];                                                                                                    // 386
  }));                                                                                                                // 387
}));                                                                                                                  // 388
                                                                                                                      // 389
Template.__define__("spacebars_template_test_dots_subtemplate", (function() {                                         // 390
  var self = this;                                                                                                    // 391
  var template = this;                                                                                                // 392
  return [ "TITLE\n  1", function() {                                                                                 // 393
    return Spacebars.mustache(self.lookup("title"));                                                                  // 394
  }, "\n  2", function() {                                                                                            // 395
    return Spacebars.mustache(Spacebars.dot(self.lookup("."), "title"));                                              // 396
  }, "\n  3", function() {                                                                                            // 397
    return Spacebars.mustache(Spacebars.dot(self.lookup(".."), "title"));                                             // 398
  }, "\n  4", function() {                                                                                            // 399
    return Spacebars.mustache(Spacebars.dot(self.lookup("..."), "title"));                                            // 400
  }, "\n\n  GETTITLE\n  5", function() {                                                                              // 401
    return Spacebars.mustache(self.lookup("getTitle"), self.lookup("."));                                             // 402
  }, "\n  6", function() {                                                                                            // 403
    return Spacebars.mustache(self.lookup("getTitle"), self.lookup(".."));                                            // 404
  }, "\n  7", function() {                                                                                            // 405
    return Spacebars.mustache(self.lookup("getTitle"), self.lookup("..."));                                           // 406
  } ];                                                                                                                // 407
}));                                                                                                                  // 408
                                                                                                                      // 409
Template.__define__("spacebars_template_test_select_tag", (function() {                                               // 410
  var self = this;                                                                                                    // 411
  var template = this;                                                                                                // 412
  return HTML.SELECT("\n    ", UI.Each(function() {                                                                   // 413
    return Spacebars.call(self.lookup("optgroups"));                                                                  // 414
  }, UI.block(function() {                                                                                            // 415
    var self = this;                                                                                                  // 416
    return [ "\n      ", HTML.OPTGROUP({                                                                              // 417
      label: function() {                                                                                             // 418
        return Spacebars.mustache(self.lookup("label"));                                                              // 419
      }                                                                                                               // 420
    }, "\n        ", UI.Each(function() {                                                                             // 421
      return Spacebars.call(self.lookup("options"));                                                                  // 422
    }, UI.block(function() {                                                                                          // 423
      var self = this;                                                                                                // 424
      return [ "\n          ", HTML.OPTION({                                                                          // 425
        value: function() {                                                                                           // 426
          return Spacebars.mustache(self.lookup("value"));                                                            // 427
        },                                                                                                            // 428
        $dynamic: [ function() {                                                                                      // 429
          return Spacebars.attrMustache(self.lookup("selectedAttr"));                                                 // 430
        } ]                                                                                                           // 431
      }, function() {                                                                                                 // 432
        return Spacebars.mustache(self.lookup("label"));                                                              // 433
      }), "\n        " ];                                                                                             // 434
    })), "\n      "), "\n    " ];                                                                                     // 435
  })), "\n  ");                                                                                                       // 436
}));                                                                                                                  // 437
                                                                                                                      // 438
Template.__define__("test_template_issue770", (function() {                                                           // 439
  var self = this;                                                                                                    // 440
  var template = this;                                                                                                // 441
  return [ Spacebars.With(function() {                                                                                // 442
    return Spacebars.call(self.lookup("value1"));                                                                     // 443
  }, UI.block(function() {                                                                                            // 444
    var self = this;                                                                                                  // 445
    return [ "\n    ", function() {                                                                                   // 446
      return Spacebars.mustache(self.lookup("."));                                                                    // 447
    }, "\n  " ];                                                                                                      // 448
  }), UI.block(function() {                                                                                           // 449
    var self = this;                                                                                                  // 450
    return "\n    xxx\n  ";                                                                                           // 451
  })), "\n\n  ", Spacebars.With(function() {                                                                          // 452
    return Spacebars.call(self.lookup("value2"));                                                                     // 453
  }, UI.block(function() {                                                                                            // 454
    var self = this;                                                                                                  // 455
    return [ "\n    ", function() {                                                                                   // 456
      return Spacebars.mustache(self.lookup("."));                                                                    // 457
    }, "\n  " ];                                                                                                      // 458
  }), UI.block(function() {                                                                                           // 459
    var self = this;                                                                                                  // 460
    return "\n    xxx\n  ";                                                                                           // 461
  })), "\n\n  ", Spacebars.With(function() {                                                                          // 462
    return Spacebars.call(self.lookup("value1"));                                                                     // 463
  }, UI.block(function() {                                                                                            // 464
    var self = this;                                                                                                  // 465
    return [ "\n    ", function() {                                                                                   // 466
      return Spacebars.mustache(self.lookup("."));                                                                    // 467
    }, "\n  " ];                                                                                                      // 468
  })), "\n\n  ", Spacebars.With(function() {                                                                          // 469
    return Spacebars.call(self.lookup("value2"));                                                                     // 470
  }, UI.block(function() {                                                                                            // 471
    var self = this;                                                                                                  // 472
    return [ "\n    ", function() {                                                                                   // 473
      return Spacebars.mustache(self.lookup("."));                                                                    // 474
    }, "\n  " ];                                                                                                      // 475
  })) ];                                                                                                              // 476
}));                                                                                                                  // 477
                                                                                                                      // 478
Template.__define__("spacebars_template_test_tricky_attrs", (function() {                                             // 479
  var self = this;                                                                                                    // 480
  var template = this;                                                                                                // 481
  return [ HTML.INPUT({                                                                                               // 482
    type: function() {                                                                                                // 483
      return Spacebars.mustache(self.lookup("theType"));                                                              // 484
    }                                                                                                                 // 485
  }), HTML.INPUT({                                                                                                    // 486
    type: "checkbox",                                                                                                 // 487
    "class": function() {                                                                                             // 488
      return Spacebars.mustache(self.lookup("theClass"));                                                             // 489
    }                                                                                                                 // 490
  }) ];                                                                                                               // 491
}));                                                                                                                  // 492
                                                                                                                      // 493
Template.__define__("spacebars_template_test_no_data", (function() {                                                  // 494
  var self = this;                                                                                                    // 495
  var template = this;                                                                                                // 496
  return [ function() {                                                                                               // 497
    return Spacebars.mustache(Spacebars.dot(self.lookup("."), "foo"));                                                // 498
  }, UI.Unless(function() {                                                                                           // 499
    return Spacebars.call(Spacebars.dot(self.lookup("."), "bar"));                                                    // 500
  }, UI.block(function() {                                                                                            // 501
    var self = this;                                                                                                  // 502
    return "asdf";                                                                                                    // 503
  })) ];                                                                                                              // 504
}));                                                                                                                  // 505
                                                                                                                      // 506
Template.__define__("spacebars_template_test_isolate", (function() {                                                  // 507
  var self = this;                                                                                                    // 508
  var template = this;                                                                                                // 509
  return Spacebars.include(self.lookupTemplate("isolate"), UI.block(function() {                                      // 510
    var self = this;                                                                                                  // 511
    return "hello";                                                                                                   // 512
  }));                                                                                                                // 513
}));                                                                                                                  // 514
                                                                                                                      // 515
Template.__define__("spacebars_template_test_constant", (function() {                                                 // 516
  var self = this;                                                                                                    // 517
  var template = this;                                                                                                // 518
  return Spacebars.include(self.lookupTemplate("constant"), UI.block(function() {                                     // 519
    var self = this;                                                                                                  // 520
    return "hello";                                                                                                   // 521
  }));                                                                                                                // 522
}));                                                                                                                  // 523
                                                                                                                      // 524
Template.__define__("spacebars_template_test_textarea", (function() {                                                 // 525
  var self = this;                                                                                                    // 526
  var template = this;                                                                                                // 527
  return HTML.TEXTAREA(function() {                                                                                   // 528
    return Spacebars.mustache(self.lookup("foo"));                                                                    // 529
  });                                                                                                                 // 530
}));                                                                                                                  // 531
                                                                                                                      // 532
Template.__define__("spacebars_template_test_textarea2", (function() {                                                // 533
  var self = this;                                                                                                    // 534
  var template = this;                                                                                                // 535
  return HTML.TEXTAREA(UI.If(function() {                                                                             // 536
    return Spacebars.call(self.lookup("foo"));                                                                        // 537
  }, UI.block(function() {                                                                                            // 538
    var self = this;                                                                                                  // 539
    return "</not a tag>";                                                                                            // 540
  }), UI.block(function() {                                                                                           // 541
    var self = this;                                                                                                  // 542
    return "<also not a tag>";                                                                                        // 543
  })));                                                                                                               // 544
}));                                                                                                                  // 545
                                                                                                                      // 546
Template.__define__("spacebars_template_test_textarea_each", (function() {                                            // 547
  var self = this;                                                                                                    // 548
  var template = this;                                                                                                // 549
  return HTML.TEXTAREA(UI.Each(function() {                                                                           // 550
    return Spacebars.call(self.lookup("foo"));                                                                        // 551
  }, UI.block(function() {                                                                                            // 552
    var self = this;                                                                                                  // 553
    return [ "<not a tag ", function() {                                                                              // 554
      return Spacebars.mustache(self.lookup("."));                                                                    // 555
    }, " " ];                                                                                                         // 556
  }), UI.block(function() {                                                                                           // 557
    var self = this;                                                                                                  // 558
    return "<>";                                                                                                      // 559
  })));                                                                                                               // 560
}));                                                                                                                  // 561
                                                                                                                      // 562
Template.__define__("spacebars_template_test_defer_in_rendered", (function() {                                        // 563
  var self = this;                                                                                                    // 564
  var template = this;                                                                                                // 565
  return UI.Each(function() {                                                                                         // 566
    return Spacebars.call(self.lookup("items"));                                                                      // 567
  }, UI.block(function() {                                                                                            // 568
    var self = this;                                                                                                  // 569
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_template_test_defer_in_rendered_subtemplate")), "\n  " ];
  }));                                                                                                                // 571
}));                                                                                                                  // 572
                                                                                                                      // 573
Template.__define__("spacebars_template_test_defer_in_rendered_subtemplate", (function() {                            // 574
  var self = this;                                                                                                    // 575
  var template = this;                                                                                                // 576
  return "";                                                                                                          // 577
}));                                                                                                                  // 578
                                                                                                                      // 579
Template.__define__("spacebars_template_test_with_someData", (function() {                                            // 580
  var self = this;                                                                                                    // 581
  var template = this;                                                                                                // 582
  return Spacebars.With(function() {                                                                                  // 583
    return Spacebars.call(self.lookup("someData"));                                                                   // 584
  }, UI.block(function() {                                                                                            // 585
    var self = this;                                                                                                  // 586
    return [ "\n    ", function() {                                                                                   // 587
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 588
    }, " ", function() {                                                                                              // 589
      return Spacebars.mustache(self.lookup("bar"));                                                                  // 590
    }, "\n  " ];                                                                                                      // 591
  }));                                                                                                                // 592
}));                                                                                                                  // 593
                                                                                                                      // 594
Template.__define__("spacebars_template_test_each_stops", (function() {                                               // 595
  var self = this;                                                                                                    // 596
  var template = this;                                                                                                // 597
  return UI.Each(function() {                                                                                         // 598
    return Spacebars.call(self.lookup("items"));                                                                      // 599
  }, UI.block(function() {                                                                                            // 600
    var self = this;                                                                                                  // 601
    return "\n    x\n  ";                                                                                             // 602
  }));                                                                                                                // 603
}));                                                                                                                  // 604
                                                                                                                      // 605
Template.__define__("spacebars_template_test_block_helpers_in_attribute", (function() {                               // 606
  var self = this;                                                                                                    // 607
  var template = this;                                                                                                // 608
  return HTML.DIV({                                                                                                   // 609
    "class": UI.Each(function() {                                                                                     // 610
      return Spacebars.call(self.lookup("classes"));                                                                  // 611
    }, UI.block(function() {                                                                                          // 612
      var self = this;                                                                                                // 613
      return UI.If(function() {                                                                                       // 614
        return Spacebars.dataMustache(self.lookup("startsLowerCase"), self.lookup("name"));                           // 615
      }, UI.block(function() {                                                                                        // 616
        var self = this;                                                                                              // 617
        return [ function() {                                                                                         // 618
          return Spacebars.mustache(self.lookup("name"));                                                             // 619
        }, " " ];                                                                                                     // 620
      }));                                                                                                            // 621
    }), UI.block(function() {                                                                                         // 622
      var self = this;                                                                                                // 623
      return "none";                                                                                                  // 624
    }))                                                                                                               // 625
  }, "Hello");                                                                                                        // 626
}));                                                                                                                  // 627
                                                                                                                      // 628
Template.__define__("spacebars_template_test_block_helpers_in_attribute_2", (function() {                             // 629
  var self = this;                                                                                                    // 630
  var template = this;                                                                                                // 631
  return HTML.INPUT({                                                                                                 // 632
    value: UI.If(function() {                                                                                         // 633
      return Spacebars.call(self.lookup("foo"));                                                                      // 634
    }, UI.block(function() {                                                                                          // 635
      var self = this;                                                                                                // 636
      return '"';                                                                                                     // 637
    }), UI.block(function() {                                                                                         // 638
      var self = this;                                                                                                // 639
      return [ "&", HTML.CharRef({                                                                                    // 640
        html: "&lt;",                                                                                                 // 641
        str: "<"                                                                                                      // 642
      }), "></x>" ];                                                                                                  // 643
    }))                                                                                                               // 644
  });                                                                                                                 // 645
}));                                                                                                                  // 646
                                                                                                                      // 647
Template.__define__("spacebars_template_test_constant_each_argument", (function() {                                   // 648
  var self = this;                                                                                                    // 649
  var template = this;                                                                                                // 650
  return Spacebars.With(function() {                                                                                  // 651
    return Spacebars.call(self.lookup("someData"));                                                                   // 652
  }, UI.block(function() {                                                                                            // 653
    var self = this;                                                                                                  // 654
    return [ "\n    ", UI.Each(function() {                                                                           // 655
      return Spacebars.call(self.lookup("anArray"));                                                                  // 656
    }, UI.block(function() {                                                                                          // 657
      var self = this;                                                                                                // 658
      return [ "\n      ", function() {                                                                               // 659
        return Spacebars.mustache(self.lookup("justReturn"), self.lookup("."));                                       // 660
      }, "\n    " ];                                                                                                  // 661
    })), "\n    ", function() {                                                                                       // 662
      return Spacebars.mustache(self.lookup("."));                                                                    // 663
    }, "\n  " ];                                                                                                      // 664
  }));                                                                                                                // 665
}));                                                                                                                  // 666
                                                                                                                      // 667
Template.__define__("spacebars_template_test_markdown_basic", (function() {                                           // 668
  var self = this;                                                                                                    // 669
  var template = this;                                                                                                // 670
  return Spacebars.With(function() {                                                                                  // 671
    return Spacebars.call(self.lookup("obj"));                                                                        // 672
  }, UI.block(function() {                                                                                            // 673
    var self = this;                                                                                                  // 674
    return [ "\n    ", Spacebars.include(self.lookupTemplate("markdown"), UI.block(function() {                       // 675
      var self = this;                                                                                                // 676
      return [ "\n", function() {                                                                                     // 677
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 678
      }, "\n/each}}\n\n<b>", function() {                                                                             // 679
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 680
      }, "</b>\n<b>/each}}</b>\n\n* ", function() {                                                                   // 681
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 682
      }, "\n* /each}}\n\n* <b>", function() {                                                                         // 683
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 684
      }, "</b>\n* <b>/each}}</b>\n\nsome paragraph to fix showdown's four space parsing below.\n\n    ", function() { // 685
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 686
      }, "\n    /each}}\n\n    <b>", function() {                                                                     // 687
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 688
      }, "</b>\n    <b>/each}}</b>\n\n&gt\n\n* &gt\n\n`&gt`\n\n    &gt\n\n&gt;\n\n* &gt;\n\n`&gt;`\n\n    &gt;\n\n`", function() {
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 690
      }, "`\n`/each}}`\n\n`<b>", function() {                                                                         // 691
        return Spacebars.mustache(self.lookup("hi"));                                                                 // 692
      }, "</b>`\n`<b>/each}}`\n\n    " ];                                                                             // 693
    })), "\n  " ];                                                                                                    // 694
  }));                                                                                                                // 695
}));                                                                                                                  // 696
                                                                                                                      // 697
Template.__define__("spacebars_template_test_markdown_if", (function() {                                              // 698
  var self = this;                                                                                                    // 699
  var template = this;                                                                                                // 700
  return Spacebars.include(self.lookupTemplate("markdown"), UI.block(function() {                                     // 701
    var self = this;                                                                                                  // 702
    return [ "\n\n", UI.If(function() {                                                                               // 703
      return Spacebars.call(self.lookup("cond"));                                                                     // 704
    }, UI.block(function() {                                                                                          // 705
      var self = this;                                                                                                // 706
      return "true";                                                                                                  // 707
    }), UI.block(function() {                                                                                         // 708
      var self = this;                                                                                                // 709
      return "false";                                                                                                 // 710
    })), "\n\n<b>", UI.If(function() {                                                                                // 711
      return Spacebars.call(self.lookup("cond"));                                                                     // 712
    }, UI.block(function() {                                                                                          // 713
      var self = this;                                                                                                // 714
      return "true";                                                                                                  // 715
    }), UI.block(function() {                                                                                         // 716
      var self = this;                                                                                                // 717
      return "false";                                                                                                 // 718
    })), "</b>\n\n* ", UI.If(function() {                                                                             // 719
      return Spacebars.call(self.lookup("cond"));                                                                     // 720
    }, UI.block(function() {                                                                                          // 721
      var self = this;                                                                                                // 722
      return "true";                                                                                                  // 723
    }), UI.block(function() {                                                                                         // 724
      var self = this;                                                                                                // 725
      return "false";                                                                                                 // 726
    })), "\n\n* <b>", UI.If(function() {                                                                              // 727
      return Spacebars.call(self.lookup("cond"));                                                                     // 728
    }, UI.block(function() {                                                                                          // 729
      var self = this;                                                                                                // 730
      return "true";                                                                                                  // 731
    }), UI.block(function() {                                                                                         // 732
      var self = this;                                                                                                // 733
      return "false";                                                                                                 // 734
    })), "</b>\n\nsome paragraph to fix showdown's four space parsing below.\n\n    ", UI.If(function() {             // 735
      return Spacebars.call(self.lookup("cond"));                                                                     // 736
    }, UI.block(function() {                                                                                          // 737
      var self = this;                                                                                                // 738
      return "true";                                                                                                  // 739
    }), UI.block(function() {                                                                                         // 740
      var self = this;                                                                                                // 741
      return "false";                                                                                                 // 742
    })), "\n\n    <b>", UI.If(function() {                                                                            // 743
      return Spacebars.call(self.lookup("cond"));                                                                     // 744
    }, UI.block(function() {                                                                                          // 745
      var self = this;                                                                                                // 746
      return "true";                                                                                                  // 747
    }), UI.block(function() {                                                                                         // 748
      var self = this;                                                                                                // 749
      return "false";                                                                                                 // 750
    })), "</b>\n\n`", UI.If(function() {                                                                              // 751
      return Spacebars.call(self.lookup("cond"));                                                                     // 752
    }, UI.block(function() {                                                                                          // 753
      var self = this;                                                                                                // 754
      return "true";                                                                                                  // 755
    }), UI.block(function() {                                                                                         // 756
      var self = this;                                                                                                // 757
      return "false";                                                                                                 // 758
    })), "`\n\n`<b>", UI.If(function() {                                                                              // 759
      return Spacebars.call(self.lookup("cond"));                                                                     // 760
    }, UI.block(function() {                                                                                          // 761
      var self = this;                                                                                                // 762
      return "true";                                                                                                  // 763
    }), UI.block(function() {                                                                                         // 764
      var self = this;                                                                                                // 765
      return "false";                                                                                                 // 766
    })), "</b>`\n\n  " ];                                                                                             // 767
  }));                                                                                                                // 768
}));                                                                                                                  // 769
                                                                                                                      // 770
Template.__define__("spacebars_template_test_markdown_each", (function() {                                            // 771
  var self = this;                                                                                                    // 772
  var template = this;                                                                                                // 773
  return Spacebars.include(self.lookupTemplate("markdown"), UI.block(function() {                                     // 774
    var self = this;                                                                                                  // 775
    return [ "\n\n", UI.Each(function() {                                                                             // 776
      return Spacebars.call(self.lookup("seq"));                                                                      // 777
    }, UI.block(function() {                                                                                          // 778
      var self = this;                                                                                                // 779
      return function() {                                                                                             // 780
        return Spacebars.mustache(self.lookup("."));                                                                  // 781
      };                                                                                                              // 782
    })), "\n\n<b>", UI.Each(function() {                                                                              // 783
      return Spacebars.call(self.lookup("seq"));                                                                      // 784
    }, UI.block(function() {                                                                                          // 785
      var self = this;                                                                                                // 786
      return function() {                                                                                             // 787
        return Spacebars.mustache(self.lookup("."));                                                                  // 788
      };                                                                                                              // 789
    })), "</b>\n\n* ", UI.Each(function() {                                                                           // 790
      return Spacebars.call(self.lookup("seq"));                                                                      // 791
    }, UI.block(function() {                                                                                          // 792
      var self = this;                                                                                                // 793
      return function() {                                                                                             // 794
        return Spacebars.mustache(self.lookup("."));                                                                  // 795
      };                                                                                                              // 796
    })), "\n\n* <b>", UI.Each(function() {                                                                            // 797
      return Spacebars.call(self.lookup("seq"));                                                                      // 798
    }, UI.block(function() {                                                                                          // 799
      var self = this;                                                                                                // 800
      return function() {                                                                                             // 801
        return Spacebars.mustache(self.lookup("."));                                                                  // 802
      };                                                                                                              // 803
    })), "</b>\n\nsome paragraph to fix showdown's four space parsing below.\n\n    ", UI.Each(function() {           // 804
      return Spacebars.call(self.lookup("seq"));                                                                      // 805
    }, UI.block(function() {                                                                                          // 806
      var self = this;                                                                                                // 807
      return function() {                                                                                             // 808
        return Spacebars.mustache(self.lookup("."));                                                                  // 809
      };                                                                                                              // 810
    })), "\n\n    <b>", UI.Each(function() {                                                                          // 811
      return Spacebars.call(self.lookup("seq"));                                                                      // 812
    }, UI.block(function() {                                                                                          // 813
      var self = this;                                                                                                // 814
      return function() {                                                                                             // 815
        return Spacebars.mustache(self.lookup("."));                                                                  // 816
      };                                                                                                              // 817
    })), "</b>\n\n`", UI.Each(function() {                                                                            // 818
      return Spacebars.call(self.lookup("seq"));                                                                      // 819
    }, UI.block(function() {                                                                                          // 820
      var self = this;                                                                                                // 821
      return function() {                                                                                             // 822
        return Spacebars.mustache(self.lookup("."));                                                                  // 823
      };                                                                                                              // 824
    })), "`\n\n`<b>", UI.Each(function() {                                                                            // 825
      return Spacebars.call(self.lookup("seq"));                                                                      // 826
    }, UI.block(function() {                                                                                          // 827
      var self = this;                                                                                                // 828
      return function() {                                                                                             // 829
        return Spacebars.mustache(self.lookup("."));                                                                  // 830
      };                                                                                                              // 831
    })), "</b>`\n\n  " ];                                                                                             // 832
  }));                                                                                                                // 833
}));                                                                                                                  // 834
                                                                                                                      // 835
Template.__define__("spacebars_template_test_markdown_inclusion", (function() {                                       // 836
  var self = this;                                                                                                    // 837
  var template = this;                                                                                                // 838
  return Spacebars.include(self.lookupTemplate("markdown"), UI.block(function() {                                     // 839
    var self = this;                                                                                                  // 840
    return [ "\n", Spacebars.include(self.lookupTemplate("spacebars_template_test_markdown_inclusion_subtmpl")), "\n  " ];
  }));                                                                                                                // 842
}));                                                                                                                  // 843
                                                                                                                      // 844
Template.__define__("spacebars_template_test_markdown_inclusion_subtmpl", (function() {                               // 845
  var self = this;                                                                                                    // 846
  var template = this;                                                                                                // 847
  return HTML.SPAN(UI.If(function() {                                                                                 // 848
    return Spacebars.call(self.lookup("foo"));                                                                        // 849
  }, UI.block(function() {                                                                                            // 850
    var self = this;                                                                                                  // 851
    return [ "Foo is ", function() {                                                                                  // 852
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 853
    }, "." ];                                                                                                         // 854
  })));                                                                                                               // 855
}));                                                                                                                  // 856
                                                                                                                      // 857
Template.__define__("spacebars_template_test_markdown_block_helpers", (function() {                                   // 858
  var self = this;                                                                                                    // 859
  var template = this;                                                                                                // 860
  return Spacebars.include(self.lookupTemplate("markdown"), UI.block(function() {                                     // 861
    var self = this;                                                                                                  // 862
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_template_test_just_content"), UI.block(function() {
      var self = this;                                                                                                // 864
      return "\nHi there!\n    ";                                                                                     // 865
    })), "\n  " ];                                                                                                    // 866
  }));                                                                                                                // 867
}));                                                                                                                  // 868
                                                                                                                      // 869
Template.__define__("spacebars_template_test_just_content", (function() {                                             // 870
  var self = this;                                                                                                    // 871
  var template = this;                                                                                                // 872
  return UI.InTemplateScope(template, Spacebars.include(function() {                                                  // 873
    return template.__content;                                                                                        // 874
  }));                                                                                                                // 875
}));                                                                                                                  // 876
                                                                                                                      // 877
Template.__define__("spacebars_template_test_simple_helpers_are_isolated", (function() {                              // 878
  var self = this;                                                                                                    // 879
  var template = this;                                                                                                // 880
  return function() {                                                                                                 // 881
    return Spacebars.mustache(self.lookup("foo"));                                                                    // 882
  };                                                                                                                  // 883
}));                                                                                                                  // 884
                                                                                                                      // 885
Template.__define__("spacebars_template_test_attr_helpers_are_isolated", (function() {                                // 886
  var self = this;                                                                                                    // 887
  var template = this;                                                                                                // 888
  return HTML.P({                                                                                                     // 889
    attr: function() {                                                                                                // 890
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 891
    }                                                                                                                 // 892
  });                                                                                                                 // 893
}));                                                                                                                  // 894
                                                                                                                      // 895
Template.__define__("spacebars_template_test_attr_object_helpers_are_isolated", (function() {                         // 896
  var self = this;                                                                                                    // 897
  var template = this;                                                                                                // 898
  return HTML.P({                                                                                                     // 899
    $dynamic: [ function() {                                                                                          // 900
      return Spacebars.attrMustache(self.lookup("attrs"));                                                            // 901
    } ]                                                                                                               // 902
  });                                                                                                                 // 903
}));                                                                                                                  // 904
                                                                                                                      // 905
Template.__define__("spacebars_template_test_inclusion_helpers_are_isolated", (function() {                           // 906
  var self = this;                                                                                                    // 907
  var template = this;                                                                                                // 908
  return Spacebars.include(self.lookupTemplate("foo"));                                                               // 909
}));                                                                                                                  // 910
                                                                                                                      // 911
Template.__define__("spacebars_template_test_inclusion_helpers_are_isolated_subtemplate", (function() {               // 912
  var self = this;                                                                                                    // 913
  var template = this;                                                                                                // 914
  return "";                                                                                                          // 915
}));                                                                                                                  // 916
                                                                                                                      // 917
Template.__define__("spacebars_template_test_nully_attributes0", (function() {                                        // 918
  var self = this;                                                                                                    // 919
  var template = this;                                                                                                // 920
  return HTML.Raw('<input type="checkbox" checked="" stuff="">');                                                     // 921
}));                                                                                                                  // 922
                                                                                                                      // 923
Template.__define__("spacebars_template_test_nully_attributes1", (function() {                                        // 924
  var self = this;                                                                                                    // 925
  var template = this;                                                                                                // 926
  return HTML.INPUT({                                                                                                 // 927
    type: "checkbox",                                                                                                 // 928
    checked: function() {                                                                                             // 929
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 930
    },                                                                                                                // 931
    stuff: function() {                                                                                               // 932
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 933
    }                                                                                                                 // 934
  });                                                                                                                 // 935
}));                                                                                                                  // 936
                                                                                                                      // 937
Template.__define__("spacebars_template_test_nully_attributes2", (function() {                                        // 938
  var self = this;                                                                                                    // 939
  var template = this;                                                                                                // 940
  return HTML.INPUT({                                                                                                 // 941
    type: "checkbox",                                                                                                 // 942
    checked: [ function() {                                                                                           // 943
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 944
    }, function() {                                                                                                   // 945
      return Spacebars.mustache(self.lookup("bar"));                                                                  // 946
    } ],                                                                                                              // 947
    stuff: [ function() {                                                                                             // 948
      return Spacebars.mustache(self.lookup("foo"));                                                                  // 949
    }, function() {                                                                                                   // 950
      return Spacebars.mustache(self.lookup("bar"));                                                                  // 951
    } ]                                                                                                               // 952
  });                                                                                                                 // 953
}));                                                                                                                  // 954
                                                                                                                      // 955
Template.__define__("spacebars_template_test_nully_attributes3", (function() {                                        // 956
  var self = this;                                                                                                    // 957
  var template = this;                                                                                                // 958
  return HTML.INPUT({                                                                                                 // 959
    type: "checkbox",                                                                                                 // 960
    checked: UI.If(function() {                                                                                       // 961
      return Spacebars.call(self.lookup("foo"));                                                                      // 962
    }, UI.block(function() {                                                                                          // 963
      var self = this;                                                                                                // 964
      return null;                                                                                                    // 965
    })),                                                                                                              // 966
    stuff: UI.If(function() {                                                                                         // 967
      return Spacebars.call(self.lookup("foo"));                                                                      // 968
    }, UI.block(function() {                                                                                          // 969
      var self = this;                                                                                                // 970
      return null;                                                                                                    // 971
    }))                                                                                                               // 972
  });                                                                                                                 // 973
}));                                                                                                                  // 974
                                                                                                                      // 975
Template.__define__("spacebars_template_test_double", (function() {                                                   // 976
  var self = this;                                                                                                    // 977
  var template = this;                                                                                                // 978
  return function() {                                                                                                 // 979
    return Spacebars.mustache(self.lookup("foo"));                                                                    // 980
  };                                                                                                                  // 981
}));                                                                                                                  // 982
                                                                                                                      // 983
Template.__define__("spacebars_template_test_inclusion_lookup", (function() {                                         // 984
  var self = this;                                                                                                    // 985
  var template = this;                                                                                                // 986
  return [ Spacebars.include(self.lookupTemplate("spacebars_template_test_inclusion_lookup_subtmpl")), "\n  ", Spacebars.include(self.lookupTemplate("dataContextSubtmpl")) ];
}));                                                                                                                  // 988
                                                                                                                      // 989
Template.__define__("spacebars_template_test_inclusion_lookup_subtmpl", (function() {                                 // 990
  var self = this;                                                                                                    // 991
  var template = this;                                                                                                // 992
  return "This is the template.";                                                                                     // 993
}));                                                                                                                  // 994
                                                                                                                      // 995
Template.__define__("spacebars_template_test_inclusion_lookup_subtmpl2", (function() {                                // 996
  var self = this;                                                                                                    // 997
  var template = this;                                                                                                // 998
  return "This is generated by a helper with the same name.";                                                         // 999
}));                                                                                                                  // 1000
                                                                                                                      // 1001
Template.__define__("spacebars_template_test_inclusion_lookup_subtmpl3", (function() {                                // 1002
  var self = this;                                                                                                    // 1003
  var template = this;                                                                                                // 1004
  return "This is a template passed in the data context.";                                                            // 1005
}));                                                                                                                  // 1006
                                                                                                                      // 1007
Template.__define__("spacebars_template_test_content_context", (function() {                                          // 1008
  var self = this;                                                                                                    // 1009
  var template = this;                                                                                                // 1010
  return Spacebars.With(function() {                                                                                  // 1011
    return Spacebars.call(self.lookup("foo"));                                                                        // 1012
  }, UI.block(function() {                                                                                            // 1013
    var self = this;                                                                                                  // 1014
    return [ "\n    ", Spacebars.With(function() {                                                                    // 1015
      return Spacebars.call(self.lookup("bar"));                                                                      // 1016
    }, UI.block(function() {                                                                                          // 1017
      var self = this;                                                                                                // 1018
      return [ "\n      ", Spacebars.TemplateWith(function() {                                                        // 1019
        return {                                                                                                      // 1020
          condition: Spacebars.call(self.lookup("cond"))                                                              // 1021
        };                                                                                                            // 1022
      }, UI.block(function() {                                                                                        // 1023
        var self = this;                                                                                              // 1024
        return Spacebars.include(self.lookupTemplate("spacebars_template_test_iftemplate"), UI.block(function() {     // 1025
          var self = this;                                                                                            // 1026
          return [ "\n        ", function() {                                                                         // 1027
            return Spacebars.mustache(self.lookup("firstLetter"));                                                    // 1028
          }, function() {                                                                                             // 1029
            return Spacebars.mustache(Spacebars.dot(self.lookup(".."), "secondLetter"));                              // 1030
          }, "\n      " ];                                                                                            // 1031
        }), UI.block(function() {                                                                                     // 1032
          var self = this;                                                                                            // 1033
          return [ "\n        ", function() {                                                                         // 1034
            return Spacebars.mustache(Spacebars.dot(self.lookup(".."), "firstLetter"));                               // 1035
          }, function() {                                                                                             // 1036
            return Spacebars.mustache(self.lookup("secondLetter"));                                                   // 1037
          }, "\n      " ];                                                                                            // 1038
        }));                                                                                                          // 1039
      })), "\n    " ];                                                                                                // 1040
    })), "\n  " ];                                                                                                    // 1041
  }));                                                                                                                // 1042
}));                                                                                                                  // 1043
                                                                                                                      // 1044
Template.__define__("spacebars_test_control_input", (function() {                                                     // 1045
  var self = this;                                                                                                    // 1046
  var template = this;                                                                                                // 1047
  return HTML.INPUT({                                                                                                 // 1048
    type: function() {                                                                                                // 1049
      return Spacebars.mustache(self.lookup("type"));                                                                 // 1050
    },                                                                                                                // 1051
    value: function() {                                                                                               // 1052
      return Spacebars.mustache(self.lookup("value"));                                                                // 1053
    }                                                                                                                 // 1054
  });                                                                                                                 // 1055
}));                                                                                                                  // 1056
                                                                                                                      // 1057
Template.__define__("spacebars_test_control_textarea", (function() {                                                  // 1058
  var self = this;                                                                                                    // 1059
  var template = this;                                                                                                // 1060
  return HTML.TEXTAREA(function() {                                                                                   // 1061
    return Spacebars.mustache(self.lookup("value"));                                                                  // 1062
  });                                                                                                                 // 1063
}));                                                                                                                  // 1064
                                                                                                                      // 1065
Template.__define__("spacebars_test_control_select", (function() {                                                    // 1066
  var self = this;                                                                                                    // 1067
  var template = this;                                                                                                // 1068
  return HTML.SELECT("\n    ", UI.Each(function() {                                                                   // 1069
    return Spacebars.call(self.lookup("options"));                                                                    // 1070
  }, UI.block(function() {                                                                                            // 1071
    var self = this;                                                                                                  // 1072
    return [ "\n      ", HTML.OPTION({                                                                                // 1073
      selected: function() {                                                                                          // 1074
        return Spacebars.mustache(self.lookup("selected"));                                                           // 1075
      }                                                                                                               // 1076
    }, function() {                                                                                                   // 1077
      return Spacebars.mustache(self.lookup("."));                                                                    // 1078
    }), "\n    " ];                                                                                                   // 1079
  })), "\n  ");                                                                                                       // 1080
}));                                                                                                                  // 1081
                                                                                                                      // 1082
Template.__define__("spacebars_test_control_radio", (function() {                                                     // 1083
  var self = this;                                                                                                    // 1084
  var template = this;                                                                                                // 1085
  return [ "Band:\n\n  ", UI.Each(function() {                                                                        // 1086
    return Spacebars.call(self.lookup("bands"));                                                                      // 1087
  }, UI.block(function() {                                                                                            // 1088
    var self = this;                                                                                                  // 1089
    return [ "\n    ", HTML.INPUT({                                                                                   // 1090
      name: "bands",                                                                                                  // 1091
      type: "radio",                                                                                                  // 1092
      value: function() {                                                                                             // 1093
        return Spacebars.mustache(self.lookup("."));                                                                  // 1094
      },                                                                                                              // 1095
      checked: function() {                                                                                           // 1096
        return Spacebars.mustache(self.lookup("isChecked"));                                                          // 1097
      }                                                                                                               // 1098
    }), "\n  " ];                                                                                                     // 1099
  })), "\n\n  ", function() {                                                                                         // 1100
    return Spacebars.mustache(self.lookup("band"));                                                                   // 1101
  } ];                                                                                                                // 1102
}));                                                                                                                  // 1103
                                                                                                                      // 1104
Template.__define__("spacebars_test_control_checkbox", (function() {                                                  // 1105
  var self = this;                                                                                                    // 1106
  var template = this;                                                                                                // 1107
  return UI.Each(function() {                                                                                         // 1108
    return Spacebars.call(self.lookup("labels"));                                                                     // 1109
  }, UI.block(function() {                                                                                            // 1110
    var self = this;                                                                                                  // 1111
    return [ "\n    ", HTML.INPUT({                                                                                   // 1112
      type: "checkbox",                                                                                               // 1113
      value: function() {                                                                                             // 1114
        return Spacebars.mustache(self.lookup("."));                                                                  // 1115
      },                                                                                                              // 1116
      checked: function() {                                                                                           // 1117
        return Spacebars.mustache(self.lookup("isChecked"));                                                          // 1118
      }                                                                                                               // 1119
    }), "\n  " ];                                                                                                     // 1120
  }));                                                                                                                // 1121
}));                                                                                                                  // 1122
                                                                                                                      // 1123
Template.__define__("spacebars_test_nonexistent_template", (function() {                                              // 1124
  var self = this;                                                                                                    // 1125
  var template = this;                                                                                                // 1126
  return Spacebars.include(self.lookupTemplate("this_template_lives_in_outer_space"));                                // 1127
}));                                                                                                                  // 1128
                                                                                                                      // 1129
Template.__define__("spacebars_test_if_helper", (function() {                                                         // 1130
  var self = this;                                                                                                    // 1131
  var template = this;                                                                                                // 1132
  return UI.If(function() {                                                                                           // 1133
    return Spacebars.call(self.lookup("foo"));                                                                        // 1134
  }, UI.block(function() {                                                                                            // 1135
    var self = this;                                                                                                  // 1136
    return "\n    true\n  ";                                                                                          // 1137
  }), UI.block(function() {                                                                                           // 1138
    var self = this;                                                                                                  // 1139
    return "\n    false\n  ";                                                                                         // 1140
  }));                                                                                                                // 1141
}));                                                                                                                  // 1142
                                                                                                                      // 1143
Template.__define__("spacebars_test_block_helper_function", (function() {                                             // 1144
  var self = this;                                                                                                    // 1145
  var template = this;                                                                                                // 1146
  return Spacebars.include(self.lookupTemplate("foo"), UI.block(function() {                                          // 1147
    var self = this;                                                                                                  // 1148
    return "\n  ";                                                                                                    // 1149
  }));                                                                                                                // 1150
}));                                                                                                                  // 1151
                                                                                                                      // 1152
Template.__define__("spacebars_test_helpers_stop_onetwo", (function() {                                               // 1153
  var self = this;                                                                                                    // 1154
  var template = this;                                                                                                // 1155
  return UI.If(function() {                                                                                           // 1156
    return Spacebars.call(self.lookup("showOne"));                                                                    // 1157
  }, UI.block(function() {                                                                                            // 1158
    var self = this;                                                                                                  // 1159
    return [ "\n    ", Spacebars.include(self.lookupTemplate("one")), "\n  " ];                                       // 1160
  }), UI.block(function() {                                                                                           // 1161
    var self = this;                                                                                                  // 1162
    return [ "\n    ", Spacebars.include(self.lookupTemplate("two")), "\n  " ];                                       // 1163
  }));                                                                                                                // 1164
}));                                                                                                                  // 1165
                                                                                                                      // 1166
Template.__define__("spacebars_test_helpers_stop_onetwo_attribute", (function() {                                     // 1167
  var self = this;                                                                                                    // 1168
  var template = this;                                                                                                // 1169
  return HTML.BR({                                                                                                    // 1170
    "data-stuff": UI.If(function() {                                                                                  // 1171
      return Spacebars.call(self.lookup("showOne"));                                                                  // 1172
    }, UI.block(function() {                                                                                          // 1173
      var self = this;                                                                                                // 1174
      return [ "\n    ", Spacebars.include(self.lookupTemplate("one")), "\n  " ];                                     // 1175
    }), UI.block(function() {                                                                                         // 1176
      var self = this;                                                                                                // 1177
      return [ "\n    ", Spacebars.include(self.lookupTemplate("two")), "\n  " ];                                     // 1178
    }))                                                                                                               // 1179
  });                                                                                                                 // 1180
}));                                                                                                                  // 1181
                                                                                                                      // 1182
Template.__define__("spacebars_test_helpers_stop_with1", (function() {                                                // 1183
  var self = this;                                                                                                    // 1184
  var template = this;                                                                                                // 1185
  return Spacebars.With(function() {                                                                                  // 1186
    return Spacebars.call(self.lookup("options"));                                                                    // 1187
  }, UI.block(function() {                                                                                            // 1188
    var self = this;                                                                                                  // 1189
    return "\n    one\n  ";                                                                                           // 1190
  }));                                                                                                                // 1191
}));                                                                                                                  // 1192
                                                                                                                      // 1193
Template.__define__("spacebars_test_helpers_stop_with2", (function() {                                                // 1194
  var self = this;                                                                                                    // 1195
  var template = this;                                                                                                // 1196
  return Spacebars.With(function() {                                                                                  // 1197
    return Spacebars.call(self.lookup("options"));                                                                    // 1198
  }, UI.block(function() {                                                                                            // 1199
    var self = this;                                                                                                  // 1200
    return "\n    two\n  ";                                                                                           // 1201
  }));                                                                                                                // 1202
}));                                                                                                                  // 1203
                                                                                                                      // 1204
Template.__define__("spacebars_test_helpers_stop_each1", (function() {                                                // 1205
  var self = this;                                                                                                    // 1206
  var template = this;                                                                                                // 1207
  return UI.Each(function() {                                                                                         // 1208
    return Spacebars.call(self.lookup("options"));                                                                    // 1209
  }, UI.block(function() {                                                                                            // 1210
    var self = this;                                                                                                  // 1211
    return "\n    one\n  ";                                                                                           // 1212
  }));                                                                                                                // 1213
}));                                                                                                                  // 1214
                                                                                                                      // 1215
Template.__define__("spacebars_test_helpers_stop_each2", (function() {                                                // 1216
  var self = this;                                                                                                    // 1217
  var template = this;                                                                                                // 1218
  return UI.Each(function() {                                                                                         // 1219
    return Spacebars.call(self.lookup("options"));                                                                    // 1220
  }, UI.block(function() {                                                                                            // 1221
    var self = this;                                                                                                  // 1222
    return "\n    two\n  ";                                                                                           // 1223
  }));                                                                                                                // 1224
}));                                                                                                                  // 1225
                                                                                                                      // 1226
Template.__define__("spacebars_test_helpers_stop_with_each1", (function() {                                           // 1227
  var self = this;                                                                                                    // 1228
  var template = this;                                                                                                // 1229
  return Spacebars.With(function() {                                                                                  // 1230
    return Spacebars.call(self.lookup("options"));                                                                    // 1231
  }, UI.block(function() {                                                                                            // 1232
    var self = this;                                                                                                  // 1233
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_test_helpers_stop_with_each3")), "\n  " ];    // 1234
  }));                                                                                                                // 1235
}));                                                                                                                  // 1236
                                                                                                                      // 1237
Template.__define__("spacebars_test_helpers_stop_with_each2", (function() {                                           // 1238
  var self = this;                                                                                                    // 1239
  var template = this;                                                                                                // 1240
  return Spacebars.With(function() {                                                                                  // 1241
    return Spacebars.call(self.lookup("options"));                                                                    // 1242
  }, UI.block(function() {                                                                                            // 1243
    var self = this;                                                                                                  // 1244
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_test_helpers_stop_with_each3")), "\n  " ];    // 1245
  }));                                                                                                                // 1246
}));                                                                                                                  // 1247
                                                                                                                      // 1248
Template.__define__("spacebars_test_helpers_stop_with_each3", (function() {                                           // 1249
  var self = this;                                                                                                    // 1250
  var template = this;                                                                                                // 1251
  return UI.Each(function() {                                                                                         // 1252
    return Spacebars.call(self.lookup("."));                                                                          // 1253
  }, UI.block(function() {                                                                                            // 1254
    var self = this;                                                                                                  // 1255
    return "\n  ";                                                                                                    // 1256
  }));                                                                                                                // 1257
}));                                                                                                                  // 1258
                                                                                                                      // 1259
Template.__define__("spacebars_test_helpers_stop_if1", (function() {                                                  // 1260
  var self = this;                                                                                                    // 1261
  var template = this;                                                                                                // 1262
  return UI.If(function() {                                                                                           // 1263
    return Spacebars.call(self.lookup("options"));                                                                    // 1264
  }, UI.block(function() {                                                                                            // 1265
    var self = this;                                                                                                  // 1266
    return "\n    one\n  ";                                                                                           // 1267
  }));                                                                                                                // 1268
}));                                                                                                                  // 1269
                                                                                                                      // 1270
Template.__define__("spacebars_test_helpers_stop_if2", (function() {                                                  // 1271
  var self = this;                                                                                                    // 1272
  var template = this;                                                                                                // 1273
  return UI.If(function() {                                                                                           // 1274
    return Spacebars.call(self.lookup("options"));                                                                    // 1275
  }, UI.block(function() {                                                                                            // 1276
    var self = this;                                                                                                  // 1277
    return "\n    two\n  ";                                                                                           // 1278
  }));                                                                                                                // 1279
}));                                                                                                                  // 1280
                                                                                                                      // 1281
Template.__define__("spacebars_test_helpers_stop_inclusion1", (function() {                                           // 1282
  var self = this;                                                                                                    // 1283
  var template = this;                                                                                                // 1284
  return Spacebars.include(self.lookupTemplate("options"));                                                           // 1285
}));                                                                                                                  // 1286
                                                                                                                      // 1287
Template.__define__("spacebars_test_helpers_stop_inclusion2", (function() {                                           // 1288
  var self = this;                                                                                                    // 1289
  var template = this;                                                                                                // 1290
  return Spacebars.include(self.lookupTemplate("options"));                                                           // 1291
}));                                                                                                                  // 1292
                                                                                                                      // 1293
Template.__define__("spacebars_test_helpers_stop_inclusion3", (function() {                                           // 1294
  var self = this;                                                                                                    // 1295
  var template = this;                                                                                                // 1296
  return "blah";                                                                                                      // 1297
}));                                                                                                                  // 1298
                                                                                                                      // 1299
Template.__define__("spacebars_test_helpers_stop_with_callbacks1", (function() {                                      // 1300
  var self = this;                                                                                                    // 1301
  var template = this;                                                                                                // 1302
  return Spacebars.With(function() {                                                                                  // 1303
    return Spacebars.call(self.lookup("options"));                                                                    // 1304
  }, UI.block(function() {                                                                                            // 1305
    var self = this;                                                                                                  // 1306
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_test_helpers_stop_with_callbacks3")), "\n  " ];
  }));                                                                                                                // 1308
}));                                                                                                                  // 1309
                                                                                                                      // 1310
Template.__define__("spacebars_test_helpers_stop_with_callbacks2", (function() {                                      // 1311
  var self = this;                                                                                                    // 1312
  var template = this;                                                                                                // 1313
  return Spacebars.With(function() {                                                                                  // 1314
    return Spacebars.call(self.lookup("options"));                                                                    // 1315
  }, UI.block(function() {                                                                                            // 1316
    var self = this;                                                                                                  // 1317
    return [ "\n    ", Spacebars.include(self.lookupTemplate("spacebars_test_helpers_stop_with_callbacks3")), "\n  " ];
  }));                                                                                                                // 1319
}));                                                                                                                  // 1320
                                                                                                                      // 1321
Template.__define__("spacebars_test_helpers_stop_with_callbacks3", (function() {                                      // 1322
  var self = this;                                                                                                    // 1323
  var template = this;                                                                                                // 1324
  return "blah";                                                                                                      // 1325
}));                                                                                                                  // 1326
                                                                                                                      // 1327
Template.__define__("spacebars_test_helpers_stop_unless1", (function() {                                              // 1328
  var self = this;                                                                                                    // 1329
  var template = this;                                                                                                // 1330
  return UI.Unless(function() {                                                                                       // 1331
    return Spacebars.call(self.lookup("options"));                                                                    // 1332
  }, UI.block(function() {                                                                                            // 1333
    var self = this;                                                                                                  // 1334
    return "\n    one\n  ";                                                                                           // 1335
  }));                                                                                                                // 1336
}));                                                                                                                  // 1337
                                                                                                                      // 1338
Template.__define__("spacebars_test_helpers_stop_unless2", (function() {                                              // 1339
  var self = this;                                                                                                    // 1340
  var template = this;                                                                                                // 1341
  return UI.Unless(function() {                                                                                       // 1342
    return Spacebars.call(self.lookup("options"));                                                                    // 1343
  }, UI.block(function() {                                                                                            // 1344
    var self = this;                                                                                                  // 1345
    return "\n    two\n  ";                                                                                           // 1346
  }));                                                                                                                // 1347
}));                                                                                                                  // 1348
                                                                                                                      // 1349
Template.__define__("spacebars_test_no_data_context", (function() {                                                   // 1350
  var self = this;                                                                                                    // 1351
  var template = this;                                                                                                // 1352
  return [ HTML.Raw("<button></button>\n  "), function() {                                                            // 1353
    return Spacebars.mustache(self.lookup("foo"));                                                                    // 1354
  } ];                                                                                                                // 1355
}));                                                                                                                  // 1356
                                                                                                                      // 1357
Template.__define__("spacebars_test_falsy_with", (function() {                                                        // 1358
  var self = this;                                                                                                    // 1359
  var template = this;                                                                                                // 1360
  return Spacebars.With(function() {                                                                                  // 1361
    return Spacebars.call(self.lookup("obj"));                                                                        // 1362
  }, UI.block(function() {                                                                                            // 1363
    var self = this;                                                                                                  // 1364
    return [ "\n    ", function() {                                                                                   // 1365
      return Spacebars.mustache(self.lookup("greekLetter"));                                                          // 1366
    }, "\n  " ];                                                                                                      // 1367
  }));                                                                                                                // 1368
}));                                                                                                                  // 1369
                                                                                                                      // 1370
Template.__define__("spacebars_test_helpers_dont_leak", (function() {                                                 // 1371
  var self = this;                                                                                                    // 1372
  var template = this;                                                                                                // 1373
  return Spacebars.TemplateWith(function() {                                                                          // 1374
    return {                                                                                                          // 1375
      foo: Spacebars.call("correct")                                                                                  // 1376
    };                                                                                                                // 1377
  }, UI.block(function() {                                                                                            // 1378
    var self = this;                                                                                                  // 1379
    return Spacebars.include(self.lookupTemplate("spacebars_test_helpers_dont_leak2"));                               // 1380
  }));                                                                                                                // 1381
}));                                                                                                                  // 1382
                                                                                                                      // 1383
Template.__define__("spacebars_test_helpers_dont_leak2", (function() {                                                // 1384
  var self = this;                                                                                                    // 1385
  var template = this;                                                                                                // 1386
  return [ function() {                                                                                               // 1387
    return Spacebars.mustache(self.lookup("foo"));                                                                    // 1388
  }, function() {                                                                                                     // 1389
    return Spacebars.mustache(self.lookup("bar"));                                                                    // 1390
  }, " ", Spacebars.include(self.lookupTemplate("spacebars_template_test_content"), UI.block(function() {             // 1391
    var self = this;                                                                                                  // 1392
    return function() {                                                                                               // 1393
      return Spacebars.mustache(self.lookup("bonus"));                                                                // 1394
    };                                                                                                                // 1395
  })) ];                                                                                                              // 1396
}));                                                                                                                  // 1397
                                                                                                                      // 1398
Template.__define__("spacebars_test_event_returns_false", (function() {                                               // 1399
  var self = this;                                                                                                    // 1400
  var template = this;                                                                                                // 1401
  return HTML.Raw('<a href="#bad-url" id="spacebars_test_event_returns_false_link">click me</a>');                    // 1402
}));                                                                                                                  // 1403
                                                                                                                      // 1404
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// packages/spacebars-tests/template_tests.js                                                                         //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
var renderToDiv = function (comp) {                                                                                   // 1
  var div = document.createElement("DIV");                                                                            // 2
  UI.materialize(comp, div);                                                                                          // 3
  return div;                                                                                                         // 4
};                                                                                                                    // 5
                                                                                                                      // 6
var divRendersTo = function (test, div, html) {                                                                       // 7
  Deps.flush({_throwFirstError: true});                                                                               // 8
  var actual = canonicalizeHtml(div.innerHTML);                                                                       // 9
  test.equal(actual, html);                                                                                           // 10
};                                                                                                                    // 11
                                                                                                                      // 12
var nodesToArray = function (array) {                                                                                 // 13
  // Starting in underscore 1.4, _.toArray does not work right on a node                                              // 14
  // list in IE8. This is a workaround to support IE8.                                                                // 15
  return _.map(array, _.identity);                                                                                    // 16
};                                                                                                                    // 17
                                                                                                                      // 18
var clickIt = function (elem) {                                                                                       // 19
  // jQuery's bubbling change event polyfill for IE 8 seems                                                           // 20
  // to require that the element in question have focus when                                                          // 21
  // it receives a simulated click.                                                                                   // 22
  if (elem.focus)                                                                                                     // 23
    elem.focus();                                                                                                     // 24
  clickElement(elem);                                                                                                 // 25
};                                                                                                                    // 26
                                                                                                                      // 27
Tinytest.add("spacebars - templates - simple helper", function (test) {                                               // 28
  var tmpl = Template.spacebars_template_test_simple_helper;                                                          // 29
  var R = ReactiveVar(1);                                                                                             // 30
  tmpl.foo = function (x) {                                                                                           // 31
    return x + R.get();                                                                                               // 32
  };                                                                                                                  // 33
  tmpl.bar = function () {                                                                                            // 34
    return 123;                                                                                                       // 35
  };                                                                                                                  // 36
  var div = renderToDiv(tmpl);                                                                                        // 37
                                                                                                                      // 38
  test.equal(canonicalizeHtml(div.innerHTML), "124");                                                                 // 39
  R.set(2);                                                                                                           // 40
  Deps.flush();                                                                                                       // 41
  test.equal(canonicalizeHtml(div.innerHTML), "125");                                                                 // 42
                                                                                                                      // 43
  // Test that `{{foo bar}}` throws if `foo` is missing or not a function.                                            // 44
  tmpl.foo = 3;                                                                                                       // 45
  test.throws(function () {                                                                                           // 46
    renderToDiv(tmpl);                                                                                                // 47
  });                                                                                                                 // 48
                                                                                                                      // 49
  delete tmpl.foo;                                                                                                    // 50
  // We'd like this to throw, but it doesn't because of how self.lookup                                               // 51
  // works.  D'oh.  Fix this as part of "new this".                                                                   // 52
  //test.throws(function () {                                                                                         // 53
    renderToDiv(tmpl);                                                                                                // 54
  //});                                                                                                               // 55
                                                                                                                      // 56
  tmpl.foo = function () {};                                                                                          // 57
  // doesn't throw                                                                                                    // 58
  var div = renderToDiv(tmpl);                                                                                        // 59
  test.equal(canonicalizeHtml(div.innerHTML), '');                                                                    // 60
});                                                                                                                   // 61
                                                                                                                      // 62
Tinytest.add("spacebars - templates - dynamic template", function (test) {                                            // 63
  var tmpl = Template.spacebars_template_test_dynamic_template;                                                       // 64
  var aaa = Template.spacebars_template_test_aaa;                                                                     // 65
  var bbb = Template.spacebars_template_test_bbb;                                                                     // 66
  var R = ReactiveVar("aaa");                                                                                         // 67
  tmpl.foo = function () {                                                                                            // 68
    return R.get() === 'aaa' ? aaa : bbb;                                                                             // 69
  };                                                                                                                  // 70
  var div = renderToDiv(tmpl);                                                                                        // 71
  test.equal(canonicalizeHtml(div.innerHTML), "aaa");                                                                 // 72
                                                                                                                      // 73
  R.set('bbb');                                                                                                       // 74
  Deps.flush();                                                                                                       // 75
                                                                                                                      // 76
  test.equal(canonicalizeHtml(div.innerHTML), "bbb");                                                                 // 77
});                                                                                                                   // 78
                                                                                                                      // 79
Tinytest.add("spacebars - templates - interpolate attribute", function (test) {                                       // 80
  var tmpl = Template.spacebars_template_test_interpolate_attribute;                                                  // 81
  tmpl.foo = function (x) {                                                                                           // 82
    return x+1;                                                                                                       // 83
  };                                                                                                                  // 84
  tmpl.bar = function () {                                                                                            // 85
    return 123;                                                                                                       // 86
  };                                                                                                                  // 87
  var div = renderToDiv(tmpl);                                                                                        // 88
                                                                                                                      // 89
  test.equal($(div).find('div')[0].className, "aaa124zzz");                                                           // 90
});                                                                                                                   // 91
                                                                                                                      // 92
Tinytest.add("spacebars - templates - dynamic attrs", function (test) {                                               // 93
  var tmpl = Template.spacebars_template_test_dynamic_attrs;                                                          // 94
                                                                                                                      // 95
  var R2 = ReactiveVar({x: "X"});                                                                                     // 96
  var R3 = ReactiveVar('selected');                                                                                   // 97
  tmpl.attrsObj = function () { return R2.get(); };                                                                   // 98
  tmpl.singleAttr = function () { return R3.get(); };                                                                 // 99
                                                                                                                      // 100
  var div = renderToDiv(tmpl);                                                                                        // 101
  var span = $(div).find('span')[0];                                                                                  // 102
  test.equal(span.innerHTML, 'hi');                                                                                   // 103
  test.isTrue(span.hasAttribute('selected'));                                                                         // 104
  test.equal(span.getAttribute('x'), 'X');                                                                            // 105
                                                                                                                      // 106
  R2.set({y: "Y", z: "Z"});                                                                                           // 107
  R3.set('');                                                                                                         // 108
  Deps.flush();                                                                                                       // 109
  test.equal(canonicalizeHtml(span.innerHTML), 'hi');                                                                 // 110
  test.isFalse(span.hasAttribute('selected'));                                                                        // 111
  test.isFalse(span.hasAttribute('x'));                                                                               // 112
  test.equal(span.getAttribute('y'), 'Y');                                                                            // 113
  test.equal(span.getAttribute('z'), 'Z');                                                                            // 114
});                                                                                                                   // 115
                                                                                                                      // 116
Tinytest.add("spacebars - templates - triple", function (test) {                                                      // 117
  var tmpl = Template.spacebars_template_test_triple;                                                                 // 118
                                                                                                                      // 119
  var R = ReactiveVar('<span class="hi">blah</span>');                                                                // 120
  tmpl.html = function () { return R.get(); };                                                                        // 121
                                                                                                                      // 122
  var div = renderToDiv(tmpl);                                                                                        // 123
  var elems = $(div).find("> *");                                                                                     // 124
  test.equal(elems.length, 1);                                                                                        // 125
  test.equal(elems[0].nodeName, 'SPAN');                                                                              // 126
  var span = elems[0];                                                                                                // 127
  test.equal(span.className, 'hi');                                                                                   // 128
  test.equal(span.innerHTML, 'blah');                                                                                 // 129
                                                                                                                      // 130
  R.set('asdf');                                                                                                      // 131
  Deps.flush();                                                                                                       // 132
  elems = $(div).find("> *");                                                                                         // 133
  test.equal(elems.length, 0);                                                                                        // 134
  test.equal(canonicalizeHtml(div.innerHTML), 'asdf');                                                                // 135
                                                                                                                      // 136
  R.set('<span class="hi">blah</span>');                                                                              // 137
  Deps.flush();                                                                                                       // 138
  elems = $(div).find("> *");                                                                                         // 139
  test.equal(elems.length, 1);                                                                                        // 140
  test.equal(elems[0].nodeName, 'SPAN');                                                                              // 141
  span = elems[0];                                                                                                    // 142
  test.equal(span.className, 'hi');                                                                                   // 143
  test.equal(canonicalizeHtml(span.innerHTML), 'blah');                                                               // 144
                                                                                                                      // 145
  var tmpl = Template.spacebars_template_test_triple2;                                                                // 146
  tmpl.html = function () {};                                                                                         // 147
  tmpl.html2 = function () { return null; };                                                                          // 148
  // no tmpl.html3                                                                                                    // 149
  div = renderToDiv(tmpl);                                                                                            // 150
  test.equal(canonicalizeHtml(div.innerHTML), 'xy');                                                                  // 151
});                                                                                                                   // 152
                                                                                                                      // 153
Tinytest.add("spacebars - templates - inclusion args", function (test) {                                              // 154
  var tmpl = Template.spacebars_template_test_inclusion_args;                                                         // 155
                                                                                                                      // 156
  var R = ReactiveVar(Template.spacebars_template_test_aaa);                                                          // 157
  tmpl.foo = function () { return R.get(); };                                                                         // 158
                                                                                                                      // 159
  var div = renderToDiv(tmpl);                                                                                        // 160
  // `{{> foo bar}}`, with `foo` resolving to Template.aaa,                                                           // 161
  // which consists of "aaa"                                                                                          // 162
  test.equal(canonicalizeHtml(div.innerHTML), 'aaa');                                                                 // 163
  R.set(Template.spacebars_template_test_bbb);                                                                        // 164
  Deps.flush();                                                                                                       // 165
  test.equal(canonicalizeHtml(div.innerHTML), 'bbb');                                                                 // 166
                                                                                                                      // 167
  ////// Ok, now `foo` *is* Template.aaa                                                                              // 168
  tmpl.foo = Template.spacebars_template_test_aaa;                                                                    // 169
  div = renderToDiv(tmpl);                                                                                            // 170
  test.equal(canonicalizeHtml(div.innerHTML), 'aaa');                                                                 // 171
                                                                                                                      // 172
  ////// Ok, now `foo` is a template that takes an argument; bar is a string.                                         // 173
  tmpl.foo = Template.spacebars_template_test_bracketed_this;                                                         // 174
  tmpl.bar = 'david';                                                                                                 // 175
  div = renderToDiv(tmpl);                                                                                            // 176
  test.equal(canonicalizeHtml(div.innerHTML), '[david]');                                                             // 177
                                                                                                                      // 178
  ////// Now `foo` is a template that takes an arg; bar is a function.                                                // 179
  tmpl.foo = Template.spacebars_template_test_span_this;                                                              // 180
  R = ReactiveVar('david');                                                                                           // 181
  tmpl.bar = function () { return R.get(); };                                                                         // 182
  div = renderToDiv(tmpl);                                                                                            // 183
  test.equal(canonicalizeHtml(div.innerHTML), '<span>david</span>');                                                  // 184
  var span1 = div.querySelector('span');                                                                              // 185
  R.set('avi');                                                                                                       // 186
  Deps.flush();                                                                                                       // 187
  test.equal(canonicalizeHtml(div.innerHTML), '<span>avi</span>');                                                    // 188
  var span2 = div.querySelector('span');                                                                              // 189
  test.isTrue(span1 === span2);                                                                                       // 190
});                                                                                                                   // 191
                                                                                                                      // 192
Tinytest.add("spacebars - templates - inclusion args 2", function (test) {                                            // 193
  // `{{> foo bar q=baz}}`                                                                                            // 194
  var tmpl = Template.spacebars_template_test_inclusion_args2;                                                        // 195
                                                                                                                      // 196
  tmpl.foo = Template.spacebars_template_test_span_this;                                                              // 197
  tmpl.bar = function (options) {                                                                                     // 198
    return options.hash.q;                                                                                            // 199
  };                                                                                                                  // 200
                                                                                                                      // 201
  var R = ReactiveVar('david!');                                                                                      // 202
  tmpl.baz = function () { return R.get().slice(0,5); };                                                              // 203
  var div = renderToDiv(tmpl);                                                                                        // 204
  test.equal(canonicalizeHtml(div.innerHTML), '<span>david</span>');                                                  // 205
  var span1 = div.querySelector('span');                                                                              // 206
  R.set('brillo');                                                                                                    // 207
  Deps.flush();                                                                                                       // 208
  test.equal(canonicalizeHtml(div.innerHTML), '<span>brill</span>');                                                  // 209
  var span2 = div.querySelector('span');                                                                              // 210
  test.isTrue(span1 === span2);                                                                                       // 211
});                                                                                                                   // 212
                                                                                                                      // 213
Tinytest.add("spacebars - templates - inclusion dotted args", function (test) {                                       // 214
  // `{{> foo bar.baz}}`                                                                                              // 215
  var tmpl = Template.spacebars_template_test_inclusion_dotted_args;                                                  // 216
                                                                                                                      // 217
  var initCount = 0;                                                                                                  // 218
  tmpl.foo = Template.spacebars_template_test_bracketed_this.extend({                                                 // 219
    init: function () { initCount++; }                                                                                // 220
  });                                                                                                                 // 221
  var R = ReactiveVar('david');                                                                                       // 222
  tmpl.bar = function () {                                                                                            // 223
    // make sure `this` is bound correctly                                                                            // 224
    return { baz: this.symbol + R.get() };                                                                            // 225
  };                                                                                                                  // 226
                                                                                                                      // 227
  var div = renderToDiv(tmpl.extend({data: {symbol:'%'}}));                                                           // 228
  test.equal(initCount, 1);                                                                                           // 229
  test.equal(canonicalizeHtml(div.innerHTML), '[%david]');                                                            // 230
                                                                                                                      // 231
  R.set('avi');                                                                                                       // 232
  Deps.flush();                                                                                                       // 233
  test.equal(canonicalizeHtml(div.innerHTML), '[%avi]');                                                              // 234
  // check that invalidating the argument to `foo` doesn't require                                                    // 235
  // creating a new `foo`.                                                                                            // 236
  test.equal(initCount, 1);                                                                                           // 237
});                                                                                                                   // 238
                                                                                                                      // 239
Tinytest.add("spacebars - templates - inclusion slashed args", function (test) {                                      // 240
  // `{{> foo bar/baz}}`                                                                                              // 241
  var tmpl = Template.spacebars_template_test_inclusion_dotted_args;                                                  // 242
                                                                                                                      // 243
  var initCount = 0;                                                                                                  // 244
  tmpl.foo = Template.spacebars_template_test_bracketed_this.extend({                                                 // 245
    init: function () { initCount++; }                                                                                // 246
  });                                                                                                                 // 247
  var R = ReactiveVar('david');                                                                                       // 248
  tmpl.bar = function () {                                                                                            // 249
    // make sure `this` is bound correctly                                                                            // 250
    return { baz: this.symbol + R.get() };                                                                            // 251
  };                                                                                                                  // 252
                                                                                                                      // 253
  var div = renderToDiv(tmpl.extend({data: {symbol:'%'}}));                                                           // 254
  test.equal(initCount, 1);                                                                                           // 255
  test.equal(canonicalizeHtml(div.innerHTML), '[%david]');                                                            // 256
});                                                                                                                   // 257
                                                                                                                      // 258
Tinytest.add("spacebars - templates - block helper", function (test) {                                                // 259
  // test the case where `foo` is a calculated template that changes                                                  // 260
  // reactively.                                                                                                      // 261
  // `{{#foo}}bar{{else}}baz{{/foo}}`                                                                                 // 262
  var tmpl = Template.spacebars_template_test_block_helper;                                                           // 263
  var R = ReactiveVar(Template.spacebars_template_test_content);                                                      // 264
  tmpl.foo = function () {                                                                                            // 265
    return R.get();                                                                                                   // 266
  };                                                                                                                  // 267
  var div = renderToDiv(tmpl);                                                                                        // 268
  test.equal(canonicalizeHtml(div.innerHTML), "bar");                                                                 // 269
                                                                                                                      // 270
  R.set(Template.spacebars_template_test_elsecontent);                                                                // 271
  Deps.flush();                                                                                                       // 272
  test.equal(canonicalizeHtml(div.innerHTML), "baz");                                                                 // 273
});                                                                                                                   // 274
                                                                                                                      // 275
Tinytest.add("spacebars - templates - block helper function with one string arg", function (test) {                   // 276
  // `{{#foo "bar"}}content{{/foo}}`                                                                                  // 277
  var tmpl = Template.spacebars_template_test_block_helper_function_one_string_arg;                                   // 278
  tmpl.foo = function () {                                                                                            // 279
    if (String(this) === "bar")                                                                                       // 280
      return Template.spacebars_template_test_content;                                                                // 281
    else                                                                                                              // 282
      return null;                                                                                                    // 283
  };                                                                                                                  // 284
  var div = renderToDiv(tmpl);                                                                                        // 285
  test.equal(canonicalizeHtml(div.innerHTML), "content");                                                             // 286
});                                                                                                                   // 287
                                                                                                                      // 288
Tinytest.add("spacebars - templates - block helper function with one helper arg", function (test) {                   // 289
  var tmpl = Template.spacebars_template_test_block_helper_function_one_helper_arg;                                   // 290
  var R = ReactiveVar("bar");                                                                                         // 291
  tmpl.bar = function () { return R.get(); };                                                                         // 292
  tmpl.foo = function () {                                                                                            // 293
    if (String(this) === "bar")                                                                                       // 294
      return Template.spacebars_template_test_content;                                                                // 295
    else                                                                                                              // 296
      return null;                                                                                                    // 297
  };                                                                                                                  // 298
  var div = renderToDiv(tmpl);                                                                                        // 299
  test.equal(canonicalizeHtml(div.innerHTML), "content");                                                             // 300
                                                                                                                      // 301
  R.set("baz");                                                                                                       // 302
  Deps.flush();                                                                                                       // 303
  test.equal(canonicalizeHtml(div.innerHTML), "");                                                                    // 304
});                                                                                                                   // 305
                                                                                                                      // 306
Tinytest.add("spacebars - templates - block helper component with one helper arg", function (test) {                  // 307
  var tmpl = Template.spacebars_template_test_block_helper_component_one_helper_arg;                                  // 308
  var R = ReactiveVar(true);                                                                                          // 309
  tmpl.bar = function () { return R.get(); };                                                                         // 310
  var div = renderToDiv(tmpl);                                                                                        // 311
  test.equal(canonicalizeHtml(div.innerHTML), "content");                                                             // 312
                                                                                                                      // 313
  R.set(false);                                                                                                       // 314
  Deps.flush();                                                                                                       // 315
  test.equal(canonicalizeHtml(div.innerHTML), "");                                                                    // 316
});                                                                                                                   // 317
                                                                                                                      // 318
Tinytest.add("spacebars - templates - block helper component with three helper args", function (test) {               // 319
  var tmpl = Template.spacebars_template_test_block_helper_component_three_helper_args;                               // 320
  var R = ReactiveVar("bar");                                                                                         // 321
  tmpl.bar_or_baz = function () {                                                                                     // 322
    return R.get();                                                                                                   // 323
  };                                                                                                                  // 324
  tmpl.equals = function (x, y) {                                                                                     // 325
    return x === y;                                                                                                   // 326
  };                                                                                                                  // 327
  var div = renderToDiv(tmpl);                                                                                        // 328
  test.equal(canonicalizeHtml(div.innerHTML), "content");                                                             // 329
                                                                                                                      // 330
  R.set("baz");                                                                                                       // 331
  Deps.flush();                                                                                                       // 332
  test.equal(canonicalizeHtml(div.innerHTML), "");                                                                    // 333
});                                                                                                                   // 334
                                                                                                                      // 335
Tinytest.add("spacebars - templates - block helper with dotted arg", function (test) {                                // 336
  var tmpl = Template.spacebars_template_test_block_helper_dotted_arg;                                                // 337
  var R1 = ReactiveVar(1);                                                                                            // 338
  var R2 = ReactiveVar(10);                                                                                           // 339
  var R3 = ReactiveVar(100);                                                                                          // 340
                                                                                                                      // 341
  var initCount = 0;                                                                                                  // 342
  tmpl.foo = Template.spacebars_template_test_bracketed_this.extend({                                                 // 343
    init: function () { initCount++; }                                                                                // 344
  });                                                                                                                 // 345
  tmpl.bar = function () {                                                                                            // 346
    return {                                                                                                          // 347
      r1: R1.get(),                                                                                                   // 348
      baz: function (r3) {                                                                                            // 349
        return this.r1 + R2.get() + r3;                                                                               // 350
      }                                                                                                               // 351
    };                                                                                                                // 352
  };                                                                                                                  // 353
  tmpl.qux = function () { return R3.get(); };                                                                        // 354
                                                                                                                      // 355
  var div = renderToDiv(tmpl);                                                                                        // 356
  test.equal(canonicalizeHtml(div.innerHTML), "[111]");                                                               // 357
  test.equal(initCount, 1);                                                                                           // 358
                                                                                                                      // 359
  R1.set(2);                                                                                                          // 360
  Deps.flush();                                                                                                       // 361
  test.equal(canonicalizeHtml(div.innerHTML), "[112]");                                                               // 362
  test.equal(initCount, 1);                                                                                           // 363
                                                                                                                      // 364
  R2.set(20);                                                                                                         // 365
  Deps.flush();                                                                                                       // 366
  test.equal(canonicalizeHtml(div.innerHTML), "[122]");                                                               // 367
  test.equal(initCount, 1);                                                                                           // 368
                                                                                                                      // 369
  R3.set(200);                                                                                                        // 370
  Deps.flush();                                                                                                       // 371
  test.equal(canonicalizeHtml(div.innerHTML), "[222]");                                                               // 372
  test.equal(initCount, 1);                                                                                           // 373
                                                                                                                      // 374
  R2.set(30);                                                                                                         // 375
  Deps.flush();                                                                                                       // 376
  test.equal(canonicalizeHtml(div.innerHTML), "[232]");                                                               // 377
  test.equal(initCount, 1);                                                                                           // 378
                                                                                                                      // 379
  R1.set(3);                                                                                                          // 380
  Deps.flush();                                                                                                       // 381
  test.equal(canonicalizeHtml(div.innerHTML), "[233]");                                                               // 382
  test.equal(initCount, 1);                                                                                           // 383
                                                                                                                      // 384
  R3.set(300);                                                                                                        // 385
  Deps.flush();                                                                                                       // 386
  test.equal(canonicalizeHtml(div.innerHTML), "[333]");                                                               // 387
  test.equal(initCount, 1);                                                                                           // 388
});                                                                                                                   // 389
                                                                                                                      // 390
Tinytest.add("spacebars - templates - nested content", function (test) {                                              // 391
  // Test that `{{> UI.contentBlock}}` in an `{{#if}}` works.                                                         // 392
                                                                                                                      // 393
  // ```                                                                                                              // 394
  // <template name="spacebars_template_test_iftemplate">                                                             // 395
  //   {{#if condition}}                                                                                              // 396
  //     {{> UI.contentBlock}}                                                                                        // 397
  //   {{else}}                                                                                                       // 398
  //     {{> UI.elseBlock}}                                                                                           // 399
  //   {{/if}}                                                                                                        // 400
  // </template>                                                                                                      // 401
  // ```                                                                                                              // 402
                                                                                                                      // 403
  // ```                                                                                                              // 404
  //  {{#spacebars_template_test_iftemplate flag}}                                                                    // 405
  //    hello                                                                                                         // 406
  //  {{else}}                                                                                                        // 407
  //    world                                                                                                         // 408
  //  {{/spacebars_template_test_iftemplate}}                                                                         // 409
  // ```                                                                                                              // 410
                                                                                                                      // 411
  var tmpl = Template.spacebars_template_test_nested_content;                                                         // 412
  var R = ReactiveVar(true);                                                                                          // 413
  tmpl.flag = function () {                                                                                           // 414
    return R.get();                                                                                                   // 415
  };                                                                                                                  // 416
  var div = renderToDiv(tmpl);                                                                                        // 417
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 418
  R.set(false);                                                                                                       // 419
  Deps.flush();                                                                                                       // 420
  test.equal(canonicalizeHtml(div.innerHTML), 'world');                                                               // 421
  R.set(true);                                                                                                        // 422
  Deps.flush();                                                                                                       // 423
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 424
                                                                                                                      // 425
  // Also test that `{{> UI.contentBlock}}` in a custom block helper works.                                           // 426
  tmpl = Template.spacebars_template_test_nested_content2;                                                            // 427
  R = ReactiveVar(true);                                                                                              // 428
  tmpl.x = function () {                                                                                              // 429
    return R.get();                                                                                                   // 430
  };                                                                                                                  // 431
  div = renderToDiv(tmpl);                                                                                            // 432
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 433
  R.set(false);                                                                                                       // 434
  Deps.flush();                                                                                                       // 435
  test.equal(canonicalizeHtml(div.innerHTML), 'world');                                                               // 436
  R.set(true);                                                                                                        // 437
  Deps.flush();                                                                                                       // 438
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 439
});                                                                                                                   // 440
                                                                                                                      // 441
Tinytest.add("spacebars - template - if", function (test) {                                                           // 442
  var tmpl = Template.spacebars_template_test_if;                                                                     // 443
  var R = ReactiveVar(true);                                                                                          // 444
  tmpl.foo = function () {                                                                                            // 445
    return R.get();                                                                                                   // 446
  };                                                                                                                  // 447
  tmpl.bar = 1;                                                                                                       // 448
  tmpl.baz = 2;                                                                                                       // 449
                                                                                                                      // 450
  var div = renderToDiv(tmpl);                                                                                        // 451
  var rendersTo = function (html) { divRendersTo(test, div, html); };                                                 // 452
                                                                                                                      // 453
  rendersTo("1");                                                                                                     // 454
  R.set(false);                                                                                                       // 455
  rendersTo("2");                                                                                                     // 456
});                                                                                                                   // 457
                                                                                                                      // 458
Tinytest.add("spacebars - template - if in with", function (test) {                                                   // 459
  var tmpl = Template.spacebars_template_test_if_in_with;                                                             // 460
  tmpl.foo = {bar: "bar"};                                                                                            // 461
                                                                                                                      // 462
  var div = renderToDiv(tmpl);                                                                                        // 463
  divRendersTo(test, div, "bar bar");                                                                                 // 464
});                                                                                                                   // 465
                                                                                                                      // 466
Tinytest.add("spacebars - templates - each on cursor", function (test) {                                              // 467
  var tmpl = Template.spacebars_template_test_each;                                                                   // 468
  var coll = new Meteor.Collection(null);                                                                             // 469
  tmpl.items = function () {                                                                                          // 470
    return coll.find({}, {sort: {pos: 1}});                                                                           // 471
  };                                                                                                                  // 472
                                                                                                                      // 473
  var div = renderToDiv(tmpl);                                                                                        // 474
  var rendersTo = function (html) { divRendersTo(test, div, html); };                                                 // 475
                                                                                                                      // 476
  rendersTo("else-clause");                                                                                           // 477
  coll.insert({text: "one", pos: 1});                                                                                 // 478
  rendersTo("one");                                                                                                   // 479
  coll.insert({text: "two", pos: 2});                                                                                 // 480
  rendersTo("one two");                                                                                               // 481
  coll.update({text: "two"}, {$set: {text: "three"}});                                                                // 482
  rendersTo("one three");                                                                                             // 483
  coll.update({text: "three"}, {$set: {pos: 0}});                                                                     // 484
  rendersTo("three one");                                                                                             // 485
  coll.remove({});                                                                                                    // 486
  rendersTo("else-clause");                                                                                           // 487
});                                                                                                                   // 488
                                                                                                                      // 489
Tinytest.add("spacebars - templates - each on array", function (test) {                                               // 490
  var tmpl = Template.spacebars_template_test_each;                                                                   // 491
  var R = new ReactiveVar([]);                                                                                        // 492
  tmpl.items = function () {                                                                                          // 493
    return R.get();                                                                                                   // 494
  };                                                                                                                  // 495
  tmpl.text = function () {                                                                                           // 496
    return this;                                                                                                      // 497
  };                                                                                                                  // 498
                                                                                                                      // 499
  var div = renderToDiv(tmpl);                                                                                        // 500
  var rendersTo = function (html) { divRendersTo(test, div, html); };                                                 // 501
                                                                                                                      // 502
  rendersTo("else-clause");                                                                                           // 503
  R.set([""]);                                                                                                        // 504
  rendersTo("");                                                                                                      // 505
  R.set(["x", "", "toString"]);                                                                                       // 506
  rendersTo("x toString");                                                                                            // 507
  R.set(["toString"]);                                                                                                // 508
  rendersTo("toString");                                                                                              // 509
  R.set([]);                                                                                                          // 510
  rendersTo("else-clause");                                                                                           // 511
  R.set([0, 1, 2]);                                                                                                   // 512
  rendersTo("0 1 2");                                                                                                 // 513
  R.set([]);                                                                                                          // 514
  rendersTo("else-clause");                                                                                           // 515
});                                                                                                                   // 516
                                                                                                                      // 517
Tinytest.add("spacebars - templates - ..", function (test) {                                                          // 518
  var tmpl = Template.spacebars_template_test_dots;                                                                   // 519
  Template.spacebars_template_test_dots_subtemplate.getTitle = function (from) {                                      // 520
    return from.title;                                                                                                // 521
  };                                                                                                                  // 522
                                                                                                                      // 523
  tmpl.foo = {title: "foo"};                                                                                          // 524
  tmpl.foo.bar = {title: "bar"};                                                                                      // 525
  tmpl.foo.bar.items = [{title: "item"}];                                                                             // 526
  var div = renderToDiv(tmpl);                                                                                        // 527
                                                                                                                      // 528
  test.equal(canonicalizeHtml(div.innerHTML), [                                                                       // 529
    "A", "B", "C", "D",                                                                                               // 530
    // {{> spacebars_template_test_dots_subtemplate}}                                                                 // 531
    "TITLE", "1item", "2item", "3bar", "4foo", "GETTITLE", "5item", "6bar", "7foo",                                   // 532
    // {{> spacebars_template_test_dots_subtemplate ..}}                                                              // 533
    "TITLE", "1bar", "2bar", "3item", "4bar", "GETTITLE", "5bar", "6item", "7bar"].join(" "));                        // 534
});                                                                                                                   // 535
                                                                                                                      // 536
Tinytest.add("spacebars - templates - select tags", function (test) {                                                 // 537
  var tmpl = Template.spacebars_template_test_select_tag;                                                             // 538
                                                                                                                      // 539
  // {label: (string)}                                                                                                // 540
  var optgroups = new Meteor.Collection(null);                                                                        // 541
                                                                                                                      // 542
  // {optgroup: (id), value: (string), selected: (boolean), label: (string)}                                          // 543
  var options = new Meteor.Collection(null);                                                                          // 544
                                                                                                                      // 545
  tmpl.optgroups = function () { return optgroups.find(); };                                                          // 546
  tmpl.options = function () { return options.find({optgroup: this._id}); };                                          // 547
  tmpl.selectedAttr = function () { return this.selected ? {selected: true} : {}; };                                  // 548
                                                                                                                      // 549
  var div = renderToDiv(tmpl);                                                                                        // 550
  var selectEl = $(div).find('select')[0];                                                                            // 551
                                                                                                                      // 552
  // returns canonicalized contents of `div` in the form eg                                                           // 553
  // ["<select>", "</select>"]. strip out selected attributes -- we                                                   // 554
  // verify correctness by observing the `selected` property                                                          // 555
  var divContent = function () {                                                                                      // 556
    return canonicalizeHtml(                                                                                          // 557
      div.innerHTML.replace(/selected="[^"]*"/g, '').replace(/selected/g, ''))                                        // 558
          .replace(/\>\s*\</g, '>\n<')                                                                                // 559
          .split('\n');                                                                                               // 560
  };                                                                                                                  // 561
                                                                                                                      // 562
  test.equal(divContent(), ["<select>", "</select>"]);                                                                // 563
                                                                                                                      // 564
  var optgroup1 = optgroups.insert({label: "one"});                                                                   // 565
  var optgroup2 = optgroups.insert({label: "two"});                                                                   // 566
  test.equal(divContent(), [                                                                                          // 567
    '<select>',                                                                                                       // 568
    '<optgroup label="one">',                                                                                         // 569
    '</optgroup>',                                                                                                    // 570
    '<optgroup label="two">',                                                                                         // 571
    '</optgroup>',                                                                                                    // 572
    '</select>'                                                                                                       // 573
  ]);                                                                                                                 // 574
                                                                                                                      // 575
  options.insert({optgroup: optgroup1, value: "value1", selected: false, label: "label1"});                           // 576
  options.insert({optgroup: optgroup1, value: "value2", selected: true, label: "label2"});                            // 577
  test.equal(divContent(), [                                                                                          // 578
    '<select>',                                                                                                       // 579
    '<optgroup label="one">',                                                                                         // 580
    '<option value="value1">label1</option>',                                                                         // 581
    '<option value="value2">label2</option>',                                                                         // 582
    '</optgroup>',                                                                                                    // 583
    '<optgroup label="two">',                                                                                         // 584
    '</optgroup>',                                                                                                    // 585
    '</select>'                                                                                                       // 586
  ]);                                                                                                                 // 587
  test.equal(selectEl.value, "value2");                                                                               // 588
  test.equal($(selectEl).find('option')[0].selected, false);                                                          // 589
  test.equal($(selectEl).find('option')[1].selected, true);                                                           // 590
                                                                                                                      // 591
  // swap selection                                                                                                   // 592
  options.update({value: "value1"}, {$set: {selected: true}});                                                        // 593
  options.update({value: "value2"}, {$set: {selected: false}});                                                       // 594
  Deps.flush();                                                                                                       // 595
                                                                                                                      // 596
  test.equal(divContent(), [                                                                                          // 597
    '<select>',                                                                                                       // 598
    '<optgroup label="one">',                                                                                         // 599
    '<option value="value1">label1</option>',                                                                         // 600
    '<option value="value2">label2</option>',                                                                         // 601
    '</optgroup>',                                                                                                    // 602
    '<optgroup label="two">',                                                                                         // 603
    '</optgroup>',                                                                                                    // 604
    '</select>'                                                                                                       // 605
  ]);                                                                                                                 // 606
  test.equal(selectEl.value, "value1");                                                                               // 607
  test.equal($(selectEl).find('option')[0].selected, true);                                                           // 608
  test.equal($(selectEl).find('option')[1].selected, false);                                                          // 609
                                                                                                                      // 610
  // change value and label                                                                                           // 611
  options.update({value: "value1"}, {$set: {value: "value1.0"}});                                                     // 612
  options.update({value: "value2"}, {$set: {label: "label2.0"}});                                                     // 613
  Deps.flush();                                                                                                       // 614
                                                                                                                      // 615
  test.equal(divContent(), [                                                                                          // 616
    '<select>',                                                                                                       // 617
    '<optgroup label="one">',                                                                                         // 618
    '<option value="value1.0">label1</option>',                                                                       // 619
    '<option value="value2">label2.0</option>',                                                                       // 620
    '</optgroup>',                                                                                                    // 621
    '<optgroup label="two">',                                                                                         // 622
    '</optgroup>',                                                                                                    // 623
    '</select>'                                                                                                       // 624
  ]);                                                                                                                 // 625
  test.equal(selectEl.value, "value1.0");                                                                             // 626
  test.equal($(selectEl).find('option')[0].selected, true);                                                           // 627
  test.equal($(selectEl).find('option')[1].selected, false);                                                          // 628
                                                                                                                      // 629
  // unselect and then select both options. normally, the second is                                                   // 630
  // selected (since it got selected later). then switch to <select                                                   // 631
  // multiple="">. both should be selected.                                                                           // 632
  options.update({}, {$set: {selected: false}}, {multi: true});                                                       // 633
  Deps.flush();                                                                                                       // 634
  options.update({}, {$set: {selected: true}}, {multi: true});                                                        // 635
  Deps.flush();                                                                                                       // 636
  test.equal($(selectEl).find('option')[0].selected, false);                                                          // 637
  test.equal($(selectEl).find('option')[1].selected, true);                                                           // 638
                                                                                                                      // 639
  selectEl.multiple = true; // allow multiple selection                                                               // 640
  options.update({}, {$set: {selected: false}}, {multi: true});                                                       // 641
  Deps.flush();                                                                                                       // 642
  options.update({}, {$set: {selected: true}}, {multi: true});                                                        // 643
  window.avital = true;                                                                                               // 644
  Deps.flush();                                                                                                       // 645
  test.equal($(selectEl).find('option')[0].selected, true);                                                           // 646
  test.equal($(selectEl).find('option')[1].selected, true);                                                           // 647
});                                                                                                                   // 648
                                                                                                                      // 649
Tinytest.add('spacebars - templates - {{#with}} falsy; issue #770', function (test) {                                 // 650
  Template.test_template_issue770.value1 = function () { return "abc"; };                                             // 651
  Template.test_template_issue770.value2 = function () { return false; };                                             // 652
  var div = renderToDiv(Template.test_template_issue770);                                                             // 653
  test.equal(canonicalizeHtml(div.innerHTML),                                                                         // 654
             "abc xxx abc");                                                                                          // 655
});                                                                                                                   // 656
                                                                                                                      // 657
Tinytest.add("spacebars - templates - tricky attrs", function (test) {                                                // 658
  var tmpl = Template.spacebars_template_test_tricky_attrs;                                                           // 659
  tmpl.theType = function () { return 'text'; };                                                                      // 660
  var R = ReactiveVar('foo');                                                                                         // 661
  tmpl.theClass = function () { return R.get(); };                                                                    // 662
                                                                                                                      // 663
  var div = renderToDiv(tmpl);                                                                                        // 664
  test.equal(canonicalizeHtml(div.innerHTML).slice(0, 30),                                                            // 665
             '<input type="text"><input class="foo" type="checkbox">'.slice(0, 30));                                  // 666
                                                                                                                      // 667
  R.set('bar');                                                                                                       // 668
  Deps.flush();                                                                                                       // 669
  test.equal(canonicalizeHtml(div.innerHTML),                                                                         // 670
             '<input type="text"><input class="bar" type="checkbox">');                                               // 671
                                                                                                                      // 672
});                                                                                                                   // 673
                                                                                                                      // 674
Tinytest.add('spacebars - templates - no data context', function (test) {                                             // 675
  var tmpl = Template.spacebars_template_test_no_data;                                                                // 676
                                                                                                                      // 677
  // failure is if an exception is thrown here                                                                        // 678
  var div = renderToDiv(tmpl);                                                                                        // 679
  test.equal(canonicalizeHtml(div.innerHTML), 'asdf');                                                                // 680
});                                                                                                                   // 681
                                                                                                                      // 682
// test that #isolate is a no-op, for back compat                                                                     // 683
Tinytest.add('spacebars - templates - isolate', function (test) {                                                     // 684
  var tmpl = Template.spacebars_template_test_isolate;                                                                // 685
                                                                                                                      // 686
  Meteor._suppress_log(1); // we print a deprecation notice                                                           // 687
  var div = renderToDiv(tmpl);                                                                                        // 688
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 689
                                                                                                                      // 690
});                                                                                                                   // 691
                                                                                                                      // 692
// test that #constant is a no-op, for back compat                                                                    // 693
Tinytest.add('spacebars - templates - constant', function (test) {                                                    // 694
  var tmpl = Template.spacebars_template_test_constant;                                                               // 695
                                                                                                                      // 696
  Meteor._suppress_log(1); // we print a deprecation notice                                                           // 697
  var div = renderToDiv(tmpl);                                                                                        // 698
  test.equal(canonicalizeHtml(div.innerHTML), 'hello');                                                               // 699
                                                                                                                      // 700
});                                                                                                                   // 701
                                                                                                                      // 702
Tinytest.add('spacebars - templates - textarea', function (test) {                                                    // 703
  var tmpl = Template.spacebars_template_test_textarea;                                                               // 704
                                                                                                                      // 705
  var R = ReactiveVar('hello');                                                                                       // 706
                                                                                                                      // 707
  tmpl.foo = function () {                                                                                            // 708
    return R.get();                                                                                                   // 709
  };                                                                                                                  // 710
                                                                                                                      // 711
  var div = renderToDiv(tmpl);                                                                                        // 712
  var textarea = div.querySelector('textarea');                                                                       // 713
  test.equal(textarea.value, 'hello');                                                                                // 714
                                                                                                                      // 715
  R.set('world');                                                                                                     // 716
  Deps.flush();                                                                                                       // 717
  test.equal(textarea.value, 'world');                                                                                // 718
                                                                                                                      // 719
});                                                                                                                   // 720
                                                                                                                      // 721
Tinytest.add('spacebars - templates - textarea 2', function (test) {                                                  // 722
  var tmpl = Template.spacebars_template_test_textarea2;                                                              // 723
                                                                                                                      // 724
  var R = ReactiveVar(true);                                                                                          // 725
                                                                                                                      // 726
  tmpl.foo = function () {                                                                                            // 727
    return R.get();                                                                                                   // 728
  };                                                                                                                  // 729
                                                                                                                      // 730
  var div = renderToDiv(tmpl);                                                                                        // 731
  var textarea = div.querySelector('textarea');                                                                       // 732
  test.equal(textarea.value, '</not a tag>');                                                                         // 733
                                                                                                                      // 734
  R.set(false);                                                                                                       // 735
  Deps.flush();                                                                                                       // 736
  test.equal(textarea.value, '<also not a tag>');                                                                     // 737
                                                                                                                      // 738
  R.set(true);                                                                                                        // 739
  Deps.flush();                                                                                                       // 740
  test.equal(textarea.value, '</not a tag>');                                                                         // 741
                                                                                                                      // 742
});                                                                                                                   // 743
                                                                                                                      // 744
Tinytest.add('spacebars - templates - textarea each', function (test) {                                               // 745
  var tmpl = Template.spacebars_template_test_textarea_each;                                                          // 746
                                                                                                                      // 747
  var R = ReactiveVar(['APPLE', 'BANANA']);                                                                           // 748
                                                                                                                      // 749
  tmpl.foo = function () {                                                                                            // 750
    return R.get();                                                                                                   // 751
  };                                                                                                                  // 752
                                                                                                                      // 753
  var div = renderToDiv(tmpl);                                                                                        // 754
  var textarea = div.querySelector('textarea');                                                                       // 755
  test.equal(textarea.value, '<not a tag APPLE <not a tag BANANA ');                                                  // 756
                                                                                                                      // 757
  R.set([]);                                                                                                          // 758
  Deps.flush();                                                                                                       // 759
  test.equal(textarea.value, '<>');                                                                                   // 760
                                                                                                                      // 761
  R.set(['CUCUMBER']);                                                                                                // 762
  Deps.flush();                                                                                                       // 763
  test.equal(textarea.value, '<not a tag CUCUMBER ');                                                                 // 764
                                                                                                                      // 765
});                                                                                                                   // 766
                                                                                                                      // 767
// Ensure that one can call `Meteor.defer` within a rendered callback                                                 // 768
// triggered by a document insertion that happend in a method stub.                                                   // 769
testAsyncMulti('spacebars - template - defer in rendered callbacks', [function (test, expect) {                       // 770
  var tmpl = Template.spacebars_template_test_defer_in_rendered;                                                      // 771
  var coll = new Meteor.Collection("test-defer-in-rendered--client-only");                                            // 772
  tmpl.items = function () {                                                                                          // 773
    return coll.find();                                                                                               // 774
  };                                                                                                                  // 775
                                                                                                                      // 776
  var subtmpl = Template.spacebars_template_test_defer_in_rendered_subtemplate;                                       // 777
  subtmpl.rendered = expect(function () {                                                                             // 778
    // will throw if called in a method stub                                                                          // 779
    Meteor.defer(function () {                                                                                        // 780
    });                                                                                                               // 781
  });                                                                                                                 // 782
                                                                                                                      // 783
  var div = renderToDiv(tmpl);                                                                                        // 784
                                                                                                                      // 785
  // `coll` is not defined on the server so we'll get an error.  We                                                   // 786
  // can't make this a client-only collection since then we won't be                                                  // 787
  // running in a stub and the error won't fire.                                                                      // 788
  Meteor._suppress_log(1);                                                                                            // 789
  // cause a new instance of `subtmpl` to be placed in the DOM. verify                                                // 790
  // that it's not fired directly within a method stub, in which                                                      // 791
  // `Meteor.defer` is not allowed.                                                                                   // 792
  coll.insert({});                                                                                                    // 793
}]);                                                                                                                  // 794
                                                                                                                      // 795
testAsyncMulti('spacebars - template - rendered template is DOM in rendered callbacks', [                             // 796
  function (test, expect) {                                                                                           // 797
    var tmpl = Template.spacebars_template_test_aaa;                                                                  // 798
    tmpl.rendered = expect(function () {                                                                              // 799
      test.equal(canonicalizeHtml(div.innerHTML), "aaa");                                                             // 800
    });                                                                                                               // 801
    var div = renderToDiv(tmpl);                                                                                      // 802
    Deps.flush();                                                                                                     // 803
  }                                                                                                                   // 804
]);                                                                                                                   // 805
                                                                                                                      // 806
// Test that in:                                                                                                      // 807
//                                                                                                                    // 808
// ```                                                                                                                // 809
// {{#with someData}}                                                                                                 // 810
//   {{foo}} {{bar}}                                                                                                  // 811
// {{/with}}                                                                                                          // 812
// ```                                                                                                                // 813
//                                                                                                                    // 814
// ... we run `someData` once even if `foo` re-renders.                                                               // 815
Tinytest.add('spacebars - templates - with someData', function (test) {                                               // 816
  var tmpl = Template.spacebars_template_test_with_someData;                                                          // 817
                                                                                                                      // 818
  var foo = ReactiveVar('AAA');                                                                                       // 819
  var someDataRuns = 0;                                                                                               // 820
                                                                                                                      // 821
  tmpl.someData = function () {                                                                                       // 822
    someDataRuns++;                                                                                                   // 823
    return {};                                                                                                        // 824
  };                                                                                                                  // 825
  tmpl.foo = function () {                                                                                            // 826
    return foo.get();                                                                                                 // 827
  };                                                                                                                  // 828
  tmpl.bar = function () {                                                                                            // 829
    return 'YO';                                                                                                      // 830
  };                                                                                                                  // 831
                                                                                                                      // 832
  var div = renderToDiv(tmpl);                                                                                        // 833
                                                                                                                      // 834
  test.equal(someDataRuns, 1);                                                                                        // 835
  test.equal(canonicalizeHtml(div.innerHTML), 'AAA YO');                                                              // 836
                                                                                                                      // 837
  foo.set('BBB');                                                                                                     // 838
  Deps.flush();                                                                                                       // 839
  test.equal(someDataRuns, 1);                                                                                        // 840
  test.equal(canonicalizeHtml(div.innerHTML), 'BBB YO');                                                              // 841
                                                                                                                      // 842
  foo.set('CCC');                                                                                                     // 843
  Deps.flush();                                                                                                       // 844
  test.equal(someDataRuns, 1);                                                                                        // 845
  test.equal(canonicalizeHtml(div.innerHTML), 'CCC YO');                                                              // 846
});                                                                                                                   // 847
                                                                                                                      // 848
Tinytest.add('spacebars - template - #each stops when rendered element is removed', function (test) {                 // 849
  var tmpl = Template.spacebars_template_test_each_stops;                                                             // 850
  var coll = new Meteor.Collection(null);                                                                             // 851
  coll.insert({});                                                                                                    // 852
  tmpl.items = function () { return coll.find(); };                                                                   // 853
                                                                                                                      // 854
  var div = renderToDiv(tmpl);                                                                                        // 855
  divRendersTo(test, div, 'x');                                                                                       // 856
                                                                                                                      // 857
  // trigger #each component destroyed                                                                                // 858
  $(div).remove();                                                                                                    // 859
                                                                                                                      // 860
  // insert another document. cursor should no longer be observed so                                                  // 861
  // should have no effect.                                                                                           // 862
  coll.insert({});                                                                                                    // 863
  divRendersTo(test, div, 'x');                                                                                       // 864
});                                                                                                                   // 865
                                                                                                                      // 866
Tinytest.add('spacebars - templates - block helpers in attribute', function (test) {                                  // 867
  var tmpl = Template.spacebars_template_test_block_helpers_in_attribute;                                             // 868
                                                                                                                      // 869
  var coll = new Meteor.Collection(null);                                                                             // 870
  tmpl.classes = function () {                                                                                        // 871
    return coll.find({}, {sort: {name: 1}});                                                                          // 872
  };                                                                                                                  // 873
  tmpl.startsLowerCase = function (name) {                                                                            // 874
    return /^[a-z]/.test(name);                                                                                       // 875
  };                                                                                                                  // 876
  coll.insert({name: 'David'});                                                                                       // 877
  coll.insert({name: 'noodle'});                                                                                      // 878
  coll.insert({name: 'donut'});                                                                                       // 879
  coll.insert({name: 'frankfurter'});                                                                                 // 880
  coll.insert({name: 'Steve'});                                                                                       // 881
                                                                                                                      // 882
  var containerDiv = renderToDiv(tmpl);                                                                               // 883
  var div = containerDiv.querySelector('div');                                                                        // 884
                                                                                                                      // 885
  var shouldBe = function (className) {                                                                               // 886
    Deps.flush();                                                                                                     // 887
    test.equal(div.innerHTML, "Hello");                                                                               // 888
    test.equal(div.className, className);                                                                             // 889
    var result = canonicalizeHtml(containerDiv.innerHTML);                                                            // 890
    if (result === '<div>Hello</div>')                                                                                // 891
      result = '<div class="">Hello</div>'; // e.g. IE 9 and 10                                                       // 892
    test.equal(result, '<div class="' + className + '">Hello</div>');                                                 // 893
  };                                                                                                                  // 894
                                                                                                                      // 895
  shouldBe('donut frankfurter noodle');                                                                               // 896
  coll.remove({name: 'frankfurter'}); // (it was kind of a mouthful)                                                  // 897
  shouldBe('donut noodle');                                                                                           // 898
  coll.remove({name: 'donut'});                                                                                       // 899
  shouldBe('noodle');                                                                                                 // 900
  coll.remove({name: 'noodle'});                                                                                      // 901
  shouldBe(''); // 'David' and 'Steve' appear in the #each but fail the #if                                           // 902
  coll.remove({});                                                                                                    // 903
  shouldBe('none'); // now the `{{else}}` case kicks in                                                               // 904
  coll.insert({name: 'bubblegum'});                                                                                   // 905
  shouldBe('bubblegum');                                                                                              // 906
});                                                                                                                   // 907
                                                                                                                      // 908
Tinytest.add('spacebars - templates - block helpers in attribute 2', function (test) {                                // 909
  var tmpl = Template.spacebars_template_test_block_helpers_in_attribute_2;                                           // 910
                                                                                                                      // 911
  var R = ReactiveVar(true);                                                                                          // 912
                                                                                                                      // 913
  tmpl.foo = function () { return R.get(); };                                                                         // 914
                                                                                                                      // 915
  var div = renderToDiv(tmpl);                                                                                        // 916
  var input = div.querySelector('input');                                                                             // 917
                                                                                                                      // 918
  test.equal(input.value, '"');                                                                                       // 919
  R.set(false);                                                                                                       // 920
  Deps.flush();                                                                                                       // 921
  test.equal(input.value, '&<></x>');                                                                                 // 922
});                                                                                                                   // 923
                                                                                                                      // 924
                                                                                                                      // 925
// Test that if the argument to #each is a constant, it doesn't establish a                                           // 926
// dependency on the data context, so when the context changes, items of                                              // 927
// the #each are not "changed" and helpers do not rerun.                                                              // 928
Tinytest.add('spacebars - templates - constant #each argument', function (test) {                                     // 929
  var tmpl = Template.spacebars_template_test_constant_each_argument;                                                 // 930
                                                                                                                      // 931
  var justReturnRuns = 0; // how many times `justReturn` is called                                                    // 932
  var R = ReactiveVar(1);                                                                                             // 933
                                                                                                                      // 934
  tmpl.someData = function () {                                                                                       // 935
    return R.get();                                                                                                   // 936
  };                                                                                                                  // 937
  tmpl.anArray = ['foo', 'bar'];                                                                                      // 938
  tmpl.justReturn = function (x) {                                                                                    // 939
    justReturnRuns++;                                                                                                 // 940
    return String(x);                                                                                                 // 941
  };                                                                                                                  // 942
                                                                                                                      // 943
  var div = renderToDiv(tmpl);                                                                                        // 944
                                                                                                                      // 945
  test.equal(justReturnRuns, 2);                                                                                      // 946
  test.equal(canonicalizeHtml(div.innerHTML).replace(/\s+/g, ' '),                                                    // 947
             'foo bar 1');                                                                                            // 948
                                                                                                                      // 949
  R.set(2);                                                                                                           // 950
  Deps.flush();                                                                                                       // 951
                                                                                                                      // 952
  test.equal(justReturnRuns, 2); // still 2, no new runs!                                                             // 953
  test.equal(canonicalizeHtml(div.innerHTML).replace(/\s+/g, ' '),                                                    // 954
             'foo bar 2');                                                                                            // 955
});                                                                                                                   // 956
                                                                                                                      // 957
// extract a multi-line string from a comment within a function.                                                      // 958
// @param f {Function} eg function () { /* [[[...content...]]] */ }                                                   // 959
// @returns {String} eg "content"                                                                                     // 960
var textFromFunction = function(f) {                                                                                  // 961
  var str = f.toString().match(/\[\[\[([\S\s]*)\]\]\]/m)[1];                                                          // 962
  // remove line number comments added by linker                                                                      // 963
  str = str.replace(/[ ]*\/\/ \d+$/gm, '');                                                                           // 964
  return str;                                                                                                         // 965
};                                                                                                                    // 966
                                                                                                                      // 967
Tinytest.add('spacebars - templates - #markdown - basic', function (test) {                                           // 968
  var tmpl = Template.spacebars_template_test_markdown_basic;                                                         // 969
  tmpl.obj = {snippet: "<i>hi</i>"};                                                                                  // 970
  tmpl.hi = function () {                                                                                             // 971
    return this.snippet;                                                                                              // 972
  };                                                                                                                  // 973
  var div = renderToDiv(tmpl);                                                                                        // 974
  test.equal(canonicalizeHtml(div.innerHTML), canonicalizeHtml(textFromFunction(function () { /*                      // 975
[[[<p><i>hi</i>                                                                                                       // 976
/each}}</p>                                                                                                           // 977
                                                                                                                      // 978
<p><b><i>hi</i></b>                                                                                                   // 979
<b>/each}}</b></p>                                                                                                    // 980
                                                                                                                      // 981
<ul>                                                                                                                  // 982
<li><i>hi</i></li>                                                                                                    // 983
<li><p>/each}}</p></li>                                                                                               // 984
<li><p><b><i>hi</i></b></p></li>                                                                                      // 985
<li><b>/each}}</b></li>                                                                                               // 986
</ul>                                                                                                                 // 987
                                                                                                                      // 988
<p>some paragraph to fix showdown's four space parsing below.</p>                                                     // 989
                                                                                                                      // 990
<pre><code>&lt;i&gt;hi&lt;/i&gt;                                                                                      // 991
/each}}                                                                                                               // 992
                                                                                                                      // 993
&lt;b&gt;&lt;i&gt;hi&lt;/i&gt;&lt;/b&gt;                                                                              // 994
&lt;b&gt;/each}}&lt;/b&gt;                                                                                            // 995
</code></pre>                                                                                                         // 996
                                                                                                                      // 997
<p>&amp;gt</p>                                                                                                        // 998
                                                                                                                      // 999
<ul>                                                                                                                  // 1000
<li>&amp;gt</li>                                                                                                      // 1001
</ul>                                                                                                                 // 1002
                                                                                                                      // 1003
<p><code>&amp;gt</code></p>                                                                                           // 1004
                                                                                                                      // 1005
<pre><code>&amp;gt                                                                                                    // 1006
</code></pre>                                                                                                         // 1007
                                                                                                                      // 1008
<p>&gt;</p>                                                                                                           // 1009
                                                                                                                      // 1010
<ul>                                                                                                                  // 1011
<li>&gt;</li>                                                                                                         // 1012
</ul>                                                                                                                 // 1013
                                                                                                                      // 1014
<p><code>&amp;gt;</code></p>                                                                                          // 1015
                                                                                                                      // 1016
<pre><code>&amp;gt;                                                                                                   // 1017
</code></pre>                                                                                                         // 1018
                                                                                                                      // 1019
<p><code>&lt;i&gt;hi&lt;/i&gt;</code>                                                                                 // 1020
<code>/each}}</code></p>                                                                                              // 1021
                                                                                                                      // 1022
<p><code>&lt;b&gt;&lt;i&gt;hi&lt;/i&gt;&lt;/b&gt;</code>                                                              // 1023
<code>&lt;b&gt;/each}}</code></p>]]] */                                                                               // 1024
  })));                                                                                                               // 1025
});                                                                                                                   // 1026
                                                                                                                      // 1027
Tinytest.add('spacebars - templates - #markdown - if', function (test) {                                              // 1028
  var tmpl = Template.spacebars_template_test_markdown_if;                                                            // 1029
  var R = new ReactiveVar(false);                                                                                     // 1030
  tmpl.cond = function () { return R.get(); };                                                                        // 1031
                                                                                                                      // 1032
  var div = renderToDiv(tmpl);                                                                                        // 1033
  test.equal(canonicalizeHtml(div.innerHTML), canonicalizeHtml(textFromFunction(function () { /*                      // 1034
[[[<p>false</p>                                                                                                       // 1035
                                                                                                                      // 1036
<p><b>false</b></p>                                                                                                   // 1037
                                                                                                                      // 1038
<ul>                                                                                                                  // 1039
<li><p>false</p></li>                                                                                                 // 1040
<li><p><b>false</b></p></li>                                                                                          // 1041
</ul>                                                                                                                 // 1042
                                                                                                                      // 1043
<p>some paragraph to fix showdown's four space parsing below.</p>                                                     // 1044
                                                                                                                      // 1045
<pre><code>false                                                                                                      // 1046
                                                                                                                      // 1047
&lt;b&gt;false&lt;/b&gt;                                                                                              // 1048
</code></pre>                                                                                                         // 1049
                                                                                                                      // 1050
<p><code>false</code></p>                                                                                             // 1051
                                                                                                                      // 1052
<p><code>&lt;b&gt;false&lt;/b&gt;</code></p>]]] */                                                                    // 1053
  })));                                                                                                               // 1054
  R.set(true);                                                                                                        // 1055
  Deps.flush();                                                                                                       // 1056
  test.equal(canonicalizeHtml(div.innerHTML), canonicalizeHtml(textFromFunction(function () { /*                      // 1057
[[[<p>true</p>                                                                                                        // 1058
                                                                                                                      // 1059
<p><b>true</b></p>                                                                                                    // 1060
                                                                                                                      // 1061
<ul>                                                                                                                  // 1062
<li><p>true</p></li>                                                                                                  // 1063
<li><p><b>true</b></p></li>                                                                                           // 1064
</ul>                                                                                                                 // 1065
                                                                                                                      // 1066
<p>some paragraph to fix showdown's four space parsing below.</p>                                                     // 1067
                                                                                                                      // 1068
<pre><code>true                                                                                                       // 1069
                                                                                                                      // 1070
&lt;b&gt;true&lt;/b&gt;                                                                                               // 1071
</code></pre>                                                                                                         // 1072
                                                                                                                      // 1073
<p><code>true</code></p>                                                                                              // 1074
                                                                                                                      // 1075
<p><code>&lt;b&gt;true&lt;/b&gt;</code></p>]]] */                                                                     // 1076
  })));                                                                                                               // 1077
});                                                                                                                   // 1078
                                                                                                                      // 1079
Tinytest.add('spacebars - templates - #markdown - each', function (test) {                                            // 1080
  var tmpl = Template.spacebars_template_test_markdown_each;                                                          // 1081
  var R = new ReactiveVar([]);                                                                                        // 1082
  tmpl.seq = function () { return R.get(); };                                                                         // 1083
                                                                                                                      // 1084
  var div = renderToDiv(tmpl);                                                                                        // 1085
  test.equal(canonicalizeHtml(div.innerHTML), canonicalizeHtml(textFromFunction(function () { /*                      // 1086
[[[<p><b></b></p>                                                                                                     // 1087
                                                                                                                      // 1088
<ul>                                                                                                                  // 1089
<li></li>                                                                                                             // 1090
<li><b></b></li>                                                                                                      // 1091
</ul>                                                                                                                 // 1092
                                                                                                                      // 1093
<p>some paragraph to fix showdown's four space parsing below.</p>                                                     // 1094
                                                                                                                      // 1095
<pre><code>&lt;b&gt;&lt;/b&gt;                                                                                        // 1096
</code></pre>                                                                                                         // 1097
                                                                                                                      // 1098
<p>``</p>                                                                                                             // 1099
                                                                                                                      // 1100
<p><code>&lt;b&gt;&lt;/b&gt;</code></p>]]] */                                                                         // 1101
    })));                                                                                                             // 1102
                                                                                                                      // 1103
  R.set(["item"]);                                                                                                    // 1104
  Deps.flush();                                                                                                       // 1105
  test.equal(canonicalizeHtml(div.innerHTML), canonicalizeHtml(textFromFunction(function () { /*                      // 1106
[[[<p>item</p>                                                                                                        // 1107
                                                                                                                      // 1108
<p><b>item</b></p>                                                                                                    // 1109
                                                                                                                      // 1110
<ul>                                                                                                                  // 1111
<li><p>item</p></li>                                                                                                  // 1112
<li><p><b>item</b></p></li>                                                                                           // 1113
</ul>                                                                                                                 // 1114
                                                                                                                      // 1115
<p>some paragraph to fix showdown's four space parsing below.</p>                                                     // 1116
                                                                                                                      // 1117
<pre><code>item                                                                                                       // 1118
                                                                                                                      // 1119
&lt;b&gt;item&lt;/b&gt;                                                                                               // 1120
</code></pre>                                                                                                         // 1121
                                                                                                                      // 1122
<p><code>item</code></p>                                                                                              // 1123
                                                                                                                      // 1124
<p><code>&lt;b&gt;item&lt;/b&gt;</code></p>]]] */                                                                     // 1125
    })));                                                                                                             // 1126
});                                                                                                                   // 1127
                                                                                                                      // 1128
Tinytest.add('spacebars - templates - #markdown - inclusion', function (test) {                                       // 1129
  var tmpl = Template.spacebars_template_test_markdown_inclusion;                                                     // 1130
  var subtmpl = Template.spacebars_template_test_markdown_inclusion_subtmpl;                                          // 1131
  subtmpl.foo = "bar";                                                                                                // 1132
  var div = renderToDiv(tmpl);                                                                                        // 1133
  test.equal(canonicalizeHtml(div.innerHTML), "<p><span>Foo is bar.</span></p>");                                     // 1134
});                                                                                                                   // 1135
                                                                                                                      // 1136
Tinytest.add('spacebars - templates - #markdown - block helpers', function (test) {                                   // 1137
  var tmpl = Template.spacebars_template_test_markdown_block_helpers;                                                 // 1138
  var div = renderToDiv(tmpl);                                                                                        // 1139
  test.equal(canonicalizeHtml(div.innerHTML), "<p>Hi there!</p>");                                                    // 1140
});                                                                                                                   // 1141
                                                                                                                      // 1142
// Test that when a simple helper re-runs due to a dependency changing                                                // 1143
// but the return value is the same, the DOM text node is not                                                         // 1144
// re-rendered.                                                                                                       // 1145
Tinytest.add('spacebars - templates - simple helpers are isolated', function (test) {                                 // 1146
  var runs = [{                                                                                                       // 1147
    helper: function () { return "foo"; },                                                                            // 1148
    nodeValue: "foo"                                                                                                  // 1149
  }, {                                                                                                                // 1150
    helper: function () { return new Spacebars.SafeString("bar"); },                                                  // 1151
    nodeValue: "bar"                                                                                                  // 1152
  }];                                                                                                                 // 1153
                                                                                                                      // 1154
  _.each(runs, function (run) {                                                                                       // 1155
    var tmpl = Template.spacebars_template_test_simple_helpers_are_isolated;                                          // 1156
    var dep = new Deps.Dependency;                                                                                    // 1157
    tmpl.foo = function () {                                                                                          // 1158
      dep.depend();                                                                                                   // 1159
      return run.helper();                                                                                            // 1160
    };                                                                                                                // 1161
    var div = renderToDiv(tmpl);                                                                                      // 1162
    var fooTextNode = _.find(div.childNodes, function (node) {                                                        // 1163
      return node.nodeValue === run.nodeValue;                                                                        // 1164
    });                                                                                                               // 1165
                                                                                                                      // 1166
    test.isTrue(fooTextNode);                                                                                         // 1167
                                                                                                                      // 1168
    dep.changed();                                                                                                    // 1169
    Deps.flush();                                                                                                     // 1170
    var newFooTextNode = _.find(div.childNodes, function (node) {                                                     // 1171
      return node.nodeValue === run.nodeValue;                                                                        // 1172
    });                                                                                                               // 1173
                                                                                                                      // 1174
    test.equal(fooTextNode, newFooTextNode);                                                                          // 1175
  });                                                                                                                 // 1176
});                                                                                                                   // 1177
                                                                                                                      // 1178
// Test that when a helper in an element attribute re-runs due to a                                                   // 1179
// dependency changing but the return value is the same, the attribute                                                // 1180
// value is not set.                                                                                                  // 1181
Tinytest.add('spacebars - templates - attribute helpers are isolated', function (test) {                              // 1182
  var tmpl = Template.spacebars_template_test_attr_helpers_are_isolated;                                              // 1183
  var dep = new Deps.Dependency;                                                                                      // 1184
  tmpl.foo = function () {                                                                                            // 1185
    dep.depend();                                                                                                     // 1186
    return "foo";                                                                                                     // 1187
  };                                                                                                                  // 1188
  var div = renderToDiv(tmpl);                                                                                        // 1189
  var pElement = div.querySelector('p');                                                                              // 1190
                                                                                                                      // 1191
  test.equal(pElement.getAttribute('attr'), 'foo');                                                                   // 1192
                                                                                                                      // 1193
  // set the attribute to something else, afterwards check that it                                                    // 1194
  // hasn't been updated back to the correct value.                                                                   // 1195
  pElement.setAttribute('attr', 'not-foo');                                                                           // 1196
  dep.changed();                                                                                                      // 1197
  Deps.flush();                                                                                                       // 1198
  test.equal(pElement.getAttribute('attr'), 'not-foo');                                                               // 1199
});                                                                                                                   // 1200
                                                                                                                      // 1201
// A helper can return an object with a set of element attributes via                                                 // 1202
// `<p {{attrs}}>`. When it re-runs due to a dependency changing the                                                  // 1203
// value for a given attribute might stay the same. Test that the                                                     // 1204
// attribute is not set on the DOM element.                                                                           // 1205
Tinytest.add('spacebars - templates - attribute object helpers are isolated', function (test) {                       // 1206
  var tmpl = Template.spacebars_template_test_attr_object_helpers_are_isolated;                                       // 1207
  var dep = new Deps.Dependency;                                                                                      // 1208
  tmpl.attrs = function () {                                                                                          // 1209
    dep.depend();                                                                                                     // 1210
    return {foo: "bar"};                                                                                              // 1211
  };                                                                                                                  // 1212
  var div = renderToDiv(tmpl);                                                                                        // 1213
  var pElement = div.querySelector('p');                                                                              // 1214
                                                                                                                      // 1215
  test.equal(pElement.getAttribute('foo'), 'bar');                                                                    // 1216
                                                                                                                      // 1217
  // set the attribute to something else, afterwards check that it                                                    // 1218
  // hasn't been updated back to the correct value.                                                                   // 1219
  pElement.setAttribute('foo', 'not-bar');                                                                            // 1220
  dep.changed();                                                                                                      // 1221
  Deps.flush();                                                                                                       // 1222
  test.equal(pElement.getAttribute('foo'), 'not-bar');                                                                // 1223
});                                                                                                                   // 1224
                                                                                                                      // 1225
// Test that when a helper in an inclusion directive (`{{> foo }}`)                                                   // 1226
// re-runs due to a dependency changing but the return value is the                                                   // 1227
// same, the template is not re-rendered.                                                                             // 1228
//                                                                                                                    // 1229
// Also, verify that an error is thrown if the return value from such                                                 // 1230
// a helper is not a component.                                                                                       // 1231
Tinytest.add('spacebars - templates - inclusion helpers are isolated', function (test) {                              // 1232
  var tmpl = Template.spacebars_template_test_inclusion_helpers_are_isolated;                                         // 1233
  var dep = new Deps.Dependency;                                                                                      // 1234
  var subtmpl = Template.                                                                                             // 1235
        spacebars_template_test_inclusion_helpers_are_isolated_subtemplate                                            // 1236
        .extend({}); // fresh instance                                                                                // 1237
  var R = new ReactiveVar(subtmpl);                                                                                   // 1238
  tmpl.foo = function () {                                                                                            // 1239
    dep.depend();                                                                                                     // 1240
    return R.get();                                                                                                   // 1241
  };                                                                                                                  // 1242
                                                                                                                      // 1243
  var div = renderToDiv(tmpl);                                                                                        // 1244
  subtmpl.rendered = function () {                                                                                    // 1245
    test.fail("shouldn't re-render when same value returned from helper");                                            // 1246
  };                                                                                                                  // 1247
                                                                                                                      // 1248
  dep.changed();                                                                                                      // 1249
  Deps.flush({_throwFirstError: true}); // `subtmpl.rendered` not called                                              // 1250
                                                                                                                      // 1251
  R.set(null);                                                                                                        // 1252
  Deps.flush({_throwFirstError: true}); // no error thrown                                                            // 1253
                                                                                                                      // 1254
  R.set("neither a component nor null");                                                                              // 1255
                                                                                                                      // 1256
  test.throws(function () {                                                                                           // 1257
    Deps.flush({_throwFirstError: true});                                                                             // 1258
  }, /Expected null or template/);                                                                                    // 1259
});                                                                                                                   // 1260
                                                                                                                      // 1261
Tinytest.add('spacebars - templates - nully attributes', function (test) {                                            // 1262
  var tmpls = {                                                                                                       // 1263
    0: Template.spacebars_template_test_nully_attributes0,                                                            // 1264
    1: Template.spacebars_template_test_nully_attributes1,                                                            // 1265
    2: Template.spacebars_template_test_nully_attributes2,                                                            // 1266
    3: Template.spacebars_template_test_nully_attributes3,                                                            // 1267
    4: Template.spacebars_template_test_nully_attributes4,                                                            // 1268
    5: Template.spacebars_template_test_nully_attributes5,                                                            // 1269
    6: Template.spacebars_template_test_nully_attributes6                                                             // 1270
  };                                                                                                                  // 1271
                                                                                                                      // 1272
  var run = function (whichTemplate, data, expectTrue) {                                                              // 1273
    var templateWithData = tmpls[whichTemplate].extend({data: function () {                                           // 1274
      return data; }});                                                                                               // 1275
    var div = renderToDiv(templateWithData);                                                                          // 1276
    var input = div.querySelector('input');                                                                           // 1277
    var descr = JSON.stringify([whichTemplate, data, expectTrue]);                                                    // 1278
    if (expectTrue) {                                                                                                 // 1279
      test.isTrue(input.checked, descr);                                                                              // 1280
      test.equal(typeof input.getAttribute('stuff'), 'string', descr);                                                // 1281
    } else {                                                                                                          // 1282
      test.isFalse(input.checked);                                                                                    // 1283
      test.equal(JSON.stringify(input.getAttribute('stuff')), 'null', descr);                                         // 1284
    }                                                                                                                 // 1285
                                                                                                                      // 1286
    var html = HTML.toHTML(templateWithData);                                                                         // 1287
    test.equal(/ checked="[^"]*"/.test(html), !! expectTrue);                                                         // 1288
    test.equal(/ stuff="[^"]*"/.test(html), !! expectTrue);                                                           // 1289
  };                                                                                                                  // 1290
                                                                                                                      // 1291
  run(0, {}, true);                                                                                                   // 1292
                                                                                                                      // 1293
  var truthies = [true, ''];                                                                                          // 1294
  var falsies = [false, null, undefined];                                                                             // 1295
                                                                                                                      // 1296
  _.each(truthies, function (x) {                                                                                     // 1297
    run(1, {foo: x}, true);                                                                                           // 1298
  });                                                                                                                 // 1299
  _.each(falsies, function (x) {                                                                                      // 1300
    run(1, {foo: x}, false);                                                                                          // 1301
  });                                                                                                                 // 1302
                                                                                                                      // 1303
  _.each(truthies, function (x) {                                                                                     // 1304
    _.each(truthies, function (y) {                                                                                   // 1305
      run(2, {foo: x, bar: y}, true);                                                                                 // 1306
    });                                                                                                               // 1307
    _.each(falsies, function (y) {                                                                                    // 1308
      run(2, {foo: x, bar: y}, true);                                                                                 // 1309
    });                                                                                                               // 1310
  });                                                                                                                 // 1311
  _.each(falsies, function (x) {                                                                                      // 1312
    _.each(truthies, function (y) {                                                                                   // 1313
      run(2, {foo: x, bar: y}, true);                                                                                 // 1314
    });                                                                                                               // 1315
    _.each(falsies, function (y) {                                                                                    // 1316
      run(2, {foo: x, bar: y}, false);                                                                                // 1317
    });                                                                                                               // 1318
  });                                                                                                                 // 1319
                                                                                                                      // 1320
  run(3, {foo: true}, false);                                                                                         // 1321
  run(3, {foo: false}, false);                                                                                        // 1322
});                                                                                                                   // 1323
                                                                                                                      // 1324
Tinytest.add("spacebars - templates - double", function (test) {                                                      // 1325
  var tmpl = Template.spacebars_template_test_double;                                                                 // 1326
                                                                                                                      // 1327
  var run = function (foo, expectedResult) {                                                                          // 1328
    tmpl.foo = foo;                                                                                                   // 1329
    var div = renderToDiv(tmpl);                                                                                      // 1330
    test.equal(canonicalizeHtml(div.innerHTML), expectedResult);                                                      // 1331
  };                                                                                                                  // 1332
                                                                                                                      // 1333
  run('asdf', 'asdf');                                                                                                // 1334
  run(1.23, '1.23');                                                                                                  // 1335
  run(0, '0');                                                                                                        // 1336
  run(true, 'true');                                                                                                  // 1337
  run(false, '');                                                                                                     // 1338
  run(null, '');                                                                                                      // 1339
  run(undefined, '');                                                                                                 // 1340
});                                                                                                                   // 1341
                                                                                                                      // 1342
Tinytest.add("spacebars - templates - inclusion lookup order", function (test) {                                      // 1343
  // test that {{> foo}} looks for a helper named 'foo', then a                                                       // 1344
  // template named 'foo', then a 'foo' field in the data context.                                                    // 1345
  var tmpl = Template.spacebars_template_test_inclusion_lookup;                                                       // 1346
  tmpl.data = function () {                                                                                           // 1347
    return {                                                                                                          // 1348
      // shouldn't have an effect since we define a helper with the                                                   // 1349
      // same name.                                                                                                   // 1350
      spacebars_template_test_inclusion_lookup_subtmpl: Template.                                                     // 1351
        spacebars_template_test_inclusion_lookup_subtmpl3,                                                            // 1352
      dataContextSubtmpl: Template.                                                                                   // 1353
        spacebars_template_test_inclusion_lookup_subtmpl3};                                                           // 1354
  };                                                                                                                  // 1355
                                                                                                                      // 1356
  tmpl.spacebars_template_test_inclusion_lookup_subtmpl =                                                             // 1357
    Template.spacebars_template_test_inclusion_lookup_subtmpl2;                                                       // 1358
                                                                                                                      // 1359
  test.equal(canonicalizeHtml(renderToDiv(tmpl).innerHTML),                                                           // 1360
    ["This is generated by a helper with the same name.",                                                             // 1361
     "This is a template passed in the data context."].join(' '));                                                    // 1362
});                                                                                                                   // 1363
                                                                                                                      // 1364
Tinytest.add("spacebars - templates - content context", function (test) {                                             // 1365
  var tmpl = Template.spacebars_template_test_content_context;                                                        // 1366
  var R = ReactiveVar(true);                                                                                          // 1367
  tmpl.foo = {                                                                                                        // 1368
    firstLetter: 'F',                                                                                                 // 1369
    secondLetter: 'O',                                                                                                // 1370
    bar: {                                                                                                            // 1371
      cond: function () { return R.get(); },                                                                          // 1372
      firstLetter: 'B',                                                                                               // 1373
      secondLetter: 'A'                                                                                               // 1374
    }                                                                                                                 // 1375
  };                                                                                                                  // 1376
                                                                                                                      // 1377
  var div = renderToDiv(tmpl);                                                                                        // 1378
  test.equal(canonicalizeHtml(div.innerHTML), 'BO');                                                                  // 1379
  R.set(false);                                                                                                       // 1380
  Deps.flush();                                                                                                       // 1381
  test.equal(canonicalizeHtml(div.innerHTML), 'FA');                                                                  // 1382
});                                                                                                                   // 1383
                                                                                                                      // 1384
_.each(['textarea', 'text', 'password', 'submit', 'button',                                                           // 1385
        'reset', 'select', 'hidden'], function (type) {                                                               // 1386
  Tinytest.add("spacebars - controls - " + type, function(test) {                                                     // 1387
    var R = ReactiveVar({x:"test"});                                                                                  // 1388
    var R2 = ReactiveVar("");                                                                                         // 1389
    var tmpl;                                                                                                         // 1390
                                                                                                                      // 1391
    if (type === 'select') {                                                                                          // 1392
      tmpl = Template.spacebars_test_control_select;                                                                  // 1393
      tmpl.options = ['This is a test', 'This is a fridge',                                                           // 1394
                      'This is a frog', 'This is a new frog', 'foobar',                                               // 1395
                      'This is a photograph', 'This is a monkey',                                                     // 1396
                      'This is a donkey'];                                                                            // 1397
      tmpl.selected = function () {                                                                                   // 1398
        R2.get();  // Re-render when R2 is changed, even though it                                                    // 1399
                   // doesn't affect HTML.                                                                            // 1400
        return ('This is a ' + R.get().x) === this.toString();                                                        // 1401
      };                                                                                                              // 1402
    } else if (type === 'textarea') {                                                                                 // 1403
      tmpl = Template.spacebars_test_control_textarea;                                                                // 1404
      tmpl.value = function () {                                                                                      // 1405
        R2.get();  // Re-render when R2 is changed, even though it                                                    // 1406
                   // doesn't affect HTML.                                                                            // 1407
        return 'This is a ' + R.get().x;                                                                              // 1408
      };                                                                                                              // 1409
    } else {                                                                                                          // 1410
      tmpl = Template.spacebars_test_control_input;                                                                   // 1411
      tmpl.value = function () {                                                                                      // 1412
        R2.get();  // Re-render when R2 is changed, even though it                                                    // 1413
                   // doesn't affect HTML.                                                                            // 1414
        return 'This is a ' + R.get().x;                                                                              // 1415
      };                                                                                                              // 1416
      tmpl.type = type;                                                                                               // 1417
    };                                                                                                                // 1418
                                                                                                                      // 1419
    var div = renderToDiv(tmpl);                                                                                      // 1420
    document.body.appendChild(div);                                                                                   // 1421
    var canFocus = (type !== 'hidden');                                                                               // 1422
                                                                                                                      // 1423
    // find first element child, ignoring any marker nodes                                                            // 1424
    var input = div.firstChild;                                                                                       // 1425
    while (input.nodeType !== 1)                                                                                      // 1426
      input = input.nextSibling;                                                                                      // 1427
                                                                                                                      // 1428
    if (type === 'textarea' || type === 'select') {                                                                   // 1429
      test.equal(input.nodeName, type.toUpperCase());                                                                 // 1430
    } else {                                                                                                          // 1431
      test.equal(input.nodeName, 'INPUT');                                                                            // 1432
      test.equal(input.type, type);                                                                                   // 1433
    }                                                                                                                 // 1434
    test.equal(DomUtils.getElementValue(input), "This is a test");                                                    // 1435
                                                                                                                      // 1436
    // value updates reactively                                                                                       // 1437
    R.set({x:"fridge"});                                                                                              // 1438
    Deps.flush();                                                                                                     // 1439
    test.equal(DomUtils.getElementValue(input), "This is a fridge");                                                  // 1440
                                                                                                                      // 1441
    if (canFocus) {                                                                                                   // 1442
      // ...unless focused                                                                                            // 1443
      focusElement(input);                                                                                            // 1444
      R.set({x:"frog"});                                                                                              // 1445
                                                                                                                      // 1446
      Deps.flush();                                                                                                   // 1447
      test.equal(DomUtils.getElementValue(input), "This is a fridge");                                                // 1448
                                                                                                                      // 1449
      // blurring and re-setting works                                                                                // 1450
      blurElement(input);                                                                                             // 1451
      Deps.flush();                                                                                                   // 1452
      test.equal(DomUtils.getElementValue(input), "This is a fridge");                                                // 1453
    }                                                                                                                 // 1454
    R.set({x:"new frog"});                                                                                            // 1455
    Deps.flush();                                                                                                     // 1456
                                                                                                                      // 1457
    test.equal(DomUtils.getElementValue(input), "This is a new frog");                                                // 1458
                                                                                                                      // 1459
    // Setting a value (similar to user typing) should prevent value from being                                       // 1460
    // reverted if the div is re-rendered but the rendered value (ie, R) does                                         // 1461
    // not change.                                                                                                    // 1462
    DomUtils.setElementValue(input, "foobar");                                                                        // 1463
    R2.set("change");                                                                                                 // 1464
    Deps.flush();                                                                                                     // 1465
    test.equal(DomUtils.getElementValue(input), "foobar");                                                            // 1466
                                                                                                                      // 1467
    // ... but if the actual rendered value changes, that should take effect.                                         // 1468
    R.set({x:"photograph"});                                                                                          // 1469
    Deps.flush();                                                                                                     // 1470
    test.equal(DomUtils.getElementValue(input), "This is a photograph");                                              // 1471
                                                                                                                      // 1472
    document.body.removeChild(div);                                                                                   // 1473
  });                                                                                                                 // 1474
});                                                                                                                   // 1475
                                                                                                                      // 1476
Tinytest.add("spacebars - controls - radio", function(test) {                                                         // 1477
  var R = ReactiveVar("");                                                                                            // 1478
  var R2 = ReactiveVar("");                                                                                           // 1479
  var change_buf = [];                                                                                                // 1480
  var tmpl = Template.spacebars_test_control_radio;                                                                   // 1481
  tmpl.bands = ["AM", "FM", "XM"];                                                                                    // 1482
  tmpl.isChecked = function () {                                                                                      // 1483
    return R.get() === this.toString();                                                                               // 1484
  };                                                                                                                  // 1485
  tmpl.band = function () {                                                                                           // 1486
    return R.get();                                                                                                   // 1487
  };                                                                                                                  // 1488
  tmpl.events({                                                                                                       // 1489
    'change input': function (event) {                                                                                // 1490
      var btn = event.target;                                                                                         // 1491
      var band = btn.value;                                                                                           // 1492
      change_buf.push(band);                                                                                          // 1493
      R.set(band);                                                                                                    // 1494
    }                                                                                                                 // 1495
  });                                                                                                                 // 1496
                                                                                                                      // 1497
  var div = renderToDiv(tmpl);                                                                                        // 1498
  document.body.appendChild(div);                                                                                     // 1499
                                                                                                                      // 1500
  // get the three buttons; they should not change identities!                                                        // 1501
  var btns = nodesToArray(div.getElementsByTagName("INPUT"));                                                         // 1502
  var text = function () {                                                                                            // 1503
    var text = div.innerText || div.textContent;                                                                      // 1504
    return text.replace(/[ \n\r]+/g, " ");                                                                            // 1505
  };                                                                                                                  // 1506
                                                                                                                      // 1507
  test.equal(_.pluck(btns, 'checked'), [false, false, false]);                                                        // 1508
  test.equal(text(), "Band: ");                                                                                       // 1509
                                                                                                                      // 1510
  clickIt(btns[0]);                                                                                                   // 1511
  test.equal(change_buf, ['AM']);                                                                                     // 1512
  change_buf.length = 0;                                                                                              // 1513
  Deps.flush();                                                                                                       // 1514
  test.equal(_.pluck(btns, 'checked'), [true, false, false]);                                                         // 1515
  test.equal(text(), "Band: AM");                                                                                     // 1516
                                                                                                                      // 1517
  R2.set("change");                                                                                                   // 1518
  Deps.flush();                                                                                                       // 1519
  test.length(change_buf, 0);                                                                                         // 1520
  test.equal(_.pluck(btns, 'checked'), [true, false, false]);                                                         // 1521
  test.equal(text(), "Band: AM");                                                                                     // 1522
                                                                                                                      // 1523
  clickIt(btns[1]);                                                                                                   // 1524
  test.equal(change_buf, ['FM']);                                                                                     // 1525
  change_buf.length = 0;                                                                                              // 1526
  Deps.flush();                                                                                                       // 1527
  test.equal(_.pluck(btns, 'checked'), [false, true, false]);                                                         // 1528
  test.equal(text(), "Band: FM");                                                                                     // 1529
                                                                                                                      // 1530
  clickIt(btns[2]);                                                                                                   // 1531
  test.equal(change_buf, ['XM']);                                                                                     // 1532
  change_buf.length = 0;                                                                                              // 1533
  Deps.flush();                                                                                                       // 1534
  test.equal(_.pluck(btns, 'checked'), [false, false, true]);                                                         // 1535
  test.equal(text(), "Band: XM");                                                                                     // 1536
                                                                                                                      // 1537
  clickIt(btns[1]);                                                                                                   // 1538
  test.equal(change_buf, ['FM']);                                                                                     // 1539
  change_buf.length = 0;                                                                                              // 1540
  Deps.flush();                                                                                                       // 1541
  test.equal(_.pluck(btns, 'checked'), [false, true, false]);                                                         // 1542
  test.equal(text(), "Band: FM");                                                                                     // 1543
                                                                                                                      // 1544
  document.body.removeChild(div);                                                                                     // 1545
});                                                                                                                   // 1546
                                                                                                                      // 1547
Tinytest.add("spacebars - controls - checkbox", function(test) {                                                      // 1548
  var tmpl = Template.spacebars_test_control_checkbox;                                                                // 1549
  tmpl.labels = ["Foo", "Bar", "Baz"];                                                                                // 1550
  var Rs = {};                                                                                                        // 1551
  _.each(tmpl.labels, function (label) {                                                                              // 1552
    Rs[label] = ReactiveVar(false);                                                                                   // 1553
  });                                                                                                                 // 1554
  tmpl.isChecked = function () {                                                                                      // 1555
    return Rs[this.toString()].get();                                                                                 // 1556
  };                                                                                                                  // 1557
  var changeBuf = [];                                                                                                 // 1558
                                                                                                                      // 1559
  var div = renderToDiv(tmpl);                                                                                        // 1560
  document.body.appendChild(div);                                                                                     // 1561
                                                                                                                      // 1562
  var boxes = nodesToArray(div.getElementsByTagName("INPUT"));                                                        // 1563
                                                                                                                      // 1564
  test.equal(_.pluck(boxes, 'checked'), [false, false, false]);                                                       // 1565
                                                                                                                      // 1566
  // Re-render with first one checked.                                                                                // 1567
  Rs.Foo.set(true);                                                                                                   // 1568
  Deps.flush();                                                                                                       // 1569
  test.equal(_.pluck(boxes, 'checked'), [true, false, false]);                                                        // 1570
                                                                                                                      // 1571
  // Re-render with first one unchecked again.                                                                        // 1572
  Rs.Foo.set(false);                                                                                                  // 1573
  Deps.flush();                                                                                                       // 1574
  test.equal(_.pluck(boxes, 'checked'), [false, false, false]);                                                       // 1575
                                                                                                                      // 1576
  // User clicks the second one.                                                                                      // 1577
  clickElement(boxes[1]);                                                                                             // 1578
  test.equal(_.pluck(boxes, 'checked'), [false, true, false]);                                                        // 1579
  Deps.flush();                                                                                                       // 1580
  test.equal(_.pluck(boxes, 'checked'), [false, true, false]);                                                        // 1581
                                                                                                                      // 1582
  // Re-render with third one checked. Second one should stay checked because                                         // 1583
  // it's a user update!                                                                                              // 1584
  Rs.Baz.set(true);                                                                                                   // 1585
  Deps.flush();                                                                                                       // 1586
  test.equal(_.pluck(boxes, 'checked'), [false, true, true]);                                                         // 1587
                                                                                                                      // 1588
  // User turns second and third off.                                                                                 // 1589
  clickElement(boxes[1]);                                                                                             // 1590
  clickElement(boxes[2]);                                                                                             // 1591
  test.equal(_.pluck(boxes, 'checked'), [false, false, false]);                                                       // 1592
  Deps.flush();                                                                                                       // 1593
  test.equal(_.pluck(boxes, 'checked'), [false, false, false]);                                                       // 1594
                                                                                                                      // 1595
  // Re-render with first one checked. Third should stay off because it's a user                                      // 1596
  // update!                                                                                                          // 1597
  Rs.Foo.set(true);                                                                                                   // 1598
  Deps.flush();                                                                                                       // 1599
  test.equal(_.pluck(boxes, 'checked'), [true, false, false]);                                                        // 1600
                                                                                                                      // 1601
  // Re-render with first one unchecked. Third should still stay off.                                                 // 1602
  Rs.Foo.set(false);                                                                                                  // 1603
  Deps.flush();                                                                                                       // 1604
  test.equal(_.pluck(boxes, 'checked'), [false, false, false]);                                                       // 1605
                                                                                                                      // 1606
  document.body.removeChild(div);                                                                                     // 1607
});                                                                                                                   // 1608
                                                                                                                      // 1609
Tinytest.add('spacebars - template - unfound template', function (test) {                                             // 1610
  test.throws(function () {                                                                                           // 1611
    renderToDiv(Template.spacebars_test_nonexistent_template);                                                        // 1612
  }, /Can't find template/);                                                                                          // 1613
});                                                                                                                   // 1614
                                                                                                                      // 1615
Tinytest.add('spacebars - template - helper passed to #if called exactly once when invalidated', function (test) {    // 1616
  var tmpl = Template.spacebars_test_if_helper;                                                                       // 1617
                                                                                                                      // 1618
  var count = 0;                                                                                                      // 1619
  var d = new Deps.Dependency;                                                                                        // 1620
  tmpl.foo = function () {                                                                                            // 1621
    d.depend();                                                                                                       // 1622
    count++;                                                                                                          // 1623
    return foo;                                                                                                       // 1624
  };                                                                                                                  // 1625
                                                                                                                      // 1626
  foo = false;                                                                                                        // 1627
  var div = renderToDiv(tmpl);                                                                                        // 1628
  divRendersTo(test, div, "false");                                                                                   // 1629
  test.equal(count, 1);                                                                                               // 1630
                                                                                                                      // 1631
  foo = true;                                                                                                         // 1632
  d.changed();                                                                                                        // 1633
  divRendersTo(test, div, "true");                                                                                    // 1634
  test.equal(count, 2);                                                                                               // 1635
});                                                                                                                   // 1636
                                                                                                                      // 1637
Tinytest.add('spacebars - template - custom block helper functions called exactly once when invalidated', function (test) {
  var tmpl = Template.spacebars_test_block_helper_function;                                                           // 1639
                                                                                                                      // 1640
  var count = 0;                                                                                                      // 1641
  var d = new Deps.Dependency;                                                                                        // 1642
  tmpl.foo = function () {                                                                                            // 1643
    d.depend();                                                                                                       // 1644
    count++;                                                                                                          // 1645
    return UI.block(function () { return []; });                                                                      // 1646
  };                                                                                                                  // 1647
                                                                                                                      // 1648
  foo = false;                                                                                                        // 1649
  renderToDiv(tmpl);                                                                                                  // 1650
  Deps.flush();                                                                                                       // 1651
  test.equal(count, 1);                                                                                               // 1652
                                                                                                                      // 1653
  foo = true;                                                                                                         // 1654
  d.changed();                                                                                                        // 1655
  Deps.flush();                                                                                                       // 1656
  test.equal(count, 2);                                                                                               // 1657
});                                                                                                                   // 1658
                                                                                                                      // 1659
var runOneTwoTest = function (test, subTemplateName, optionsData) {                                                   // 1660
  _.each([Template.spacebars_test_helpers_stop_onetwo,                                                                // 1661
          Template.spacebars_test_helpers_stop_onetwo_attribute],                                                     // 1662
         function (tmpl) {                                                                                            // 1663
                                                                                                                      // 1664
           tmpl.one = Template[subTemplateName + '1'];                                                                // 1665
           tmpl.two = Template[subTemplateName + '2'];                                                                // 1666
                                                                                                                      // 1667
           var buf = '';                                                                                              // 1668
                                                                                                                      // 1669
           var showOne = ReactiveVar(true);                                                                           // 1670
           var dummy = ReactiveVar(0);                                                                                // 1671
                                                                                                                      // 1672
           tmpl.showOne = function () { return showOne.get(); };                                                      // 1673
           tmpl.one.options = function () {                                                                           // 1674
             var x = dummy.get();                                                                                     // 1675
             buf += '1';                                                                                              // 1676
             if (optionsData)                                                                                         // 1677
               return optionsData[x];                                                                                 // 1678
             else                                                                                                     // 1679
               return ['something'];                                                                                  // 1680
           };                                                                                                         // 1681
           tmpl.two.options = function () {                                                                           // 1682
             var x = dummy.get();                                                                                     // 1683
             buf += '2';                                                                                              // 1684
             if (optionsData)                                                                                         // 1685
               return optionsData[x];                                                                                 // 1686
             else                                                                                                     // 1687
               return ['something'];                                                                                  // 1688
           };                                                                                                         // 1689
                                                                                                                      // 1690
           var div = renderToDiv(tmpl);                                                                               // 1691
           Deps.flush();                                                                                              // 1692
           test.equal(buf, '1');                                                                                      // 1693
                                                                                                                      // 1694
           showOne.set(false);                                                                                        // 1695
           dummy.set(1);                                                                                              // 1696
           Deps.flush();                                                                                              // 1697
           test.equal(buf, '12');                                                                                     // 1698
                                                                                                                      // 1699
           showOne.set(true);                                                                                         // 1700
           dummy.set(2);                                                                                              // 1701
           Deps.flush();                                                                                              // 1702
           test.equal(buf, '121');                                                                                    // 1703
                                                                                                                      // 1704
           // clean up the div                                                                                        // 1705
           $(div).remove();                                                                                           // 1706
           test.equal(showOne.numListeners(), 0);                                                                     // 1707
           test.equal(dummy.numListeners(), 0);                                                                       // 1708
         });                                                                                                          // 1709
};                                                                                                                    // 1710
                                                                                                                      // 1711
Tinytest.add('spacebars - template - with stops without re-running helper', function (test) {                         // 1712
  runOneTwoTest(test, 'spacebars_test_helpers_stop_with');                                                            // 1713
});                                                                                                                   // 1714
                                                                                                                      // 1715
Tinytest.add('spacebars - template - each stops without re-running helper', function (test) {                         // 1716
  runOneTwoTest(test, 'spacebars_test_helpers_stop_each');                                                            // 1717
});                                                                                                                   // 1718
                                                                                                                      // 1719
Tinytest.add('spacebars - template - each inside with stops without re-running helper', function (test) {             // 1720
  runOneTwoTest(test, 'spacebars_test_helpers_stop_with_each');                                                       // 1721
});                                                                                                                   // 1722
                                                                                                                      // 1723
Tinytest.add('spacebars - template - if stops without re-running helper', function (test) {                           // 1724
  runOneTwoTest(test, 'spacebars_test_helpers_stop_if', ['a', 'b', 'a']);                                             // 1725
});                                                                                                                   // 1726
                                                                                                                      // 1727
Tinytest.add('spacebars - template - unless stops without re-running helper', function (test) {                       // 1728
  runOneTwoTest(test, 'spacebars_test_helpers_stop_unless', ['a', 'b', 'a']);                                         // 1729
});                                                                                                                   // 1730
                                                                                                                      // 1731
Tinytest.add('spacebars - template - inclusion stops without re-running function', function (test) {                  // 1732
  var t = Template.spacebars_test_helpers_stop_inclusion3;                                                            // 1733
  runOneTwoTest(test, 'spacebars_test_helpers_stop_inclusion', [t, t, t]);                                            // 1734
});                                                                                                                   // 1735
                                                                                                                      // 1736
Tinytest.add('spacebars - template - template with callbacks inside with stops without recalculating data', function (test) {
  var tmpl = Template.spacebars_test_helpers_stop_with_callbacks3;                                                    // 1738
  tmpl.created = function () {};                                                                                      // 1739
  tmpl.rendered = function () {};                                                                                     // 1740
  tmpl.destroyed = function () {};                                                                                    // 1741
  runOneTwoTest(test, 'spacebars_test_helpers_stop_with_callbacks');                                                  // 1742
});                                                                                                                   // 1743
                                                                                                                      // 1744
Tinytest.add('spacebars - template - no data context is seen as an empty object', function (test) {                   // 1745
  var tmpl = Template.spacebars_test_no_data_context;                                                                 // 1746
                                                                                                                      // 1747
  var dataInHelper = 'UNSET';                                                                                         // 1748
  var dataInRendered = 'UNSET';                                                                                       // 1749
  var dataInCreated = 'UNSET';                                                                                        // 1750
  var dataInDestroyed = 'UNSET';                                                                                      // 1751
  var dataInEvent = 'UNSET';                                                                                          // 1752
                                                                                                                      // 1753
  tmpl.foo = function () {                                                                                            // 1754
    dataInHelper = this;                                                                                              // 1755
  };                                                                                                                  // 1756
  tmpl.created = function () {                                                                                        // 1757
    dataInCreated = this.data;                                                                                        // 1758
  };                                                                                                                  // 1759
  tmpl.rendered = function () {                                                                                       // 1760
    dataInRendered = this.data;                                                                                       // 1761
  };                                                                                                                  // 1762
  tmpl.destroyed = function () {                                                                                      // 1763
    dataInDestroyed = this.data;                                                                                      // 1764
  };                                                                                                                  // 1765
  tmpl.events({                                                                                                       // 1766
    'click': function () {                                                                                            // 1767
      dataInEvent = this;                                                                                             // 1768
    }                                                                                                                 // 1769
  });                                                                                                                 // 1770
                                                                                                                      // 1771
  var div = renderToDiv(tmpl);                                                                                        // 1772
  document.body.appendChild(div);                                                                                     // 1773
  clickElement(div.querySelector('button'));                                                                          // 1774
  Deps.flush(); // rendered gets called afterFlush                                                                    // 1775
  $(div).remove();                                                                                                    // 1776
                                                                                                                      // 1777
  test.isFalse(dataInHelper === window);                                                                              // 1778
  test.equal(dataInHelper, {});                                                                                       // 1779
  test.equal(dataInCreated, null);                                                                                    // 1780
  test.equal(dataInRendered, null);                                                                                   // 1781
  test.equal(dataInDestroyed, null);                                                                                  // 1782
  test.isFalse(dataInEvent === window);                                                                               // 1783
  test.equal(dataInEvent, {});                                                                                        // 1784
});                                                                                                                   // 1785
                                                                                                                      // 1786
Tinytest.add('spacebars - template - falsy with', function (test) {                                                   // 1787
  var tmpl = Template.spacebars_test_falsy_with;                                                                      // 1788
  var R = ReactiveVar(null);                                                                                          // 1789
  tmpl.obj = function () { return R.get(); };                                                                         // 1790
                                                                                                                      // 1791
  var div = renderToDiv(tmpl);                                                                                        // 1792
  divRendersTo(test, div, "");                                                                                        // 1793
                                                                                                                      // 1794
  R.set({greekLetter: 'alpha'});                                                                                      // 1795
  divRendersTo(test, div, "alpha");                                                                                   // 1796
                                                                                                                      // 1797
  R.set(null);                                                                                                        // 1798
  divRendersTo(test, div, "");                                                                                        // 1799
                                                                                                                      // 1800
  R.set({greekLetter: 'alpha'});                                                                                      // 1801
  divRendersTo(test, div, "alpha");                                                                                   // 1802
});                                                                                                                   // 1803
                                                                                                                      // 1804
Tinytest.add("spacebars - template - helpers don't leak", function (test) {                                           // 1805
  var tmpl = Template.spacebars_test_helpers_dont_leak;                                                               // 1806
  tmpl.foo = "wrong";                                                                                                 // 1807
  tmpl.bar = function () { return "WRONG"; };                                                                         // 1808
                                                                                                                      // 1809
  // Also test that custom block helpers (implemented as templates) do NOT                                            // 1810
  // interfere with helper lookup in the current template                                                             // 1811
  Template.spacebars_test_helpers_dont_leak2.bonus =                                                                  // 1812
    function () { return 'BONUS'; };                                                                                  // 1813
                                                                                                                      // 1814
  var div = renderToDiv(tmpl);                                                                                        // 1815
  divRendersTo(test, div, "correct BONUS");                                                                           // 1816
});                                                                                                                   // 1817
                                                                                                                      // 1818
Tinytest.add(                                                                                                         // 1819
  "spacebars - template - event handler returns false",                                                               // 1820
  function (test) {                                                                                                   // 1821
    var tmpl = Template.spacebars_test_event_returns_false;                                                           // 1822
    var elemId = "spacebars_test_event_returns_false_link";                                                           // 1823
    tmpl.events({                                                                                                     // 1824
      'click a': function (evt) { return false; }                                                                     // 1825
    });                                                                                                               // 1826
                                                                                                                      // 1827
    var div = renderToDiv(tmpl);                                                                                      // 1828
    document.body.appendChild(div);                                                                                   // 1829
    clickIt(document.getElementById(elemId));                                                                         // 1830
    test.isFalse(/#bad-url/.test(window.location.hash));                                                              // 1831
  }                                                                                                                   // 1832
);                                                                                                                    // 1833
                                                                                                                      // 1834
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
