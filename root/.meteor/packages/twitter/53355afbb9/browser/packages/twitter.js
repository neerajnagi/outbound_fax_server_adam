(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/twitter/template.twitter_configure.js                                       //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
                                                                                        // 1
Template.__define__("configureLoginServiceDialogForTwitter", (function() {              // 2
  var self = this;                                                                      // 3
  var template = this;                                                                  // 4
  return [ HTML.Raw("<p>\n    First, you'll need to register your app on Twitter. Follow these steps:\n  </p>\n  "), HTML.OL(HTML.Raw('\n    <li>\n      Visit <a href="https://dev.twitter.com/apps/new" target="_blank">https://dev.twitter.com/apps/new</a>\n    </li>\n    '), HTML.LI("\n      Set Callback URL to: ", HTML.SPAN({
    "class": "url"                                                                      // 6
  }, function() {                                                                       // 7
    return Spacebars.mustache(self.lookup("siteUrl"));                                  // 8
  }, "_oauth/twitter?close"), "\n    "), "\n  ") ];                                     // 9
}));                                                                                    // 10
                                                                                        // 11
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/twitter/twitter_configure.js                                                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
Template.configureLoginServiceDialogForTwitter.siteUrl = function () {                  // 1
  // Twitter doesn't recognize localhost as a domain name                               // 2
  return Meteor.absoluteUrl({replaceLocalhost: true});                                  // 3
};                                                                                      // 4
                                                                                        // 5
Template.configureLoginServiceDialogForTwitter.fields = function () {                   // 6
  return [                                                                              // 7
    {property: 'consumerKey', label: 'Consumer key'},                                   // 8
    {property: 'secret', label: 'Consumer secret'}                                      // 9
  ];                                                                                    // 10
};                                                                                      // 11
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/twitter/twitter_client.js                                                   //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
Twitter = {};                                                                           // 1
                                                                                        // 2
// Request Twitter credentials for the user                                             // 3
// @param options {optional}  XXX support options.requestPermissions                    // 4
// @param credentialRequestCompleteCallback {Function} Callback function to call on     // 5
//   completion. Takes one argument, credentialToken on success, or Error on            // 6
//   error.                                                                             // 7
Twitter.requestCredential = function (options, credentialRequestCompleteCallback) {     // 8
  // support both (options, callback) and (callback).                                   // 9
  if (!credentialRequestCompleteCallback && typeof options === 'function') {            // 10
    credentialRequestCompleteCallback = options;                                        // 11
    options = {};                                                                       // 12
  }                                                                                     // 13
                                                                                        // 14
  var config = ServiceConfiguration.configurations.findOne({service: 'twitter'});       // 15
  if (!config) {                                                                        // 16
    credentialRequestCompleteCallback && credentialRequestCompleteCallback(new ServiceConfiguration.ConfigError("Service not configured"));
    return;                                                                             // 18
  }                                                                                     // 19
                                                                                        // 20
  var credentialToken = Random.id();                                                    // 21
  // We need to keep credentialToken across the next two 'steps' so we're adding        // 22
  // a credentialToken parameter to the url and the callback url that we'll be returned // 23
  // to by oauth provider                                                               // 24
                                                                                        // 25
  // url to app, enters "step 1" as described in                                        // 26
  // packages/accounts-oauth1-helper/oauth1_server.js                                   // 27
  var loginUrl = '/_oauth/twitter/?requestTokenAndRedirect=true'                        // 28
        + '&state=' + credentialToken;                                                  // 29
                                                                                        // 30
  Oauth.showPopup(                                                                      // 31
    loginUrl,                                                                           // 32
    _.bind(credentialRequestCompleteCallback, null, credentialToken)                    // 33
  );                                                                                    // 34
};                                                                                      // 35
                                                                                        // 36
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
