(function () {

(function () {

//////////////////////////////////////////////////////////////////////////////
//                                                                          //
// plugin/compile-less.js                                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
                                                                            //
var fs = Npm.require('fs');                                                 // 1
var path = Npm.require('path');                                             // 2
var less = Npm.require('less');                                             // 3
var Future = Npm.require('fibers/future');                                  // 4
                                                                            // 5
Plugin.registerSourceHandler("less", function (compileStep) {               // 6
  // XXX annoying that this is replicated in .css, .less, and .styl         // 7
  if (! compileStep.archMatches('browser')) {                               // 8
    // XXX in the future, might be better to emit some kind of a            // 9
    // warning if a stylesheet is included on the server, rather than       // 10
    // silently ignoring it. but that would mean you can't stick .css       // 11
    // at the top level of your app, which is kind of silly.                // 12
    return;                                                                 // 13
  }                                                                         // 14
                                                                            // 15
  var source = compileStep.read().toString('utf8');                         // 16
  var options = {                                                           // 17
    // Use fs.readFileSync to process @imports. This is the bundler, so     // 18
    // that's not going to cause concurrency issues, and it means that (a)  // 19
    // we don't have to use Futures and (b) errors thrown by bugs in less   // 20
    // actually get caught.                                                 // 21
    syncImport: true,                                                       // 22
    paths: [path.dirname(compileStep._fullInputPath)] // for @import        // 23
  };                                                                        // 24
                                                                            // 25
  var parser = new less.Parser(options);                                    // 26
  var astFuture = new Future;                                               // 27
  var sourceMap = null;                                                     // 28
  try {                                                                     // 29
    parser.parse(source, astFuture.resolver());                             // 30
    var ast = astFuture.wait();                                             // 31
                                                                            // 32
    var css = ast.toCSS({                                                   // 33
      sourceMap: true,                                                      // 34
      writeSourceMap: function (sm) {                                       // 35
        sourceMap = JSON.parse(sm);                                         // 36
      }                                                                     // 37
    });                                                                     // 38
  } catch (e) {                                                             // 39
    // less.Parser.parse is supposed to report any errors via its           // 40
    // callback. But sometimes, it throws them instead. This is             // 41
    // probably a bug in less. Be prepared for either behavior.             // 42
    compileStep.error({                                                     // 43
      message: "Less compiler error: " + e.message,                         // 44
      sourcePath: e.filename || compileStep.inputPath,                      // 45
      line: e.line - 1,  // dunno why, but it matches                       // 46
      column: e.column + 1                                                  // 47
    });                                                                     // 48
    return;                                                                 // 49
  }                                                                         // 50
                                                                            // 51
                                                                            // 52
  if (sourceMap) {                                                          // 53
    sourceMap.sources = [compileStep.inputPath];                            // 54
    sourceMap.sourcesContent = [source];                                    // 55
    sourceMap = JSON.stringify(sourceMap);                                  // 56
  }                                                                         // 57
                                                                            // 58
  compileStep.addStylesheet({                                               // 59
    path: compileStep.inputPath + ".css",                                   // 60
    data: css,                                                              // 61
    sourceMap: sourceMap                                                    // 62
  });                                                                       // 63
});;                                                                        // 64
                                                                            // 65
// Register import.less files with the dependency watcher, without actually // 66
// processing them. There is a similar rule in the stylus package.          // 67
Plugin.registerSourceHandler("import.less", function () {                   // 68
  // Do nothing                                                             // 69
});                                                                         // 70
                                                                            // 71
// Backward compatibility with Meteor 0.7                                   // 72
Plugin.registerSourceHandler("lessimport", function () {});                 // 73
                                                                            // 74
//////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.compileLess = {};

})();
