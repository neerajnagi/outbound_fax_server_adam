(function () {

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/less/less_tests.js                                       //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
                                                                     // 1
Tinytest.add("less - presence", function(test) {                     // 2
                                                                     // 3
  var div = document.createElement('div');                           // 4
  UI.materialize(Template.less_test_presence, div);                  // 5
  div.style.display = 'block';                                       // 6
  document.body.appendChild(div);                                    // 7
                                                                     // 8
  var p = div.querySelector('p');                                    // 9
  test.equal(getStyleProperty(p, 'border-left-style'), "dashed");    // 10
                                                                     // 11
  // test @import                                                    // 12
  test.equal(getStyleProperty(p, 'border-right-style'), "dotted");   // 13
  test.equal(getStyleProperty(p, 'border-bottom-style'), "double");  // 14
                                                                     // 15
  document.body.removeChild(div);                                    // 16
});                                                                  // 17
                                                                     // 18
///////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/less/template.less_tests.js                              //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
                                                                     // 1
Template.__define__("less_test_presence", (function() {              // 2
  var self = this;                                                   // 3
  var template = this;                                               // 4
  return HTML.Raw('<p class="less-dashy-left-border"></p>');         // 5
}));                                                                 // 6
                                                                     // 7
///////////////////////////////////////////////////////////////////////

}).call(this);
