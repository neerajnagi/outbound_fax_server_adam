(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/accounts-password/password_tests_setup.js                                   //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
Accounts.validateNewUser(function (user) {                                              // 1
  if (user.profile && user.profile.invalidAndThrowException)                            // 2
    throw new Meteor.Error(403, "An exception thrown within Accounts.validateNewUser"); // 3
  return !(user.profile && user.profile.invalid);                                       // 4
});                                                                                     // 5
                                                                                        // 6
Accounts.onCreateUser(function (options, user) {                                        // 7
  if (options.testOnCreateUserHook) {                                                   // 8
    user.profile = user.profile || {};                                                  // 9
    user.profile.touchedByOnCreateUser = true;                                          // 10
    return user;                                                                        // 11
  } else {                                                                              // 12
    return 'TEST DEFAULT HOOK';                                                         // 13
  }                                                                                     // 14
});                                                                                     // 15
                                                                                        // 16
                                                                                        // 17
// connection id -> true                                                                // 18
var invalidateLogins = {};                                                              // 19
                                                                                        // 20
                                                                                        // 21
Meteor.methods({                                                                        // 22
  testInvalidateLogins: function (flag) {                                               // 23
    if (flag)                                                                           // 24
      invalidateLogins[this.connection.id] = true;                                      // 25
    else                                                                                // 26
      delete invalidateLogins[this.connection.id];                                      // 27
  }                                                                                     // 28
});                                                                                     // 29
                                                                                        // 30
                                                                                        // 31
Accounts.validateLoginAttempt(function (attempt) {                                      // 32
  return ! (attempt &&                                                                  // 33
            attempt.connection &&                                                       // 34
            invalidateLogins[attempt.connection.id]);                                   // 35
});                                                                                     // 36
                                                                                        // 37
                                                                                        // 38
// connection id -> [{successful: boolean, attempt: object}]                            // 39
var capturedLogins = {};                                                                // 40
                                                                                        // 41
Meteor.methods({                                                                        // 42
  testCaptureLogins: function () {                                                      // 43
    capturedLogins[this.connection.id] = [];                                            // 44
  },                                                                                    // 45
                                                                                        // 46
  testFetchCapturedLogins: function () {                                                // 47
    if (capturedLogins[this.connection.id]) {                                           // 48
      var logins = capturedLogins[this.connection.id];                                  // 49
      delete capturedLogins[this.connection.id];                                        // 50
      return logins;                                                                    // 51
    }                                                                                   // 52
    else                                                                                // 53
      return [];                                                                        // 54
  }                                                                                     // 55
});                                                                                     // 56
                                                                                        // 57
Accounts.onLogin(function (attempt) {                                                   // 58
  if (capturedLogins[attempt.connection.id])                                            // 59
    capturedLogins[attempt.connection.id].push({                                        // 60
      successful: true,                                                                 // 61
      attempt: _.omit(attempt, 'connection')                                            // 62
    });                                                                                 // 63
});                                                                                     // 64
                                                                                        // 65
Accounts.onLoginFailure(function (attempt) {                                            // 66
  if (capturedLogins[attempt.connection.id]) {                                          // 67
    capturedLogins[attempt.connection.id].push({                                        // 68
      successful: false,                                                                // 69
      attempt: _.omit(attempt, 'connection')                                            // 70
    });                                                                                 // 71
  }                                                                                     // 72
});                                                                                     // 73
                                                                                        // 74
// Because this is global state that affects every client, we can't turn                // 75
// it on and off during the tests. Doing so would mean two simultaneous                 // 76
// test runs could collide with each other.                                             // 77
//                                                                                      // 78
// We should probably have some sort of server-isolation between                        // 79
// multiple test runs. Perhaps a separate server instance per run. This                 // 80
// problem isn't unique to this test, there are other places in the code                // 81
// where we do various hacky things to work around the lack of                          // 82
// server-side isolation.                                                               // 83
//                                                                                      // 84
// For now, we just test the one configuration state. You can comment                   // 85
// out each configuration option and see that the tests fail.                           // 86
Accounts.config({                                                                       // 87
  sendVerificationEmail: true                                                           // 88
});                                                                                     // 89
                                                                                        // 90
                                                                                        // 91
Meteor.methods({                                                                        // 92
  testMeteorUser: function () { return Meteor.user(); },                                // 93
  clearUsernameAndProfile: function () {                                                // 94
    if (!this.userId)                                                                   // 95
      throw new Error("Not logged in!");                                                // 96
    Meteor.users.update(this.userId,                                                    // 97
                        {$unset: {profile: 1, username: 1}});                           // 98
  },                                                                                    // 99
                                                                                        // 100
  expireTokens: function () {                                                           // 101
    Accounts._expireTokens(new Date(), this.userId);                                    // 102
  },                                                                                    // 103
  removeUser: function (username) {                                                     // 104
    Meteor.users.remove({ "username": username });                                      // 105
  }                                                                                     // 106
});                                                                                     // 107
                                                                                        // 108
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/accounts-password/password_tests.js                                         //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
Accounts._noConnectionCloseDelayForTest = true;                                         // 1
                                                                                        // 2
if (Meteor.isServer) {                                                                  // 3
  Meteor.methods({                                                                      // 4
    getUserId: function () {                                                            // 5
      return this.userId;                                                               // 6
    }                                                                                   // 7
  });                                                                                   // 8
}                                                                                       // 9
                                                                                        // 10
if (Meteor.isClient) (function () {                                                     // 11
                                                                                        // 12
  // XXX note, only one test can do login/logout things at once! for                    // 13
  // now, that is this test.                                                            // 14
                                                                                        // 15
  Accounts._isolateLoginTokenForTest();                                                 // 16
                                                                                        // 17
  var logoutStep = function (test, expect) {                                            // 18
    Meteor.logout(expect(function (error) {                                             // 19
      test.equal(error, undefined);                                                     // 20
      test.equal(Meteor.user(), null);                                                  // 21
    }));                                                                                // 22
  };                                                                                    // 23
  var loggedInAs = function (someUsername, test, expect) {                              // 24
    return expect(function (error) {                                                    // 25
      test.equal(error, undefined);                                                     // 26
      test.equal(Meteor.user().username, someUsername);                                 // 27
    });                                                                                 // 28
  };                                                                                    // 29
  var waitForLoggedOutStep = function (test, expect) {                                  // 30
    pollUntil(expect, function () {                                                     // 31
      return Meteor.userId() === null;                                                  // 32
    }, 10 * 1000, 100);                                                                 // 33
  };                                                                                    // 34
  var invalidateLoginsStep = function (test, expect) {                                  // 35
    Meteor.call("testInvalidateLogins", true, expect(function (error) {                 // 36
      test.isFalse(error);                                                              // 37
    }));                                                                                // 38
  };                                                                                    // 39
  var validateLoginsStep = function (test, expect) {                                    // 40
    Meteor.call("testInvalidateLogins", false, expect(function (error) {                // 41
      test.isFalse(error);                                                              // 42
    }));                                                                                // 43
  };                                                                                    // 44
                                                                                        // 45
  testAsyncMulti("passwords - basic login with password", [                             // 46
    function (test, expect) {                                                           // 47
      // setup                                                                          // 48
      this.username = Random.id();                                                      // 49
      this.email = Random.id() + '-intercept@example.com';                              // 50
      this.password = 'password';                                                       // 51
                                                                                        // 52
      Accounts.createUser(                                                              // 53
        {username: this.username, email: this.email, password: this.password},          // 54
        loggedInAs(this.username, test, expect));                                       // 55
    },                                                                                  // 56
    function (test, expect) {                                                           // 57
      test.notEqual(Meteor.userId(), null);                                             // 58
    },                                                                                  // 59
    logoutStep,                                                                         // 60
    function (test, expect) {                                                           // 61
      Meteor.loginWithPassword(this.username, this.password,                            // 62
                               loggedInAs(this.username, test, expect));                // 63
    },                                                                                  // 64
    logoutStep,                                                                         // 65
    // This next step tests reactive contexts which are reactive on                     // 66
    // Meteor.user().                                                                   // 67
    function (test, expect) {                                                           // 68
      // Set up a reactive context that only refreshes when Meteor.user() is            // 69
      // invalidated.                                                                   // 70
      var loaded = false;                                                               // 71
      var handle = Deps.autorun(function () {                                           // 72
        if (Meteor.user() && Meteor.user().emails)                                      // 73
          loaded = true;                                                                // 74
      });                                                                               // 75
      // At the beginning, we're not logged in.                                         // 76
      test.isFalse(loaded);                                                             // 77
      Meteor.loginWithPassword(this.username, this.password, expect(function (error) {  // 78
        test.equal(error, undefined);                                                   // 79
        test.notEqual(Meteor.userId(), null);                                           // 80
        // By the time of the login callback, the user should be loaded.                // 81
        test.isTrue(Meteor.user().emails);                                              // 82
        // Flushing should get us the rerun as well.                                    // 83
        Deps.flush();                                                                   // 84
        test.isTrue(loaded);                                                            // 85
        handle.stop();                                                                  // 86
      }));                                                                              // 87
    },                                                                                  // 88
    logoutStep,                                                                         // 89
    function (test, expect) {                                                           // 90
      Meteor.loginWithPassword({username: this.username}, this.password,                // 91
                               loggedInAs(this.username, test, expect));                // 92
    },                                                                                  // 93
    logoutStep,                                                                         // 94
    function (test, expect) {                                                           // 95
      Meteor.loginWithPassword(this.email, this.password,                               // 96
                               loggedInAs(this.username, test, expect));                // 97
    },                                                                                  // 98
    logoutStep,                                                                         // 99
    function (test, expect) {                                                           // 100
      Meteor.loginWithPassword({email: this.email}, this.password,                      // 101
                               loggedInAs(this.username, test, expect));                // 102
    },                                                                                  // 103
    logoutStep                                                                          // 104
  ]);                                                                                   // 105
                                                                                        // 106
                                                                                        // 107
  testAsyncMulti("passwords - plain text passwords", [                                  // 108
    function (test, expect) {                                                           // 109
      // setup                                                                          // 110
      this.username = Random.id();                                                      // 111
      this.email = Random.id() + '-intercept@example.com';                              // 112
      this.password = 'password';                                                       // 113
                                                                                        // 114
      // create user with raw password (no API, need to invoke callLoginMethod          // 115
      // directly)                                                                      // 116
      Accounts.callLoginMethod({                                                        // 117
        methodName: 'createUser',                                                       // 118
        methodArguments: [{username: this.username, password: this.password}],          // 119
        userCallback: loggedInAs(this.username, test, expect)                           // 120
      });                                                                               // 121
    },                                                                                  // 122
    logoutStep,                                                                         // 123
    // check can login normally with this password.                                     // 124
    function(test, expect) {                                                            // 125
      Meteor.loginWithPassword({username: this.username}, this.password,                // 126
                               loggedInAs(this.username, test, expect));                // 127
    },                                                                                  // 128
    logoutStep,                                                                         // 129
    // plain text password. no API for this, have to invoke callLoginMethod             // 130
    // directly.                                                                        // 131
    function (test, expect) {                                                           // 132
      Accounts.callLoginMethod({                                                        // 133
        // wrong password                                                               // 134
        methodArguments: [{user: {username: this.username}, password: 'wrong'}],        // 135
        userCallback: expect(function (error) {                                         // 136
          test.isTrue(error);                                                           // 137
          test.isFalse(Meteor.user());                                                  // 138
        })});                                                                           // 139
    },                                                                                  // 140
    function (test, expect) {                                                           // 141
      Accounts.callLoginMethod({                                                        // 142
        // right password                                                               // 143
        methodArguments: [{user: {username: this.username},                             // 144
                           password: this.password}],                                   // 145
        userCallback: loggedInAs(this.username, test, expect)                           // 146
      });                                                                               // 147
    },                                                                                  // 148
    logoutStep                                                                          // 149
  ]);                                                                                   // 150
                                                                                        // 151
                                                                                        // 152
  testAsyncMulti("passwords - changing passwords", [                                    // 153
    function (test, expect) {                                                           // 154
      // setup                                                                          // 155
      this.username = Random.id();                                                      // 156
      this.email = Random.id() + '-intercept@example.com';                              // 157
      this.password = 'password';                                                       // 158
      this.password2 = 'password2';                                                     // 159
                                                                                        // 160
      Accounts.createUser(                                                              // 161
        {username: this.username, email: this.email, password: this.password},          // 162
        loggedInAs(this.username, test, expect));                                       // 163
    },                                                                                  // 164
    // change password with bad old password. we stay logged in.                        // 165
    function (test, expect) {                                                           // 166
      var self = this;                                                                  // 167
      Accounts.changePassword('wrong', 'doesntmatter', expect(function (error) {        // 168
        test.isTrue(error);                                                             // 169
        test.equal(Meteor.user().username, self.username);                              // 170
      }));                                                                              // 171
    },                                                                                  // 172
    // change password with good old password.                                          // 173
    function (test, expect) {                                                           // 174
      Accounts.changePassword(this.password, this.password2,                            // 175
                              loggedInAs(this.username, test, expect));                 // 176
    },                                                                                  // 177
    logoutStep,                                                                         // 178
    // old password, failed login                                                       // 179
    function (test, expect) {                                                           // 180
      Meteor.loginWithPassword(this.email, this.password, expect(function (error) {     // 181
        test.isTrue(error);                                                             // 182
        test.isFalse(Meteor.user());                                                    // 183
      }));                                                                              // 184
    },                                                                                  // 185
    // new password, success                                                            // 186
    function (test, expect) {                                                           // 187
      Meteor.loginWithPassword(this.email, this.password2,                              // 188
                               loggedInAs(this.username, test, expect));                // 189
    },                                                                                  // 190
    logoutStep                                                                          // 191
  ]);                                                                                   // 192
                                                                                        // 193
                                                                                        // 194
  testAsyncMulti("passwords - new user hooks", [                                        // 195
    function (test, expect) {                                                           // 196
      // setup                                                                          // 197
      this.username = Random.id();                                                      // 198
      this.email = Random.id() + '-intercept@example.com';                              // 199
      this.password = 'password';                                                       // 200
    },                                                                                  // 201
    // test Accounts.validateNewUser                                                    // 202
    function(test, expect) {                                                            // 203
      Accounts.createUser(                                                              // 204
        {username: this.username, password: this.password,                              // 205
         // should fail the new user validators                                         // 206
         profile: {invalid: true}},                                                     // 207
        expect(function (error) {                                                       // 208
          test.equal(error.error, 403);                                                 // 209
          test.equal(error.reason, "User validation failed");                           // 210
        }));                                                                            // 211
    },                                                                                  // 212
    logoutStep,                                                                         // 213
    function(test, expect) {                                                            // 214
      Accounts.createUser(                                                              // 215
        {username: this.username, password: this.password,                              // 216
         // should fail the new user validator with a special                           // 217
         // exception                                                                   // 218
         profile: {invalidAndThrowException: true}},                                    // 219
        expect(function (error) {                                                       // 220
          test.equal(                                                                   // 221
            error.reason,                                                               // 222
            "An exception thrown within Accounts.validateNewUser");                     // 223
        }));                                                                            // 224
    },                                                                                  // 225
    // test Accounts.onCreateUser                                                       // 226
    function(test, expect) {                                                            // 227
      Accounts.createUser(                                                              // 228
        {username: this.username, password: this.password,                              // 229
         testOnCreateUserHook: true},                                                   // 230
        loggedInAs(this.username, test, expect));                                       // 231
    },                                                                                  // 232
    function(test, expect) {                                                            // 233
      test.equal(Meteor.user().profile.touchedByOnCreateUser, true);                    // 234
    },                                                                                  // 235
    logoutStep                                                                          // 236
  ]);                                                                                   // 237
                                                                                        // 238
                                                                                        // 239
  testAsyncMulti("passwords - Meteor.user()", [                                         // 240
    function (test, expect) {                                                           // 241
      // setup                                                                          // 242
      this.username = Random.id();                                                      // 243
      this.password = 'password';                                                       // 244
                                                                                        // 245
      Accounts.createUser(                                                              // 246
        {username: this.username, password: this.password,                              // 247
         testOnCreateUserHook: true},                                                   // 248
        loggedInAs(this.username, test, expect));                                       // 249
    },                                                                                  // 250
    // test Meteor.user(). This test properly belongs in                                // 251
    // accounts-base/accounts_tests.js, but this is where the tests that                // 252
    // actually log in are.                                                             // 253
    function(test, expect) {                                                            // 254
      var self = this;                                                                  // 255
      var clientUser = Meteor.user();                                                   // 256
      Accounts.connection.call('testMeteorUser', expect(function (err, result) {        // 257
        test.equal(result._id, clientUser._id);                                         // 258
        test.equal(result.username, clientUser.username);                               // 259
        test.equal(result.username, self.username);                                     // 260
        test.equal(result.profile.touchedByOnCreateUser, true);                         // 261
        test.equal(err, undefined);                                                     // 262
      }));                                                                              // 263
    },                                                                                  // 264
    function(test, expect) {                                                            // 265
      // Test that even with no published fields, we still have a document.             // 266
      Accounts.connection.call('clearUsernameAndProfile', expect(function() {           // 267
        test.isTrue(Meteor.userId());                                                   // 268
        var user = Meteor.user();                                                       // 269
        test.equal(user, {_id: Meteor.userId()});                                       // 270
      }));                                                                              // 271
    },                                                                                  // 272
    logoutStep,                                                                         // 273
    function(test, expect) {                                                            // 274
      var clientUser = Meteor.user();                                                   // 275
      test.equal(clientUser, null);                                                     // 276
      test.equal(Meteor.userId(), null);                                                // 277
      Accounts.connection.call('testMeteorUser', expect(function (err, result) {        // 278
        test.equal(err, undefined);                                                     // 279
        test.equal(result, null);                                                       // 280
      }));                                                                              // 281
    }                                                                                   // 282
  ]);                                                                                   // 283
                                                                                        // 284
  testAsyncMulti("passwords - allow rules", [                                           // 285
    // create a second user to have an id for in a later test                           // 286
    function (test, expect) {                                                           // 287
      this.otherUsername = Random.id();                                                 // 288
      Accounts.createUser(                                                              // 289
        {username: this.otherUsername, password: 'dontcare',                            // 290
         testOnCreateUserHook: true},                                                   // 291
        loggedInAs(this.otherUsername, test, expect));                                  // 292
    },                                                                                  // 293
    function (test, expect) {                                                           // 294
      this.otherUserId = Meteor.userId();                                               // 295
    },                                                                                  // 296
    function (test, expect) {                                                           // 297
      // real setup                                                                     // 298
      this.username = Random.id();                                                      // 299
      this.password = 'password';                                                       // 300
                                                                                        // 301
      Accounts.createUser(                                                              // 302
        {username: this.username, password: this.password,                              // 303
         testOnCreateUserHook: true},                                                   // 304
        loggedInAs(this.username, test, expect));                                       // 305
    },                                                                                  // 306
    // test the default Meteor.users allow rule. This test properly belongs in          // 307
    // accounts-base/accounts_tests.js, but this is where the tests that                // 308
    // actually log in are.                                                             // 309
    function(test, expect) {                                                            // 310
      this.userId = Meteor.userId();                                                    // 311
      test.notEqual(this.userId, null);                                                 // 312
      test.notEqual(this.userId, this.otherUserId);                                     // 313
      // Can't update fields other than profile.                                        // 314
      Meteor.users.update(                                                              // 315
        this.userId, {$set: {disallowed: true, 'profile.updated': 42}},                 // 316
        expect(function (err) {                                                         // 317
          test.isTrue(err);                                                             // 318
          test.equal(err.error, 403);                                                   // 319
          test.isFalse(_.has(Meteor.user(), 'disallowed'));                             // 320
          test.isFalse(_.has(Meteor.user().profile, 'updated'));                        // 321
        }));                                                                            // 322
    },                                                                                  // 323
    function(test, expect) {                                                            // 324
      // Can't update another user.                                                     // 325
      Meteor.users.update(                                                              // 326
        this.otherUserId, {$set: {'profile.updated': 42}},                              // 327
        expect(function (err) {                                                         // 328
          test.isTrue(err);                                                             // 329
          test.equal(err.error, 403);                                                   // 330
        }));                                                                            // 331
    },                                                                                  // 332
    function(test, expect) {                                                            // 333
      // Can't update using a non-ID selector. (This one is thrown client-side.)        // 334
      test.throws(function () {                                                         // 335
        Meteor.users.update(                                                            // 336
          {username: this.username}, {$set: {'profile.updated': 42}});                  // 337
      });                                                                               // 338
      test.isFalse(_.has(Meteor.user().profile, 'updated'));                            // 339
    },                                                                                  // 340
    function(test, expect) {                                                            // 341
      // Can update own profile using ID.                                               // 342
      Meteor.users.update(                                                              // 343
        this.userId, {$set: {'profile.updated': 42}},                                   // 344
        expect(function (err) {                                                         // 345
          test.isFalse(err);                                                            // 346
          test.equal(42, Meteor.user().profile.updated);                                // 347
        }));                                                                            // 348
    },                                                                                  // 349
    logoutStep                                                                          // 350
  ]);                                                                                   // 351
                                                                                        // 352
                                                                                        // 353
  testAsyncMulti("passwords - tokens", [                                                // 354
    function (test, expect) {                                                           // 355
      // setup                                                                          // 356
      this.username = Random.id();                                                      // 357
      this.password = 'password';                                                       // 358
                                                                                        // 359
      Accounts.createUser(                                                              // 360
        {username: this.username, password: this.password},                             // 361
        loggedInAs(this.username, test, expect));                                       // 362
    },                                                                                  // 363
                                                                                        // 364
    function (test, expect) {                                                           // 365
      // we can't login with an invalid token                                           // 366
      var expectLoginError = expect(function (err) {                                    // 367
        test.isTrue(err);                                                               // 368
      });                                                                               // 369
      Meteor.loginWithToken('invalid', expectLoginError);                               // 370
    },                                                                                  // 371
                                                                                        // 372
    function (test, expect) {                                                           // 373
      // we can login with a valid token                                                // 374
      var expectLoginOK = expect(function (err) {                                       // 375
        test.isFalse(err);                                                              // 376
      });                                                                               // 377
      Meteor.loginWithToken(Accounts._storedLoginToken(), expectLoginOK);               // 378
    },                                                                                  // 379
                                                                                        // 380
    function (test, expect) {                                                           // 381
      // test logging out invalidates our token                                         // 382
      var expectLoginError = expect(function (err) {                                    // 383
        test.isTrue(err);                                                               // 384
      });                                                                               // 385
      var token = Accounts._storedLoginToken();                                         // 386
      test.isTrue(token);                                                               // 387
      Meteor.logout(function () {                                                       // 388
        Meteor.loginWithToken(token, expectLoginError);                                 // 389
      });                                                                               // 390
    },                                                                                  // 391
                                                                                        // 392
    function (test, expect) {                                                           // 393
      var self = this;                                                                  // 394
      // Test that login tokens get expired. We should get logged out when a            // 395
      // token expires, and not be able to log in again with the same token.            // 396
      var expectNoError = expect(function (err) {                                       // 397
        test.isFalse(err);                                                              // 398
      });                                                                               // 399
                                                                                        // 400
      Meteor.loginWithPassword(this.username, this.password, function (error) {         // 401
        self.token = Accounts._storedLoginToken();                                      // 402
        test.isTrue(self.token);                                                        // 403
        expectNoError(error);                                                           // 404
        Accounts.connection.call("expireTokens");                                       // 405
      });                                                                               // 406
    },                                                                                  // 407
    waitForLoggedOutStep,                                                               // 408
    function (test, expect) {                                                           // 409
      var token = Accounts._storedLoginToken();                                         // 410
      test.isFalse(token);                                                              // 411
    },                                                                                  // 412
    function (test, expect) {                                                           // 413
      // Test that once expireTokens is finished, we can't login again with our         // 414
      // previous token.                                                                // 415
      Meteor.loginWithToken(this.token, expect(function (err, result) {                 // 416
        test.isTrue(err);                                                               // 417
        test.equal(Meteor.userId(), null);                                              // 418
      }));                                                                              // 419
    },                                                                                  // 420
    function (test, expect) {                                                           // 421
      var self = this;                                                                  // 422
                                                                                        // 423
      // copied from livedata/client_convenience.js                                     // 424
      self.ddpUrl = '/';                                                                // 425
      if (typeof __meteor_runtime_config__ !== "undefined") {                           // 426
        if (__meteor_runtime_config__.DDP_DEFAULT_CONNECTION_URL)                       // 427
          self.ddpUrl = __meteor_runtime_config__.DDP_DEFAULT_CONNECTION_URL;           // 428
      }                                                                                 // 429
      // XXX can we get the url from the existing connection somehow                    // 430
      // instead?                                                                       // 431
                                                                                        // 432
      // Test that Meteor.logoutOtherClients logs out a second authenticated            // 433
      // connection while leaving Accounts.connection logged in.                        // 434
      var token;                                                                        // 435
      var userId;                                                                       // 436
      self.secondConn = DDP.connect(self.ddpUrl);                                       // 437
                                                                                        // 438
      var expectLoginError = expect(function (err) {                                    // 439
        test.isTrue(err);                                                               // 440
      });                                                                               // 441
      var expectValidToken = expect(function (err, result) {                            // 442
        test.isFalse(err);                                                              // 443
        test.isTrue(result);                                                            // 444
        self.tokenFromLogoutOthers = result.token;                                      // 445
      });                                                                               // 446
      var expectSecondConnLoggedIn = expect(function (err, result) {                    // 447
        test.equal(result.token, token);                                                // 448
        test.isFalse(err);                                                              // 449
        // This test will fail if an unrelated reconnect triggers before the            // 450
        // connection is logged out. In general our tests aren't resilient to           // 451
        // mid-test reconnects.                                                         // 452
        self.secondConn.onReconnect = function () {                                     // 453
          self.secondConn.call("login", { resume: token }, expectLoginError);           // 454
        };                                                                              // 455
        Accounts.connection.call("logoutOtherClients", expectValidToken);               // 456
      });                                                                               // 457
                                                                                        // 458
      Meteor.loginWithPassword(this.username, this.password, expect(function (err) {    // 459
        test.isFalse(err);                                                              // 460
        token = Accounts._storedLoginToken();                                           // 461
        self.beforeLogoutOthersToken = token;                                           // 462
        test.isTrue(token);                                                             // 463
        userId = Meteor.userId();                                                       // 464
        self.secondConn.call("login", { resume: token },                                // 465
                             expectSecondConnLoggedIn);                                 // 466
      }));                                                                              // 467
    },                                                                                  // 468
    // Test that logoutOtherClients logged out Accounts.connection and that the         // 469
    // previous token is no longer valid.                                               // 470
    waitForLoggedOutStep,                                                               // 471
    function (test, expect) {                                                           // 472
      var self = this;                                                                  // 473
      var token = Accounts._storedLoginToken();                                         // 474
      test.isFalse(token);                                                              // 475
      this.secondConn.close();                                                          // 476
      Meteor.loginWithToken(                                                            // 477
        self.beforeLogoutOthersToken,                                                   // 478
        expect(function (err) {                                                         // 479
          test.isTrue(err);                                                             // 480
          test.isFalse(Meteor.userId());                                                // 481
        })                                                                              // 482
      );                                                                                // 483
    },                                                                                  // 484
    // Test that logoutOtherClients returned a new token that we can use to             // 485
    // log in.                                                                          // 486
    function (test, expect) {                                                           // 487
      var self = this;                                                                  // 488
      Meteor.loginWithToken(                                                            // 489
        self.tokenFromLogoutOthers,                                                     // 490
        expect(function (err) {                                                         // 491
          test.isFalse(err);                                                            // 492
          test.isTrue(Meteor.userId());                                                 // 493
        })                                                                              // 494
      );                                                                                // 495
    },                                                                                  // 496
    logoutStep,                                                                         // 497
    function (test, expect) {                                                           // 498
      var self = this;                                                                  // 499
      // Test that, when we call logoutOtherClients, if the server disconnects          // 500
      // us before the logoutOtherClients callback runs, then we still end up           // 501
      // logged in.                                                                     // 502
      var expectServerLoggedIn = expect(function (err, result) {                        // 503
        test.isFalse(err);                                                              // 504
        test.isTrue(Meteor.userId());                                                   // 505
        test.equal(result, Meteor.userId());                                            // 506
      });                                                                               // 507
                                                                                        // 508
      Meteor.loginWithPassword(                                                         // 509
        self.username,                                                                  // 510
        self.password,                                                                  // 511
        expect(function (err) {                                                         // 512
          test.isFalse(err);                                                            // 513
          test.isTrue(Meteor.userId());                                                 // 514
                                                                                        // 515
          // The test is only useful if things interleave in the following order:       // 516
          // - logoutOtherClients runs on the server                                    // 517
          // - onReconnect fires and sends a login method with the old token,           // 518
          //   which results in an error                                                // 519
          // - logoutOtherClients callback runs and stores the new token and            // 520
          //   logs in with it                                                          // 521
          // In practice they seem to interleave this way, but I'm not sure how         // 522
          // to make sure that they do.                                                 // 523
                                                                                        // 524
          Meteor.logoutOtherClients(function (err) {                                    // 525
            test.isFalse(err);                                                          // 526
            Meteor.call("getUserId", expectServerLoggedIn);                             // 527
          });                                                                           // 528
        })                                                                              // 529
      );                                                                                // 530
    },                                                                                  // 531
    logoutStep,                                                                         // 532
    function (test, expect) {                                                           // 533
      var self = this;                                                                  // 534
      // Test that deleting a user logs out that user's connections.                    // 535
      Meteor.loginWithPassword(this.username, this.password, expect(function (err) {    // 536
        test.isFalse(err);                                                              // 537
        Accounts.connection.call("removeUser", self.username);                          // 538
      }));                                                                              // 539
    },                                                                                  // 540
    waitForLoggedOutStep                                                                // 541
  ]);                                                                                   // 542
                                                                                        // 543
  testAsyncMulti("passwords - validateLoginAttempt", [                                  // 544
    function (test, expect) {                                                           // 545
      this.username = Random.id();                                                      // 546
      this.password = "password";                                                       // 547
                                                                                        // 548
      Accounts.createUser(                                                              // 549
        {username: this.username, password: this.password},                             // 550
        loggedInAs(this.username, test, expect));                                       // 551
    },                                                                                  // 552
    logoutStep,                                                                         // 553
    invalidateLoginsStep,                                                               // 554
    function (test, expect) {                                                           // 555
      Meteor.loginWithPassword(                                                         // 556
        this.username,                                                                  // 557
        this.password,                                                                  // 558
        expect(function (error) {                                                       // 559
          test.isTrue(error);                                                           // 560
          test.equal(error.reason, "Login forbidden");                                  // 561
        })                                                                              // 562
      );                                                                                // 563
    },                                                                                  // 564
    validateLoginsStep                                                                  // 565
  ]);                                                                                   // 566
                                                                                        // 567
  testAsyncMulti("passwords - onLogin hook", [                                          // 568
    function (test, expect) {                                                           // 569
      Meteor.call("testCaptureLogins", expect(function (error) {                        // 570
        test.isFalse(error);                                                            // 571
      }));                                                                              // 572
    },                                                                                  // 573
    function (test, expect) {                                                           // 574
      this.username = Random.id();                                                      // 575
      this.password = "password";                                                       // 576
                                                                                        // 577
      Accounts.createUser(                                                              // 578
        {username: this.username, password: this.password},                             // 579
        loggedInAs(this.username, test, expect));                                       // 580
    },                                                                                  // 581
    function (test, expect) {                                                           // 582
      var self = this;                                                                  // 583
      Meteor.call("testFetchCapturedLogins", expect(function (error, logins) {          // 584
        test.isFalse(error);                                                            // 585
        test.equal(logins.length, 1);                                                   // 586
        var login = logins[0];                                                          // 587
        test.isTrue(login.successful);                                                  // 588
        var attempt = login.attempt;                                                    // 589
        test.equal(attempt.type, "password");                                           // 590
        test.isTrue(attempt.allowed);                                                   // 591
        test.equal(attempt.methodName, "createUser");                                   // 592
        test.equal(attempt.methodArguments[0].username, self.username);                 // 593
      }));                                                                              // 594
    }                                                                                   // 595
  ]);                                                                                   // 596
                                                                                        // 597
  testAsyncMulti("passwords - onLoginFailed hook", [                                    // 598
    function (test, expect) {                                                           // 599
      this.username = Random.id();                                                      // 600
      this.password = "password";                                                       // 601
                                                                                        // 602
      Accounts.createUser(                                                              // 603
        {username: this.username, password: this.password},                             // 604
        loggedInAs(this.username, test, expect));                                       // 605
    },                                                                                  // 606
    logoutStep,                                                                         // 607
    function (test, expect) {                                                           // 608
      Meteor.call("testCaptureLogins", expect(function (error) {                        // 609
        test.isFalse(error);                                                            // 610
      }));                                                                              // 611
    },                                                                                  // 612
    function (test, expect) {                                                           // 613
      Meteor.loginWithPassword(this.username, "incorrect", expect(function (error) {    // 614
        test.isTrue(error);                                                             // 615
      }));                                                                              // 616
    },                                                                                  // 617
    function (test, expect) {                                                           // 618
      Meteor.call("testFetchCapturedLogins", expect(function (error, logins) {          // 619
        test.isFalse(error);                                                            // 620
        test.equal(logins.length, 1);                                                   // 621
        var login = logins[0];                                                          // 622
        test.isFalse(login.successful);                                                 // 623
        var attempt = login.attempt;                                                    // 624
        test.equal(attempt.type, "password");                                           // 625
        test.isFalse(attempt.allowed);                                                  // 626
        test.equal(attempt.error.reason, "Incorrect password");                         // 627
      }));                                                                              // 628
    },                                                                                  // 629
    function (test, expect) {                                                           // 630
      Meteor.call("testCaptureLogins", expect(function (error) {                        // 631
        test.isFalse(error);                                                            // 632
      }));                                                                              // 633
    },                                                                                  // 634
    function (test, expect) {                                                           // 635
      Meteor.loginWithPassword("no such user", "incorrect", expect(function (error) {   // 636
        test.isTrue(error);                                                             // 637
      }));                                                                              // 638
    },                                                                                  // 639
    function (test, expect) {                                                           // 640
      Meteor.call("testFetchCapturedLogins", expect(function (error, logins) {          // 641
        test.isFalse(error);                                                            // 642
        test.equal(logins.length, 1);                                                   // 643
        var login = logins[0];                                                          // 644
        test.isFalse(login.successful);                                                 // 645
        var attempt = login.attempt;                                                    // 646
        test.equal(attempt.type, "password");                                           // 647
        test.isFalse(attempt.allowed);                                                  // 648
        test.equal(attempt.error.reason, "User not found");                             // 649
      }));                                                                              // 650
    }                                                                                   // 651
  ]);                                                                                   // 652
                                                                                        // 653
}) ();                                                                                  // 654
                                                                                        // 655
                                                                                        // 656
if (Meteor.isServer) (function () {                                                     // 657
                                                                                        // 658
  Tinytest.add(                                                                         // 659
    'passwords - setup more than one onCreateUserHook',                                 // 660
    function (test) {                                                                   // 661
      test.throws(function() {                                                          // 662
        Accounts.onCreateUser(function () {});                                          // 663
      });                                                                               // 664
    });                                                                                 // 665
                                                                                        // 666
                                                                                        // 667
  Tinytest.add(                                                                         // 668
    'passwords - createUser hooks',                                                     // 669
    function (test) {                                                                   // 670
      var username = Random.id();                                                       // 671
      test.throws(function () {                                                         // 672
        // should fail the new user validators                                          // 673
        Accounts.createUser({username: username, profile: {invalid: true}});            // 674
      });                                                                               // 675
                                                                                        // 676
      var userId = Accounts.createUser({username: username,                             // 677
                                        testOnCreateUserHook: true});                   // 678
                                                                                        // 679
      test.isTrue(userId);                                                              // 680
      var user = Meteor.users.findOne(userId);                                          // 681
      test.equal(user.profile.touchedByOnCreateUser, true);                             // 682
    });                                                                                 // 683
                                                                                        // 684
                                                                                        // 685
  Tinytest.add(                                                                         // 686
    'passwords - setPassword',                                                          // 687
    function (test) {                                                                   // 688
      var username = Random.id();                                                       // 689
                                                                                        // 690
      var userId = Accounts.createUser({username: username});                           // 691
                                                                                        // 692
      var user = Meteor.users.findOne(userId);                                          // 693
      // no services yet.                                                               // 694
      test.equal(user.services.password, undefined);                                    // 695
                                                                                        // 696
      // set a new password.                                                            // 697
      Accounts.setPassword(userId, 'new password');                                     // 698
      user = Meteor.users.findOne(userId);                                              // 699
      var oldVerifier = user.services.password.srp;                                     // 700
      test.isTrue(user.services.password.srp);                                          // 701
                                                                                        // 702
      // reset with the same password, see we get a different verifier                  // 703
      Accounts.setPassword(userId, 'new password');                                     // 704
      user = Meteor.users.findOne(userId);                                              // 705
      var newVerifier = user.services.password.srp;                                     // 706
      test.notEqual(oldVerifier.salt, newVerifier.salt);                                // 707
      test.notEqual(oldVerifier.identity, newVerifier.identity);                        // 708
      test.notEqual(oldVerifier.verifier, newVerifier.verifier);                        // 709
                                                                                        // 710
      // cleanup                                                                        // 711
      Meteor.users.remove(userId);                                                      // 712
    });                                                                                 // 713
                                                                                        // 714
                                                                                        // 715
  // This test properly belongs in accounts-base/accounts_tests.js, but                 // 716
  // this is where the tests that actually log in are.                                  // 717
  Tinytest.add('accounts - user() out of context', function (test) {                    // 718
    // basic server context, no method.                                                 // 719
    test.throws(function () {                                                           // 720
      Meteor.user();                                                                    // 721
    });                                                                                 // 722
  });                                                                                   // 723
                                                                                        // 724
  // XXX would be nice to test Accounts.config({forbidClientAccountCreation: true})     // 725
                                                                                        // 726
  Tinytest.addAsync(                                                                    // 727
    'passwords - login tokens cleaned up',                                              // 728
    function (test, onComplete) {                                                       // 729
      var username = Random.id();                                                       // 730
      Accounts.createUser({                                                             // 731
        username: username,                                                             // 732
        password: 'password'                                                            // 733
      });                                                                               // 734
                                                                                        // 735
      makeTestConnection(                                                               // 736
        test,                                                                           // 737
        function (clientConn, serverConn) {                                             // 738
          serverConn.onClose(function () {                                              // 739
            test.isFalse(_.contains(                                                    // 740
              Accounts._getTokenConnections(token), serverConn.id));                    // 741
            onComplete();                                                               // 742
          });                                                                           // 743
          var result = clientConn.call('login', {                                       // 744
            user: {username: username},                                                 // 745
            password: 'password'                                                        // 746
          });                                                                           // 747
          test.isTrue(result);                                                          // 748
          var token = Accounts._getAccountData(serverConn.id, 'loginToken');            // 749
          test.isTrue(token);                                                           // 750
          test.isTrue(_.contains(                                                       // 751
            Accounts._getTokenConnections(token), serverConn.id));                      // 752
          clientConn.disconnect();                                                      // 753
        },                                                                              // 754
        onComplete                                                                      // 755
      );                                                                                // 756
    }                                                                                   // 757
  );                                                                                    // 758
}) ();                                                                                  // 759
                                                                                        // 760
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/accounts-password/email_tests_setup.js                                      //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
//                                                                                      // 1
// a mechanism to intercept emails sent to addressing including                         // 2
// the string "intercept", storing them in an array that can then                       // 3
// be retrieved using the getInterceptedEmails method                                   // 4
//                                                                                      // 5
var interceptedEmails = {}; // (email address) -> (array of options)                    // 6
                                                                                        // 7
// add html email templates that just contain the url                                   // 8
Accounts.emailTemplates.resetPassword.html =                                            // 9
  Accounts.emailTemplates.enrollAccount.html =                                          // 10
  Accounts.emailTemplates.verifyEmail.html = function (user, url) {                     // 11
    return url;                                                                         // 12
  };                                                                                    // 13
                                                                                        // 14
EmailTest.hookSend(function (options) {                                                 // 15
  var to = options.to;                                                                  // 16
  if (to.indexOf('intercept') === -1) {                                                 // 17
    return true; // go ahead and send                                                   // 18
  } else {                                                                              // 19
    if (!interceptedEmails[to])                                                         // 20
      interceptedEmails[to] = [];                                                       // 21
                                                                                        // 22
    interceptedEmails[to].push(options);                                                // 23
    return false; // skip sending                                                       // 24
  }                                                                                     // 25
});                                                                                     // 26
                                                                                        // 27
Meteor.methods({                                                                        // 28
  getInterceptedEmails: function (email) {                                              // 29
    check(email, String);                                                               // 30
    return interceptedEmails[email];                                                    // 31
  },                                                                                    // 32
                                                                                        // 33
  addEmailForTestAndVerify: function (email) {                                          // 34
    check(email, String);                                                               // 35
    Meteor.users.update(                                                                // 36
      {_id: this.userId},                                                               // 37
      {$push: {emails: {address: email, verified: false}}});                            // 38
    Accounts.sendVerificationEmail(this.userId, email);                                 // 39
  },                                                                                    // 40
                                                                                        // 41
  createUserOnServer: function (email) {                                                // 42
    check(email, String);                                                               // 43
    var userId = Accounts.createUser({email: email});                                   // 44
    Accounts.sendEnrollmentEmail(userId);                                               // 45
    return Meteor.users.findOne(userId);                                                // 46
  }                                                                                     // 47
});                                                                                     // 48
                                                                                        // 49
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
