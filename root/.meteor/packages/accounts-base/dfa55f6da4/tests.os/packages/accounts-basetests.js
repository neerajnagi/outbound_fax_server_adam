(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                     //
// packages/accounts-base/accounts_tests.js                                                            //
//                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                       //
// XXX it'd be cool to also test that the right thing happens if options                               // 1
// *are* validated, but Accounts._options is global state which makes this hard                        // 2
// (impossible?)                                                                                       // 3
Tinytest.add('accounts - config validates keys', function (test) {                                     // 4
  test.throws(function () {                                                                            // 5
    Accounts.config({foo: "bar"});                                                                     // 6
  });                                                                                                  // 7
});                                                                                                    // 8
                                                                                                       // 9
                                                                                                       // 10
var idsInValidateNewUser = {};                                                                         // 11
Accounts.validateNewUser(function (user) {                                                             // 12
  idsInValidateNewUser[user._id] = true;                                                               // 13
  return true;                                                                                         // 14
});                                                                                                    // 15
                                                                                                       // 16
Tinytest.add('accounts - validateNewUser gets passed user with _id', function (test) {                 // 17
  var newUserId = Accounts.updateOrCreateUserFromExternalService('foobook', {id: Random.id()}).userId; // 18
  test.isTrue(newUserId in idsInValidateNewUser);                                                      // 19
});                                                                                                    // 20
                                                                                                       // 21
Tinytest.add('accounts - updateOrCreateUserFromExternalService - Facebook', function (test) {          // 22
  var facebookId = Random.id();                                                                        // 23
                                                                                                       // 24
  // create an account with facebook                                                                   // 25
  var uid1 = Accounts.updateOrCreateUserFromExternalService(                                           // 26
    'facebook', {id: facebookId, monkey: 42}, {profile: {foo: 1}}).id;                                 // 27
  var users = Meteor.users.find({"services.facebook.id": facebookId}).fetch();                         // 28
  test.length(users, 1);                                                                               // 29
  test.equal(users[0].profile.foo, 1);                                                                 // 30
  test.equal(users[0].services.facebook.monkey, 42);                                                   // 31
                                                                                                       // 32
  // create again with the same id, see that we get the same user.                                     // 33
  // it should update services.facebook but not profile.                                               // 34
  var uid2 = Accounts.updateOrCreateUserFromExternalService(                                           // 35
    'facebook', {id: facebookId, llama: 50},                                                           // 36
    {profile: {foo: 1000, bar: 2}}).id;                                                                // 37
  test.equal(uid1, uid2);                                                                              // 38
  users = Meteor.users.find({"services.facebook.id": facebookId}).fetch();                             // 39
  test.length(users, 1);                                                                               // 40
  test.equal(users[0].profile.foo, 1);                                                                 // 41
  test.equal(users[0].profile.bar, undefined);                                                         // 42
  test.equal(users[0].services.facebook.llama, 50);                                                    // 43
  // make sure we *don't* lose values not passed this call to                                          // 44
  // updateOrCreateUserFromExternalService                                                             // 45
  test.equal(users[0].services.facebook.monkey, 42);                                                   // 46
                                                                                                       // 47
  // cleanup                                                                                           // 48
  Meteor.users.remove(uid1);                                                                           // 49
});                                                                                                    // 50
                                                                                                       // 51
Tinytest.add('accounts - updateOrCreateUserFromExternalService - Weibo', function (test) {             // 52
  var weiboId1 = Random.id();                                                                          // 53
  var weiboId2 = Random.id();                                                                          // 54
                                                                                                       // 55
  // users that have different service ids get different users                                         // 56
  var uid1 = Accounts.updateOrCreateUserFromExternalService(                                           // 57
    'weibo', {id: weiboId1}, {profile: {foo: 1}}).id;                                                  // 58
  var uid2 = Accounts.updateOrCreateUserFromExternalService(                                           // 59
    'weibo', {id: weiboId2}, {profile: {bar: 2}}).id;                                                  // 60
  test.equal(Meteor.users.find({"services.weibo.id": {$in: [weiboId1, weiboId2]}}).count(), 2);        // 61
  test.equal(Meteor.users.findOne({"services.weibo.id": weiboId1}).profile.foo, 1);                    // 62
  test.equal(Meteor.users.findOne({"services.weibo.id": weiboId1}).emails, undefined);                 // 63
  test.equal(Meteor.users.findOne({"services.weibo.id": weiboId2}).profile.bar, 2);                    // 64
  test.equal(Meteor.users.findOne({"services.weibo.id": weiboId2}).emails, undefined);                 // 65
                                                                                                       // 66
  // cleanup                                                                                           // 67
  Meteor.users.remove(uid1);                                                                           // 68
  Meteor.users.remove(uid2);                                                                           // 69
});                                                                                                    // 70
                                                                                                       // 71
Tinytest.add('accounts - updateOrCreateUserFromExternalService - Twitter', function (test) {           // 72
  var twitterIdOld = parseInt(Random.hexString(4), 16);                                                // 73
  var twitterIdNew = ''+twitterIdOld;                                                                  // 74
                                                                                                       // 75
  // create an account with twitter using the old ID format of integer                                 // 76
  var uid1 = Accounts.updateOrCreateUserFromExternalService(                                           // 77
    'twitter', {id: twitterIdOld, monkey: 42}, {profile: {foo: 1}}).id;                                // 78
  var users = Meteor.users.find({"services.twitter.id": twitterIdOld}).fetch();                        // 79
  test.length(users, 1);                                                                               // 80
  test.equal(users[0].profile.foo, 1);                                                                 // 81
  test.equal(users[0].services.twitter.monkey, 42);                                                    // 82
                                                                                                       // 83
  // Update the account with the new ID format of string                                               // 84
  // test that the existing user is found, and that the ID                                             // 85
  // gets updated to a string value                                                                    // 86
  var uid2 = Accounts.updateOrCreateUserFromExternalService(                                           // 87
    'twitter', {id: twitterIdNew, monkey: 42}, {profile: {foo: 1}}).id;                                // 88
  test.equal(uid1, uid2);                                                                              // 89
  users = Meteor.users.find({"services.twitter.id": twitterIdNew}).fetch();                            // 90
  test.length(users, 1);                                                                               // 91
                                                                                                       // 92
  // cleanup                                                                                           // 93
  Meteor.users.remove(uid1);                                                                           // 94
});                                                                                                    // 95
                                                                                                       // 96
                                                                                                       // 97
Tinytest.add('accounts - insertUserDoc username', function (test) {                                    // 98
  var userIn = {                                                                                       // 99
    username: Random.id()                                                                              // 100
  };                                                                                                   // 101
                                                                                                       // 102
  // user does not already exist. create a user object with fields set.                                // 103
  var userId = Accounts.insertUserDoc(                                                                 // 104
    {profile: {name: 'Foo Bar'}},                                                                      // 105
    userIn                                                                                             // 106
  );                                                                                                   // 107
  var userOut = Meteor.users.findOne(userId);                                                          // 108
                                                                                                       // 109
  test.equal(typeof userOut.createdAt, 'object');                                                      // 110
  test.equal(userOut.profile.name, 'Foo Bar');                                                         // 111
  test.equal(userOut.username, userIn.username);                                                       // 112
                                                                                                       // 113
  // run the hook again. now the user exists, so it throws an error.                                   // 114
  test.throws(function () {                                                                            // 115
    Accounts.insertUserDoc(                                                                            // 116
      {profile: {name: 'Foo Bar'}},                                                                    // 117
      userIn                                                                                           // 118
    );                                                                                                 // 119
  });                                                                                                  // 120
                                                                                                       // 121
  // cleanup                                                                                           // 122
  Meteor.users.remove(userId);                                                                         // 123
});                                                                                                    // 124
                                                                                                       // 125
Tinytest.add('accounts - insertUserDoc email', function (test) {                                       // 126
  var email1 = Random.id();                                                                            // 127
  var email2 = Random.id();                                                                            // 128
  var email3 = Random.id();                                                                            // 129
  var userIn = {                                                                                       // 130
    emails: [{address: email1, verified: false},                                                       // 131
             {address: email2, verified: true}]                                                        // 132
  };                                                                                                   // 133
                                                                                                       // 134
  // user does not already exist. create a user object with fields set.                                // 135
  var userId = Accounts.insertUserDoc(                                                                 // 136
    {profile: {name: 'Foo Bar'}},                                                                      // 137
    userIn                                                                                             // 138
  );                                                                                                   // 139
  var userOut = Meteor.users.findOne(userId);                                                          // 140
                                                                                                       // 141
  test.equal(typeof userOut.createdAt, 'object');                                                      // 142
  test.equal(userOut.profile.name, 'Foo Bar');                                                         // 143
  test.equal(userOut.emails, userIn.emails);                                                           // 144
                                                                                                       // 145
  // run the hook again with the exact same emails.                                                    // 146
  // run the hook again. now the user exists, so it throws an error.                                   // 147
  test.throws(function () {                                                                            // 148
    Accounts.insertUserDoc(                                                                            // 149
      {profile: {name: 'Foo Bar'}},                                                                    // 150
      userIn                                                                                           // 151
    );                                                                                                 // 152
  });                                                                                                  // 153
                                                                                                       // 154
  // now with only one of them.                                                                        // 155
  test.throws(function () {                                                                            // 156
    Accounts.insertUserDoc(                                                                            // 157
      {}, {emails: [{address: email1}]}                                                                // 158
    );                                                                                                 // 159
  });                                                                                                  // 160
                                                                                                       // 161
  test.throws(function () {                                                                            // 162
    Accounts.insertUserDoc(                                                                            // 163
      {}, {emails: [{address: email2}]}                                                                // 164
    );                                                                                                 // 165
  });                                                                                                  // 166
                                                                                                       // 167
                                                                                                       // 168
  // a third email works.                                                                              // 169
  var userId3 = Accounts.insertUserDoc(                                                                // 170
      {}, {emails: [{address: email3}]}                                                                // 171
  );                                                                                                   // 172
  var user3 = Meteor.users.findOne(userId3);                                                           // 173
  test.equal(typeof user3.createdAt, 'object');                                                        // 174
                                                                                                       // 175
  // cleanup                                                                                           // 176
  Meteor.users.remove(userId);                                                                         // 177
  Meteor.users.remove(userId3);                                                                        // 178
});                                                                                                    // 179
                                                                                                       // 180
// More token expiration tests are in accounts-password                                                // 181
Tinytest.addAsync('accounts - expire numeric token', function (test, onComplete) {                     // 182
  var userIn = { username: Random.id() };                                                              // 183
  var userId = Accounts.insertUserDoc({ profile: {                                                     // 184
    name: 'Foo Bar'                                                                                    // 185
  } }, userIn);                                                                                        // 186
  var date = new Date(new Date() - 5000);                                                              // 187
  Meteor.users.update(userId, {                                                                        // 188
    $set: {                                                                                            // 189
      "services.resume.loginTokens": [{                                                                // 190
        hashedToken: Random.id(),                                                                      // 191
        when: date                                                                                     // 192
      }, {                                                                                             // 193
        hashedToken: Random.id(),                                                                      // 194
        when: +date                                                                                    // 195
      }]                                                                                               // 196
    }                                                                                                  // 197
  });                                                                                                  // 198
  var observe = Meteor.users.find(userId).observe({                                                    // 199
    changed: function (newUser) {                                                                      // 200
      if (newUser.services && newUser.services.resume &&                                               // 201
          _.isEmpty(newUser.services.resume.loginTokens)) {                                            // 202
        observe.stop();                                                                                // 203
        onComplete();                                                                                  // 204
      }                                                                                                // 205
    }                                                                                                  // 206
  });                                                                                                  // 207
  Accounts._expireTokens(new Date(), userId);                                                          // 208
});                                                                                                    // 209
                                                                                                       // 210
                                                                                                       // 211
// Login tokens used to be stored unhashed in the database.  We want                                   // 212
// to make sure users can still login after upgrading.                                                 // 213
var insertUnhashedLoginToken = function (userId, stampedToken) {                                       // 214
  Meteor.users.update(                                                                                 // 215
    userId,                                                                                            // 216
    {$push: {'services.resume.loginTokens': stampedToken}}                                             // 217
  );                                                                                                   // 218
};                                                                                                     // 219
                                                                                                       // 220
Tinytest.addAsync('accounts - login token', function (test, onComplete) {                              // 221
  // Test that we can login when the database contains a leftover                                      // 222
  // old style unhashed login token.                                                                   // 223
  var userId1 = Accounts.insertUserDoc({}, {username: Random.id()});                                   // 224
  var stampedToken = Accounts._generateStampedLoginToken();                                            // 225
  insertUnhashedLoginToken(userId1, stampedToken);                                                     // 226
  var connection = DDP.connect(Meteor.absoluteUrl());                                                  // 227
  connection.call('login', {resume: stampedToken.token});                                              // 228
  connection.disconnect();                                                                             // 229
                                                                                                       // 230
  // Steal the unhashed token from the database and use it to login.                                   // 231
  // This is a sanity check so that when we *can't* login with a                                       // 232
  // stolen *hashed* token, we know it's not a problem with the test.                                  // 233
  var userId2 = Accounts.insertUserDoc({}, {username: Random.id()});                                   // 234
  insertUnhashedLoginToken(userId2, Accounts._generateStampedLoginToken());                            // 235
  var stolenToken = Meteor.users.findOne(userId2).services.resume.loginTokens[0].token;                // 236
  test.isTrue(stolenToken);                                                                            // 237
  connection = DDP.connect(Meteor.absoluteUrl());                                                      // 238
  connection.call('login', {resume: stolenToken});                                                     // 239
  connection.disconnect();                                                                             // 240
                                                                                                       // 241
  // Now do the same thing, this time with a stolen hashed token.                                      // 242
  var userId3 = Accounts.insertUserDoc({}, {username: Random.id()});                                   // 243
  Accounts._insertLoginToken(userId3, Accounts._generateStampedLoginToken());                          // 244
  stolenToken = Meteor.users.findOne(userId3).services.resume.loginTokens[0].hashedToken;              // 245
  test.isTrue(stolenToken);                                                                            // 246
  connection = DDP.connect(Meteor.absoluteUrl());                                                      // 247
  // evil plan foiled                                                                                  // 248
  test.throws(                                                                                         // 249
    function () {                                                                                      // 250
      connection.call('login', {resume: stolenToken});                                                 // 251
    },                                                                                                 // 252
    /You\'ve been logged out by the server/                                                            // 253
  );                                                                                                   // 254
  connection.disconnect();                                                                             // 255
                                                                                                       // 256
  // Old style unhashed tokens are replaced by hashed tokens when                                      // 257
  // encountered.  This means that after someone logins once, the                                      // 258
  // old unhashed token is no longer available to be stolen.                                           // 259
  var userId4 = Accounts.insertUserDoc({}, {username: Random.id()});                                   // 260
  var stampedToken = Accounts._generateStampedLoginToken();                                            // 261
  insertUnhashedLoginToken(userId4, stampedToken);                                                     // 262
  connection = DDP.connect(Meteor.absoluteUrl());                                                      // 263
  connection.call('login', {resume: stampedToken.token});                                              // 264
  connection.disconnect();                                                                             // 265
                                                                                                       // 266
  // The token is no longer available to be stolen.                                                    // 267
  stolenToken = Meteor.users.findOne(userId4).services.resume.loginTokens[0].token;                    // 268
  test.isFalse(stolenToken);                                                                           // 269
                                                                                                       // 270
  // After the upgrade, the client can still login with their original                                 // 271
  // unhashed login token.                                                                             // 272
  connection = DDP.connect(Meteor.absoluteUrl());                                                      // 273
  connection.call('login', {resume: stampedToken.token});                                              // 274
  connection.disconnect();                                                                             // 275
                                                                                                       // 276
  onComplete();                                                                                        // 277
});                                                                                                    // 278
                                                                                                       // 279
Tinytest.addAsync(                                                                                     // 280
  'accounts - connection data cleaned up',                                                             // 281
  function (test, onComplete) {                                                                        // 282
    makeTestConnection(                                                                                // 283
      test,                                                                                            // 284
      function (clientConn, serverConn) {                                                              // 285
        // onClose callbacks are called in order, so we run after the                                  // 286
        // close callback in accounts.                                                                 // 287
        serverConn.onClose(function () {                                                               // 288
          test.isFalse(Accounts._getAccountData(serverConn.id, 'connection'));                         // 289
          onComplete();                                                                                // 290
        });                                                                                            // 291
                                                                                                       // 292
        test.isTrue(Accounts._getAccountData(serverConn.id, 'connection'));                            // 293
        serverConn.close();                                                                            // 294
      },                                                                                               // 295
      onComplete                                                                                       // 296
    );                                                                                                 // 297
  }                                                                                                    // 298
);                                                                                                     // 299
                                                                                                       // 300
/////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
