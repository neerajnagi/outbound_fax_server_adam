(function () {

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
// packages/meteor-developer/meteor_developer_common.js                             //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////
                                                                                    //
METEOR_DEVELOPER_URL = "https://www.meteor.com";                                    // 1
                                                                                    // 2
//////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
// packages/meteor-developer/template.meteor_developer_configure.js                 //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////
                                                                                    //
Template.__define__("configureLoginServiceDialogForMeteorDeveloper",Package.handlebars.Handlebars.json_ast_to_func(["<p>\n    First, you'll need to get a Meteor developer account Client ID.\n    Follow these steps:\n  </p>\n  <ol>\n    <li> Visit <a href=\"https://www.meteor.com/account-settings\" target=\"_blank\">https://www.meteor.com/account-settings</a> and sign in.\n    </li>\n    <li> Click \"New app\" in the \"Meteor developer account apps\" section\n      and give your app a name.</li>\n    <li> Add\n      <span class=\"url\">\n        ",["{",[[0,"siteUrl"]]],"_oauth/meteor-developer?close\n      </span>\n      as an Allowed Redirect URL.\n    </li>\n  </ol>"]));
                                                                                    // 2
//////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
// packages/meteor-developer/meteor_developer_configure.js                          //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////
                                                                                    //
Template.configureLoginServiceDialogForMeteorDeveloper.siteUrl = function () {      // 1
  return Meteor.absoluteUrl();                                                      // 2
};                                                                                  // 3
                                                                                    // 4
Template.configureLoginServiceDialogForMeteorDeveloper.fields = function () {       // 5
  return [                                                                          // 6
    {property: 'clientId', label: 'App ID'},                                        // 7
    {property: 'secret', label: 'App secret'}                                       // 8
  ];                                                                                // 9
};                                                                                  // 10
                                                                                    // 11
//////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
// packages/meteor-developer/meteor_developer_client.js                             //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////
                                                                                    //
MeteorDeveloperAccounts = {};                                                       // 1
                                                                                    // 2
// Request Meteor developer account credentials for the user                        // 3
// @param credentialRequestCompleteCallback {Function} Callback function to call on // 4
//   completion. Takes one argument, credentialToken on success, or Error on        // 5
//   error.                                                                         // 6
var requestCredential = function (credentialRequestCompleteCallback) {              // 7
  var config = ServiceConfiguration.configurations.findOne({                        // 8
    service: 'meteor-developer'                                                     // 9
  });                                                                               // 10
  if (!config) {                                                                    // 11
    credentialRequestCompleteCallback &&                                            // 12
      credentialRequestCompleteCallback(                                            // 13
        new ServiceConfiguration.ConfigError("Service not configured")              // 14
      );                                                                            // 15
    return;                                                                         // 16
  }                                                                                 // 17
                                                                                    // 18
  var credentialToken = Random.id();                                                // 19
                                                                                    // 20
  var loginUrl =                                                                    // 21
        METEOR_DEVELOPER_URL + "/oauth2/authorize?" +                               // 22
        "state=" + credentialToken +                                                // 23
        "&response_type=code&" +                                                    // 24
        "client_id=" + config.clientId +                                            // 25
        "&redirect_uri=" + Meteor.absoluteUrl("_oauth/meteor-developer?close");     // 26
                                                                                    // 27
  Oauth.showPopup(                                                                  // 28
    loginUrl,                                                                       // 29
    _.bind(credentialRequestCompleteCallback, null, credentialToken),               // 30
    {                                                                               // 31
      width: 470,                                                                   // 32
      height: 420                                                                   // 33
    }                                                                               // 34
  );                                                                                // 35
};                                                                                  // 36
                                                                                    // 37
MeteorDeveloperAccounts.requestCredential = requestCredential;                      // 38
                                                                                    // 39
//////////////////////////////////////////////////////////////////////////////////////

}).call(this);
