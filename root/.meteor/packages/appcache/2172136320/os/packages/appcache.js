(function () {

/////////////////////////////////////////////////////////////////////////////////
//                                                                             //
// packages/appcache/appcache-server.js                                        //
//                                                                             //
/////////////////////////////////////////////////////////////////////////////////
                                                                               //
var crypto = Npm.require('crypto');                                            // 1
var fs = Npm.require('fs');                                                    // 2
var path = Npm.require('path');                                                // 3
                                                                               // 4
var knownBrowsers = [                                                          // 5
  'android',                                                                   // 6
  'chrome',                                                                    // 7
  'chromium',                                                                  // 8
  'chromeMobileIOS',                                                           // 9
  'firefox',                                                                   // 10
  'ie',                                                                        // 11
  'mobileSafari',                                                              // 12
  'safari'                                                                     // 13
];                                                                             // 14
                                                                               // 15
var browsersEnabledByDefault = [                                               // 16
  'android',                                                                   // 17
  'chrome',                                                                    // 18
  'chromium',                                                                  // 19
  'chromeMobileIOS',                                                           // 20
  'ie',                                                                        // 21
  'mobileSafari',                                                              // 22
  'safari'                                                                     // 23
];                                                                             // 24
                                                                               // 25
var enabledBrowsers = {};                                                      // 26
_.each(browsersEnabledByDefault, function (browser) {                          // 27
  enabledBrowsers[browser] = true;                                             // 28
});                                                                            // 29
                                                                               // 30
Meteor.AppCache = {                                                            // 31
  config: function(options) {                                                  // 32
    _.each(options, function (value, option) {                                 // 33
      if (option === 'browsers') {                                             // 34
        enabledBrowsers = {};                                                  // 35
        _.each(value, function (browser) {                                     // 36
          enabledBrowsers[browser] = true;                                     // 37
        });                                                                    // 38
      }                                                                        // 39
      else if (_.contains(knownBrowsers, option)) {                            // 40
        enabledBrowsers[option] = value;                                       // 41
      }                                                                        // 42
      else if (option === 'onlineOnly') {                                      // 43
        _.each(value, function (urlPrefix) {                                   // 44
          RoutePolicy.declare(urlPrefix, 'static-online');                     // 45
        });                                                                    // 46
      } else {                                                                 // 47
        throw new Error('Invalid AppCache config option: ' + option);          // 48
      }                                                                        // 49
    });                                                                        // 50
  }                                                                            // 51
};                                                                             // 52
                                                                               // 53
var browserEnabled = function(request) {                                       // 54
  return enabledBrowsers[request.browser.name];                                // 55
};                                                                             // 56
                                                                               // 57
WebApp.addHtmlAttributeHook(function (request) {                               // 58
  if (browserEnabled(request))                                                 // 59
    return 'manifest="/app.manifest"';                                         // 60
  else                                                                         // 61
    return null;                                                               // 62
});                                                                            // 63
                                                                               // 64
WebApp.connectHandlers.use(function(req, res, next) {                          // 65
  if (req.url !== '/app.manifest') {                                           // 66
    return next();                                                             // 67
  }                                                                            // 68
                                                                               // 69
  // Browsers will get confused if we unconditionally serve the                // 70
  // manifest and then disable the app cache for that browser.  If             // 71
  // the app cache had previously been enabled for a browser, it               // 72
  // will continue to fetch the manifest as long as it's available,            // 73
  // even if we now are not including the manifest attribute in the            // 74
  // app HTML.  (Firefox for example will continue to display "this            // 75
  // website is asking to store data on your computer for offline              // 76
  // use").  Returning a 404 gets the browser to really turn off the           // 77
  // app cache.                                                                // 78
                                                                               // 79
  if (!browserEnabled(WebApp.categorizeRequest(req))) {                        // 80
    res.writeHead(404);                                                        // 81
    res.end();                                                                 // 82
    return;                                                                    // 83
  }                                                                            // 84
                                                                               // 85
  var manifest = "CACHE MANIFEST\n\n";                                         // 86
                                                                               // 87
  // After the browser has downloaded the app files from the server and        // 88
  // has populated the browser's application cache, the browser will           // 89
  // *only* connect to the server and reload the application if the            // 90
  // *contents* of the app manifest file has changed.                          // 91
  //                                                                           // 92
  // So to ensure that the client updates if client resources change,          // 93
  // include a hash of client resources in the manifest.                       // 94
                                                                               // 95
  manifest += "# " + WebApp.clientHash + "\n";                                 // 96
                                                                               // 97
  // When using the autoupdate package, also include                           // 98
  // AUTOUPDATE_VERSION.  Otherwise the client will get into an                // 99
  // infinite loop of reloads when the browser doesn't fetch the new           // 100
  // app HTML which contains the new version, and autoupdate will              // 101
  // reload again trying to get the new code.                                  // 102
                                                                               // 103
  if (Package.autoupdate) {                                                    // 104
    var version = Package.autoupdate.Autoupdate.autoupdateVersion;             // 105
    if (version !== WebApp.clientHash)                                         // 106
      manifest += "# " + version + "\n";                                       // 107
  }                                                                            // 108
                                                                               // 109
  manifest += "\n";                                                            // 110
                                                                               // 111
  manifest += "CACHE:" + "\n";                                                 // 112
  manifest += "/" + "\n";                                                      // 113
  _.each(WebApp.clientProgram.manifest, function (resource) {                  // 114
    if (resource.where === 'client' &&                                         // 115
        ! RoutePolicy.classify(resource.url)) {                                // 116
      manifest += resource.url;                                                // 117
      // If the resource is not already cacheable (has a query                 // 118
      // parameter, presumably with a hash or version of some sort),           // 119
      // put a version with a hash in the cache.                               // 120
      //                                                                       // 121
      // Avoid putting a non-cacheable asset into the cache, otherwise         // 122
      // the user can't modify the asset until the cache headers               // 123
      // expire.                                                               // 124
      if (!resource.cacheable)                                                 // 125
        manifest += "?" + resource.hash;                                       // 126
                                                                               // 127
      manifest += "\n";                                                        // 128
    }                                                                          // 129
  });                                                                          // 130
  manifest += "\n";                                                            // 131
                                                                               // 132
  manifest += "FALLBACK:\n";                                                   // 133
  manifest += "/ /" + "\n";                                                    // 134
  // Add a fallback entry for each uncacheable asset we added above.           // 135
  //                                                                           // 136
  // This means requests for the bare url (/image.png instead of               // 137
  // /image.png?hash) will work offline. Online, however, the browser          // 138
  // will send a request to the server. Users can remove this extra            // 139
  // request to the server and have the asset served from cache by             // 140
  // specifying the full URL with hash in their code (manually, with           // 141
  // some sort of URL rewriting helper)                                        // 142
  _.each(WebApp.clientProgram.manifest, function (resource) {                  // 143
    if (resource.where === 'client' &&                                         // 144
        ! RoutePolicy.classify(resource.url) &&                                // 145
        !resource.cacheable) {                                                 // 146
      manifest += resource.url + " " + resource.url +                          // 147
        "?" + resource.hash + "\n";                                            // 148
    }                                                                          // 149
  });                                                                          // 150
                                                                               // 151
  manifest += "\n";                                                            // 152
                                                                               // 153
  manifest += "NETWORK:\n";                                                    // 154
  // TODO adding the manifest file to NETWORK should be unnecessary?           // 155
  // Want more testing to be sure.                                             // 156
  manifest += "/app.manifest" + "\n";                                          // 157
  _.each(                                                                      // 158
    [].concat(                                                                 // 159
      RoutePolicy.urlPrefixesFor('network'),                                   // 160
      RoutePolicy.urlPrefixesFor('static-online')                              // 161
    ),                                                                         // 162
    function (urlPrefix) {                                                     // 163
      manifest += urlPrefix + "\n";                                            // 164
    }                                                                          // 165
  );                                                                           // 166
  manifest += "*" + "\n";                                                      // 167
                                                                               // 168
  // content length needs to be based on bytes                                 // 169
  var body = new Buffer(manifest);                                             // 170
                                                                               // 171
  res.setHeader('Content-Type', 'text/cache-manifest');                        // 172
  res.setHeader('Content-Length', body.length);                                // 173
  return res.end(body);                                                        // 174
});                                                                            // 175
                                                                               // 176
var sizeCheck = function() {                                                   // 177
  var totalSize = 0;                                                           // 178
  _.each(WebApp.clientProgram.manifest, function (resource) {                  // 179
    if (resource.where === 'client' &&                                         // 180
        ! RoutePolicy.classify(resource.url)) {                                // 181
      totalSize += resource.size;                                              // 182
    }                                                                          // 183
  });                                                                          // 184
  if (totalSize > 5 * 1024 * 1024) {                                           // 185
    Meteor._debug(                                                             // 186
      "** You are using the appcache package but the total size of the\n" +    // 187
      "** cached resources is " +                                              // 188
      (totalSize / 1024 / 1024).toFixed(1) + "MB.\n" +                         // 189
      "**\n" +                                                                 // 190
      "** This is over the recommended maximum of 5 MB and may break your\n" + // 191
      "** app in some browsers! See http://docs.meteor.com/#appcache\n" +      // 192
      "** for more information and fixes.\n"                                   // 193
    );                                                                         // 194
  }                                                                            // 195
};                                                                             // 196
                                                                               // 197
sizeCheck();                                                                   // 198
                                                                               // 199
/////////////////////////////////////////////////////////////////////////////////

}).call(this);
