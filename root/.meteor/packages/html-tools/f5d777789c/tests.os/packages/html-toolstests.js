(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// packages/html-tools/charref_tests.js                                                                             //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
var Scanner = HTMLTools.Scanner;                                                                                    // 1
var getCharacterReference = HTMLTools.Parse.getCharacterReference;                                                  // 2
                                                                                                                    // 3
Tinytest.add("html-tools - entities", function (test) {                                                             // 4
  var succeed = function (input, match, codepoints) {                                                               // 5
    if (typeof input === 'string')                                                                                  // 6
      input = {input: input};                                                                                       // 7
                                                                                                                    // 8
    // match arg is optional; codepoints is never a string                                                          // 9
    if (typeof match !== 'string') {                                                                                // 10
      codepoints = match;                                                                                           // 11
      match = input.input;                                                                                          // 12
    }                                                                                                               // 13
                                                                                                                    // 14
    var scanner = new Scanner(input.input);                                                                         // 15
    var result = getCharacterReference(scanner, input.inAttribute, input.allowedChar);                              // 16
    test.isTrue(result);                                                                                            // 17
    test.equal(scanner.pos, match.length);                                                                          // 18
    test.equal(result, {                                                                                            // 19
      t: 'CharRef',                                                                                                 // 20
      v: match,                                                                                                     // 21
      cp: _.map(codepoints,                                                                                         // 22
                function (x) { return (typeof x === 'string' ?                                                      // 23
                                       x.charCodeAt(0) : x); })                                                     // 24
    });                                                                                                             // 25
  };                                                                                                                // 26
                                                                                                                    // 27
  var ignore = function (input) {                                                                                   // 28
    if (typeof input === 'string')                                                                                  // 29
      input = {input: input};                                                                                       // 30
                                                                                                                    // 31
    var scanner = new Scanner(input.input);                                                                         // 32
    var result = getCharacterReference(scanner, input.inAttribute, input.allowedChar);                              // 33
    test.isFalse(result);                                                                                           // 34
    test.equal(scanner.pos, 0);                                                                                     // 35
  };                                                                                                                // 36
                                                                                                                    // 37
  var fatal = function (input, messageContains) {                                                                   // 38
    if (typeof input === 'string')                                                                                  // 39
      input = {input: input};                                                                                       // 40
                                                                                                                    // 41
    var scanner = new Scanner(input.input);                                                                         // 42
    var error;                                                                                                      // 43
    try {                                                                                                           // 44
      getCharacterReference(scanner, input.inAttribute, input.allowedChar);                                         // 45
    } catch (e) {                                                                                                   // 46
      error = e;                                                                                                    // 47
    }                                                                                                               // 48
    test.isTrue(error);                                                                                             // 49
    if (error)                                                                                                      // 50
      test.isTrue(messageContains && error.message.indexOf(messageContains) >= 0, error.message);                   // 51
  };                                                                                                                // 52
                                                                                                                    // 53
  ignore('a');                                                                                                      // 54
  ignore('&');                                                                                                      // 55
  ignore('&&');                                                                                                     // 56
  ignore('&\t');                                                                                                    // 57
  ignore('& ');                                                                                                     // 58
  fatal('&#', 'Invalid numerical character reference starting with &#');                                            // 59
  ignore('&a');                                                                                                     // 60
  fatal('&a;', 'Invalid character reference: &a;');                                                                 // 61
  ignore({input: '&"', allowedChar: '"'});                                                                          // 62
  ignore('&"');                                                                                                     // 63
                                                                                                                    // 64
  succeed('&gt;', ['>']);                                                                                           // 65
  fatal('&gt', 'Character reference requires semicolon');                                                           // 66
  ignore('&aaa');                                                                                                   // 67
  fatal('&gta', 'Character reference requires semicolon');                                                          // 68
  ignore({input: '&gta', inAttribute: true});                                                                       // 69
  fatal({input: '&gt=', inAttribute: true}, 'Character reference requires semicolon: &gt');                         // 70
                                                                                                                    // 71
  succeed('&gt;;', '&gt;', ['>']);                                                                                  // 72
                                                                                                                    // 73
  fatal('&asdflkj;', 'Invalid character reference: &asdflkj;');                                                     // 74
  fatal('&A0asdflkj;', 'Invalid character reference: &A0asdflkj;');                                                 // 75
  ignore('&A0asdflkj');                                                                                             // 76
                                                                                                                    // 77
  succeed('&zopf;', [120171]);                                                                                      // 78
  succeed('&acE;', [8766, 819]);                                                                                    // 79
                                                                                                                    // 80
  succeed('&#10;', [10]);                                                                                           // 81
  fatal('&#10', 'Invalid numerical character reference starting with &#');                                          // 82
  fatal('&#xg;', 'Invalid numerical character reference starting with &#');                                         // 83
  fatal('&#;', 'Invalid numerical character reference starting with &#');                                           // 84
  fatal('&#a;', 'Invalid numerical character reference starting with &#');                                          // 85
  fatal('&#a', 'Invalid numerical character reference starting with &#');                                           // 86
  fatal('&#z', 'Invalid numerical character reference starting with &#');                                           // 87
  succeed('&#000000000000010;', [10]);                                                                              // 88
  fatal('&#0001000000000010;', 'Numerical character reference too large: 1000000000010');                           // 89
  succeed('&#x00000000000000000000a;', [10]);                                                                       // 90
  fatal('&#x000100000000000a;', 'Numerical character reference too large: 0x100000000000a');                        // 91
  succeed('&#010;', [10]);                                                                                          // 92
  succeed('&#xa;', [10]);                                                                                           // 93
  succeed('&#Xa;', [10]);                                                                                           // 94
  succeed('&#XA;', [10]);                                                                                           // 95
  succeed('&#xA;', [10]);                                                                                           // 96
                                                                                                                    // 97
  fatal('&#0;', 'Illegal codepoint in numerical character reference: &#0;');                                        // 98
  fatal('&#x0;', 'Illegal codepoint in numerical character reference: &#x0;');                                      // 99
                                                                                                                    // 100
  fatal('&#xb;', 'Illegal codepoint in numerical character reference: &#xb;');                                      // 101
  succeed('&#xc;', [12]);                                                                                           // 102
  fatal('&#11;', 'Illegal codepoint in numerical character reference: &#11;');                                      // 103
  succeed('&#12;', [12]);                                                                                           // 104
                                                                                                                    // 105
  fatal('&#x10ffff;', 'Illegal codepoint in numerical character reference');                                        // 106
  fatal('&#x10fffe;', 'Illegal codepoint in numerical character reference');                                        // 107
  succeed('&#x10fffd;', [0x10fffd]);                                                                                // 108
                                                                                                                    // 109
  fatal('&#1114111;', 'Illegal codepoint in numerical character reference');                                        // 110
  fatal('&#1114110;', 'Illegal codepoint in numerical character reference');                                        // 111
  succeed('&#1114109;', [0x10fffd]);                                                                                // 112
                                                                                                                    // 113
});                                                                                                                 // 114
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// packages/html-tools/tokenize_tests.js                                                                            //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
var Scanner = HTMLTools.Scanner;                                                                                    // 1
var getComment = HTMLTools.Parse.getComment;                                                                        // 2
var getDoctype = HTMLTools.Parse.getDoctype;                                                                        // 3
var getHTMLToken = HTMLTools.Parse.getHTMLToken;                                                                    // 4
                                                                                                                    // 5
// "tokenize" is not really a great operation for real use, because                                                 // 6
// it ignores the special content rules for tags like "style" and                                                   // 7
// "script".                                                                                                        // 8
var tokenize = function (input) {                                                                                   // 9
  var scanner = new Scanner(input);                                                                                 // 10
  var tokens = [];                                                                                                  // 11
  while (! scanner.isEOF()) {                                                                                       // 12
    var token = getHTMLToken(scanner);                                                                              // 13
    if (token)                                                                                                      // 14
      tokens.push(token);                                                                                           // 15
  }                                                                                                                 // 16
                                                                                                                    // 17
  return tokens;                                                                                                    // 18
};                                                                                                                  // 19
                                                                                                                    // 20
                                                                                                                    // 21
Tinytest.add("html-tools - comments", function (test) {                                                             // 22
  var succeed = function (input, content) {                                                                         // 23
    var scanner = new Scanner(input);                                                                               // 24
    var result = getComment(scanner);                                                                               // 25
    test.isTrue(result);                                                                                            // 26
    test.equal(scanner.pos, content.length + 7);                                                                    // 27
    test.equal(result, {                                                                                            // 28
      t: 'Comment',                                                                                                 // 29
      v: content                                                                                                    // 30
    });                                                                                                             // 31
  };                                                                                                                // 32
                                                                                                                    // 33
  var ignore = function (input) {                                                                                   // 34
    var scanner = new Scanner(input);                                                                               // 35
    var result = getComment(scanner);;                                                                              // 36
    test.isFalse(result);                                                                                           // 37
    test.equal(scanner.pos, 0);                                                                                     // 38
  };                                                                                                                // 39
                                                                                                                    // 40
  var fatal = function (input, messageContains) {                                                                   // 41
    var scanner = new Scanner(input);                                                                               // 42
    var error;                                                                                                      // 43
    try {                                                                                                           // 44
      getComment(scanner);                                                                                          // 45
    } catch (e) {                                                                                                   // 46
      error = e;                                                                                                    // 47
    }                                                                                                               // 48
    test.isTrue(error);                                                                                             // 49
    if (error)                                                                                                      // 50
      test.isTrue(messageContains && error.message.indexOf(messageContains) >= 0, error.message);                   // 51
  };                                                                                                                // 52
                                                                                                                    // 53
  test.equal(getComment(new Scanner("<!-- hello -->")),                                                             // 54
             { t: 'Comment', v: ' hello ' });                                                                       // 55
                                                                                                                    // 56
  ignore("<!DOCTYPE>");                                                                                             // 57
  ignore("<!-a");                                                                                                   // 58
  ignore("<--");                                                                                                    // 59
  ignore("<!");                                                                                                     // 60
  ignore("abc");                                                                                                    // 61
  ignore("<a");                                                                                                     // 62
                                                                                                                    // 63
  fatal('<!--', 'Unclosed');                                                                                        // 64
  fatal('<!---', 'Unclosed');                                                                                       // 65
  fatal('<!----', 'Unclosed');                                                                                      // 66
  fatal('<!-- -', 'Unclosed');                                                                                      // 67
  fatal('<!-- --', 'Unclosed');                                                                                     // 68
  fatal('<!-- -- abcd', 'Unclosed');                                                                                // 69
  fatal('<!-- ->', 'Unclosed');                                                                                     // 70
  fatal('<!-- a--b -->', 'cannot contain');                                                                         // 71
  fatal('<!--x--->', 'must end at first');                                                                          // 72
                                                                                                                    // 73
  fatal('<!-- a\u0000b -->', 'cannot contain');                                                                     // 74
  fatal('<!--\u0000 x-->', 'cannot contain');                                                                       // 75
                                                                                                                    // 76
  succeed('<!---->', '');                                                                                           // 77
  succeed('<!---x-->', '-x');                                                                                       // 78
  succeed('<!--x-->', 'x');                                                                                         // 79
  succeed('<!-- hello - - world -->', ' hello - - world ');                                                         // 80
});                                                                                                                 // 81
                                                                                                                    // 82
Tinytest.add("html-tools - doctype", function (test) {                                                              // 83
  var succeed = function (input, expectedProps) {                                                                   // 84
    var scanner = new Scanner(input);                                                                               // 85
    var result = getDoctype(scanner);                                                                               // 86
    test.isTrue(result);                                                                                            // 87
    test.equal(scanner.pos, result.v.length);                                                                       // 88
    test.equal(input.slice(0, result.v.length), result.v);                                                          // 89
    var actualProps = _.extend({}, result);                                                                         // 90
    delete actualProps.t;                                                                                           // 91
    delete actualProps.v;                                                                                           // 92
    test.equal(actualProps, expectedProps);                                                                         // 93
  };                                                                                                                // 94
                                                                                                                    // 95
  var fatal = function (input, messageContains) {                                                                   // 96
    var scanner = new Scanner(input);                                                                               // 97
    var error;                                                                                                      // 98
    try {                                                                                                           // 99
      getDoctype(scanner);                                                                                          // 100
    } catch (e) {                                                                                                   // 101
      error = e;                                                                                                    // 102
    }                                                                                                               // 103
    test.isTrue(error);                                                                                             // 104
    if (messageContains)                                                                                            // 105
      test.isTrue(error.message.indexOf(messageContains) >= 0, error.message);                                      // 106
  };                                                                                                                // 107
                                                                                                                    // 108
  test.equal(getDoctype(new Scanner("<!DOCTYPE html>x")),                                                           // 109
             { t: 'Doctype',                                                                                        // 110
               v: '<!DOCTYPE html>',                                                                                // 111
               name: 'html' });                                                                                     // 112
                                                                                                                    // 113
  test.equal(getDoctype(new Scanner("<!DOCTYPE html SYSTEM 'about:legacy-compat'>x")),                              // 114
             { t: 'Doctype',                                                                                        // 115
               v: "<!DOCTYPE html SYSTEM 'about:legacy-compat'>",                                                   // 116
               name: 'html',                                                                                        // 117
               systemId: 'about:legacy-compat' });                                                                  // 118
                                                                                                                    // 119
  test.equal(getDoctype(new Scanner("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.0//EN'>x")),                         // 120
             { t: 'Doctype',                                                                                        // 121
               v: "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.0//EN'>",                                              // 122
               name: 'html',                                                                                        // 123
               publicId: '-//W3C//DTD HTML 4.0//EN' });                                                             // 124
                                                                                                                    // 125
  test.equal(getDoctype(new Scanner("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.0//EN' 'http://www.w3.org/TR/html4/strict.dtd'>x")),
             { t: 'Doctype',                                                                                        // 127
               v: "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.0//EN' 'http://www.w3.org/TR/html4/strict.dtd'>",      // 128
               name: 'html',                                                                                        // 129
               publicId: '-//W3C//DTD HTML 4.0//EN',                                                                // 130
               systemId: 'http://www.w3.org/TR/html4/strict.dtd' });                                                // 131
                                                                                                                    // 132
  succeed('<!DOCTYPE html>', {name: 'html'});                                                                       // 133
  succeed('<!DOCTYPE htML>', {name: 'html'});                                                                       // 134
  succeed('<!DOCTYPE HTML>', {name: 'html'});                                                                       // 135
  succeed('<!doctype html>', {name: 'html'});                                                                       // 136
  succeed('<!doctYPE html>', {name: 'html'});                                                                       // 137
  succeed('<!DOCTYPE html \u000c>', {name: 'html'});                                                                // 138
  fatal('<!DOCTYPE', 'Expected space');                                                                             // 139
  fatal('<!DOCTYPE ', 'Malformed DOCTYPE');                                                                         // 140
  fatal('<!DOCTYPE  ', 'Malformed DOCTYPE');                                                                        // 141
  fatal('<!DOCTYPE>', 'Expected space');                                                                            // 142
  fatal('<!DOCTYPE >', 'Malformed DOCTYPE');                                                                        // 143
  fatal('<!DOCTYPE\u0000', 'Expected space');                                                                       // 144
  fatal('<!DOCTYPE \u0000', 'Malformed DOCTYPE');                                                                   // 145
  fatal('<!DOCTYPE html\u0000>', 'Malformed DOCTYPE');                                                              // 146
  fatal('<!DOCTYPE html', 'Malformed DOCTYPE');                                                                     // 147
                                                                                                                    // 148
  succeed('<!DOCTYPE html SYSTEM "about:legacy-compat">', {name: 'html', systemId: 'about:legacy-compat'});         // 149
  succeed('<!doctype HTML system "about:legacy-compat">', {name: 'html', systemId: 'about:legacy-compat'});         // 150
  succeed("<!DOCTYPE html SYSTEM 'about:legacy-compat'>", {name: 'html', systemId: 'about:legacy-compat'});         // 151
  succeed("<!dOcTyPe HtMl sYsTeM 'about:legacy-compat'>", {name: 'html', systemId: 'about:legacy-compat'});         // 152
  succeed('<!DOCTYPE  html\tSYSTEM\t"about:legacy-compat"   \t>', {name: 'html', systemId: 'about:legacy-compat'}); // 153
  fatal('<!DOCTYPE html SYSTE "about:legacy-compat">', 'Expected PUBLIC or SYSTEM');                                // 154
  fatal('<!DOCTYPE html SYSTE', 'Expected PUBLIC or SYSTEM');                                                       // 155
  fatal('<!DOCTYPE html SYSTEM"about:legacy-compat">', 'Expected space');                                           // 156
  fatal('<!DOCTYPE html SYSTEM');                                                                                   // 157
  fatal('<!DOCTYPE html SYSTEM ');                                                                                  // 158
  fatal('<!DOCTYPE html SYSTEM>');                                                                                  // 159
  fatal('<!DOCTYPE html SYSTEM >');                                                                                 // 160
  fatal('<!DOCTYPE html SYSTEM ">">');                                                                              // 161
  fatal('<!DOCTYPE html SYSTEM "\u0000about:legacy-compat">');                                                      // 162
  fatal('<!DOCTYPE html SYSTEM "about:legacy-compat\u0000">');                                                      // 163
  fatal('<!DOCTYPE html SYSTEM "');                                                                                 // 164
  fatal('<!DOCTYPE html SYSTEM "">');                                                                               // 165
  fatal('<!DOCTYPE html SYSTEM \'');                                                                                // 166
  fatal('<!DOCTYPE html SYSTEM\'a\'>');                                                                             // 167
  fatal('<!DOCTYPE html SYSTEM about:legacy-compat>');                                                              // 168
                                                                                                                    // 169
  succeed('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0//EN">',                                                      // 170
          { name: 'html',                                                                                           // 171
            publicId: '-//W3C//DTD HTML 4.0//EN'});                                                                 // 172
  succeed('<!DOCTYPE html PUBLIC \'-//W3C//DTD HTML 4.0//EN\'>',                                                    // 173
          { name: 'html',                                                                                           // 174
            publicId: '-//W3C//DTD HTML 4.0//EN'});                                                                 // 175
  succeed('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">',         // 176
          { name: 'html',                                                                                           // 177
            publicId: '-//W3C//DTD HTML 4.0//EN',                                                                   // 178
            systemId: 'http://www.w3.org/TR/REC-html40/strict.dtd'});                                               // 179
  succeed('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0//EN" \'http://www.w3.org/TR/REC-html40/strict.dtd\'>',       // 180
          { name: 'html',                                                                                           // 181
            publicId: '-//W3C//DTD HTML 4.0//EN',                                                                   // 182
            systemId: 'http://www.w3.org/TR/REC-html40/strict.dtd'});                                               // 183
  succeed('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' \'http://www.w3.org/TR/REC-html40/strict.dtd\'>',     // 184
          { name: 'html',                                                                                           // 185
            publicId: '-//W3C//DTD HTML 4.0//EN',                                                                   // 186
            systemId: 'http://www.w3.org/TR/REC-html40/strict.dtd'});                                               // 187
  succeed('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\'\t\'http://www.w3.org/TR/REC-html40/strict.dtd\'   >', // 188
          { name: 'html',                                                                                           // 189
            publicId: '-//W3C//DTD HTML 4.0//EN',                                                                   // 190
            systemId: 'http://www.w3.org/TR/REC-html40/strict.dtd'});                                               // 191
  fatal('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' \'http://www.w3.org/TR/REC-html40/strict.dtd\'');       // 192
  fatal('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' \'http://www.w3.org/TR/REC-html40/strict.dtd\'');       // 193
  fatal('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' \'http://www.w3.org/TR/REC-html40/strict.dtd');         // 194
  fatal('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' \'');                                                   // 195
  fatal('<!DOCTYPE html public \'-//W3C//DTD HTML 4.0//EN\' ');                                                     // 196
  fatal('<!DOCTYPE html public \'- ');                                                                              // 197
  fatal('<!DOCTYPE html public>');                                                                                  // 198
  fatal('<!DOCTYPE html public "-//W3C//DTD HTML 4.0//EN""http://www.w3.org/TR/REC-html40/strict.dtd">');           // 199
});                                                                                                                 // 200
                                                                                                                    // 201
Tinytest.add("html-tools - tokenize", function (test) {                                                             // 202
                                                                                                                    // 203
  var fatal = function (input, messageContains) {                                                                   // 204
    var error;                                                                                                      // 205
    try {                                                                                                           // 206
      tokenize(input);                                                                                              // 207
    } catch (e) {                                                                                                   // 208
      error = e;                                                                                                    // 209
    }                                                                                                               // 210
    test.isTrue(error);                                                                                             // 211
    if (messageContains)                                                                                            // 212
      test.isTrue(error.message.indexOf(messageContains) >= 0, error.message);                                      // 213
  };                                                                                                                // 214
                                                                                                                    // 215
                                                                                                                    // 216
  test.equal(tokenize(''), []);                                                                                     // 217
  test.equal(tokenize('abc'), [{t: 'Chars', v: 'abc'}]);                                                            // 218
  test.equal(tokenize('&'), [{t: 'Chars', v: '&'}]);                                                                // 219
  test.equal(tokenize('&amp;'), [{t: 'CharRef', v: '&amp;', cp: [38]}]);                                            // 220
  test.equal(tokenize('ok&#32;fine'),                                                                               // 221
             [{t: 'Chars', v: 'ok'},                                                                                // 222
              {t: 'CharRef', v: '&#32;', cp: [32]},                                                                 // 223
              {t: 'Chars', v: 'fine'}]);                                                                            // 224
                                                                                                                    // 225
  test.equal(tokenize('a<!--b-->c'),                                                                                // 226
             [{t: 'Chars',                                                                                          // 227
               v: 'a'},                                                                                             // 228
              {t: 'Comment',                                                                                        // 229
               v: 'b'},                                                                                             // 230
              {t: 'Chars',                                                                                          // 231
               v: 'c'}]);                                                                                           // 232
                                                                                                                    // 233
  test.equal(tokenize('<a>'), [{t: 'Tag', n: 'a'}]);                                                                // 234
                                                                                                                    // 235
  fatal('<');                                                                                                       // 236
  fatal('<x');                                                                                                      // 237
  fatal('<x ');                                                                                                     // 238
  fatal('<x a');                                                                                                    // 239
  fatal('<x a ');                                                                                                   // 240
  fatal('<x a =');                                                                                                  // 241
  fatal('<x a = ');                                                                                                 // 242
  fatal('<x a = b');                                                                                                // 243
  fatal('<x a = "b');                                                                                               // 244
  fatal('<x a = \'b');                                                                                              // 245
  fatal('<x a = b ');                                                                                               // 246
  fatal('<x a = b /');                                                                                              // 247
  test.equal(tokenize('<x a = b />'),                                                                               // 248
             [{t: 'Tag', n: 'x',                                                                                    // 249
               attrs: { a: [{t: 'Chars', v: 'b'}] },                                                                // 250
               isSelfClosing: true}]);                                                                              // 251
                                                                                                                    // 252
  test.equal(tokenize('<a>X</a>'),                                                                                  // 253
             [{t: 'Tag', n: 'a'},                                                                                   // 254
              {t: 'Chars', v: 'X'},                                                                                 // 255
              {t: 'Tag', n: 'a', isEnd: true}]);                                                                    // 256
                                                                                                                    // 257
  fatal('<x a a>'); // duplicate attribute value                                                                    // 258
  test.equal(tokenize('<a b  >'),                                                                                   // 259
             [{t: 'Tag', n: 'a', attrs: { b: [] }}]);                                                               // 260
  fatal('< a>');                                                                                                    // 261
  fatal('< /a>');                                                                                                   // 262
  fatal('</ a>');                                                                                                   // 263
                                                                                                                    // 264
  // Slash does not end an unquoted attribute, interestingly                                                        // 265
  test.equal(tokenize('<a b=/>'),                                                                                   // 266
             [{t: 'Tag', n: 'a', attrs: { b: [{t: 'Chars', v: '/'}] }}]);                                           // 267
                                                                                                                    // 268
  test.equal(tokenize('<a b="c" d=e f=\'g\' h \t>'),                                                                // 269
             [{t: 'Tag', n: 'a',                                                                                    // 270
               attrs: { b: [{t: 'Chars', v: 'c'}],                                                                  // 271
                        d: [{t: 'Chars', v: 'e'}],                                                                  // 272
                        f: [{t: 'Chars', v: 'g'}],                                                                  // 273
                        h: [] }}]);                                                                                 // 274
                                                                                                                    // 275
  fatal('</a b="c" d=e f=\'g\' h \t\u0000>');                                                                       // 276
  fatal('</a b="c" d=ef=\'g\' h \t>');                                                                              // 277
  fatal('</a b="c"d=e f=\'g\' h \t>');                                                                              // 278
                                                                                                                    // 279
  test.equal(tokenize('<a/>'), [{t: 'Tag', n: 'a', isSelfClosing: true}]);                                          // 280
                                                                                                                    // 281
  fatal('<a/ >');                                                                                                   // 282
  fatal('<a/b>');                                                                                                   // 283
  fatal('<a b=c`>');                                                                                                // 284
  fatal('<a b=c<>');                                                                                                // 285
                                                                                                                    // 286
  test.equal(tokenize('<a# b0="c@" d1=e2 f#=\'g  \' h \t>'),                                                        // 287
             [{t: 'Tag', n: 'a#',                                                                                   // 288
               attrs: { b0: [{t: 'Chars', v: 'c@'}],                                                                // 289
                        d1: [{t: 'Chars', v: 'e2'}],                                                                // 290
                        'f#': [{t: 'Chars', v: 'g  '}],                                                             // 291
                        h: [] }}]);                                                                                 // 292
                                                                                                                    // 293
  test.equal(tokenize('<div class=""></div>'),                                                                      // 294
             [{t: 'Tag', n: 'div', attrs: { 'class': [] }},                                                         // 295
              {t: 'Tag', n: 'div', isEnd: true}]);                                                                  // 296
                                                                                                                    // 297
  test.equal(tokenize('<div class="&">'),                                                                           // 298
             [{t: 'Tag', n: 'div', attrs: { 'class': [{t: 'Chars', v: '&'}] }}]);                                   // 299
  test.equal(tokenize('<div class=&>'),                                                                             // 300
             [{t: 'Tag', n: 'div', attrs: { 'class': [{t: 'Chars', v: '&'}] }}]);                                   // 301
  test.equal(tokenize('<div class=&amp;>'),                                                                         // 302
             [{t: 'Tag', n: 'div', attrs: { 'class': [{t: 'CharRef', v: '&amp;', cp: [38]}] }}]);                   // 303
                                                                                                                    // 304
  test.equal(tokenize('<div class=aa&&zopf;&acE;&bb>'),                                                             // 305
             [{t: 'Tag', n: 'div', attrs: { 'class': [                                                              // 306
               {t: 'Chars', v: 'aa&'},                                                                              // 307
               {t: 'CharRef', v: '&zopf;', cp: [120171]},                                                           // 308
               {t: 'CharRef', v: '&acE;', cp: [8766, 819]},                                                         // 309
               {t: 'Chars', v: '&bb'}                                                                               // 310
             ] }}]);                                                                                                // 311
                                                                                                                    // 312
  test.equal(tokenize('<div class="aa &&zopf;&acE;& bb">'),                                                         // 313
             [{t: 'Tag', n: 'div', attrs: { 'class': [                                                              // 314
               {t: 'Chars', v: 'aa &'},                                                                             // 315
               {t: 'CharRef', v: '&zopf;', cp: [120171]},                                                           // 316
               {t: 'CharRef', v: '&acE;', cp: [8766, 819]},                                                         // 317
               {t: 'Chars', v: '& bb'}                                                                              // 318
             ] }}]);                                                                                                // 319
                                                                                                                    // 320
  test.equal(tokenize('<a b="\'`<>&">'),                                                                            // 321
             [{t: 'Tag', n: 'a', attrs: { b: [{t: 'Chars', v: '\'`<>&'}] }}]);                                      // 322
  test.equal(tokenize('<a b=\'"`<>&\'>'),                                                                           // 323
             [{t: 'Tag', n: 'a', attrs: { b: [{t: 'Chars', v: '"`<>&'}] }}]);                                       // 324
                                                                                                                    // 325
  fatal('&gt');                                                                                                     // 326
  fatal('&gtc');                                                                                                    // 327
  test.equal(tokenize('<a b=&gtc>'),                                                                                // 328
             [{t: 'Tag', n: 'a', attrs: { b: [{t: 'Chars', v: '&gtc' }] }}]);                                       // 329
  test.equal(tokenize('<a b="&gtc">'),                                                                              // 330
             [{t: 'Tag', n: 'a', attrs: { b: [{t: 'Chars', v: '&gtc' }] }}]);                                       // 331
  fatal('<a b=&gt>');                                                                                               // 332
  fatal('<a b="&gt">');                                                                                             // 333
  fatal('<a b="&gt=">');                                                                                            // 334
                                                                                                                    // 335
  fatal('<!');                                                                                                      // 336
  fatal('<!x>');                                                                                                    // 337
                                                                                                                    // 338
  fatal('<a{{b}}>');                                                                                                // 339
  fatal('<{{a}}>');                                                                                                 // 340
  fatal('</a b=c>'); // end tag can't have attributes                                                               // 341
  fatal('</a/>'); // end tag can't be self-closing                                                                  // 342
  fatal('</a  />');                                                                                                 // 343
});                                                                                                                 // 344
                                                                                                                    // 345
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// packages/html-tools/parse_tests.js                                                                               //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
var Scanner = HTMLTools.Scanner;                                                                                    // 1
var getContent = HTMLTools.Parse.getContent;                                                                        // 2
                                                                                                                    // 3
var CharRef = HTML.CharRef;                                                                                         // 4
var Comment = HTML.Comment;                                                                                         // 5
var Special = HTMLTools.Special;                                                                                    // 6
                                                                                                                    // 7
var BR = HTML.BR;                                                                                                   // 8
var HR = HTML.HR;                                                                                                   // 9
var INPUT = HTML.INPUT;                                                                                             // 10
var A = HTML.A;                                                                                                     // 11
var DIV = HTML.DIV;                                                                                                 // 12
var P = HTML.P;                                                                                                     // 13
var TEXTAREA = HTML.TEXTAREA;                                                                                       // 14
                                                                                                                    // 15
Tinytest.add("html-tools - parser getContent", function (test) {                                                    // 16
                                                                                                                    // 17
  var succeed = function (input, expected) {                                                                        // 18
    var endPos = input.indexOf('^^^');                                                                              // 19
    if (endPos < 0)                                                                                                 // 20
      endPos = input.length;                                                                                        // 21
                                                                                                                    // 22
    var scanner = new Scanner(input.replace('^^^', ''));                                                            // 23
    var result = getContent(scanner);                                                                               // 24
    test.equal(scanner.pos, endPos);                                                                                // 25
    test.equal(HTML.toJS(result), HTML.toJS(expected));                                                             // 26
  };                                                                                                                // 27
                                                                                                                    // 28
  var fatal = function (input, messageContains) {                                                                   // 29
    var scanner = new Scanner(input);                                                                               // 30
    var error;                                                                                                      // 31
    try {                                                                                                           // 32
      getContent(scanner);                                                                                          // 33
    } catch (e) {                                                                                                   // 34
      error = e;                                                                                                    // 35
    }                                                                                                               // 36
    test.isTrue(error);                                                                                             // 37
    if (messageContains)                                                                                            // 38
      test.isTrue(messageContains && error.message.indexOf(messageContains) >= 0, error.message);                   // 39
  };                                                                                                                // 40
                                                                                                                    // 41
                                                                                                                    // 42
  succeed('', null);                                                                                                // 43
  succeed('abc', 'abc');                                                                                            // 44
  succeed('abc^^^</x>', 'abc');                                                                                     // 45
  succeed('a&lt;b', ['a', CharRef({html: '&lt;', str: '<'}), 'b']);                                                 // 46
  succeed('<!-- x -->', Comment(' x '));                                                                            // 47
  succeed('&acE;', CharRef({html: '&acE;', str: '\u223e\u0333'}));                                                  // 48
  succeed('&zopf;', CharRef({html: '&zopf;', str: '\ud835\udd6b'}));                                                // 49
  succeed('&&>&g&gt;;', ['&&>&g', CharRef({html: '&gt;', str: '>'}), ';']);                                         // 50
                                                                                                                    // 51
  // Can't have an unescaped `&` if followed by certain names like `gt`                                             // 52
  fatal('&gt&');                                                                                                    // 53
  // tests for other failure cases                                                                                  // 54
  fatal('<');                                                                                                       // 55
                                                                                                                    // 56
  succeed('<br>', BR());                                                                                            // 57
  succeed('<br/>', BR());                                                                                           // 58
  fatal('<div/>', 'self-close');                                                                                    // 59
                                                                                                                    // 60
  succeed('<hr id=foo>', HR({id:'foo'}));                                                                           // 61
  succeed('<hr id=&lt;foo&gt;>', HR({id:[CharRef({html:'&lt;', str:'<'}),                                           // 62
                                         'foo',                                                                     // 63
                                         CharRef({html:'&gt;', str:'>'})]}));                                       // 64
  succeed('<input selected>', INPUT({selected: ''}));                                                               // 65
  succeed('<input selected/>', INPUT({selected: ''}));                                                              // 66
  succeed('<input selected />', INPUT({selected: ''}));                                                             // 67
  var FOO = HTML.getTag('foo');                                                                                     // 68
  succeed('<foo bar></foo>', FOO({bar: ''}));                                                                       // 69
  succeed('<foo bar baz ></foo>', FOO({bar: '', baz: ''}));                                                         // 70
  succeed('<foo bar=x baz qux=y blah ></foo>',                                                                      // 71
          FOO({bar: 'x', baz: '', qux: 'y', blah: ''}));                                                            // 72
  succeed('<foo bar="x" baz qux="y" blah ></foo>',                                                                  // 73
          FOO({bar: 'x', baz: '', qux: 'y', blah: ''}));                                                            // 74
  fatal('<input bar"baz">');                                                                                        // 75
  fatal('<input x="y"z >');                                                                                         // 76
  fatal('<input x=\'y\'z >');                                                                                       // 77
  succeed('<br x=&&&>', BR({x: '&&&'}));                                                                            // 78
  succeed('<br><br><br>', [BR(), BR(), BR()]);                                                                      // 79
  succeed('aaa<br>\nbbb<br>\nccc<br>', ['aaa', BR(), '\nbbb', BR(), '\nccc', BR()]);                                // 80
                                                                                                                    // 81
  succeed('<a></a>', A());                                                                                          // 82
  fatal('<');                                                                                                       // 83
  fatal('<a');                                                                                                      // 84
  fatal('<a>');                                                                                                     // 85
  fatal('<a><');                                                                                                    // 86
  fatal('<a></');                                                                                                   // 87
  fatal('<a></a');                                                                                                  // 88
                                                                                                                    // 89
  succeed('<a href="http://www.apple.com/">Apple</a>',                                                              // 90
          A({href: "http://www.apple.com/"}, 'Apple'));                                                             // 91
                                                                                                                    // 92
  (function () {                                                                                                    // 93
    var A = HTML.getTag('a');                                                                                       // 94
    var B = HTML.getTag('b');                                                                                       // 95
    var C = HTML.getTag('c');                                                                                       // 96
    var D = HTML.getTag('d');                                                                                       // 97
                                                                                                                    // 98
    succeed('<a>1<b>2<c>3<d>4</d>5</c>6</b>7</a>8',                                                                 // 99
            [A('1', B('2', C('3', D('4'), '5'), '6'), '7'), '8']);                                                  // 100
  })();                                                                                                             // 101
                                                                                                                    // 102
  fatal('<b>hello <i>there</b> world</i>');                                                                         // 103
                                                                                                                    // 104
  // XXX support implied end tags in cases allowed by the spec                                                      // 105
  fatal('<p>');                                                                                                     // 106
                                                                                                                    // 107
  fatal('<a>Foo</a/>');                                                                                             // 108
  fatal('<a>Foo</a b=c>');                                                                                          // 109
                                                                                                                    // 110
  succeed('<textarea>asdf</textarea>', TEXTAREA("asdf"));                                                           // 111
  succeed('<textarea x=y>asdf</textarea>', TEXTAREA({x: "y"}, "asdf"));                                             // 112
  succeed('<textarea><p></textarea>', TEXTAREA("<p>"));                                                             // 113
  succeed('<textarea>a&amp;b</textarea>',                                                                           // 114
          TEXTAREA("a", CharRef({html: '&amp;', str: '&'}), "b"));                                                  // 115
  succeed('<textarea></textarea</textarea>', TEXTAREA("</textarea"));                                               // 116
  // absorb up to one initial newline, as per HTML parsing spec                                                     // 117
  succeed('<textarea>\n</textarea>', TEXTAREA());                                                                   // 118
  succeed('<textarea>\nasdf</textarea>', TEXTAREA("asdf"));                                                         // 119
  succeed('<textarea>\n\nasdf</textarea>', TEXTAREA("\nasdf"));                                                     // 120
  succeed('<textarea>\n\n</textarea>', TEXTAREA("\n"));                                                             // 121
  succeed('<textarea>\nasdf\n</textarea>', TEXTAREA("asdf\n"));                                                     // 122
  succeed('<textarea><!-- --></textarea>', TEXTAREA("<!-- -->"));                                                   // 123
  succeed('<tExTaReA>asdf</TEXTarea>', TEXTAREA("asdf"));                                                           // 124
  fatal('<textarea>asdf');                                                                                          // 125
  fatal('<textarea>asdf</textarea');                                                                                // 126
  fatal('<textarea>&davidgreenspan;</textarea>');                                                                   // 127
  succeed('<textarea>&</textarea>', TEXTAREA("&"));                                                                 // 128
  succeed('<textarea></textarea  \n<</textarea  \n>asdf',                                                           // 129
          [TEXTAREA("</textarea  \n<"), "asdf"]);                                                                   // 130
                                                                                                                    // 131
  // CR/LF behavior                                                                                                 // 132
  succeed('<br\r\n x>', BR({x:''}));                                                                                // 133
  succeed('<br\r x>', BR({x:''}));                                                                                  // 134
  succeed('<br x="y"\r\n>', BR({x:'y'}));                                                                           // 135
  succeed('<br x="y"\r>', BR({x:'y'}));                                                                             // 136
  succeed('<br x=\r\n"y">', BR({x:'y'}));                                                                           // 137
  succeed('<br x=\r"y">', BR({x:'y'}));                                                                             // 138
  succeed('<br x\r=\r"y">', BR({x:'y'}));                                                                           // 139
  succeed('<!--\r\n-->', Comment('\n'));                                                                            // 140
  succeed('<!--\r-->', Comment('\n'));                                                                              // 141
  succeed('<textarea>a\r\nb\r\nc</textarea>', TEXTAREA('a\nb\nc'));                                                 // 142
  succeed('<textarea>a\rb\rc</textarea>', TEXTAREA('a\nb\nc'));                                                     // 143
  succeed('<br x="\r\n\r\n">', BR({x:'\n\n'}));                                                                     // 144
  succeed('<br x="\r\r">', BR({x:'\n\n'}));                                                                         // 145
  succeed('<br x=y\r>', BR({x:'y'}));                                                                               // 146
  fatal('<br x=\r>');                                                                                               // 147
});                                                                                                                 // 148
                                                                                                                    // 149
Tinytest.add("html-tools - parseFragment", function (test) {                                                        // 150
  test.equal(HTML.toJS(HTMLTools.parseFragment("<div><p id=foo>Hello</p></div>")),                                  // 151
             HTML.toJS(DIV(P({id:'foo'}, 'Hello'))));                                                               // 152
                                                                                                                    // 153
  _.each(['asdf</br>', '{{!foo}}</br>', '{{!foo}} </br>',                                                           // 154
          'asdf</a>', '{{!foo}}</a>', '{{!foo}} </a>'], function (badFrag) {                                        // 155
            test.throws(function() {                                                                                // 156
              HTMLTools.parseFragment(badFrag);                                                                     // 157
            }, /Unexpected HTML close tag/);                                                                        // 158
          });                                                                                                       // 159
                                                                                                                    // 160
  (function () {                                                                                                    // 161
    var p = HTMLTools.parseFragment('<p></p>');                                                                     // 162
    test.equal(p.tagName, 'p');                                                                                     // 163
    test.equal(p.attrs, null);                                                                                      // 164
    test.isTrue(p instanceof HTML.Tag);                                                                             // 165
    test.equal(p.children.length, 0);                                                                               // 166
  })();                                                                                                             // 167
                                                                                                                    // 168
  (function () {                                                                                                    // 169
    var p = HTMLTools.parseFragment('<p>x</p>');                                                                    // 170
    test.equal(p.tagName, 'p');                                                                                     // 171
    test.equal(p.attrs, null);                                                                                      // 172
    test.isTrue(p instanceof HTML.Tag);                                                                             // 173
    test.equal(p.children.length, 1);                                                                               // 174
    test.equal(p.children[0], 'x');                                                                                 // 175
  })();                                                                                                             // 176
                                                                                                                    // 177
  (function () {                                                                                                    // 178
    var p = HTMLTools.parseFragment('<p>x&#65;</p>');                                                               // 179
    test.equal(p.tagName, 'p');                                                                                     // 180
    test.equal(p.attrs, null);                                                                                      // 181
    test.isTrue(p instanceof HTML.Tag);                                                                             // 182
    test.equal(p.children.length, 2);                                                                               // 183
    test.equal(p.children[0], 'x');                                                                                 // 184
                                                                                                                    // 185
    test.isTrue(p.children[1] instanceof HTML.CharRef);                                                             // 186
    test.equal(p.children[1].html, '&#65;');                                                                        // 187
    test.equal(p.children[1].str, 'A');                                                                             // 188
  })();                                                                                                             // 189
                                                                                                                    // 190
  (function () {                                                                                                    // 191
    var pp = HTMLTools.parseFragment('<p>x</p><p>y</p>');                                                           // 192
    test.isTrue(pp instanceof Array);                                                                               // 193
    test.equal(pp.length, 2);                                                                                       // 194
                                                                                                                    // 195
    test.equal(pp[0].tagName, 'p');                                                                                 // 196
    test.equal(pp[0].attrs, null);                                                                                  // 197
    test.isTrue(pp[0] instanceof HTML.Tag);                                                                         // 198
    test.equal(pp[0].children.length, 1);                                                                           // 199
    test.equal(pp[0].children[0], 'x');                                                                             // 200
                                                                                                                    // 201
    test.equal(pp[1].tagName, 'p');                                                                                 // 202
    test.equal(pp[1].attrs, null);                                                                                  // 203
    test.isTrue(pp[1] instanceof HTML.Tag);                                                                         // 204
    test.equal(pp[1].children.length, 1);                                                                           // 205
    test.equal(pp[1].children[0], 'y');                                                                             // 206
  })();                                                                                                             // 207
                                                                                                                    // 208
  var scanner = new Scanner('asdf');                                                                                // 209
  scanner.pos = 1;                                                                                                  // 210
  test.equal(HTMLTools.parseFragment(scanner), 'sdf');                                                              // 211
                                                                                                                    // 212
  test.throws(function () {                                                                                         // 213
    var scanner = new Scanner('asdf</p>');                                                                          // 214
    scanner.pos = 1;                                                                                                // 215
    HTMLTools.parseFragment(scanner);                                                                               // 216
  });                                                                                                               // 217
});                                                                                                                 // 218
                                                                                                                    // 219
Tinytest.add("html-tools - getSpecialTag", function (test) {                                                        // 220
                                                                                                                    // 221
  // match a simple tag consisting of `{{`, an optional `!`, one                                                    // 222
  // or more ASCII letters, spaces or html tags, and a closing `}}`.                                                // 223
  var mustache = /^\{\{(!?[a-zA-Z 0-9</>]+)\}\}/;                                                                   // 224
                                                                                                                    // 225
  // This implementation of `getSpecialTag` looks for "{{" and if it                                                // 226
  // finds it, it will match the regex above or fail fatally trying.                                                // 227
  // The object it returns is opaque to the tokenizer/parser and can                                                // 228
  // be anything we want.                                                                                           // 229
  var getSpecialTag = function (scanner, position) {                                                                // 230
    if (! (scanner.peek() === '{' && // one-char peek is just an optimization                                       // 231
           scanner.rest().slice(0, 2) === '{{'))                                                                    // 232
      return null;                                                                                                  // 233
                                                                                                                    // 234
    var match = mustache.exec(scanner.rest());                                                                      // 235
    if (! match)                                                                                                    // 236
      scanner.fatal("Bad mustache");                                                                                // 237
                                                                                                                    // 238
    scanner.pos += match[0].length;                                                                                 // 239
                                                                                                                    // 240
    if (match[1].charAt(0) === '!')                                                                                 // 241
      return null; // `{{!foo}}` is like a comment                                                                  // 242
                                                                                                                    // 243
    return { stuff: match[1] };                                                                                     // 244
  };                                                                                                                // 245
                                                                                                                    // 246
                                                                                                                    // 247
                                                                                                                    // 248
  var succeed = function (input, expected) {                                                                        // 249
    var endPos = input.indexOf('^^^');                                                                              // 250
    if (endPos < 0)                                                                                                 // 251
      endPos = input.length;                                                                                        // 252
                                                                                                                    // 253
    var scanner = new Scanner(input.replace('^^^', ''));                                                            // 254
    scanner.getSpecialTag = getSpecialTag;                                                                          // 255
    var result;                                                                                                     // 256
    try {                                                                                                           // 257
      result = getContent(scanner);                                                                                 // 258
    } catch (e) {                                                                                                   // 259
      result = String(e);                                                                                           // 260
    }                                                                                                               // 261
    test.equal(scanner.pos, endPos);                                                                                // 262
    test.equal(HTML.toJS(result), HTML.toJS(expected));                                                             // 263
  };                                                                                                                // 264
                                                                                                                    // 265
  var fatal = function (input, messageContains) {                                                                   // 266
    var scanner = new Scanner(input);                                                                               // 267
    scanner.getSpecialTag = getSpecialTag;                                                                          // 268
    var error;                                                                                                      // 269
    try {                                                                                                           // 270
      getContent(scanner);                                                                                          // 271
    } catch (e) {                                                                                                   // 272
      error = e;                                                                                                    // 273
    }                                                                                                               // 274
    test.isTrue(error);                                                                                             // 275
    if (messageContains)                                                                                            // 276
      test.isTrue(messageContains && error.message.indexOf(messageContains) >= 0, error.message);                   // 277
  };                                                                                                                // 278
                                                                                                                    // 279
                                                                                                                    // 280
  succeed('{{foo}}', Special({stuff: 'foo'}));                                                                      // 281
                                                                                                                    // 282
  succeed('<a href=http://www.apple.com/>{{foo}}</a>',                                                              // 283
          A({href: "http://www.apple.com/"}, Special({stuff: 'foo'})));                                             // 284
                                                                                                                    // 285
  // tags not parsed in comments                                                                                    // 286
  succeed('<!--{{foo}}-->', Comment("{{foo}}"));                                                                    // 287
  succeed('<!--{{foo-->', Comment("{{foo"));                                                                        // 288
                                                                                                                    // 289
  succeed('&am{{foo}}p;', ['&am', Special({stuff: 'foo'}), 'p;']);                                                  // 290
                                                                                                                    // 291
  // can't start a mustache and not finish it                                                                       // 292
  fatal('{{foo');                                                                                                   // 293
  fatal('<a>{{</a>');                                                                                               // 294
                                                                                                                    // 295
  // no mustache allowed in tag name                                                                                // 296
  fatal('<{{a}}>');                                                                                                 // 297
  fatal('<{{a}}b>');                                                                                                // 298
  fatal('<a{{b}}>');                                                                                                // 299
                                                                                                                    // 300
  // single curly brace is no biggie                                                                                // 301
  succeed('a{b', 'a{b');                                                                                            // 302
  succeed('<br x={ />', BR({x:'{'}));                                                                               // 303
  succeed('<br x={foo} />', BR({x:'{foo}'}));                                                                       // 304
                                                                                                                    // 305
  succeed('<br {{x}}>', BR({$specials: [Special({stuff: 'x'})]}));                                                  // 306
  succeed('<br {{x}} {{y}}>', BR({$specials: [Special({stuff: 'x'}),                                                // 307
                                              Special({stuff: 'y'})]}));                                            // 308
  succeed('<br {{x}} y>', BR({$specials: [Special({stuff: 'x'})], y:''}));                                          // 309
  fatal('<br {{x}}y>');                                                                                             // 310
  fatal('<br {{x}}=y>');                                                                                            // 311
  succeed('<br x={{y}} z>', BR({x: Special({stuff: 'y'}), z: ''}));                                                 // 312
  succeed('<br x=y{{z}}w>', BR({x: ['y', Special({stuff: 'z'}), 'w']}));                                            // 313
  succeed('<br x="y{{z}}w">', BR({x: ['y', Special({stuff: 'z'}), 'w']}));                                          // 314
  succeed('<br x="y {{z}}{{w}} v">', BR({x: ['y ', Special({stuff: 'z'}),                                           // 315
                                             Special({stuff: 'w'}), ' v']}));                                       // 316
  // Slash is parsed as part of unquoted attribute!  This is consistent with                                        // 317
  // the HTML tokenization spec.  It seems odd for some inputs but is probably                                      // 318
  // for cases like `<a href=http://foo.com/>` or `<a href=/foo/>`.                                                 // 319
  succeed('<br x={{y}}/>', BR({x: [Special({stuff: 'y'}), '/']}));                                                  // 320
  succeed('<br x={{z}}{{w}}>', BR({x: [Special({stuff: 'z'}),                                                       // 321
                                       Special({stuff: 'w'})]}));                                                   // 322
  fatal('<br x="y"{{z}}>');                                                                                         // 323
                                                                                                                    // 324
  succeed('<br x=&amp;>', BR({x:CharRef({html: '&amp;', str: '&'})}));                                              // 325
                                                                                                                    // 326
                                                                                                                    // 327
  // check tokenization of stache tags with spaces                                                                  // 328
  succeed('<br {{x 1}}>', BR({$specials: [Special({stuff: 'x 1'})]}));                                              // 329
  succeed('<br {{x 1}} {{y 2}}>', BR({$specials: [Special({stuff: 'x 1'}),                                          // 330
                                                  Special({stuff: 'y 2'})]}));                                      // 331
  succeed('<br {{x 1}} y>', BR({$specials: [Special({stuff: 'x 1'})], y:''}));                                      // 332
  fatal('<br {{x 1}}y>');                                                                                           // 333
  fatal('<br {{x 1}}=y>');                                                                                          // 334
  succeed('<br x={{y 2}} z>', BR({x: Special({stuff: 'y 2'}), z: ''}));                                             // 335
  succeed('<br x=y{{z 3}}w>', BR({x: ['y', Special({stuff: 'z 3'}), 'w']}));                                        // 336
  succeed('<br x="y{{z 3}}w">', BR({x: ['y', Special({stuff: 'z 3'}), 'w']}));                                      // 337
  succeed('<br x="y {{z 3}}{{w 4}} v">', BR({x: ['y ', Special({stuff: 'z 3'}),                                     // 338
                                                 Special({stuff: 'w 4'}), ' v']}));                                 // 339
  succeed('<br x={{y 2}}/>', BR({x: [Special({stuff: 'y 2'}), '/']}));                                              // 340
  succeed('<br x={{z 3}}{{w 4}}>', BR({x: [Special({stuff: 'z 3'}),                                                 // 341
                                           Special({stuff: 'w 4'})]}));                                             // 342
                                                                                                                    // 343
  succeed('<p></p>', P());                                                                                          // 344
                                                                                                                    // 345
  succeed('x{{foo}}{{bar}}y', ['x', Special({stuff: 'foo'}),                                                        // 346
                               Special({stuff: 'bar'}), 'y']);                                                      // 347
  succeed('x{{!foo}}{{!bar}}y', 'xy');                                                                              // 348
  succeed('x{{!foo}}{{bar}}y', ['x', Special({stuff: 'bar'}), 'y']);                                                // 349
  succeed('x{{foo}}{{!bar}}y', ['x', Special({stuff: 'foo'}), 'y']);                                                // 350
  succeed('<div>{{!foo}}{{!bar}}</div>', DIV());                                                                    // 351
  succeed('<div>{{!foo}}<br />{{!bar}}</div>', DIV(BR()));                                                          // 352
  succeed('<div> {{!foo}} {{!bar}} </div>', DIV("   "));                                                            // 353
  succeed('<div> {{!foo}} <br /> {{!bar}}</div>', DIV("  ", BR(), " "));                                            // 354
  succeed('{{! <div></div> }}', null);                                                                              // 355
  succeed('{{!<div></div>}}', null);                                                                                // 356
                                                                                                                    // 357
  succeed('', null);                                                                                                // 358
  succeed('{{!foo}}', null);                                                                                        // 359
});                                                                                                                 // 360
                                                                                                                    // 361
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
