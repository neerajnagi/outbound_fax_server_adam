(function () {

(function () {

//////////////////////////////////////////////////////////////////////////////
//                                                                          //
// plugin/compile-stylus.js                                                 //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
                                                                            //
var fs = Npm.require('fs');                                                 // 1
var stylus = Npm.require('stylus');                                         // 2
var nib = Npm.require('nib');                                               // 3
var path = Npm.require('path');                                             // 4
var Future = Npm.require('fibers/future');                                  // 5
                                                                            // 6
Plugin.registerSourceHandler("styl", function (compileStep) {               // 7
  // XXX annoying that this is replicated in .css, .less, and .styl         // 8
  if (! compileStep.archMatches('browser')) {                               // 9
    // XXX in the future, might be better to emit some kind of a            // 10
    // warning if a stylesheet is included on the server, rather than       // 11
    // silently ignoring it. but that would mean you can't stick .css       // 12
    // at the top level of your app, which is kind of silly.                // 13
    return;                                                                 // 14
  }                                                                         // 15
                                                                            // 16
  var f = new Future;                                                       // 17
  stylus(compileStep.read().toString('utf8'))                               // 18
    .use(nib())                                                             // 19
    .set('filename', compileStep.inputPath)                                 // 20
    // Include needed to allow relative @imports in stylus files            // 21
    .include(path.dirname(compileStep._fullInputPath))                      // 22
    .render(f.resolver());                                                  // 23
                                                                            // 24
  try {                                                                     // 25
    var css = f.wait();                                                     // 26
  } catch (e) {                                                             // 27
    compileStep.error({                                                     // 28
      message: "Stylus compiler error: " + e.message                        // 29
    });                                                                     // 30
    return;                                                                 // 31
  }                                                                         // 32
  compileStep.addStylesheet({                                               // 33
    path: compileStep.inputPath + ".css",                                   // 34
    data: css                                                               // 35
  });                                                                       // 36
});                                                                         // 37
                                                                            // 38
// Register import.styl files with the dependency watcher, without actually // 39
// processing them. There is a similar rule in the less package.            // 40
Plugin.registerSourceHandler("import.styl", function () {                   // 41
  // Do nothing                                                             // 42
});                                                                         // 43
                                                                            // 44
                                                                            // 45
//////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.compileStylus = {};

})();
