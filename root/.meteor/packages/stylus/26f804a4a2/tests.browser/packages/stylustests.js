(function () {

//////////////////////////////////////////////////////////////////////////////////
//                                                                              //
// packages/stylus/stylus_tests.js                                              //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////
                                                                                //
                                                                                // 1
Tinytest.add("stylus - presence", function(test) {                              // 2
  var d = OnscreenDiv(Meteor.render(function() {                                // 3
    return '<p class="stylus-dashy-left-border"></p>'; }));                     // 4
  d.node().style.display = 'block';                                             // 5
                                                                                // 6
  var p = d.node().firstChild;                                                  // 7
  var leftBorder = getStyleProperty(p, 'border-left-style');                    // 8
  test.equal(leftBorder, "dashed");                                             // 9
                                                                                // 10
  d.kill();                                                                     // 11
});                                                                             // 12
                                                                                // 13
Tinytest.add("stylus - @import", function(test) {                               // 14
  var d = OnscreenDiv(Meteor.render(function() {                                // 15
    return '<p class="stylus-import-dashy-border stylus-overwrite-color"></p>'; // 16
  }));                                                                          // 17
  d.node().style.display = 'block';                                             // 18
                                                                                // 19
  var p = d.node().firstChild;                                                  // 20
  test.equal(getStyleProperty(p, 'font-size'), "20px");                         // 21
  test.equal(getStyleProperty(p, 'border-left-style'), "dashed");               // 22
                                                                                // 23
  d.kill();                                                                     // 24
});                                                                             // 25
                                                                                // 26
//////////////////////////////////////////////////////////////////////////////////

}).call(this);
