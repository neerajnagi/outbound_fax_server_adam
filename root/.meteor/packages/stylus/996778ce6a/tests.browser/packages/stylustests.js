(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/stylus/template.stylus_tests.js                                             //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
                                                                                        // 1
Template.__define__("stylus_test_presence", (function() {                               // 2
  var self = this;                                                                      // 3
  var template = this;                                                                  // 4
  return HTML.Raw('<p class="stylus-dashy-left-border"></p>');                          // 5
}));                                                                                    // 6
                                                                                        // 7
Template.__define__("stylus_test_import", (function() {                                 // 8
  var self = this;                                                                      // 9
  var template = this;                                                                  // 10
  return HTML.Raw('<p class="stylus-import-dashy-border stylus-overwrite-color"></p>'); // 11
}));                                                                                    // 12
                                                                                        // 13
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/stylus/stylus_tests.js                                                      //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
                                                                                        // 1
Tinytest.add("stylus - presence", function(test) {                                      // 2
                                                                                        // 3
  var div = document.createElement('div');                                              // 4
  UI.materialize(Template.stylus_test_presence, div);                                   // 5
  div.style.display = 'block';                                                          // 6
  document.body.appendChild(div);                                                       // 7
                                                                                        // 8
  var p = div.querySelector('p');                                                       // 9
  var leftBorder = getStyleProperty(p, 'border-left-style');                            // 10
  test.equal(leftBorder, "dashed");                                                     // 11
                                                                                        // 12
  document.body.removeChild(div);                                                       // 13
});                                                                                     // 14
                                                                                        // 15
Tinytest.add("stylus - @import", function(test) {                                       // 16
  var div = document.createElement('div');                                              // 17
  UI.materialize(Template.stylus_test_import, div);                                     // 18
  div.style.display = 'block';                                                          // 19
  document.body.appendChild(div);                                                       // 20
                                                                                        // 21
  var p = div.querySelector('p');                                                       // 22
  test.equal(getStyleProperty(p, 'font-size'), "20px");                                 // 23
  test.equal(getStyleProperty(p, 'border-left-style'), "dashed");                       // 24
                                                                                        // 25
  document.body.removeChild(div);                                                       // 26
});                                                                                     // 27
                                                                                        // 28
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
