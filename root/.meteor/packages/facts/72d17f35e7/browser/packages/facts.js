(function () {

/////////////////////////////////////////////////////////////////////////////////
//                                                                             //
// packages/facts/template.facts.js                                            //
//                                                                             //
/////////////////////////////////////////////////////////////////////////////////
                                                                               //
                                                                               // 1
Template.__define__("serverFacts", (function() {                               // 2
  var self = this;                                                             // 3
  var template = this;                                                         // 4
  return HTML.UL("\n    ", UI.Each(function() {                                // 5
    return Spacebars.call(self.lookup("factsByPackage"));                      // 6
  }, UI.block(function() {                                                     // 7
    var self = this;                                                           // 8
    return [ "\n      ", HTML.LI(function() {                                  // 9
      return Spacebars.mustache(self.lookup("_id"));                           // 10
    }, "\n        ", HTML.DL("\n          ", UI.Each(function() {              // 11
      return Spacebars.call(self.lookup("facts"));                             // 12
    }, UI.block(function() {                                                   // 13
      var self = this;                                                         // 14
      return [ "\n            ", HTML.DT(function() {                          // 15
        return Spacebars.mustache(self.lookup("name"));                        // 16
      }), "\n            ", HTML.DD(function() {                               // 17
        return Spacebars.mustache(self.lookup("value"));                       // 18
      }), "\n          " ];                                                    // 19
    })), "\n        "), "\n      "), "\n    " ];                               // 20
  })), "\n  ");                                                                // 21
}));                                                                           // 22
                                                                               // 23
/////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////
//                                                                             //
// packages/facts/facts.js                                                     //
//                                                                             //
/////////////////////////////////////////////////////////////////////////////////
                                                                               //
Facts = {};                                                                    // 1
                                                                               // 2
var serverFactsCollection = 'meteor_Facts_server';                             // 3
                                                                               // 4
if (Meteor.isServer) {                                                         // 5
  // By default, we publish facts to no user if autopublish is off, and to all // 6
  // users if autopublish is on.                                               // 7
  var userIdFilter = function (userId) {                                       // 8
    return !!Package.autopublish;                                              // 9
  };                                                                           // 10
                                                                               // 11
  // XXX make this take effect at runtime too?                                 // 12
  Facts.setUserIdFilter = function (filter) {                                  // 13
    userIdFilter = filter;                                                     // 14
  };                                                                           // 15
                                                                               // 16
  // XXX Use a minimongo collection instead and hook up an observeChanges      // 17
  // directly to a publish.                                                    // 18
  var factsByPackage = {};                                                     // 19
  var activeSubscriptions = [];                                                // 20
                                                                               // 21
  // Make factsByPackage data available to the server environment              // 22
  Facts._factsByPackage = factsByPackage;                                      // 23
                                                                               // 24
  Facts.incrementServerFact = function (pkg, fact, increment) {                // 25
    if (!_.has(factsByPackage, pkg)) {                                         // 26
      factsByPackage[pkg] = {};                                                // 27
      factsByPackage[pkg][fact] = increment;                                   // 28
      _.each(activeSubscriptions, function (sub) {                             // 29
        sub.added(serverFactsCollection, pkg, factsByPackage[pkg]);            // 30
      });                                                                      // 31
      return;                                                                  // 32
    }                                                                          // 33
                                                                               // 34
    var packageFacts = factsByPackage[pkg];                                    // 35
    if (!_.has(packageFacts, fact))                                            // 36
      factsByPackage[pkg][fact] = 0;                                           // 37
    factsByPackage[pkg][fact] += increment;                                    // 38
    var changedField = {};                                                     // 39
    changedField[fact] = factsByPackage[pkg][fact];                            // 40
    _.each(activeSubscriptions, function (sub) {                               // 41
      sub.changed(serverFactsCollection, pkg, changedField);                   // 42
    });                                                                        // 43
  };                                                                           // 44
                                                                               // 45
  // Deferred, because we have an unordered dependency on livedata.            // 46
  // XXX is this safe? could somebody try to connect before Meteor.publish is  // 47
  // called?                                                                   // 48
  Meteor.defer(function () {                                                   // 49
    // XXX Also publish facts-by-package.                                      // 50
    Meteor.publish("meteor_facts", function () {                               // 51
      var sub = this;                                                          // 52
      if (!userIdFilter(this.userId)) {                                        // 53
        sub.ready();                                                           // 54
        return;                                                                // 55
      }                                                                        // 56
      activeSubscriptions.push(sub);                                           // 57
      _.each(factsByPackage, function (facts, pkg) {                           // 58
        sub.added(serverFactsCollection, pkg, facts);                          // 59
      });                                                                      // 60
      sub.onStop(function () {                                                 // 61
        activeSubscriptions = _.without(activeSubscriptions, sub);             // 62
      });                                                                      // 63
      sub.ready();                                                             // 64
    }, {is_auto: true});                                                       // 65
  });                                                                          // 66
} else {                                                                       // 67
  Facts.server = new Meteor.Collection(serverFactsCollection);                 // 68
                                                                               // 69
  Template.serverFacts.factsByPackage = function () {                          // 70
    return Facts.server.find();                                                // 71
  };                                                                           // 72
  Template.serverFacts.facts = function () {                                   // 73
    var factArray = [];                                                        // 74
    _.each(this, function (value, name) {                                      // 75
      if (name !== '_id')                                                      // 76
        factArray.push({name: name, value: value});                            // 77
    });                                                                        // 78
    return factArray;                                                          // 79
  };                                                                           // 80
                                                                               // 81
  // Subscribe when the template is first made, and unsubscribe when it        // 82
  // is removed. If for some reason puts two copies of the template on         // 83
  // the screen at once, we'll subscribe twice. Meh.                           // 84
  Template.serverFacts.created = function () {                                 // 85
    this._stopHandle = Meteor.subscribe("meteor_facts");                       // 86
  };                                                                           // 87
  Template.serverFacts.destroyed = function () {                               // 88
    if (this._stopHandle) {                                                    // 89
      this._stopHandle.stop();                                                 // 90
      this._stopHandle = null;                                                 // 91
    }                                                                          // 92
  };                                                                           // 93
}                                                                              // 94
                                                                               // 95
/////////////////////////////////////////////////////////////////////////////////

}).call(this);
