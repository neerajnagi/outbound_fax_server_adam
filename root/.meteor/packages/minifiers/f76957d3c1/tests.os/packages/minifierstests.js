(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                      //
// packages/minifiers/minifiers-tests.js                                                                //
//                                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                        //
Tinytest.add("minifiers - simple css minification", function (test) {                                   // 1
  var t = function (css, expected, desc) {                                                              // 2
    test.equal(CssTools.minifyCss(css), expected, desc);                                                // 3
  }                                                                                                     // 4
                                                                                                        // 5
  t('a \t\n{ color: red } \n', 'a{color:red}', 'whitespace check');                                     // 6
  t('a \t\n{ color: red; margin: 1; } \n', 'a{color:red;margin:1}', 'only last one loses semicolon');   // 7
  t('a \t\n{ color: red;;; margin: 1;;; } \n', 'a{color:red;margin:1}', 'more semicolons than needed'); // 8
  t('a , p \t\n{ color: red; } \n', 'a,p{color:red}', 'multiple selectors');                            // 9
  t('body {}', '', 'removing empty rules');                                                             // 10
  t('*.my-class { color: #fff; }', '.my-class{color:#fff}', 'removing universal selector');             // 11
  t('p > *.my-class { color: #fff; }', 'p>.my-class{color:#fff}', 'removing optional whitespace around ">" in selector');
  t('p +  *.my-class { color: #fff; }', 'p+.my-class{color:#fff}', 'removing optional whitespace around "+" in selector');
  // XXX url parsing is difficult to support at the moment                                              // 14
  t('a {\n\
  font:12px \'Helvetica\',"Arial",\'Nautica\';\n\
  background:url("/some/nice/picture.png");\n}',                                                        // 17
  'a{font:12px Helvetica,Arial,Nautica;background:url("/some/nice/picture.png")}',  'removing quotes in font and url (if possible)');
  t('/* no comments */ a { color: red; }', 'a{color:red}', 'remove comments');                          // 19
});                                                                                                     // 20
                                                                                                        // 21
                                                                                                        // 22
//////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
