(function () {

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// packages/minifiers/beautify_tests.js                                      //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
                                                                             //
                                                                             // 1
// The UglifyJSMinify API can also be used for beautification.  Test that it // 2
// behaves as expected.                                                      // 3
                                                                             // 4
Tinytest.add('minifiers - uglify beautify', function (test) {                // 5
  // See <https://github.com/mishoo/UglifyJS2#the-simple-way> and            // 6
  // <http://lisperator.net/uglifyjs/codegen> for the API we're calling.     // 7
  test.equal(UglifyJSMinify('one = function () { return 1; };',              // 8
                            { fromString: true,                              // 9
                              output: { beautify: true,                      // 10
                                        indent_level: 2,                     // 11
                                        width: 80 } }).code,                 // 12
             'one = function() {\n' +                                        // 13
             '  return 1;\n' +                                               // 14
             '};');                                                          // 15
});                                                                          // 16
                                                                             // 17
///////////////////////////////////////////////////////////////////////////////

}).call(this);
