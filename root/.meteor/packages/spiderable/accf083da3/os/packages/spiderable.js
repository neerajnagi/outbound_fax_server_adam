(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/spiderable/spiderable.js                                                                               //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var fs = Npm.require('fs');                                                                                        // 1
var child_process = Npm.require('child_process');                                                                  // 2
var querystring = Npm.require('querystring');                                                                      // 3
var urlParser = Npm.require('url');                                                                                // 4
                                                                                                                   // 5
Spiderable = {};                                                                                                   // 6
                                                                                                                   // 7
// list of bot user agents that we want to serve statically, but do                                                // 8
// not obey the _escaped_fragment_ protocol. The page is served                                                    // 9
// statically to any client whos user agent matches any of these                                                   // 10
// regexps. Users may modify this array.                                                                           // 11
//                                                                                                                 // 12
// An original goal with the spiderable package was to avoid doing                                                 // 13
// user-agent based tests. But the reality is not enough bots support                                              // 14
// the _escaped_fragment_ protocol, so we need to hardcode a list                                                  // 15
// here. I shed a silent tear.                                                                                     // 16
Spiderable.userAgentRegExps = [                                                                                    // 17
    /^facebookexternalhit/i, /^linkedinbot/i, /^twitterbot/i];                                                     // 18
                                                                                                                   // 19
// how long to let phantomjs run before we kill it                                                                 // 20
var REQUEST_TIMEOUT = 15*1000;                                                                                     // 21
// maximum size of result HTML. node's default is 200k which is too                                                // 22
// small for our docs.                                                                                             // 23
var MAX_BUFFER = 5*1024*1024; // 5MB                                                                               // 24
                                                                                                                   // 25
WebApp.connectHandlers.use(function (req, res, next) {                                                             // 26
  // _escaped_fragment_ comes from Google's AJAX crawling spec:                                                    // 27
  // https://developers.google.com/webmasters/ajax-crawling/docs/specification                                     // 28
  // This spec was designed during the brief era where using "#!" URLs was                                         // 29
  // common, so it mostly describes how to translate "#!" URLs into                                                // 30
  // _escaped_fragment_ URLs. Since then, "#!" URLs have gone out of style, but                                    // 31
  // the <meta name="fragment" content="!"> (see spiderable.html) approach also                                    // 32
  // described in the spec is still common and used by several crawlers.                                           // 33
  if (/\?.*_escaped_fragment_=/.test(req.url) ||                                                                   // 34
      _.any(Spiderable.userAgentRegExps, function (re) {                                                           // 35
        return re.test(req.headers['user-agent']); })) {                                                           // 36
                                                                                                                   // 37
    // reassembling url without escaped fragment if exists                                                         // 38
    var parsedUrl = urlParser.parse(req.url);                                                                      // 39
    var parsedQuery = querystring.parse(parsedUrl.query);                                                          // 40
    delete parsedQuery['_escaped_fragment_'];                                                                      // 41
    var newQuery = querystring.stringify(parsedQuery);                                                             // 42
    var newPath = parsedUrl.pathname + (newQuery ? ('?' + newQuery) : '');                                         // 43
    var url = "http://" + req.headers.host + newPath;                                                              // 44
                                                                                                                   // 45
    // This string is going to be put into a bash script, so it's important                                        // 46
    // that 'url' (which comes from the network) can neither exploit phantomjs                                     // 47
    // or the bash script. JSON stringification should prevent it from                                             // 48
    // exploiting phantomjs, and since the output of JSON.stringify shouldn't                                      // 49
    // be able to contain newlines, it should be unable to exploit bash as                                         // 50
    // well.                                                                                                       // 51
    var phantomScript = "var url = " + JSON.stringify(url) + ";" +                                                 // 52
          "var page = require('webpage').create();" +                                                              // 53
          "page.open(url);" +                                                                                      // 54
          "setInterval(function() {" +                                                                             // 55
          "  var ready = page.evaluate(function () {" +                                                            // 56
          "    if (typeof Meteor !== 'undefined' " +                                                               // 57
          "        && typeof(Meteor.status) !== 'undefined' " +                                                    // 58
          "        && Meteor.status().connected) {" +                                                              // 59
          "      Deps.flush();" +                                                                                  // 60
          "      return DDP._allSubscriptionsReady();" +                                                           // 61
          "    }" +                                                                                                // 62
          "    return false;" +                                                                                    // 63
          "  });" +                                                                                                // 64
          "  if (ready) {" +                                                                                       // 65
          "    var out = page.content;" +                                                                          // 66
          "    out = out.replace(/<script[^>]+>(.|\\n|\\r)*?<\\/script\\s*>/ig, '');" +                            // 67
          "    out = out.replace('<meta name=\"fragment\" content=\"!\">', '');" +                                 // 68
          "    console.log(out);" +                                                                                // 69
          "    phantom.exit();" +                                                                                  // 70
          "  }" +                                                                                                  // 71
          "}, 100);\n";                                                                                            // 72
                                                                                                                   // 73
    // Run phantomjs.                                                                                              // 74
    //                                                                                                             // 75
    // Use '/dev/stdin' to avoid writing to a temporary file. We can't                                             // 76
    // just omit the file, as PhantomJS takes that to mean 'use a                                                  // 77
    // REPL' and exits as soon as stdin closes.                                                                    // 78
    //                                                                                                             // 79
    // However, Node 0.8 broke the ability to open /dev/stdin in the                                               // 80
    // subprocess, so we can't just write our string to the process's stdin                                        // 81
    // directly; see https://gist.github.com/3751746 for the gory details. We                                      // 82
    // work around this with a bash heredoc. (We previous used a "cat |"                                           // 83
    // instead, but that meant we couldn't use exec and had to manage several                                      // 84
    // processes.)                                                                                                 // 85
    child_process.execFile(                                                                                        // 86
      '/bin/bash',                                                                                                 // 87
      ['-c',                                                                                                       // 88
       ("exec phantomjs --load-images=no /dev/stdin <<'END'\n" +                                                   // 89
        phantomScript + "END\n")],                                                                                 // 90
      {timeout: REQUEST_TIMEOUT, maxBuffer: MAX_BUFFER},                                                           // 91
      function (error, stdout, stderr) {                                                                           // 92
        if (!error && /<html/i.test(stdout)) {                                                                     // 93
          res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});                                        // 94
          res.end(stdout);                                                                                         // 95
        } else {                                                                                                   // 96
          // phantomjs failed. Don't send the error, instead send the                                              // 97
          // normal page.                                                                                          // 98
          if (error && error.code === 127)                                                                         // 99
            Meteor._debug("spiderable: phantomjs not installed. Download and install from http://phantomjs.org/"); // 100
          else                                                                                                     // 101
            Meteor._debug("spiderable: phantomjs failed:", error, "\nstderr:", stderr);                            // 102
                                                                                                                   // 103
          next();                                                                                                  // 104
        }                                                                                                          // 105
      });                                                                                                          // 106
  } else {                                                                                                         // 107
    next();                                                                                                        // 108
  }                                                                                                                // 109
});                                                                                                                // 110
                                                                                                                   // 111
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
