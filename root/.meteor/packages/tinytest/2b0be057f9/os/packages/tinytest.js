(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                 //
// packages/tinytest/tinytest.js                                                                   //
//                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                   //
/******************************************************************************/                   // 1
/* TestCaseResults                                                            */                   // 2
/******************************************************************************/                   // 3
                                                                                                   // 4
TestCaseResults = function (test_case, onEvent, onException, stop_at_offset) {                     // 5
  var self = this;                                                                                 // 6
  self.test_case = test_case;                                                                      // 7
  self.onEvent = onEvent;                                                                          // 8
  self.expecting_failure = false;                                                                  // 9
  self.current_fail_count = 0;                                                                     // 10
  self.stop_at_offset = stop_at_offset;                                                            // 11
  self.onException = onException;                                                                  // 12
  self.id = Random.id();                                                                           // 13
};                                                                                                 // 14
                                                                                                   // 15
_.extend(TestCaseResults.prototype, {                                                              // 16
  ok: function (doc) {                                                                             // 17
    var self = this;                                                                               // 18
    var ok = {type: "ok"};                                                                         // 19
    if (doc)                                                                                       // 20
      ok.details = doc;                                                                            // 21
    if (self.expecting_failure) {                                                                  // 22
      ok.details = ok.details || {};                                                               // 23
      ok.details["was_expecting_failure"] = true;                                                  // 24
      self.expecting_failure = false;                                                              // 25
    }                                                                                              // 26
    self.onEvent(ok);                                                                              // 27
  },                                                                                               // 28
                                                                                                   // 29
  expect_fail: function () {                                                                       // 30
    var self = this;                                                                               // 31
    self.expecting_failure = true;                                                                 // 32
  },                                                                                               // 33
                                                                                                   // 34
  fail: function (doc) {                                                                           // 35
    var self = this;                                                                               // 36
                                                                                                   // 37
    if (typeof doc === "string") {                                                                 // 38
      // Some very old code still tries to call fail() with a                                      // 39
      // string. Don't do this!                                                                    // 40
      doc = { type: "fail", message: doc };                                                        // 41
    }                                                                                              // 42
                                                                                                   // 43
    if (self.stop_at_offset === 0) {                                                               // 44
      if (Meteor.isClient) {                                                                       // 45
        // Only supported on the browser for now..                                                 // 46
        var now = (+new Date);                                                                     // 47
        debugger;                                                                                  // 48
        if ((+new Date) - now < 100)                                                               // 49
          alert("To use this feature, first enable your browser's debugger.");                     // 50
      }                                                                                            // 51
      self.stop_at_offset = null;                                                                  // 52
    }                                                                                              // 53
    if (self.stop_at_offset)                                                                       // 54
      self.stop_at_offset--;                                                                       // 55
                                                                                                   // 56
    // Get filename and line number of failure if we're using v8 (Chrome or                        // 57
    // Node).                                                                                      // 58
    if (Error.captureStackTrace) {                                                                 // 59
      var savedPrepareStackTrace = Error.prepareStackTrace;                                        // 60
      Error.prepareStackTrace = function(_, stack){ return stack; };                               // 61
      var err = new Error;                                                                         // 62
      Error.captureStackTrace(err);                                                                // 63
      var stack = err.stack;                                                                       // 64
      Error.prepareStackTrace = savedPrepareStackTrace;                                            // 65
      for (var i = stack.length - 1; i >= 0; --i) {                                                // 66
        var frame = stack[i];                                                                      // 67
        // Heuristic: use the OUTERMOST line which is in a :tests.js                               // 68
        // file (this is less likely to be a test helper function).                                // 69
        if (frame.getFileName().match(/:tests\.js/)) {                                             // 70
          doc.filename = frame.getFileName();                                                      // 71
          doc.line = frame.getLineNumber();                                                        // 72
          break;                                                                                   // 73
        }                                                                                          // 74
      }                                                                                            // 75
    }                                                                                              // 76
                                                                                                   // 77
    self.onEvent({                                                                                 // 78
        type: (self.expecting_failure ? "expected_fail" : "fail"),                                 // 79
        details: doc,                                                                              // 80
        cookie: {name: self.test_case.name, offset: self.current_fail_count,                       // 81
                 groupPath: self.test_case.groupPath,                                              // 82
                 shortName: self.test_case.shortName}                                              // 83
    });                                                                                            // 84
    self.expecting_failure = false;                                                                // 85
    self.current_fail_count++;                                                                     // 86
  },                                                                                               // 87
                                                                                                   // 88
  // Call this to fail the test with an exception. Use this to record                              // 89
  // exceptions that occur inside asynchronous callbacks in tests.                                 // 90
  //                                                                                               // 91
  // It should only be used with asynchronous tests, and if you call                               // 92
  // this function, you should make sure that (1) the test doesn't                                 // 93
  // call its callback (onComplete function); (2) the test function                                // 94
  // doesn't directly raise an exception.                                                          // 95
  exception: function (exception) {                                                                // 96
    this.onException(exception);                                                                   // 97
  },                                                                                               // 98
                                                                                                   // 99
  // returns a unique ID for this test run, for convenience use by                                 // 100
  // your tests                                                                                    // 101
  runId: function () {                                                                             // 102
    return this.id;                                                                                // 103
  },                                                                                               // 104
                                                                                                   // 105
  // === Following patterned after http://vowsjs.org/#reference ===                                // 106
                                                                                                   // 107
  // XXX eliminate 'message' and 'not' arguments                                                   // 108
  equal: function (actual, expected, message, not) {                                               // 109
                                                                                                   // 110
    if ((! not) && (typeof actual === 'string') &&                                                 // 111
        (typeof expected === 'string')) {                                                          // 112
      this._stringEqual(actual, expected, message);                                                // 113
      return;                                                                                      // 114
    }                                                                                              // 115
                                                                                                   // 116
    /* If expected is a DOM node, do a literal '===' comparison with                               // 117
     * actual. Otherwise do a deep comparison, as implemented by _.isEqual.                        // 118
     */                                                                                            // 119
                                                                                                   // 120
    var matched;                                                                                   // 121
    // XXX remove cruft specific to liverange                                                      // 122
    if (typeof expected === "object" && expected && expected.nodeType) {                           // 123
      matched = expected === actual;                                                               // 124
      expected = "[Node]";                                                                         // 125
      actual = "[Unknown]";                                                                        // 126
    } else if (typeof Uint8Array !== 'undefined' && expected instanceof Uint8Array) {              // 127
      // I have no idea why but _.isEqual on Chrome horks completely on Uint8Arrays.               // 128
      // and the symptom is the chrome renderer taking up an entire CPU and freezing               // 129
      // your web page, but not pausing anywhere in _.isEqual.  I don't understand it              // 130
      // but we fall back to a manual comparison                                                   // 131
      if (!(actual instanceof Uint8Array))                                                         // 132
        this.fail({type: "assert_equal", message: "found object is not a typed array",             // 133
                   expected: "A typed array", actual: actual.constructor.toString()});             // 134
      if (expected.length !== actual.length)                                                       // 135
        this.fail({type: "assert_equal", message: "lengths of typed arrays do not match",          // 136
                   expected: expected.length, actual: actual.length});                             // 137
      for (var i = 0; i < expected.length; i++) {                                                  // 138
        this.equal(actual[i], expected[i]);                                                        // 139
      }                                                                                            // 140
    } else {                                                                                       // 141
      matched = EJSON.equals(expected, actual);                                                    // 142
    }                                                                                              // 143
                                                                                                   // 144
    if (matched === !!not) {                                                                       // 145
      this.fail({type: "assert_equal", message: message,                                           // 146
                 expected: JSON.stringify(expected), actual: JSON.stringify(actual), not: !!not}); // 147
    } else                                                                                         // 148
      this.ok();                                                                                   // 149
  },                                                                                               // 150
                                                                                                   // 151
  notEqual: function (actual, expected, message) {                                                 // 152
    this.equal(actual, expected, message, true);                                                   // 153
  },                                                                                               // 154
                                                                                                   // 155
  instanceOf: function (obj, klass) {                                                              // 156
    if (obj instanceof klass)                                                                      // 157
      this.ok();                                                                                   // 158
    else                                                                                           // 159
      this.fail({type: "instanceOf"}); // XXX what other data?                                     // 160
  },                                                                                               // 161
                                                                                                   // 162
  matches: function (actual, regexp, message) {                                                    // 163
    if (regexp.test(actual))                                                                       // 164
      this.ok();                                                                                   // 165
    else                                                                                           // 166
      this.fail({type: "matches", message: message,                                                // 167
                 actual: actual, regexp: regexp.toString()});                                      // 168
  },                                                                                               // 169
                                                                                                   // 170
  // XXX nodejs assert.throws can take an expected error, as a class,                              // 171
  // regular expression, or predicate function.  However, with its                                 // 172
  // implementation if a constructor (class) is passed in and `actual`                             // 173
  // fails the instanceof test, the constructor is then treated as                                 // 174
  // a predicate and called with `actual` (!)                                                      // 175
  //                                                                                               // 176
  // expected can be:                                                                              // 177
  //  undefined: accept any exception.                                                             // 178
  //  regexp: accept an exception with message passing the regexp.                                 // 179
  //  function: call the function as a predicate with the exception.                               // 180
  throws: function (f, expected) {                                                                 // 181
    var actual, predicate;                                                                         // 182
                                                                                                   // 183
    if (expected === undefined)                                                                    // 184
      predicate = function (actual) {                                                              // 185
        return true;                                                                               // 186
      };                                                                                           // 187
    else if (expected instanceof RegExp)                                                           // 188
      predicate = function (actual) {                                                              // 189
        return expected.test(actual.message)                                                       // 190
      };                                                                                           // 191
    else if (typeof expected === 'function')                                                       // 192
      predicate = expected;                                                                        // 193
    else                                                                                           // 194
      throw new Error('expected should be a predicate function or regexp');                        // 195
                                                                                                   // 196
    try {                                                                                          // 197
      f();                                                                                         // 198
    } catch (exception) {                                                                          // 199
      actual = exception;                                                                          // 200
    }                                                                                              // 201
                                                                                                   // 202
    if (actual && predicate(actual))                                                               // 203
      this.ok({message: actual.message});                                                          // 204
    else                                                                                           // 205
      this.fail({type: "throws"});                                                                 // 206
  },                                                                                               // 207
                                                                                                   // 208
  isTrue: function (v, msg) {                                                                      // 209
    if (v)                                                                                         // 210
      this.ok();                                                                                   // 211
    else                                                                                           // 212
      this.fail({type: "true", message: msg});                                                     // 213
  },                                                                                               // 214
                                                                                                   // 215
  isFalse: function (v, msg) {                                                                     // 216
    if (v)                                                                                         // 217
      this.fail({type: "true", message: msg});                                                     // 218
    else                                                                                           // 219
      this.ok();                                                                                   // 220
  },                                                                                               // 221
                                                                                                   // 222
  isNull: function (v, msg) {                                                                      // 223
    if (v === null)                                                                                // 224
      this.ok();                                                                                   // 225
    else                                                                                           // 226
      this.fail({type: "null", message: msg});                                                     // 227
  },                                                                                               // 228
                                                                                                   // 229
  isNotNull: function (v, msg) {                                                                   // 230
    if (v === null)                                                                                // 231
      this.fail({type: "true", message: msg});                                                     // 232
    else                                                                                           // 233
      this.ok();                                                                                   // 234
  },                                                                                               // 235
                                                                                                   // 236
  isUndefined: function (v, msg) {                                                                 // 237
    if (v === undefined)                                                                           // 238
      this.ok();                                                                                   // 239
    else                                                                                           // 240
      this.fail({type: "undefined", message: msg});                                                // 241
  },                                                                                               // 242
                                                                                                   // 243
  isNaN: function (v, msg) {                                                                       // 244
    if (isNaN(v))                                                                                  // 245
      this.ok();                                                                                   // 246
    else                                                                                           // 247
      this.fail({type: "NaN", message: msg});                                                      // 248
  },                                                                                               // 249
                                                                                                   // 250
  include: function (s, v) {                                                                       // 251
    var pass = false;                                                                              // 252
    if (s instanceof Array)                                                                        // 253
      pass = _.any(s, function(it) {return _.isEqual(v, it);});                                    // 254
    else if (typeof s === "object")                                                                // 255
      pass = v in s;                                                                               // 256
    else if (typeof s === "string")                                                                // 257
      if (s.indexOf(v) > -1) {                                                                     // 258
        pass = true;                                                                               // 259
      }                                                                                            // 260
    else                                                                                           // 261
      /* fail -- not something that contains other things */;                                      // 262
    if (pass)                                                                                      // 263
      this.ok();                                                                                   // 264
    else {                                                                                         // 265
      this.fail({type: "include", sequence: s, should_contain_value: v});                          // 266
    }                                                                                              // 267
  },                                                                                               // 268
                                                                                                   // 269
  // XXX should change to lengthOf to match vowsjs                                                 // 270
  length: function (obj, expected_length) {                                                        // 271
    if (obj.length === expected_length)                                                            // 272
      this.ok();                                                                                   // 273
    else                                                                                           // 274
      this.fail({type: "length", expected: expected_length,                                        // 275
                 actual: obj.length});                                                             // 276
  },                                                                                               // 277
                                                                                                   // 278
  // EXPERIMENTAL way to compare two strings that results in                                       // 279
  // a nicer display in the test runner, e.g. for multiline                                        // 280
  // strings                                                                                       // 281
  _stringEqual: function (actual, expected, message) {                                             // 282
    if (actual !== expected) {                                                                     // 283
      this.fail({type: "string_equal",                                                             // 284
                 message: message,                                                                 // 285
                 expected: expected,                                                               // 286
                 actual: actual});                                                                 // 287
    } else {                                                                                       // 288
      this.ok();                                                                                   // 289
    }                                                                                              // 290
  }                                                                                                // 291
                                                                                                   // 292
                                                                                                   // 293
});                                                                                                // 294
                                                                                                   // 295
/******************************************************************************/                   // 296
/* TestCase                                                                   */                   // 297
/******************************************************************************/                   // 298
                                                                                                   // 299
TestCase = function (name, func, async) {                                                          // 300
  var self = this;                                                                                 // 301
  self.name = name;                                                                                // 302
  self.func = func;                                                                                // 303
  self.async = async || false;                                                                     // 304
                                                                                                   // 305
  var nameParts = _.map(name.split(" - "), function(s) {                                           // 306
    return s.replace(/^\s*|\s*$/g, ""); // trim                                                    // 307
  });                                                                                              // 308
  self.shortName = nameParts.pop();                                                                // 309
  nameParts.unshift("tinytest");                                                                   // 310
  self.groupPath = nameParts;                                                                      // 311
};                                                                                                 // 312
                                                                                                   // 313
_.extend(TestCase.prototype, {                                                                     // 314
  // Run the test asynchronously, delivering results via onEvent;                                  // 315
  // then call onComplete() on success, or else onException(e) if the                              // 316
  // test raised (or voluntarily reported) an exception.                                           // 317
  run: function (onEvent, onComplete, onException, stop_at_offset) {                               // 318
    var self = this;                                                                               // 319
                                                                                                   // 320
    var completed = false;                                                                         // 321
    var markComplete = function () {                                                               // 322
      if (completed) {                                                                             // 323
        Meteor._debug("*** Test error -- test '" + self.name +                                     // 324
                      "' returned multiple times.");                                               // 325
        return false;                                                                              // 326
      }                                                                                            // 327
      completed = true;                                                                            // 328
      return true;                                                                                 // 329
    };                                                                                             // 330
                                                                                                   // 331
    var wrappedOnEvent = function (e) {                                                            // 332
      // If this trace prints, it means you ran some test.* function after the                     // 333
      // test finished! Another symptom will be that the test will display as                      // 334
      // "waiting" even when it counts as passed or failed.                                        // 335
      if (completed)                                                                               // 336
        console.trace("event after complete!");                                                    // 337
      return onEvent(e);                                                                           // 338
    };                                                                                             // 339
                                                                                                   // 340
    var results = new TestCaseResults(self, wrappedOnEvent,                                        // 341
                                      function (e) {                                               // 342
                                        if (markComplete())                                        // 343
                                          onException(e);                                          // 344
                                      }, stop_at_offset);                                          // 345
                                                                                                   // 346
    Meteor.defer(function () {                                                                     // 347
      try {                                                                                        // 348
        if (self.async) {                                                                          // 349
          self.func(results, function () {                                                         // 350
            if (markComplete())                                                                    // 351
              onComplete();                                                                        // 352
          });                                                                                      // 353
        } else {                                                                                   // 354
          self.func(results);                                                                      // 355
          if (markComplete())                                                                      // 356
            onComplete();                                                                          // 357
        }                                                                                          // 358
      } catch (e) {                                                                                // 359
        if (markComplete())                                                                        // 360
          onException(e);                                                                          // 361
      }                                                                                            // 362
    });                                                                                            // 363
  }                                                                                                // 364
});                                                                                                // 365
                                                                                                   // 366
/******************************************************************************/                   // 367
/* TestManager                                                                */                   // 368
/******************************************************************************/                   // 369
                                                                                                   // 370
TestManager = function () {                                                                        // 371
  var self = this;                                                                                 // 372
  self.tests = {};                                                                                 // 373
  self.ordered_tests = [];                                                                         // 374
};                                                                                                 // 375
                                                                                                   // 376
_.extend(TestManager.prototype, {                                                                  // 377
  addCase: function (test) {                                                                       // 378
    var self = this;                                                                               // 379
    if (test.name in self.tests)                                                                   // 380
      throw new Error(                                                                             // 381
        "Every test needs a unique name, but there are two tests named '" +                        // 382
          test.name + "'");                                                                        // 383
    self.tests[test.name] = test;                                                                  // 384
    self.ordered_tests.push(test);                                                                 // 385
  },                                                                                               // 386
                                                                                                   // 387
  createRun: function (onReport, pathPrefix) {                                                     // 388
    var self = this;                                                                               // 389
    return new TestRun(self, onReport, pathPrefix);                                                // 390
  }                                                                                                // 391
});                                                                                                // 392
                                                                                                   // 393
// singleton                                                                                       // 394
TestManager = new TestManager;                                                                     // 395
                                                                                                   // 396
/******************************************************************************/                   // 397
/* TestRun                                                                    */                   // 398
/******************************************************************************/                   // 399
                                                                                                   // 400
TestRun = function (manager, onReport, pathPrefix) {                                               // 401
  var self = this;                                                                                 // 402
  self.manager = manager;                                                                          // 403
  self.onReport = onReport;                                                                        // 404
  self.next_sequence_number = 0;                                                                   // 405
  self._pathPrefix = pathPrefix || [];                                                             // 406
  _.each(self.manager.ordered_tests, function (test) {                                             // 407
    if (self._prefixMatch(test.groupPath))                                                         // 408
      self._report(test);                                                                          // 409
  });                                                                                              // 410
};                                                                                                 // 411
                                                                                                   // 412
_.extend(TestRun.prototype, {                                                                      // 413
                                                                                                   // 414
  _prefixMatch: function (testPath) {                                                              // 415
    var self = this;                                                                               // 416
    for (var i = 0; i < self._pathPrefix.length; i++) {                                            // 417
      if (!testPath[i] || self._pathPrefix[i] !== testPath[i]) {                                   // 418
        return false;                                                                              // 419
      }                                                                                            // 420
    }                                                                                              // 421
    return true;                                                                                   // 422
  },                                                                                               // 423
                                                                                                   // 424
  _runOne: function (test, onComplete, stop_at_offset) {                                           // 425
    var self = this;                                                                               // 426
    var startTime = (+new Date);                                                                   // 427
    if (self._prefixMatch(test.groupPath)) {                                                       // 428
      test.run(function (event) {                                                                  // 429
        /* onEvent */                                                                              // 430
        self._report(test, event);                                                                 // 431
      }, function () {                                                                             // 432
        /* onComplete */                                                                           // 433
        var totalTime = (+new Date) - startTime;                                                   // 434
        self._report(test, {type: "finish", timeMs: totalTime});                                   // 435
        onComplete && onComplete();                                                                // 436
      }, function (exception) {                                                                    // 437
        /* onException */                                                                          // 438
                                                                                                   // 439
        // XXX you want the "name" and "message" fields on the                                     // 440
        // exception, to start with..                                                              // 441
        self._report(test, {                                                                       // 442
          type: "exception",                                                                       // 443
          details: {                                                                               // 444
            message: exception.message, // XXX empty???                                            // 445
            stack: exception.stack // XXX portability                                              // 446
          }                                                                                        // 447
        });                                                                                        // 448
                                                                                                   // 449
        onComplete && onComplete();                                                                // 450
      }, stop_at_offset);                                                                          // 451
    } else {                                                                                       // 452
      onComplete && onComplete();                                                                  // 453
    }                                                                                              // 454
  },                                                                                               // 455
                                                                                                   // 456
  run: function (onComplete) {                                                                     // 457
    var self = this;                                                                               // 458
    // create array of arrays of tests; synchronous tests in                                       // 459
    // different groups are run in parallel on client, async tests or                              // 460
    // tests in different groups are run in sequence, as are all                                   // 461
    // tests on server                                                                             // 462
    var testGroups = _.values(                                                                     // 463
      _.groupBy(self.manager.ordered_tests,                                                        // 464
                function(t) {                                                                      // 465
                  if (Meteor.isServer)                                                             // 466
                    return "SERVER";                                                               // 467
                  if (t.async)                                                                     // 468
                    return "ASYNC";                                                                // 469
                  return t.name.split(" - ")[0];                                                   // 470
                }));                                                                               // 471
                                                                                                   // 472
    if (! testGroups.length) {                                                                     // 473
      onComplete();                                                                                // 474
    } else {                                                                                       // 475
      var groupsDone = 0;                                                                          // 476
                                                                                                   // 477
      _.each(testGroups, function(tests) {                                                         // 478
        var runNext = function () {                                                                // 479
          if (tests.length) {                                                                      // 480
            self._runOne(tests.shift(), runNext);                                                  // 481
          } else {                                                                                 // 482
            groupsDone++;                                                                          // 483
            if (groupsDone >= testGroups.length)                                                   // 484
              onComplete();                                                                        // 485
          }                                                                                        // 486
        };                                                                                         // 487
                                                                                                   // 488
        runNext();                                                                                 // 489
      });                                                                                          // 490
    }                                                                                              // 491
  },                                                                                               // 492
                                                                                                   // 493
  // An alternative to run(). Given the 'cookie' attribute of a                                    // 494
  // failure record, try to rerun that particular test up to that                                  // 495
  // failure, and then open the debugger.                                                          // 496
  debug: function (cookie, onComplete) {                                                           // 497
    var self = this;                                                                               // 498
    var test = self.manager.tests[cookie.name];                                                    // 499
    if (!test)                                                                                     // 500
      throw new Error("No such test '" + cookie.name + "'");                                       // 501
    self._runOne(test, onComplete, cookie.offset);                                                 // 502
  },                                                                                               // 503
                                                                                                   // 504
  _report: function (test, event) {                                                                // 505
    var self = this;                                                                               // 506
    if (event)                                                                                     // 507
      var events = [_.extend({sequence: self.next_sequence_number++}, event)];                     // 508
    else                                                                                           // 509
      var events = [];                                                                             // 510
    self.onReport({                                                                                // 511
      groupPath: test.groupPath,                                                                   // 512
      test: test.shortName,                                                                        // 513
      events: events                                                                               // 514
    });                                                                                            // 515
  }                                                                                                // 516
});                                                                                                // 517
                                                                                                   // 518
/******************************************************************************/                   // 519
/* Public API                                                                 */                   // 520
/******************************************************************************/                   // 521
                                                                                                   // 522
Tinytest = {};                                                                                     // 523
                                                                                                   // 524
Tinytest.add = function (name, func) {                                                             // 525
  TestManager.addCase(new TestCase(name, func));                                                   // 526
};                                                                                                 // 527
                                                                                                   // 528
Tinytest.addAsync = function (name, func) {                                                        // 529
  TestManager.addCase(new TestCase(name, func, true));                                             // 530
};                                                                                                 // 531
                                                                                                   // 532
// Run every test, asynchronously. Runs the test in the current                                    // 533
// process only (if called on the server, runs the tests on the                                    // 534
// server, and likewise for the client.) Report results via                                        // 535
// onReport. Call onComplete when it's done.                                                       // 536
//                                                                                                 // 537
Tinytest._runTests = function (onReport, onComplete, pathPrefix) {                                 // 538
  var testRun = TestManager.createRun(onReport, pathPrefix);                                       // 539
  testRun.run(onComplete);                                                                         // 540
};                                                                                                 // 541
                                                                                                   // 542
// Run just one test case, and stop the debugger at a particular                                   // 543
// error, all as indicated by 'cookie', which will have come from a                                // 544
// failure event output by _runTests.                                                              // 545
//                                                                                                 // 546
Tinytest._debugTest = function (cookie, onReport, onComplete) {                                    // 547
  var testRun = TestManager.createRun(onReport);                                                   // 548
  testRun.debug(cookie, onComplete);                                                               // 549
};                                                                                                 // 550
                                                                                                   // 551
/////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                 //
// packages/tinytest/model.js                                                                      //
//                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                   //
Meteor._ServerTestResultsSubscription = 'tinytest_results_subscription';                           // 1
Meteor._ServerTestResultsCollection = 'tinytest_results_collection';                               // 2
                                                                                                   // 3
/////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                 //
// packages/tinytest/tinytest_server.js                                                            //
//                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                   //
var Fiber = Npm.require('fibers');                                                                 // 1
var handlesForRun = {};                                                                            // 2
var reportsForRun = {};                                                                            // 3
                                                                                                   // 4
Meteor.publish(Meteor._ServerTestResultsSubscription, function (runId) {                           // 5
  check(runId, String);                                                                            // 6
  var self = this;                                                                                 // 7
  if (!_.has(handlesForRun, runId))                                                                // 8
    handlesForRun[runId] = [self];                                                                 // 9
  else                                                                                             // 10
    handlesForRun[runId].push(self);                                                               // 11
  self.onStop(function () {                                                                        // 12
    handlesForRun[runId] = _.without(handlesForRun[runId], self);                                  // 13
  });                                                                                              // 14
  if (_.has(reportsForRun, runId)) {                                                               // 15
    self.added(Meteor._ServerTestResultsCollection, runId,                                         // 16
               reportsForRun[runId]);                                                              // 17
  } else {                                                                                         // 18
    self.added(Meteor._ServerTestResultsCollection, runId, {});                                    // 19
  }                                                                                                // 20
  self.ready();                                                                                    // 21
});                                                                                                // 22
                                                                                                   // 23
Meteor.methods({                                                                                   // 24
  'tinytest/run': function (runId, pathPrefix) {                                                   // 25
    check(runId, String);                                                                          // 26
    check(pathPrefix, Match.Optional([String]));                                                   // 27
    this.unblock();                                                                                // 28
                                                                                                   // 29
    // XXX using private API === lame                                                              // 30
    var path = Npm.require('path');                                                                // 31
    var Future = Npm.require(path.join('fibers', 'future'));                                       // 32
    var future = new Future;                                                                       // 33
                                                                                                   // 34
    reportsForRun[runId] = {};                                                                     // 35
                                                                                                   // 36
    var onReport = function (report) {                                                             // 37
      if (! Fiber.current) {                                                                       // 38
        Meteor._debug("Trying to report a test not in a fiber! "+                                  // 39
                      "You probably forgot to wrap a callback in bindEnvironment.");               // 40
        console.trace();                                                                           // 41
      }                                                                                            // 42
      var dummyKey = Random.id();                                                                  // 43
      var fields = {};                                                                             // 44
      fields[dummyKey] = report;                                                                   // 45
      _.each(handlesForRun[runId], function (handle) {                                             // 46
        handle.changed(Meteor._ServerTestResultsCollection, runId, fields);                        // 47
      });                                                                                          // 48
      // Save for future subscriptions.                                                            // 49
      reportsForRun[runId][dummyKey] = report;                                                     // 50
    };                                                                                             // 51
                                                                                                   // 52
    var onComplete = function() {                                                                  // 53
      future['return']();                                                                          // 54
    };                                                                                             // 55
                                                                                                   // 56
    Tinytest._runTests(onReport, onComplete, pathPrefix);                                          // 57
                                                                                                   // 58
    future.wait();                                                                                 // 59
  },                                                                                               // 60
  'tinytest/clearResults': function (runId) {                                                      // 61
    check(runId, String);                                                                          // 62
    _.each(handlesForRun[runId], function (handle) {                                               // 63
      // XXX this doesn't actually notify the client that it has been                              // 64
      // unsubscribed.                                                                             // 65
      handle.stop();                                                                               // 66
    });                                                                                            // 67
    delete handlesForRun[runId];                                                                   // 68
    delete reportsForRun[runId];                                                                   // 69
  }                                                                                                // 70
});                                                                                                // 71
                                                                                                   // 72
/////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);
