u:Gem::Specificationi[I"
1.3.6:ETiI"redis-namespace; TU:Gem::Version[I"
1.4.1; TIu:	Time�l�    :@_zoneI"UTC; TI"Namespaces Redis commands.; TU:Gem::Requirement[[[I">=; TU;[I"0; TU;	[[[I">=; TU;[I"0; TI"	ruby; F[o:Gem::Dependency
:
@nameI"
redis; T:@requirementU;	[[[I"~>; TU;[I"
3.0.4; T:
@type:runtime:@prereleaseF:@version_requirements@"o;

;I"	rake; T;U;	[[[I">=; TU;[I"0; T;:development;F;@,o;

;I"
rspec; T;U;	[[[I">=; TU;[I"0; T;;;F;@60[I"chris@ozmm.org; TI"hone02@gmail.com; TI"steve@steveklabnik.com; T[I"Chris Wanstrath; TI"Terence Lee; TI"Steve Klabnik; TI"�Adds a Redis::Namespace class which can be used to namespace calls
to Redis. This is useful when using a single instance of Redis with
multiple, different applications.
; TI"-http://github.com/resque/redis-namespace; TT@[ 